# Hawaii

## Backend

### Getting Started
- These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
- What things you need to set up before starting server:

- Install [Java 11](https://jdk.java.net/java-se-ri/11) and set the `JAVA_HOME` Environment Variable. It is mandatory to install `OpenJDK` (not the Oracle JDK).
- Install [Maven](https://maven.apache.org/install.html)
- Install [MySQL](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/)
- Install [IntelliJ](https://www.jetbrains.com/idea/download/)
- Install [Tomcat 9](https://octopus.com/blog/installing-tomcat-from-scratch) (not necessary if running the application through IntelliJ)

#### Common
- Install Lombok plugin in IntelliJ
- Install PlantUml plugin in IntelliJ

#### Coding style in Java
- Predefined code formatting scheme needs to be imported from config/formatter.xml into IDE

#### Other

- To connect with Google services, the application needs to be registered in the Google Cloud Platform API console and 
    provided with service account credentials. Credentials are stored in a JSON file that can be obtained from
     the API console. To create the credentials file, go to 
```
Google API console > Credentials > Create credentials > 
Service account key > Choose JSON (default) > Create.
```
Save the generated file to the applications `src/main/resources` folder and rename it
to `service_account.json`.

- Create `application.properties` in reference to `application.properties.example` and save file to `src/main/resources`
    folder.

- Create `.env` file in reference to `.env.example` and save file to `src/main/frontend`

### Create MySQL database
```sh
Database name: hawaii
Port: 3306
Username: root
Password: root
```

### Building application

##### 1. Navigate to project root in a terminal 

##### 2. Generate war file <br/>

To generate war file with default profile (automatically builds frontend as build-frontend is default profile) enter
    the command:
```
mvn clean install 
```
 which is the same as entering:
```
mvn clean install -P build-frontend
```
To generate war file with development profile (does not build frontend automatically) enter the command:
```
mvn clean install -P dev
```

### Importing files 
Before starting the application, five files need to be imported. The instruction is linked at the end of this page.

### Starting application with maven
To run the application with default profile enter command:
```
mvn spring-boot:run
```
which is the same as entering:
```
mvn spring-boot:run -P build-frontend 
```
and runs the application with the frontend already served on top of the backend.

To run the application with development profile enter command: 
```
mvn spring-boot:run -P dev 
```
runs application without frontend which needs to be built separately and served on another port. 

Go to http://localhost:8080/ to see results

### Starting application with tomcat

##### 1. Move war file from _target_ folder to tomcat _webapps_ folder

##### 2. Start tomcat with command: <br/>
```
service tomcat start 
```

Or startup.sh from bin folder <br/>
```
./startup.sh
```

Then go to http://localhost:8080/hawaii (*) to see results

(*) _/hawaii_ path depends on tomcat configuration

##### 3 . Stop tomcat with: <br/>
```
./shutdown.sh 
```

## Frontend

### Getting Started

- These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

#### What will you need to set up before starting the server

- Install [Node](https://nodejs.org/en/)
- Install [Yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable)

### Starting server

##### 1. Navigate to <br/>
```
project_root/src/main/frontend
```

##### 2. Install node_modules <br/>
```
yarn or npm install
```

##### 3. And start with <br/>
```
yarn start or npm start
```

Then open http://localhost:3000/ to see your app

Install Redux DevTools plugin for your browser

### Swagger

Always you can check endpoints on URL:

```http://localhost:8080/swagger-ui.html```

## Releases

### Getting Started

- Only tags are deployed to staging and production, there are two types of releases <b>pre-release</b> and <b>release</b>.

- There should be two segments at the description of every release which are <b>Fixes</b> and <b>Enhancements</b>, and they should have 
    all the issues between previous and new releases linked in them to indicate changes made between concurrent
     releases and pre-releases.
    
- <b>Pre-releases</b> should have only changes between previous release or pre-release displayed where <b>Releases</b> 
    should have everything from the previous release displayed meaning contents off all pre-releases following the final 
    release of that version.
      

### Pre-releases 

- Tags that are meant to be deployed to the staging server, they should be one version higher than the latest release and have 
    suffix <b>-beta.x</b> where x is number of beta release for that specific version.
    
- Assuming that <b>Latest release</b> is <b>v1.0.1</b> examples of pre release tags are <b>v1.0.2-beta.1, v1.0.2-beta.2</b>

- Description for version <b>v1.0.2-beta.1</b> should have fixes and enhancements made after release of version <b>v1.0.1</b> 
    where description for version <b>v1.0.2-beta.2</b> should have fixes and enhancements made after release of 
    version <b>v1.0.2-beta.1</b> 
    
### Releases

- Tags that are meant to be deployed to the production server, they should have the same version as the most recent pre-release 
     version minus the suffix <b>-beta.x</b>.
     
- Assuming that latest <b>Pre-release</b> is <b>v1.0.2-beta.8</b> then <b>Latest release</b> should have version <b>v1.0.2</b>

- Description for version <b>v1.0.2</b> should have all fixes and enhancements made after release of version <b>v1.0.1</b>
    meaning descriptions of all pre-release versions <b>v1.0.2-beta.x</b> preceding final release of version <b>v1.0.2</b>   

## Deployment 
#### - Manual deployment of application to staging server
<b>Detailed explanation for obtaining and modifying import csv files can be found  [here](https://docs.google.com/document/d/14jZW_AdAGsxGBPvFVbFPf4ASCUUp_DcDL9lfWjRDJtw/edit).</b>
<b>Detailed explanation for manual deployment of Hawaii application to staging can be found [here](https://docs.google.com/document/d/1HSEqODsAeOjbJrXwTCUuQl0x0KvEoJnnr5UUF47c7DU).</b>
<b>Detailed explanation for importing data from csv files can be found [here](https://docs.google.com/document/d/1HSEqODsAeOjbJrXwTCUuQl0x0KvEoJnnr5UUF47c7DU).</b>

#### - Deployment of application to staging and production server using Jenkins
<b>Detailed explanation for the deployment of Hawaii application to staging and production with Jenkins can be found [here](https://docs.google.com/document/d/1SvlKMv5LpxpPbIg-6I-3T5IdGbtYiemWFJlY2N8-y2o).</b>
