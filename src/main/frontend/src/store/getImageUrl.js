export const getImageUrl = (image, size) => {
  return `${image}=s${size}`;
};
