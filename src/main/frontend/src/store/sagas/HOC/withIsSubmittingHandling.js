import { put, select } from 'redux-saga/effects';
import { isSubmitted, isSubmitting } from '../../actions/isSubmittingActions';
import { getIsSubmitting } from '../../selectors';

export function withSubmittingHandling(saga) {
  return function* sagaWithSubmitHandler(...args) {
    const isSubmittingValue = yield select(getIsSubmitting);

    if (!isSubmittingValue) {
      try {
        // yield* iterates over a generator and yields every value from it
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/yield*
        yield put(isSubmitting());
        yield* saga(...args);
      } finally {
        yield put(isSubmitted());
      }
    }
  };
}
