import { put } from 'redux-saga/effects';
import { loadingEnd, loadingStart } from '../../actions/loadingActions';

export function withLoader(saga) {
  return function* sagaWithLoader(...args) {
    try {
      yield put(loadingStart(...args));
      yield* saga(...args);
    } catch (e) {
      throw e;
    } finally {
      yield put(loadingEnd(...args));
    }
  };
}
