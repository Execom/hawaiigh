import { call, put } from 'redux-saga/effects';
import {
  requestDataExport,
  successReceivingDataExport,
  errorReceivingDataExport
} from '../actions/exportActions';
import {
  sagaWithErrorHandling,
  extractExportDataFromResponse,
  downloadFile
} from './helpers/sagaHelpers';
import {
  getExportRequestsApi,
  getExportUsersApi,
  getExportAllowancesApi,
  getExportLeaveProfilesApi
} from '@/store/services/exportService';
import {
  LEAVE_REQUESTS_REPORT,
  SICKNESS_REPORT,
  USERS_REPORT,
  ALLOWANCES_REPORT,
  LEAVE_PROFILES_REPORT
} from '@/helpers/enum';

const getDataExportSaga = function*({ payload }) {
  const { queryParams, reportType } = { ...payload };
  let response;
  switch (reportType) {
    case LEAVE_REQUESTS_REPORT:
    case SICKNESS_REPORT:
      response = yield call(getExportRequestsApi(queryParams));
      break;
    case USERS_REPORT:
      response = yield call(getExportUsersApi(queryParams));
      break;
    case ALLOWANCES_REPORT:
      response = yield call(getExportAllowancesApi(queryParams));
      break;
    case LEAVE_PROFILES_REPORT:
      response = yield call(getExportLeaveProfilesApi(queryParams));
      break;
    default:
      return;
  }
  const { data, filename } = extractExportDataFromResponse(response);

  yield put(successReceivingDataExport());
  downloadFile(data, filename, queryParams.exportFormat);
};

export const exportSaga = [
  sagaWithErrorHandling(
    requestDataExport,
    getDataExportSaga,
    errorReceivingDataExport
  )
];
