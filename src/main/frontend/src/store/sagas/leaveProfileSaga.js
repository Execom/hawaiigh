import { call, put } from 'redux-saga/effects';
import { closeDrawer } from '../actions/drawerActions';
import {
  createLeaveProfile,
  createLeaveProfileError,
  createLeaveProfileSuccessful,
  errorReceivingLeaveProfile,
  errorReceivingLeaveProfiles,
  errorRemovingLeaveProfile,
  receiveLeaveProfile,
  receiveLeaveProfiles,
  removeLeaveProfile,
  removeLeaveProfileSuccess,
  requestLeaveProfile,
  requestLeaveProfiles,
  updateLeaveProfile,
  updateLeaveProfileError,
  updateLeaveProfileSuccessful
} from '../actions/leaveProfileActions';
import {
  createLeaveProfileApi,
  getLeaveProfileApi,
  getLeaveProfilesApi,
  removeLeaveProfileApi,
  updateLeaveProfileApi
} from '../services/leaveProfileService';
import {
  sagaWithErrorHandling,
  sagaWithSubmitWrapper
} from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';
import { setShouldTableScrollToTop } from '../actions/administrationActions';

const getLeaveProfileSaga = function*({ payload }) {
  const leaveProfile = yield call(getLeaveProfileApi(payload));
  const leaveProfileInDays = { ...leaveProfile };

  leaveProfileInDays.entitlement /= 8;
  leaveProfileInDays.maxAllowanceFromNextYear /= 8;
  leaveProfileInDays.maxBonusDays /= 8;
  leaveProfileInDays.maxCarriedOver /= 8;
  leaveProfileInDays.training /= 8;

  yield put(receiveLeaveProfile(leaveProfileInDays));
};

const removeLeaveProfileSaga = function*({ payload: { id } }) {
  yield call(removeLeaveProfileApi(id));
  yield put(removeLeaveProfileSuccess());
  yield put(toastrSuccess('Succesfully removed leave profile'));
  yield put(
    requestLeaveProfiles({
      page: 0,
      size: 30
    })
  );
};

const createLeaveProfileSaga = function*({ payload }) {
  const payloadInDays = { ...payload };

  payloadInDays.entitlement *= 8;
  payloadInDays.maxAllowanceFromNextYear *= 8;
  payloadInDays.maxBonusDays *= 8;
  payloadInDays.maxCarriedOver *= 8;
  payloadInDays.training *= 8;
  payloadInDays.leaveProfileType = 'CUSTOM';

  yield call(createLeaveProfileApi(payloadInDays));
  yield put(createLeaveProfileSuccessful());
  yield put(toastrSuccess('Successfully created LeaveProfile'));
  yield put(setShouldTableScrollToTop(true));
  yield put(closeDrawer());
  yield put(
    requestLeaveProfiles({
      page: 0,
      size: 30
    })
  );
};

const updateLeaveProfileSaga = function*({ payload }) {
  const payloadInDays = { ...payload };

  payloadInDays.entitlement *= 8;
  payloadInDays.maxAllowanceFromNextYear *= 8;
  payloadInDays.maxBonusDays *= 8;
  payloadInDays.maxCarriedOver *= 8;
  payloadInDays.training *= 8;

  yield call(updateLeaveProfileApi(payloadInDays));
  yield put(updateLeaveProfileSuccessful());
  yield put(toastrSuccess('Succesfully updated leave profile'));
  yield put(closeDrawer());
  yield put(
    requestLeaveProfiles({
      page: 0,
      size: 30
    })
  );
};

const getAllLeaveProfilesSaga = function*({ payload }) {
  const leaveProfiles = yield call(getLeaveProfilesApi(payload));

  yield put(receiveLeaveProfiles(leaveProfiles));
};

export const leaveProfileSaga = [
  sagaWithErrorHandling(
    requestLeaveProfile,
    getLeaveProfileSaga,
    errorReceivingLeaveProfile
  ),
  sagaWithErrorHandling(
    removeLeaveProfile,
    removeLeaveProfileSaga,
    errorRemovingLeaveProfile
  ),
  sagaWithSubmitWrapper(
    createLeaveProfile,
    createLeaveProfileSaga,
    createLeaveProfileError
  ),
  sagaWithSubmitWrapper(
    updateLeaveProfile,
    updateLeaveProfileSaga,
    updateLeaveProfileError
  ),
  sagaWithErrorHandling(
    requestLeaveProfiles,
    getAllLeaveProfilesSaga,
    errorReceivingLeaveProfiles
  )
];
