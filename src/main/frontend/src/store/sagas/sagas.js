import { all } from 'redux-saga/effects';
import { absenceSaga } from './absenceSaga';
import { allowanceSaga } from './allowanceSaga';
import { auditSaga } from './auditSaga';
import { employeeSaga } from './employeeSaga';
import { authenticateSaga } from './getTokenFromLocalStorageSaga';
import { getTokenSaga } from './getTokenSaga';
import { leaveProfileSaga } from './leaveProfileSaga';
import { modalSaga } from './modalSaga';
import { publicHolidaySaga } from './publicHolidaySaga';
import { requestsSaga } from './requestsSaga';
import { teamSaga } from './teamSaga';
import { userSaga } from './userSaga';
import { yearSaga } from './yearSaga';
import { exportSaga } from './exportSaga';
import { monitoringSaga } from './monitoringSaga';
import { adminSaga } from './adminSaga';

export default function* saga() {
  yield all([
    ...getTokenSaga,
    ...authenticateSaga,
    ...teamSaga,
    ...userSaga,
    ...employeeSaga,
    ...absenceSaga,
    ...publicHolidaySaga,
    ...leaveProfileSaga,
    ...allowanceSaga,
    ...auditSaga,
    ...yearSaga,
    ...modalSaga,
    ...requestsSaga,
    ...exportSaga,
    ...monitoringSaga,
    ...adminSaga
  ]);
}
