import _includes from 'lodash/includes';
import moment from 'moment';
import { call, put, select } from 'redux-saga/effects';
import { requestAllowance } from '../actions/allowanceActions';
import { closeModal } from '../actions/modalActions';
import {
  createBonusRequest,
  createBonusRequestError,
  createBonusRequestSuccessful,
  createLeaveRequest,
  createLeaveRequestError,
  createLeaveRequestSuccessful,
  createSicknessRequest,
  createSicknessRequestError,
  createSicknessRequestSuccessful,
  errorReceivingRequestsForApproval,
  errorReceivingTeamRequestsForMonth,
  handleRequest,
  handleRequestError,
  handleRequestSuccessful,
  receiveLeaveRequestsForYear,
  receiveRequestsForApproval,
  receiveTeamRequestsForMonth,
  receiveUserLeaveRequestsForYear,
  requestLeaveRequestsForYear,
  requestLeaveRequestsForYearError,
  requestRequestsForApproval,
  requestTeamRequestsForMonth,
  requestUserLeaveRequestsForYear,
  requestUserLeaveRequestsForYearError
} from '../actions/requestsActions';
import { updateUserDaysForTeam } from '../actions/teamActions';
import {
  requestUserDays,
  requestUpdateUserDaysForTeam,
  errorReceivingUserDaysForTeamUpdate
} from '../actions/userActions';
import { getSearchedEmployee, getSelectedDate, getUser } from '../selectors';
import {
  getRequestsForApprovalApi,
  getRequestsForTeamApi,
  handleRequestApi,
  requestApi,
  requestUserLeaveRequestsForYearApi
} from '../services/requestsService';
import { getUserDaysForMonthApi } from '../services/userService';
import {
  sagaWithErrorHandling,
  sagaWithSubmitWrapper
} from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';

const fetchDataAfterRequest = function*() {
  const selectedDate = yield select(getSelectedDate);
  const searchedEmployee = yield select(getSearchedEmployee);

  if (_includes(selectedDate, '-')) {
    const user = yield select(getUser);

    yield put(
      requestTeamRequestsForMonth({
        teamId: user.teamId,
        date: moment(selectedDate, 'YYYY-MMM').format('YYYY-MM')
      })
    );
    yield put(requestUpdateUserDaysForTeam(user, selectedDate));
  } else {
    if (searchedEmployee) {
      yield put(
        requestUserLeaveRequestsForYear({
          year: selectedDate,
          id: searchedEmployee.id
        })
      );
    } else {
      yield put(requestUserDays({ year: selectedDate }));
      yield put(requestAllowance(selectedDate));
      yield put(requestLeaveRequestsForYear({ year: selectedDate }));
    }
  }
};

const createLeaveRequestSaga = function*({ payload }) {
  yield call(requestApi(payload));
  yield put(createLeaveRequestSuccessful());
  yield put(toastrSuccess('Leave request successfully created'));
  yield fetchDataAfterRequest();
  yield put(closeModal('requestModal'));
};

const createSicknessRequestSaga = function*({ payload }) {
  yield call(requestApi(payload));
  yield put(createSicknessRequestSuccessful());
  yield put(toastrSuccess('Sickness request successfully created'));
  yield fetchDataAfterRequest();
  yield put(closeModal('requestModal'));
};

const createBonusRequestSaga = function*({ payload }) {
  yield call(requestApi(payload));
  yield put(createBonusRequestSuccessful());
  yield put(toastrSuccess('Bonus request successfully created'));
  yield fetchDataAfterRequest();
  yield put(closeModal('requestModal'));
};

const handleRequestSaga = function*({ payload }) {
  yield call(handleRequestApi(payload));
  yield put(handleRequestSuccessful());
  yield put(
    toastrSuccess(`Request successfully ${payload.requestStatus.toLowerCase()}`)
  );
  if (payload.isApprover) {
    yield put(requestRequestsForApproval());
  } else {
    const year = yield select(getSelectedDate);

    yield put(requestUserDays({ year }));
    yield put(requestAllowance(year));
    yield put(requestLeaveRequestsForYear({ year }));
  }
  yield put(closeModal('handleRequestModal'));
};

const requestLeaveRequestsForYearSaga = function*({ payload }) {
  const requestsInformation = yield call(
    requestUserLeaveRequestsForYearApi(payload)
  );

  yield put(receiveLeaveRequestsForYear(requestsInformation));
};

const requestUserLeaveRequestsForYearSaga = function*({ payload }) {
  const requestsInformation = yield call(
    requestUserLeaveRequestsForYearApi(payload)
  );

  yield put(receiveUserLeaveRequestsForYear(requestsInformation));
};

const requestRequestsForApprovalSaga = function*() {
  const requestsForApprovalInformation = yield call(
    getRequestsForApprovalApi()
  );

  yield put(receiveRequestsForApproval(requestsForApprovalInformation));
};

const requestLeaveRequestsForTeamSaga = function*({ payload }) {
  const requestsInformation = yield call(getRequestsForTeamApi(payload));

  yield put(receiveTeamRequestsForMonth(requestsInformation));
};

const requestUpdateUserDaysForTeamSaga = function*(user, selectedDate) {
  const userDays = yield call(
    getUserDaysForMonthApi({
      month: moment(selectedDate, 'YYYY-MMM').format('YYYY-MM'),
      userId: user.id
    })
  );
  yield put(updateUserDaysForTeam({ userId: user.id, userDays }));
};

export const requestsSaga = [
  sagaWithSubmitWrapper(
    createLeaveRequest,
    createLeaveRequestSaga,
    createLeaveRequestError
  ),
  sagaWithSubmitWrapper(
    createSicknessRequest,
    createSicknessRequestSaga,
    createSicknessRequestError
  ),
  sagaWithSubmitWrapper(
    createBonusRequest,
    createBonusRequestSaga,
    createBonusRequestError
  ),
  sagaWithSubmitWrapper(handleRequest, handleRequestSaga, handleRequestError),
  sagaWithErrorHandling(
    requestLeaveRequestsForYear,
    requestLeaveRequestsForYearSaga,
    requestLeaveRequestsForYearError
  ),
  sagaWithErrorHandling(
    requestUserLeaveRequestsForYear,
    requestUserLeaveRequestsForYearSaga,
    requestUserLeaveRequestsForYearError
  ),
  sagaWithErrorHandling(
    requestRequestsForApproval,
    requestRequestsForApprovalSaga,
    errorReceivingRequestsForApproval
  ),
  sagaWithErrorHandling(
    requestTeamRequestsForMonth,
    requestLeaveRequestsForTeamSaga,
    errorReceivingTeamRequestsForMonth
  ),
  sagaWithErrorHandling(
    requestUpdateUserDaysForTeam,
    requestUpdateUserDaysForTeamSaga,
    errorReceivingUserDaysForTeamUpdate
  )
];
