import _omit from 'lodash/omit';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { closeDrawer } from '../actions/drawerActions';
import { closeModal } from '../actions/modalActions';
import {
  adminSearchEmployees,
  adminSearchEmployeesSuccess,
  clearEmployees,
  createEmployee,
  createEmployeeSuccess,
  errorCreatingEmployee,
  errorReceivingEmployee,
  errorReceivingEmployees,
  errorRemovingEmployee,
  errorCancelEmployeeDelete,
  errorRestoringEmployee,
  errorSearchingEmployees,
  receiveEmployee,
  receiveEmployees,
  removeEmployee,
  cancelEmployeeDelete,
  cancelEmployeeDeleteSuccess,
  removeEmployeeSuccess,
  requestEmployee,
  requestEmployees,
  restoreEmployee,
  restoreEmployeeSuccess,
  searchEmployees,
  searchEmployeesSuccess,
  updateEmployee,
  updateEmployeeError,
  updateEmployeeSuccessful
} from '../actions/userActions';
import { getModal } from '../selectors';
import {
  createEmployeeApi,
  getEmployeeApi,
  getEmployeesApi,
  removeEmployeeApi,
  restoreEmployeeApi,
  searchEmployeesApi,
  updateEmployeeApi,
  cancelEmplyeeDeleteApi
} from '../services/userService';
import {
  sagaWithErrorHandling,
  sagaWithSubmitWrapper
} from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';
import { setShouldTableScrollToTop } from '../actions/administrationActions';
import { formatDate } from '../helperFunctions';

const createEmployeeSaga = function*(action) {
  yield call(createEmployeeApi(action.payload));
  yield put(createEmployeeSuccess());
  yield put(toastrSuccess('Succesfully created employee'));
  yield put(setShouldTableScrollToTop(true));
  yield put(closeDrawer());

  if (action.payload.activeTab[0] === action.payload.userStatusType) {
    yield put(
      adminSearchEmployees({
        page: 0,
        size: 30,
        userStatusType: action.payload.activeTab
      })
    );
  }
};

const getEmployeeSaga = function*({ payload }) {
  const employeeInformation = yield call(getEmployeeApi(payload));

  yield put(receiveEmployee(employeeInformation));
};

const updateEmployeeSaga = function*({ payload }) {
  const modal = yield select(getModal);

  yield call(updateEmployeeApi(payload));
  yield put(updateEmployeeSuccessful());
  yield put(toastrSuccess('Succesfully updated employee'));
  yield put(closeDrawer());
  yield put(
    adminSearchEmployees({
      page: 0,
      size: 30,
      userStatusType: [payload.activeTab]
    })
  );

  if (modal.open) {
    yield put(closeModal());
  }
};

const removeEmployeeSaga = function*({
  payload: { id, userStatusType, stoppedWorkingDate, fullName }
}) {
  const modal = yield select(getModal);

  yield call(removeEmployeeApi(id, stoppedWorkingDate));
  yield put(removeEmployeeSuccess());
  const currentDate = new Date();
  const deletionDate = new Date(stoppedWorkingDate);
  const message =
    currentDate >= deletionDate
      ? `Succesfully archived employee ${fullName}`
      : `Employee ${fullName} will be archived on ${formatDate(
          stoppedWorkingDate
        )}`;
  yield put(toastrSuccess(message));

  if (modal.open) {
    yield put(closeModal());
  }

  yield put(closeDrawer());
  yield put(
    adminSearchEmployees({
      page: 0,
      size: 30,
      userStatusType: [userStatusType]
    })
  );
};

const cancelEmployeeDeleteSaga = function*({ payload: { id, fullName } }) {
  const modal = yield select(getModal);

  yield call(cancelEmplyeeDeleteApi(id));
  yield put(cancelEmployeeDeleteSuccess());
  const message = `Scheduled deletion of user ${fullName} is canceled`;
  yield put(toastrSuccess(message));

  if (modal.open) {
    yield put(closeModal());
  }

  yield put(closeDrawer());
  yield put(
    adminSearchEmployees({
      page: 0,
      size: 30,
      userStatusType: 'ACTIVE'
    })
  );
};

const restoreEmployeeSaga = function*({ payload }) {
  const modal = yield select(getModal);

  yield call(restoreEmployeeApi(payload));
  yield put(restoreEmployeeSuccess());
  yield put(toastrSuccess('Succesfully restored employee'));

  if (modal.open) {
    yield put(closeModal());
  }

  yield put(closeDrawer());
  yield put(
    adminSearchEmployees({
      page: 0,
      size: 30,
      userStatusType: [payload.userStatusType]
    })
  );
};

const getEmployeesSaga = function*({ payload }) {
  const employeesInformation = yield call(getEmployeesApi(payload));

  yield put(receiveEmployees(employeesInformation));
};

const searchEmployeesSaga = function*({ payload }) {
  if (!payload) {
    yield put(clearEmployees());
  }

  try {
    const employeesInformation = yield call(searchEmployeesApi(payload));

    yield put(searchEmployeesSuccess(employeesInformation));
  } catch (error) {
    yield put(errorSearchingEmployees(error));
  }
};

const adminSearchEmployeesSaga = function*({ payload }) {
  if (!payload) {
    yield put(clearEmployees());
  }

  if (!payload.searchQuery) {
    payload = _omit(payload, ['searchQuery']);
  }

  try {
    const employeesInformation = yield call(searchEmployeesApi(payload));

    yield put(adminSearchEmployeesSuccess(employeesInformation));
  } catch (error) {
    yield put(errorSearchingEmployees(error));
  }
};

export const employeeSaga = [
  sagaWithErrorHandling(
    requestEmployee,
    getEmployeeSaga,
    errorReceivingEmployee
  ),
  sagaWithSubmitWrapper(
    updateEmployee,
    updateEmployeeSaga,
    updateEmployeeError
  ),
  sagaWithSubmitWrapper(
    createEmployee,
    createEmployeeSaga,
    errorCreatingEmployee
  ),
  sagaWithErrorHandling(
    removeEmployee,
    removeEmployeeSaga,
    errorRemovingEmployee
  ),
  sagaWithErrorHandling(
    cancelEmployeeDelete,
    cancelEmployeeDeleteSaga,
    errorCancelEmployeeDelete
  ),
  sagaWithSubmitWrapper(
    restoreEmployee,
    restoreEmployeeSaga,
    errorRestoringEmployee
  ),
  sagaWithErrorHandling(
    requestEmployees,
    getEmployeesSaga,
    errorReceivingEmployees
  ),
  sagaWithErrorHandling(
    adminSearchEmployees,
    adminSearchEmployeesSaga,
    errorReceivingEmployees
  ),
  takeLatest(searchEmployees, searchEmployeesSaga)
];
