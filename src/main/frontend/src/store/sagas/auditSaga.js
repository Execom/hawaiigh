import { call, put } from 'redux-saga/effects';
import { sagaWithErrorHandling } from './helpers/sagaHelpers';
import {
  requestEmployeeAudit,
  requestAllowanceAudit,
  requestTeamAudit,
  receiveAudit,
  receiveAuditError
} from '../actions/auditActions';
import {
  getEmployeeAuditApi,
  getAllowanceAuditApi,
  getTeamAuditApi
} from '../services/auditService';

const getEmployeeAuditSaga = function*({ payload }) {
  const auditInformation = yield call(getEmployeeAuditApi(payload));

  yield put(receiveAudit({ type: 'employee', audit: auditInformation }));
};

const getAllowanceAuditSaga = function*({ payload }) {
  const auditInformation = yield call(getAllowanceAuditApi(payload));

  yield put(receiveAudit({ type: 'allowance', audit: auditInformation }));
};

const getTeamAuditSaga = function*({ payload }) {
  const auditInformation = yield call(getTeamAuditApi(payload));

  yield put(receiveAudit({ type: 'team', audit: auditInformation }));
};

export const auditSaga = [
  sagaWithErrorHandling(
    requestEmployeeAudit,
    getEmployeeAuditSaga,
    receiveAuditError
  ),
  sagaWithErrorHandling(
    requestAllowanceAudit,
    getAllowanceAuditSaga,
    receiveAuditError
  ),
  sagaWithErrorHandling(requestTeamAudit, getTeamAuditSaga, receiveAuditError)
];
