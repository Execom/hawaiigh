import { takeLatest } from 'redux-saga/effects';
import {
  genericErrorHandler,
  withErrorHandling
} from '../HOC/withErrorHandling';
import { withSubmittingHandling } from '../HOC/withIsSubmittingHandling';
import { withLoader } from '../HOC/withLoader';
import FileSaver from 'file-saver';

export const sagaWithSubmitWrapper = (request, saga, errorHandler) =>
  takeLatest(
    request,
    withSubmittingHandling(
      withErrorHandling(withLoader(saga), genericErrorHandler(errorHandler))
    )
  );

export const sagaWithErrorHandling = (request, saga, errorHandler) =>
  takeLatest(
    request,
    withErrorHandling(withLoader(saga), genericErrorHandler(errorHandler))
  );

export const extractExportDataFromResponse = response => {
  const data = response.response;
  const filename = response
    .getResponseHeader('Content-Disposition')
    .split('filename=')[1]
    .replace(/"/g, '');
  return { data, filename };
};

export const downloadFile = (data, filename, exportFormat) => {
  const formattedData = new Blob(
    [data],
    exportFormat === 'csv'
      ? { type: 'text/csv;charset=utf-8;' }
      : { type: 'application/pdf;charset=utf-8;' }
  );
  FileSaver.saveAs(formattedData, filename);
};
