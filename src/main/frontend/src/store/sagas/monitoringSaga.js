import { call, put } from 'redux-saga/effects';
import { sagaWithErrorHandling } from './helpers/sagaHelpers';
import {
  requestHardwareMonitoringData,
  recieveHardwareMonitoringData,
  errorRecieveHardwareMonitoringData,
  requestHttpEndpointsMonitoringData,
  recieveHttpEndpointsMonitoringData,
  errorRecieveHttpEndpointsMonitoringData,
  requestOldestTimestamp,
  recieveOldestTimestamp,
  errorRecieveOldestTimestampData
} from '@/store/actions/monitoringActions';
import {
  getHardwareMetricsApi,
  getHttpEndpointMetricsApi,
  getOldestTimestampApi
} from '@/store/services/monitoringService';

const getHardwareMonitoringDataSaga = function*(payload) {
  const hardwareMonitoringData = yield call(
    getHardwareMetricsApi(payload.payload)
  );
  yield put(recieveHardwareMonitoringData(hardwareMonitoringData));
};

const getOldestTimestampSaga = function*() {
  const oldestTimestamp = yield call(getOldestTimestampApi());
  yield put(recieveOldestTimestamp(oldestTimestamp));
};

const getHttpEndpointsMonitoringDataSaga = function*() {
  const httpEndpointsMonitoringData = yield call(getHttpEndpointMetricsApi());
  yield put(recieveHttpEndpointsMonitoringData(httpEndpointsMonitoringData));
};

export const monitoringSaga = [
  sagaWithErrorHandling(
    requestHardwareMonitoringData,
    getHardwareMonitoringDataSaga,
    errorRecieveHardwareMonitoringData
  ),
  sagaWithErrorHandling(
    requestOldestTimestamp,
    getOldestTimestampSaga,
    errorRecieveOldestTimestampData
  ),
  sagaWithErrorHandling(
    requestHttpEndpointsMonitoringData,
    getHttpEndpointsMonitoringDataSaga,
    errorRecieveHttpEndpointsMonitoringData
  )
];
