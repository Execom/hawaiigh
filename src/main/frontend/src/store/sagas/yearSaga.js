import { call, put } from 'redux-saga/effects';
import { closeDrawer } from '../actions/drawerActions';
import { closeModal } from '../actions/modalActions';
import {
  createYear,
  createYearError,
  createYearSuccessful,
  errorReceivingYear,
  errorReceivingYears,
  errorRemovingYear,
  receiveYear,
  receiveYears,
  removeYear,
  removeYearSuccess,
  requestYear,
  requestYears,
  updateYear,
  updateYearError,
  updateYearSuccessful
} from '../actions/yearActions';
import {
  createYearApi,
  getYearApi,
  getYearsApi,
  removeYearApi,
  updateYearApi
} from '../services/yearService';
import {
  sagaWithErrorHandling,
  sagaWithSubmitWrapper
} from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';
import { setShouldTableScrollToTop } from '../actions/administrationActions';

const requestYearsDefaultQueryParamsObj = {
  page: 0,
  size: 30,
  sort: 'year,desc'
};

const getYearSaga = function*({ payload }) {
  const yearInformation = yield call(getYearApi(payload));

  yield put(receiveYear(yearInformation));
};

const removeYearSaga = function*({ payload: { id } }) {
  yield call(removeYearApi(id));
  yield put(removeYearSuccess());
  yield put(toastrSuccess('Succesfully removed year'));
  yield put(requestYears(requestYearsDefaultQueryParamsObj));
};

const updateYearSaga = function*({ payload }) {
  yield call(updateYearApi(payload));
  yield put(updateYearSuccessful());
  yield put(toastrSuccess('Succesfully updated year'));
  yield put(closeDrawer());
  yield put(requestYears(requestYearsDefaultQueryParamsObj));
};

const createYearSaga = function*({ payload }) {
  yield call(createYearApi(payload));
  yield put(createYearSuccessful());
  yield put(toastrSuccess('Successfully created year'));
  yield put(closeModal('confirmationModal'));
  yield put(setShouldTableScrollToTop(true));
  yield put(closeDrawer());
  yield put(requestYears(requestYearsDefaultQueryParamsObj));
};

const getAllYearsSaga = function*({ payload }) {
  const yearsInformation = yield call(
    getYearsApi({ ...requestYearsDefaultQueryParamsObj, ...payload })
  );

  yield put(receiveYears(yearsInformation));
};

export const yearSaga = [
  sagaWithErrorHandling(requestYear, getYearSaga, errorReceivingYear),
  sagaWithSubmitWrapper(updateYear, updateYearSaga, updateYearError),
  sagaWithErrorHandling(removeYear, removeYearSaga, errorRemovingYear),
  sagaWithSubmitWrapper(createYear, createYearSaga, createYearError),
  sagaWithErrorHandling(requestYears, getAllYearsSaga, errorReceivingYears)
];
