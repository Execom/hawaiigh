import { push, replace } from 'connected-react-router';
import _replace from 'lodash/replace';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { receiveAudit } from '../actions/auditActions';
import {
  closeModal,
  handleRequestModal,
  openConfirmationModal,
  openHandleRequestModal,
  openInfoModal,
  openRequestModal,
  openTeamRequestsModal
} from '../actions/modalActions';
import { getRouter } from '../selectors';
import { getApprovalFlowApi } from '../services/auditService';
import { getRequestApi } from '../services/requestsService';

const handleRequestModalSaga = function*({
  payload: { id, previewOnly, isApprover, request }
}) {
  if (id) {
    const requestInfo = yield call(getRequestApi(id));
    const requestApprovalFlow = yield call(getApprovalFlowApi(id));

    yield put(receiveAudit({ type: 'approval', audit: requestApprovalFlow }));
    yield put(
      openHandleRequestModal({
        requestInfo,
        previewOnly
      })
    );
  } else {
    const requestApprovalFlow = yield call(getApprovalFlowApi(request.id));

    yield put(receiveAudit({ type: 'approval', audit: requestApprovalFlow }));
    yield put(
      openHandleRequestModal({
        requestInfo: request,
        isApprover,
        previewOnly
      })
    );
  }
};

const openModalSaga = function*() {
  const router = yield select(getRouter);

  yield put(push(`${router.location.search}?m`));
};

const closeModalSaga = function*() {
  const router = yield select(getRouter);

  if (router.location.search) {
    yield put(
      replace(
        `${router.location.pathname}${_replace(
          router.location.search,
          '?m',
          ''
        )}`
      )
    );
  }
};

export const modalSaga = [
  takeLatest(handleRequestModal, handleRequestModalSaga),
  takeLatest(closeModal, closeModalSaga),
  takeLatest(
    [
      openRequestModal,
      openHandleRequestModal,
      openInfoModal,
      openConfirmationModal,
      openTeamRequestsModal
    ],
    openModalSaga
  )
];
