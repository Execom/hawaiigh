import { call, put } from 'redux-saga/effects';
import {
  errorReceivingAllowance,
  errorReceivingAllowanceForUser,
  errorReceivingYearsWithAllowance,
  errorUpdatingAllowance,
  receiveAllowance,
  receiveAllowanceForUser,
  receiveYearsWithAllowance,
  requestAllowance,
  requestAllowanceForUser,
  requestYearsWithAllowance,
  updateAllowance,
  updateAllowanceSuccess,
  requestAllowancePerUserForYear,
  recieveAllowancePerUserForYear
} from '../actions/allowanceActions';
import { closeDrawer } from '../actions/drawerActions';
import {
  getAllowanceApi,
  getAllowanceForUserApi,
  getYearsWithAllowanceApi,
  updateAllowanceApi,
  getAllowancePerUserForYearApi
} from '../services/allowanceService';
import { sagaWithErrorHandling } from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';
import { requestAllowanceAudit } from '../actions/auditActions';

export const getAllowancesSaga = function*({ payload }) {
  const allowance = yield call(getAllowanceApi(payload));

  yield put(receiveAllowance(allowance));
};

export const getAllowancesForUserSaga = function*({ payload }) {
  yield put(receiveAllowanceForUser(null));
  const allowance = yield call(getAllowanceForUserApi(payload));

  yield put(receiveAllowanceForUser(allowance));
};

export const getYearsWithAllowanceSaga = function*({ payload }) {
  const years = yield call(getYearsWithAllowanceApi(payload));

  yield put(receiveYearsWithAllowance(years));
};

export const updateAllowanceSaga = function*({ payload }) {
  const { id, userId, year } = payload;

  yield call(updateAllowanceApi(payload));
  yield put(updateAllowanceSuccess());
  yield put(toastrSuccess('Successfully adjusted allowance.'));
  yield getAllowancesForUserSaga({ payload: { userId, year } });
  yield put(requestAllowanceAudit(id));
  yield put(closeDrawer());
};

const getAllowancePerUserForYearSaga = function*({
  payload: { page, size, searchQuery, year, teamId }
}) {
  let allowancesPerUserForYear;

  if (!searchQuery) {
    allowancesPerUserForYear = yield call(
      getAllowancePerUserForYearApi(year, { page, size, teamId })
    );
  } else {
    allowancesPerUserForYear = yield call(
      getAllowancePerUserForYearApi(year, { page, size, searchQuery, teamId })
    );
  }

  yield put(recieveAllowancePerUserForYear(allowancesPerUserForYear));
};

export const allowanceSaga = [
  sagaWithErrorHandling(
    requestAllowance,
    getAllowancesSaga,
    errorReceivingAllowance
  ),
  sagaWithErrorHandling(
    requestAllowanceForUser,
    getAllowancesForUserSaga,
    errorReceivingAllowanceForUser
  ),
  sagaWithErrorHandling(
    requestYearsWithAllowance,
    getYearsWithAllowanceSaga,
    errorReceivingYearsWithAllowance
  ),
  sagaWithErrorHandling(
    updateAllowance,
    updateAllowanceSaga,
    errorUpdatingAllowance
  ),
  sagaWithErrorHandling(
    requestAllowancePerUserForYear,
    getAllowancePerUserForYearSaga,
    errorUpdatingAllowance
  )
];
