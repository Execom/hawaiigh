import { call, put } from 'redux-saga/effects';
import {
  errorReceivingUser,
  errorReceivingUserDays,
  receiveUser,
  receiveUserDays,
  requestUser,
  requestUserDays
} from '../actions/userActions';
import { getUserApi, getUserDaysApi } from '../services/userService';
import { sagaWithErrorHandling } from './helpers/sagaHelpers';

const getUserSaga = function*({ payload }) {
  const userInformation = yield call(getUserApi, payload);

  yield put(receiveUser(userInformation));
};

const getUserDaysSaga = function*({ payload }) {
  const userDays = yield call(getUserDaysApi(payload));

  yield put(receiveUserDays(userDays));
};

export const userSaga = [
  sagaWithErrorHandling(requestUser, getUserSaga, errorReceivingUser),
  sagaWithErrorHandling(
    requestUserDays,
    getUserDaysSaga,
    errorReceivingUserDays
  )
];
