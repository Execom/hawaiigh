import { call, put } from 'redux-saga/effects';
import { sagaWithErrorHandling } from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';
import {
  requestAdmins,
  receiveAdmins,
  errorReceivingAdmins,
  removeAdmin,
  removeAdminSuccess,
  errorRemovingAdmin,
  addAdmin,
  addAdminSuccess,
  errorAddingAdmin,
  superAdminSearchUsers,
  superAdminSearchUsersSuccess,
  errorSearchingUsers,
  clearUsers
} from '@/store/actions/adminActions';
import {
  getAdminsApi,
  updateAdminApi,
  searchUsersApi
} from '@/store/services/adminService';

const getAdminsSaga = function*() {
  const hardwareMonitoringData = yield call(getAdminsApi());
  yield put(receiveAdmins(hardwareMonitoringData));
};

const removeAdminSaga = function*({ payload }) {
  yield call(updateAdminApi(payload));
  yield put(removeAdminSuccess());
  yield put(toastrSuccess(`Admin ${payload.fullName} removed successfully.`));
  yield put(requestAdmins());
};

const addAdminSaga = function*({ payload }) {
  yield call(updateAdminApi(payload));
  yield put(addAdminSuccess());
  yield put(toastrSuccess(`Admin ${payload.fullName} added successfully.`));
  yield put(requestAdmins());
};

const superAdminSearchUsersSaga = function*({ payload }) {
  if (!payload) {
    yield put(clearUsers());
  }

  try {
    const usersInformation = yield call(searchUsersApi(payload));
    yield put(superAdminSearchUsersSuccess(usersInformation));
  } catch (error) {
    yield put(errorSearchingUsers(error));
  }
};

export const adminSaga = [
  sagaWithErrorHandling(requestAdmins, getAdminsSaga, errorReceivingAdmins),
  sagaWithErrorHandling(removeAdmin, removeAdminSaga, errorRemovingAdmin),
  sagaWithErrorHandling(addAdmin, addAdminSaga, errorAddingAdmin),
  sagaWithErrorHandling(
    superAdminSearchUsers,
    superAdminSearchUsersSaga,
    errorSearchingUsers
  )
];
