import { call, put } from 'redux-saga/effects';
import {
  createLeaveType,
  createLeaveTypeError,
  createLeaveTypeSuccessful,
  errorReceivingLeaveType,
  errorReceivingLeaveTypeIcons,
  errorReceivingLeaveTypes,
  errorRemovingLeaveType,
  receiveLeaveType,
  receiveLeaveTypeIcons,
  receiveLeaveTypes,
  removeLeaveType,
  removeLeaveTypeSuccess,
  requestLeaveType,
  requestLeaveTypeIcons,
  requestLeaveTypes,
  updateLeaveType,
  updateLeaveTypeError,
  updateLeaveTypeSuccessful
} from '../actions/absenceActions';
import { closeDrawer } from '../actions/drawerActions';
import {
  createAbsenceApi,
  getAbsenceApi,
  getAbsencesApi,
  getLeaveTypesIconsApi,
  removeAbsenceApi,
  updateAbsenceApi
} from '../services/absenceService';
import {
  sagaWithErrorHandling,
  sagaWithSubmitWrapper
} from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';
import { setShouldTableScrollToTop } from '../actions/administrationActions';

const getLeaveTypeSaga = function*(action) {
  const leaveTypeInformation = yield call(getAbsenceApi(action.payload));

  yield put(receiveLeaveType(leaveTypeInformation));
};

const removeLeaveTypeSaga = function*({ payload: { id } }) {
  yield call(removeAbsenceApi(id));
  yield put(removeLeaveTypeSuccess());
  yield put(toastrSuccess('Succesfully removed leave type'));
  yield getAllLeaveTypesSaga({
    payload: {
      page: 0,
      size: 30
    }
  });
};

const updateLeaveTypeSaga = function*({ payload }) {
  yield call(updateAbsenceApi(payload));
  yield put(updateLeaveTypeSuccessful());
  yield put(toastrSuccess('Succesfully updated leave type'));
  yield put(closeDrawer());
  yield getAllLeaveTypesSaga({
    payload: {
      page: 0,
      size: 30
    }
  });
};

const createLeaveTypeSaga = function*({ payload }) {
  yield call(createAbsenceApi(payload));
  yield put(createLeaveTypeSuccessful());
  yield put(toastrSuccess('Successfully created leave type'));
  yield put(setShouldTableScrollToTop(true));
  yield put(closeDrawer());
  yield getAllLeaveTypesSaga({
    payload: {
      page: 0,
      size: 30
    }
  });
};

const getAllLeaveTypesSaga = function*({ payload }) {
  const leaveTypes = yield call(getAbsencesApi(payload));

  yield put(receiveLeaveTypes(leaveTypes));
};

const getAllLeaveTypeIconsSaga = function*() {
  const leaveTypeIcons = yield call(getLeaveTypesIconsApi);

  yield put(receiveLeaveTypeIcons(leaveTypeIcons));
};

export const absenceSaga = [
  sagaWithErrorHandling(
    requestLeaveType,
    getLeaveTypeSaga,
    errorReceivingLeaveType
  ),
  sagaWithSubmitWrapper(
    updateLeaveType,
    updateLeaveTypeSaga,
    updateLeaveTypeError
  ),
  sagaWithErrorHandling(
    removeLeaveType,
    removeLeaveTypeSaga,
    errorRemovingLeaveType
  ),
  sagaWithSubmitWrapper(
    createLeaveType,
    createLeaveTypeSaga,
    createLeaveTypeError
  ),
  sagaWithErrorHandling(
    requestLeaveTypes,
    getAllLeaveTypesSaga,
    errorReceivingLeaveTypes
  ),
  sagaWithErrorHandling(
    requestLeaveTypeIcons,
    getAllLeaveTypeIconsSaga,
    errorReceivingLeaveTypeIcons
  )
];
