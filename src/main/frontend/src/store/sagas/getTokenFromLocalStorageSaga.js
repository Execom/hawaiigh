import { replace } from 'connected-react-router';
import _startsWith from 'lodash/startsWith';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import { receiveToken } from '../actions/getTokenActions';
import {
  receiveTokenFromStorageError,
  REQUEST_TOKEN_FROM_STORAGE
} from '../actions/getTokenFromSessionStorageActions';
import { addRedirectAfterLoginRoute } from '../actions/redirectAfterLoginActions';
import { getRouter } from '../selectors';
import { getTokenFromLocalStorage } from '../services/helpers/getTokenFromLocalStorageService';
import { requestUser } from '../actions/userActions';

export const authenticate = function*() {
  try {
    const authentication = yield call(getTokenFromLocalStorage);
    yield put(receiveToken(authentication));
    yield* redirect(authentication);
  } catch (error) {
    yield put(receiveTokenFromStorageError(error));
  }
};

export const redirect = function*(authentication) {
  const router = yield select(getRouter);

  if (!authentication) {
    yield _startsWith(router.location.search, '?requestId') &&
      put(
        addRedirectAfterLoginRoute(
          `${router.location.pathname}${router.location.search}`
        )
      );
    yield put(
      replace('/login', {
        logout: true
      })
    );
  } else {
    yield put(requestUser());
  }
};

export const authenticateSaga = [
  takeEvery(REQUEST_TOKEN_FROM_STORAGE, authenticate)
];
