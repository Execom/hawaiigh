import { call, put, takeLatest } from 'redux-saga/effects';
import { closeDrawer } from '../actions/drawerActions';
import {
  clearTeams,
  createTeam,
  createTeamError,
  createTeamSuccessful,
  errorReceivingTeam,
  errorReceivingTeams,
  errorReceivingTeamsName,
  errorReceivingUserDaysForTeam,
  errorRemovingTeam,
  errorSearchingTeams,
  receiveTeam,
  receiveTeams,
  receiveUserDaysForTeam,
  removeTeam,
  removeTeamSuccess,
  requestTeam,
  requestTeams,
  requestTeamsName,
  requestUserDaysForTeam,
  searchTeams,
  searchTeamsSuccess,
  updateTeam,
  updateTeamError,
  updateTeamSuccessful,
  receiveTeamsName
} from '../actions/teamActions';
import {
  createTeamApi,
  getTeamApi,
  getTeamsApi,
  getTeamsNameApi,
  removeTeamApi,
  searchTeamsApi,
  updateTeamApi
} from '../services/teamService';
import { getUserDaysForTeamApi } from '../services/userService';
import {
  sagaWithErrorHandling,
  sagaWithSubmitWrapper
} from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';
import { setShouldTableScrollToTop } from '../actions/administrationActions';

const getTeamSaga = function*({ payload }) {
  const teamInformation = yield call(getTeamApi(payload));

  yield put(receiveTeam(teamInformation));
};

const removeTeamSaga = function*({ payload: { id } }) {
  yield call(removeTeamApi(id));
  yield put(removeTeamSuccess());
  yield put(toastrSuccess('Succesfully removed team'));
  yield put(
    requestTeams({
      page: 0,
      size: 30
    })
  );
};

const updateTeamSaga = function*({ payload }) {
  yield call(updateTeamApi(payload));
  yield put(updateTeamSuccessful());
  yield put(toastrSuccess('Succesfully updated team'));
  yield put(closeDrawer());
  yield put(
    requestTeams({
      page: 0,
      size: 30
    })
  );
};

const createTeamSaga = function*({ payload }) {
  yield call(createTeamApi(payload));
  yield put(createTeamSuccessful());
  yield put(toastrSuccess('Successfully created team'));
  yield put(setShouldTableScrollToTop(true));
  yield put(closeDrawer());
  yield put(
    requestTeams({
      page: 0,
      size: 30
    })
  );
};

const getUserDaysForTeamSaga = function*({ payload }) {
  const userDays = yield call(getUserDaysForTeamApi(payload));

  yield put(receiveUserDaysForTeam(userDays));
};

const getAllTeamsSaga = function*({ payload }) {
  const teamsInformation = yield call(getTeamsApi(payload));

  yield put(receiveTeams(teamsInformation));
};

const getTeamsNameSaga = function*() {
  const teamsInformation = yield call(getTeamsNameApi());

  yield put(receiveTeamsName(teamsInformation));
};

const searchTeamsSaga = function*({ payload }) {
  if (!payload) {
    return yield put(clearTeams());
  }

  try {
    const teamsInformation = yield call(searchTeamsApi(payload));

    yield put(searchTeamsSuccess(teamsInformation));
  } catch (error) {
    yield put(errorSearchingTeams(error));
  }
};

export const teamSaga = [
  sagaWithErrorHandling(requestTeam, getTeamSaga, errorReceivingTeam),
  sagaWithSubmitWrapper(updateTeam, updateTeamSaga, updateTeamError),
  sagaWithErrorHandling(removeTeam, removeTeamSaga, errorRemovingTeam),
  sagaWithSubmitWrapper(createTeam, createTeamSaga, createTeamError),
  sagaWithErrorHandling(
    requestUserDaysForTeam,
    getUserDaysForTeamSaga,
    errorReceivingUserDaysForTeam
  ),
  sagaWithErrorHandling(requestTeams, getAllTeamsSaga, errorReceivingTeams),
  sagaWithErrorHandling(
    requestTeamsName,
    getTeamsNameSaga,
    errorReceivingTeamsName
  ),
  takeLatest(searchTeams, searchTeamsSaga)
];
