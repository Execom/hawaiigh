import { call, put, select } from 'redux-saga/effects';
import { closeDrawer } from '../actions/drawerActions';
import {
  createPublicHoliday,
  createPublicHolidayError,
  createPublicHolidaySuccessful,
  errorReceivingPublicHoliday,
  errorReceivingPublicHolidays,
  errorReceivingPublicHolidaysByYear,
  errorReceivingPublicHolidayYears,
  errorRemovingPublicHoliday,
  expandAccordion,
  receivePublicHoliday,
  receivePublicHolidays,
  receivePublicHolidaysByYear,
  receivePublicHolidayYears,
  removePublicHoliday,
  removePublicHolidaySuccess,
  requestPublicHoliday,
  requestPublicHolidays,
  requestPublicHolidaysByYear,
  requestPublicHolidayYears,
  updatePublicHoliday,
  updatePublicHolidayError,
  updatePublicHolidaySuccessful
} from '../actions/publicHolidayActions';
import {
  createPublicHolidayApi,
  getPublicHolidayApi,
  getPublicHolidaysApi,
  getPublicHolidaysByYearApi,
  getPublicHolidayYearsApi,
  removePublicHolidayApi,
  updatePublicHolidayApi
} from '../services/publicHolidayService';
import {
  sagaWithErrorHandling,
  sagaWithSubmitWrapper
} from './helpers/sagaHelpers';
import { toastrSuccess } from './helpers/toastrHelperSaga';
import moment from 'moment';
import { setShouldTableScrollToTop } from '../actions/administrationActions';

const getPublicHolidaySaga = function*({ payload }) {
  const publicHolidayInformation = yield call(getPublicHolidayApi(payload));

  yield put(receivePublicHoliday(publicHolidayInformation));
};

const removePublicHolidaySaga = function*({ payload: { id, year } }) {
  yield call(removePublicHolidayApi(id));
  yield put(removePublicHolidaySuccess());
  yield put(toastrSuccess('Succesfully removed public holiday'));
  yield put(requestPublicHolidays());

  yield getPublicHolidaysByYearSaga({ payload: { year } });
};

const updatePublicHolidaySaga = function*({ payload }) {
  const publicHoliday = yield call(updatePublicHolidayApi(payload));
  const year = moment(publicHoliday.date).year();

  yield put(updatePublicHolidaySuccessful());
  yield put(toastrSuccess('Succesfully updated public holiday'));
  yield put(closeDrawer());

  const publicHolidayObject = yield select(state => state.publicHolidayObject);

  if (publicHolidayObject.publicHolidayYears.years[year].length) {
    yield getPublicHolidaysByYearSaga({ payload: { year } });
  }
};

const createPublicHolidaySaga = function*({ payload }) {
  const publicHoliday = yield call(createPublicHolidayApi(payload));
  const year = moment(publicHoliday.date).year();

  yield put(createPublicHolidaySuccessful());
  yield put(toastrSuccess('Successfully created public holiday'));
  yield put(setShouldTableScrollToTop(true));
  yield put(closeDrawer());

  const publicHolidayObject = yield select(state => state.publicHolidayObject);

  if (
    !publicHolidayObject.publicHolidayYears.years[year] ||
    (publicHolidayObject.publicHolidayYears.years[year] &&
      publicHolidayObject.publicHolidayYears.years[year].length)
  ) {
    yield getPublicHolidaysByYearSaga({ payload: { year } });
  }
};

const getPublicHolidaysSaga = function*() {
  const publicHolidays = yield call(getPublicHolidaysApi);

  yield put(receivePublicHolidays(publicHolidays));
};

const getPublicHolidayYearsSaga = function*() {
  const publicHolidayYears = yield call(getPublicHolidayYearsApi);

  yield put(receivePublicHolidayYears(publicHolidayYears));
};

const getPublicHolidaysByYearSaga = function*({ payload }) {
  const publicHolidaysByYear = yield call(getPublicHolidaysByYearApi(payload));

  yield put(
    receivePublicHolidaysByYear({
      content: publicHolidaysByYear,
      yearSelected: payload.year
    })
  );
  yield put(expandAccordion({ year: payload.year, expanded: true }));
};

export const publicHolidaySaga = [
  sagaWithErrorHandling(
    requestPublicHoliday,
    getPublicHolidaySaga,
    errorReceivingPublicHoliday
  ),
  sagaWithSubmitWrapper(
    updatePublicHoliday,
    updatePublicHolidaySaga,
    updatePublicHolidayError
  ),
  sagaWithErrorHandling(
    removePublicHoliday,
    removePublicHolidaySaga,
    errorRemovingPublicHoliday
  ),
  sagaWithSubmitWrapper(
    createPublicHoliday,
    createPublicHolidaySaga,
    createPublicHolidayError
  ),
  sagaWithErrorHandling(
    requestPublicHolidays,
    getPublicHolidaysSaga,
    errorReceivingPublicHolidays
  ),
  sagaWithErrorHandling(
    requestPublicHolidayYears,
    getPublicHolidayYearsSaga,
    errorReceivingPublicHolidayYears
  ),
  sagaWithErrorHandling(
    requestPublicHolidaysByYear,
    getPublicHolidaysByYearSaga,
    errorReceivingPublicHolidaysByYear
  )
];
