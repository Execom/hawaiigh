export const getAuthorization = state => state.authorization;
export const getTeams = state => state.teamObject.teams;
export const getTeam = state => state.teamObject.team;
export const getUser = state => state.userObject.user;
export const getEmployees = state => state.userObject.employees;
export const getEmployee = state => state.userObject.employee;
export const getAudit = state => state.audit;
export const getSearchEmployees = state => state.userObject.employeesSearch;
export const getSearchTeams = state => state.teamObject.teamsSearch;
export const getSearchedEmployee = state => state.userObject.searchedEmployee;
export const getPublicHolidays = state =>
  state.publicHolidayObject.publicHolidays;
export const getPublicHolidayYears = state =>
  state.publicHolidayObject.publicHolidayYears;
export const getPublicHoliday = state =>
  state.publicHolidayObject.publicHoliday;
export const getUserDays = state => state.userObject.userDays;
export const getUserDaysForTeam = state => state.userObject.userDaysForTeam;
export const getRequest = state => state.request;
export const getModal = state => state.modal;
export const getLeaveTypes = state => state.absenceObject.leaveTypes;
export const getLeaveType = state => state.absenceObject.leaveType;
export const getLeaveTypesIcons = state => state.absenceObject.leaveTypesIcons;
export const getLeaveProfiles = state =>
  state.leaveProfilesObject.leaveProfiles;
export const getLeaveProfileResults = state =>
  state.leaveProfilesObject.leaveProfiles.results;
export const getLeaveProfile = state => state.leaveProfilesObject.leaveProfile;
export const getAllowance = state => state.allowance.allowances;
export const getYearsWithAllowance = state => state.allowance.years;
export const getActiveTab = state => state.activeTab;
export const getLeaveRequests = state => state.leaveRequests;
export const getTeamRequests = state => state.teamObject.teamRequests;
export const getLoading = state => state.loading;
export const getYear = state => state.yearObject.year;
export const getYears = state => state.yearObject.years;
export const getSelectedDate = state => state.selectedDate;
export const getRequestsForApproval = state => state.approvals;
export const getRouter = state => state.router;
export const getRequestHover = state => state.requestHover;
export const getIsSubmitting = state => state.isSubmitting;
export const getMenuDrawer = state => state.menuDrawer;
export const getActionDrawer = state => state.actionDrawer;
export const getWindowWidth = state => state.windowWidth;
export const getApprovalFlow = state => state.audit;
export const getRedirectAfterLoginRoute = state =>
  state.redirectAfterLoginRoute;
export const getAdministration = state => state.administration;
export const getAllowancePerUserForYear = state => state.allowance;
export const getHardwareMonitoringData = state => state.monitoring.hardware;
export const getOldestTimestamp = state => state.monitoring.oldestTimestamp;
export const getHttpEndpointsMonitoringData = state =>
  state.monitoring.httpEndpoints;
export const getAdmins = state => state.admin.admins;
export const getUsers = state => state.admin.users;
