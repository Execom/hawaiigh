import {
  delFactory,
  getFactory,
  postFactory,
  putFactory
} from './helpers/request';

const apiEndpoint = '/publicholidays';

export const createPublicHolidayApi = publicHolidayObject =>
  postFactory(apiEndpoint, publicHolidayObject);
export const getPublicHolidayApi = id => getFactory(`${apiEndpoint}/${id}`);
export const removePublicHolidayApi = id => delFactory(`${apiEndpoint}/${id}`);
export const updatePublicHolidayApi = publicHolidayObject =>
  putFactory(`${apiEndpoint}/${publicHolidayObject.id}`, publicHolidayObject);
export const getPublicHolidaysApi = getFactory(`${apiEndpoint}?deleted=false`);
export const getPublicHolidayYearsApi = getFactory(
  `${apiEndpoint}/administration`
);
export const getPublicHolidaysByYearApi = publicHolidayObject =>
  getFactory(`${apiEndpoint}/year`, publicHolidayObject);
