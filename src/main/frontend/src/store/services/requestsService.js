import { getFactory, postFactory, putFactory } from './helpers/request';

const apiEndpoint = '/requests';

export const requestApi = requestObject =>
  postFactory(apiEndpoint, requestObject);
export const handleRequestApi = requestObject =>
  putFactory(`${apiEndpoint}/${requestObject.id}`, requestObject);
export const getRequestApi = id => getFactory(`${apiEndpoint}/${id}`);
export const requestUserLeaveRequestsForYearApi = obj =>
  getFactory(`${apiEndpoint}/user/year`, obj);
export const getRequestsForApprovalApi = () =>
  getFactory(`${apiEndpoint}/approval`);
export const getRequestsForTeamApi = requestObject =>
  getFactory(`${apiEndpoint}/team/month`, requestObject);
