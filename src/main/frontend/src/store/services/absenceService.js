import {
  delFactory,
  getFactory,
  postFactory,
  putFactory
} from './helpers/request';

const apiEndpoint = '/leavetypes';

export const createAbsenceApi = leaveTypeObject =>
  postFactory(apiEndpoint, leaveTypeObject);
export const getAbsenceApi = id => getFactory(`${apiEndpoint}/${id}`);
export const removeAbsenceApi = id => delFactory(`${apiEndpoint}/${id}`);
export const updateAbsenceApi = leaveTypeObject =>
  putFactory(`${apiEndpoint}/${leaveTypeObject.id}`, leaveTypeObject);
export const getAbsencesApi = leaveTypeObject =>
  getFactory(apiEndpoint, leaveTypeObject);
export const getLeaveTypesIconsApi = getFactory(`${apiEndpoint}/icons`);
