import { getFactory, putFactory } from './helpers/request';

const apiEndpoint = '/admins';

export const getAdminsApi = () => getFactory(`${apiEndpoint}`);
export const searchUsersApi = query =>
  getFactory(`${apiEndpoint}/users`, { ...query });
export const updateAdminApi = admin =>
  putFactory(`${apiEndpoint}/${admin.id}`, admin);
