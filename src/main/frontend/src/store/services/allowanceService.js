import { getFactory, putFactory } from './helpers/request';

const apiEndpoint = '/allowances';

export const getAllowanceApi = year =>
  getFactory(`${apiEndpoint}/me`, { year });
export const getAllowanceForUserApi = obj =>
  getFactory(`${apiEndpoint}/user/year`, obj);
export const getAllowancePerUserForYearApi = (year, query) =>
  getFactory(`${apiEndpoint}/users/${year}`, { ...query });
export const getYearsWithAllowanceApi = obj =>
  getFactory(`${apiEndpoint}/years`, obj);
export const updateAllowanceApi = obj =>
  putFactory(`${apiEndpoint}/${obj.id}`, obj);
