import request from 'superagent';
import { getLink } from '../../getLink';
import { getTokenFromLocalStorage } from './getTokenFromLocalStorageService';

const get = (url, data) => {
  return request
    .get(getLink(url))
    .query(data)
    .set('Authorization', `Bearer ${getTokenFromLocalStorage()}`)
    .then(res => res.body);
};

const post = (url, data) => {
  return request
    .post(getLink(url))
    .send(data)
    .set('Authorization', `Bearer ${getTokenFromLocalStorage()}`)
    .then(res => res.body);
};

const put = (url, data) => {
  return request
    .put(getLink(url))
    .send(data)
    .set('Authorization', `Bearer ${getTokenFromLocalStorage()}`)
    .then(res => res.body);
};

const del = url => {
  return request
    .delete(getLink(url))
    .set('Authorization', `Bearer ${getTokenFromLocalStorage()}`)
    .then(res => res.body);
};

const search = (url, query) => {
  return request
    .get(getLink(url))
    .query(query)
    .set('Authorization', `Bearer ${getTokenFromLocalStorage()}`)
    .then(res => res.body);
};

const getExport = (url, data) => {
  return request
    .get(getLink(url))
    .query(data)
    .set('Authorization', `Bearer ${getTokenFromLocalStorage()}`)
    .responseType('arraybuffer')
    .then(res => res.xhr);
};

export const getFactory = (url, data) => async () => await get(url, data);
export const delFactory = url => async () => await del(url);
export const postFactory = (url, data) => async () => await post(url, data);
export const putFactory = (url, data) => async () => await put(url, data);
export const searchFactory = (url, query) => async () =>
  await search(url, query);
export const getExportFactory = (url, data) => async () =>
  await getExport(url, data);
