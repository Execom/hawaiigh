export const getTokenFromLocalStorage = () => {
  const token = localStorage.getItem('token');

  return token;
};
