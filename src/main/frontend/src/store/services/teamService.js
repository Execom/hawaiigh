import {
  delFactory,
  getFactory,
  postFactory,
  putFactory,
  searchFactory
} from './helpers/request';

const apiEndpoint = '/teams';

export const createTeamApi = teamObject => postFactory(apiEndpoint, teamObject);
export const getTeamApi = id => getFactory(`${apiEndpoint}/${id}`);
export const removeTeamApi = id => delFactory(`${apiEndpoint}/${id}`);
export const updateTeamApi = teamObject =>
  putFactory(`${apiEndpoint}/${teamObject.id}`, teamObject);
export const getTeamsApi = teamObject => getFactory(apiEndpoint, teamObject);
export const getTeamsNameApi = () => getFactory(`${apiEndpoint}/name`);
export const searchTeamsApi = ({ searchQuery, page }) =>
  searchFactory(`${apiEndpoint}/search`, {
    searchQuery,
    page,
    size: 5
  });
