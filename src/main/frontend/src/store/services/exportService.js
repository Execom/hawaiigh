import { getExportFactory } from './helpers/request';

const apiEndpoint = '/export';

export const getExportRequestsApi = dataObject =>
  getExportFactory(`${apiEndpoint}/requests`, dataObject);

export const getExportUsersApi = dataObject =>
  getExportFactory(`${apiEndpoint}/users`, dataObject);

export const getExportAllowancesApi = dataObject =>
  getExportFactory(`${apiEndpoint}/allowances`, dataObject);

export const getExportLeaveProfilesApi = dataObject =>
  getExportFactory(`${apiEndpoint}/leaveprofiles`, dataObject);
