import { getFactory } from './helpers/request';

const apiEndpoint = '/metrics';

export const getHardwareMetricsApi = payload =>
  getFactory(`${apiEndpoint}/hardware`, { ...payload });
export const getOldestTimestampApi = payload =>
  getFactory(`${apiEndpoint}/oldestTimestamp`);
export const getHttpEndpointMetricsApi = () =>
  getFactory(`${apiEndpoint}/http`);
