import { getFactory } from './helpers/request';

const apiEndpoint = '/audit';

export const getApprovalFlowApi = id =>
  getFactory(`${apiEndpoint}/approvalFlow/${id}`);

export const getEmployeeAuditApi = id =>
  getFactory(`${apiEndpoint}/user/${id}`);

export const getAllowanceAuditApi = id =>
  getFactory(`${apiEndpoint}/allowance/${id}`);

export const getTeamAuditApi = id => getFactory(`${apiEndpoint}/team/${id}`);
