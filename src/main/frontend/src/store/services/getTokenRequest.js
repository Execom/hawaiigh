import request from 'superagent';
import { getLink } from '../getLink';

export const tokenRequest = async _token => {
  const response = await request.get(getLink('/users/me')).set({
    Authorization: `Bearer ${_token}`,
    Accept: 'application/json'
  });

  const [token, role, user] = [
    response.headers['x-auth-token'],
    response.headers.role,
    response.body
  ];

  return {
    token,
    role,
    user
  };
};
