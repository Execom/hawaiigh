import {
  delFactory,
  getFactory,
  postFactory,
  putFactory,
  searchFactory
} from './helpers/request';

const apiEndpoint = '/users';

export const getUserApi = getFactory(`${apiEndpoint}/me`);
export const getUserDaysForTeamApi = queryObject =>
  getFactory(`${apiEndpoint}/teamCalendarList`, queryObject);
export const getEmployeeApi = id => getFactory(`${apiEndpoint}/${id}`);
export const removeEmployeeApi = (id, stoppedWorkingDate) =>
  delFactory(`${apiEndpoint}/${id}?stoppedWorkingDate=${stoppedWorkingDate}`);
export const cancelEmplyeeDeleteApi = id =>
  putFactory(`${apiEndpoint}/canceldelete/${id}`);
export const restoreEmployeeApi = employeeObject =>
  putFactory(`${apiEndpoint}/${employeeObject.id}/activate`, employeeObject);
export const createEmployeeApi = employeeObject =>
  postFactory(apiEndpoint, employeeObject);
export const updateEmployeeApi = employeeObject =>
  putFactory(`${apiEndpoint}/${employeeObject.id}`, employeeObject);
export const updateEmployeeImageApi = employeeObject =>
  putFactory(`${apiEndpoint}/${employeeObject.id}/loginUpdate`, employeeObject);
export const getEmployeesApi = dataObject =>
  getFactory(apiEndpoint, dataObject);
export const searchEmployeesApi = query =>
  searchFactory(`${apiEndpoint}/search`, {
    ...query
  });
export const getUserDaysApi = queryObject =>
  getFactory(`${apiEndpoint}/days`, queryObject);
export const getUserDaysForMonthApi = queryObject =>
  getFactory(`${apiEndpoint}/monthDays`, queryObject);
