import _replace from 'lodash/replace';
import moment from 'moment';

export const reducerTotal = (a, b) => a + b;
export const formatDate = date => moment(date).format('DD MMM YYYY');
export const formatMonth = date => moment(date, 'YYYY-MMM').format('MMM YYYY');
export const formatSelectedMonth = date =>
  moment(date, 'YYYY-MMM').format('YYYY-MM');
export const initializeExpirationDateField = (date, year) =>
  date
    ? formatExpirationDate(date)
    : formatExpirationDate(...getDefaultExpirationDate(year));
const getDefaultExpirationDate = year => [`${year + 1}-04-01`, 'YYYY-MM-DD'];
const formatExpirationDate = date => moment(date).toDate();
export const formatTime = date => moment(date).format('HH:mm');
export const formatDateAndTime = date =>
  moment(date).format('DD MMM YYYY HH:mm');
export const formatLocation = location =>
  _replace(_replace(location, '/', ''), '-', ' ');
