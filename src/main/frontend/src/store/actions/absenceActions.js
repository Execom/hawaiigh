import { createAction } from 'redux-actions';

export const REQUEST_LEAVE_TYPE = 'REQUEST_LEAVE_TYPE';
export const RECEIVE_LEAVE_TYPE = 'RECEIVE_LEAVE_TYPE';
export const RECEIVE_LEAVE_TYPE_ERROR = 'RECEIVE_LEAVE_TYPE_ERROR';
export const REMOVE_LEAVE_TYPE = 'REMOVE_LEAVE_TYPE';
export const REMOVE_LEAVE_TYPE_SUCCESS = 'REMOVE_LEAVE_TYPE_SUCCESS';
export const REMOVE_LEAVE_TYPE_ERROR = 'REMOVE_LEAVE_TYPE_ERROR';
export const UPDATE_LEAVE_TYPE = 'UPDATE_LEAVE_TYPE';
export const UPDATE_LEAVE_TYPE_SUCCESSFUL = 'UPDATE_LEAVE_TYPE_SUCCESSFUL';
export const UPDATE_LEAVE_TYPE_ERROR = 'UPDATE_LEAVE_TYPE_ERROR';
export const CREATE_LEAVE_TYPE = 'CREATE_LEAVE_TYPE';
export const CREATE_LEAVE_TYPE_SUCCESSFUL = 'CREATE_LEAVE_TYPE_SUCCESSFUL';
export const CREATE_LEAVE_TYPE_ERROR = 'CREATE_LEAVE_TYPE_ERROR';
export const REQUEST_LEAVE_TYPES = 'REQUEST_LEAVE_TYPES';
export const RECEIVE_LEAVE_TYPES = 'RECEIVE_LEAVE_TYPES';
export const RECEIVE_LEAVE_TYPES_ERROR = 'RECEIVE_LEAVE_TYPES_ERROR';
export const REQUEST_LEAVE_TYPE_ICONS = 'REQUEST_LEAVE_TYPE_ICONS';
export const RECEIVE_LEAVE_TYPE_ICONS = 'RECEIVE_LEAVE_TYPE_ICONS';
export const RECEIVE_LEAVE_TYPE_ICONS_ERROR = 'RECEIVE_LEAVE_TYPE_ICONS_ERROR';
export const CLEAR_LEAVE_TYPE = 'CLEAR_LEAVE_TYPE';

export const requestLeaveType = createAction(REQUEST_LEAVE_TYPE);
export const receiveLeaveType = createAction(RECEIVE_LEAVE_TYPE);
export const errorReceivingLeaveType = createAction(RECEIVE_LEAVE_TYPE_ERROR);
export const removeLeaveType = createAction(REMOVE_LEAVE_TYPE);
export const removeLeaveTypeSuccess = createAction(REMOVE_LEAVE_TYPE_SUCCESS);
export const errorRemovingLeaveType = createAction(REMOVE_LEAVE_TYPE_ERROR);
export const updateLeaveType = createAction(UPDATE_LEAVE_TYPE);
export const updateLeaveTypeSuccessful = createAction(
  UPDATE_LEAVE_TYPE_SUCCESSFUL
);
export const updateLeaveTypeError = createAction(UPDATE_LEAVE_TYPE_ERROR);
export const createLeaveType = createAction(CREATE_LEAVE_TYPE);
export const createLeaveTypeSuccessful = createAction(
  CREATE_LEAVE_TYPE_SUCCESSFUL
);
export const createLeaveTypeError = createAction(CREATE_LEAVE_TYPE_ERROR);
export const requestLeaveTypes = createAction(REQUEST_LEAVE_TYPES);
export const receiveLeaveTypes = createAction(RECEIVE_LEAVE_TYPES);
export const errorReceivingLeaveTypes = createAction(RECEIVE_LEAVE_TYPES_ERROR);
export const requestLeaveTypeIcons = createAction(REQUEST_LEAVE_TYPE_ICONS);
export const receiveLeaveTypeIcons = createAction(RECEIVE_LEAVE_TYPE_ICONS);
export const errorReceivingLeaveTypeIcons = createAction(
  RECEIVE_LEAVE_TYPE_ICONS_ERROR
);
export const clearLeaveType = createAction(CLEAR_LEAVE_TYPE);
