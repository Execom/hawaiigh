import { createAction } from 'redux-actions';

export const SET_SHOULD_TABLE_SCROLL_TO_TOP = 'SET_SHOULD_TABLE_SCROLL_TO_TOP';
export const SET_IS_FAB_VISIBLE = 'SET_IS_FAB_VISIBLE';

export const setShouldTableScrollToTop = createAction(
  SET_SHOULD_TABLE_SCROLL_TO_TOP
);
export const setIsFabVisible = createAction(SET_IS_FAB_VISIBLE);
