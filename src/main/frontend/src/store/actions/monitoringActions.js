import { createAction } from 'redux-actions';

export const REQUEST_HARDWARE_MONITORING_DATA =
  'REQUEST_HARDWARE_MONITORING_DATA';
export const RECIEVE_HARDWARE_MONITORING_DATA =
  'RECIEVE_HARDWARE_MONITORING_DATA';
export const RECIEVE_HARDWARE_MONITORING_DATA_ERROR =
  'RECIEVE_HARDWARE_MONITORING_DATA_ERROR';
export const REQUEST_HTTP_ENDPOINTS_MONITORING_DATA =
  'REQUEST_HTTP_ENDPOINTS_MONITORING_DATA';
export const RECIEVE_HTTP_ENDPOINTS_MONITORING_DATA =
  'RECIEVE_HTTP_ENDPOINTS_MONITORING_DATA';
export const RECIEVE_HTTP_ENDPOINTS_MONITORING_DATA_ERROR =
  'RECIEVE_HTTP_ENDPOINTS_MONITORING_DATA_ERROR';
export const REQUEST_OLDES_TIMESTAMP = 'REQUEST_OLDES_TIMESTAMP';
export const RECIEVE_OLDES_TIMESTAMP = 'RECIEVE_OLDES_TIMESTAMP';
export const RECIEVE_OLDEST_TIMESTAMP_ERROR = 'RECIEVE_OLDEST_TIMESTAMP_ERROR';

export const requestHardwareMonitoringData = createAction(
  REQUEST_HARDWARE_MONITORING_DATA
);
export const recieveHardwareMonitoringData = createAction(
  RECIEVE_HARDWARE_MONITORING_DATA
);
export const errorRecieveHardwareMonitoringData = createAction(
  RECIEVE_HARDWARE_MONITORING_DATA_ERROR
);
export const requestHttpEndpointsMonitoringData = createAction(
  REQUEST_HTTP_ENDPOINTS_MONITORING_DATA
);
export const recieveHttpEndpointsMonitoringData = createAction(
  RECIEVE_HTTP_ENDPOINTS_MONITORING_DATA
);
export const errorRecieveHttpEndpointsMonitoringData = createAction(
  RECIEVE_HTTP_ENDPOINTS_MONITORING_DATA_ERROR
);
export const requestOldestTimestamp = createAction(REQUEST_OLDES_TIMESTAMP);
export const recieveOldestTimestamp = createAction(RECIEVE_OLDES_TIMESTAMP);
export const errorRecieveOldestTimestampData = createAction(
  RECIEVE_OLDEST_TIMESTAMP_ERROR
);
