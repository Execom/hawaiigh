import { createAction } from 'redux-actions';

export const REQUEST_TEAM = 'REQUEST_TEAM';
export const RECEIVE_TEAM = 'RECEIVE_TEAM';
export const RECEIVE_TEAM_ERROR = 'RECEIVE_TEAM_ERROR';
export const REMOVE_TEAM = 'REMOVE_TEAM';
export const REMOVE_TEAM_SUCCESS = 'REMOVE_TEAM_SUCCESS';
export const REMOVE_TEAM_ERROR = 'REMOVE_TEAM_ERROR';
export const UPDATE_TEAM = 'UPDATE_TEAM';
export const UPDATE_TEAM_SUCCESSFUL = 'UPDATE_TEAM_SUCCESSFUL';
export const UPDATE_TEAM_ERROR = 'UPDATE_TEAM_ERROR';
export const CREATE_TEAM = 'CREATE_TEAM';
export const CREATE_TEAM_SUCCESSFUL = 'CREATE_TEAM_SUCCESSFUL';
export const CREATE_TEAM_ERROR = 'CREATE_TEAM_ERROR';
export const REQUEST_USER_DAYS_FOR_TEAM = 'REQUEST_USER_DAYS_FOR_TEAM';
export const RECEIVE_USER_DAYS_FOR_TEAM = 'RECEIVE_USER_DAYS_FOR_TEAM';
export const RECEIVE_USER_DAYS_FOR_TEAM_ERROR =
  'RECEIVE_USER_DAYS_FOR_TEAM_ERROR';
export const REMOVE_USER_DAYS_FOR_TEAM_ON_YEAR_CHANGE =
  'REMOVE_USER_DAYS_FOR_TEAM_ON_YEAR_CHANGE';
export const RESET_USER_DAYS_VALUES = 'RESET_USER_DAYS_VALUES';
export const UPDATE_USER_DAYS_FOR_TEAM = 'UPDATE_USER_DAYS_FOR_TEAM';
export const REQUEST_TEAMS = 'REQUEST_TEAMS';
export const RECEIVE_TEAMS = 'RECEIVE_TEAMS';
export const RECEIVE_TEAMS_ERROR = 'RECEIVE_TEAMS_ERROR';
export const REQUEST_TEAMS_NAME = 'REQUEST_TEAMS_NAME';
export const RECEIVE_TEAMS_NAME = 'RECEIVE_TEAMS_NAME';
export const RECEIVE_TEAMS_NAME_ERROR = 'RECEIVE_TEAMS_NAME_ERROR';
export const SEARCH_TEAMS = 'SEARCH_TEAMS';
export const SEARCH_TEAMS_SUCCESS = 'SEARCH_TEAMS_SUCCESS';
export const SEARCH_TEAMS_ERROR = 'SEARCH_TEAMS_ERROR';
export const CLEAR_TEAMS = 'CLEAR_TEAMS';
export const CLEAR_TEAM = 'CLEAR_TEAM';

export const requestTeam = createAction(REQUEST_TEAM);
export const receiveTeam = createAction(RECEIVE_TEAM);
export const errorReceivingTeam = createAction(RECEIVE_TEAM_ERROR);
export const removeTeam = createAction(REMOVE_TEAM);
export const removeTeamSuccess = createAction(REMOVE_TEAM_SUCCESS);
export const errorRemovingTeam = createAction(REMOVE_TEAM_ERROR);
export const updateTeam = createAction(UPDATE_TEAM);
export const updateTeamSuccessful = createAction(UPDATE_TEAM_SUCCESSFUL);
export const updateTeamError = createAction(UPDATE_TEAM_ERROR);
export const createTeam = createAction(CREATE_TEAM);
export const createTeamSuccessful = createAction(CREATE_TEAM_SUCCESSFUL);
export const createTeamError = createAction(CREATE_TEAM_ERROR);
export const requestUserDaysForTeam = createAction(REQUEST_USER_DAYS_FOR_TEAM);
export const receiveUserDaysForTeam = createAction(RECEIVE_USER_DAYS_FOR_TEAM);
export const errorReceivingUserDaysForTeam = createAction(
  RECEIVE_USER_DAYS_FOR_TEAM_ERROR
);
export const removeUserDaysForTeamOnYearChange = createAction(
  REMOVE_USER_DAYS_FOR_TEAM_ON_YEAR_CHANGE
);
export const resetUserDaysValues = createAction(RESET_USER_DAYS_VALUES);
export const updateUserDaysForTeam = createAction(UPDATE_USER_DAYS_FOR_TEAM);
export const requestTeams = createAction(REQUEST_TEAMS);
export const receiveTeams = createAction(RECEIVE_TEAMS);
export const errorReceivingTeams = createAction(RECEIVE_TEAMS_ERROR);
export const requestTeamsName = createAction(REQUEST_TEAMS_NAME);
export const receiveTeamsName = createAction(RECEIVE_TEAMS_NAME);
export const errorReceivingTeamsName = createAction(RECEIVE_TEAMS_NAME_ERROR);
export const searchTeams = createAction(SEARCH_TEAMS);
export const searchTeamsSuccess = createAction(SEARCH_TEAMS_SUCCESS);
export const errorSearchingTeams = createAction(SEARCH_TEAMS_ERROR);
export const clearTeams = createAction(CLEAR_TEAMS);
export const clearTeam = createAction(CLEAR_TEAM);
