import { createAction } from 'redux-actions';

export const REQUEST_DATA_EXPORT = 'REQUEST_DATA_EXPORT';
export const RECEIVE_DATA_EXPORT_SUCCESS = 'RECEIVE_DATA_EXPORT_SUCCESS';
export const RECEIVE_DATA_EXPORT_ERROR = 'RECEIVE_DATA_EXPORT_ERROR';

export const requestDataExport = createAction(REQUEST_DATA_EXPORT);
export const successReceivingDataExport = createAction(
  RECEIVE_DATA_EXPORT_SUCCESS
);
export const errorReceivingDataExport = createAction(RECEIVE_DATA_EXPORT_ERROR);
