import { createAction } from 'redux-actions';

export const REQUEST_PUBLIC_HOLIDAY = 'REQUEST_PUBLIC_HOLIDAY';
export const RECEIVE_PUBLIC_HOLIDAY = 'RECEIVE_PUBLIC_HOLIDAY';
export const RECEIVE_PUBLIC_HOLIDAY_ERROR = 'RECEIVE_PUBLIC_HOLIDAY_ERROR';
export const REMOVE_PUBLIC_HOLIDAY = 'REMOVE_PUBLIC_HOLIDAY';
export const REMOVE_PUBLIC_HOLIDAY_SUCCESS = 'REMOVE_PUBLIC_HOLIDAY_SUCCESS';
export const REMOVE_PUBLIC_HOLIDAY_ERROR = 'REMOVE_PUBLIC_HOLIDAY_ERROR';
export const UPDATE_PUBLIC_HOLIDAY = 'UPDATE_PUBLIC_HOLIDAY';
export const UPDATE_PUBLIC_HOLIDAY_SUCCESSFUL =
  'UPDATE_PUBLIC_HOLIDAY_SUCCESSFUL';
export const UPDATE_PUBLIC_HOLIDAY_ERROR = 'UPDATE_PUBLIC_HOLIDAY_ERROR';
export const CREATE_PUBLIC_HOLIDAY = 'CREATE_PUBLIC_HOLIDAY';
export const CREATE_PUBLIC_HOLIDAY_SUCCESSFUL =
  'CREATE_PUBLIC_HOLIDAY_SUCCESSFUL';
export const CREATE_PUBLIC_HOLIDAY_ERROR = 'CREATE_PUBLIC_HOLIDAY_ERROR';
export const REQUEST_PUBLIC_HOLIDAYS = 'REQUEST_PUBLIC_HOLIDAYS';
export const RECEIVE_PUBLIC_HOLIDAYS = 'RECEIVE_PUBLIC_HOLIDAYS';
export const RECEIVE_PUBLIC_HOLIDAYS_ERROR = 'RECEIVE_PUBLIC_HOLIDAYS_ERROR';

export const REQUEST_PUBLIC_HOLIDAY_YEARS = 'REQUEST_PUBLIC_HOLIDAY_YEARS';
export const RECEIVE_PUBLIC_HOLIDAY_YEARS = 'RECEIVE_PUBLIC_HOLIDAY_YEARS';
export const RECEIVE_PUBLIC_HOLIDAY_YEARS_ERROR =
  'RECEIVE_PUBLIC_HOLIDAY_YEARS_ERROR';

export const REQUEST_PUBLIC_HOLIDAYS_BY_YEAR =
  'REQUEST_PUBLIC_HOLIDAYS_BY_YEAR';
export const RECEIVE_PUBLIC_HOLIDAYS_BY_YEAR =
  'RECEIVE_PUBLIC_HOLIDAYS_BY_YEAR';
export const RECEIVE_PUBLIC_HOLIDAYS_BY_YEAR_ERROR =
  'RECEIVE_PUBLIC_HOLIDAYS_BY_YEAR_ERROR';
export const EXPAND_ACCORDION = 'EXPAND_ACCORDION';
export const CLEAR_PUBLIC_HOLIDAY = 'CLEAR_PUBLIC_HOLIDAY';

export const requestPublicHoliday = createAction(REQUEST_PUBLIC_HOLIDAY);
export const receivePublicHoliday = createAction(RECEIVE_PUBLIC_HOLIDAY);
export const errorReceivingPublicHoliday = createAction(
  RECEIVE_PUBLIC_HOLIDAY_ERROR
);
export const removePublicHoliday = createAction(REMOVE_PUBLIC_HOLIDAY);
export const removePublicHolidaySuccess = createAction(
  REMOVE_PUBLIC_HOLIDAY_SUCCESS
);
export const errorRemovingPublicHoliday = createAction(
  REMOVE_PUBLIC_HOLIDAY_ERROR
);
export const updatePublicHoliday = createAction(UPDATE_PUBLIC_HOLIDAY);
export const updatePublicHolidaySuccessful = createAction(
  UPDATE_PUBLIC_HOLIDAY_SUCCESSFUL
);
export const updatePublicHolidayError = createAction(
  UPDATE_PUBLIC_HOLIDAY_ERROR
);
export const createPublicHoliday = createAction(CREATE_PUBLIC_HOLIDAY);
export const createPublicHolidaySuccessful = createAction(
  CREATE_PUBLIC_HOLIDAY_SUCCESSFUL
);
export const createPublicHolidayError = createAction(
  CREATE_PUBLIC_HOLIDAY_ERROR
);
export const requestPublicHolidays = createAction(REQUEST_PUBLIC_HOLIDAYS);
export const receivePublicHolidays = createAction(RECEIVE_PUBLIC_HOLIDAYS);
export const errorReceivingPublicHolidays = createAction(
  RECEIVE_PUBLIC_HOLIDAYS_ERROR
);
export const requestPublicHolidayYears = createAction(
  REQUEST_PUBLIC_HOLIDAY_YEARS
);
export const receivePublicHolidayYears = createAction(
  RECEIVE_PUBLIC_HOLIDAY_YEARS
);
export const errorReceivingPublicHolidayYears = createAction(
  RECEIVE_PUBLIC_HOLIDAY_YEARS_ERROR
);
export const requestPublicHolidaysByYear = createAction(
  REQUEST_PUBLIC_HOLIDAYS_BY_YEAR
);
export const receivePublicHolidaysByYear = createAction(
  RECEIVE_PUBLIC_HOLIDAYS_BY_YEAR
);
export const errorReceivingPublicHolidaysByYear = createAction(
  RECEIVE_PUBLIC_HOLIDAYS_BY_YEAR_ERROR
);
export const expandAccordion = createAction(EXPAND_ACCORDION);
export const clearPublicHoliday = createAction(CLEAR_PUBLIC_HOLIDAY);
