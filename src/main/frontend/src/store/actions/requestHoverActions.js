import { createAction } from 'redux-actions';

export const REQUEST_HOVER = 'REQUEST_HOVER';
export const REQUEST_UNHOVER = 'REQUEST_UNHOVER';

export const requestHover = createAction(REQUEST_HOVER);
export const requestUnhover = createAction(REQUEST_UNHOVER);
