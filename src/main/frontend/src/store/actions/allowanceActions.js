import { createAction } from 'redux-actions';

export const REQUEST_ALLOWANCE = 'REQUEST_ALLOWANCE';
export const RECEIVE_ALLOWANCE = 'RECEIVE_ALLOWANCE';
export const RECEIVE_ALLOWANCE_ERROR = 'RECEIVE_ALLOWANCE_ERROR';
export const REQUEST_ALLOWANCE_FOR_USER = 'REQUEST_ALLOWANCE_FOR_USER';
export const RECEIVE_ALLOWANCE_FOR_USER = 'RECEIVE_ALLOWANCE_FOR_USER';
export const RECEIVE_ALLOWANCE_FOR_USER_ERROR =
  'RECEIVE_ALLOWANCE_FOR_USER_ERROR';
export const REQUEST_YEARS_WITH_ALLOWANCE = 'REQUEST_YEARS_WITH_ALLOWANCE';
export const RECEIVE_YEARS_WITH_ALLOWANCE = 'RECEIVE_YEARS_WITH_ALLOWANCE';
export const RECEIVE_YEARS_WITH_ALLOWANCE_ERROR =
  'RECEIVE_YEARS_WITH_ALLOWANCE_ERROR';
export const UPDATE_ALLOWANCE = 'UPDATE_ALLOWANCE';
export const UPDATE_ALLOWANCE_SUCCESS = 'UPDATE_ALLOWANCE_SUCCESS';
export const UPDATE_ALLOWANCE_ERROR = 'UPDATE_ALLOWANCE_ERROR';
export const REQUEST_ALLOWANCE_PER_USER_FOR_YEAR =
  'REQUEST_ALLOWANCE_PER_USER_FOR_YEAR';
export const RECIEVE_ALLOWANCE_PER_USER_FOR_YEAR =
  'RECIEVE_ALLOWANCE_PER_USER_FOR_YEAR';

export const requestAllowance = createAction(REQUEST_ALLOWANCE);
export const receiveAllowance = createAction(RECEIVE_ALLOWANCE);
export const errorReceivingAllowance = createAction(RECEIVE_ALLOWANCE_ERROR);
export const requestAllowanceForUser = createAction(REQUEST_ALLOWANCE_FOR_USER);
export const receiveAllowanceForUser = createAction(RECEIVE_ALLOWANCE_FOR_USER);
export const errorReceivingAllowanceForUser = createAction(
  RECEIVE_ALLOWANCE_FOR_USER_ERROR
);
export const requestYearsWithAllowance = createAction(
  REQUEST_YEARS_WITH_ALLOWANCE
);
export const receiveYearsWithAllowance = createAction(
  RECEIVE_YEARS_WITH_ALLOWANCE
);
export const errorReceivingYearsWithAllowance = createAction(
  RECEIVE_YEARS_WITH_ALLOWANCE_ERROR
);
export const updateAllowance = createAction(UPDATE_ALLOWANCE);
export const updateAllowanceSuccess = createAction(UPDATE_ALLOWANCE_SUCCESS);
export const errorUpdatingAllowance = createAction(UPDATE_ALLOWANCE_ERROR);
export const requestAllowancePerUserForYear = createAction(
  REQUEST_ALLOWANCE_PER_USER_FOR_YEAR
);
export const recieveAllowancePerUserForYear = createAction(
  RECIEVE_ALLOWANCE_PER_USER_FOR_YEAR
);
