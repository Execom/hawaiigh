import { createAction } from 'redux-actions';

export const SET_ACTIVE_TAB = 'SET_ACTIVE_TAB';

export const setActiveTab = createAction(SET_ACTIVE_TAB);
