import { createAction } from 'redux-actions';

export const ADD_REDIRECT_AFTER_LOGIN_ROUTE = 'ADD_REDIRECT_AFTER_LOGIN_ROUTE';
export const REMOVE_REDIRECT_AFTER_LOGIN_ROUTE =
  'REMOVE_REDIRECT_AFTER_LOGIN_ROUTE';

export const addRedirectAfterLoginRoute = createAction(
  ADD_REDIRECT_AFTER_LOGIN_ROUTE
);
export const removeRedirectAfterLoginRoute = createAction(
  REMOVE_REDIRECT_AFTER_LOGIN_ROUTE
);
