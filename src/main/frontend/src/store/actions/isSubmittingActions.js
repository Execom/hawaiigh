import { createAction } from 'redux-actions';

export const IS_SUBMITTING = 'IS_SUBMITTING';
export const IS_SUBMITTED = 'IS_SUBMITTED';

export const isSubmitting = createAction(IS_SUBMITTING);
export const isSubmitted = createAction(IS_SUBMITTED);
