import { createAction } from 'redux-actions';

export const REQUEST_ADMINS = 'REQUEST_ADMINS';
export const RECEIVE_ADMINS = 'RECEIVE_ADMINS';
export const RECEIVE_ADMINS_ERROR = 'RECEIVE_ADMINS_ERROR';
export const REMOVE_ADMIN = 'REMOVE_ADMIN';
export const REMOVE_ADMIN_SUCCESS = 'REMOVE_ADMIN_SUCCESS';
export const REMOVE_ADMIN_ERROR = 'REMOVE_ADMIN_ERROR';
export const ADD_ADMIN = 'ADD_ADMIN';
export const ADD_ADMIN_SUCCESS = 'ADD_ADMIN_SUCCESS';
export const ADD_ADMIN_ERROR = 'ADD_ADMIN_ERROR';
export const SUPER_ADMIN_SEARCH_USERS = 'ADMIN_SEARCH_USERS';
export const SUPER_ADMIN_SEARCH_USERS_SUCCESS = 'ADMIN_SEARCH_USERS_SUCCESS';
export const CLEAR_USERS = 'CLEAR_USERS';
export const SUPER_ADMIN_SEARCH_USERS_ERROR = 'SUPER_ADMIN_SEARCH_USERS_ERROR';

export const requestAdmins = createAction(REQUEST_ADMINS);
export const receiveAdmins = createAction(RECEIVE_ADMINS);
export const errorReceivingAdmins = createAction(RECEIVE_ADMINS_ERROR);
export const removeAdmin = createAction(REMOVE_ADMIN);
export const removeAdminSuccess = createAction(REMOVE_ADMIN_SUCCESS);
export const errorRemovingAdmin = createAction(REMOVE_ADMIN_ERROR);
export const addAdmin = createAction(ADD_ADMIN);
export const addAdminSuccess = createAction(ADD_ADMIN_SUCCESS);
export const errorAddingAdmin = createAction(ADD_ADMIN_ERROR);
export const superAdminSearchUsers = createAction(SUPER_ADMIN_SEARCH_USERS);
export const superAdminSearchUsersSuccess = createAction(
  SUPER_ADMIN_SEARCH_USERS_SUCCESS
);
export const clearUsers = createAction(CLEAR_USERS);
export const errorSearchingUsers = createAction(SUPER_ADMIN_SEARCH_USERS_ERROR);
