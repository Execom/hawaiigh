import { createAction } from 'redux-actions';

export const CREATE_LEAVE_REQUEST = 'CREATE_LEAVE_REQUEST';
export const CREATE_LEAVE_REQUEST_SUCCESSFUL =
  'CREATE_LEAVE_REQUEST_SUCCESSFUL';
export const CREATE_LEAVE_REQUEST_ERROR = 'CREATE_LEAVE_REQUEST_ERROR';
export const CREATE_SICKNESS_REQUEST = 'CREATE_SICKNESS_REQUEST';
export const CREATE_SICKNESS_REQUEST_SUCCESSFUL =
  'CREATE_SICKNESS_REQUEST_SUCCESSFUL';
export const CREATE_SICKNESS_REQUEST_ERROR = 'CREATE_SICKNESS_REQUEST_ERROR';
export const CREATE_BONUS_REQUEST = 'CREATE_BONUS_REQUEST';
export const CREATE_BONUS_REQUEST_SUCCESSFUL =
  'CREATE_BONUS_REQUEST_SUCCESSFUL';
export const CREATE_BONUS_REQUEST_ERROR = 'CREATE_BONUS_REQUEST_ERROR';
export const REQUEST_LEAVE_REQUESTS_FOR_YEAR =
  'REQUEST_LEAVE_REQUESTS_FOR_YEAR';
export const RECEIVE_LEAVE_REQUESTS_FOR_YEAR =
  'RECEIVE_LEAVE_REQUESTS_FOR_YEAR';
export const REQUEST_LEAVE_REQUESTS_FOR_YEAR_ERROR =
  'REQUEST_LEAVE_REQUESTS_FOR_YEAR_ERROR';
export const HANDLE_REQUEST = 'HANDLE_REQUEST';
export const HANDLE_REQUEST_SUCCESSFUL = 'HANDLE_REQUEST_SUCCESSFUL';
export const HANDLE_REQUEST_ERROR = 'HANDLE_REQUEST_ERROR';
export const REQUEST_REQUESTS_FOR_APPROVAL = 'REQUEST_REQUESTS_FOR_APPROVAL';
export const RECEIVE_REQUESTS_FOR_APPROVAL = 'RECEIVE_REQUESTS_FOR_APPROVAL';
export const RECEIVE_REQUESTS_FOR_APPROVAL_ERROR =
  'RECEIVE_REQUESTS_FOR_APPROVAL_ERROR';
export const REQUEST_USER_LEAVE_REQUESTS_FOR_YEAR =
  'REQUEST_USER_LEAVE_REQUESTS_FOR_YEAR';
export const RECEIVE_USER_LEAVE_REQUESTS_FOR_YEAR =
  'RECEIVE_USER_LEAVE_REQUESTS_FOR_YEAR';
export const REQUEST_USER_LEAVE_REQUESTS_FOR_YEAR_ERROR =
  'REQUEST_USER_LEAVE_REQUESTS_FOR_YEAR_ERROR';
export const REQUEST_TEAM_REQUESTS_FOR_MONTH =
  'REQUEST_TEAM_REQUESTS_FOR_MONTH';
export const RECEIVE_TEAM_REQUESTS_FOR_MONTH =
  'RECEIVE_TEAM_REQUESTS_FOR_MONTH';
export const RECEIVE_TEAM_REQUESTS_FOR_MONTH_ERROR =
  'RECEIVE_TEAM_REQUESTS_FOR_MONTH_ERROR';

export const createLeaveRequest = createAction(CREATE_LEAVE_REQUEST);
export const createLeaveRequestSuccessful = createAction(
  CREATE_LEAVE_REQUEST_SUCCESSFUL
);
export const createLeaveRequestError = createAction(CREATE_LEAVE_REQUEST_ERROR);
export const createSicknessRequest = createAction(CREATE_SICKNESS_REQUEST);
export const createSicknessRequestSuccessful = createAction(
  CREATE_SICKNESS_REQUEST_SUCCESSFUL
);
export const createSicknessRequestError = createAction(
  CREATE_SICKNESS_REQUEST_ERROR
);
export const createBonusRequest = createAction(CREATE_BONUS_REQUEST);
export const createBonusRequestSuccessful = createAction(
  CREATE_BONUS_REQUEST_SUCCESSFUL
);
export const createBonusRequestError = createAction(CREATE_BONUS_REQUEST_ERROR);
export const requestLeaveRequestsForYear = createAction(
  REQUEST_LEAVE_REQUESTS_FOR_YEAR
);
export const receiveLeaveRequestsForYear = createAction(
  RECEIVE_LEAVE_REQUESTS_FOR_YEAR
);
export const requestLeaveRequestsForYearError = createAction(
  REQUEST_LEAVE_REQUESTS_FOR_YEAR_ERROR
);
export const handleRequest = createAction(HANDLE_REQUEST);
export const handleRequestSuccessful = createAction(HANDLE_REQUEST_SUCCESSFUL);
export const handleRequestError = createAction(HANDLE_REQUEST_ERROR);
export const requestRequestsForApproval = createAction(
  REQUEST_REQUESTS_FOR_APPROVAL
);
export const receiveRequestsForApproval = createAction(
  RECEIVE_REQUESTS_FOR_APPROVAL
);
export const errorReceivingRequestsForApproval = createAction(
  RECEIVE_REQUESTS_FOR_APPROVAL_ERROR
);
export const requestUserLeaveRequestsForYear = createAction(
  REQUEST_USER_LEAVE_REQUESTS_FOR_YEAR
);
export const receiveUserLeaveRequestsForYear = createAction(
  RECEIVE_USER_LEAVE_REQUESTS_FOR_YEAR
);
export const requestUserLeaveRequestsForYearError = createAction(
  REQUEST_USER_LEAVE_REQUESTS_FOR_YEAR_ERROR
);
export const requestTeamRequestsForMonth = createAction(
  REQUEST_TEAM_REQUESTS_FOR_MONTH
);
export const receiveTeamRequestsForMonth = createAction(
  RECEIVE_TEAM_REQUESTS_FOR_MONTH
);
export const errorReceivingTeamRequestsForMonth = createAction(
  RECEIVE_TEAM_REQUESTS_FOR_MONTH_ERROR
);
