import { createAction } from 'redux-actions';

export const OPEN_MENU_DRAWER = 'OPEN_MENU_DRAWER';
export const OPEN_ACTION_DRAWER = 'OPEN_ACTION_DRAWER';
export const TOGGLE_MODAL_SIZE = 'TOGGLE_MODAL_SIZE';
export const CLOSE_DRAWER = 'CLOSE_DRAWER';

export const openMenuDrawer = createAction(OPEN_MENU_DRAWER);
export const toggleModalSize = createAction(TOGGLE_MODAL_SIZE);
export const openActionDrawer = createAction(OPEN_ACTION_DRAWER);
export const closeDrawer = createAction(CLOSE_DRAWER);
