import { createAction } from 'redux-actions';

export const CLEAR_STORE = 'CLEAR_STORE';

export const clearStore = createAction(CLEAR_STORE);
