import { createAction } from 'redux-actions';

export const LOADING_START = 'LOADING_START';
export const LOADING_END = 'LOADING_END';

export const loadingStart = createAction(LOADING_START);
export const loadingEnd = createAction(LOADING_END);
