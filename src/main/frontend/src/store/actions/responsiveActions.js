import { createAction } from 'redux-actions';

export const SET_WINDOW_WIDTH = 'SET_WINDOW_WIDTH';

export const setWindowWidth = createAction(SET_WINDOW_WIDTH);
