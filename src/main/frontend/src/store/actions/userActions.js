import { createAction } from 'redux-actions';

export const REQUEST_USER = 'REQUEST_USER';
export const RECEIVE_USER = 'RECEIVE_USER';
export const RECEIVE_USER_ERROR = 'RECEIVE_USER_ERROR';
export const CLEAR_USER = 'CLEAR_USER';
export const REQUEST_USER_DAYS = 'REQUEST_USER_DAYS';
export const RECEIVE_USER_DAYS = 'RECEIVE_USER_DAYS';
export const RECEIVE_USER_DAYS_ERROR = 'RECEIVE_USER_DAYS_ERROR';
export const REQUEST_UPDATE_USER_DAYS_FOR_TEAM =
  'REQUEST_UPDATE_USER_DAYS_FOR_TEAM';
export const UPDATE_USER_DAYS_FOR_TEAM_ERROR =
  'UPDATE_USER_DAYS_FOR_TEAM_ERROR';
export const REQUEST_USER_DAYS_FOR_MONTH = 'REQUEST_USER_DAYS_FOR_MONTH';
export const RECEIVE_USER_DAYS_FOR_MONTH = 'RECEIVE_USER_DAYS_FOR_MONTH';
export const RECEIVE_USER_DAYS_FOR_MONTH_ERROR =
  'RECEIVE_USER_DAYS_FOR_MONTH_ERROR';
export const CREATE_EMPLOYEE = 'CREATE_EMPLOYEE';
export const CREATE_EMPLOYEE_SUCCESS = 'CREATE_EMPLOYEE_SUCCESS';
export const CREATE_EMPLOYEE_ERROR = 'CREATE_EMPLOYEE_ERROR';
export const REQUEST_EMPLOYEE = 'REQUEST_EMPLOYEE';
export const RECEIVE_EMPLOYEE = 'RECEIVE_EMPLOYEE';
export const RECEIVE_EMPLOYEE_ERROR = 'RECEIVE_EMPLOYEE_ERROR';
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE';
export const UPDATE_EMPLOYEE_SUCCESSFUL = 'UPDATE_EMPLOYEE_SUCCESSFUL';
export const UPDATE_EMPLOYEE_ERROR = 'UPDATE_EMPLOYEE_ERROR';
export const REMOVE_EMPLOYEE = 'REMOVE_EMPLOYEE';
export const REMOVE_EMPLOYEE_SUCCESS = 'REMOVE_EMPLOYEE_SUCCESS';
export const REMOVE_EMPLOYEE_ERROR = 'REMOVE_EMPLOYEE_ERROR';
export const RESTORE_EMPLOYEE = 'RESTORE_EMPLOYEE';
export const RESTORE_EMPLOYEE_SUCCESS = 'RESTORE_EMPLOYEE_SUCCESS';
export const RESTORE_EMPLOYEE_ERROR = 'RESTORE_EMPLOYEE_ERROR';
export const REQUEST_EMPLOYEES = 'REQUEST_EMPLOYEES';
export const RECEIVE_EMPLOYEES = 'RECEIVE_EMPLOYEES';
export const RECEIVE_EMPLOYEES_ERROR = 'RECEIVE_EMPLOYEES_ERROR';
export const RESET_EMPLOYEES = 'RESET_EMPLOYEES';
export const SEARCH_EMPLOYEES = 'SEARCH_EMPLOYEES';
export const ADMIN_SEARCH_EMPLOYEES = 'ADMIN_SEARCH_EMPLOYEES';
export const SEARCH_EMPLOYEES_SUCCESS = 'SEARCH_EMPLOYEES_SUCCESS';
export const ADMIN_SEARCH_EMPLOYEES_SUCCESS = 'ADMIN_SEARCH_EMPLOYEES_SUCCESS';
export const SEARCH_EMPLOYEES_ERROR = 'SEARCH_EMPLOYEES_ERROR';
export const CLEAR_EMPLOYEES = 'CLEAR_EMPLOYEES';
export const ADD_SEARCHED_EMPLOYEE = 'ADD_SEARCHED_EMPLOYEE';
export const REMOVE_SEARCHED_EMPLOYEE = 'REMOVE_SEARCHED_EMPLOYEE';
export const CLEAR_EMPLOYEE = 'CLEAR_EMPLOYEE';
export const CANCEL_EMPLOYEE_DELETE = 'CANCEL_EMPLOYEE_DELETE';
export const CANCEL_EMPLOYEE_DELETE_SUCCESS = 'CANCEL_EMPLOYEE_DELETE_SUCCESS';
export const CANCEL_EMPLOYEE_DELETE_ERROR = 'CANCEL_EMPLOYEE_DELETE_ERROR';

export const requestUser = createAction(REQUEST_USER);
export const receiveUser = createAction(RECEIVE_USER);
export const errorReceivingUser = createAction(RECEIVE_USER_ERROR);
export const clearUser = createAction(CLEAR_USER);
export const requestUserDays = createAction(REQUEST_USER_DAYS);
export const receiveUserDays = createAction(RECEIVE_USER_DAYS);
export const errorReceivingUserDays = createAction(RECEIVE_USER_DAYS_ERROR);
export const requestUpdateUserDaysForTeam = createAction(
  REQUEST_UPDATE_USER_DAYS_FOR_TEAM
);
export const errorReceivingUserDaysForTeamUpdate = createAction(
  UPDATE_USER_DAYS_FOR_TEAM_ERROR
);
export const requestUserDaysForMonth = createAction(
  REQUEST_USER_DAYS_FOR_MONTH
);
export const receiveUserDaysForMonth = createAction(
  RECEIVE_USER_DAYS_FOR_MONTH
);
export const errorReceivingUserDaysForMonth = createAction(
  RECEIVE_USER_DAYS_FOR_MONTH_ERROR
);
export const createEmployee = createAction(CREATE_EMPLOYEE);
export const createEmployeeSuccess = createAction(CREATE_EMPLOYEE_SUCCESS);
export const errorCreatingEmployee = createAction(CREATE_EMPLOYEE_ERROR);
export const requestEmployee = createAction(REQUEST_EMPLOYEE);
export const receiveEmployee = createAction(RECEIVE_EMPLOYEE);
export const errorReceivingEmployee = createAction(RECEIVE_EMPLOYEE_ERROR);
export const updateEmployee = createAction(UPDATE_EMPLOYEE);
export const updateEmployeeSuccessful = createAction(
  UPDATE_EMPLOYEE_SUCCESSFUL
);
export const updateEmployeeError = createAction(UPDATE_EMPLOYEE_ERROR);
export const removeEmployee = createAction(REMOVE_EMPLOYEE);
export const cancelEmployeeDelete = createAction(CANCEL_EMPLOYEE_DELETE);
export const cancelEmployeeDeleteSuccess = createAction(
  CANCEL_EMPLOYEE_DELETE_SUCCESS
);
export const errorCancelEmployeeDelete = createAction(
  CANCEL_EMPLOYEE_DELETE_ERROR
);
export const removeEmployeeSuccess = createAction(REMOVE_EMPLOYEE_SUCCESS);
export const errorRemovingEmployee = createAction(REMOVE_EMPLOYEE_ERROR);
export const restoreEmployee = createAction(RESTORE_EMPLOYEE);
export const restoreEmployeeSuccess = createAction(RESTORE_EMPLOYEE_SUCCESS);
export const errorRestoringEmployee = createAction(RESTORE_EMPLOYEE_ERROR);
export const requestEmployees = createAction(REQUEST_EMPLOYEES);
export const receiveEmployees = createAction(RECEIVE_EMPLOYEES);
export const errorReceivingEmployees = createAction(RECEIVE_EMPLOYEES_ERROR);
export const resetEmployees = createAction(RESET_EMPLOYEES);
export const searchEmployees = createAction(SEARCH_EMPLOYEES);
export const searchEmployeesSuccess = createAction(SEARCH_EMPLOYEES_SUCCESS);
export const adminSearchEmployees = createAction(ADMIN_SEARCH_EMPLOYEES);
export const adminSearchEmployeesSuccess = createAction(
  ADMIN_SEARCH_EMPLOYEES_SUCCESS
);
export const errorSearchingEmployees = createAction(SEARCH_EMPLOYEES_ERROR);
export const clearEmployees = createAction(CLEAR_EMPLOYEES);
export const addSearchedEmployee = createAction(ADD_SEARCHED_EMPLOYEE);
export const removeSearchedEmployee = createAction(REMOVE_SEARCHED_EMPLOYEE);
export const clearEmployee = createAction(CLEAR_EMPLOYEE);
