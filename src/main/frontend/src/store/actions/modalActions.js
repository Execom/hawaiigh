import { createAction } from 'redux-actions';

export const OPEN_REQUEST_MODAL = 'OPEN_REQUEST_MODAL';
export const OPEN_CONFIRMATION_MODAL = 'OPEN_CONFIRMATION_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const HANDLE_REQUEST_MODAL = 'HANDLE_REQUEST_MODAL';
export const OPEN_HANDLE_REQUEST_MODAL = 'OPEN_HANDLE_REQUEST_MODAL';
export const OPEN_INFO_MODAL = 'OPEN_INFO_MODAL';
export const OPEN_TEAM_REQUESTS_MODAL = 'OPEN_TEAM_REQUESTS_MODAL';
export const OPEN_EMPLOYEE_RESTORE_MODAL = 'OPEN_EMPLOYEE_RESTORE_MODAL';
export const OPEN_EMPLOYEE_DELETE_MODAL = 'OPEN_EMPLOYEE_DELETE_MODAL';
export const OPEN_INPUT_MODAL = 'OPEN_INPUT_MODAL';
export const OPEN_EMPLOYEE_CANCEL_DELETE_MODAL =
  'OPEN_EMPLOYEE_CANCEL_DELETE_MODAL';
export const OPEN_ALLOWANCE_CALENDAR_VIEW = 'OPEN_ALLOWANCE_CALENDAR_VIEW';
export const OPEN_ADD_ADMIN_MODAL = 'OPEN_ADD_ADMIN_MODAL';

export const openRequestModal = createAction(OPEN_REQUEST_MODAL);
export const openConfirmationModal = createAction(OPEN_CONFIRMATION_MODAL);
export const closeModal = createAction(CLOSE_MODAL);
export const handleRequestModal = createAction(HANDLE_REQUEST_MODAL);
export const openHandleRequestModal = createAction(OPEN_HANDLE_REQUEST_MODAL);
export const openInfoModal = createAction(OPEN_INFO_MODAL);
export const openTeamRequestsModal = createAction(OPEN_TEAM_REQUESTS_MODAL);
export const openEmployeeRestoreModal = createAction(
  OPEN_EMPLOYEE_RESTORE_MODAL
);
export const openEmployeeDeleteModal = createAction(OPEN_EMPLOYEE_DELETE_MODAL);
export const openEmployeeCancelDeleteModal = createAction(
  OPEN_EMPLOYEE_CANCEL_DELETE_MODAL
);
export const openInputModal = createAction(OPEN_INPUT_MODAL);
export const openAllowanceCalendarView = createAction(
  OPEN_ALLOWANCE_CALENDAR_VIEW
);
export const openAddAdminModal = createAction(OPEN_ADD_ADMIN_MODAL);
