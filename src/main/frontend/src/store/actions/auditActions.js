import { createAction } from 'redux-actions';

export const REQUEST_EMPLOYEE_AUDIT = 'REQUEST_EMPLOYEE_AUDIT';
export const REQUEST_ALLOWANCE_AUDIT = 'REQUEST_ALLOWANCE_AUDIT';
export const REQUEST_TEAM_AUDIT = 'REQUEST_TEAM_AUDIT';
export const RECEIVE_AUDIT = 'RECEIVE_AUDIT';
export const RECEIVE_AUDIT_ERROR = 'RECEIVE_AUDIT_ERROR';

export const requestEmployeeAudit = createAction(REQUEST_EMPLOYEE_AUDIT);
export const requestAllowanceAudit = createAction(REQUEST_ALLOWANCE_AUDIT);
export const requestTeamAudit = createAction(REQUEST_TEAM_AUDIT);
export const receiveAudit = createAction(RECEIVE_AUDIT);
export const receiveAuditError = createAction(RECEIVE_AUDIT_ERROR);
