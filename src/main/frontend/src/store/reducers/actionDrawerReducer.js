import { handleActions } from 'redux-actions';
import {
  closeDrawer,
  openActionDrawer,
  toggleModalSize
} from '../actions/drawerActions';
import { navigateOut } from '../actions/navigateActions';

const initialState = {
  payload: {
    type: null,
    id: null
  },
  open: false,
  wide: 0
};

const actionHandlers = {
  [openActionDrawer]: (state, { payload }) => {
    return {
      payload,
      open: true
    };
  },
  [closeDrawer]: state => {
    return {
      ...state,
      open: false
    };
  },
  [toggleModalSize]: state => {
    return {
      ...state,
      wide: !state.wide
    };
  },
  [navigateOut]: () => initialState
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
