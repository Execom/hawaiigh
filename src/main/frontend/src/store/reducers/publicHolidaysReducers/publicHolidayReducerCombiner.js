import { combineReducers } from 'redux';
import publicHoliday from './publicHolidayReducer';
import publicHolidayYears from './publicHolidayYearsReducer';
import publicHolidays from './publicHolidaysReducer';

export default combineReducers({
  publicHoliday,
  publicHolidayYears,
  publicHolidays
});
