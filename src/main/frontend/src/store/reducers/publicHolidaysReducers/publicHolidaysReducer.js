import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import { receivePublicHolidays } from '../../actions/publicHolidayActions';

const initialState = [];

const actionHandlers = {
  [receivePublicHolidays]: (state, action) => action.payload,
  [navigateOut]: () => initialState
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
