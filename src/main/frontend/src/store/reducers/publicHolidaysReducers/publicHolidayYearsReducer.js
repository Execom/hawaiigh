import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import {
  expandAccordion,
  receivePublicHolidaysByYear,
  receivePublicHolidayYears
} from '../../actions/publicHolidayActions';

const initialState = {
  years: {},
  last: false,
  accordionExpanded: {}
};

const actionHandlers = {
  [navigateOut]: () => initialState,
  [receivePublicHolidayYears]: (state, { payload }) => {
    return {
      ...state,
      years: payload.content.reduce(
        (total, current) => ({
          ...total,
          [current]: []
        }),
        state.years
      ),
      last: payload.last,
      page: payload.number
    };
  },
  [receivePublicHolidaysByYear]: (state, { payload }) => {
    return {
      ...state,
      years: {
        ...state.years,
        [payload.yearSelected]: payload.content
      },
      yearSelected: payload.yearSelected
    };
  },
  [expandAccordion]: (state, { payload: { year, expanded } }) => {
    return {
      ...state,
      accordionExpanded: {
        ...state.accordionExpanded,
        [year]: state.accordionExpanded[year] ? expanded : true
      }
    };
  }
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
