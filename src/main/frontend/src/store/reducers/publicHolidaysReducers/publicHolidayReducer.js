import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import {
  clearPublicHoliday,
  receivePublicHoliday
} from '../../actions/publicHolidayActions';

const initialState = null;

const actionHandlers = {
  [receivePublicHoliday]: (state, action) => action.payload,
  [navigateOut]: () => null,
  [clearPublicHoliday]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
