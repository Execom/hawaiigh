import { handleActions } from 'redux-actions';
import { clearStore } from '../actions/clearStoreActions';
import { navigateOut } from '../actions/navigateActions';
import { receiveRequestsForApproval } from '../actions/requestsActions';

const initialState = {
  requests: null,
  total: 0
};

const actionHandlers = {
  [receiveRequestsForApproval]: (state, action) => {
    return {
      requests: action.payload,
      total: action.payload.length
    };
  },
  [navigateOut]: (state, action) => {
    return {
      ...state,
      requests: null
    };
  },
  [clearStore]: (state, action) => {
    return {
      ...state,
      requests: null
    };
  }
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
