import { handleActions } from 'redux-actions';
import {
  receiveAllowance,
  receiveAllowanceForUser,
  receiveYearsWithAllowance,
  recieveAllowancePerUserForYear
} from '../actions/allowanceActions';
import { clearStore } from '../actions/clearStoreActions';
import { navigateOut } from '../actions/navigateActions';
import { makeUpdaterReducer } from './helpers/reducerHelpers';

const initialState = {
  allowances: null,
  results: null,
  last: false,
  searched: false,
  years: []
};

const actionHandlers = {
  [receiveAllowance]: makeUpdaterReducer('allowances'),
  [receiveAllowanceForUser]: makeUpdaterReducer('allowances'),
  [receiveYearsWithAllowance]: makeUpdaterReducer('years'),
  [navigateOut]: (state, action) => {
    return {
      ...state,
      allowances: null,
      years: []
    };
  },
  [clearStore]: (state, action) => {
    return {
      ...state,
      allowances: null,
      years: []
    };
  },
  [recieveAllowancePerUserForYear]: (state, action) => {
    const {
      content,
      last,
      first,
      pageable: { pageNumber }
    } = action.payload;

    return {
      ...state,
      results: first ? content : [...state.results, ...content],
      last: last,
      page: pageNumber,
      searched: true
    };
  }
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
