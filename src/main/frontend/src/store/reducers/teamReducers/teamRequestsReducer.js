import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import { receiveTeamRequestsForMonth } from '../../actions/requestsActions';

const initialState = null;

const actionHandlers = {
  [receiveTeamRequestsForMonth]: (state, action) => action.payload,
  [navigateOut]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
