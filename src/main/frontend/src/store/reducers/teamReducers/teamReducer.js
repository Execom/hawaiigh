import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import { clearTeam, receiveTeam } from '../../actions/teamActions';

const initialState = null;

const actionHandlers = {
  [receiveTeam]: (state, action) => action.payload,
  [navigateOut]: () => null,
  [clearTeam]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
