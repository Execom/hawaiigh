import { combineReducers } from 'redux';
import team from './teamReducer';
import teamRequests from './teamRequestsReducer';
import teams from './teamsReducer';
import teamsSearch from './teamsSearchReducer';

export default combineReducers({
  team,
  teams,
  teamsSearch,
  teamRequests
});
