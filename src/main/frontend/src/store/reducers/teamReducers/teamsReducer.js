import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import { receiveTeams, receiveTeamsName } from '../../actions/teamActions';

const initialState = {
  results: [],
  last: false,
  name: []
};

const actionHandlers = {
  [receiveTeams]: (state, { payload }) => {
    return {
      ...state,
      results: payload.first
        ? payload.content
        : [...state.results, ...payload.content],
      last: payload.last,
      page: payload.number
    };
  },
  [receiveTeamsName]: (state, { payload }) => {
    return {
      ...state,
      name: payload
    };
  },
  [navigateOut]: () => initialState
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
