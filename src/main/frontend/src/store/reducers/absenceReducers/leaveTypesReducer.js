import { handleActions } from 'redux-actions';
import { receiveLeaveTypes } from '../../actions/absenceActions';
import { navigateOut } from '../../actions/navigateActions';
import { getResults } from '../helpers/reducerHelpers';

const initialState = { results: [], last: false };

const actionHandlers = {
  [receiveLeaveTypes]: (state, { payload }) => {
    return {
      ...state,
      results: getResults(state, payload),
      last: payload.last,
      page: payload.number
    };
  },
  [navigateOut]: () => ({ results: [], last: false })
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
