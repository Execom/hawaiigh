import { handleActions } from 'redux-actions';
import { receiveLeaveTypeIcons } from '../../actions/absenceActions';

const initialState = null;

const actionHandlers = {
  [receiveLeaveTypeIcons]: (state, action) => action.payload
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
