import { handleActions } from 'redux-actions';
import { clearLeaveType, receiveLeaveType } from '../../actions/absenceActions';
import { navigateOut } from '../../actions/navigateActions';

const initialState = null;

const actionHandlers = {
  [receiveLeaveType]: (state, action) => action.payload,
  [navigateOut]: () => null,
  [clearLeaveType]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
