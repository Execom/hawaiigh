import { combineReducers } from 'redux';
import leaveType from './leaveTypeReducer';
import leaveTypesIcons from './leaveTypesIconsReducer';
import leaveTypes from './leaveTypesReducer';

export default combineReducers({
  leaveType,
  leaveTypes,
  leaveTypesIcons
});
