import { handleActions } from 'redux-actions';
import { clearStore } from '../actions/clearStoreActions';
import { navigateOut } from '../actions/navigateActions';
import {
  receiveLeaveRequestsForYear,
  receiveUserLeaveRequestsForYear
} from '../actions/requestsActions';

const initialState = null;

const makeFilterUpdaterReducer = (state, action) => ({
  ...state,
  leave: action.payload.filter(item => item.absence.absenceType === 'LEAVE'),
  sickness: action.payload.filter(
    item => item.absence.absenceType === 'SICKNESS'
  ),
  training: action.payload.filter(
    item => item.absence.absenceType === 'TRAINING'
  ),
  bonus: action.payload.filter(item => item.absence.absenceType === 'BONUS')
});

const actionHandlers = {
  [receiveLeaveRequestsForYear]: (state, action) =>
    makeFilterUpdaterReducer(state, action),
  [receiveUserLeaveRequestsForYear]: (state, action) =>
    makeFilterUpdaterReducer(state, action),
  [navigateOut]: () => null,
  [clearStore]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
