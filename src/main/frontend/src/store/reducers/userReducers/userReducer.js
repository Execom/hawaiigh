import { handleActions } from 'redux-actions';
import { clearUser, receiveUser } from '../../actions/userActions';

const initialState = null;

const actionHandlers = {
  [receiveUser]: (state, action) => action.payload,
  [clearUser]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
