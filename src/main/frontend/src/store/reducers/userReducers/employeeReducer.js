import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import { clearEmployee, receiveEmployee } from '../../actions/userActions';

const initialState = null;

const actionHandlers = {
  [receiveEmployee]: (state, action) => action.payload,
  [navigateOut]: () => null,
  [clearEmployee]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
