import { handleActions } from 'redux-actions';
import {
  addSearchedEmployee,
  removeSearchedEmployee
} from '../../actions/userActions';

const initialState = null;

const actionHandlers = {
  [addSearchedEmployee]: (state, action) => action.payload,
  [removeSearchedEmployee]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
