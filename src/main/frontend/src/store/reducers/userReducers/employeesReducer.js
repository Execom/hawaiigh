import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import {
  adminSearchEmployeesSuccess,
  receiveEmployees,
  resetEmployees
} from '../../actions/userActions';
import { getResults } from '../helpers/reducerHelpers';

const initialState = { results: [], last: false, searched: false };

const resetReducer = state => ({
  ...state,
  results: null,
  last: false
});

const actionHandlers = {
  [receiveEmployees]: (state, action) => {
    return {
      ...state,
      results: getResults(state, action.payload),
      last: action.payload.last,
      page: action.payload.number
    };
  },
  [resetEmployees]: state => resetReducer(state),
  [navigateOut]: state => resetReducer(state),
  [adminSearchEmployeesSuccess]: (state, action) => {
    const {
      content,
      last,
      first,
      pageable: { pageNumber }
    } = action.payload;

    return {
      ...state,
      results: first ? content : [...state.results, ...content],
      last: last,
      page: pageNumber,
      searched: true
    };
  }
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
