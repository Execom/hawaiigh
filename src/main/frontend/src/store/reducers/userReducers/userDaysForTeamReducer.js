import _cloneDeep from 'lodash/cloneDeep';
import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import {
  receiveUserDaysForTeam,
  removeUserDaysForTeamOnYearChange,
  resetUserDaysValues,
  updateUserDaysForTeam
} from '../../actions/teamActions';

const handleUpdate = (results, userId, userDays) => {
  const newResults = _cloneDeep(results);
  const userArrayIndex = newResults.findIndex(user => user.id === userId);
  newResults[userArrayIndex] = userDays;

  return newResults;
};

const initialState = { results: [], last: false, page: 0 };

const resetReducer = state => ({
  ...state,
  results: [],
  last: false,
  page: 0
});

const actionHandlers = {
  [receiveUserDaysForTeam]: (state, action) => {
    return {
      ...state,
      results: [...state.results, ...action.payload.content],
      last: action.payload.last,
      page: action.payload.number
    };
  },
  [updateUserDaysForTeam]: (state, action) => {
    return {
      ...state,
      results: handleUpdate(
        state.results,
        action.payload.userId,
        action.payload.userDays
      )
    };
  },
  [removeUserDaysForTeamOnYearChange]: (state, action) => resetReducer(state),
  [resetUserDaysValues]: (state, action) => resetReducer(state),
  [navigateOut]: (state, action) => resetReducer(state)
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
