import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import { receiveUserDays } from '../../actions/userActions';

const initialState = null;

const actionHandlers = {
  [receiveUserDays]: (state, action) => action.payload,
  [navigateOut]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
