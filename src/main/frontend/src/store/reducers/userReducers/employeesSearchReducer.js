import { handleActions } from 'redux-actions';
import { navigateOut } from '../../actions/navigateActions';
import {
  clearEmployees,
  searchEmployees,
  searchEmployeesSuccess
} from '../../actions/userActions';

const initialState = {
  isFetching: false,
  results: [],
  last: false,
  page: 0
};

const actionHandlers = {
  [searchEmployees]: state => {
    return {
      ...state,
      isFetching: true
    };
  },
  [searchEmployeesSuccess]: (state, action) => {
    const {
      content,
      last,
      pageable: { pageNumber }
    } = action.payload;

    return {
      ...state,
      isFetching: false,
      results: [...state.results, ...content],
      last: last,
      page: pageNumber
    };
  },
  [clearEmployees]: state => {
    return {
      ...state,
      isFetching: false,
      results: [],
      last: false,
      page: 0
    };
  },
  [navigateOut]: state => {
    return {
      ...state,
      results: [],
      last: false,
      page: 0
    };
  }
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
