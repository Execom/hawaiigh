import { combineReducers } from 'redux';
import employee from './employeeReducer';
import employees from './employeesReducer';
import employeesSearch from './employeesSearchReducer';
import searchedEmployee from './searchedEmployeeReducer';
import userDaysForTeam from './userDaysForTeamReducer';
import userDays from './userDaysReducer';
import user from './userReducer';

export default combineReducers({
  employee,
  employees,
  employeesSearch,
  searchedEmployee,
  user,
  userDays,
  userDaysForTeam
});
