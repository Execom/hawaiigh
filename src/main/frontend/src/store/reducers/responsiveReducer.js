import { handleActions } from 'redux-actions';
import { setWindowWidth } from '../actions/responsiveActions';

const initialState = null;

const actionHandlers = {
  [setWindowWidth]: (state, action) => action.payload
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
