import { handleActions } from 'redux-actions';
import {
  clearLeaveProfile,
  receiveLeaveProfile
} from '../../actions/leaveProfileActions';
import { navigateOut } from '../../actions/navigateActions';

const initialState = null;

const actionHandlers = {
  [receiveLeaveProfile]: (state, action) => action.payload,
  [clearLeaveProfile]: () => null,
  [navigateOut]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
