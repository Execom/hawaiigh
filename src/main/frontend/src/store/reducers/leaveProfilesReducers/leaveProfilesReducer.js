import { handleActions } from 'redux-actions';
import { receiveLeaveProfiles } from '../../actions/leaveProfileActions';
import { navigateOut } from '../../actions/navigateActions';
import { getResults } from '../helpers/reducerHelpers';

const initialState = {
  results: [],
  last: false
};

const actionHandlers = {
  [receiveLeaveProfiles]: (state, { payload }) => {
    return {
      ...state,
      results: getResults(state, payload),
      last: payload.last,
      page: payload.number
    };
  },
  [navigateOut]: () => ({
    results: [],
    last: false
  })
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
