import { combineReducers } from 'redux';
import leaveProfile from './leaveProfileReducer';
import leaveProfiles from './leaveProfilesReducer';

export default combineReducers({
  leaveProfile,
  leaveProfiles
});
