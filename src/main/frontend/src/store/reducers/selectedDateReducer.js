import { handleActions } from 'redux-actions';
import { navigateOut } from '../actions/navigateActions';
import { setSelectedDate } from '../actions/yearActions';

const initialState = null;

const actionHandlers = {
  [setSelectedDate]: (state, action) => action.payload,
  [navigateOut]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
