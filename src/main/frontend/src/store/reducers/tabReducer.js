import { handleActions } from 'redux-actions';
import { clearStore } from '../actions/clearStoreActions';
import { setActiveTab } from '../actions/tabActions';

const initialState = 0;

const actionHandlers = {
  [setActiveTab]: (state, action) => action.payload,
  [clearStore]: (state, action) => 0
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
