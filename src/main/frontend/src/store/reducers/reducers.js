import { reducer as toastr } from 'react-redux-toastr';
import { combineReducers } from 'redux';
import absenceObject from './absenceReducers/absenceReducerCombiner';
import actionDrawer from './actionDrawerReducer';
import allowance from './allowanceReducer';
import approvals from './approvalsReducer';
import audit from './auditReducer';
import authorization from './getTokenReducer';
import isSubmitting from './isSubmittingReducer';
import leaveProfilesObject from './leaveProfilesReducers/leaveProfilesReducerCombiner';
import leaveRequests from './leaveRequestsReducer';
import loading from './loadingReducer';
import menuDrawer from './menuDrawerReducer';
import modal from './modalReducer';
import publicHolidayObject from './publicHolidaysReducers/publicHolidayReducerCombiner';
import redirectAfterLoginRoute from './redirectAfterLoginReducer';
import requestHover from './requestHoverReducer';
import windowWidth from './responsiveReducer';
import selectedDate from './selectedDateReducer';
import activeTab from './tabReducer';
import teamObject from './teamReducers/teamReducerCombiner';
import userObject from './userReducers/userReducerCombiner';
import yearObject from './yearReducers/yearReducerCombiner';
import administration from './administrationReducer';
import monitoring from './monitoringReducer/monitoringReducer';
import admin from './adminReducer';

export default combineReducers({
  audit,
  absenceObject,
  authorization,
  teamObject,
  userObject,
  publicHolidayObject,
  leaveProfilesObject,
  modal,
  redirectAfterLoginRoute,
  yearObject,
  allowance,
  leaveRequests,
  approvals,
  loading,
  selectedDate,
  activeTab,
  toastr,
  requestHover,
  isSubmitting,
  menuDrawer,
  actionDrawer,
  windowWidth,
  administration,
  monitoring,
  admin
});
