import { handleActions } from 'redux-actions';
import {
  receiveAdmins,
  clearUsers,
  superAdminSearchUsers,
  superAdminSearchUsersSuccess
} from '@/store/actions/adminActions';
import { makeUpdaterReducer } from './helpers/reducerHelpers';

const initialState = {
  admin: null,
  users: {
    isFetching: false,
    results: []
  }
};

const actionHandlers = {
  [receiveAdmins]: makeUpdaterReducer('admins'),
  [superAdminSearchUsers]: state => {
    return {
      ...state,
      isFetching: true
    };
  },
  [superAdminSearchUsersSuccess]: (state, action) => {
    const users = {
      isFetching: false,
      results: [...action.payload]
    };

    return {
      ...state,
      users
    };
  },
  [clearUsers]: state => {
    const users = {
      isFetching: false,
      results: []
    };

    return {
      ...state,
      users
    };
  }
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
