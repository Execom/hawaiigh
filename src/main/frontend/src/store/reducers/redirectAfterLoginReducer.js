import { handleActions } from 'redux-actions';
import {
  addRedirectAfterLoginRoute,
  removeRedirectAfterLoginRoute
} from '../actions/redirectAfterLoginActions';

const initialState = null;

const actionHandlers = {
  [addRedirectAfterLoginRoute]: (state, action) => action.payload,
  [removeRedirectAfterLoginRoute]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
