import { handleActions } from 'redux-actions';
import { receiveAudit } from '../actions/auditActions';
import { navigateOut } from '../actions/navigateActions';

const initialState = {};

const actionHandlers = {
  [receiveAudit]: (state, action) => {
    return {
      ...state,
      [action.payload.type]: action.payload.audit
    };
  },
  [navigateOut]: () => initialState
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
