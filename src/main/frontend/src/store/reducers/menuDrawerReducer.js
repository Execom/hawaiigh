import { handleActions } from 'redux-actions';
import { closeDrawer, openMenuDrawer } from '../actions/drawerActions';

const initialState = false;

const actionHandlers = {
  [openMenuDrawer]: state => true,
  [closeDrawer]: state => false
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
