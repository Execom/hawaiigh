import { combineReducers } from 'redux';
import year from './yearReducer';
import years from './yearsReducer';

export default combineReducers({
  year,
  years
});
