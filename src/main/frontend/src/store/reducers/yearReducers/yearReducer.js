import { handleActions } from 'redux-actions';
import { clearStore } from '../../actions/clearStoreActions';
import { navigateOut } from '../../actions/navigateActions';
import { receiveYear, resetYear } from '../../actions/yearActions';

const initialState = null;

const actionHandlers = {
  [receiveYear]: (state, action) => action.payload,
  [navigateOut]: () => null,
  [resetYear]: () => null,
  [clearStore]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
