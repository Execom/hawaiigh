import { handleActions } from 'redux-actions';
import { clearStore } from '../../actions/clearStoreActions';
import { navigateOut } from '../../actions/navigateActions';
import { receiveYears } from '../../actions/yearActions';

const initialState = {
  results: [],
  last: false
};

const actionHandlers = {
  [receiveYears]: (state, { payload }) => {
    return {
      ...state,
      results: payload.first
        ? payload.content
        : [...state.results, ...payload.content],
      last: payload.last,
      page: payload.number
    };
  },
  [navigateOut]: () => ({
    results: [],
    last: false
  }),
  [clearStore]: () => ({
    results: [],
    last: false
  })
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
