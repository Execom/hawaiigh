import { handleActions } from 'redux-actions';
import { isSubmitted, isSubmitting } from '../actions/isSubmittingActions';

const initialState = false;

const actionHandlers = {
  [isSubmitting]: (state, action) => true,
  [isSubmitted]: () => false
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
