export const makeUpdaterReducer = propName => (state, action) => ({
  ...state,
  [propName]: action.payload
});

export const getResults = (state, payload) =>
  payload.first ? payload.content : [...state.results, ...payload.content];
