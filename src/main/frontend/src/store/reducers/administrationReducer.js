import { handleActions } from 'redux-actions';
import {
  setShouldTableScrollToTop,
  setIsFabVisible
} from '../actions/administrationActions';
import { navigateOut } from '../actions/navigateActions';

const initialState = {
  shouldTableScrollToTop: false,
  isFabVisible: true
};

const actionHandlers = {
  [setShouldTableScrollToTop]: (state, action) => ({
    ...state,
    shouldTableScrollToTop: action.payload
  }),
  [setIsFabVisible]: (state, action) => ({
    ...state,
    isFabVisible: action.payload
  }),
  [navigateOut]: (state, action) => ({
    ...state,
    isFabVisible: true
  })
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
