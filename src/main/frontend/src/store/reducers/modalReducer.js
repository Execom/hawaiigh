import { handleActions } from 'redux-actions';
import _get from 'lodash/get';
import {
  closeModal,
  openConfirmationModal,
  openEmployeeRestoreModal,
  openEmployeeDeleteModal,
  openEmployeeCancelDeleteModal,
  openHandleRequestModal,
  openInfoModal,
  openAllowanceCalendarView,
  openRequestModal,
  openTeamRequestsModal,
  openInputModal,
  openAddAdminModal
} from '@/store/actions/modalActions';

const initialState = {
  type: null,
  open: false,
  payload: {}
};

const actionHandlers = {
  [openRequestModal]: (state, { payload }) => {
    return {
      ...state,
      type: 'requestModal',
      open: true,
      payload: {
        selectedDay: _get(payload, 'day'),
        publicHoliday: _get(payload, 'publicHoliday'),
        isWeekend: _get(payload, 'isWeekend'),
        type: _get(payload, 'type')
      }
    };
  },
  [openConfirmationModal]: (state, action) => {
    const {
      payload: { clickAction, values, message }
    } = action;

    return {
      ...state,
      type: 'confirmationModal',
      open: true,
      payload: {
        clickAction,
        values,
        message
      }
    };
  },
  [openEmployeeRestoreModal]: (state, action) => {
    const {
      payload: { clickAction, values, teams, options }
    } = action;

    return {
      ...state,
      type: 'employeeRestoreModal',
      open: true,
      payload: {
        clickAction,
        values,
        teams,
        options: options || {}
      }
    };
  },
  [openEmployeeDeleteModal]: (state, action) => {
    const {
      payload: { clickAction, values, message }
    } = action;

    return {
      ...state,
      type: 'employeeDeleteModal',
      open: true,
      payload: {
        clickAction,
        values,
        message
      }
    };
  },
  [openEmployeeCancelDeleteModal]: (state, action) => {
    const {
      payload: { clickAction, values, message }
    } = action;

    return {
      ...state,
      type: 'employeeCancelDeleteModal',
      open: true,
      payload: {
        clickAction,
        values,
        message
      }
    };
  },
  [openHandleRequestModal]: (state, action) => {
    return {
      ...state,
      type: 'handleRequestModal',
      open: !state.open,
      payload: {
        leaveRequest: action.payload
      }
    };
  },
  [openInfoModal]: (state, action) => {
    return {
      ...state,
      type: 'infoModal',
      open: !state.open,
      payload: {
        message: action.payload
      }
    };
  },
  [openAllowanceCalendarView]: (state, action) => {
    const {
      payload: { message, year }
    } = action;

    return {
      ...state,
      type: 'allowanceCalendarModal',
      open: true,
      payload: {
        year,
        message
      }
    };
  },
  [openTeamRequestsModal]: (state, action) => {
    return {
      ...state,
      type: 'teamRequestsModal',
      open: !state.open,
      payload: action.payload
    };
  },
  [closeModal]: (state, action) => {
    return {
      ...state,
      type: action.payload,
      open: false
    };
  },
  [openInputModal]: (state, action) => {
    const {
      payload: { clickAction, values, message, insertKey, initialValues }
    } = action;

    return {
      ...state,
      type: 'inputModal',
      open: true,
      payload: {
        clickAction,
        values,
        message,
        insertKey,
        initialValues
      }
    };
  },
  [openAddAdminModal]: (state, action) => {
    return {
      ...state,
      type: 'addAdminModal',
      open: !state.open,
      payload: {
        message: action.payload
      }
    };
  }
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
