import { handleActions } from 'redux-actions';
import {
  recieveHardwareMonitoringData,
  recieveHttpEndpointsMonitoringData,
  recieveOldestTimestamp
} from '@/store/actions/monitoringActions';
import { makeUpdaterReducer } from '../helpers/reducerHelpers';

const initialState = {
  hardware: null,
  httpEndpoints: null,
  oldestTimestamp: null
};

const actionHandlers = {
  [recieveHardwareMonitoringData]: makeUpdaterReducer('hardware'),
  [recieveHttpEndpointsMonitoringData]: makeUpdaterReducer('httpEndpoints'),
  [recieveOldestTimestamp]: makeUpdaterReducer('oldestTimestamp')
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
