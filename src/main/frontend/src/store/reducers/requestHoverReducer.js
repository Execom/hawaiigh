import { handleActions } from 'redux-actions';
import { requestHover, requestUnhover } from '../actions/requestHoverActions';

const initialState = null;

const actionHandlers = {
  [requestHover]: (state, action) => action.payload,
  [requestUnhover]: () => null
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
