import { handleActions } from 'redux-actions';
import { loadingEnd, loadingStart } from '../actions/loadingActions';

const initialState = 0;

const actionHandlers = {
  [loadingStart]: state => state + 1,
  [loadingEnd]: state => state - 1
};

const reducer = handleActions(actionHandlers, initialState);

export default reducer;
