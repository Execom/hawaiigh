import styled, { css } from 'styled-components';
import { TableCell as TableCellBasic } from '@/components/common/Table';

export const SeparatorRow = styled.tr`
  height: ${props => props.height || '5px'};
`;

export const TableRow = styled.tr`
  cursor: ${props => (props.clickable ? 'pointer' : 'default')};
  background: white;

  &:hover {
    background: #ececec !important;
  }
`;

const sharedStyle = (color, notImportant) => css`
  @media (min-width: 991px) {
    border-top: 1px solid ${color} ${!notImportant && '!important'};
    border-bottom: 1px solid ${color} ${!notImportant && '!important'};

    &:first-of-type {
      border-top-left-radius: 5px;
      border-bottom-left-radius: 5px;
      border-left: 1px solid ${color};
    }

    &:last-of-type {
      border-top-right-radius: 5px;
      border-bottom-right-radius: 5px;
      border-right: 1px solid ${color};
    }
  }
`;

export const TableCell = styled(TableCellBasic)`
  ${props => sharedStyle(props.color)}
  vertical-align: middle !important;

  @media (max-width: 991px) {
    width: 100%;
  }
`;

export const TableHeaderWrapper = styled.div`
  padding-right: 13px;
`;

export const TableHeader = styled.div`
  ${props => sharedStyle(props.color, props.borderless)}
  font-size: 18px;
  height: 50px;
  flex: 1 1 0;

  &:first-of-type {
    margin-left: ${props =>
      props.employeesTabAlignment || props.allowanceTabAlignment
        ? '23px'
        : '0'};
  }

  &:last-of-type {
    margin-right: ${props => (props.allowanceTabAlignment ? '70px' : '0')};
  }

  @media (max-width: 992px) {
    height: ${props => (props.showOnMobile ? '50px' : '20px')};
    display: none !important;
  }
`;

export const TableHeaderNative = styled.th`
  ${props => sharedStyle(props.color, props.borderless)}
  font-size: 18px;
  height: 50px;
  flex: 1 1 0;

  @media (max-width: 992px) {
    display: none;
  }
`;

// needs to override bootstraps class
export const TableDataYear = styled.td`
  padding: 0 !important;
`;
