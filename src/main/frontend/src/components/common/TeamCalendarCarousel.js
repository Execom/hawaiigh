import React from 'react';
import styled from 'styled-components';
import { ifProp } from 'styled-tools';
import { ArrowSelect } from '@/pages/dashboard/components/styled';
import ArrowDown from '@/img/icons/arrow-down.svg';
import { formatMonth } from '@/store/helperFunctions';

export const CarouselControlButton = styled.button`
  background-color: transparent;
  color: #fb4b4f;
  cursor: pointer;
  transition: 200ms ease;
  color: ${ifProp('year', '#fb4b4f', '#a5999a')};
  pointer-events: ${ifProp('year', 'auto', 'none')};
  &:hover {
    transform: scale(1.5);
  }
`;

export const CarouselContainer = styled.div`
  background-color: #d3d3d3;
  border: none;
  color: #fb4b4f;
  min-height: 41px;

  &.bg-col {
    background-color: #ededed;
  }
`;

export const TeamCalendarCarousel = ({
  selectedYear,
  yearsWithAllowance,
  selectedMonth,
  monthsWithAllowance,
  handleYearChange,
  handleMonthChange
}) => {
  const handleChange = (e, value) =>
    value === selectedYear
      ? handleYearChange(e.target.value)
      : handleMonthChange(e.target.value);

  return (
    <CarouselContainer className="rounded d-flex align-items-center justify-content-center p-2 my-3">
      <div className="position-relative">
        <select
          className="select-custom select-year pl-2 pr-5 py-1 rounded d-none d-md-flex"
          onChange={handleChange}
          value={selectedYear}
        >
          {yearsWithAllowance.map(year => (
            <option key={year} value={year}>
              {year}
            </option>
          ))}
        </select>
        <ArrowSelect src={ArrowDown} alt="arrow down" />
      </div>
      <div className="position-relative">
        <select
          onChange={handleChange}
          value={selectedMonth}
          className="select-custom mb-md-0 d-md-none pl-2 pr-5 py-1 rounded"
        >
          {monthsWithAllowance.map(month => (
            <option key={month} value={month}>
              {formatMonth(month)}
            </option>
          ))}
        </select>
        <ArrowSelect src={ArrowDown} alt="arrow down" />
      </div>
    </CarouselContainer>
  );
};
