import React, { Fragment } from 'react';
import styled from 'styled-components';

export const AppVersionWrapperSidebar = styled.div`
  text-align: center;
  color: #ffffff33;
  font-family: monospace;
  white-space: nowrap;

  @media (max-height: 604px) {
    margin-inline-start: auto;
    padding: 11px;
  }
`;

const AppVersion = () => {
  const version = process.env.REACT_APP_VERSION;

  if (!version)
    console.warn(
      'REACT_APP_VERSION could not be found in your .env. Please add "REACT_APP_VERSION=$npm_package_version" to your .env file.'
    );

  return <Fragment>{version ? `v${version}` : 'unknown version'}</Fragment>;
};

export default AppVersion;
