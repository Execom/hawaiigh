import React from 'react';
import styled from 'styled-components';

const HamburgerWrapper = styled.div`
  height: 50px;
  width: 45px;
  cursor: pointer;
`;

const ToggleButton = styled.div`
  height: 2px;
  width: 25px;
  background-color: ${props => props.color || '#fff'};
  top: 50%;
  left: 0;
  transition: all 0.3s ease-in-out;

  @media (max-width: 991px) {
    width: 20px;
  }

  &:before {
    content: '';
    height: 2px;
    width: 25px;
    box-shadow: 0 -10px 0 0 ${props => props.color || '#fff'};
    position: absolute;
    top: 0;
    left: 0;
    transition: all 0.3s ease-in-out;

    @media (max-width: 991px) {
      top: 3px;
      width: 20px;
    }
  }
  &:after {
    content: '';
    height: 2px;
    width: 25px;
    box-shadow: 0 10px 0 0 ${props => props.color || '#fff'};
    position: absolute;
    top: 0;
    left: 0;
    transition: all 0.3s ease-in-out;

    @media (max-width: 991px) {
      top: -3px;
      width: 20px;
    }
  }
`;

export const ToggleSidebarButton = ({ click, color }) => {
  return (
    <HamburgerWrapper onClick={click}>
      <ToggleButton color={color} className="m-auto position-relative" />
    </HamburgerWrapper>
  );
};
