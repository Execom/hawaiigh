import styled from 'styled-components';

export const TableHeader = styled.thead`
  background-color: #ededed;

  th {
    color: #fb4b4f;
    font-size: 18px;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;

export const TableCell = styled.td`
  &:before {
    content: attr(data-label);
    display: flex;
    align-items: center;
    ${props => (props.hideCellTitle ? 'display: none' : '')}

    @media (min-width: 992px) {
      display: none;
    }
  }
`;

export const TableRow = styled.tr`
  cursor: pointer;

  &:nth-child(even) {
    background-color: rgba(0, 0, 0, 0.075);
  }
`;

export const TableFooter = styled.tfoot`
  @media (max-width: 992px) {
    display: none;
  }
`;
