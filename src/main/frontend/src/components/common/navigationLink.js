import styled from 'styled-components';

export const NavigationLink = styled.span`
  background: transparent;
  transition: 0.2s ease-in-out;
  color: #fff;
  font-weight: 500;
  max-width: 200px;

  &.logout {
    @media (min-width: 992px) {
      display: none;
    }
  }

  ${props =>
    props.userRole === 'HR_MANAGER' &&
    `@media (max-width: 1300px) {
      font-size: 14px; 
    } 
    
    @media (max-width: 1129px) { 
      font-size: 12px; 
    }
    
    @media (max-width: 1038px) { 
      font-size: 10px; 
    }
    
    @media (max-width: 992px) { 
      font-size: 15px; 
    }`}
`;
