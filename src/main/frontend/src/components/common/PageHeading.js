import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { HR_MANAGER } from '@/helpers/enum';
import { clearEmployees, searchEmployees } from '@/store/actions/userActions';
import { getSearchEmployees, getUser } from '@/store/selectors';
import EmployeeSearchResults from '@/components/search-dropdown/search-results/EmployeeSearchResults';
import SearchDropdown from '@/components/search-dropdown/SearchDropdown';
import Button from '@/components/common/Button';

const PageHeadingContainer = styled.div`
  max-height: 55px;
  margin-bottom: 15px;
`;

const PageHeadingTitle = styled.h1`
  font-size: 20px;
  font-weight: 600;
`;

class PageHeading extends Component {
  renderSearchBar = (location, user) => {
    const { userRole, isApprover } = user;

    if (
      (userRole === HR_MANAGER || isApprover) &&
      location &&
      (location.includes('dashboard') || location.includes('requests'))
    ) {
      return (
        <div className="flex-lg-grow-1 pl-md-4 d-flex justify-content-center justify-content-md-end order-1 order-md-0 pr-lg-4 col-lg-5">
          <SearchDropdown
            width="400px"
            searchResults={this.props.employees}
            searchAction={searchEmployees}
            resetAction={clearEmployees}
          >
            {(inputReference, resetInput) => (
              <EmployeeSearchResults
                resetInput={resetInput}
                location={this.props.location}
                inputReference={inputReference}
                employees={this.props.employees}
              />
            )}
          </SearchDropdown>
        </div>
      );
    }
  };

  render() {
    const {
      title,
      buttonTitle,
      buttonAction,
      location,
      user,
      mobileHidden
    } = this.props;

    return (
      <PageHeadingContainer
        className={`${
          mobileHidden ? 'd-none' : 'd-block'
        } d-lg-flex justify-content-between pt-4 pl-0 pr-lg-4 pr-md-0 align-items-end`}
      >
        <div className="d-flex flex-column flex-md-row justify-content-between text-center align-items-end p-lg-0 col-lg-12">
          <PageHeadingTitle className="d-none d-lg-flex text-center text-lg-left ml-lg-4 mb-3 mb-lg-0">
            {title}
          </PageHeadingTitle>
          {user && this.renderSearchBar(location, user)}
          {buttonTitle && (
            <div className="d-none d-md-flex flex-shrink-0 justify-content-end col-3 request-button">
              <Button
                title={buttonTitle}
                click={buttonAction}
                styling={{ fontColor: 'white' }}
                className="mb-3 mb-md-0"
              />
            </div>
          )}
        </div>
      </PageHeadingContainer>
    );
  }
}
const mapStateToProps = state => ({
  employees: getSearchEmployees(state),
  user: getUser(state)
});

export default connect(mapStateToProps)(PageHeading);
