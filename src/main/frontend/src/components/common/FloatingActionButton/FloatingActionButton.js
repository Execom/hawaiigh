import React, { Component } from 'react';
import OutsideClickHandler from 'react-outside-click-handler';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Add from '@/img/icons/add.svg';
import BonusIcon from '@/img/icons/bonus_ss.svg';
import LeaveIcon from '@/img/icons/palm_tree.svg';
import SicknessIcon from '@/img/icons/sickness_ss.svg';
import { openRequestModal } from '@/store/actions/modalActions';
import {
  Fab,
  FabItem,
  FabWrapper
} from '@/components/common/FloatingActionButton/styled';
import { getAdministration } from '@/store/selectors';

class FloatingActionButton extends Component {
  state = {
    isOpen: false
  };

  toggleFab = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  closeFab = () => {
    if (this.state.isOpen) {
      this.setState({
        isOpen: false
      });
    }
  };

  openModal = type => {
    this.props.openRequestModal({ type });
    this.closeFab();
  };

  render() {
    const { isOpen } = this.state;

    return (
      <FabWrapper
        className="d-lg-none d-block position-relative"
        hide={!this.props.isFabVisible}
      >
        <OutsideClickHandler onOutsideClick={this.closeFab}>
          <Fab
            onClick={this.props.onClick || this.toggleFab}
            className="p-2"
            active={isOpen}
          >
            <img src={Add} alt="plus" />
          </Fab>
          {!this.props.onClick && (
            <div>
              <FabItem
                onClick={() => this.openModal('isBonus')}
                active={isOpen}
              >
                <img src={BonusIcon} alt="bonus icon" />
              </FabItem>
              <FabItem
                onClick={() => this.openModal('isSickness')}
                active={isOpen}
              >
                <img src={SicknessIcon} alt="sickness icon" />
              </FabItem>
              <FabItem
                onClick={() => this.openModal('isLeave')}
                active={isOpen}
              >
                <img src={LeaveIcon} alt="leave icon" />
              </FabItem>
            </div>
          )}
        </OutsideClickHandler>
      </FabWrapper>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      openRequestModal
    },
    dispatch
  );

export default connect(
  state => ({
    isFabVisible: getAdministration(state).isFabVisible
  }),
  mapDispatchToProps
)(FloatingActionButton);
