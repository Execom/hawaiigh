import styled from 'styled-components';

export const FabWrapper = styled.div`
  opacity: ${props => (props.hide ? '0' : '1')};
  ${props => (props.hide ? 'pointer-events: none' : '')};
  transition: opacity 0.2s;
  z-index: 10;
`;

export const Fab = styled.div`
  display: flex;
  position: fixed;
  border-radius: 50%;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  transition: 0.4s;
  background: #fb4b4f;
  width: 50px;
  height: 50px;
  bottom: 10px;
  right: 10px;
  z-index: 10;

  img {
    transition: 0.4s;
    transform: rotateZ(${props => (props.active ? '45deg' : '0')});
  }
`;

export const FabItem = styled.div`
  display: flex;
  position: fixed;
  border-radius: 50%;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  transition: 0.4s;
  width: 40px;
  height: 40px;
  bottom: 15px;
  right: 15px;
  z-index: 9;
  padding: 4px;
  background: white;

  ${({ active }) =>
    active &&
    `
  &:nth-child(1) {
      bottom: 80px;
    }
    &:nth-child(2) {
        bottom: 140px;
    }
    &:nth-child(3) {
        bottom: 200px;
    }
    `}
`;
