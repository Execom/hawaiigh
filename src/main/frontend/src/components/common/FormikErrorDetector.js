import { Component } from 'react';
import _isEqual from 'lodash/isEqual';

export default class FormikErrorDetector extends Component {
  componentDidUpdate(prevProps) {
    const { formikProps, onError } = this.props;

    if (!_isEqual(prevProps.formikProps, formikProps)) {
      if (formikProps.isSubmitting && Object.keys(formikProps.errors).length) {
        onError(Object.values(formikProps.errors)[0]);
      }
    }
  }

  render() {
    return null;
  }
}
