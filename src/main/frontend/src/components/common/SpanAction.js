import styled from 'styled-components';

export default styled.span`
  cursor: pointer;
  color: #dc3545;
  transition: 0.2s ease-in-out;

  &:hover {
    color: #ea757f;
  }
`;
