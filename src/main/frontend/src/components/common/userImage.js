import styled from 'styled-components';
import { ifProp, prop } from 'styled-tools';

export const UserImage = styled.div`
  border-radius: 50%;
  width: ${prop('size', '50px')};
  height: ${prop('size', '50px')};
  overflow: hidden;
  display: inline-flex;
  justify-content: center;
  background-image: url('${prop(
    'image',
    'https://openclipart.org/download/247324/abstract-user-flat-1.svg'
  )}');
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border: ${ifProp('isSmall', '1px solid white', '5px solid white')};
  border: ${ifProp('borderBlack', '1px solid black', '1px solid white')};
  ${props =>
    props.position !== 0 && props.isInTeamRow ? `margin-left: -15px;` : ''};
  ${props =>
    props.position === -1 && props.isInTeamRow ? `margin-left: 5px;` : ''};
  z-index: ${props => 5 - props.position};

  @media(max-width: 991px) {
    ${props => (props.isInSidebar ? 'width: 80px; height: 80px;' : '')};
  }
`;
