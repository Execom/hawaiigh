import React, { Component } from 'react';
import styled from 'styled-components';
import Calendar from '../../img/icons/calendar.svg';

const StyledImage = styled.img`
  width: 15px;
  top: 3px;
  right: 5px;

  @media (min-width: 576px) {
    right: 10px;
  }
`;

const CustomInputStyled = styled.button`
  border-bottom: 1px solid;
  padding: 3px;
`;

class CustomInput extends Component {
  render() {
    return (
      <CustomInputStyled
        className="pl-2 w-100 text-left bg-white position-relative"
        onClick={this.props.onClick}
      >
        {this.props.value}
        <StyledImage className="position-absolute" src={Calendar} alt="" />
      </CustomInputStyled>
    );
  }
}

export default CustomInput;
