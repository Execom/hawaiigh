import styled from 'styled-components';
import { ifProp } from 'styled-tools';

export const IconsList = styled.img`
  cursor: pointer;
  transition: 0.2s ease-in-out;
  opacity: ${ifProp('isSelected', '1', '.4')};
  &:hover {
    opacity: 1;
  }
`;
