import _get from 'lodash/get';
import React from 'react';
import styled from 'styled-components';
import { ifProp } from 'styled-tools';

const ButtonContainer = styled.button`
  background-color: ${props => _get(props.styling, 'color') || '#fb4b4f'}
  border-radius: ${ifProp('styling.rounded', '20px', '5px')};
  color: ${props => _get(props.styling, 'fontColor') || 'black'};
  padding: 5px 10px 5px 10px;
  transition: font-size 100ms ease;
  border: 1px solid white;
  transition: 200ms ease;
  cursor: ${props => (props.disabled ? 'cursor' : 'pointer')};
  opacity: ${props => (props.disabled ? '0.4' : '1')};

  &:hover {
    border-color: ${props => (props.disabled ? 'transparent' : '#323234')};
  }
`;

const Button = props => {
  return (
    <ButtonContainer
      className={props.className}
      styling={props.styling}
      type={props.type}
      onClick={props.click}
      disabled={_get(props, 'disabled')}
    >
      {props.title}
    </ButtonContainer>
  );
};

export default Button;
