import styled from 'styled-components';

export const CapitalizedText = styled.span`
  text-transform: lowercase;
  display: inline-block;

  &:first-letter {
    text-transform: uppercase;
  }
`;
