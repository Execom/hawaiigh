import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Chip } from '@material-ui/core';
import styled from 'styled-components';
import { toastrError } from '../../store/sagas/helpers/toastrHelperSaga';
import { validateEmail } from '../../helpers';

const ChipWrapper = styled.div`
  margin: 2px 2px 2px 0;

  .MuiChip-root {
    height: 23px;

    .MuiChip-deleteIcon {
      width: 20px;
      height: 20px;
    }
  }

  &.chip-error .MuiChip-root {
    border: 1px solid red;
  }
`;

class InputChips extends Component {
  state = {
    value: '',
    chips: this.props.defaultValue ? this.props.defaultValue.split(',') : []
  };

  sanitize = s => s.replace(/,/g, '').replace(/ /g, '');

  addChip = chip => {
    const { chips } = this.state;

    this.setState({ chips: [...chips, chip], value: '' }, this.handleChange);
  };

  addChipAttempt = e => {
    const chip = e.target.value;
    const { chips } = this.state;
    const sanitizedChip = this.sanitize(chip);

    if (
      chips.includes(sanitizedChip) ||
      chips.includes(`${sanitizedChip}@execom.eu`) ||
      !sanitizedChip.length
    )
      return this.setState({ value: '' });

    if (validateEmail(sanitizedChip)) return this.addChip(sanitizedChip);

    if (validateEmail(`${sanitizedChip}@execom.eu`))
      return this.addChip(`${sanitizedChip}@execom.eu`);

    e.preventDefault();
    this.addChip(sanitizedChip);
  };

  removeChip = i => {
    const { chips } = this.state;

    this.setState(
      {
        chips: chips.slice(0, i).concat(chips.slice(i + 1, chips.length))
      },
      this.handleChange
    );
  };

  handleChange = () => this.props.onChange(this.state.chips.join(','));

  onKeyDown = e => {
    const {
      key,
      target: { value }
    } = e;

    if (key === ',' || key === ' ') {
      this.addChipAttempt(e);
    } else if (key === 'Backspace' && !/\S/.test(value)) {
      this.removeChip(this.state.chips.length - 1);
    }
  };

  onChange = e => {
    this.setState({ value: this.sanitize(e.target.value) });
  };

  render() {
    return (
      <div className="d-flex justify-content-flex-start flex-wrap">
        <Fragment>
          {this.state.chips.map((chip, i) => (
            <ChipWrapper
              key={chip}
              className={!validateEmail(chip) ? 'chip-error' : ''}
            >
              <Chip label={chip} onDelete={() => this.removeChip(i)} />
            </ChipWrapper>
          ))}
        </Fragment>
        <input
          className="border-0 bg-transparent d-inline-block flex-grow-1 flex-shrink-1 auto p-1"
          onChange={this.onChange}
          onBlur={this.addChipAttempt}
          onKeyDown={this.onKeyDown}
          value={this.state.value}
        />
      </div>
    );
  }
}

export default connect(
  null,
  {
    toastrError
  }
)(InputChips);
