import React from 'react';
import styled from 'styled-components';
import { Tooltip } from '@material-ui/core';

const ButtonContainer = styled.button`
  background-color: #fb4b4f;
  border-radius: 50%;
  color: white;
  padding: 5px;
  transition: 0.1s ease-in-out;
  cursor: pointer;
  line-height: 0.6;
  font-size: 24px;

  &:hover {
    border-color: #323234;
  }
`;

export default props => {
  return (
    <Tooltip title="Create" placement="top">
      <ButtonContainer className={props.className} onClick={props.click}>
        +
      </ButtonContainer>
    </Tooltip>
  );
};
