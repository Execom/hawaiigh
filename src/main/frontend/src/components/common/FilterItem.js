import _replace from 'lodash/replace';
import React from 'react';
import styled from 'styled-components';

const FilterContainer = styled.div`
  label {
    cursor: pointer;
    transition: color 200ms ease-in-out;
    margin-left: 10px;

    &:before {
      content: '';
      width: 10px;
      height: 10px;
      border-radius: 50%;
      background-color: #fb4b4f;
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%) scale3d(1, 1, 1);
      transition: all 300ms cubic-bezier(0.4, 0, 0.2, 1);
      opacity: 0;
      z-index: -1;
    }

    &:after {
      content: '';
      width: 17px;
      height: 17px;
      border: 1px solid grey;
      background-color: #fff;
      background-repeat: no-repeat;
      background-position: 2px 3px;
      background-size: 14px;
      border-radius: 50%;
      z-index: 2;
      position: absolute;
      left: -10px;
      top: 50%;
      transform: translateY(-50%);
      cursor: pointer;
      transition: all 200ms ease-in-out;
    }
  }

  input:checked ~ label {
    &:after {
      background-color: #fb4b4f;
      border-color: white;
    }
  }
`;

export const FilterItem = props => (
  <FilterContainer className="rounded d-inline-block">
    <input
      className="position-absolute invisible"
      onChange={props.action}
      value={props.filter.toUpperCase()}
      id={props.filter}
      name={props.filter}
      type="checkbox"
    />
    <label
      className="text-capitalize rounded position-relative d-block px-3 py-1"
      htmlFor={props.filter}
    >
      {_replace(props.filter, '_', ' ')}
    </label>
  </FilterContainer>
);
