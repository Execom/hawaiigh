import React, { Component } from 'react';
import styled from 'styled-components';

const RadioLabel = styled.label`
  font-size: 14px;

  &:before {
    content: '';
    border-radius: 100%;
    border: 1px solid;
    display: inline-block;
    width: 15px;
    height: 15px;
    position: relative;
    top: -0.2em;
    margin-right: 10px;
    vertical-align: top;
    cursor: pointer;
    text-align: center;
    transition: all 250ms ease;
    flex: 0 0 auto;
  }
  &:empty {
    &:before {
      margin-right: 0;
    }
  }
`;

const RadioButton = styled.input`
  position: absolute;
  opacity: 0;
  &:checked {
     + ${RadioLabel} {
      &:before {
        background-color: #E45052;
        box-shadow: inset 0 0 0 2px #f4f4f4;
      }
    }
  }
  &:focus {
     + ${RadioLabel} {
      &:before {
        outline: none;
      }
    }
  }
  &:disabled {
     + ${RadioLabel} {
      &:before {
        box-shadow: inset 0 0 0 4px #f4f4f4;
        border-color: darken(#f4f4f4, 25%);
        background: darken(#f4f4f4, 25%);
      }
    }
  }
}
`;

export default class Radio extends Component {
  render() {
    return (
      <div className="d-flex px-1 justify-content-between justify-content-lg-end">
        {this.props.options.map((option, index) => (
          <div key={option.value} className="px-2">
            <RadioButton
              onClick={option.action}
              id={option.value}
              name="radio"
              type="radio"
              defaultChecked={index === 0}
            />
            <RadioLabel className="d-flex" htmlFor={option.value}>
              {option.label}
            </RadioLabel>
          </div>
        ))}
      </div>
    );
  }
}
