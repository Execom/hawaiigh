import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { getAdministration } from '../../store/selectors';
import { setIsFabVisible } from '../../store/actions/administrationActions';

class ScrollDetector extends React.Component {
  scrollRef = React.createRef();
  state = {
    scrollY: 0
  };

  handleScroll = e => {
    const scrollPosition = e.currentTarget.scrollTop;

    if (
      scrollPosition > this.state.scrollPosition &&
      this.props.administration.isFabVisible
    ) {
      this.props.setIsFabVisible(false);
    } else if (
      scrollPosition < this.state.scrollPosition &&
      !this.props.administration.isFabVisible
    ) {
      this.props.setIsFabVisible(true);
    }

    this.setState({
      scrollPosition
    });
  };

  render() {
    return (
      <Fragment>
        {React.cloneElement(this.props.component, {
          onScroll: this.handleScroll
        })}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  administration: getAdministration(state)
});

const mapDispatchToProps = {
  setIsFabVisible
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScrollDetector);
