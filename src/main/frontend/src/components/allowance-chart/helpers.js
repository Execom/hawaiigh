import { makeAllowanceInDays } from '@/pages/administration/helpers';

const colorPending = '#fef2b0';
const colorTaken = '#fda5a7';
const colorRemaining = '#a6d7a8';

const pending = 'Pending';
const taken = 'Taken';
const remaining = 'Remaining';
const total = 'Total';

const datasetLabel = 'Allowance';

const defaultFontFamily = 'Montserrat';
const defaultFontColor = '#212529';

const datalabels = {
  font: {
    size: 14,
    family: defaultFontFamily,
    weight: 'bold'
  },
  color: defaultFontColor,
  display: function(context) {
    const index = context.dataIndex;
    const value = context.dataset.data[index];

    return value !== 0;
  }
};

export const constructChartData = nextProps => {
  const { activeTab, allowance } = nextProps;
  const allowanceInDays = makeAllowanceInDays(allowance);
  const {
    pendingTraining,
    pendingAnnual,
    takenAnnual,
    takenTraining,
    remainingTraining,
    remainingAnnual,
    totalTraining,
    totalAnnual,
    sickness
  } = allowanceInDays;

  switch (activeTab) {
    case 'LEAVE':
      return {
        chartData: {
          labels: [pending, taken, remaining],
          datasets: [
            {
              label: datasetLabel,
              data: [pendingAnnual, takenAnnual, remainingAnnual],
              backgroundColor: [colorPending, colorTaken, colorRemaining]
            }
          ]
        },
        innerLabel: total,
        innerValue: totalAnnual,
        datalabels
      };
    case 'TRAINING':
      return {
        chartData: {
          labels: [pending, taken, remaining],
          datasets: [
            {
              label: datasetLabel,
              data: [pendingTraining, takenTraining, remainingTraining],
              backgroundColor: [colorPending, colorTaken, colorRemaining]
            }
          ]
        },
        innerLabel: total,
        innerValue: totalTraining,
        datalabels
      };
    case 'SICKNESS':
      return {
        chartData: {
          labels: [taken],
          datasets: [
            {
              label: datasetLabel,
              data: [1],
              backgroundColor: [colorTaken]
            }
          ]
        },
        innerLabel: taken,
        innerValue: sickness,
        datalabels: { display: false }
      };
    default:
      return;
  }
};

export const drawDoughnutCenterText = chart => {
  //Get ctx from string
  const ctx = chart.chart.ctx;

  //Get options from the center object in options
  const centerConfig = chart.config.options.elements.center;
  const innerLabel = centerConfig.innerLabel;
  const innerValue = centerConfig.innerValue;

  //Calculate and set font settings
  const centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
  const centerYLabel =
    (chart.chartArea.top + chart.chartArea.bottom) / 2 / 1.11;
  const centerYValue =
    ((chart.chartArea.top + chart.chartArea.bottom) / 2) * 1.03;
  ctx.textAlign = 'center';
  ctx.textBaseline = 'middle';
  ctx.fillStyle = defaultFontColor;
  ctx.font = '35px ' + defaultFontFamily;

  //Draw value in center
  ctx.fillText(innerValue, centerX, centerYValue);

  //Draw label above value with lower font
  ctx.font = '12px ' + defaultFontFamily;
  ctx.fillText(innerLabel, centerX, centerYLabel);
};
