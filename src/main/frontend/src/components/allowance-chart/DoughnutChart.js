import React, { Component } from 'react';
import { Doughnut, Chart } from 'react-chartjs-2';
import 'chartjs-plugin-datalabels';
import {
  constructChartData,
  drawDoughnutCenterText
} from '@/components/allowance-chart/helpers';

class DoughnutChart extends Component {
  chartReference = React.createRef();
  state = {
    chartData: {
      labels: [],
      datasets: []
    },
    innerLabel: '',
    innerValue: '',
    datalabels: []
  };

  componentDidMount() {
    Chart.pluginService.register({
      beforeDraw: function(chart) {
        drawDoughnutCenterText(chart);
      }
    });
  }

  static getDerivedStateFromProps(nextProps) {
    return constructChartData(nextProps);
  }

  render() {
    return (
      <Doughnut
        data={this.state.chartData}
        ref={this.chartReference}
        options={{
          maintainAspectRatio: false,
          tooltips: {
            enabled: false
          },
          layout: {
            padding: 0
          },
          title: {
            display: true,
            text: 'Allowance in days',
            fontSize: 18,
            fontFamily: 'Montserrat',
            fontColor: '#212529',
            padding: 25
          },
          legend: {
            display: true,
            position: 'bottom',
            align: 'center',
            labels: {
              fontColor: '#212529',
              fontSize: 14,
              boxWidth: 15,
              fontFamily: 'Source Sans Pro'
            }
          },
          elements: {
            center: {
              innerLabel: `${this.state.innerLabel}`,
              innerValue: `${this.state.innerValue}`
            },
            arc: {
              borderWidth: 0
            }
          },
          plugins: {
            datalabels: { ...this.state.datalabels }
          }
        }}
      />
    );
  }
}

export default DoughnutChart;
