import React, { Component } from 'react';
import { TabButton } from '@/components/tabs/styled';

class ChartTab extends Component {
  onTabClick = () => {
    this.props.handleTabClick(this.props.tabId);
  };

  render() {
    const { label, isActive } = this.props;

    return (
      <TabButton
        className="mx-1 px-0 pb-2 d-block w-100"
        active={isActive}
        onClick={this.onTabClick}
      >
        {label}
      </TabButton>
    );
  }
}

export default ChartTab;
