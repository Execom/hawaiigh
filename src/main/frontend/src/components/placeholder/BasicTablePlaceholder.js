import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import {
  BasicTablePlaceholderWrapper,
  BasicTableRowPlaceholder
} from '@/pages/administration/components/common/styled';

const tableRowCount = 5;

const BasicTablePlaceholder = props => (
  <BasicTablePlaceholderWrapper
    className="position-absolute w-100"
    hasMobileHeader={props.hasMobileHeader}
  >
    {Array(tableRowCount)
      .fill(0)
      .map((_, i) => (
        <BasicTableRowPlaceholder
          className="d-flex flex-column flex-lg-row w-100 bg-white"
          key={i}
          style={{
            opacity: `${(tableRowCount - i) / tableRowCount}`
          }}
        />
      ))}
  </BasicTablePlaceholderWrapper>
);

export default BasicTablePlaceholder;
