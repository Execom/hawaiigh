import React from 'react';
import {
  TablePlaceholderWrapper,
  TableRowPlaceholder,
  TableCellPlaceholderInner,
  TableCellPlaceholderWrapper,
  TableCellPlaceholderAction
} from '@/pages/administration/components/common/styled';

const tableRowCount = 15;

const getTableCellWidthByIndex = (rowIndex, colIndex) =>
  100 + 25 * Math.sin(rowIndex + colIndex);

const TableCellPlaceholder = props => (
  <TableCellPlaceholderWrapper {...props}>
    <div className={`d-flex ${props.index !== 0 ? 'mt-2' : ''} mt-lg-0`}>
      <div className="flex-grow-1 d-block d-lg-none text-left">
        <TableCellPlaceholderInner style={{ width: 120 }} />
      </div>
      {!props.isAction ? (
        <div className="flex-grow-1 text-right text-lg-left">
          <TableCellPlaceholderInner
            className="d-inline-block"
            style={{
              width: `${getTableCellWidthByIndex(
                props.rowIndex,
                props.colIndex
              )}px`
            }}
          />
        </div>
      ) : (
        <div className="text-right flex-grow-1">
          <TableCellPlaceholderAction className="d-inline-block" />
          <TableCellPlaceholderAction className="d-inline-block" />
        </div>
      )}
    </div>
  </TableCellPlaceholderWrapper>
);

const TablePlaceholder = props => (
  <TablePlaceholderWrapper
    className="d-block position-absolute"
    hasMobileHeader={props.hasMobileHeader}
  >
    {Array(tableRowCount)
      .fill(0)
      .map((_, i) => (
        <TableRowPlaceholder
          className="d-flex flex-column flex-lg-row bg-white"
          key={i}
          style={{
            opacity: `${(tableRowCount - i) / tableRowCount}`
          }}
        >
          {Array(props.cellCount)
            .fill(0)
            .map((_, j) => (
              <TableCellPlaceholder
                className="pl-lg-4"
                index={j}
                key={j}
                rowIndex={i}
                colIndex={j}
              />
            ))}
          <TableCellPlaceholder
            className="pl-lg-4"
            isAction
            key={props.cellCount + 1}
          />
        </TableRowPlaceholder>
      ))}
  </TablePlaceholderWrapper>
);

export default TablePlaceholder;
