import styled from 'styled-components';
import { ifProp } from 'styled-tools';

export const UserInfoWrapper = styled.div`
  border-top: 1px solid #c0c0c3;
  box-shadow: 0px -2px 10px 2px rgba(0, 0, 0, 0.35);
  z-index: 1;
  flex: 0 0 auto;
`;

export const NavHeader = styled.header`
  background: #fff;
  z-index: 3;
  flex: 0 0 auto;

  @media (min-width: 992px) {
    background: transparent;
    position: absolute;
    width: 100%;
  }
`;

export const LogoImage = styled.img`
  height: 150px;
  height: 23px;
  align-self: center;
`;

export const NavSpan = styled.span`
  height: ${ifProp('height', '50px', 'auto')};
`;

export const Heading = styled.h5`
  font-size: 20px;
  font-weight: 600;
`;

export const CenteredLogo = styled.div`
  top: 100%;
  left: 54%;
  transform: translate(-100%, -50%);
`;
