import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  clearEmployees,
  searchEmployees
} from '../../store/actions/userActions';
import { getSearchEmployees } from '../../store/selectors';
import EmployeeSearchResults from '../search-dropdown/search-results/EmployeeSearchResults';
import SearchDropdown from '../search-dropdown/SearchDropdown';
import UserInfoExtended from '../UserInfoExtended';
import { UserInfoWrapper } from './styled';

class InformationHeader extends Component {
  renderSearchBar = location => {
    if (location.includes('dashboard') || location.includes('requests')) {
      return (
        <div className="flex-grow-1 pr-4 d-flex justify-content-end mt-4">
          <SearchDropdown
            searchResults={this.props.employees}
            searchAction={searchEmployees}
            resetAction={clearEmployees}
          >
            {(inputReference, resetInput) => (
              <EmployeeSearchResults
                resetInput={resetInput}
                location={this.props.location}
                inputReference={inputReference}
                employees={this.props.employees}
              />
            )}
          </SearchDropdown>
        </div>
      );
    }
  };

  checkIfSearchedEmployee = searchedEmployee => {
    if (searchedEmployee) {
      return this.props.searchedEmployee;
    }

    return this.props.user;
  };

  render() {
    return (
      <UserInfoWrapper className="d-flex align-items-center">
        <UserInfoExtended
          userInfo={this.checkIfSearchedEmployee(this.props.searchedEmployee)}
        />
        <div className="d-flex flex-column flex-grow-1 justify-content-center mt-4 pt-1">
          <p>Team:</p>
          <p>
            {this.checkIfSearchedEmployee(this.props.searchedEmployee).teamName}
          </p>
        </div>
        {this.renderSearchBar(this.props.location.pathname)}
      </UserInfoWrapper>
    );
  }
}

const mapStateToProps = state => ({
  employees: getSearchEmployees(state)
});

export default connect(mapStateToProps)(InformationHeader);
