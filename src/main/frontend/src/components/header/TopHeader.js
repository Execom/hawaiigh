import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import ExecomHawaiiLogo from '@/img/execom_hawaii_logo.png';
import { openMenuDrawer } from '@/store/actions/drawerActions';
import { formatLocation } from '@/store/helperFunctions';
import { getRouter } from '@/store/selectors';
import { ToggleSidebarButton } from '@/components/common/ToggleSidebarButton';
import { UserInfo } from '@/components/user-info/UserInfo';
import {
  CenteredLogo,
  Heading,
  LogoImage,
  NavHeader,
  NavSpan
} from '@/components/header/styled';
import AppVersion, {
  AppVersionWrapperSidebar
} from '@/components/common/AppVersion';

class TopHeader extends Component {
  openMenuDrawer = () => {
    this.props.openMenuDrawer();
  };

  render() {
    const { imageUrl, fullName, email } = this.props.user;

    return (
      <NavHeader className="d-flex justify-content-between align-items-center pr-lg-5">
        <NavSpan height="true" className="d-flex align-items-center">
          <div className="d-lg-none">
            <ToggleSidebarButton color="black" click={this.openMenuDrawer} />
          </div>
          <Heading className="ml-3 d-lg-none text-capitalize">
            {formatLocation(this.props.router.location.pathname)}
          </Heading>
        </NavSpan>
        <CenteredLogo className="d-flex flex-column position-absolute">
          <NavLink className="d-none d-lg-flex " to={'/dashboard'}>
            <LogoImage src={ExecomHawaiiLogo} alt="Execom Hawaii logo" />
          </NavLink>
          <AppVersionWrapperSidebar className="d-none d-lg-block text-left pl-5 ml-2 pt-1">
            <AppVersion />
          </AppVersionWrapperSidebar>
        </CenteredLogo>
        <NavLink to={'/dashboard'}>
          <UserInfo imageUrl={imageUrl} fullName={fullName} userEmail={email} />
        </NavLink>
      </NavHeader>
    );
  }
}

const mapStateToProps = state => ({
  router: getRouter(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      openMenuDrawer
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TopHeader);
