import React from 'react';
import { getImageUrl } from '@/store/getImageUrl';
import { UserImage } from '@/components/common/userImage';
import { UserInfoContainer } from '@/components/user-info/styled';

export const UserInfo = ({ imageUrl, fullName, userEmail }) => {
  return (
    <div className="d-flex mr-2 align-items-center">
      <UserImage isSmall image={getImageUrl(imageUrl, 30)} size="30px" />
      <UserInfoContainer className="d-none d-lg-flex flex-column align-items-center ml-2 text-white">
        <p>{fullName}</p>
        <p>{userEmail}</p>
      </UserInfoContainer>
    </div>
  );
};
