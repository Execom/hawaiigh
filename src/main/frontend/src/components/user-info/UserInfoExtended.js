import React, { Component } from 'react';
import _includes from 'lodash/includes';
import { HR_MANAGER } from '@/helpers/enum';
import Execom from '@/img/execom.svg';
import PalmTree from '@/img/icons/palm_tree.svg';
import PalmTrees from '@/img/icons/palm_trees.svg';
import { getImageUrl } from '@/store/getImageUrl';
import { UserImage } from '@/components/common/userImage';
import {
  InfoContainer,
  NameHeading,
  PalmContainer
} from '@/components/user-info/styled';

export default class UserInfoExtended extends Component {
  userRoleHandler = (userRole, isApprover) => {
    if (userRole === HR_MANAGER || isApprover) {
      return (
        <PalmContainer className="position-absolute d-flex justify-content-center align-items-center shadow">
          <img
            src={userRole === HR_MANAGER ? PalmTrees : PalmTree}
            alt="palm"
          />
        </PalmContainer>
      );
    }
  };

  render() {
    const {
      userInfo: { fullName, jobTitle, imageUrl, userRole, teamName, isApprover }
    } = this.props;
    const userImage = getImageUrl(imageUrl, 100);
    const isHrManagerOrApprover = userRole === HR_MANAGER || isApprover;

    return (
      <div className="text-lg-center d-flex flex-column flex-lg-row p-4 p-lg-0 mb-5 mb-lg-0">
        <InfoContainer className="position-relative d-lg-inline-block justify-content-center justify-content-sm-center d-inline-flex mb-2">
          <UserImage
            image={_includes(userImage, 'null') ? Execom : userImage}
            isInSidebar
            size="120px"
          />
          {this.userRoleHandler(userRole, isApprover)}
        </InfoContainer>
        <InfoContainer
          className={`text-white text-whited-flex flex-column pr-4 align-self-lg-end ${
            isHrManagerOrApprover ? 'pl-lg-2' : 'pl-0'
          }`}
        >
          <div className="d-inline-flex d-lg-flex flex-column flex-lg-column">
            <NameHeading className="ellipsis-text">{fullName}</NameHeading>
            <h5 className="ellipsis-text font-weight-normal align-self-auto align-self-lg-start py-2 py-lg-0">
              {jobTitle}
            </h5>
          </div>
          <div className="d-inline-flex d-lg-flex font-weight-normal py-lg-1">
            <h5 className="ellipsis-text mr-2">Team:</h5>
            <h5 className="ellipsis-text">{teamName}</h5>
          </div>
        </InfoContainer>
      </div>
    );
  }
}
