import styled from 'styled-components';

export const UserInfoContainer = styled.div`
  font-size: 12px;
`;

export const PalmContainer = styled.div`
  bottom: 5px;
  right: 0;
  background: white;
  border-radius: 50%;
  border: 1px solid;
  width: 40px;
  height: 40px;
  padding: 2px;
  transition: 0.2s ease-in-out;

  @media (min-width: 992px) {
    right: 0px;
    bottom: 16px;
  }

  @media (max-width: 992px) {
    width: 30px;
    height: 30px;
    right: 85px;
    top: 52px;
  }
`;

export const NameHeading = styled.h3`
  font-size: 16px;
  font-weight: bold;

  @media (min-width: 1200px) {
    font-size: 18px;
  }
`;

export const InfoContainer = styled.div`
  transition: 0.2s ease-in-out;
  text-align: left;
  padding: 4px 0;

  h5 {
    font-size: 12px;
    align-self: flex-end;
  }

  @media (max-width: 992px) {
    font-size: 0.75rem;
    text-align: center;
  }
`;
