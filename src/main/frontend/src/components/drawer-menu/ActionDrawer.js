import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closeDrawer, openMenuDrawer } from '../../store/actions/drawerActions';
import { getActionDrawer } from '../../store/selectors';
import { renderView } from './helper';
import {
  CloseButton,
  HeaderContainer,
  SwipeableDrawerContainer
} from './styled';

class Drawer extends Component {
  toggleDrawer = open => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    open ? this.props.openMenuDrawer() : this.props.closeDrawer();
  };

  closeDrawer = () => {
    this.props.closeDrawer();
  };

  render() {
    const { payload } = this.props.actionDrawer;

    return (
      <SwipeableDrawer
        anchor="right"
        open={this.props.actionDrawer.open}
        onClose={this.toggleDrawer(false)}
        onOpen={this.toggleDrawer(false)}
        disableSwipeToOpen
        classes={{ paper: 'drawer' }}
      >
        <SwipeableDrawerContainer
          wide={this.props.actionDrawer.wide}
          className="d-flex flex-column flex-grow-1"
        >
          <HeaderContainer className="d-flex pl-2 pt-4 pb-2 border-dark justify-content-between border-bottom align-items-center">
            <h5>{payload.heading}</h5>
            <CloseButton onClick={this.closeDrawer}>x</CloseButton>
          </HeaderContainer>
          <div className="flex-grow-1 d-flex flex-column bg-white mt-4 ml-3 rounded-top">
            {renderView(this.props.actionDrawer.payload)}
          </div>
        </SwipeableDrawerContainer>
      </SwipeableDrawer>
    );
  }
}

const mapStateToProps = state => ({
  actionDrawer: getActionDrawer(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      openMenuDrawer,
      closeDrawer
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Drawer);
