import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closeDrawer, openMenuDrawer } from '../../store/actions/drawerActions';
import { getMenuDrawer } from '../../store/selectors';
import Sidebar from '../navigation/Sidebar';

class Drawer extends Component {
  toggleDrawer = open => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    open ? this.props.openMenuDrawer() : this.props.closeDrawer();
  };

  render() {
    return (
      <SwipeableDrawer
        className="d-lg-none"
        open={this.props.menuDrawerOpen}
        onClose={this.toggleDrawer(false)}
        onOpen={this.toggleDrawer(true)}
        SwipeAreaProps={{ className: 'swipe-area' }}
        swipeAreaWidth={25}
      >
        <Sidebar className="d-flex h-100" />
      </SwipeableDrawer>
    );
  }
}

const mapStateToProps = state => ({
  menuDrawerOpen: getMenuDrawer(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      openMenuDrawer,
      closeDrawer
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Drawer);
