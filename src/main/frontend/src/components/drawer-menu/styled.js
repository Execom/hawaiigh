import styled from 'styled-components';

export const HeaderContainer = styled.div`
  margin: 0 25px 0 40px;
`;

export const CloseButton = styled.span`
  cursor: pointer;
`;

export const SwipeableDrawerContainer = styled.div`
  transition: 0.1s ease-in-out;
  background: #ededed;
  width: 450px;

  @media (max-width: 550px) {
    width: 100%;
  }

  @media (min-width: 992px) {
    width: ${props => (props.wide ? '800px' : '450px')};
  }
`;
