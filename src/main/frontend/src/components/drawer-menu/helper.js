import React from 'react';
import CreateEmployee from '../../pages/administration/components/employees/CreateEmployee';
import EditEmployeeContainer from '../../pages/administration/components/employees/EditEmployeeContainer';
import CreateLeaveProfile from '../../pages/administration/components/leave-profiles/CreateLeaveProfile';
import EditLeaveProfile from '../../pages/administration/components/leave-profiles/EditLeaveProfile';
import CreateLeaveType from '../../pages/administration/components/leave-types/CreateLeaveType';
import EditLeaveType from '../../pages/administration/components/leave-types/EditLeaveType';
import CreatePublicHoliday from '../../pages/administration/components/public-holidays/CreatePublicHoliday';
import EditPublicHoliday from '../../pages/administration/components/public-holidays/EditPublicHoliday';
import CreateTeam from '../../pages/administration/components/teams/CreateTeam';
import CreateYear from '../../pages/administration/components/years/CreateYear';
import EditYear from '../../pages/administration/components/years/EditYear';
import { EditTeamContainer } from '../../pages/administration/components/employees/EditTeamContainer';

export const renderView = ({ type, id, activeTab }) => {
  switch (type) {
    case 'createYear':
      return <CreateYear />;
    case 'editYear':
      return <EditYear id={id} />;
    case 'createTeam':
      return <CreateTeam />;
    case 'editTeam':
      return <EditTeamContainer id={id} />;
    case 'createLeaveProfile':
      return <CreateLeaveProfile />;
    case 'editLeaveProfile':
      return <EditLeaveProfile id={id} />;
    case 'createLeaveType':
      return <CreateLeaveType />;
    case 'editLeaveType':
      return <EditLeaveType id={id} />;
    case 'createEmployee':
      return <CreateEmployee activeTab={activeTab} />;
    case 'editEmployee':
      return <EditEmployeeContainer id={id} />;
    case 'createPublicHoliday':
      return <CreatePublicHoliday />;
    case 'editPublicHoliday':
      return <EditPublicHoliday id={id} />;
    default:
      return <span>Empty blade</span>;
  }
};
