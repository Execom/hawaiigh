import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import LogoutIcon from '@/img/icons/logout-white.svg';
import { closeDrawer } from '@/store/actions/drawerActions';
import { getImageUrl } from '@/store/getImageUrl';
import {
  getRequestsForApproval,
  getRouter,
  getSearchedEmployee,
  getUser
} from '@/store/selectors';
import { NavigationLink } from '@/components/common/navigationLink';
import UserInfoExtended from '@/components/user-info/UserInfoExtended';
import { routes, isMobile } from '@/components/navigation/helpers';
import {
  Aside,
  ImgContainer,
  LinkWrapper,
  SidebarContainer,
  NavLine
} from '@/components/navigation/styled';
import AppVersion, {
  AppVersionWrapperSidebar
} from '@/components/common/AppVersion';

class Sidebar extends Component {
  navLinks = (
    searchedEmployee,
    location,
    requestsTotal,
    userRole,
    userAdminPermission,
    isApprover
  ) => {
    const userRoleRouts = routes[userRole](
      searchedEmployee,
      requestsTotal,
      isApprover
    ).map(navLink => (
      <NavLink
        className={
          navLink.searchedEmployee && navLink.url.includes(location.pathname)
            ? 'active'
            : ''
        }
        key={navLink.url}
        to={navLink.url}
        onClick={this.closeDrawer}
      >
        <LinkWrapper
          isMobile={isMobile()}
          navLinkName={navLink.name}
          className="align-items-center p-3 p-lg-2"
        >
          <ImgContainer requestsTotal={navLink.requestsTotal}>
            <img
              src={navLink.icon}
              className="mr-3 w-100"
              alt="Navigation icon"
            />
          </ImgContainer>
          <NavigationLink userRole={userRole}>{navLink.name}</NavigationLink>
        </LinkWrapper>
      </NavLink>
    ));
    const navLink = routes.ADMIN[0];
    const adminPermissionRoutes = ['SUPER', 'BASIC'].includes(
      userAdminPermission
    ) && (
      <NavLink
        className={
          navLink.searchedEmployee && navLink.url.includes(location.pathname)
            ? 'active'
            : ''
        }
        key={navLink.url}
        to={navLink.url}
        onClick={this.closeDrawer}
      >
        <LinkWrapper
          isMobile={isMobile()}
          navLinkName={navLink.name}
          className="align-items-center p-3 p-lg-2"
        >
          <ImgContainer requestsTotal={navLink.requestsTotal}>
            <img
              src={navLink.icon}
              className="mr-3 w-100"
              alt="Navigation icon"
            />
          </ImgContainer>
          <NavigationLink userRole={userRole}>{navLink.name}</NavigationLink>
        </LinkWrapper>
      </NavLink>
    );
    return [...userRoleRouts, adminPermissionRoutes];
  };

  closeDrawer = () => {
    this.props.closeDrawer();
  };

  checkIfSearchedEmployee = searchedEmployee => {
    if (searchedEmployee) {
      return this.props.searchedEmployee;
    }

    return this.props.user;
  };

  getUserImage = (searchedEmployee, user) =>
    getImageUrl(
      searchedEmployee ? searchedEmployee.imageUrl : user.imageUrl,
      30
    );

  render() {
    const {
      searchedEmployee,
      router: { location },
      requestsTotal,
      user,
      className
    } = this.props;

    return (
      <SidebarContainer className={`position-relative ${className}`}>
        <Aside className="d-flex flex-column py-2">
          <div className="justify-content-between d-flex flex-column flex-grow-1">
            <div className="justify-content-between d-sm-flex flex-column flex-lg-row px-lg-4">
              <NavLine />
              {user && (
                <UserInfoExtended
                  className="mb-5"
                  userInfo={this.checkIfSearchedEmployee(searchedEmployee)}
                />
              )}
              <div className="d-flex flex-column flex-lg-row align-self-lg-end">
                {user &&
                  this.navLinks(
                    searchedEmployee,
                    location,
                    requestsTotal,
                    user.userRole,
                    user.userAdminPermission,
                    user.isApprover
                  )}
              </div>
            </div>
            <div className="d-flex flex-column ">
              <NavLink
                to={{
                  pathname: '/login',
                  state: {
                    logout: true
                  }
                }}
              >
                <LinkWrapper className="align-items-center d-flex logout">
                  <img className="logout" src={LogoutIcon} alt="" />
                  <NavigationLink className="logout" background="transparent">
                    Log out
                  </NavigationLink>
                </LinkWrapper>
              </NavLink>
            </div>
            <AppVersionWrapperSidebar className="d-lg-none text-left pl-4">
              <AppVersion />
            </AppVersionWrapperSidebar>
          </div>
        </Aside>
      </SidebarContainer>
    );
  }
}

const mapStateToProps = state => ({
  searchedEmployee: getSearchedEmployee(state),
  router: getRouter(state),
  requestsTotal: getRequestsForApproval(state).total,
  user: getUser(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      closeDrawer
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);
