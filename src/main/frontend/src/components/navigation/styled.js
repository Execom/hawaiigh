import styled from 'styled-components';
import { handleRequestsNum } from './helpers';

export const Aside = styled.aside`
  background-color: #323234;
  width: 100%;
  font-weight: 600;
  transition: 0.2s ease-in-out;
  overflow: hidden;
  position: relative;

  @media (max-width: 991px) {
    max-width: 300px;
    width: 300px;
    overflow-y: auto;
  }
`;

export const NavLine = styled.span`
  border-bottom: 1px solid #fff;
  position: absolute;
  top: 91px;
  left: 139px;
  width: 100%;

  @media (max-width: 992px) {
    display: none;
  }
`;

export const SidebarContainer = styled.div`
  transition: 0.2s ease-in-out;
`;

export const LinkWrapper = styled.div`
  transition: 0.2s ease-in-out;
  max-height: 55px;
  width: fit-content;
  font-size: 15px;
  display: ${props =>
    (props.navLinkName === 'Administration' ||
      props.navLinkName === 'Data Export' ||
      props.navLinkName === 'Admin Dashboard') &&
    props.isMobile
      ? 'none'
      : 'flex'};

  img.logout {
    width: 24px;
  }

  &.logout {
    position: absolute;
    top: 13px;
    right: 13px;
    z-index: 20;

    @media (max-width: 992px) {
      top: unset;
      bottom: 30px;
      left: 13px;
      padding: 0;
    }

    @media (max-height: 604px) {
      bottom: unset;
      margin-top: 7px;
      margin-bottom: 12px;
    }
  }

  @media (max-width: 992px) {
    padding: 0 1.5rem;
  }

  @media (min-width: 1200px) {
    font-size: 17px;
  }
`;

export const MinimizedImageContainer = styled.div`
  cursor: pointer;
  z-index: 99;
  top: 50px;
  transition: 0.1s ease-in-out;

  @media (min-width: 992px) {
    opacity: 0;
  }
`;

export const ImgContainer = styled.div`
  position: relative;
  width: 25px;
  padding-right: 5px;
  &:after {
    content: ${props =>
      props.requestsTotal ? `'${props.requestsTotal}'` : ''};
    color: red;
    position: absolute;
    top: ${handleRequestsNum('-7px', '-10px')};
    left: ${handleRequestsNum('-7px', '-5px')};
    min-width: ${handleRequestsNum('25px', '20px')};
    min-height: ${handleRequestsNum('25px', '20px')};;
    background-size: cover;
    background: white;
    text-align: center;
    border-radius: 10%;
    line-height: ${handleRequestsNum('1.6', '1.2')};
    padding: 1px;
    }
  }

  @media (min-width: 1200px) {
    width: 34px;
    padding-right: 10px;
  }
`;
