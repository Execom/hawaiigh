import AdministrationIcon from '../../img/icons/administration-white.svg';
import LeaveHistoryIcon from '../../img/icons/approvals-white.svg';
import DashboardIcon from '../../img/icons/dashboard-home-white.svg';
import LeaveIcon from '../../img/icons/requests-white.svg';
import TeamCalendarIcon from '../../img/icons/team-calendar-white.svg';
import DataExportIcon from '../../img/icons/data-export-white.svg';
import AdminDashboardIcon from '../../img/icons/admin_dashboard.svg';

const userRoutes = (searchedEmployee, requestsTotal, isApprover) => [
  {
    url: `/dashboard${
      searchedEmployee ? `?userId=${searchedEmployee.id}` : ''
    }`,
    name: 'Dashboard',
    icon: DashboardIcon,
    searchedEmployee
  },
  {
    url: `/requests${searchedEmployee ? `?userId=${searchedEmployee.id}` : ''}`,
    name: 'Requests',
    icon: LeaveIcon,
    searchedEmployee
  },
  {
    url: '/team-calendar',
    name: 'Team calendar',
    icon: TeamCalendarIcon
  },
  ...(isApprover
    ? [
        {
          url: '/approvals',
          name: 'Approvals',
          icon: LeaveHistoryIcon,
          requestsTotal
        }
      ]
    : [])
];

const hrManagerRoutes = (searchedEmployee, requestsTotal, isApprover) => [
  ...userRoutes(searchedEmployee, requestsTotal, isApprover),
  { url: '/administration', name: 'Administration', icon: AdministrationIcon },
  { url: '/data-export', name: 'Data Export', icon: DataExportIcon }
];

const officeManagerRoutes = (searchedEmployee, requestsTotal, isApprover) => [
  ...userRoutes(searchedEmployee, requestsTotal, isApprover),
  { url: '/data-export', name: 'Data Export', icon: DataExportIcon }
];

const adminRouts = [
  { url: '/admin-dashboard', name: 'Admin Dashboard', icon: AdminDashboardIcon }
];

export const routes = {
  HR_MANAGER: hrManagerRoutes,
  OFFICE_MANAGER: officeManagerRoutes,
  USER: userRoutes,
  ADMIN: adminRouts
};

// There isn't a straightforward way to detect if a user is using a mobile or a
// desktop device. Viable options include checking for device browser features
// (maxTouchPoints, matchMedia("(pointer:coarse)"), 'orientation' in window,
// etc.), or user-agent. As stated on
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Browser_detection_using_the_user_agent,
// relying on user agent to check what device user is using is not reliable,
// but as there isn't better solution that doesn't exclude touchscreen laptops
// and desktops, this implementation should suffice
// (even though it will allow certain mobile user-agents to fail the check).

export const isMobile = () => {
  const UA = navigator.userAgent;
  const isMobile =
    UA &&
    (/\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(UA) ||
      /\b(Android|Windows Phone|iPad|iPod)\b/i.test(UA));
  return isMobile;
};

export const handleRequestsNum = (bigger, smaller) => props =>
  props.requestsTotal > 99 ? bigger : smaller;
