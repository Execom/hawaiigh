import React, { Component } from 'react';
import Tab from '../tabs/Tab';
import { TabButtonContainer, TabsContainer } from './styled';
import ArrowDown from '../../img/icons/arrow-down.svg';
import ReactSVG from 'react-svg';
import styled from 'styled-components';

const Arrow = styled.div`
  color: #bbb;
  transform-origin: 50% 50%;
  width: 36px;
  height: 36px;
  position: absolute;
  top: 14px;

  div,
  svg {
    width: 14px;
    height: 14px;
    fill: currentColor;
  }
`;

const ArrowLeft = styled(Arrow)`
  transform: rotate(90deg);
  left: 0px;
  background-image: linear-gradient(180deg, transparent, #fff, #fff);
`;

const ArrowRight = styled(Arrow)`
  transform: rotate(-90deg);
  right: 0px;
  background-image: linear-gradient(-180deg, transparent, #fff, #fff);
`;

export class Tabs extends Component {
  tabsRef = React.createRef(null);

  state = {
    scrollIndicatorLeft: false,
    scrollIndicatorRight: false
  };

  componentDidMount() {
    if (this.tabsRef.current.scrollWidth !== this.tabsRef.current.clientWidth) {
      this.setState({
        scrollIndicatorRight: true
      });
    }
  }

  onScroll = e => {
    const { scrollIndicatorLeft, scrollIndicatorRight } = this.state;
    const leftLimit = 20;
    const rightLimit = e.target.scrollWidth - e.target.clientWidth - 20;

    if (e.target.scrollLeft > leftLimit && !scrollIndicatorLeft) {
      this.setState({
        scrollIndicatorLeft: true
      });
    } else if (e.target.scrollLeft < leftLimit && scrollIndicatorLeft) {
      this.setState({
        scrollIndicatorLeft: false
      });
    }

    if (e.target.scrollLeft < rightLimit && !scrollIndicatorRight) {
      this.setState({
        scrollIndicatorRight: true
      });
    } else if (e.target.scrollLeft > rightLimit && scrollIndicatorRight) {
      this.setState({
        scrollIndicatorRight: false
      });
    }
  };

  render() {
    const { activeTabIndex, data, handleTabClick } = this.props;

    return (
      <div className="position-relative mt-lg-3">
        {this.state.scrollIndicatorLeft && (
          <ArrowLeft className="d-flex align-items-center justify-content-center">
            <ReactSVG src={ArrowDown} />
          </ArrowLeft>
        )}
        <TabsContainer
          className="d-flex flex-row bg-white"
          innerRef={this.tabsRef}
          onScroll={this.onScroll}
        >
          {data.map(({ label }, index) => (
            <TabButtonContainer className="flex-column" key={index}>
              <Tab
                label={label}
                isActive={activeTabIndex === index}
                handleTabClick={handleTabClick}
                tabIndex={index}
              />
            </TabButtonContainer>
          ))}
        </TabsContainer>
        {this.state.scrollIndicatorRight && (
          <ArrowRight className="d-flex align-items-center justify-content-center">
            <ReactSVG src={ArrowDown} />
          </ArrowRight>
        )}
      </div>
    );
  }
}
