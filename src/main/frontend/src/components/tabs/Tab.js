import React, { Component } from 'react';
import { TabButton } from './styled';

class Tab extends Component {
  onTabClick = () => {
    this.props.handleTabClick(this.props.tabIndex);
  };

  render() {
    const { label, isActive } = this.props;

    return (
      <TabButton
        className="px-3 pb-2 d-block w-100"
        active={isActive}
        onClick={this.onTabClick}
      >
        {label}
      </TabButton>
    );
  }
}

export default Tab;
