import styled from 'styled-components';

export const TabButtonContainer = styled.div`
  flex: 1;
  height: 50px;
  padding: 0px 24px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  font-size: 16px;
`;

export const TabButton = styled.button`
  cursor: pointer;
  background: transparent;
  outline: none;
  transition: border-color 0.2s ease-in;
  border: none;
  border-bottom: 2px solid ${props => (props.active ? '#dc3545' : 'white')};
  &:focus,
  &:hover,
  &:active {
    border-bottom: 2px solid #dc3545;
  }
`;

export const TabsContainer = styled.div`
  box-shadow: inset -2px 6px 12px -13px rgba(0, 0, 0, 0.75);
  white-space: nowrap;
  overflow-x: auto;
`;
