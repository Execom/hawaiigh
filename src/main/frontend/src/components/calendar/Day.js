import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { ifNotProp, ifProp } from 'styled-tools';
import { AFTERNOON, FULL_DAY, MORNING } from '../../helpers/enum';
import HolidayImg from '../../img/holiday2.svg';
import {
  handleRequestModal,
  openRequestModal
} from '../../store/actions/modalActions';
import {
  requestHover,
  requestUnhover
} from '../../store/actions/requestHoverActions';
import { getRequestHover, getSearchedEmployee } from '../../store/selectors';
import { backgroundColorHandler, pointerEventsHandler } from './dayHelper';

// TODO: Handle overriding better with styled-components
export const DayCell = styled.div`
  display: ${ifProp('isMobile', 'flex', 'table-cell')};
  transition: 0.2s ease-in-out;
  min-width: 18px;
  font-size: 14px;
  background-color: ${ifProp('highlighted', 'rgba(0,0,0,0.5)')};
  background-color: ${ifNotProp('day', '#9e9e9e')};
  background-color: ${ifProp('isWeekend', '#E0E0E0')};
  ${props => (props.isListView ? 'width: 2%;' : '')};

  &:hover {
    z-index: 1;
    background-color: ${props =>
      props.simplified || props.searchedEmployee || props.userDay.length
        ? 'transparent'
        : 'rgba(0, 0, 0, 0.5)'};
    box-shadow: ${props =>
      props.simplified || props.searchedEmployee || props.userDay.length
        ? 'transparent'
        : '4px 5px 13px 0px rgba(0, 0, 0, 0.65)'};
  }

  pointer-events: ${pointerEventsHandler};
  cursor: ${props =>
    props.simplified || props.searchedEmployee ? 'default' : 'pointer'};
  border-color: ${ifProp('isToday', 'grey', 'transparent')};
  border-left: 1px solid #c0c0c0;
  border-top: 1px solid #c0c0c0;
  border: ${ifProp('simplified', 'none')} ${ifProp('isToday', '2px solid grey')};
  border-style: solid;
  border-style: ${ifProp('simplified', 'none')};

  &:last-child {
    border-right: 1px solid #c0c0c0;
  }
`;

const ImageContainer = styled.div`
  width: ${props =>
    props.userDays.length === 1 && props.userDays[0].duration !== FULL_DAY
      ? '50%'
      : '100%'};
  z-index: 1;
  top: 0;
  justify-content: space-evenly;
  ${props =>
    props.userDays.length === 1 && props.userDays[0].duration === MORNING
      ? 'left: 0'
      : ''};
  ${props =>
    props.userDays.length === 1 && props.userDays[0].duration === AFTERNOON
      ? 'right: 0'
      : ''};
`;

const Image = styled.img`
  width: ${props => (props.holiday ? '22px' : '12px')};
`;

const ImageBlock = styled.div`
  cursor: ${props =>
    props.searchedEmployee || !props.simplified ? 'pointer' : 'default'};
  flex: 1 1 auto;
`;

const RequestStatus = styled.div`
  cursor: ${ifProp('searchedEmployee', 'default')};
  top: 0;
  width: ${props => (props.duration === FULL_DAY ? '100%' : '50%')};
  background-color: ${props =>
    backgroundColorHandler(props.requestStatus, props.absenceType)};
  opacity: ${props => (props.hovered ? '0.7' : '1')};
  transition: 0.1s ease-in-out;
  ${props => (props.duration === MORNING ? 'left: 0' : '')};
  ${props => (props.duration === AFTERNOON ? 'right: 0' : '')};

  &:hover {
    z-index: 1;
    background-color: ${props =>
      props.simplified || props.searchedEmployee
        ? 'transparent'
        : 'rgba(0, 0, 0, 0.5)'};
    box-shadow: ${props =>
      props.simplified || props.searchedEmployee
        ? 'transparent'
        : '4px 5px 13px 0px rgba(0, 0, 0, 0.65)'};
  }
`;

class Day extends Component {
  openModal = (day, userDay, searchedEmployee, publicHoliday, isWeekend) => {
    if (!userDay) {
      return this.props.openRequestModal({
        day,
        publicHoliday,
        isWeekend
      });
    }

    if (searchedEmployee) {
      this.props.handleRequestModal({
        id: userDay.requestId,
        previewOnly: true
      });
    } else {
      this.props.handleRequestModal({ id: userDay.requestId });
    }
  };

  render() {
    const {
      userDay,
      simplified,
      searchedEmployee,
      day,
      publicHoliday,
      publicHolidaysHover,
      isWeekend,
      isMobile,
      isToday,
      isListView,
      allowanceCalendarModal
    } = this.props;

    return (
      <DayCell
        highlighted={publicHolidaysHover && publicHoliday}
        isListView={isListView}
        {...!allowanceCalendarModal && {
          onClick: () => {
            userDay.length === 0 &&
              !simplified &&
              this.openModal(
                day,
                null,
                searchedEmployee,
                publicHoliday,
                isWeekend
              );
          }
        }}
        className={`${isMobile &&
          !isToday &&
          'border-0'} text-center align-middle justify-content-center align-items-center position-relative ${
          !isListView ? 'h-100' : 'd-flex'
        }`}
        searchedEmployee={searchedEmployee}
        {...this.props}
      >
        {userDay.map(day => (
          <RequestStatus
            className="position-absolute h-100"
            key={day.id}
            duration={day.duration}
            requestStatus={day.requestStatus}
            absenceType={day.absenceType}
            hovered={this.props.requestHoverId === day.requestId}
          />
        ))}
        {userDay.length === 1 && userDay.duration !== FULL_DAY && (
          <RequestStatus
            className="position-absolute h-100"
            {...!allowanceCalendarModal && {
              onClick: () => {
                !simplified &&
                  !searchedEmployee &&
                  this.openModal(day, null, searchedEmployee, publicHoliday);
              }
            }}
            hovered={this.props.requestHoverId === userDay[0].requestId}
            simplified={simplified}
            searchedEmployee={searchedEmployee}
            duration={userDay[0].duration === MORNING ? AFTERNOON : MORNING}
          />
        )}
        {!publicHoliday &&
          userDay.length === 0 &&
          (simplified || isMobile) &&
          day.format('D')}
        {publicHoliday && (
          <Image
            className="holiday-img"
            holiday
            src={HolidayImg}
            alt="public_holiday"
          />
        )}
        <ImageContainer
          className="h-100 position-absolute align-items-center d-flex"
          title={publicHoliday}
          userDays={userDay}
        >
          {userDay.map(dayItem => {
            return (
              <ImageBlock
                className="h-100 align-items-center d-flex justify-content-center"
                onMouseEnter={() => this.props.requestHover(dayItem.requestId)}
                onMouseLeave={() => this.props.requestUnhover()}
                key={dayItem.id}
                title={dayItem.absenceName}
                searchedEmployee={searchedEmployee}
                simplified={simplified}
                {...!allowanceCalendarModal && {
                  onClick: () => {
                    !simplified &&
                      this.openModal(
                        day,
                        dayItem,
                        searchedEmployee,
                        publicHoliday
                      );
                  }
                }}
              >
                <Image src={dayItem.iconUrl} />
              </ImageBlock>
            );
          })}
        </ImageContainer>
      </DayCell>
    );
  }
}

const mapStateToProps = state => ({
  searchedEmployee: getSearchedEmployee(state),
  requestHoverId: getRequestHover(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { openRequestModal, handleRequestModal, requestHover, requestUnhover },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Day);
