import {
  APPROVED,
  CANCELLATION_PENDING,
  PENDING,
  SICKNESS
} from '../../helpers/enum';

export const backgroundColorHandler = (requestStatus, absenceType) => {
  switch (requestStatus) {
    case APPROVED:
      if (absenceType === SICKNESS) {
        return '#FDA5A7';
      }

      return '#A6D7A8';
    case PENDING:
      return '#FEF2B0';
    case CANCELLATION_PENDING:
      return '#FEF2B0';
    default:
      return 'transparent';
  }
};

export const pointerEventsHandler = props => {
  if (!props.day || (!props.userDay.length && props.searchedEmployee)) {
    return 'none';
  }

  return 'auto';
};
