import React, { Fragment } from 'react';
import styled from 'styled-components';
import { DayCell } from './Day';

const MonthName = styled.div`
  font-size: 16px;
`;

const TableRow = styled.div`
  &:last-child {
    ${DayCell} {
      border-bottom: 1px solid #c0c0c0;
    }
  }
`;

export const Month = ({ name, children }) => {
  return (
    <Fragment>
      <TableRow className="d-table-row">
        <MonthName className="text-right py-1 pr-2 d-table-cell">
          {name}
        </MonthName>
        {children}
      </TableRow>
    </Fragment>
  );
};
