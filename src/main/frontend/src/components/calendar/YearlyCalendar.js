import React, { Component } from 'react';
import styled from 'styled-components';
import Day from './Day';
import { Month } from './Month';

const TableHeading = styled.div`
  min-width: 18px;
  font-size: 14px;
`;

const FirstCell = styled.div`
  width: 10px;
`;

const numberOfDays = [];
for (let i = 1; i <= 31; i++) {
  numberOfDays.push(i);
}

class YearlyCalendar extends Component {
  render() {
    return (
      <div className="w-100 d-table">
        <div className="d-table-row-group">
          <div className="d-table-row">
            <FirstCell className="d-table-cell" />
            {numberOfDays.map(day => (
              <TableHeading className="text-center py-1 d-table-cell" key={day}>
                {day}
              </TableHeading>
            ))}
          </div>
        </div>
        <div className="d-table-row-group">
          {this.props.calendar.map(month => (
            <Month key={month.name} {...month}>
              {month.days.map((day, index) =>
                day ? (
                  <Day
                    allowanceCalendarModal={this.props.allowanceCalendarModal}
                    publicHolidaysHover={this.props.publicHolidaysHover}
                    key={`${month.name}.${index}`}
                    {...day}
                  />
                ) : (
                  <Day key={`${month.name}.${index}`} />
                )
              )}
            </Month>
          ))}
        </div>
      </div>
    );
  }
}

export default YearlyCalendar;
