import _flow from 'lodash/flow';
import _get from 'lodash/get';
import moment from 'moment';

const createMonths = (year, publicHolidays, personalData) => {
  const months = moment.monthsShort().map(month => {
    return {
      name: month,
      year
    };
  });

  return { months, publicHolidays, personalData };
};

const createDays = data => {
  const monthsAndDays = data.months.map(month => {
    const days = [];

    for (let day = 1; day <= 31; day++) {
      const date = moment(`${day}-${month.name}-${month.year}`, 'DD-MMMM-YYYY');
      days.push(
        date.isValid()
          ? fillDaysWithData(date, data.publicHolidays, data.personalData)
          : { userDay: [] }
      );
    }

    return {
      ...month,
      days
    };
  });

  return monthsAndDays;
};

export const createMonth = (month, publicHolidays, personalData) => {
  const days = [];

  for (let day = 1; day <= moment(month, 'YYYY-MMMM').daysInMonth(); day++) {
    const date = moment(`${month}-${day}`, 'YYYY-MMMM-DD');
    days.push(fillDaysWithData(date, publicHolidays, personalData));
  }

  return days;
};

const checkIfPublicHoliday = (day, publicHolidays, personalData) => {
  if (!publicHolidays.length) return { day, personalData, publicHoliday: null };

  const publicHolidayCheck = publicHolidays.find(holiday =>
    moment(holiday.date).isSame(day, 'day')
  );

  return {
    day,
    personalData,
    publicHoliday: _get(publicHolidayCheck, 'name')
  };
};

const checkIfUserDay = dayObject => {
  const { day, personalData } = dayObject;

  if (!personalData || !personalData.length) {
    return { ...dayObject, userDay: [] };
  }

  const userDay = personalData.filter(userDay =>
    moment(userDay.date).isSame(day, 'day')
  );

  if (userDay.length === 2 && userDay[0].duration === 'AFTERNOON') {
    userDay.reverse();
  }

  return { ...dayObject, userDay };
};

const checkIfWeekend = dayObject =>
  dayObject.day.day() === 0 || dayObject.day.day() === 6
    ? { ...dayObject, isWeekend: true }
    : dayObject;

const checkIfToday = dayObject =>
  moment().isSame(dayObject.day, 'day')
    ? { ...dayObject, isToday: true }
    : dayObject;

const fillDaysWithData = _flow([
  checkIfPublicHoliday,
  checkIfUserDay,
  checkIfWeekend,
  checkIfToday
]);

export const createYearCalendar = _flow([createMonths, createDays]);

// This function is for selecting calendar year or year and month on calendar load.
// If current year is in the yearsWithAllowance array it will be selected,
// if it's not there selected year will be the first year of yearsWithAllowance array
export const selectFromYearArray = (year, years, format) => {
  if (years.find(arrayYear => arrayYear === year)) {
    if (format === 'year') {
      return year;
    }

    return `${year}-${moment().format('MMMM')}`;
  } else {
    if (format === 'year') {
      return years[0];
    }

    return `${years[0]}-January`;
  }
};

const addRequestsToDay = (date, publicHolidays, data, type) => {
  const formatedDate = date.format('YYYY-MM-DD');

  if (type === 'days') {
    return {
      day: date,
      requests: data.filter(day => day.date === formatedDate),
      publicHolidays
    };
  }

  const requests = data.filter(
    dataItem => dataItem.days.map(({ date }) => date).indexOf(formatedDate) > -1
  );

  return {
    day: date,
    requests,
    publicHolidays
  };
};

const addWeekNumberToDay = day => {
  return {
    ...day,
    week: day.day.isoWeek()
  };
};

const checkIfPublicHolidayMonth = dayObject => {
  let publicHolidayCheck;

  if (_get(dayObject, 'publicHolidays')) {
    publicHolidayCheck = dayObject.publicHolidays.find(holiday =>
      moment(holiday.date).isSame(dayObject.day, 'day')
    );
  }

  return {
    ...dayObject,
    publicHoliday: _get(publicHolidayCheck, 'name')
  };
};

const fillMonthCalendarDaysWithData = _flow([
  addRequestsToDay,
  addWeekNumberToDay,
  checkIfWeekend,
  checkIfToday,
  checkIfPublicHolidayMonth
]);

const calendarMonth = (month, publicHolidays, data, type) => {
  const daysInMonth = moment(`${month}-01`, 'YYYY-MMMM-DD').daysInMonth();
  const days = [];

  for (let i = 0; i < daysInMonth; i++) {
    const date = moment(`${month}-${i + 1}`, 'YYYY-MMMM-DD');

    days.push(
      date.isValid()
        ? fillMonthCalendarDaysWithData(date, publicHolidays, data, type)
        : null
    );
  }

  return days;
};

const addPaddingBefore = days => {
  const dayOfWeek = days[0].day.isoWeekday() - 1;

  if (dayOfWeek !== 0) {
    const padding = Array(dayOfWeek).fill(null);
    return [...padding, ...days];
  }

  return days;
};

const addPaddingAfter = days => {
  const dayOfWeek = days[days.length - 1].day.isoWeekday();

  if (dayOfWeek !== 7) {
    const padding = Array(7 - dayOfWeek).fill(null);
    return [...days, ...padding];
  }

  return days;
};

export const createMonthCalendar = _flow([
  calendarMonth,
  addPaddingBefore,
  addPaddingAfter
]);
