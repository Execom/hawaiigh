import React, { Component } from 'react';
import { DebounceInput } from 'react-debounce-input';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import Loupe from '../../img/icons/loupe.svg';
import Loading from '../loading/Loading';
import { LoupeImage, Results, ResultsContainer } from './styled';

const numberOfCharacters = 3;

class SearchDropdown extends Component {
  state = {
    inputIsActive: false,
    dropdownIsActive: false,
    inputValue: '',
    page: 0
  };

  componentDidUpdate(prevProps) {
    if (prevProps.searchResults.page !== this.props.searchResults.page) {
      this.setState({
        page: this.props.searchResults.page
      });
    }
  }

  mouseEnter = () => {
    this.setState({
      dropdownIsActive: true
    });
  };

  mouseLeave = () => {
    this.setState({
      dropdownIsActive: false,
      inputIsActive: true
    });
    this.inputReference.focus();
  };

  inputBlur = () => {
    this.setState({
      inputIsActive: false
    });
  };

  resetInput = () => {
    this.setState({
      inputValue: ''
    });
  };

  search = searchQuery => {
    const {
      dispatch,
      searchAction,
      resetAction,
      includeLoggedUser
    } = this.props;

    this.setState({
      inputIsActive: true,
      inputValue: this.inputReference.value,
      page: 0
    });

    if (searchQuery.length >= numberOfCharacters) {
      dispatch(resetAction());
      dispatch(
        searchAction({
          searchQuery,
          page: 0,
          size: 5,
          includeLoggedUser
        })
      );
    }
  };

  inputFocus = input => {
    input.focus();
    this.setState({
      inputIsActive: true
    });
  };

  render() {
    const shouldRenderResults =
      this.state.inputValue &&
      this.inputReference &&
      this.inputReference.value.length >= numberOfCharacters &&
      (this.state.inputIsActive || this.state.dropdownIsActive);

    const { inputValue, inputIsActive } = this.state;

    const {
      searchResults: { results, isFetching, last },
      dispatch,
      searchAction,
      children,
      includeLoggedUser,
      width
    } = this.props;

    return (
      <ResultsContainer width={width} className="position-relative">
        <DebounceInput
          inputRef={ref => {
            this.inputReference = ref;
          }}
          onBlur={this.inputBlur}
          minLength={numberOfCharacters}
          debounceTimeout={300}
          value={inputValue}
          onChange={event => this.search(event.target.value)}
          placeholder={this.props.placeholder || 'Users search'}
          className="w-100 p-1 pl-3 debounce-input-custom border-top-0 border-left-0 border-right-0"
          onClick={() => this.inputFocus(this.inputReference)}
        />
        <LoupeImage
          onClick={() => this.inputFocus(this.inputReference)}
          inputIsActive={inputIsActive}
          src={Loupe}
          alt="loupe"
        />
        {shouldRenderResults && (
          <Results
            onMouseEnter={this.mouseEnter}
            onMouseLeave={this.mouseLeave}
            className="results position-absolute w-100 mt-1 shadow p-3"
          >
            <InfiniteScroll
              loadMore={() =>
                !last &&
                !isFetching &&
                dispatch(
                  searchAction({
                    searchQuery: this.inputReference.value,
                    page: this.state.page + 1,
                    size: 5,
                    includeLoggedUser
                  })
                )
              }
              hasMore={!last}
              initialLoad={false}
              useWindow={false}
              loader={results.length > 0 && <Loading key="loader" />}
            >
              {children(this.inputReference, this.resetInput)}
            </InfiniteScroll>
          </Results>
        )}
      </ResultsContainer>
    );
  }
}

export default connect()(SearchDropdown);
