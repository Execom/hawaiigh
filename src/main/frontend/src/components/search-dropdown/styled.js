import styled from 'styled-components';

export const ResultsContainer = styled.div`
  width: ${props => props.width || '100%'};

  @media (max-width: 768px) {
    max-width: 400px;
    width: 100%;
  }

  @media (min-width: 993px) and (max-width: 1075px) {
    width: 320px;
  }
`;

export const Results = styled.div`
  background: white;
  max-height: 200px;
  overflow: auto;
  box-shadow: 0px 1px 6px 0px rgba(0, 0, 0, 0.22);
  border-radius: 8px;
  z-index: 10;
`;

export const LoupeImage = styled.img`
  position: absolute;
  width: 14px;
  right: 10px;
  top: 6px;
  pointer-events: ${props => (props.inputIsActive ? 'none' : 'all')};
`;

export const TeamSearchResultsButton = styled.button`
  pointer-events: ${props => (props.disabled ? 'none' : 'all')};
`;
