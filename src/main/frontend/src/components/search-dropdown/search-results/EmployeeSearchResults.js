import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { addSearchedEmployee } from '../../../store/actions/userActions';
import Loading from '../../loading/Loading';

class EmployeeTeamSearchResults extends Component {
  render() {
    if (
      this.props.employees.isFetching &&
      !this.props.employees.results.length
    ) {
      return <Loading />;
    }

    if (this.props.employees.results.length === 0) {
      return <span className="px-3 text-danger">No results</span>;
    }

    return (
      <Fragment>
        {this.props.employees.results.map(result => {
          return (
            <div className="pb-1 mb-1 border-bottom px-2" key={result.id}>
              {this.props.adminSearchAction ? (
                <div
                  className="admin-search-result"
                  onClick={e => {
                    e.stopPropagation();
                    this.props.adminSearchAction(result);
                  }}
                >
                  {result.fullName}
                </div>
              ) : (
                <NavLink
                  className="nav-link"
                  onClick={() => {
                    this.props.addSearchedEmployee(result);
                    this.props.resetInput();
                  }}
                  to={`?userId=${result.id}`}
                >
                  {result.fullName}
                </NavLink>
              )}
            </div>
          );
        })}
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addSearchedEmployee
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(EmployeeTeamSearchResults);
