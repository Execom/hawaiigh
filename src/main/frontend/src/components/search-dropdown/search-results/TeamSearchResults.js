import { FieldArray } from 'formik';
import React, { Component } from 'react';
import Loading from '../../loading/Loading';

export default class TeamSearchResults extends Component {
  checkIfTeamSelected = (selectedTeams, employeeTeamId, teamId) =>
    selectedTeams.find(team => team.id === teamId) ||
    teamId === +employeeTeamId;

  render() {
    if (this.props.teams.isFetching) {
      return <Loading />;
    }

    if (this.props.teams.results.length === 0) {
      return <span>No results</span>;
    }

    return this.props.teams.results.map(result => {
      return (
        <label className="d-flex justify-content-between" key={result.id}>
          {result.name}
          <FieldArray
            name="approverTeams"
            render={arrayHelpers => (
              <button
                disabled={this.checkIfTeamSelected(
                  this.props.selectedTeams,
                  this.props.employeeTeamId,
                  result.id
                )}
                className="btn mr-2"
                type="button"
                onClick={() => {
                  arrayHelpers.push(result);
                  this.props.inputReference.focus();
                }}
              >
                Add as an approver
              </button>
            )}
          />
        </label>
      );
    });
  }
}
