import { FieldArray } from 'formik';
import React, { Component } from 'react';
import { Tooltip } from '@material-ui/core';
import { TeamSearchResultsButton } from '../styled';

export default class EmployeeTeamSearchResults extends Component {
  checkIfUserSelected = (selectedApprovers, selectedMembers, userId) => {
    return (
      selectedMembers.find(member => member.id === userId) ||
      selectedApprovers.find(member => member.id === userId)
    );
  };

  render() {
    if (this.props.employees.results.length === 0) {
      return <span>No results</span>;
    }

    return this.props.employees.results.map(result => {
      const isUserSelected = this.checkIfUserSelected(
        this.props.selectedApprovers,
        this.props.selectedMembers,
        result.id
      );

      return (
        <div
          className="d-flex justify-content-between border-bottom mb-2 align-items-center"
          key={result.id}
        >
          {result.fullName}
          <div className="mb-2">
            <FieldArray
              name="users"
              render={arrayHelpers => (
                <Tooltip title="Member" placement="top">
                  <label className="mr-2 rounded-circle">
                    <TeamSearchResultsButton
                      disabled={isUserSelected}
                      className="btn rounded-circle bg-light"
                      type="button"
                      onClick={() => {
                        arrayHelpers.push(result);
                        this.props.inputReference.focus();
                      }}
                    >
                      M
                    </TeamSearchResultsButton>
                  </label>
                </Tooltip>
              )}
            />
            <FieldArray
              name="teamApprovers"
              render={arrayHelpers => (
                <Tooltip title="Approver" placement="top">
                  <label className="rounded-circle">
                    <TeamSearchResultsButton
                      disabled={isUserSelected}
                      className="btn rounded-circle bg-light"
                      type="button"
                      onClick={() => {
                        arrayHelpers.push(result);
                        this.props.inputReference.focus();
                      }}
                    >
                      A
                    </TeamSearchResultsButton>
                  </label>
                </Tooltip>
              )}
            />
          </div>
        </div>
      );
    });
  }
}
