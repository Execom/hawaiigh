import styled from 'styled-components';

export const StyledTextarea = styled.textarea`
  border: 1px solid grey;
  border-radius: 5px;
  background: #ededed;
`;
