import React from 'react';
import styled from 'styled-components';

const ModalHeaderContainer = styled.div`
  background: #e45052;
  border-top-right-radius: 5px;
  border-top-left-radius: 5px;
`;

const CloseIcon = styled.span`
  right: 0;
  top: 0;
  cursor: pointer;
`;

export const ModalHeader = props => {
  return (
    <ModalHeaderContainer className="d-flex justify-content-center p-3">
      <h3 className="text-white text-center px-3 pr-md-0">{props.title}</h3>
      <CloseIcon
        className="text-white position-absolute p-3"
        onClick={props.closeAction}
      >
        x
      </CloseIcon>
    </ModalHeaderContainer>
  );
};
