import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { ModalBody, ModalFooter } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { closeModal } from '../../store/actions/modalActions';
import { getModal, getUsers } from '../../store/selectors';
import Button from '../common/Button';
import { ModalHeader } from './components/ModalHeader';
import SearchDropdown from '../search-dropdown/SearchDropdown';
import EmployeeSearchResults from '../search-dropdown/search-results/EmployeeSearchResults';
import {
  clearUsers,
  superAdminSearchUsers
} from '../../store/actions/adminActions';
import { addAdmin } from '@/store/actions/adminActions';

class AddAdminModal extends Component {
  toggle = () => {
    this.props.closeModal('addAdminModal');
  };

  adminSearchAction = user => {
    this.toggle();
    this.props.clearUsers();
    user.userAdminPermission = 'BASIC';
    this.props.addAdmin(user);
  };

  render() {
    return (
      <Fragment>
        <ModalHeader
          title={<span>Add new Admin</span>}
          closeAction={this.toggle}
        />
        <ModalBody className="text-center">
          <SearchDropdown
            searchResults={this.props.users}
            searchAction={superAdminSearchUsers}
            resetAction={clearUsers}
          >
            {(inputReference, resetInput) => (
              <EmployeeSearchResults
                adminSearchAction={this.adminSearchAction}
                resetInput={resetInput}
                location={this.props.location}
                inputReference={inputReference}
                employees={this.props.users}
              />
            )}
          </SearchDropdown>
        </ModalBody>
        <ModalFooter>
          <Button
            styling={{ rounded: true, fontColor: 'white' }}
            title="Close"
            color="secondary"
            click={this.toggle}
          />
        </ModalFooter>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  users: getUsers(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ closeModal, clearUsers, addAdmin }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddAdminModal);
