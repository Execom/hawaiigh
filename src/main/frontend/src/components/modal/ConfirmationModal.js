import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { closeModal } from '../../store/actions/modalActions';
import {
  getAllowance,
  getLeaveTypes,
  getModal,
  getUser
} from '../../store/selectors';
import Button from '../common/Button';

class ConfirmationModal extends Component {
  render() {
    const {
      modal: {
        payload: { clickAction, values, message }
      },
      isSubmitting,
      dispatch
    } = this.props;

    return (
      <Fragment>
        <ModalHeader className="text-danger">Warning!</ModalHeader>
        <ModalBody>{message}</ModalBody>
        <ModalFooter>
          <Button
            className="px-4 py-2 mr-2"
            disabled={isSubmitting}
            title="Yes"
            click={() => {
              dispatch(clickAction(values));
              dispatch(closeModal('confirmationModal'));
            }}
            styling={{ fontColor: 'white' }}
          />
          <Button
            className="px-4 py-2"
            title="No"
            click={() => dispatch(closeModal('confirmationModal'))}
            styling={{ color: 'white' }}
          />
        </ModalFooter>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  user: getUser(state),
  leaveTypes: getLeaveTypes(state),
  allowance: getAllowance(state)
});

export default connect(mapStateToProps)(ConfirmationModal);
