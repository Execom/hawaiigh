import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { closeModal } from '../../store/actions/modalActions';
import { getLoading, getModal, getWindowWidth } from '../../store/selectors';
import { modalType } from './helpers';

class ModalWrapper extends Component {
  closeModal = type => {
    this.props.closeModal(type);
  };

  assignModalClasses = () => {
    const isRequestOrAllowanceCalendarModal =
      this.props.modal.type === 'requestModal' ||
      this.props.modal.type === 'allowanceCalendarModal';

    return `${this.props.className} ${isRequestOrAllowanceCalendarModal &&
      'modal-xl-custom'}`;
  };

  render() {
    return (
      <Modal
        backdrop={
          this.props.modal.type === 'requestModal' &&
          this.props.windowWidth > 991
            ? 'static'
            : true
        }
        isOpen={this.props.modal.open}
        toggle={() => this.closeModal(this.props.modal.type)}
        className={this.assignModalClasses()}
        centered
      >
        {modalType(this.props.modal.type)}
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  loading: getLoading(state),
  windowWidth: getWindowWidth(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ closeModal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalWrapper);
