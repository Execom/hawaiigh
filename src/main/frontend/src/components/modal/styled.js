import styled, { keyframes } from 'styled-components';
import { ifProp } from 'styled-tools';

export const IconImg = styled.img`
  height: 20px;
`;

export const InfoBlock = styled.div`
  &:nth-child(even) {
    background: #efefef;
  }
`;

export const IconWrapper = styled.div`
  cursor: pointer;
  border: 1px solid black;
  transition: ease-in 100ms;
  width: 100%;
`;

const swing = keyframes`
    15% {
        transform: translateX(5px);
    }
    30% {
        transform: translateX(-5px);
    }
    50% {
        transform: translateX(3px);
    }
    65% {
        transform: translateX(-3px);
    }
    80% {
        transform: translateX(2px);
    }
    100% {
        transform: translateX(0);
    }
  `;

export const LeaveWrapper = styled.div`
  pointer-events: ${ifProp('disabledClick', 'none', 'all')};
  max-width: 60px;

  @media (min-width: 400px) {
    max-width: 90px;
  }

  @media (min-width: 500px) {
    max-width: 200px;
  }

  img,
  p {
    transition: opacity ease-in 100ms;
    opacity: ${ifProp('disabledClick', '0.4', '1')};
  }
  &:hover {
    img,
    p {
      animation: ${swing} 1s ease;
      animation-iteration-count: 1;
    }
  }
`;

export const ModalHeading = styled.h3`
  font-size: 20px;
`;

export const Icon = styled.img`
  width: 100%;
`;

export const ContainedText = styled.div`
  flex: 0 0 55%;
`;

export const ModalContainer = styled.div`
  min-height: ${props => `${props.blockHeight}px`};
  max-height: ${props => `${props.blockHeight}px`};
`;
