import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { ModalBody } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { closeModal } from '../../store/actions/modalActions';
import { getModal } from '../../store/selectors';
import { ModalHeader } from './components/ModalHeader';
import { IconImg, InfoBlock } from './styled';

class TeamRequestsModal extends Component {
  toggle = () => {
    this.props.closeModal('teamRequestsModal');
  };

  render() {
    return (
      <Fragment>
        <ModalHeader
          title={<span>Team requests</span>}
          closeAction={this.toggle}
        />
        <ModalBody className="text-center">
          {this.props.modal.payload.map(
            ({ user: { fullName }, absenceName, iconUrl, id }) => (
              <InfoBlock
                className="d-flex justify-content-between align-items-center py-2 px-1"
                key={id}
              >
                <h5 className="text-left mr-3">{fullName}</h5>
                <div className="d-flex align-items-center">
                  <h5 className="text-right">{absenceName}</h5>
                  <IconImg className="ml-2" src={iconUrl} alt="" />
                </div>
              </InfoBlock>
            )
          )}
        </ModalBody>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ closeModal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamRequestsModal);
