import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ModalBody } from 'reactstrap';
import _trimStart from 'lodash/trimStart';
import {
  BONUS,
  IS_BONUS,
  IS_LEAVE,
  IS_SICKNESS,
  LEAVE,
  REQUEST_MODAL,
  SICKNESS,
  TRAINING
} from '@/helpers/enum';
import BonusIcon from '@/img/icons/bonus_ss.svg';
import LeaveIcon from '@/img/icons/palm_tree.svg';
import SicknessIcon from '@/img/icons/sickness_ss.svg';
import { closeModal } from '@/store/actions/modalActions';
import {
  createBonusRequest,
  createLeaveRequest,
  createSicknessRequest
} from '@/store/actions/requestsActions';
import {
  getAllowance,
  getLeaveTypes,
  getModal,
  getPublicHolidays,
  getUser,
  getSearchedEmployee
} from '@/store/selectors';
import Button from '@/components/common/Button';
import { ModalHeader } from '@/components/modal/components/ModalHeader';
import RequestForm from '@/components/modal/requests/RequestForm';
import {
  Icon,
  IconWrapper,
  LeaveWrapper,
  ModalHeading
} from '@/components/modal/styled';

class RequestModal extends Component {
  state = {
    isLeave: false,
    isSickness: false,
    isBonus: false,
    title: ''
  };

  componentDidMount() {
    if (this.props.modal.payload.type) {
      this.setState({
        [this.props.modal.payload.type]: true,
        title: _trimStart(this.props.modal.payload.type, 'is')
      });
    }
  }

  closeRequestModal = () => {
    this.props.closeModal(REQUEST_MODAL);
  };

  resetState = () => {
    this.setState({
      isLeave: false,
      isSickness: false,
      isBonus: false,
      title: ''
    });
  };

  requestType = leaveType => {
    this.setState({
      [leaveType]: true,
      title: _trimStart(leaveType, 'is')
    });
  };

  filterLeaveTypes = leaveType => {
    const { results } = this.props.leaveTypes;

    if (leaveType === LEAVE) {
      const leaveTypesDeducted = results.filter(
        leave => leave.absenceType === leaveType && leave.deducted
      );

      const leaveTypesNonDeducted = results.filter(
        leave => leave.absenceType === leaveType && !leave.deducted
      );

      const training = results.find(leave => leave.absenceType === TRAINING);

      return [
        { name: 'Annual Leave', items: [...leaveTypesDeducted] },
        { name: 'Deducted Leave', items: [training] },
        { name: 'Non Deducted', items: [...leaveTypesNonDeducted] }
      ];
    }

    return results.filter(leave => leave.absenceType === leaveType);
  };

  getFormProps = () => {
    if (this.state.isLeave) {
      return {
        leaveTypes: this.filterLeaveTypes(LEAVE),
        requestAction: createLeaveRequest,
        remainingLeave: this.props.allowance['remainingAnnual'] / 8,
        remainingTraining: this.props.allowance['remainingTraining'] / 8,
        nextYearAllowance: this.props.allowance.nextYearAllowance
      };
    } else if (this.state.isSickness) {
      return {
        leaveTypes: this.filterLeaveTypes(SICKNESS),
        requestAction: createSicknessRequest
      };
    } else {
      return {
        leaveTypes: this.filterLeaveTypes(BONUS),
        requestAction: createBonusRequest,
        remainingLeave: this.props.allowance['remainingBonus'] / 8
      };
    }
  };

  render() {
    const isSpecificRequest =
      this.state.isLeave || this.state.isBonus || this.state.isSickness;

    const {
      user,
      searchedEmployee,
      modal: {
        payload: { selectedDay, publicHoliday, isWeekend }
      },
      publicHolidays
    } = this.props;

    return (
      <Fragment>
        <ModalHeader
          title={`New ${this.state.title} Request`}
          closeAction={this.closeRequestModal}
        />
        <ModalBody className="py-4 container-fluid overflow-visible">
          {isSpecificRequest && (
            <RequestForm
              user={searchedEmployee || user}
              selectedDay={selectedDay}
              publicHolidays={publicHolidays}
              {...this.getFormProps()}
            />
          )}
          {!isSpecificRequest && (
            <div className="d-flex justify-content-center">
              <LeaveWrapper
                disabledClick={publicHoliday || isWeekend}
                className="d-flex align-items-center flex-column mt-lg-5 mb-lg-4 mx-3 flex-grow-1"
                onClick={() => this.requestType(IS_LEAVE)}
              >
                <IconWrapper className="text-center rounded">
                  <Icon src={LeaveIcon} alt="leave_icon" />
                </IconWrapper>
                <ModalHeading className="p-lg-3 pt-3">Leave</ModalHeading>
              </LeaveWrapper>
              <LeaveWrapper
                disabledClick={publicHoliday || isWeekend}
                className="d-flex align-items-center flex-column mt-lg-5 mb-lg-4 mx-3 flex-grow-1"
                onClick={() => this.requestType(IS_SICKNESS)}
              >
                <IconWrapper className="text-center rounded">
                  <Icon src={SicknessIcon} alt="sickness_icon" />
                </IconWrapper>
                <ModalHeading className="p-lg-3 pt-3">Sickness</ModalHeading>
              </LeaveWrapper>
              <LeaveWrapper
                className="d-flex align-items-center flex-column mt-lg-5 mb-lg-4 mx-3 flex-grow-1"
                onClick={() => this.requestType(IS_BONUS)}
              >
                <IconWrapper className="text-center rounded">
                  <Icon src={BonusIcon} alt="bonus_icon" />
                </IconWrapper>
                <ModalHeading className="p-lg-3 pt-3">Bonus</ModalHeading>
              </LeaveWrapper>
            </div>
          )}

          {isSpecificRequest && (
            <Button
              title="Back"
              type="submit"
              styling={{ rounded: true, color: '#EDEDED' }}
              click={this.resetState}
            />
          )}
        </ModalBody>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  user: getUser(state),
  searchedEmployee: getSearchedEmployee(state),
  leaveTypes: getLeaveTypes(state),
  allowance: getAllowance(state),
  publicHolidays: getPublicHolidays(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ closeModal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestModal);
