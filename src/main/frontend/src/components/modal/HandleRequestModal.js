import classnames from 'classnames';
import _replace from 'lodash/replace';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {
  ModalBody,
  ModalFooter,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from 'reactstrap';
import { bindActionCreators } from 'redux';
import {
  APPROVED,
  CANCELED,
  CANCELLATION_PENDING,
  HANDLE_REQUEST_MODAL,
  PENDING,
  REJECTED
} from '../../helpers/enum';
import { closeModal } from '../../store/actions/modalActions';
import { handleRequest } from '../../store/actions/requestsActions';
import { formatDate, formatTime } from '../../store/helperFunctions';
import {
  getAllowance,
  getIsSubmitting,
  getLeaveTypes,
  getModal,
  getUser,
  getAudit
} from '../../store/selectors';
import Button from '../common/Button';
import { CapitalizedText } from '../common/capitalizedText';
import { ModalHeader } from './components/ModalHeader';
import HandleRequestForm from './handleRequestForm/HandleRequestForm';
import { constructStatus, getApprovers, takenHours } from './helpers';
import { ContainedText, ModalContainer } from './styled';

class handleRequestModal extends Component {
  state = {
    view: '',
    activeTab: '1',
    modalHeight: 0
  };

  handleRequest = (status, request) => {
    this.props.handleRequest({
      ...request,
      requestStatus: status
    });
  };

  closeModal = () => {
    this.props.closeModal(HANDLE_REQUEST_MODAL);
  };

  changeView = view =>
    this.setState({
      view
    });

  toggleTab = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };

  createModalFooter = (request, isApprover, previewOnly) => {
    if (!previewOnly && isApprover) {
      return (
        <ModalFooter>
          <Button
            disabled={this.props.isSubmitting}
            key="reject"
            styling={{ rounded: true, fontColor: 'white' }}
            title="Reject"
            click={() =>
              this.changeView(
                request.requestStatus === CANCELLATION_PENDING
                  ? CANCELLATION_PENDING
                  : REJECTED
              )
            }
          />
          <Button
            disabled={this.props.isSubmitting}
            key="approve"
            styling={{ rounded: true, color: '#69BF69', fontColor: 'white' }}
            title="Approve"
            click={() =>
              this.handleRequest(APPROVED, { ...request, isApprover })
            }
          />
        </ModalFooter>
      );
    } else if (
      !previewOnly &&
      (request.requestStatus === PENDING || request.requestStatus === APPROVED)
    ) {
      return (
        <ModalFooter>
          <Button
            color="secondary"
            styling={{ rounded: true, fontColor: 'white' }}
            title="Submit cancellation"
            click={() => this.changeView(CANCELED)}
          />
        </ModalFooter>
      );
    }

    return null;
  };

  getHeight = element => {
    if (element && !this.state.modalHeight) {
      this.setState({ modalHeight: element.clientHeight });
    }
  };

  render() {
    const {
      requestInfo: {
        absence,
        requestStatus,
        reason,
        user,
        submissionTime,
        days,
        currentlyApprovedBy
      },
      requestInfo,
      previewOnly,
      isApprover
    } = this.props.modal.payload.leaveRequest;

    return (
      <Fragment>
        <ModalHeader
          title={
            <span>
              <CapitalizedText className="mr-1">
                {absence.absenceType}
              </CapitalizedText>
              request for {user.fullName}
            </span>
          }
          closeAction={this.closeModal}
        />
        {this.state.view ? (
          <HandleRequestForm
            requestAction={handleRequest}
            view={this.state.view}
            request={{
              ...requestInfo,
              requestStatus: this.state.view === CANCELED ? CANCELED : REJECTED,
              isApprover
            }}
          />
        ) : (
          <ModalContainer className="d-flex flex-column">
            <ModalBody className="p-0 mb-2 d-flex flex-column overflow-hidden">
              <Nav tabs className="pt-1 justify-content-center">
                <NavItem className="flex-grow-1 text-center">
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '1'
                    })}
                    onClick={() => {
                      this.toggleTab('1');
                    }}
                  >
                    Request info
                  </NavLink>
                </NavItem>
                <NavItem className="flex-grow-1 text-center">
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '2'
                    })}
                    onClick={() => {
                      this.toggleTab('2');
                    }}
                  >
                    Request history
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent
                activeTab={this.state.activeTab}
                className="flex-grow-1 d-flex flex-column overflow-hidden"
              >
                <TabPane tabId="1">
                  <div ref={this.getHeight}>
                    <div className="border-bottom px-3 d-flex justify-content-between py-2">
                      <span>Status</span>
                      <ContainedText className="text-right">
                        <CapitalizedText>
                          {_replace(requestStatus, '_', ' ')}
                        </CapitalizedText>
                      </ContainedText>
                    </div>
                    <div className="border-bottom px-3 d-flex justify-content-between py-2">
                      <span>Date submited</span>
                      <ContainedText className="text-right">
                        <span>
                          {moment(submissionTime).format('DD MMM YYYY - HH:mm')}
                        </span>
                      </ContainedText>
                    </div>
                    <div className="border-bottom px-3 d-flex justify-content-between py-2">
                      <span>Leave type</span>
                      <ContainedText className="text-right">
                        <CapitalizedText>{absence.name}</CapitalizedText>
                      </ContainedText>
                    </div>
                    <div className="border-bottom px-3 d-flex justify-content-between py-2">
                      <span>Requested time</span>
                      <ContainedText className="text-right">
                        {/* TODO: Extract to a method */}
                        {`${takenHours(days) / 8} ${
                          takenHours(days) / 8 === 1 ? 'day' : 'days'
                        }`}
                      </ContainedText>
                    </div>
                    <div className="px-3 d-flex justify-content-between py-2">
                      <span>Reason for leave</span>
                      <ContainedText className="text-right">
                        {reason || '/'}
                      </ContainedText>
                    </div>
                    {currentlyApprovedBy.length > 0 && (
                      <div className=" border-top px-3 d-flex justify-content-between pt-2">
                        <span>Approved by</span>
                        <ContainedText className="text-right">
                          {getApprovers(currentlyApprovedBy)}
                        </ContainedText>
                      </div>
                    )}
                  </div>
                </TabPane>
                <TabPane className="px-3 overflow-auto" tabId="2">
                  <ModalContainer blockHeight={this.state.modalHeight}>
                    <div
                      key="tab-2"
                      className="d-flex justify-content-between py-2 border-bottom"
                    >
                      <div className="d-flex flex-column">
                        <span className="mb-1">{user.fullName}</span>
                        <span className="small">{`${formatDate(
                          submissionTime
                        )} - ${formatTime(submissionTime)}`}</span>
                      </div>
                      <div className="text-right d-flex flex-column">
                        <span className="mb-1 d-inline-block text-warning">
                          Request submitted
                        </span>
                        <span className="small">{reason}</span>
                      </div>
                    </div>
                    {this.props.audit.approval.map(item => (
                      <div
                        key={item.id}
                        className="d-flex justify-content-between py-2 border-bottom"
                      >
                        <div className="d-flex flex-column">
                          <span className="mb-1">
                            {item.requestHandlerFullName}
                          </span>
                          <span className="small">{`${formatDate(
                            item.modifiedDateTime
                          )} - ${formatTime(item.modifiedDateTime)}`}</span>
                        </div>
                        <div className="text-right d-flex flex-column">
                          <span
                            className={`mb-1 d-inline-block ${
                              item.requestStatus === APPROVED
                                ? 'text-success'
                                : 'text-danger'
                            }`}
                          >
                            {constructStatus(item.requestStatus)}
                          </span>
                          <span className="small">{item.comment}</span>
                        </div>
                      </div>
                    ))}
                  </ModalContainer>
                </TabPane>
              </TabContent>
            </ModalBody>
            {this.createModalFooter(requestInfo, isApprover, previewOnly)}
          </ModalContainer>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  user: getUser(state),
  leaveTypes: getLeaveTypes(state),
  allowance: getAllowance(state),
  isSubmitting: getIsSubmitting(state),
  audit: getAudit(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ handleRequest, closeModal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(handleRequestModal);
