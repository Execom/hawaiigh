import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { ModalBody, ModalHeader } from 'reactstrap';
import { Formik } from 'formik';
import { closeModal } from '@/store/actions/modalActions';
import { getIsSubmitting, getModal, getUser } from '@/store/selectors';
import Button from '@/components/common/Button';
import { bindActionCreators } from 'redux';
import { cancelEmployeeDelete } from '@/store/actions/userActions';

class CancelEmployeeDeleteModal extends Component {
  getDatePickerClassName(errors, touched) {
    return errors.stoppedWorkingAtExecomDate &&
      touched.stoppedWorkingAtExecomDate
      ? 'border-danger'
      : '';
  }

  render() {
    const {
      modal: {
        payload: { values, clickAction, message }
      },
      cancelEmployeeDelete,
      closeModal
    } = this.props;
    return (
      <Fragment>
        <ModalHeader className="text-danger">{message}</ModalHeader>
        <ModalBody>
          <Formik
            initialValues={{
              ...values,
              message
            }}
            onSubmit={values => {
              clickAction({
                ...values
              });
              cancelEmployeeDelete({
                ...values
              });
            }}
            render={({ handleSubmit }) => (
              <Fragment>
                <div className="d-flex justify-content-end pt-2">
                  <Button
                    type="submit"
                    className="px-4 py-2 mr-2"
                    title="Cancel"
                    click={() => closeModal('employeeDeleteModal')}
                    styling={{ color: 'white' }}
                  />
                  <Button
                    type="submit"
                    className="px-4 py-2"
                    disabled={this.props.isSubmitting}
                    title="Yes"
                    click={handleSubmit}
                    styling={{ fontColor: 'white' }}
                  />
                </div>
              </Fragment>
            )}
          />
        </ModalBody>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  user: getUser(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      cancelEmployeeDelete,
      closeModal
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CancelEmployeeDeleteModal);
