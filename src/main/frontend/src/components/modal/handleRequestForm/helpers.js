import * as Yup from 'yup';
import { CANCELLATION_PENDING, REJECTED } from '../../../helpers/enum';

export const checkCommentType = view => {
  switch (view) {
    case REJECTED:
      return 'approverComment';

    case CANCELLATION_PENDING:
      return 'approverCancellationComment';

    default:
      return 'cancellationReason';
  }
};

export const validationSchema = view =>
  Yup.object().shape({
    [checkCommentType(view)]: Yup.string()
      .required()
      .max(255)
  });
