import { Formik } from 'formik';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CANCELED } from '../../../helpers/enum';
import { getIsSubmitting } from '../../../store/selectors';
import Button from '../../common/Button';
import { StyledTextarea } from '../components/styled';
import { checkCommentType, validationSchema } from './helpers';

class HandleRequestForm extends Component {
  render() {
    const { dispatch, requestAction, request, view } = this.props;
    const commentType = checkCommentType(view);
    const label = view === CANCELED ? 'cancellation' : 'rejection';

    return (
      <Formik
        validationSchema={validationSchema(view)}
        initialValues={{
          ...request
        }}
        onSubmit={values => dispatch(requestAction(values))}
        render={({ handleSubmit, handleChange, values, errors, touched }) => (
          <div className="container-fluid py-3">
            <div className="row">
              <div className="col-12">
                <label className="w-100">
                  <h5 className="font-italic mb-1 ml-4 text-danger small">
                    {`Enter a reason for your ${label}:`}
                  </h5>
                  <StyledTextarea
                    maxLength="255"
                    className={`${errors[commentType] &&
                      touched[commentType] &&
                      'border-danger'} mb-3 w-100 p-3`}
                    name={commentType}
                    rows="8"
                    onChange={handleChange}
                  />
                </label>
                <div className="text-right">
                  <Button
                    disabled={this.props.isSubmitting || !values[commentType]}
                    title={`Confirm ${label}`}
                    type="submit"
                    styling={{ rounded: true, fontColor: 'white' }}
                    click={handleSubmit}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  isSubmitting: getIsSubmitting(state)
});

export default connect(
  mapStateToProps,
  null
)(HandleRequestForm);
