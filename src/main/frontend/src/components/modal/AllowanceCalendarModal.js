import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { ModalBody, ModalHeader } from 'reactstrap';
import { closeModal } from '@/store/actions/modalActions';
import { getModal } from '@/store/selectors';
import Button from '@/components/common/Button';
import { bindActionCreators } from 'redux';
import YearlyCalendar from '@/components/calendar/YearlyCalendar';
import { createYearCalendar } from '@/components/calendar/calendarUtils';
import { getPublicHolidays, getUserDays } from '../../store/selectors';
import moment from 'moment';

class AllowanceCalendarModal extends Component {
  currentYear = moment().year();

  state = {
    calendar: createYearCalendar(
      this.props.modal.payload.year,
      this.props.publicHolidays,
      this.props.userDays
    ),
    publicHolidaysHover: false
  };

  componentDidUpdate(prevProps) {
    if (prevProps.userDays !== this.props.userDays) {
      this.renderYearCalendar();
    }
  }

  renderYearCalendar = () => {
    this.setState(prevState => ({
      ...prevState,
      calendar: createYearCalendar(
        this.props.modal.payload.year,
        this.props.publicHolidays,
        this.props.userDays
      )
    }));
  };

  render() {
    const {
      modal: {
        payload: { message }
      },
      closeModal
    } = this.props;

    return (
      <Fragment>
        <ModalHeader className="text-danger">{message}</ModalHeader>
        <ModalBody>
          <YearlyCalendar
            calendar={this.state.calendar}
            selectDay
            publicHolidaysHover
            allowanceCalendarModal
          />
          <div className="d-flex justify-content-end border-top pt-2">
            <Button
              type="submit"
              className="px-4 py-2 mr-2"
              title="Close"
              click={() => closeModal('allowanceCalendarModal')}
              styling={{ color: 'white' }}
            />
          </div>
        </ModalBody>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  publicHolidays: getPublicHolidays(state),
  userDays: getUserDays(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      closeModal
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllowanceCalendarModal);
