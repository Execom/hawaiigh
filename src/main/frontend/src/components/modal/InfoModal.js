import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { ModalBody, ModalFooter } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { closeModal } from '../../store/actions/modalActions';
import { getModal } from '../../store/selectors';
import Button from '../common/Button';
import { ModalHeader } from './components/ModalHeader';

class InfoModal extends Component {
  toggle = () => {
    this.props.closeModal('infoModal');
  };

  render() {
    return (
      <Fragment>
        <ModalHeader
          title={<span>Approval modal</span>}
          closeAction={this.toggle}
        />
        <ModalBody className="text-center">
          {this.props.modal.payload.message}
        </ModalBody>
        <ModalFooter>
          <Button
            styling={{ rounded: true, fontColor: 'white' }}
            title="Close"
            color="secondary"
            click={this.toggle}
          />
        </ModalFooter>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ closeModal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InfoModal);
