import { Formik } from 'formik';
import _flow from 'lodash/flow';
import _get from 'lodash/get';
import _isNumber from 'lodash/isNumber';
import _last from 'lodash/last';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import Switch from 'react-switch';
import { AFTERNOON, FULL_DAY, LEAVE, MORNING } from '@/helpers/enum';
import { formatDate } from '../../../store/helperFunctions';
import {
  getIsSubmitting,
  getYearsWithAllowance,
  getSearchedEmployee
} from '@/store/selectors';
import Button from '../../common/Button';
import CustomInput from '../../common/CustomInput';
import Radio from '../../common/Radio';
import { StyledTextarea } from '../components/styled';
import {
  createDays,
  handleSelectOption,
  renderOptions,
  setDayLength,
  validationSchema
} from './helpers';
import {
  AllowanceHeading,
  ButtonContainer,
  ConfirmationPopover,
  DangerText
} from './styled';
import { ArrowSelect } from '@/pages/dashboard/components/styled';
import ArrowDown from '../../../img/icons/arrow-down.svg';

class RequestForm extends Component {
  state = {
    startDate: this.props.selectedDay
      ? this.props.selectedDay.toDate()
      : moment().toDate(),
    endDate: this.props.selectedDay
      ? this.props.selectedDay.toDate()
      : moment().toDate()
  };

  selectStartDateHandler = (startDate, firstDayDuration, lastDayDuration) => {
    const stateStart = formatDate(this.state.startDate);
    const stateEnd = formatDate(this.state.endDate);
    const start = formatDate(startDate);

    if (
      (stateStart === stateEnd && stateStart !== start) ||
      (stateStart !== stateEnd && start === stateEnd)
    ) {
      return this.handleDates({ startDate }, FULL_DAY, FULL_DAY);
    }

    return this.handleDates({ startDate }, firstDayDuration, lastDayDuration);
  };

  selectEndDateHandler = (endDate, firstDayDuration, lastDayDuration) => {
    const stateStart = formatDate(this.state.startDate);
    const stateEnd = formatDate(this.state.endDate);
    const end = formatDate(endDate);

    if (
      (stateStart !== stateEnd && stateStart === end) ||
      (stateStart === stateEnd && stateEnd !== end)
    ) {
      return this.handleDates({ endDate }, FULL_DAY, FULL_DAY);
    }

    return this.handleDates({ endDate }, firstDayDuration, lastDayDuration);
  };

  handleDates = ({ startDate, endDate }, firstDayDuration, lastDayDuration) => {
    const start = startDate || this.state.startDate;
    let end = endDate || this.state.endDate;

    if (moment(start).isAfter(moment(end))) {
      end = start;
    }

    this.setState({
      startDate: start,
      endDate: end
    });

    return createDays(
      moment(start),
      moment(end),
      firstDayDuration,
      lastDayDuration
    );
  };

  checkIfNotHoliday = input => {
    return this.props.publicHolidays.find(holiday =>
      moment(holiday.date).isSame(input, 'day')
    )
      ? false
      : input;
  };

  checkIfNotValidYear = input => {
    if (!input) return false;

    return this.props.yearsWithAllowance.find(
      year => year === moment(input).year()
    )
      ? input
      : false;
  };

  checkIfNotWeekday = input => {
    if (input) {
      const dayOfWeek = moment(input).day();
      return dayOfWeek !== 0 && dayOfWeek !== 6;
    }
  };

  checkYearAndDay = _flow([
    this.checkIfNotHoliday,
    this.checkIfNotValidYear,
    this.checkIfNotWeekday
  ]);

  checkAbsenceType = absenceType =>
    absenceType === 'TRAINING'
      ? this.props.remainingTraining
      : this.props.remainingLeave;

  calculateRemainingDays = ({ days, absence: { absenceType, deducted } }) =>
    deducted
      ? this.checkAbsenceType(absenceType) - this.countTakenDays(days)
      : this.props.remainingLeave;

  shouldPopoverShow = values => {
    const remainingDays = this.calculateRemainingDays(values);

    return (
      _get(values.absence, 'absenceType') === LEAVE &&
      remainingDays < 0 &&
      remainingDays >= -this.props.nextYearAllowance / 8
    );
  };

  checkIfDisabled = (values, isSubmitting) => {
    if (!values.absence.id || !values.reason || isSubmitting) {
      return true;
    }

    if (values.absence.absenceType === LEAVE) {
      return (
        this.calculateRemainingDays(values) + this.props.nextYearAllowance / 8 <
        0
      );
    }

    return this.calculateRemainingDays(values) < 0;
  };

  countTakenDays = days => {
    let count = 0;

    days.forEach(day => {
      if (day.duration !== FULL_DAY) {
        count += 0.5;
      } else if (day.duration === FULL_DAY) {
        count += 1;
      }
      if (!this.checkIfNotHoliday(day.date)) {
        count -= 1;
      }
    });
    return count;
  };

  render() {
    return (
      <Formik
        validationSchema={validationSchema}
        initialValues={{
          user: this.props.user,
          absence:
            this.props.leaveTypes.length > 1
              ? {
                  id: ''
                }
              : this.props.leaveTypes[0],
          reason: '',
          requestStatus: 'PENDING',
          days: [
            {
              date: !this.props.selectedDay
                ? moment().format('YYYY-MM-DD')
                : this.props.selectedDay.format('YYYY-MM-DD'),
              duration: FULL_DAY,
              requestStatus: 'PENDING'
            }
          ],
          currentlyApprovedBy: []
        }}
        onSubmit={values =>
          this.props.dispatch(this.props.requestAction(values))
        }
        render={({
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue
        }) => (
          <Fragment>
            <div className="d-flex justify-content-between row mb-3">
              <div className="col-12 col-lg-6 pt-3 mb-3 mb-lg-0">
                {this.props.searchedEmployee && (
                  <div className="mb-2">
                    <div className="mb-4 row align-items-center">
                      <label
                        className="col-12 col-sm-4 text-center text-sm-left text-lg-right"
                        htmlFor="leaveType"
                      >
                        Request for
                      </label>
                      <div className="col-12 col-sm-8">
                        {this.props.searchedEmployee.fullName}
                      </div>
                    </div>
                  </div>
                )}
                {this.props.leaveTypes.length > 1 && (
                  <div className="mb-4 row align-items-center">
                    <label
                      className="col-12 col-sm-4 text-center mb-2 text-sm-left text-lg-right"
                      htmlFor="leaveType"
                    >
                      Type of Leave
                    </label>
                    <div className="col-12 col-sm-8">
                      <select
                        onChange={e => {
                          setFieldValue(
                            'absence',
                            handleSelectOption(
                              this.props.leaveTypes,
                              +e.target.value
                            )
                          );
                        }}
                        defaultValue=""
                        className="select-custom select-request pl-1 mr-2 mb-2 mb-md-0"
                      >
                        {renderOptions(this.props.leaveTypes)}
                      </select>
                      <ArrowSelect
                        style={{ top: '5px', right: '27px' }}
                        src={ArrowDown}
                        alt="arrow down"
                      />
                    </div>
                  </div>
                )}
                <div className="mb-4 row align-items-center">
                  <span className="col-12 col-sm-4 text-center mb-2 text-sm-left text-lg-right">
                    Start Date
                  </span>
                  <div className="col-12 col-sm-8">
                    <DatePicker
                      customInput={<CustomInput />}
                      className="border-left-0 border-top-0 border-right-0 border-bottom border-dark pl-2"
                      dateFormat="dd MMM yyyy"
                      locale="en-GB"
                      selected={this.state.startDate}
                      selectsStart
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                      showYearDropdown
                      onChange={e => {
                        setFieldValue(
                          'days',
                          this.selectStartDateHandler(
                            e,
                            values.days[0].duration,
                            _last(values.days).duration
                          )
                        );
                      }}
                      filterDate={
                        this.props.leaveTypes.length === 1
                          ? this.checkIfNotValidYear
                          : this.checkYearAndDay
                      }
                    />
                  </div>
                </div>
                <div className="mb-4 row align-items-center">
                  <span className="col-12 col-sm-4 text-center mb-2 text-sm-left text-lg-right">
                    End Date
                  </span>
                  <div className="col-12 col-sm-8">
                    <DatePicker
                      customInput={<CustomInput />}
                      className="border-left-0 border-top-0 border-right-0 border-bottom border-dark pl-2"
                      dateFormat="dd MMM yyyy"
                      locale="en-GB"
                      selected={this.state.endDate}
                      selectsEnd
                      showYearDropdown
                      shouldCloseOnSelect
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                      onChange={e => {
                        setFieldValue(
                          'days',
                          this.selectEndDateHandler(
                            e,
                            values.days[0].duration,
                            _last(values.days).duration
                          )
                        );
                      }}
                      filterDate={
                        this.props.leaveTypes.length === 1
                          ? this.checkIfNotValidYear
                          : this.checkYearAndDay
                      }
                    />
                  </div>
                </div>
                {values.days.length > 1 ? (
                  <Fragment>
                    <div className="d-flex justify-content-between mb-3">
                      <h5>First day afternoon only</h5>
                      <Switch
                        height={18}
                        width={35}
                        onChange={() =>
                          setFieldValue(
                            'days[0].duration',
                            values.days[0].duration === FULL_DAY
                              ? AFTERNOON
                              : FULL_DAY
                          )
                        }
                        checked={values.days[0].duration === AFTERNOON}
                        name="days[0].duration"
                      />
                    </div>
                    <div className="d-flex justify-content-between mb-3">
                      <h5>Last day morning only</h5>
                      <Switch
                        height={18}
                        width={35}
                        onChange={() =>
                          setFieldValue(
                            `days[${values.days.length - 1}].duration`,
                            values.days[values.days.length - 1].duration ===
                              FULL_DAY
                              ? MORNING
                              : FULL_DAY
                          )
                        }
                        checked={
                          values.days[values.days.length - 1].duration ===
                          MORNING
                        }
                        name={`days[${values.days.length - 1}].duration`}
                      />
                    </div>
                  </Fragment>
                ) : (
                  <Radio
                    options={[
                      {
                        label: 'Full day',
                        value: FULL_DAY,
                        action: () => {
                          setFieldValue(
                            'days',
                            setDayLength(FULL_DAY, values.days)
                          );
                        }
                      },
                      {
                        label: 'Morning only',
                        value: MORNING,
                        action: () => {
                          setFieldValue(
                            'days',
                            setDayLength(MORNING, values.days)
                          );
                        }
                      },
                      {
                        label: 'Afternoon only',
                        value: AFTERNOON,
                        action: () => {
                          setFieldValue(
                            'days',
                            setDayLength(AFTERNOON, values.days)
                          );
                        }
                      }
                    ]}
                  />
                )}
              </div>
              <div className="col-12 col-lg-6">
                <label className="w-100">
                  <h5 className="font-italic mb-1 ml-4 text-danger small">
                    Enter a reason for your leave request:
                  </h5>
                  <StyledTextarea
                    maxLength="255"
                    className={`${errors.reason &&
                      touched.reason &&
                      'border-danger'} mb-3 w-100 p-3`}
                    name="reason"
                    rows="8"
                    onChange={handleChange}
                  />
                </label>
              </div>
            </div>
            <div className="row">
              {_isNumber(_get(this.props, 'remainingLeave')) &&
                _get(values, 'absence.id') && (
                  <Fragment>
                    <div className="col-12 col-sm-4 text-center">
                      <AllowanceHeading className="mb-2">
                        Days requested
                      </AllowanceHeading>
                      <h5>{this.countTakenDays(values.days)}</h5>
                    </div>
                    <div className="col-12 col-sm-4 text-center">
                      <AllowanceHeading className="mb-2">
                        Current allowance remaining
                      </AllowanceHeading>
                      <h5>
                        {this.checkAbsenceType(values.absence.absenceType)}
                      </h5>
                    </div>
                    <div className="col-12 col-sm-4 text-center position-relative">
                      <ConfirmationPopover
                        popoverOpen={this.shouldPopoverShow(values)}
                        className="rounded shadow-lg border border-dark p-3 text-center position-absolute"
                      >
                        <h5>
                          This request will take
                          <DangerText className="text-danger mx-1">
                            {Math.abs(this.calculateRemainingDays(values)) < 5
                              ? Math.abs(this.calculateRemainingDays(values))
                              : 5}
                          </DangerText>
                          days from next year allowance
                        </h5>
                      </ConfirmationPopover>
                      <AllowanceHeading className="mb-2">
                        Allowance remaining (if approved)
                      </AllowanceHeading>
                      <h5
                        className={
                          this.calculateRemainingDays(values) < 0
                            ? 'text-danger'
                            : ''
                        }
                      >
                        {this.calculateRemainingDays(values)}
                      </h5>
                    </div>
                  </Fragment>
                )}
              <div className="col-12 text-right mt-3">
                <ButtonContainer className="position-absolute d-inline-block">
                  <Button
                    disabled={this.checkIfDisabled(
                      values,
                      this.props.isSubmitting
                    )}
                    title="Apply leave"
                    type="submit"
                    styling={{ rounded: true, fontColor: 'white' }}
                    click={handleSubmit}
                  />
                </ButtonContainer>
              </div>
            </div>
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  yearsWithAllowance: getYearsWithAllowance(state),
  isSubmitting: getIsSubmitting(state),
  searchedEmployee: getSearchedEmployee(state)
});

export default connect(
  mapStateToProps,
  null
)(RequestForm);
