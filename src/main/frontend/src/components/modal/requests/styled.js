import styled from 'styled-components';

export const DangerText = styled.span`
  font-size: 18px;
  font-weight: 600;
`;

export const ConfirmationPopover = styled.div`
  width: 260px;
  opacity: ${props => (props.popoverOpen ? '1' : '0')};
  pointer-events: ${props => (props.popoverOpen ? 'all' : 'none')};
  transition: 0.2s ease-in-out;
  right: 10px;
  z-index: 4;
  background: white;
  top: 0;
  transform: translateY(-100%);
`;

export const AllowanceHeading = styled.h5`
  font-size: 14px;
`;

export const ButtonContainer = styled.div`
  right: 10px;
`;
