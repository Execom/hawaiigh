import _flatten from 'lodash/flatten';
import moment from 'moment';
import React, { Fragment } from 'react';
import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
  reason: Yup.string()
    .required()
    .max(255),
  absence: Yup.object({
    id: Yup.string().required()
  })
});

export const createDays = (start, end, firstDayDuration, lastDayDuration) => {
  const numberOfDays = end.diff(start.startOf('day'), 'days');
  const days = [
    {
      date: start.format('YYYY-MM-DD'),
      duration: firstDayDuration,
      requestStatus: 'PENDING'
    }
  ];

  for (let i = 1; i <= numberOfDays; i++) {
    if (checkIfNotWeekday(moment(start).add(i, 'days'))) {
      days.push({
        date: moment(start)
          .add(i, 'days')
          .format('YYYY-MM-DD'),
        duration: i === numberOfDays ? lastDayDuration : 'FULL_DAY',
        requestStatus: 'PENDING'
      });
    }
  }

  return days;
};

export const checkIfNotWeekday = input => {
  if (input) {
    const dayOfWeek = moment(input).day();
    return dayOfWeek !== 0 && dayOfWeek !== 6;
  }
};

export const setDayLength = (dayLength, days) => {
  return days.map(day => {
    return (day = {
      ...day,
      duration: dayLength
    });
  });
};

export const handleSelectOption = (leaveTypes, selected) => {
  if (leaveTypes[0].items) {
    return _flatten(leaveTypes.map(leaveType => leaveType.items)).find(
      item => item.id === selected
    );
  }

  return leaveTypes.find(item => item.id === selected);
};

export const renderOptions = leaveTypes => {
  if (leaveTypes[0].items) {
    return (
      <Fragment>
        <option value="" key="default" className="d-none" disabled>
          Select leave type
        </option>
        {leaveTypes.map(group => {
          return (
            <optgroup key={group.name} label={group.name}>
              {group.items.map(item => (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              ))}
            </optgroup>
          );
        })}
      </Fragment>
    );
  }

  return (
    <Fragment>
      <option value="" key="default" className="d-none" disabled>
        Select leave type
      </option>
      {leaveTypes.map(item => (
        <option key={item.id} value={item.id}>
          {item.name}
        </option>
      ))}
    </Fragment>
  );
};
