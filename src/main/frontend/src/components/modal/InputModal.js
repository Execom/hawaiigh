import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Button, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { closeModal } from '../../store/actions/modalActions';
import { Formik } from 'formik';
import * as Yup from 'yup';
import {
  getAllowance,
  getLeaveTypes,
  getModal,
  getUser
} from '../../store/selectors';

class InputModal extends Component {
  render() {
    const {
      modal: {
        payload: { clickAction, values, message, insertKey, initialValues }
      },
      dispatch
    } = this.props;

    return (
      <Formik
        initialValues={initialValues}
        validationSchema={Yup.object().shape({
          input: Yup.string().required('Required')
        })}
        onSubmit={formikValues => {
          dispatch(clickAction({ ...values, [insertKey]: formikValues.input }));
        }}
        render={props => (
          <Fragment>
            <ModalHeader className="text-danger">Comment</ModalHeader>
            <ModalBody>
              <div className="mb-2">{message}</div>
              <form onSubmit={props.onSubmit}>
                <input
                  className="form-control"
                  type="text"
                  onChange={props.handleChange}
                  onBlur={props.handleBlur}
                  value={props.values.input}
                  name="input"
                />
                {props.errors.input && (
                  <div className="text-danger mt-1">{props.errors.input}</div>
                )}
              </form>
            </ModalBody>
            <ModalFooter>
              <Button
                color="primary"
                disabled={!props.dirty || props.isSubmitting}
                onClick={() => {
                  props.submitForm();
                }}
              >
                Submit
              </Button>
              <Button
                color="secondary"
                onClick={() => dispatch(closeModal('inputModal'))}
              >
                Cancel
              </Button>
            </ModalFooter>
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  user: getUser(state),
  leaveTypes: getLeaveTypes(state),
  allowance: getAllowance(state)
});

export default connect(mapStateToProps)(InputModal);
