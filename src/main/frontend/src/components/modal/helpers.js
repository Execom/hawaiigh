import React from 'react';
import * as Yup from 'yup';
import _lowerCase from 'lodash/lowerCase';
import _replace from 'lodash/replace';
import { FULL_DAY } from '@/helpers/enum';
import { reducerTotal } from '@/store/helperFunctions';
import ConfirmationModal from '@/components/modal/ConfirmationModal';
import HandleRequestModal from '@/components/modal/HandleRequestModal';
import InfoModal from '@/components/modal/InfoModal';
import RequestModal from '@/components/modal/RequestModal';
import RestoreEmployeeModal from '@/components/modal/RestoreEmployeeModal';
import TeamRequestsModal from '@/components/modal/TeamRequestsModal';
import InputModal from '@/components/modal/InputModal';
import DeleteEmployeeModal from '@/components/modal/DeleteEmployeeModal';
import CancelEmployeeDeleteModal from './CancelEmployeeDeleteModal';
import AllowanceCalendarModal from './AllowanceCalendarModal';
import AddAdminModal from './AddAdminModal';

export const modalType = type => {
  switch (type) {
    case 'handleRequestModal':
      return <HandleRequestModal />;
    case 'confirmationModal':
      return <ConfirmationModal />;
    case 'requestModal':
      return <RequestModal />;
    case 'infoModal':
      return <InfoModal />;
    case 'teamRequestsModal':
      return <TeamRequestsModal />;
    case 'employeeRestoreModal':
      return <RestoreEmployeeModal />;
    case 'employeeDeleteModal':
      return <DeleteEmployeeModal />;
    case 'employeeCancelDeleteModal':
      return <CancelEmployeeDeleteModal />;
    case 'inputModal':
      return <InputModal />;
    case 'allowanceCalendarModal':
      return <AllowanceCalendarModal />;
    case 'addAdminModal':
      return <AddAdminModal />;
    default:
      return null;
  }
};

export const restoreEmployeeValidationSchema = Yup.object().shape({
  teamId: Yup.string().required()
});

export const teamsList = teams =>
  teams.map(team => {
    return (
      <option key={team.id} value={team.id}>
        {team.name}
      </option>
    );
  });

export const takenHours = days =>
  days.map(day => (day.duration === FULL_DAY ? 8 : 4)).reduce(reducerTotal, 0);

export const getApprovers = approvers =>
  approvers.map((approver, index) => (
    <h5 key={index} className={approvers.length === 1 ? '' : 'mb-1'}>
      {approver.fullName}
    </h5>
  ));

export const getRequestStatus = requestStatus =>
  requestStatus === 'CANCELLATION_PENDING'
    ? 'cancellation'
    : _lowerCase(_replace(requestStatus, '_', ' '));

export const constructStatus = requestStatus =>
  `${
    requestStatus === 'CANCELLATION_PENDING' ? 'Requested' : 'Request'
  } ${getRequestStatus(requestStatus)}`;
