import { Formik } from 'formik';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import { ModalBody, ModalHeader } from 'reactstrap';
import DrawerCalendarInput, {
  InputPair
} from '../../pages/administration/components/common/DrawerElements';
import { requestLeaveProfiles } from '../../store/actions/leaveProfileActions';
import { closeModal } from '../../store/actions/modalActions';
import { requestTeams } from '../../store/actions/teamActions';
import {
  getAllowance,
  getIsSubmitting,
  getLeaveProfileResults,
  getModal,
  getTeams,
  getUser
} from '../../store/selectors';
import Button from '../common/Button';
import { restoreEmployeeValidationSchema, teamsList } from './helpers';

class RestoreEmployeeModal extends Component {
  componentDidMount() {
    this.props.dispatch(requestTeams());
    this.props.dispatch(requestLeaveProfiles());
  }

  render() {
    const {
      modal: {
        payload: {
          values: { fullName },
          values,
          clickAction,
          options: { restoreWithoutDate, isScheduleRestore }
        }
      },
      teams,
      dispatch,
      leaveProfiles,
      user: { stoppedWorkingAtExecomDate }
    } = this.props;

    const leaveProfilesListItems = leaveProfiles.map(leaveProfile => {
      return (
        <option key={leaveProfile.id} value={leaveProfile.id}>
          {leaveProfile.name}
        </option>
      );
    });

    return (
      <Fragment>
        <ModalHeader className="text-danger">
          {isScheduleRestore
            ? `Schedule restoring for ${fullName}`
            : `Activate ${fullName}`}
        </ModalHeader>
        <ModalBody>
          <Formik
            validationSchema={restoreEmployeeValidationSchema}
            initialValues={{
              ...values,
              startedWorkingAtExecomDate: moment().toDate(),
              teamId: '',
              stoppedWorkingAtExecomDate
            }}
            onSubmit={values =>
              dispatch(
                clickAction({
                  ...values,
                  startedWorkingAtExecomDate: moment(
                    values.startedWorkingAtExecomDate
                  ).format('YYYY-MM-DD'),
                  stoppedWorkingAtExecomDate: null
                })
              )
            }
            render={({
              handleSubmit,
              handleChange,
              values,
              errors,
              touched,
              setFieldValue,
              isSubmitting
            }) => (
              <Fragment>
                <InputPair
                  view="select"
                  caption="Team"
                  className={
                    errors.teamId && touched.teamId ? 'border-danger' : ''
                  }
                  name="teamId"
                  handleChange={handleChange}
                  value={values.teamId}
                  disabledValue="Select team"
                  options={teamsList(teams.results)}
                />
                <InputPair
                  view="select"
                  caption="Leave profile"
                  className={
                    errors.leaveProfileId && touched.leaveProfileId
                      ? 'border-danger'
                      : ''
                  }
                  name="leaveProfileId"
                  handleChange={handleChange}
                  value={values.leaveProfileId}
                  disabledValue="Select leave profile"
                  options={leaveProfilesListItems}
                />
                {!restoreWithoutDate && (
                  <InputPair
                    view="other"
                    caption="Started working at execom date"
                    element={
                      <DatePicker
                        customInput={<DrawerCalendarInput />}
                        className={
                          errors.startedWorkingAtExecomDate &&
                          touched.startedWorkingAtExecomDate
                            ? 'border-danger'
                            : ''
                        }
                        showYearDropdown
                        dateFormat="dd MMM yyyy"
                        locale="en-GB"
                        selected={values.startedWorkingAtExecomDate}
                        onChange={e => {
                          setFieldValue('startedWorkingAtExecomDate', e);
                        }}
                      />
                    }
                  />
                )}
                <div className="d-flex justify-content-end border-top pt-2">
                  <Button
                    type="submit"
                    className="px-4 py-2 mr-2"
                    title="Cancel"
                    click={() => dispatch(closeModal('employeeRestoreModal'))}
                    styling={{ color: 'white' }}
                  />
                  <Button
                    type="submit"
                    className="px-4 py-2"
                    disabled={this.props.isSubmitting}
                    title={isScheduleRestore ? 'Schedule Restore' : 'Activate'}
                    click={handleSubmit}
                    styling={{ fontColor: 'white' }}
                  />
                </div>
              </Fragment>
            )}
          />
        </ModalBody>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  user: getUser(state),
  leaveProfiles: getLeaveProfileResults(state),
  allowance: getAllowance(state),
  teams: getTeams(state),
  isSubmitting: getIsSubmitting(state)
});

export default connect(mapStateToProps)(RestoreEmployeeModal);
