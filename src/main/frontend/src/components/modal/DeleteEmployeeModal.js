import React, { Component, Fragment } from 'react';
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import { ModalBody, ModalHeader } from 'reactstrap';
import { Formik } from 'formik';
import moment from 'moment';
import DrawerCalendarInput, {
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import { closeModal } from '@/store/actions/modalActions';
import { getIsSubmitting, getModal, getUser } from '@/store/selectors';
import Button from '@/components/common/Button';
import { bindActionCreators } from 'redux';
import { removeEmployee } from '@/store/actions/userActions';

class DeleteEmployeeModal extends Component {
  getDatePickerClassName(errors, touched) {
    return errors.stoppedWorkingAtExecomDate &&
      touched.stoppedWorkingAtExecomDate
      ? 'border-danger'
      : '';
  }

  render() {
    const {
      modal: {
        payload: { values, clickAction, message }
      },
      removeEmployee,
      closeModal
    } = this.props;
    return (
      <Fragment>
        <ModalHeader className="text-danger">{message}</ModalHeader>
        <ModalBody>
          <Formik
            initialValues={{
              ...values,
              message,
              stoppedWorkingAtExecomDate: moment().toDate()
            }}
            onSubmit={values => {
              clickAction({
                ...values,
                stoppedWorkingDate: moment(
                  values.stoppedWorkingAtExecomDate
                ).format('YYYY-MM-DD')
              });
              removeEmployee({
                ...values,
                stoppedWorkingDate: moment(
                  values.stoppedWorkingAtExecomDate
                ).format('YYYY-MM-DD')
              });
            }}
            render={({
              handleSubmit,
              values,
              errors,
              touched,
              setFieldValue
            }) => (
              <Fragment>
                <InputPair
                  view="other"
                  caption="Select stopped working at Execom date"
                  element={
                    <DatePicker
                      customInput={<DrawerCalendarInput />}
                      className={this.getDatePickerClassName(errors, touched)}
                      showYearDropdown
                      dateFormat="dd MMM yyyy"
                      locale="en-GB"
                      selected={values.stoppedWorkingAtExecomDate}
                      onChange={e => {
                        setFieldValue('stoppedWorkingAtExecomDate', e);
                      }}
                    />
                  }
                />
                <div className="d-flex justify-content-end border-top pt-2">
                  <Button
                    type="submit"
                    className="px-4 py-2 mr-2"
                    title="Cancel"
                    click={() => closeModal('employeeDeleteModal')}
                    styling={{ color: 'white' }}
                  />
                  <Button
                    type="submit"
                    className="px-4 py-2"
                    disabled={this.props.isSubmitting}
                    title="Delete"
                    click={handleSubmit}
                    styling={{ fontColor: 'white' }}
                  />
                </div>
              </Fragment>
            )}
          />
        </ModalBody>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modal: getModal(state),
  user: getUser(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      removeEmployee,
      closeModal
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteEmployeeModal);
