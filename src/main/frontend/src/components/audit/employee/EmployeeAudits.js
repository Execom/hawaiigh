import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Collapse } from 'reactstrap';
import { requestEmployeeAudit } from '@/store/actions/auditActions';
import { getAudit, getTeams, getLeaveProfileResults } from '@/store/selectors';
import EmployeeAudit from '@/components/audit/employee/EmployeeAudit';
import { DrawerContent } from '@/pages/administration/components/common/DrawerElements';
import ExpandableAudits from '@/components/audit/ExpandableAudits';

class EmployeeAudits extends Component {
  state = {
    isOpen: false
  };

  componentDidMount() {
    this.props.requestEmployeeAudit(this.props.id);
  }

  showAuditInfo = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  render() {
    const {
      audit,
      teams: { results: teams },
      leaveProfiles
    } = this.props;

    const { isOpen } = this.state;

    if (!audit.employee || !teams.length || !leaveProfiles.length) return null;

    return (
      <DrawerContent disableGrow className="px-4 mt-4">
        <ExpandableAudits
          showAuditInfo={this.showAuditInfo}
          type="Employee"
          open={isOpen}
        />
        <Collapse isOpen={isOpen}>
          {audit.employee.map(auditSingle => (
            <EmployeeAudit
              className="p-4"
              key={auditSingle.modifiedDateTime}
              audit={auditSingle}
            />
          ))}
          {audit.employee.length === 0 && (
            <div className="p-4">This employee has no edit history.</div>
          )}
        </Collapse>
      </DrawerContent>
    );
  }
}

const mapStateToProps = state => ({
  audit: getAudit(state),
  teams: getTeams(state),
  leaveProfiles: getLeaveProfileResults(state)
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      requestEmployeeAudit
    },
    dispatch
  ),
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeAudits);
