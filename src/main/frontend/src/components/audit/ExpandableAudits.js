import React from 'react';
import styled from 'styled-components';
import { Icon } from '@/pages/requests/components/styled';
import ArrowIcon from '@/img/icons/arrow-down.svg';

const ExpandableAudit = styled.label`
  padding: 10px 5px;
  border: 1px solid #d3d3d3;
  border-radius: 5px;
  box-shadow: 1px 1px 5px #d3d3d3;
  width: 100%;
  cursor: pointer;
  text-align: center;
  margin-bottom: 10px;
  position: relative;
`;

const ExpandableAudits = ({ type, open, showAuditInfo }) => {
  return (
    <ExpandableAudit onClick={showAuditInfo}>
      <h4 className="font-weight-bold my-2">{type} edit history</h4>
      <Icon
        open={open}
        className="employee-audit-arrow"
        size="15px"
        src={ArrowIcon}
        alt="Arrow for toggling accordion item"
      />
    </ExpandableAudit>
  );
};

export default ExpandableAudits;
