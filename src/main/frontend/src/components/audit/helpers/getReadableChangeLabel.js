export default field =>
  ({
    fullName: 'Name',
    teamName: 'Team Name',
    approversInfo: 'Team Approvers',
    email: 'Email',
    teamId: 'Team',
    leaveProfileId: 'Leave Profile',
    jobTitle: 'Job Title',
    userRole: 'User Role',
    startedWorkingDate: 'Started Working',
    startedProfessionalCareerDate: 'Started Professional Career',
    startedWorkingAtExecomDate: 'Started Working At Execom',
    stoppedWorkingAtExecomDate: 'Stopped Working At Execom',
    comment: 'Comment',
    manualAdjust: 'Manual adjust',
    trainingManualAdjust: 'Training',
    bonusManualAdjust: 'Bonus',
    carriedOver: 'Carried over',
    yearsOfService: 'Years of service',
    userStatusType: 'User status type'
  }[field]);
