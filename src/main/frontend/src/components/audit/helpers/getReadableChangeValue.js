import moment from 'moment';
import { getLeaveProfileResults, getTeams } from '@/store/selectors.js';
import store from '@/store/store.js';
import {
  showOnlyOneDecimal,
  parseTeamApproversStringIntoArray,
  wrapEverySecondElementToParentheses
} from '@/pages/administration/helpers.js';

const findTeam = id => {
  const team = getTeams(store.getState()).results.find(team => team.id === +id);
  return team ? team.name : 'Archived Users';
};

const findLeaveProfile = id =>
  getLeaveProfileResults(store.getState()).find(lp => lp.id === +id).name;

export default (field, payload, isNewValue = false) => {
  if (!payload) {
    if (isNewValue) {
      return '-';
    }
    return '';
  }

  switch (field) {
    case 'fullName':
      return payload;
    case 'email':
      return payload;
    case 'jobTitle':
      return payload;
    case 'teamId':
      return findTeam(payload);
    case 'leaveProfileId':
      return findLeaveProfile(payload);
    case 'startedWorkingDate':
      return moment(payload, 'YYYY-MM-DD').format('DD.MM.YYYY');
    case 'startedProfessionalCareerDate':
      return moment(payload, 'YYYY-MM-DD').format('DD.MM.YYYY');
    case 'startedWorkingAtExecomDate':
      return moment(payload, 'YYYY-MM-DD').format('DD.MM.YYYY');
    case 'stoppedWorkingAtExecomDate':
      return moment(payload, 'YYYY-MM-DD').isValid()
        ? moment(payload, 'YYYY-MM-DD').format('DD.MM.YYYY')
        : payload;
    case 'comment':
      return payload;
    case 'manualAdjust':
    case 'trainingManualAdjust':
    case 'bonusManualAdjust':
    case 'carriedOver':
      return `${showOnlyOneDecimal(payload / 8)} ${
        payload / 8 === 1 ? 'day' : 'days'
      }`;
    case 'teamName':
      return payload;
    case 'approversInfo':
      return wrapEverySecondElementToParentheses(
        parseTeamApproversStringIntoArray(payload)
      );

    default:
      return payload;
  }
};
