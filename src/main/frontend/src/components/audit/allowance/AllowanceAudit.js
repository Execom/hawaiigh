import React from 'react';
import Audits from '@/components/audit/Audits';

export default props => {
  const { audit } = props;

  return <Audits audit={audit} />;
};
