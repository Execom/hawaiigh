import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Collapse } from 'reactstrap';
import AllowanceAudit from '@/components/audit/allowance/AllowanceAudit';
import { getAudit, getLeaveProfileResults } from '@/store/selectors';
import { requestAllowanceAudit } from '@/store/actions/auditActions';
import { DrawerContent } from '@/pages/administration/components/common/DrawerElements';
import ExpandableAudits from '@/components/audit/ExpandableAudits';

class AllowanceAudits extends Component {
  state = {
    isOpen: false
  };

  componentDidMount() {
    this.props.requestAllowanceAudit(this.props.id);
  }

  showAuditInfo = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  render() {
    const { audit, leaveProfiles } = this.props;

    const { isOpen } = this.state;

    if (!audit.allowance || !leaveProfiles.length) return null;

    return (
      <DrawerContent disableGrow className="px-4">
        <ExpandableAudits
          showAuditInfo={this.showAuditInfo}
          type="Allowance"
          open={isOpen}
        />
        <Collapse isOpen={isOpen}>
          {audit.allowance.map(auditSingle => (
            <AllowanceAudit
              className="p-4"
              key={auditSingle.modifiedDateTime}
              audit={auditSingle}
            />
          ))}
          {audit.allowance.length === 0 && (
            <div className="p-4">
              This user has no edit history for allowance manual adjust.
            </div>
          )}
        </Collapse>
      </DrawerContent>
    );
  }
}

const mapStateToProps = state => ({
  audit: getAudit(state),
  leaveProfiles: getLeaveProfileResults(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestAllowanceAudit
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllowanceAudits);
