import React from 'react';
import styled from 'styled-components';
import getReadableChangeLabel from '@/components/audit/helpers/getReadableChangeLabel';
import getReadableChangeValue from '@/components/audit/helpers/getReadableChangeValue';
import { formatDateAndTime } from '@/store/helperFunctions';

const StatusHeading = styled.div`
  background: #ededed;
`;

const Audits = ({
  audit: {
    id,
    operationPerformed,
    modifierEmail,
    modifiedDateTime,
    changesPerformed
  }
}) => {
  return (
    <div
      key={id}
      className="d-flex justify-content-between my-2 py-2 flex-column"
    >
      <StatusHeading className="d-flex flex-column justify-content-center p-2 rounded-top">
        <span className="">{operationPerformed}</span>
        <div className="text-left d-flex flex-row justify-content-between">
          <span className="small text-black-50">
            {modifierEmail}, {formatDateAndTime(modifiedDateTime)}
          </span>
        </div>
      </StatusHeading>
      <div className="d-flex flex-column-reverse pt-3 px-2 border border-top-0 rounded-bottom">
        {changesPerformed.map(change => (
          <div
            className="mb-4 d-flex flex-column align-items-center"
            key={change.fieldName}
          >
            <span className="small text-uppercase font-weight-bold mb-1">
              {getReadableChangeLabel(change.fieldName)}
            </span>
            <div className="text-center">
              <div>
                {getReadableChangeValue(
                  change.fieldName,
                  change.oldObjectValue
                )}
              </div>
              <div className="lead">
                {change.oldObjectValue !== '' && change.oldObjectValue !== '[]'
                  ? ' ↓ '
                  : ''}
              </div>
              <div>
                {getReadableChangeValue(
                  change.fieldName,
                  change.newObjectValue,
                  true
                )}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Audits;
