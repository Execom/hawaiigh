import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TeamAudit from './TeamAudit';
import { getAudit } from '../../../store/selectors';
import { requestTeamAudit } from '../../../store/actions/auditActions';
import { DrawerContent } from '../../../pages/administration/components/common/DrawerElements';

class TeamAudits extends Component {
  componentDidMount() {
    this.props.requestTeamAudit(this.props.id);
  }

  render() {
    const { audit } = this.props;

    if (!audit.team) return null;

    return (
      <DrawerContent className="p-4">
        <h4 className="font-weight-bold my-2">Team edit history</h4>
        {audit.team.map(auditSingle => (
          <TeamAudit key={auditSingle.modifiedDateTime} audit={auditSingle} />
        ))}
        {audit.team.length === 0 && <div>This team has no edit history.</div>}
      </DrawerContent>
    );
  }
}

const mapStateToProps = state => ({
  audit: getAudit(state)
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      requestTeamAudit
    },
    dispatch
  ),
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamAudits);
