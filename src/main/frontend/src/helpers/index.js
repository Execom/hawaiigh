import * as Yup from 'yup';
import { getPublicPathname } from '@/env';

export const validateEmail = email => {
  try {
    return Yup.string()
      .email()
      .test('is-execom-email', 'Email is not in execom domain.', value =>
        value.endsWith('@execom.eu')
      )
      .validateSync(email);
  } catch (error) {
    return false;
  }
};

export const getBackendLoginUrl = () =>
  process.env.NODE_ENV === 'development'
    ? `http://${
        window.location.hostname
      }:8080/oauth2/authorize/google?redirect_uri=http://${
        window.location.hostname
      }:3000/`
    : `${getPublicPathname()}/oauth2/authorize/google?redirect_uri=${
        window.location.origin
      }${getPublicPathname()}/`;
