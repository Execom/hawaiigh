export default () => {
  const token = new URL(window.location).searchParams.get('token');

  if (token) localStorage.setItem('token', token);
};
