export const APPROVED = 'APPROVED';
export const REJECTED = 'REJECTED';
export const PENDING = 'PENDING';
export const CANCELED = 'CANCELED';
export const CANCELLATION_PENDING = 'CANCELLATION_PENDING';

export const FULL_DAY = 'FULL_DAY';
export const MORNING = 'MORNING';
export const AFTERNOON = 'AFTERNOON';

export const TRAINING = 'TRAINING';
export const LEAVE = 'LEAVE';
export const SICKNESS = 'SICKNESS';
export const BONUS_DAYS = 'BONUS_DAYS';
export const BONUS = 'BONUS';

export const IS_LEAVE = 'isLeave';
export const IS_SICKNESS = 'isSickness';
export const IS_BONUS = 'isBonus';

export const REQUEST_MODAL = 'requestModal';
export const HANDLE_REQUEST_MODAL = 'handleRequestModal';

export const HR_MANAGER = 'HR_MANAGER';
export const USER = 'USER';
export const OFFICE_MANAGER = 'OFFICE_MANAGER';

export const DELETED = 'DELETED';
export const ACTIVE = 'ACTIVE';
export const INACTIVE = 'INACTIVE';

export const LEAVE_REQUESTS_REPORT = 'LEAVE_REQUESTS_REPORT';
export const SICKNESS_REPORT = 'SICKNESS_REPORT';
export const USERS_REPORT = 'USERS_REPORT';
export const ALLOWANCES_REPORT = 'ALLOWANCES_REPORT';
export const LEAVE_PROFILES_REPORT = 'LEAVE_PROFILES_REPORT';

export const USER_FULL_NAME_LABEL = 'User Full Name';
export const TEAM_NAME_LABEL = 'User Team Name';
export const USER_CURRENT_STATUS_LABEL = 'User Status';
export const USER_STARTED_WORKING_DATE_LABEL = 'Started Working Date';
export const ROLE_LABEL = 'User Role';
export const REQUEST_CURRENT_STATUS_LABEL = 'Request Status';
export const ABSENCE_TYPE_LABEL = 'Absence Type';
export const LEAVE_START_DATE_LABEL = 'Leave Start Date';
export const TAKEN_ALLOWANCE_LABEL = 'Taken Days';

export const USER_FULL_NAME = 'fullName';
export const USER_TEAM_NAME = 'team.name';
export const USER_STATUS = 'status';
export const USER_STARTED_WORKING_AT_EXECOM_DATE = 'startedWorkingAtExecomDate';
export const USER_ROLE = 'role';
export const REQUEST_USER_FULL_NAME = 'user.fullName';
export const REQUEST_USER_TEAM_NAME = 'user.team.name';
export const REQUEST_STATUS = 'requestStatus';
export const REQUEST_ABSENCE_TYPE = 'absence.absenceType';
export const REQUEST_START_DATE = 'day.date';
export const ALLOWANCE_USER_FULL_NAME = 'user.fullName';
export const ALLOWANCE_TAKEN_ANNUAL = 'takenAnnual';

export const ASC = 'ASC';
export const DESC = 'DESC';
