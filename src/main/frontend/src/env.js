export const getPublicPathname = () => {
  const publicUrl = process.env.REACT_APP_PUBLIC_URL;

  if (process.env.NODE_ENV === 'production' && publicUrl) {
    const pathname = new URL(publicUrl).pathname;

    if (pathname !== '/') {
      return pathname;
    }
    return '';
  }

  return '';
};

export const apiPrefix = `${getPublicPathname()}/api`;
