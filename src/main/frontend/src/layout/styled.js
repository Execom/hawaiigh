import styled from 'styled-components';
import { ifProp } from 'styled-tools';

export const PanelContent = styled.div`
  overflow: auto;
  background: #ededed;
`;

export const Container = styled.div`
  pointer-events: ${ifProp('drawerOpen', 'none', 'all')};
`;

export const LoadingOverlay = styled.div`
  opacity: ${ifProp('isLoading', '1', '0')};
  transition: 0.2s ease-in-out;
  pointer-events: none;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 9999;
`;
