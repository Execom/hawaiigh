import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Administration from '../pages/administration/Administration';
import Approvals from '../pages/approvals/Approvals';
import Dashboard from '../pages/dashboard/Dashboard';
import DataExport from '../pages/data-export/DataExport';
import AdminDashboard from '../pages/application-admin/AdminDashboard';
import { isMobile } from '@/components/navigation/helpers';

const userRoutes = isApprover => [
  { path: '/(dashboard|team-calendar|requests)', component: Dashboard },
  ...(isApprover ? [{ path: '/approvals', component: Approvals }] : [])
];

const hrManagerRoutes = isApprover =>
  isMobile()
    ? [...userRoutes(isApprover)]
    : [
        ...userRoutes(isApprover),
        { path: '/administration', component: Administration },
        { path: '/data-export', component: DataExport }
      ];

const officeManagerRoutes = isApprover =>
  isMobile()
    ? [...userRoutes(isApprover)]
    : [
        ...userRoutes(isApprover),
        { path: '/data-export', component: DataExport }
      ];

const adminRouts = isMobile()
  ? [...userRoutes(false)]
  : [{ path: '/admin-dashboard', component: AdminDashboard }];

export const routes = {
  HR_MANAGER: hrManagerRoutes,
  OFFICE_MANAGER: officeManagerRoutes,
  USER: userRoutes,
  ADMIN: adminRouts
};

const adminRoute = adminRouts[0];

export const generateRoutes = user => {
  const { userRole, isApprover, userAdminPermission } = user;

  return (
    <Switch>
      {[
        routes[userRole](isApprover).map(route => (
          <Route
            key={route.path}
            path={route.path}
            component={route.component}
            exact
          />
        )),
        ['SUPER', 'BASIC'].includes(userAdminPermission) && (
          <Route
            key={adminRoute.path}
            path={adminRoute.path}
            component={adminRoute.component}
            exact
          />
        )
      ]}
      <Redirect to="/dashboard" />
    </Switch>
  );
};
