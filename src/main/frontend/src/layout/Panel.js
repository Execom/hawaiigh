import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { replace } from 'connected-react-router';
import ActionDrawer from '@/components/drawer-menu/ActionDrawer';
import MenuDrawer from '@/components/drawer-menu/MenuDrawer';
import TopHeader from '@/components/header/TopHeader';
import Loading from '@/components/loading/Loading';
import Sidebar from '@/components/navigation/Sidebar';
import { USER } from '@/helpers/enum';
import { requestRequestsForApproval } from '@/store/actions/requestsActions';
import { requestUser } from '@/store/actions/userActions';
import {
  getLoading,
  getMenuDrawer,
  getSearchedEmployee,
  getUser,
  getRedirectAfterLoginRoute
} from '@/store/selectors';
import { removeRedirectAfterLoginRoute } from '@/store/actions/redirectAfterLoginActions';
import { generateRoutes } from '@/layout/helpers';
import { Container, LoadingOverlay, PanelContent } from '@/layout/styled';
import ScrollDetector from '@/components/HOC/ScrollDetector';

class Panel extends Component {
  componentDidUpdate(prevProps) {
    if (prevProps.user !== this.props.user && this.props.user !== USER) {
      this.props.requestRequestsForApproval();
    }
  }

  render() {
    return (
      <div className="root-wrapper h-100">
        {this.props.user && <TopHeader user={this.props.user} />}
        <Sidebar className="w-100 h-10 d-none d-lg-flex" />
        <MenuDrawer />
        <ActionDrawer />
        <LoadingOverlay
          className="d-flex position-absolute"
          isLoading={!!this.props.loading}
        >
          <Loading />
        </LoadingOverlay>
        <Container
          menuDrawerOpen={this.props.menuDrawerOpen}
          className="d-flex flex-column justify-content-start flex-grow-1 h-100 overflow-hidden"
        >
          <ScrollDetector
            component={
              <PanelContent className="d-flex flex-column flex-grow-1 h-100 pb-3">
                <Switch>
                  {this.props.user && generateRoutes(this.props.user)}
                </Switch>
              </PanelContent>
            }
          />
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: getUser(state),
  loading: getLoading(state),
  searchedEmployee: getSearchedEmployee(state),
  menuDrawerOpen: getMenuDrawer(state),
  redirectAfterLoginRoute: getRedirectAfterLoginRoute(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      replace,
      requestUser,
      requestRequestsForApproval,
      removeRedirectAfterLoginRoute
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Panel);
