import * as Yup from 'yup';

export const createValidationSchema = Yup.object().shape({
  name: Yup.string().required('Name is a required field.'),
  absenceType: Yup.string().required('Absence type is a required field.'),
  iconId: Yup.string().required('Please select an icon.')
});

export const editValidationSchema = Yup.object().shape({
  name: Yup.string().required('Name is a required field.')
});
