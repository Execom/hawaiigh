import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withResetOnNavigate from '../../../../components/HOC/withResetOnNavigate';
import { requestLeaveTypes } from '../../../../store/actions/absenceActions';
import { openActionDrawer } from '../../../../store/actions/drawerActions';
import { getLeaveTypes } from '../../../../store/selectors';
import AdministrationTable from '../common/AdministrationTable';
import LeaveTypeItem from './LeaveTypeItem';

class LeaveTypes extends Component {
  componentDidMount() {
    this.props.requestLeaveTypes({
      size: 30,
      page: 0
    });
  }

  render() {
    const { results, last, page } = this.props.leaveTypes;

    const leaveTypesItems = results.length
      ? results.map(item => {
          return <LeaveTypeItem key={item.id} leaveType={item} />;
        })
      : null;

    return (
      <AdministrationTable
        headerItems={['Name', 'Comment', 'Type']}
        body={leaveTypesItems}
        page={page}
        clickAction={() =>
          this.props.openActionDrawer({
            type: 'createLeaveType',
            heading: 'Create Leave Type'
          })
        }
        requestAction={requestLeaveTypes}
        placeholderCellCount={3}
        last={last}
      />
    );
  }
}

const mapStateToProps = state => ({
  leaveTypes: getLeaveTypes(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestLeaveTypes, openActionDrawer }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(LeaveTypes));
