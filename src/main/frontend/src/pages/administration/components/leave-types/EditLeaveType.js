import { Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IconsList } from '@/components/common/IconsList';
import {
  clearLeaveType,
  requestLeaveType,
  requestLeaveTypeIcons,
  updateLeaveType
} from '@/store/actions/absenceActions';
import { closeDrawer } from '@/store/actions/drawerActions';
import {
  getIsSubmitting,
  getLeaveType,
  getLeaveTypesIcons
} from '@/store/selectors';
import {
  DrawerContent,
  DrawerFooter,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import { editValidationSchema } from '@/pages/administration/components/leave-types/helpers';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';

class EditLeaveType extends Component {
  componentWillMount() {
    this.props.clearLeaveType();
  }

  componentDidMount() {
    this.props.requestLeaveTypeIcons();
    this.props.requestLeaveType(this.props.id);
  }

  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    if (!this.props.leaveType || !this.props.leaveTypeIcons) return null;

    const leaveTypeIcons = (setFieldValue, iconId) =>
      this.props.leaveTypeIcons.map(icon => {
        return (
          <IconsList
            isSelected={iconId === icon.id}
            key={icon.id}
            className="mr-3"
            width="30"
            onClick={() => setFieldValue('iconId', icon.id)}
            src={`/${icon.iconUrl}`}
            alt={icon.iconUrl}
          />
        );
      });

    return (
      <Formik
        validationSchema={editValidationSchema}
        initialValues={this.props.leaveType}
        onSubmit={this.props.updateLeaveType}
        enableReinitialize
        render={({
          initialValues,
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                caption="Leave type name"
                defaultValue={values.name}
                name="name"
                handleChange={handleChange}
                className={errors.name && touched.name ? 'border-danger' : ''}
              />
              <InputPair
                view="textArea"
                defaultValue={values.comment}
                caption="Comment"
                name="comment"
                handleChange={handleChange}
              />
              <InputPair
                view="switch"
                caption="Deducted"
                disabled={
                  values.absenceType === 'SICKNESS' ||
                  values.absenceType === 'TRAINING'
                }
                name="deducted"
                handleChange={() => setFieldValue('deducted', !values.deducted)}
                checked={values.deducted}
              />
              <label
                className={`${
                  errors.iconId && touched.iconId ? 'text-danger' : 'text-muted'
                }  mb-2`}
              >
                Select icon
              </label>
              <div className="mb-3">
                {leaveTypeIcons(setFieldValue, values.iconId)}
              </div>
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting || values === initialValues}
              formClick={handleSubmit}
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  leaveType: getLeaveType(state),
  leaveTypeIcons: getLeaveTypesIcons(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestLeaveType,
      updateLeaveType,
      requestLeaveTypeIcons,
      clearLeaveType,
      closeDrawer,
      toastrError
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditLeaveType);
