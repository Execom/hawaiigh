import { Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IconsList } from '@/components/common/IconsList';
import {
  createLeaveType,
  requestLeaveType,
  requestLeaveTypeIcons
} from '@/store/actions/absenceActions';
import { closeDrawer } from '@/store/actions/drawerActions';
import {
  getIsSubmitting,
  getLeaveType,
  getLeaveTypesIcons
} from '@/store/selectors';
import {
  DrawerContent,
  DrawerFooter,
  InputPair,
  WhiteBackgroundDiv
} from '@/pages/administration/components/common/DrawerElements';
import { createValidationSchema } from '@/pages/administration/components/leave-types/helpers';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';

const leaveTypes = [
  { id: 1, type: 'LEAVE', name: 'Leave' },
  { id: 2, type: 'SICKNESS', name: 'Sickness' }
];

class CreateLeaveType extends Component {
  componentDidMount() {
    this.props.requestLeaveTypeIcons();
  }

  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    const leaveTypeOptions = leaveTypes.map(leaveType => {
      return (
        <option key={leaveType.id} value={leaveType.type}>
          {leaveType.name}
        </option>
      );
    });

    if (!this.props.leaveTypeIcons) return null;

    const leaveTypeIcons = (setFieldValue, iconId) =>
      this.props.leaveTypeIcons.map(icon => {
        return (
          <IconsList
            isSelected={iconId === icon.id}
            key={icon.id}
            className="mr-3"
            width="30"
            onClick={() => setFieldValue('iconId', icon.id)}
            src={`/${icon.iconUrl}`}
            alt={icon.iconUrl}
          />
        );
      });

    return (
      <Formik
        validationSchema={createValidationSchema}
        initialValues={{
          absenceType: '',
          name: '',
          comment: '',
          iconId: '',
          deducted: false
        }}
        onSubmit={this.props.createLeaveType}
        enableReinitialize
        render={({
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                caption="Leave type name"
                name="name"
                handleChange={handleChange}
                className={errors.name && touched.name ? 'border-danger' : ''}
              />
              <InputPair
                view="textArea"
                caption="Comment"
                name="comment"
                handleChange={handleChange}
              />
              <InputPair
                view="select"
                caption="Absence type"
                className={`${
                  errors.absenceType && touched.absenceType
                    ? 'border-danger'
                    : ''
                } mb-3 border`}
                name="absenceType"
                handleChange={e => {
                  handleChange(e);
                  if (e.target.value === 'SICKNESS') {
                    setFieldValue('deducted', false);
                  }
                }}
                value={values.absenceType}
                disabledValue="Select absence type"
                options={leaveTypeOptions}
              />
              <InputPair
                view="switch"
                caption="Deducted"
                disabled={values.absenceType === 'SICKNESS'}
                name="deducted"
                handleChange={() => setFieldValue('deducted', !values.deducted)}
                checked={values.deducted}
              />
              <InputPair
                view="other"
                caption="Select icon"
                className="mb-2"
                element={
                  <WhiteBackgroundDiv
                    className={`mb-3 p-2 border rounded ${
                      errors.iconId && touched.iconId ? 'border-danger' : ''
                    }`}
                  >
                    {leaveTypeIcons(setFieldValue, values.iconId)}
                  </WhiteBackgroundDiv>
                }
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting}
              formClick={handleSubmit}
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  leaveType: getLeaveType(state),
  leaveTypeIcons: getLeaveTypesIcons(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestLeaveType,
      createLeaveType,
      requestLeaveTypeIcons,
      closeDrawer,
      toastrError
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateLeaveType);
