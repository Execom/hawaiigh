import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  SeparatorRow,
  TableCell,
  TableRow
} from '../../../../components/common/TableStyledComponents';
import DeleteIcon from '../../../../img/icons/delete.svg';
import EditIcon from '../../../../img/icons/edit.svg';
import { removeLeaveType } from '../../../../store/actions/absenceActions';
import { openActionDrawer } from '../../../../store/actions/drawerActions';
import { openConfirmationModal } from '../../../../store/actions/modalActions';
import { Icon } from '../years/styled';

class LeaveTypeItem extends Component {
  handleDelete = id => {
    this.props.openConfirmationModal({
      message:
        'This action will delete the selected leave type. Are you sure you want to continue?',
      values: id,
      clickAction: removeLeaveType
    });
  };

  render() {
    const {
      leaveType: { id, name, absenceType, comment }
    } = this.props;

    return (
      <Fragment>
        <TableRow
          clickable
          onClick={() =>
            this.props.openActionDrawer({
              type: 'editLeaveType',
              heading: 'Edit Leave Type',
              id
            })
          }
          className="d-block d-lg-table-row"
        >
          <TableCell
            data-label="Name"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>{name}</span>
            </div>
          </TableCell>
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell pl-lg-4"
            data-label="Comment"
            color="white"
          >
            <div className="text-right text-lg-left">
              <span>{comment}</span>
            </div>
          </TableCell>
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell pl-lg-4"
            data-label="Absence type"
            color="white"
          >
            <div className="text-right text-lg-left">
              <span>{absenceType}</span>
            </div>
          </TableCell>
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell py-lg-0 py-2"
            data-label="Actions"
            color="white"
          >
            <div className="text-right">
              <Icon
                onClick={() =>
                  this.props.openActionDrawer({
                    type: 'editLeaveType',
                    heading: 'Edit Leave Type',
                    id
                  })
                }
                className="mr-2"
                src={EditIcon}
                alt="Edit icon"
              />
              <Icon
                onClick={e => {
                  e.stopPropagation();
                  this.handleDelete({ id });
                }}
                src={DeleteIcon}
                alt="Delete icon"
              />
            </div>
          </TableCell>
        </TableRow>
        <SeparatorRow />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { removeLeaveType, openActionDrawer, openConfirmationModal },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(LeaveTypeItem);
