import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
  name: Yup.string().required('Leave type name is a required field.'),
  entitlement: Yup.number()
    .required('Entitlement is a required field.')
    .min(0),
  maxCarriedOver: Yup.number()
    .required('Max carried over is a required field.')
    .min(0),
  maxBonusDays: Yup.number()
    .required('Max bonus days is a required field.')
    .min(0),
  maxAllowanceFromNextYear: Yup.number()
    .required('Max allowance from next year is a required field.')
    .min(0),
  training: Yup.number()
    .required('Max training is a required field.')
    .min(0)
});
