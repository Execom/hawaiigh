import { Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closeDrawer } from '@/store/actions/drawerActions';
import {
  clearLeaveProfile,
  requestLeaveProfile,
  updateLeaveProfile
} from '@/store/actions/leaveProfileActions';
import { getIsSubmitting, getLeaveProfile } from '@/store/selectors';
import {
  DrawerContent,
  DrawerFooter,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import { validationSchema } from '@/pages/administration/components/leave-profiles/helpers';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { showOnlyOneDecimal } from '@/pages/administration/helpers';

class EditLeaveProfile extends Component {
  componentWillMount() {
    this.props.clearLeaveProfile();
  }

  componentDidMount() {
    this.props.requestLeaveProfile(this.props.id);
  }

  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    if (!this.props.leaveProfile) return null;

    return (
      <Formik
        validationSchema={validationSchema}
        initialValues={this.props.leaveProfile}
        onSubmit={this.props.updateLeaveProfile}
        enableReinitialize
        render={({
          initialValues,
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                caption="Leave type name"
                defaultValue={values.name}
                name="name"
                handleChange={handleChange}
                className={errors.name && touched.name ? 'border-danger' : ''}
              />
              <InputPair
                caption="Entitlement"
                defaultValue={showOnlyOneDecimal(Number(values.entitlement))}
                name="entitlement"
                handleChange={handleChange}
                className={
                  errors.entitlement && touched.entitlement
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                caption="Max carried over"
                defaultValue={showOnlyOneDecimal(Number(values.maxCarriedOver))}
                name="maxCarriedOver"
                handleChange={handleChange}
                className={
                  errors.maxCarriedOver && touched.maxCarriedOver
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                caption="Max bonus days"
                defaultValue={showOnlyOneDecimal(Number(values.maxBonusDays))}
                name="maxBonusDays"
                handleChange={handleChange}
                className={
                  errors.maxBonusDays && touched.maxBonusDays
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                caption="Max allowance from next year"
                defaultValue={showOnlyOneDecimal(
                  Number(values.maxAllowanceFromNextYear)
                )}
                name="maxAllowanceFromNextYear"
                handleChange={handleChange}
                className={
                  errors.maxAllowanceFromNextYear &&
                  touched.maxAllowanceFromNextYear
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                caption="Max training"
                defaultValue={showOnlyOneDecimal(Number(values.training))}
                name="training"
                handleChange={handleChange}
                className={
                  errors.training && touched.training ? 'border-danger' : ''
                }
              />
              <InputPair
                view="textArea"
                caption="Comment"
                defaultValue={values.comment}
                name="comment"
                handleChange={handleChange}
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting || values === initialValues}
              formClick={handleSubmit}
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  leaveProfile: getLeaveProfile(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestLeaveProfile,
      updateLeaveProfile,
      closeDrawer,
      clearLeaveProfile,
      toastrError
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditLeaveProfile);
