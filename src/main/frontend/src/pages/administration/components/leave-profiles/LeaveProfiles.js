import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withResetOnNavigate from '../../../../components/HOC/withResetOnNavigate';
import { openActionDrawer } from '../../../../store/actions/drawerActions';
import { requestLeaveProfiles } from '../../../../store/actions/leaveProfileActions';
import { getLeaveProfiles } from '../../../../store/selectors';
import AdministrationTable from '../common/AdministrationTable';
import LeaveProfileItem from './LeaveProfileItem';

class LeaveProfiles extends Component {
  componentDidMount() {
    this.props.requestLeaveProfiles({
      size: 30,
      page: 0
    });
  }

  render() {
    const { last, results, page } = this.props.leaveProfiles;

    const leaveProfiles = results.length
      ? results.map(item => {
          return <LeaveProfileItem key={item.id} leaveProfile={item} />;
        })
      : null;

    return (
      <AdministrationTable
        headerItems={['Name']}
        body={leaveProfiles}
        page={page}
        clickAction={() =>
          this.props.openActionDrawer({
            type: 'createLeaveProfile',
            heading: 'Create Leave Profile'
          })
        }
        requestAction={requestLeaveProfiles}
        placeholderCellCount={1}
        last={last}
      />
    );
  }
}

const mapStateToProps = state => ({
  leaveProfiles: getLeaveProfiles(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestLeaveProfiles, openActionDrawer }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(LeaveProfiles));
