import { Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closeDrawer } from '@/store/actions/drawerActions';
import { createLeaveProfile } from '@/store/actions/leaveProfileActions';
import { getIsSubmitting } from '@/store/selectors';
import {
  DrawerContent,
  DrawerFooter,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import { validationSchema } from '@/pages/administration/components/leave-profiles/helpers';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';

class CreateLeaveProfile extends Component {
  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    return (
      <Formik
        validationSchema={validationSchema}
        initialValues={{
          name: '',
          entitlement: '',
          maxCarriedOver: '',
          maxBonusDays: '',
          maxAllowanceFromNextYear: '',
          training: ''
        }}
        onSubmit={this.props.createLeaveProfile}
        render={({
          handleSubmit,
          handleChange,
          errors,
          touched,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                caption="Leave type name"
                name="name"
                handleChange={handleChange}
                className={errors.name && touched.name ? 'border-danger' : ''}
              />
              <InputPair
                caption="Entitlement"
                name="entitlement"
                handleChange={handleChange}
                type="number"
                className={
                  errors.entitlement && touched.entitlement
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                caption="Max carried over"
                name="maxCarriedOver"
                handleChange={handleChange}
                type="number"
                className={
                  errors.maxCarriedOver && touched.maxCarriedOver
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                caption="Max bonus days"
                name="maxBonusDays"
                handleChange={handleChange}
                type="number"
                className={
                  errors.maxBonusDays && touched.maxBonusDays
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                caption="Max allowance from next year"
                name="maxAllowanceFromNextYear"
                handleChange={handleChange}
                type="number"
                className={
                  errors.maxAllowanceFromNextYear &&
                  touched.maxAllowanceFromNextYear
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                caption="Max training"
                name="training"
                handleChange={handleChange}
                type="number"
                className={
                  errors.training && touched.training ? 'border-danger' : ''
                }
              />
              <InputPair
                view="textArea"
                caption="Comment"
                name="comment"
                handleChange={handleChange}
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting}
              formClick={handleSubmit}
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { createLeaveProfile, closeDrawer, toastrError },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateLeaveProfile);
