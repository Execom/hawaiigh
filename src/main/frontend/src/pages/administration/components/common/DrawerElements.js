import React, { Component, Fragment } from 'react';
import Switch from 'react-switch';
import styled from 'styled-components';
import Button from '../../../../components/common/Button';
import Calendar from '../../../../img/icons/calendar.svg';
import InputChips from '../../../../components/common/InputChips';
import { customScrollbar } from './styled';
import { ArrowSelect } from '@/pages/dashboard/components/styled';
import ArrowDown from '../../../../img/icons/arrow-down.svg';

export const DrawerContent = styled.div`
  ${props => (!props.disableGrow ? 'flex: 1 0 0' : '')};
  overflow: auto;

  ${customScrollbar};
`;

export const DrawerFooter = props => {
  const { element, closeClick, isSubmitting, formClick } = props;
  return (
    <div className="py-3 mx-4 border-top">
      {element && (
        <div className="mb-2 pb-2 border-bottom d-flex justify-content-end">
          {element}
        </div>
      )}
      <div className="d-flex justify-content-end">
        <Button
          className="mr-2 px-4 py-2"
          title="Cancel"
          click={closeClick}
          styling={{ color: 'white' }}
        />
        <Button
          className="px-4 py-2"
          title="Save"
          disabled={isSubmitting}
          type="submit"
          click={formClick}
          styling={{ fontColor: 'white' }}
        />
      </div>
    </div>
  );
};

export const DrawerFooterForDataExport = props => {
  const { isSubmitting, formClick, handleChange, exportFormat } = props;
  return (
    <div className="py-3 mx-4 border-top">
      <div className="d-flex justify-content-end">
        <InputPairNoCaption
          view="select-without-label"
          caption="Export Format"
          name="exportFormat"
          handleChange={handleChange}
          value={exportFormat}
          options={
            <Fragment>
              <option value="csv">CSV</option>
              <option value="pdf">PDF</option>
            </Fragment>
          }
        />
        <Button
          className="px-4 py-2"
          title="Download"
          disabled={isSubmitting}
          type="submit"
          click={formClick}
          styling={{ fontColor: 'white' }}
        />
      </div>
    </div>
  );
};

const DrawerLabel = styled.label`
  color: #a8a8a8;
  font-size: 14px;
`;

export const DrawerInput = styled.input`
  background-color: #fff;

  &:disabled {
    background: #f6f6f6;
  }
`;

export const DrawerEmailInputContainer = styled.div`
  position: relative;
`;

export const DrawerEmailInput = styled.input`
  background-color: #fff;
  border: none;
  padding: 0.5rem 7rem 0.5rem 0.5rem;
  width: 100%;

  &:disabled {
    background: #f6f6f6;
  }
`;

export const DrawerEmailInputDomainSpan = styled.span`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  padding: 0.5rem;
  display: inline-flex;
  align-items: center;
`;

export const DrawerTextArea = styled.textarea`
  background-color: #fff;
  overflow-y: auto;
  height: 200px;

  &:disabled {
    background: #f6f6f6;
  }
`;

const DrawerSelect = styled.select`
  background-color: #fff;
  appearance: none;
  cursor: pointer;

  &:disabled {
    background: #f6f6f6;
  }
`;

export const WhiteBackgroundDiv = styled.div`
  background-color: ${props => (props.disabled ? '#f6f6f6' : '#fff')};
  max-height: 125px;
  overflow: auto;

  h5 {
    margin-bottom: 8px;

    &:last-of-type {
      margin-bottom: 0px;
    }
  }
`;

const renderView = props => {
  const {
    view,
    className,
    defaultValue,
    name,
    handleChange,
    type,
    disabled,
    checked,
    value,
    disabledValue,
    options,
    element
  } = props;
  switch (view) {
    case 'email':
      return (
        <DrawerEmailInputContainer>
          <DrawerEmailInput
            className={`${className} border rounded`}
            defaultValue={defaultValue}
            id={name}
            onChange={handleChange}
            type={type || 'text'}
          />
          <DrawerEmailInputDomainSpan>@execom.eu</DrawerEmailInputDomainSpan>
        </DrawerEmailInputContainer>
      );
    case 'switch':
      return (
        <WhiteBackgroundDiv
          disabled={disabled}
          className="p-2 border rounded text-right"
        >
          <Switch
            disabled={disabled}
            height={15}
            width={30}
            onChange={handleChange}
            checked={checked}
            name={name}
            id={name}
          />
        </WhiteBackgroundDiv>
      );
    case 'chips':
      return (
        <WhiteBackgroundDiv
          className={`${className} p-1 border rounded text-right`}
        >
          <InputChips
            name={name}
            id={name}
            defaultValue={defaultValue}
            onChange={handleChange}
          />
        </WhiteBackgroundDiv>
      );
    case 'textArea':
      return (
        <DrawerTextArea
          defaultValue={defaultValue}
          className={`${className} p-2 w-100 border rounded`}
          id={name}
          onChange={handleChange}
        />
      );
    case 'select':
      return (
        <DrawerSelect
          className={`${className} p-2 w-100 border rounded`}
          name={name}
          onChange={handleChange}
          value={value}
          id={name}
        >
          <option value="" disabled>
            {disabledValue}
          </option>
          {options}
        </DrawerSelect>
      );
    case 'select-without-label':
      return (
        <DrawerSelect
          className={`${className} p-2 w-100 border rounded`}
          name={name}
          onChange={handleChange}
          value={value}
          id={name}
        >
          {options}
        </DrawerSelect>
      );
    case 'other':
      return element;
    default:
      return (
        <DrawerInput
          defaultValue={defaultValue}
          className={`${className} p-2 w-100 border rounded`}
          id={name}
          onChange={handleChange}
          type={type || 'text'}
          disabled={disabled}
        />
      );
  }
};

export const InputPair = props => {
  const {
    separator,
    name,
    caption,
    view,
    disabled,
    leftPadding,
    flex,
    sort
  } = props;

  return (
    <div
      className={`${separator && 'border-bottom pb-4 mb-4'} ${leftPadding &&
        'pl-3'} ${flex && 'flex-grow-1'} ${sort &&
        'w-100'} mb-3 position-relative`}
    >
      <DrawerLabel disabled={disabled} htmlFor={name} className="mb-2 pl-2">
        {caption}
      </DrawerLabel>
      {renderView(props)}
      {(view === 'select' || view === 'select-without-label') && (
        <ArrowSelect
          src={ArrowDown}
          alt="arrow down"
          className="edit-employee-arrow"
        />
      )}
    </div>
  );
};

export const InputPairNoCaption = props => {
  const { view } = props;
  return (
    <div className="mr-3 position-relative fixed-select-width">
      {renderView(props)}
      {(view === 'select' || view === 'select-without-label') && (
        <ArrowSelect
          src={ArrowDown}
          alt="arrow down"
          className="input-no-caption-arrow"
        />
      )}
    </div>
  );
};

const StyledImage = styled.img`
  width: 15px;
  top: 9px;
  right: 5px;

  @media (min-width: 576px) {
    right: 10px;
  }
`;

const CustomInputStyled = styled.button`
  background-color: #fff;
  height: 37px;
  cursor: pointer;

  &:disabled {
    pointer-events: none;
    background: #f6f6f6;
  }
`;

class DrawerCalendarInput extends Component {
  render() {
    const { className, onClick, value, disabled } = this.props;
    return (
      <CustomInputStyled
        className={`pl-2 w-100 text-left position-relative border rounded p-1 ${className}`}
        onClick={onClick}
        disabled={disabled}
      >
        {value}
        <StyledImage
          className="position-absolute"
          src={Calendar}
          alt="calendar icon"
        />
      </CustomInputStyled>
    );
  }
}

export default DrawerCalendarInput;
