import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CSSTransition } from 'react-transition-group';
import InfiniteScroll from 'react-infinite-scroller';
import _debounce from 'lodash/debounce';
import { Tooltip } from '@material-ui/core';
import FloatingActionButton from '@/components/common/FloatingActionButton/FloatingActionButton';
import RoundButton from '@/components/common/RoundButton';
import {
  TableHeader,
  TableHeaderWrapper
} from '@/components/common/TableStyledComponents';
import { ACTIVE, DELETED, INACTIVE } from '@/helpers/enum';
import ActiveUsers from '@/img/icons/active_users.svg';
import InactiveUsers from '@/img/icons/inactive_users.svg';
import ScheduledUsers from '@/img/icons/scheduled_users.svg';
import { adminSearchEmployees } from '@/store/actions/userActions';
import { requestAllowancePerUserForYear } from '@/store/actions/allowanceActions';
import { calculatePage } from '@/pages/administration/helpers';
import {
  FilterIcon,
  FilterIconsWrapper,
  FixedTable,
  SearchBar,
  TableContent
} from '@/pages/administration/components/common/styled';
import { getAdministration } from '@/store/selectors';
import { setShouldTableScrollToTop } from '@/store/actions/administrationActions';
import ScrollDetector from '@/components/HOC/ScrollDetector';
import TablePlaceholder from '@/components/placeholder/TablePlaceholder';
import AllowanceFilters from '@/pages/administration/components/allowances/AllowanceFilters';

class AdministrationTable extends Component {
  tableRef = React.createRef();

  state = {
    searchQuery: '',
    userStatusType: [ACTIVE],
    year: new Date().getFullYear(),
    teamId: '',
    userSortFields: {
      Name: 'fullName',
      'Job title': 'jobTitle',
      Email: 'email',
      'Years of service': 'yearsOfService',
      'Restoration date': 'startedWorkingAtExecomDate',
      Archived: 'stoppedWorkingAtExecomDate'
    },
    currentSortField: 'fullName',
    sortOrder: 'ASC'
  };

  componentWillMount() {
    this.delayedCallback = _debounce(({ target: { value } }) => {
      this.employeeSearch(value);
    }, 500);
  }

  componentDidUpdate() {
    if (this.props.administration.shouldTableScrollToTop) {
      this.tableRef.current.scrollTop = 0;
      this.props.setShouldTableScrollToTop(false);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.userStatusType &&
      nextProps.userStatusType[0] !== this.props.userStatusType[0]
    ) {
      const { dispatch } = this.props;

      dispatch(
        adminSearchEmployees({
          searchQuery: this.state.searchQuery,
          page: 0,
          size: 30,
          userStatusType: nextProps.userStatusType,
          includeLoggedUser: true
        })
      );
    }
  }

  updateFilterYear = year => {
    this.setState({ year });
  };

  updateTeam = teamId => {
    this.setState({ teamId });
  };

  searchEmployees = e => {
    e.persist();
    this.tableRef.current.scrollTop = 0;
    this.delayedCallback(e);
  };

  employeeSearch = value => {
    this.setState(
      {
        searchQuery: value
      },
      () => {
        const { dispatch } = this.props;
        const { searchQuery } = this.state;
        const commonParams = {
          searchQuery,
          page: 0,
          size: 30
        };

        if (searchQuery.length >= 3 || searchQuery.length === 0) {
          !this.props.allowanceTab
            ? dispatch(
                adminSearchEmployees({
                  ...commonParams,
                  userStatusType: this.props.userStatusType,
                  includeLoggedUser: true
                })
              )
            : dispatch(
                requestAllowancePerUserForYear({
                  ...commonParams,
                  year: this.state.year,
                  teamId: this.state.teamId
                })
              );
        }
      }
    );
  };

  loadMore = page => {
    if (this.props.last) return;

    const {
      employeePage,
      employeesTab,
      allowanceTab,
      allowancePage,
      requestObject,
      requestAction,
      dispatch,
      page: lastPage
    } = this.props;

    const { searchQuery } = this.state;

    !allowanceTab
      ? dispatch(
          requestAction({
            page: calculatePage(
              employeesTab,
              employeePage,
              lastPage === 0 ? 1 : page
            ),
            size: 30,
            searchQuery,
            ...requestObject
          })
        )
      : requestAction({
          page: calculatePage(
            allowanceTab,
            allowancePage,
            lastPage === 0 ? 1 : page
          ),
          size: 30,
          searchQuery,
          ...requestObject
        });
  };

  sortData = field => {
    const { dispatch } = this.props;
    let { currentSortField, sortOrder } = this.state;

    if (currentSortField === field) {
      if (sortOrder === 'ASC') {
        sortOrder = 'DESC';
      } else {
        sortOrder = 'ASC';
      }
    } else {
      sortOrder = 'ASC';
      currentSortField = field;
    }
    this.setState({ sortOrder, currentSortField });

    dispatch(
      adminSearchEmployees({
        page: 0,
        size: 30,
        userStatusType: this.props.userStatusType,
        includeLoggedUser: true,
        sort: field,
        sortDirection: sortOrder
      })
    );
  };

  handleFilters = param => {
    this.tableRef.current.scrollTop = 0;
    this.props.handleFilters(param);
  };

  render() {
    const {
      headerItems,
      searchBar,
      body,
      clickAction,
      width,
      last,
      employeesTab,
      allowanceTab,
      userStatusType,
      page,
      initialLoad,
      isEmployeeTab
    } = this.props;

    return (
      <div className="container-fluid d-flex">
        <div className="row">
          <div className="col-md-12 d-flex flex-column position-relative">
            <div className="w-lg-25 align-self-center my-3">
              {searchBar && (
                <SearchBar searchEmployees={this.searchEmployees} />
              )}
            </div>
            {allowanceTab && (
              <AllowanceFilters
                {...this.props}
                updateTeam={this.updateTeam}
                updateFilterYear={this.updateFilterYear}
                searchQuery={this.state.searchQuery}
                year={this.state.year}
                teamId={this.state.teamId}
              />
            )}
            <TableHeaderWrapper
              className={`d-flex ${
                employeesTab || allowanceTab ? 'ml-lg-4 pl-lg-4' : ''
              }`}
            >
              {headerItems.map((item, index) => {
                return (
                  <TableHeader
                    width={width && width[index]}
                    key={`${item}${index}`}
                    color="grey"
                    borderless
                    showOnMobile={employeesTab || allowanceTab}
                    className={`pl-lg-4 border-0 font-weight-bold align-items-center ${
                      item === '' ? 'd-none' : 'd-flex'
                    }`}
                    employeesTabAlignment={employeesTab}
                    allowanceTabAlignment={allowanceTab}
                    {...isEmployeeTab && {
                      onClick: () =>
                        this.sortData(this.state.userSortFields[item])
                    }}
                  >
                    {this.state.userSortFields[item] ===
                    this.state.currentSortField
                      ? item +
                        (isEmployeeTab
                          ? this.state.sortOrder === 'ASC'
                            ? '↑'
                            : '↓'
                          : '')
                      : item}
                  </TableHeader>
                );
              })}
              {!allowanceTab && (
                <TableHeader
                  color="grey"
                  borderless
                  showOnMobile={employeesTab || allowanceTab}
                  className="border-0 justify-content-end d-flex align-items-center pl-4"
                >
                  {employeesTab && (
                    <FilterIconsWrapper className="d-flex align-items-center">
                      <Tooltip title={'Active users'} placement="top">
                        <FilterIcon
                          onClick={() => this.handleFilters(ACTIVE)}
                          src={ActiveUsers}
                          isSelected={userStatusType.includes(ACTIVE)}
                        />
                      </Tooltip>
                      <Tooltip title={'Scheduled users'} placement="top">
                        <FilterIcon
                          onClick={() => this.handleFilters(INACTIVE)}
                          src={ScheduledUsers}
                          isSelected={userStatusType.includes(INACTIVE)}
                        />
                      </Tooltip>
                      <Tooltip title={'Archived users'} placement="top">
                        <FilterIcon
                          onClick={() => this.handleFilters(DELETED)}
                          src={InactiveUsers}
                          isSelected={userStatusType.includes(DELETED)}
                        />
                      </Tooltip>
                    </FilterIconsWrapper>
                  )}
                  <RoundButton
                    click={clickAction}
                    className="float-right d-none d-lg-block ml-2"
                  />
                </TableHeader>
              )}
            </TableHeaderWrapper>
            <ScrollDetector
              component={
                <TableContent
                  className="d-flex flex-grow-1"
                  innerRef={this.tableRef}
                >
                  <InfiniteScroll
                    pageStart={page || 0}
                    loadMore={this.loadMore}
                    hasMore={!last}
                    threshold={50}
                    initialLoad={initialLoad || false}
                    useWindow={false}
                  >
                    <FixedTable
                      className="table table-hover mb-0 table-layout-fixed position-relative"
                      cellPadding="0"
                      cellSpacing="0"
                      border="0"
                    >
                      <CSSTransition
                        timeout={200}
                        in={body != null}
                        classNames="table"
                      >
                        <tbody>{body}</tbody>
                      </CSSTransition>
                    </FixedTable>
                  </InfiniteScroll>
                </TableContent>
              }
            />
            <CSSTransition
              timeout={200}
              in={body === null}
              classNames="table"
              unmountOnExit
              hasMobileHeader={employeesTab || allowanceTab}
            >
              <TablePlaceholder cellCount={this.props.placeholderCellCount} />
            </CSSTransition>
            <FloatingActionButton onClick={clickAction} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  administration: getAdministration(state)
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  setShouldTableScrollToTop: () => dispatch(setShouldTableScrollToTop())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdministrationTable);
