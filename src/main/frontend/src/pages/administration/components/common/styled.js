import React from 'react';
import ReactSVG from 'react-svg';
import styled, { css } from 'styled-components';
import Loupe from '@/img/icons/loupe.svg';
import { LoupeImage } from '@/components/search-dropdown/styled';

export const FixedTable = styled.table`
  table-layout: fixed;
  width: calc(100% - 6px);
`;

export const FilterIconsWrapper = styled.div`
  margin-left: 2px;

  && button {
    margin: 0 2px;
  }
`;

export const customScrollbar = css`
  @media (min-width: 991px) {
    ::-webkit-scrollbar {
      width: 6px;
    }
    ::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    }
    ::-webkit-scrollbar-thumb {
      -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    }
  }
`;

export const TableContent = styled.div`
  height: 0;
  overflow-x: auto;
  overflow-y: auto;
  ${customScrollbar};
  scrollbar-width: thin;
`;

const SearchBarInput = styled.input`
  width: 100%;
  padding: 2px 5px;
  border: none;
  border-bottom: 1px solid #d3d3d3;
  background: transparent;
`;

export const SearchBar = props => (
  <div className="position-relative w-100">
    <SearchBarInput
      onChange={props.searchEmployees}
      {...props}
      type="text"
      placeholder="Users search..."
    />
    <LoupeImage src={Loupe} />
  </div>
);

export const FilterIcon = styled(({ isSelected, ...rest }) => (
  <ReactSVG {...rest} />
))`
  position: relative;
  cursor: pointer;
  &,
  div {
    width: 27px;
    height: 27px;
  }

  svg {
    width: 27px;
    height: 27px;
    path.circle {
      fill: ${props => (props.isSelected ? '#FB4B4F' : '#C3C3C3')};
    }
  }
`;

export const BasicTablePlaceholderWrapper = styled.div`
  top: 10px;
  padding: 5px 0;
`;

export const BasicTableRowPlaceholder = styled.div`
  height: 45px;
  border: 1px solid gray;
  border-radius: 6px;
  margin-bottom: 5px;
`;

export const TablePlaceholderWrapper = styled.div`
  width: calc(100% - 30px);
  top: 50px;
  left: 15px;

  @media (max-width: 991px) {
    top: ${props => (props.hasMobileHeader ? '50px' : '20px')};
  }
`;

export const TableRowPlaceholder = styled.div`
  border-bottom: 1px solid #eee;
  padding: 11px 0px;
  border-radius: 6px;
  margin-bottom: 5px;
`;

export const TableCellPlaceholderWrapper = styled.div`
  flex: 1 1 0;
  padding: 0 0.75rem;
  ${props => (props.isAction ? 'text-align: right;' : '')}
`;

export const TableCellPlaceholderInner = styled.div`
  height: 15px;
  border-radius: 8px;
  background: #f2f2f2;
`;

export const TableCellPlaceholderAction = styled.div`
  height: 16px;
  width: 16px;
  border-radius: 8px;
  background: #f2f2f2;
  margin-left: 10px;
`;
