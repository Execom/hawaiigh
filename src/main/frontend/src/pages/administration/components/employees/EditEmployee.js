import { FieldArray, Formik } from 'formik';
import _includes from 'lodash/includes';
import React, { Component, Fragment } from 'react';
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { UserImage } from '@/components/common/userImage';
import TeamSearchResults from '@/components/search-dropdown/search-results/TeamSearchResults';
import SearchDropdown from '@/components/search-dropdown/SearchDropdown';
import { DELETED } from '@/helpers/enum';
import Execom from '@/img/execom.jpg';
import { closeDrawer } from '@/store/actions/drawerActions';
import { requestLeaveProfiles } from '@/store/actions/leaveProfileActions';
import { openEmployeeRestoreModal } from '@/store/actions/modalActions';
import {
  clearTeams,
  requestTeams,
  searchTeams
} from '@/store/actions/teamActions';
import {
  clearEmployee,
  removeEmployee,
  cancelEmployeeDelete,
  requestEmployee,
  restoreEmployee,
  updateEmployee
} from '@/store/actions/userActions';
import { getImageUrl } from '@/store/getImageUrl';
import {
  getEmployee,
  getIsSubmitting,
  getLeaveProfileResults,
  getSearchTeams,
  getTeams,
  getUser
} from '@/store/selectors';
import DrawerCalendarInput, {
  DrawerContent,
  DrawerFooter,
  WhiteBackgroundDiv,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import {
  validationSchema,
  addEmailDomain,
  removeEmailDomain
} from '@/pages/administration/components/employees/helpers';
import {
  formatDate,
  formatDateOrReturnNull,
  parseDate,
  parseDateOrReturnNull
} from '@/pages/administration/components/years/helpers';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';
import { USER, HR_MANAGER, OFFICE_MANAGER } from '@/helpers/enum';

class EditEmployee extends Component {
  componentWillMount() {
    this.props.clearEmployee();
  }

  componentDidMount() {
    this.props.requestEmployee(this.props.id);
    this.props.requestTeams();
    this.props.requestLeaveProfiles();
  }

  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    if (
      !this.props.employee ||
      !this.props.teams.results.length ||
      !this.props.leaveProfiles.length
    )
      return null;

    const {
      employee,
      teams: { results },
      leaveProfiles,
      employee: {
        email,
        startedProfessionalCareerDate,
        startedWorkingAtExecomDate,
        startedWorkingDate
      }
    } = this.props;

    const teams = results.map(team => {
      return (
        <option key={team.id} value={team.id}>
          {team.name}
        </option>
      );
    });

    const leaveProfilesOptionsData = leaveProfiles.map(leaveProfile => {
      return (
        <option key={leaveProfile.id} value={leaveProfile.id}>
          {leaveProfile.name}
        </option>
      );
    });

    const initialValues = {
      ...employee,
      email: removeEmailDomain(email),
      startedProfessionalCareerDate: parseDateOrReturnNull(
        startedProfessionalCareerDate
      ),
      startedWorkingAtExecomDate: parseDate(startedWorkingAtExecomDate),
      startedWorkingDate: parseDate(startedWorkingDate)
    };

    return (
      <Formik
        validationSchema={validationSchema}
        initialValues={initialValues}
        onSubmit={(values, { setSubmitting }) => {
          const {
            updateEmployee,
            employee: { userStatusType }
          } = this.props;
          const {
            email,
            startedProfessionalCareerDate,
            startedWorkingAtExecomDate,
            startedWorkingDate
          } = values;

          updateEmployee({
            ...values,
            email: addEmailDomain(email),
            startedProfessionalCareerDate: formatDateOrReturnNull(
              startedProfessionalCareerDate
            ),
            startedWorkingAtExecomDate: formatDate(startedWorkingAtExecomDate),
            startedWorkingDate: formatDate(startedWorkingDate),
            activeTab: userStatusType
          });
          setSubmitting(false);
        }}
        enableReinitialize
      >
        {({
          initialValues,
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <div className="text-center mb-4">
                <UserImage
                  borderBlack
                  image={
                    _includes(getImageUrl(values.imageUrl, 100), 'null')
                      ? Execom
                      : values.imageUrl
                  }
                  isSmall
                  size="100px"
                />
              </div>
              <InputPair
                caption="Full Name"
                defaultValue={values.fullName}
                name="fullName"
                handleChange={handleChange}
                className={
                  errors.fullName && touched.fullName ? 'border-danger' : ''
                }
              />
              <InputPair
                view="email"
                caption="Email"
                defaultValue={values.email}
                name="email"
                handleChange={handleChange}
                className={errors.email && touched.email ? 'border-danger' : ''}
              />
              <InputPair
                caption="Job title"
                defaultValue={values.jobTitle}
                name="jobTitle"
                handleChange={handleChange}
                className={
                  errors.jobTitle && touched.jobTitle ? 'border-danger' : ''
                }
              />
              {this.props.user && this.props.user.id !== values.id && (
                <InputPair
                  view="select"
                  caption="User role"
                  className={
                    errors.userRole && touched.userRole ? 'border-danger' : ''
                  }
                  name="userRole"
                  handleChange={handleChange}
                  value={values.userRole}
                  disabledValue="Select role"
                  options={
                    <Fragment>
                      <option value={USER}>User</option>
                      <option value={HR_MANAGER}>HR Manager</option>
                      <option value={OFFICE_MANAGER}>Office Manager</option>
                    </Fragment>
                  }
                />
              )}
              <InputPair
                view="select"
                caption="Leave profile"
                className={
                  errors.leaveProfileId && touched.leaveProfileId
                    ? 'border-danger'
                    : ''
                }
                name="leaveProfileId"
                handleChange={handleChange}
                value={values.leaveProfileId}
                disabledValue="Select leave profile"
                options={leaveProfilesOptionsData}
              />
              <InputPair
                view="select"
                caption="Team"
                className={
                  errors.teamId && touched.teamId ? 'border-danger' : ''
                }
                name="teamId"
                handleChange={handleChange}
                value={values.teamId}
                disabledValue="Select team"
                options={teams}
              />
              <InputPair
                view="other"
                caption="Started professional career date"
                element={
                  <DatePicker
                    customInput={<DrawerCalendarInput />}
                    className={
                      errors.startedProfessionalCareerDate &&
                      touched.startedProfessionalCareerDate
                        ? 'border-danger'
                        : ''
                    }
                    showYearDropdown
                    dateFormat="dd MMM yyyy"
                    locale="en-GB"
                    selected={values.startedProfessionalCareerDate}
                    onChange={e => {
                      setFieldValue('startedProfessionalCareerDate', e);
                    }}
                  />
                }
              />
              <InputPair
                view="other"
                caption="Started working date"
                element={
                  <DatePicker
                    customInput={<DrawerCalendarInput />}
                    className={
                      errors.startedWorkingDate && touched.startedWorkingDate
                        ? 'border-danger'
                        : ''
                    }
                    showYearDropdown
                    dateFormat="dd MMM yyyy"
                    locale="en-GB"
                    selected={values.startedWorkingDate}
                    onChange={e => {
                      setFieldValue('startedWorkingDate', e);
                    }}
                  />
                }
              />
              <InputPair
                view="other"
                caption="Started working at execom date"
                element={
                  <DatePicker
                    customInput={<DrawerCalendarInput />}
                    className={
                      errors.startedWorkingAtExecomDate &&
                      touched.startedWorkingAtExecomDate
                        ? 'border-danger'
                        : ''
                    }
                    showYearDropdown
                    dateFormat="dd MMM yyyy"
                    locale="en-GB"
                    placeholderText="Started working at execom date"
                    selected={values.startedWorkingAtExecomDate}
                    onChange={e => {
                      setFieldValue('startedWorkingAtExecomDate', e);
                    }}
                  />
                }
              />
              <InputPair
                view="other"
                caption="Teams search"
                element={
                  <SearchDropdown
                    searchResults={this.props.searchedTeams}
                    searchAction={searchTeams}
                    resetAction={clearTeams}
                    selectedTeams={values.approverTeams}
                    employeeTeamId={values.teamId}
                    placeholder="Search for teams..."
                  >
                    {(inputReference, resetInput) => (
                      <TeamSearchResults
                        resetInput={resetInput}
                        inputReference={inputReference}
                        teams={this.props.searchedTeams}
                        selectedTeams={values.approverTeams}
                        employeeTeamId={values.teamId}
                      />
                    )}
                  </SearchDropdown>
                }
              />
              <InputPair
                view="other"
                caption="Approver in teams"
                element={
                  <WhiteBackgroundDiv className="p-2 border rounded">
                    {!values.approverTeams.length ? (
                      <span>No teams selected</span>
                    ) : (
                      values.approverTeams.map((team, index) => (
                        <h5
                          key={team.id}
                          className="d-flex justify-content-between mb-2"
                        >
                          {team.name}
                          <FieldArray
                            name="approverTeams"
                            render={arrayHelpers => (
                              <span
                                className="text-danger ml-2"
                                onClick={() => {
                                  arrayHelpers.remove(index);
                                }}
                              >
                                x
                              </span>
                            )}
                          />
                        </h5>
                      ))
                    )}
                  </WhiteBackgroundDiv>
                }
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting || values === initialValues}
              formClick={handleSubmit}
              closeClick={this.closeDrawer}
              element={values.userStatusType === DELETED && this.renderButton()}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      </Formik>
    );
  }
}

const mapStateToProps = state => ({
  employee: getEmployee(state),
  teams: getTeams(state),
  leaveProfiles: getLeaveProfileResults(state),
  user: getUser(state),
  searchedTeams: getSearchTeams(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateEmployee,
      requestEmployee,
      removeEmployee,
      cancelEmployeeDelete,
      restoreEmployee,
      requestTeams,
      requestLeaveProfiles,
      searchTeams,
      clearTeams,
      closeDrawer,
      clearEmployee,
      openEmployeeRestoreModal,
      toastrError
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditEmployee);
