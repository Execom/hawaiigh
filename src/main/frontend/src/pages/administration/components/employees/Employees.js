import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TableCell, TableRow } from '@/components/common/TableStyledComponents';
import { ACTIVE, DELETED } from '@/helpers/enum';
import { openActionDrawer } from '@/store/actions/drawerActions';
import {
  adminSearchEmployees,
  requestEmployees,
  resetEmployees
} from '@/store/actions/userActions';
import { getEmployees } from '@/store/selectors';
import AdministrationTable from '@/pages/administration/components/common/AdministrationTable';
import EmployeeInfo from '@/pages/administration/components/employees/EmployeeInfo';

class Employees extends Component {
  state = {
    userStatusType: [ACTIVE]
  };

  componentDidMount() {
    this.props.adminSearchEmployees({
      page: 0,
      size: 30,
      searchQuery: '',
      userStatusType: [ACTIVE]
    });
  }

  handleFilters = filterValue => {
    this.setState({
      userStatusType: [filterValue]
    });
  };

  render() {
    const { page: employeePage, results, searched } = this.props.employees;
    const { userStatusType } = this.state;
    const scheduled = userStatusType.includes('INACTIVE');
    const archived = userStatusType.includes('DELETED');
    const headerItems = [
      'Name',
      'Job title',
      'Email',
      'Years of service',
      `${scheduled ? 'Restoration date' : ''}`,
      `${archived ? 'Archived' : ''}`
    ];

    let employees;

    if (results && !results.length && searched) {
      employees = (
        <TableRow className="d-block d-lg-table-row">
          <TableCell
            width="100%"
            color="white"
            className="py-3 text-center w-100 d-block d-lg-table-cell"
          >
            Sorry, no results found
          </TableCell>
        </TableRow>
      );
    } else {
      employees = results
        ? results.map(employee => (
            <EmployeeInfo
              statusType={this.state.userStatusType}
              {...employee}
              key={employee.id}
              employees={this.props.employees.results}
            />
          ))
        : null;
    }

    return (
      <AdministrationTable
        headerItems={headerItems}
        searchBar
        isEmployeeTab
        body={employees}
        page={this.props.employees.page}
        clickAction={() =>
          this.props.openActionDrawer({
            type: 'createEmployee',
            heading: 'Create Employee',
            activeTab: this.state.userStatusType
          })
        }
        requestAction={adminSearchEmployees}
        requestObject={
          this.state.archived
            ? { userStatusType: DELETED }
            : { userStatusType: [...userStatusType] }
        }
        last={this.props.employees.last}
        employeesTab
        employeePage={employeePage}
        handleFilters={this.handleFilters}
        userStatusType={userStatusType}
        placeholderCellCount={5}
      />
    );
  }
}

const mapStateToProps = state => ({
  employees: getEmployees(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestEmployees,
      resetEmployees,
      adminSearchEmployees,
      openActionDrawer
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Employees);
