import styled from 'styled-components';
import ExecomLogo from '../../../../img/execom_logo.png';
import { getImageUrl } from '../../../../store/getImageUrl';

export const EmployeeContainer = styled.div`
  height: 70vh;
  overflow: auto;
`;

export const TableText = styled.span`
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const EmployeesImage = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  margin-right: 40px;
  background-color: #9d9da2;
  min-width: 40px;
  background-image: url(${props =>
    props.image ? `${getImageUrl(props.image, 40)}` : ExecomLogo});
  background-size: cover;
`;

export const TableRow = styled.div`
  width: ${props => (props.width ? props.width : '10%')};
  flex-grow: 1;
  display: flex;
`;
