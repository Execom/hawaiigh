import * as Yup from 'yup';

const emailRegExp = new RegExp('^[a-zA-Z]+$');

export const validationSchema = Yup.object().shape({
  fullName: Yup.string().required('Full Name is a required field.'),
  email: Yup.mixed()
    .required('Email is a required field.')
    .test(
      'does-contain-invalid-characters',
      'Please enter a valid email address. Only letters are allowed.',
      function(value) {
        return emailRegExp.test(value);
      }
    ),
  jobTitle: Yup.string().required('Job title is a required field.'),
  userRole: Yup.string().required('User role is a required field.'),
  leaveProfileId: Yup.string().required('Leave profile is a required field.'),
  teamId: Yup.mixed()
    .test(
      'is-member-and-approver',
      "Employee can't be both an approver and member of a team.",
      function(value) {
        return this.options.parent.approverTeams
          .map(team => team.id)
          .indexOf(+value);
      }
    )
    .required('Team is a required field.'),
  startedProfessionalCareerDate: Yup.string()
    .nullable()
    .required('Started professional career date is a required field.'),
  startedWorkingDate: Yup.string()
    .nullable()
    .required('Started working date is a required field.'),
  startedWorkingAtExecomDate: Yup.string()
    .nullable()
    .required('Started working at Execom date is a required field.'),
  approverTeams: Yup.array()
});

export const addEmailDomain = email => email.concat('@execom.eu');

export const removeEmailDomain = email =>
  email.slice(0, email.lastIndexOf('@'));

export const constructDeleteEmployeeMessage = isTheOnlyHrManager => {
  const message = isTheOnlyHrManager
    ? 'This action will delete the only HR manager, no one with admin privileges will have access to the application.'
    : 'This action will delete the selected employee who will lose access to the application.';
  return `${message} Are you sure you want to continue?`;
};
