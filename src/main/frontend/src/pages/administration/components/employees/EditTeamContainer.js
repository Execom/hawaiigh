import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import TeamAudits from '../../../../components/audit/team/TeamAudits';
import { toggleModalSize } from '../../../../store/actions/drawerActions';
import { getWindowWidth } from '../../../../store/selectors';
import EditTeam from '../teams/EditTeam';

export class EditTeamContainer extends Component {
  state = {
    activeTab: '1'
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.windowWidth > 991 &&
      this.props.windowWidth < 992 &&
      this.state.activeTab === '3'
    ) {
      this.props.dispatch(toggleModalSize());
      this.setState({
        activeTab: '1'
      });
    }
  }

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });

      if (tab === '3' || this.state.activeTab === '3') {
        this.props.dispatch(toggleModalSize());
      }
    }
  };

  render() {
    return (
      <div className="d-flex flex-column flex-grow-1">
        <Nav className="justify-content-between" tabs>
          <NavItem className="flex-grow-1">
            <NavLink
              className={this.state.activeTab === '1' ? 'active' : ''}
              onClick={() => {
                this.toggle('1');
              }}
            >
              Edit
            </NavLink>
          </NavItem>
          {this.props.id !== 1 && (
            <Fragment>
              <NavItem className="flex-grow-1">
                <NavLink
                  className={this.state.activeTab === '2' ? 'active' : ''}
                  onClick={() => {
                    this.toggle('2');
                  }}
                >
                  Audit
                </NavLink>
              </NavItem>
            </Fragment>
          )}
        </Nav>
        <TabContent
          className="d-flex flex-grow-1 flex-column"
          activeTab={this.state.activeTab}
        >
          <TabPane
            className={`${this.state.activeTab === '1' &&
              'd-flex'} flex-grow-1 flex-column`}
            tabId="1"
          >
            <EditTeam id={this.props.id} />
          </TabPane>
          <TabPane
            className={`${this.state.activeTab === '2' &&
              'd-flex'} flex-grow-1 flex-column`}
            tabId="2"
          >
            <TeamAudits id={this.props.id} />
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  windowWidth: getWindowWidth(state)
});

export default connect(mapStateToProps)(EditTeamContainer);
