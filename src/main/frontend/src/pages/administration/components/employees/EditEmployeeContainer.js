import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import EmployeeAudits from '../../../../components/audit/employee/EmployeeAudits';
import AllowanceAudits from '../../../../components/audit/allowance/AllowanceAudits';
import { toggleModalSize } from '../../../../store/actions/drawerActions';
import { getWindowWidth, getAllowance } from '../../../../store/selectors';
import ManualAdjustContainer from '../manual-adjust/ManualAdjustContainer';
import EditEmployee from './EditEmployee';

export class EditEmployeeContainer extends Component {
  state = {
    activeTab: '1'
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.windowWidth > 991 &&
      this.props.windowWidth < 992 &&
      this.state.activeTab === '3'
    ) {
      this.props.dispatch(toggleModalSize());
      this.setState({
        activeTab: '1'
      });
    }
  }

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });

      if (tab === '3' || this.state.activeTab === '3') {
        this.props.dispatch(toggleModalSize());
      }
    }
  };

  render() {
    return (
      <div className="d-flex flex-column flex-grow-1">
        <Nav className="justify-content-between" tabs>
          {this.props.id !== 1 && (
            <Fragment>
              <NavItem className="flex-grow-1">
                <NavLink
                  className={this.state.activeTab === '1' ? 'active' : ''}
                  onClick={() => {
                    this.toggle('1');
                  }}
                >
                  Edit
                </NavLink>
              </NavItem>
              <NavItem className="d-none d-lg-flex flex-grow-1 flex-column">
                <NavLink
                  className={this.state.activeTab === '3' ? 'active' : ''}
                  onClick={() => {
                    this.toggle('3');
                  }}
                >
                  Manual adjust
                </NavLink>
              </NavItem>
              <NavItem className="flex-grow-1">
                <NavLink
                  className={this.state.activeTab === '2' ? 'active' : ''}
                  onClick={() => {
                    this.toggle('2');
                  }}
                >
                  Audit
                </NavLink>
              </NavItem>
            </Fragment>
          )}
        </Nav>
        <TabContent
          className="d-flex flex-grow-1 flex-column"
          activeTab={this.state.activeTab}
        >
          <TabPane
            className={`${this.state.activeTab === '1' &&
              'd-flex'} flex-grow-1 flex-column`}
            tabId="1"
          >
            <EditEmployee id={this.props.id} />
          </TabPane>
          {this.props.id !== 1 && (
            <Fragment>
              <TabPane
                className={`${this.state.activeTab === '2' &&
                  'd-flex'} flex-grow-1 flex-column`}
                tabId="2"
              >
                <EmployeeAudits id={this.props.id} />
                {this.props.allowance && (
                  <AllowanceAudits id={this.props.allowance.id} />
                )}
              </TabPane>
              <TabPane
                className={`${this.state.activeTab === '3' &&
                  'd-lg-flex'} flex-grow-1 flex-column d-none`}
                tabId="3"
              >
                <ManualAdjustContainer id={this.props.id} />
              </TabPane>
            </Fragment>
          )}
        </TabContent>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  windowWidth: getWindowWidth(state),
  allowance: getAllowance(state)
});

export default connect(mapStateToProps)(EditEmployeeContainer);
