import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _includes from 'lodash/includes';
import {
  SeparatorRow,
  TableCell,
  TableRow
} from '@/components/common/TableStyledComponents';
import { UserImage } from '@/components/common/userImage';
import { ACTIVE, DELETED, INACTIVE, HR_MANAGER } from '@/helpers/enum';
import Execom from '@/img/execom.jpg';
import ActivateIcon from '@/img/icons/active_users.svg';
import DeleteIcon from '@/img/icons/delete.svg';
import EditIcon from '@/img/icons/edit.svg';
import ScheduleIcon from '@/img/icons/scheduled_users.svg';
import PendingIconRed from '@/img/icons/pending_red.svg';
import { openActionDrawer } from '@/store/actions/drawerActions';
import {
  openConfirmationModal,
  openEmployeeRestoreModal,
  openEmployeeDeleteModal,
  openEmployeeCancelDeleteModal
} from '@/store/actions/modalActions';
import {
  removeEmployee,
  restoreEmployee,
  cancelEmployeeDelete
} from '@/store/actions/userActions';
import { getImageUrl } from '@/store/getImageUrl';
import { getTeams } from '@/store/selectors';
import { Icon } from '@/pages/administration/components/years/styled';
import { formatDate } from '@/store/helperFunctions';
import { constructDeleteEmployeeMessage } from '@/pages/administration/components/employees/helpers';

class EmployeeInfo extends Component {
  renderActions = employee => {
    switch (employee.userStatusType) {
      case DELETED:
        return (
          <div className="text-right">
            <Icon
              onClick={() =>
                this.props.openEmployeeRestoreModal({
                  clickAction: restoreEmployee,
                  values: {
                    ...this.props
                  },
                  options: {
                    restoreWithoutDate: true
                  },
                  teams: this.props.teams
                })
              }
              className="mr-2"
              src={ActivateIcon}
              alt="Edit icon"
            />
            <Icon
              onClick={() =>
                this.props.openEmployeeRestoreModal({
                  clickAction: restoreEmployee,
                  values: {
                    ...this.props
                  },
                  options: {
                    isScheduleRestore: true
                  },
                  teams: this.props.teams
                })
              }
              src={ScheduleIcon}
              alt="Restore icon"
            />
          </div>
        );
      case ACTIVE:
        return (
          <div className="text-right">
            {employee.stoppedWorkingAtExecomDate != null && (
              <Icon
                onClick={e => {
                  e.stopPropagation();
                  this.handleCancelDelete(employee);
                }}
                className="mr-2"
                src={PendingIconRed}
              />
            )}
            <Icon
              onClick={() =>
                this.props.openActionDrawer({
                  type: 'editEmployee',
                  heading: 'Edit Employee',
                  id: employee.id
                })
              }
              className="mr-2"
              src={EditIcon}
              alt="Edit icon"
            />
            <Icon
              onClick={e => {
                e.stopPropagation();
                this.handleDelete(employee);
              }}
              src={DeleteIcon}
              alt="Delete icon"
            />
          </div>
        );
      default:
        return (
          <Fragment>
            <Icon
              onClick={e => {
                e.stopPropagation();
                this.props.openEmployeeRestoreModal({
                  clickAction: restoreEmployee,
                  values: {
                    ...this.props
                  },
                  options: {
                    restoreWithoutDate: true
                  },
                  teams: this.props.teams
                });
              }}
              className="mr-2"
              src={ActivateIcon}
              alt="Restore icon"
            />
            <Icon
              onClick={() =>
                this.props.openActionDrawer({
                  type: 'editEmployee',
                  heading: 'Edit Employee',
                  id: employee.id
                })
              }
              className="mr-2"
              src={EditIcon}
              alt="Edit icon"
            />
            <Icon
              onClick={e => {
                e.stopPropagation();
                this.handleDelete(employee);
              }}
              src={DeleteIcon}
              alt="Delete icon"
            />
          </Fragment>
        );
    }
  };

  handleDelete = employee => {
    const message = constructDeleteEmployeeMessage(
      this.isTheOnlyHrManager(employee.userRole)
    );

    this.props.openEmployeeDeleteModal({
      clickAction: removeEmployee,
      values: employee,
      message
    });
  };

  handleCancelDelete = employee => {
    this.props.openEmployeeCancelDeleteModal({
      clickAction: cancelEmployeeDelete,
      values: employee,
      message: `This action will cancel scheduled deletion of employee ${
        employee.fullName
      } on 
        ${formatDate(
          employee.stoppedWorkingAtExecomDate
        )}. Are you sure you want to continue?`
    });
  };

  isTheOnlyHrManager = userRole => {
    return (
      userRole === HR_MANAGER &&
      this.props.employees.filter(
        employee => employee.userRole === 'HR_MANAGER'
      ).length === 1
    );
  };

  render() {
    const {
      id,
      fullName,
      email,
      jobTitle,
      yearsOfService,
      imageUrl,
      userStatusType
    } = this.props;
    const clickable = userStatusType === ACTIVE || userStatusType === INACTIVE;

    const startedWorkingAtExecomDate = formatDate(
      this.props.startedWorkingAtExecomDate
    );
    const stoppedWorkingAtExecomDate = formatDate(
      this.props.stoppedWorkingAtExecomDate
    );
    const image = getImageUrl(imageUrl, 20);

    return (
      <Fragment>
        <TableRow
          clickable={clickable}
          onClick={
            clickable
              ? () =>
                  this.props.openActionDrawer({
                    type: 'editEmployee',
                    heading: 'Edit Employee',
                    id
                  })
              : null
          }
          className="d-block d-lg-table-row"
        >
          <TableCell
            width="70"
            data-label="Image"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell py-1"
          >
            <div className="text-right text-lg-left">
              <UserImage
                borderBlack
                image={
                  _includes(getImageUrl(image, 100), 'null') ? Execom : image
                }
                isSmall
                size="30px"
              />
            </div>
          </TableCell>
          <TableCell
            data-label="Name"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>{fullName}</span>
            </div>
          </TableCell>
          <TableCell
            data-label="Job title"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>{jobTitle}</span>
            </div>
          </TableCell>
          <TableCell
            data-label="Email"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>{email.replace(/@execom.eu/g, '')}</span>
            </div>
          </TableCell>
          <TableCell
            data-label="Years of service"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>Years of Service: {yearsOfService}</span>
            </div>
          </TableCell>
          {userStatusType === INACTIVE && (
            <TableCell
              data-label="Restoration date"
              className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              color="white"
              title={`Scheduled to restore on ${startedWorkingAtExecomDate}`}
            >
              {startedWorkingAtExecomDate}
            </TableCell>
          )}
          {userStatusType === DELETED && (
            <TableCell
              data-label="Stopped working at Execom date"
              className="d-flex justify-content-between d-lg-table-cell"
              color="white"
              title={`Stopped working at Execom on ${stoppedWorkingAtExecomDate}`}
            >
              {stoppedWorkingAtExecomDate}
            </TableCell>
          )}
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell py-2"
            data-label="Actions"
            color="white"
          >
            <div className="text-right">{this.renderActions(this.props)}</div>
          </TableCell>
        </TableRow>
        <SeparatorRow />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  teams: getTeams(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      removeEmployee,
      cancelEmployeeDelete,
      openActionDrawer,
      restoreEmployee,
      openEmployeeRestoreModal,
      openEmployeeDeleteModal,
      openEmployeeCancelDeleteModal,
      openConfirmationModal
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeInfo);
