import { FieldArray, Formik } from 'formik';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TeamSearchResults from '@/components/search-dropdown/search-results/TeamSearchResults';
import SearchDropdown from '@/components/search-dropdown/SearchDropdown';
import { ACTIVE, INACTIVE } from '@/helpers/enum';
import { closeDrawer } from '@/store/actions/drawerActions';
import { requestLeaveProfiles } from '@/store/actions/leaveProfileActions';
import {
  clearTeams,
  requestTeams,
  searchTeams
} from '@/store/actions/teamActions';
import { createEmployee } from '@/store/actions/userActions';
import {
  getIsSubmitting,
  getLeaveProfileResults,
  getSearchTeams,
  getTeams
} from '@/store/selectors';
import DrawerCalendarInput, {
  DrawerContent,
  DrawerFooter,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import {
  validationSchema,
  addEmailDomain
} from '@/pages/administration/components/employees/helpers';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';
import { USER, HR_MANAGER, OFFICE_MANAGER } from '@/helpers/enum';

class CreateEmployee extends Component {
  componentDidMount() {
    this.props.requestTeams();
    this.props.requestLeaveProfiles();
  }

  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    const {
      teams: { results },
      leaveProfiles: leaveProfileResults
    } = this.props;

    if (!results.length || !leaveProfileResults.length) return null;

    const teams = results.map(team => {
      return (
        <option key={team.id} value={team.id}>
          {team.name}
        </option>
      );
    });

    const leaveProfiles = leaveProfileResults.map(leaveProfile => {
      return (
        <option key={leaveProfile.id} value={leaveProfile.id}>
          {leaveProfile.name}
        </option>
      );
    });

    return (
      <Formik
        validationSchema={validationSchema}
        initialValues={{
          fullName: '',
          email: '',
          jobTitle: '',
          userRole: '',
          teamId: '',
          startedProfessionalCareerDate: null,
          startedWorkingDate: null,
          startedWorkingAtExecomDate: null,
          leaveProfileId: '',
          userStatusType: INACTIVE,
          approverTeams: []
        }}
        onSubmit={(values, { setSubmitting }) => {
          const { createEmployee, activeTab } = this.props;
          const {
            email,
            startedProfessionalCareerDate,
            startedWorkingDate,
            startedWorkingAtExecomDate
          } = values;

          createEmployee({
            ...values,
            email: addEmailDomain(email),
            startedProfessionalCareerDate: startedProfessionalCareerDate
              ? moment(startedProfessionalCareerDate).format('YYYY-MM-DD')
              : null,
            startedWorkingDate: moment(startedWorkingDate).format('YYYY-MM-DD'),
            startedWorkingAtExecomDate: moment(
              startedWorkingAtExecomDate
            ).format('YYYY-MM-DD'),
            activeTab: activeTab
          });
          setSubmitting(false);
        }}
      >
        {({
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                caption="Full name"
                name="fullName"
                handleChange={handleChange}
                className={
                  errors.fullName && touched.fullName ? 'border-danger' : ''
                }
              />
              <InputPair
                view="email"
                caption="Email"
                name="email"
                handleChange={handleChange}
                className={errors.email && touched.email ? 'border-danger' : ''}
              />
              <InputPair
                caption="Job title"
                name="jobTitle"
                handleChange={handleChange}
                className={
                  errors.jobTitle && touched.jobTitle ? 'border-danger' : ''
                }
              />
              <InputPair
                view="select"
                caption="User role"
                className={
                  errors.userRole && touched.userRole ? 'border-danger' : ''
                }
                name="userRole"
                handleChange={handleChange}
                value={values.userRole}
                disabledValue="Select role"
                options={
                  <Fragment>
                    <option value={USER}>User</option>
                    <option value={HR_MANAGER}>HR Manager</option>
                    <option value={OFFICE_MANAGER}>Office Manager</option>
                  </Fragment>
                }
              />
              <InputPair
                view="select"
                caption="Leave profile"
                className={
                  errors.leaveProfileId && touched.leaveProfileId
                    ? 'border-danger'
                    : ''
                }
                name="leaveProfileId"
                handleChange={handleChange}
                value={values.leaveProfileId}
                disabledValue="Select leave profile"
                options={leaveProfiles}
              />
              <InputPair
                view="select"
                caption="Team"
                className={
                  errors.teamId && touched.teamId ? 'border-danger' : ''
                }
                name="teamId"
                handleChange={handleChange}
                value={values.teamId}
                disabledValue="Select team"
                options={teams}
              />
              <InputPair
                view="other"
                caption="Started professional career date"
                element={
                  <DatePicker
                    customInput={<DrawerCalendarInput />}
                    className={
                      errors.startedProfessionalCareerDate &&
                      touched.startedProfessionalCareerDate
                        ? 'border-danger'
                        : ''
                    }
                    showYearDropdown
                    dateFormat="dd MMM yyyy"
                    locale="en-GB"
                    selected={values.startedProfessionalCareerDate}
                    onChange={e => {
                      setFieldValue('startedProfessionalCareerDate', e);
                    }}
                  />
                }
              />
              <InputPair
                view="other"
                caption="Started working date"
                element={
                  <DatePicker
                    customInput={<DrawerCalendarInput />}
                    className={
                      errors.startedWorkingDate && touched.startedWorkingDate
                        ? 'border-danger'
                        : ''
                    }
                    showYearDropdown
                    dateFormat="dd MMM yyyy"
                    locale="en-GB"
                    selected={values.startedWorkingDate}
                    onChange={e => {
                      setFieldValue('startedWorkingDate', e);
                    }}
                  />
                }
              />
              <InputPair
                view="other"
                caption="Started working at execom date"
                element={
                  <DatePicker
                    customInput={<DrawerCalendarInput />}
                    className={
                      errors.startedWorkingAtExecomDate &&
                      touched.startedWorkingAtExecomDate
                        ? 'border-danger'
                        : ''
                    }
                    showYearDropdown
                    dateFormat="dd MMM yyyy"
                    locale="en-GB"
                    selected={values.startedWorkingAtExecomDate}
                    onChange={e => {
                      setFieldValue('startedWorkingAtExecomDate', e);
                      setFieldValue(
                        'userStatusType',
                        moment(e).isSameOrBefore(moment(), 'day')
                          ? ACTIVE
                          : INACTIVE
                      );
                    }}
                  />
                }
              />
              <InputPair
                view="switch"
                caption="Active"
                name="userStatusType"
                handleChange={checked => {
                  setFieldValue('userStatusType', checked ? ACTIVE : INACTIVE);
                  setFieldValue(
                    'startedWorkingAtExecomDate',
                    checked
                      ? moment().toDate()
                      : moment()
                          .add(1, 'day')
                          .toDate()
                  );
                }}
                checked={values.userStatusType === ACTIVE}
              />
              <InputPair
                view="other"
                caption="Teams search"
                element={
                  <SearchDropdown
                    searchResults={this.props.searchedTeams}
                    searchAction={searchTeams}
                    resetAction={clearTeams}
                    selectedTeams={values.approverTeams}
                    employeeTeamId={values.teamId}
                    placeholder="Search for teams..."
                  >
                    {(inputReference, resetInput) => (
                      <TeamSearchResults
                        resetInput={resetInput}
                        inputReference={inputReference}
                        teams={this.props.searchedTeams}
                        selectedTeams={values.approverTeams}
                        employeeTeamId={values.teamId}
                      />
                    )}
                  </SearchDropdown>
                }
              />
              <InputPair
                view="other"
                caption="Approver in teams"
                element={
                  !values.approverTeams.length ? (
                    <div>
                      <span>No teams selected</span>
                    </div>
                  ) : (
                    values.approverTeams.map((team, index) => (
                      <h5
                        key={team.id}
                        className="d-flex justify-content-between mb-2"
                      >
                        {team.name}
                        <FieldArray
                          name="approverTeams"
                          render={arrayHelpers => (
                            <span
                              className="text-danger ml-2"
                              onClick={() => {
                                arrayHelpers.remove(index);
                              }}
                            >
                              x
                            </span>
                          )}
                        />
                      </h5>
                    ))
                  )
                }
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting}
              formClick={handleSubmit}
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      </Formik>
    );
  }
}

const mapStateToProps = state => ({
  teams: getTeams(state),
  leaveProfiles: getLeaveProfileResults(state),
  searchedTeams: getSearchTeams(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createEmployee,
      requestTeams,
      requestLeaveProfiles,
      searchTeams,
      clearTeams,
      closeDrawer,
      toastrError
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateEmployee);
