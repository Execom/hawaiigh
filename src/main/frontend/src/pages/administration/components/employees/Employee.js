import _get from 'lodash/get';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import withResetOnNavigate from '../../../../components/HOC/withResetOnNavigate';
import { DELETED } from '../../../../helpers/enum';
import {
  openConfirmationModal,
  openEmployeeRestoreModal
} from '../../../../store/actions/modalActions';
import { requestTeams } from '../../../../store/actions/teamActions';
import {
  removeEmployee,
  requestEmployee,
  restoreEmployee,
  updateEmployee
} from '../../../../store/actions/userActions';
import { getImageUrl } from '../../../../store/getImageUrl';
import {
  getEmployee,
  getTeams,
  getLeaveProfileResults
} from '../../../../store/selectors';
import getReadableChangeValue from '../../../../components/audit/helpers/getReadableChangeValue';
import { requestLeaveProfiles } from '../../../../store/actions/leaveProfileActions';

class Employee extends Component {
  componentDidMount() {
    this.props.requestEmployee(this.props.match.params.id);
    this.props.requestLeaveProfiles();
  }

  componentDidUpdate(prevProps) {
    if (
      !prevProps.employee &&
      _get(this.props.employee, 'userStatusType') === DELETED
    ) {
      this.props.requestTeams();
    }
  }

  renderButton = deleted => {
    if (deleted) {
      return (
        <Fragment>
          <button
            className="btn btn-danger mx-2"
            onClick={() =>
              this.props.openEmployeeRestoreModal({
                clickAction: restoreEmployee,
                values: {
                  ...this.props.employee
                },
                options: {
                  restoreWithoutDate: true
                },
                teams: this.props.teams
              })
            }
          >
            Activate Now
          </button>
          <button
            className="btn btn-danger mx-2"
            onClick={() =>
              this.props.openEmployeeRestoreModal({
                clickAction: restoreEmployee,
                values: {
                  ...this.props.employee
                },
                options: {
                  isScheduleRestore: true
                },
                teams: this.props.teams
              })
            }
          >
            Schedule restoring
          </button>
        </Fragment>
      );
    }

    return (
      <button
        className="btn btn-danger mx-2"
        onClick={() =>
          this.props.openConfirmationModal({
            clickAction: removeEmployee,
            values: { ...this.props.employee, userStatusType: DELETED },
            message: `Are you sure you want to archive ${this.props.employee.fullName}?`
          })
        }
      >
        Archive
      </button>
    );
  };

  render() {
    if (!this.props.employee || !this.props.leaveProfiles.length) return null;

    const {
      employee: {
        id,
        teamName,
        fullName,
        email,
        leaveProfileId,
        jobTitle,
        yearsOfService,
        startedWorkingDate,
        startedWorkingAtExecomDate,
        startedProfessionalCareerDate,
        imageUrl,
        userRole,
        userStatusType,
        approverTeams
      }
    } = this.props;

    return (
      <div className="d-flex h-100 p-4 flex-column align-items-center">
        <img src={getImageUrl(imageUrl, 100)} alt="user" />
        <h1>Name</h1>
        <h5 className="text-danger mb-3">{fullName}</h5>
        <h1>email</h1>
        <h5 className="text-danger mb-3">{email}</h5>
        <h1>Job Title</h1>
        <h5 className="text-danger mb-3">{jobTitle}</h5>
        <h1>Approver</h1>
        <h5 className="text-danger mb-3">
          {userRole === 'APPROVER' ? 'Yes' : 'No'}
        </h5>
        <h1>Leave profile</h1>
        <h5 className="text-danger mb-3">
          {getReadableChangeValue('leaveProfileId', leaveProfileId)}
        </h5>
        <h1>Years of service</h1>
        <h5 className="text-danger mb-3">{yearsOfService}</h5>
        {startedProfessionalCareerDate && (
          <Fragment>
            <h1>Started professional career</h1>
            <h5 className="text-danger mb-3">
              {startedProfessionalCareerDate}
            </h5>
          </Fragment>
        )}
        <h1>Started working at execom</h1>
        <h5 className="text-danger mb-3">{startedWorkingAtExecomDate}</h5>
        <h1>Started working date</h1>
        <h5 className="text-danger mb-3">{startedWorkingDate}</h5>
        {userStatusType !== DELETED && (
          <Fragment>
            <h1>Team name</h1>
            <h5 className="text-danger mb-3">{teamName}</h5>
            {approverTeams.length > 0 && (
              <Fragment>
                <h1>Approver in teams:</h1>
                <div className="text-danger mb-3 text-center">
                  {approverTeams.map(team => (
                    <h5 key={team.id}>{team.name}</h5>
                  ))}
                </div>
              </Fragment>
            )}
          </Fragment>
        )}
        <div>
          {userStatusType !== DELETED && (
            <button className="btn mx-2">
              <NavLink to={`/employee/${id}/edit`}>Edit</NavLink>
            </button>
          )}
          <NavLink
            className="btn btn-primary mx-2"
            to={`/employee/${id}/manual-adjust`}
          >
            Manual adjust
          </NavLink>
          {this.renderButton(userStatusType === DELETED)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  employee: getEmployee(state),
  teams: getTeams(state),
  leaveProfiles: getLeaveProfileResults(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestEmployee,
      removeEmployee,
      restoreEmployee,
      openConfirmationModal,
      updateEmployee,
      openEmployeeRestoreModal,
      requestTeams,
      requestLeaveProfiles
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(Employee));
