import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestYearsWithAllowance } from '../../../../store/actions/allowanceActions';
import { getYearsWithAllowance } from '../../../../store/selectors';
import ManualAdjust from './ManualAdjust';

class ManualAdjustContainer extends Component {
  componentDidMount() {
    this.props.requestYearsWithAllowance({
      userId: this.props.id
    });
  }

  render() {
    return (
      this.props.yearsWithAllowance.length && (
        <ManualAdjust
          yearsWithAllowance={this.props.yearsWithAllowance}
          id={this.props.id}
        />
      )
    );
  }
}

const mapStateToProps = state => ({
  yearsWithAllowance: getYearsWithAllowance(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestYearsWithAllowance
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManualAdjustContainer);
