import { Formik } from 'formik';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Yup from 'yup';
import { selectFromYearArray } from '@/components/calendar/calendarUtils';
import ArrowDown from '@/img/icons/arrow-down.svg';
import {
  requestAllowanceForUser,
  requestYearsWithAllowance,
  updateAllowance
} from '@/store/actions/allowanceActions';
import { closeDrawer } from '@/store/actions/drawerActions';
import { openInputModal } from '@/store/actions/modalActions';
import { getAllowance, getIsSubmitting } from '@/store/selectors';
import { ArrowSelect } from '@/pages/dashboard/components/styled';
import {
  DrawerContent,
  DrawerFooter,
  DrawerInput,
  DrawerTextArea
} from '@/pages/administration/components/common/DrawerElements';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';
import { showOnlyOneDecimal } from '@/pages/administration/helpers';
import { makeAllowanceInDays } from '@/pages/administration/helpers';

class ManualAdjust extends Component {
  state = {
    selectedYear: selectFromYearArray(
      moment().year(),
      this.props.yearsWithAllowance,
      'year'
    )
  };

  componentDidMount() {
    this.props.requestAllowanceForUser({
      year: this.state.selectedYear,
      userId: this.props.id
    });
  }

  handleYearChange = year => {
    this.props.requestAllowanceForUser({
      year,
      userId: this.props.id
    });

    this.setState({
      selectedYear: year
    });
  };

  convertBackToHours = obj => {
    Number((obj.bonusManualAdjust *= 8));
    Number((obj.carriedOver *= 8));
    Number((obj.manualAdjust *= 8));
    Number((obj.trainingManualAdjust *= 8));

    return obj;
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    const { allowance } = this.props;
    const allowanceInDays = makeAllowanceInDays(allowance);

    if (!allowance) return null;

    const {
      annual,
      training,
      approvedBonus,
      bonusManualAdjust,
      carriedOver,
      manualAdjust,
      trainingManualAdjust,
      pendingTraining,
      pendingAnnual,
      takenAnnual,
      takenTraining,
      expiredCarriedOver,
      id
    } = allowanceInDays;

    const validationSchema = Yup.object().shape({
      bonusManualAdjust: Yup.number('Please enter a valid number.')
        .min(0, 'Negative input for bonus days is not allowed.')
        .max(5, 'Employee can be granted for a maximum of 5 bonus days.')
        .required('Bonus days (manual) is a required field.'),
      carriedOver: Yup.number('Please enter a valid number.')
        .min(0, 'Negative input for carried over days is not allowed.')
        .max(5, 'Employee can carry over a maximum of 5 days.')
        .required('Carried over is a required field.'),
      manualAdjust: Yup.number('Please enter a valid number.')
        .min(
          -annual,
          `Manual adjustment can be done for a minimum of ${-annual} days`
        )
        .max(30, 'Manual adjustment can be done for a maximum of 30 days')
        .required('Manual adjust (leave) is a required field.'),
      trainingManualAdjust: Yup.number('Please enter a valid number.')
        .min(
          -2,
          'Manual adjustment of training leave can be done for a minimum of -2 days.'
        )
        .max(
          2,
          'Manual adjustment of training leave can be done for a maximum of 2 days.'
        )
        .required('Manual adjust (training & education) is a required field.'),
      comment: Yup.string().required('Comment is required.')
    });

    return (
      <div className="d-flex flex-column flex-grow-1">
        <div className="d-flex justify-content-center pt-4">
          <div className="position-relative">
            <select
              className="select-custom select-year pl-2 pr-5 py-1 rounded"
              onChange={e => this.handleYearChange(e.target.value)}
              value={this.state.selectedYear}
            >
              {this.props.yearsWithAllowance.map(year => (
                <option key={year} value={year}>
                  {year}
                </option>
              ))}
            </select>
            <ArrowSelect src={ArrowDown} alt="arrow down" />
          </div>
        </div>
        <Formik
          validationSchema={validationSchema}
          initialValues={{
            id,
            bonusManualAdjust,
            carriedOver,
            manualAdjust,
            trainingManualAdjust,
            comment: ''
          }}
          onSubmit={values =>
            this.props.updateAllowance(this.convertBackToHours(values))
          }
          enableReinitialize
          render={({
            handleSubmit,
            values,
            errors,
            touched,
            isSubmitting,
            setFieldValue,
            dirty
          }) => (
            <Fragment>
              <DrawerContent className="d-flex flex-column p-4">
                <div className="container">
                  <div>
                    <div className="row pb-2">
                      <div className="col w-100" />
                      <div className="col w-100">Leave</div>
                      <div className="col w-100">Training & education</div>
                    </div>
                  </div>
                  <div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">
                        Annual allowance
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(annual)}
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(training)}
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">
                        Expired carried over
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={expiredCarriedOver}
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value="N/A"
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">
                        Approved Bonus days
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(approvedBonus)}
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value="N/A"
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">
                        Bonus days (manual)
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className={`${
                            errors.bonusManualAdjust &&
                            touched.bonusManualAdjust
                              ? 'border-danger'
                              : ''
                          } p-2 w-100 border rounded`}
                          name="bonusManualAdjust"
                          onChange={e => {
                            setFieldValue('bonusManualAdjust', e.target.value);
                          }}
                          value={values.bonusManualAdjust}
                          type="text"
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value="N/A"
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">
                        Carried over
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className={`${
                            errors.carriedOver && touched.carriedOver
                              ? 'border-danger'
                              : ''
                          } p-2 w-100 border rounded`}
                          name="carriedOver"
                          onChange={e =>
                            setFieldValue('carriedOver', e.target.value)
                          }
                          value={values.carriedOver}
                          type="text"
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value="N/A"
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">
                        Manual adjust
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className={`${
                            errors.manualAdjust && touched.manualAdjust
                              ? 'border-danger'
                              : ''
                          } p-2 w-100 border rounded`}
                          name="manualAdjust"
                          onChange={e =>
                            setFieldValue('manualAdjust', e.target.value)
                          }
                          value={values.manualAdjust}
                          type="text"
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className={`${
                            errors.trainingManualAdjust &&
                            touched.trainingManualAdjust
                              ? 'border-danger'
                              : ''
                          } p-2 w-100 border rounded`}
                          name="trainingManualAdjust"
                          onChange={e =>
                            setFieldValue(
                              'trainingManualAdjust',
                              e.target.value
                            )
                          }
                          value={values.trainingManualAdjust}
                          type="text"
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">Total</div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(
                            annual +
                              approvedBonus +
                              Number(values.bonusManualAdjust) +
                              Number(values.carriedOver) +
                              Number(values.manualAdjust)
                          )}
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(
                            training + Number(values.trainingManualAdjust)
                          )}
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">Taken</div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(takenAnnual)}
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(takenTraining)}
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">
                        Remaining
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(
                            annual +
                              approvedBonus +
                              Number(values.bonusManualAdjust) +
                              Number(values.carriedOver) +
                              Number(values.manualAdjust) -
                              takenAnnual
                          )}
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(
                            training +
                              Number(values.trainingManualAdjust) -
                              pendingTraining
                          )}
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">Pending</div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(pendingAnnual)}
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(pendingTraining)}
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col w-100 align-self-center">
                        Remaining (including pending)
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(
                            annual +
                              approvedBonus +
                              Number(values.bonusManualAdjust) +
                              Number(values.carriedOver) +
                              Number(values.manualAdjust) -
                              takenAnnual -
                              pendingAnnual
                          )}
                        />
                      </div>
                      <div className="col w-100">
                        <DrawerInput
                          className="p-2 w-100 border rounded"
                          disabled
                          value={showOnlyOneDecimal(
                            training + Number(values.trainingManualAdjust)
                          )}
                        />
                      </div>
                    </div>
                    <div className="row pb-2">
                      <div className="col-4 pt-2 align-self-start">Comment</div>
                      <div className="col-8">
                        <DrawerTextArea
                          className={`${
                            errors.comment && touched.comment
                              ? 'border-danger'
                              : ''
                          } p-2 w-100 border rounded`}
                          name="comment"
                          onChange={e =>
                            setFieldValue('comment', e.target.value)
                          }
                          value={values.comment}
                          disabled={!dirty}
                          type="text"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </DrawerContent>
              <DrawerFooter
                isSubmitting={this.props.isSubmitting || !dirty}
                formClick={handleSubmit}
                closeClick={this.props.closeDrawer}
              />
              <FormikErrorDetector
                formikProps={{ isSubmitting, errors }}
                onError={this.showErrorToast}
              />
            </Fragment>
          )}
        />
        {/* <AllowanceAudits id={id} /> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  allowance: getAllowance(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestAllowanceForUser,
      requestYearsWithAllowance,
      openInputModal,
      closeDrawer,
      updateAllowance,
      toastrError
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManualAdjust);
