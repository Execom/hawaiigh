import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { TableCell, TableRow } from '@/components/common/TableStyledComponents';
import AdministrationTable from '@/pages/administration/components/common/AdministrationTable';
import AllowanceInfo from './AllowanceInfo';
import {
  requestAllowancePerUserForYear,
  requestYearsWithAllowance
} from '@/store/actions/allowanceActions';
import {
  getAllowancePerUserForYear,
  getYearsWithAllowance,
  getTeams
} from '@/store/selectors';
import { requestTeamsName } from '../../../../store/actions/teamActions';
import { openAllowanceCalendarView } from '@/store/actions/modalActions';
import { requestPublicHolidays } from '@/store/actions/publicHolidayActions';
import { requestUserDays } from '@/store/actions/userActions';

class Allowances extends Component {
  state = {
    activeTab: 'Leave',
    year: new Date().getFullYear()
  };

  componentDidMount() {
    this.props.requestPublicHolidays();
    this.props.requestYearsWithAllowance({
      includeAllUsers: true
    });
    this.props.requestTeamsName();
    this.props.requestAllowancePerUserForYear({
      page: 0,
      size: 30,
      searchQuery: '',
      year: '2020'
    });
  }

  updateYear = year => {
    this.setState({ year });
  };

  handleCalendarClick = user => {
    this.props.requestUserDays({ year: this.state.year, userId: user.id });
    this.props.openAllowanceCalendarView({
      year: this.state.year,
      message: `${this.state.year} annual allowance for ${user.fullName}`
    });
  };

  handleTabChange = e => {
    this.setState({ activeTab: e.target.value });
  };

  render() {
    const allTeams = this.props.teams;
    const sortedYears = [...this.props.years].sort((a, b) => b - a);
    const { page: allowancePage, results } = this.props.allowances;

    const commonHeaderItems = ['Employee', 'Team'];
    const headerItems = {
      Leave: ['Carried', 'Total', 'Taken', 'Pending', 'Remaining'],
      Training: ['Total', 'Taken', 'Pending', 'Remaining'],
      Sickness: ['Total']
    };

    const headerItemsForRender = [
      ...commonHeaderItems,
      ...headerItems[this.state.activeTab]
    ];

    let allowances;

    if (results && !results.length) {
      allowances = (
        <TableRow className="d-block d-lg-table-row">
          <TableCell
            width="100%"
            color="white"
            className="py-3 text-center w-100 d-block d-lg-table-cell"
          >
            Sorry, no results found
          </TableCell>
        </TableRow>
      );
    } else {
      allowances = results
        ? results.map(usersWithAllowances => (
            <AllowanceInfo
              activeTab={this.state.activeTab}
              key={usersWithAllowances.user.id}
              openAllowanceCalendarModal={this.handleCalendarClick}
              {...usersWithAllowances}
            />
          ))
        : null;
    }

    return (
      <AdministrationTable
        searchBar
        allowanceTab
        handleTabChange={this.handleTabChange}
        allTeams={allTeams}
        allYears={sortedYears}
        allowancePage={allowancePage}
        headerItems={headerItemsForRender}
        body={allowances}
        page={this.props.allowances.page}
        requestAction={requestAllowancePerUserForYear}
        placeholderCellCount={8}
        activeTab={this.state.activeTab}
        updateYear={this.updateYear}
      />
    );
  }
}

const mapStateToProps = state => ({
  allowances: getAllowancePerUserForYear(state),
  years: getYearsWithAllowance(state),
  teams: getTeams(state).name
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestAllowancePerUserForYear,
      requestTeamsName,
      openAllowanceCalendarView,
      requestPublicHolidays,
      requestUserDays,
      requestYearsWithAllowance
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Allowances);
