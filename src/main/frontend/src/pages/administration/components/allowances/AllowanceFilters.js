import React, { Component } from 'react';
import { ArrowSelect } from '@/pages/dashboard/components/styled';
import ArrowDown from '@/img/icons/arrow-down.svg';
import { requestAllowancePerUserForYear } from '@/store/actions/allowanceActions';

class AllowanceFilters extends Component {
  handleYearChange = e => {
    const year = e.target.value;

    this.props.updateFilterYear(year);
    this.props.dispatch(
      requestAllowancePerUserForYear({
        searchQuery: this.props.searchQuery,
        page: 0,
        size: 30,
        year: year,
        teamId: this.props.teamId
      })
    );
    this.props.updateYear(year);
  };

  handleTeamChange = e => {
    const teamId = e.target.value;

    this.props.updateTeam(teamId);
    this.props.dispatch(
      requestAllowancePerUserForYear({
        searchQuery: this.props.searchQuery,
        page: 0,
        size: 30,
        year: this.props.year,
        teamId: teamId
      })
    );
  };

  render() {
    const { allTeams, allYears, activeTab, handleTabChange } = this.props;

    return (
      <div className="allowance-group">
        <div className="position-relative allowance-tab">
          <div>
            Show:
            <button
              value="Leave"
              type="button"
              className={
                activeTab === 'Leave'
                  ? 'radio-button-custom active-tab'
                  : 'radio-button-custom'
              }
              onClick={handleTabChange}
            >
              Leave
            </button>
            <button
              value="Sickness"
              type="button"
              className={
                activeTab === 'Sickness'
                  ? 'radio-button-custom active-tab'
                  : 'radio-button-custom'
              }
              onClick={handleTabChange}
            >
              Sickness
            </button>
            <button
              value="Training"
              type="button"
              className={
                activeTab === 'Training'
                  ? 'radio-button-custom active-tab'
                  : 'radio-button-custom'
              }
              onClick={handleTabChange}
            >
              Training
            </button>
          </div>
        </div>
        <div className="filter-group">
          <div className="position-relative">
            <select
              className="drop-down select-custom select-year pl-2 pr-5 py-1 rounded d-md-flex"
              onChange={this.handleYearChange}
              defaultValue={this.props.year}
            >
              {allYears.map((year, index) => (
                <option key={`${year}${index}`} value={year}>
                  {year}
                </option>
              ))}
            </select>
            <ArrowSelect src={ArrowDown} alt="arrow down" />
          </div>
          <div className="position-relative">
            <select
              className="drop-down select-custom select-year pl-2 pr-5 py-1 rounded d-md-flex"
              onChange={this.handleTeamChange}
              defaultValue=""
            >
              <option key={0} value={''}>
                {'All Employees'}
              </option>
              {allTeams.map(team => (
                <option key={team.id} value={team.id}>
                  {team.name}
                </option>
              ))}
            </select>
            <ArrowSelect src={ArrowDown} alt="arrow down" />
          </div>
        </div>
      </div>
    );
  }
}

export default AllowanceFilters;
