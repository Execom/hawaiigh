import React, { Component, Fragment } from 'react';
import _includes from 'lodash/includes';
import {
  SeparatorRow,
  TableCell,
  TableRow
} from '@/components/common/TableStyledComponents';
import Execom from '@/img/execom.jpg';
import { UserImage } from '@/components/common/userImage';
import { getImageUrl } from '@/store/getImageUrl';
import CalendarViewIcon from '@/img/icons/calendar_view.svg';
import { Icon } from '@/pages/administration/components/years/styled';
import {
  makeAllowanceInDays,
  showOnlyOneDecimal
} from '@/pages/administration/helpers';

class AllowanceInfo extends Component {
  render() {
    const { fullName, imageUrl, teamName } = this.props.user;
    const image = getImageUrl(imageUrl, 20);
    const { user, allowance, activeTab } = this.props;
    const allowanceInDays = makeAllowanceInDays(allowance);
    const {
      carriedOver,
      totalAnnual,
      totalTraining,
      pendingTraining,
      pendingAnnual,
      takenAnnual,
      takenTraining,
      remainingAnnual,
      remainingTraining,
      sickness
    } = allowanceInDays;

    const allowanceInfo = tab => {
      switch (tab) {
        case 'Leave':
          return (
            <Fragment>
              <TableCell
                data-label="Carried"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(carriedOver)}</span>
                </div>
              </TableCell>
              <TableCell
                data-label="Total"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(totalAnnual)}</span>
                </div>
              </TableCell>
              <TableCell
                data-label="Taken"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(takenAnnual)}</span>
                </div>
              </TableCell>
              <TableCell
                data-label="Pending"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(pendingAnnual)}</span>
                </div>
              </TableCell>
              <TableCell
                data-label="Remaining"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(remainingAnnual)}</span>
                </div>
              </TableCell>
            </Fragment>
          );
        case 'Sickness':
          return (
            <TableCell
              data-label="Taken"
              color="white"
              className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
            >
              <div className="text-right text-lg-left">
                <span>{showOnlyOneDecimal(sickness)}</span>
              </div>
            </TableCell>
          );
        case 'Training':
          return (
            <Fragment>
              <TableCell
                data-label="Total"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(totalTraining)}</span>
                </div>
              </TableCell>
              <TableCell
                data-label="Taken"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(takenTraining)}</span>
                </div>
              </TableCell>
              <TableCell
                data-label="Pending"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(pendingTraining)}</span>
                </div>
              </TableCell>
              <TableCell
                data-label="Remaining"
                color="white"
                className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
              >
                <div className="text-right text-lg-left">
                  <span>{showOnlyOneDecimal(remainingTraining)}</span>
                </div>
              </TableCell>
            </Fragment>
          );
        default:
          return (
            <TableCell
              width="100%"
              color="white"
              className="py-3 text-center w-100 d-block d-lg-table-cell"
            >
              No results
            </TableCell>
          );
      }
    };

    return (
      <Fragment>
        <TableRow className="d-block d-lg-table-row">
          <TableCell
            width="70"
            data-label="Image"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell py-1 allowance-cell"
          >
            <div className="text-right text-lg-left">
              <UserImage
                borderBlack
                image={
                  _includes(getImageUrl(image, 100), 'null') ? Execom : image
                }
                isSmall
                size="30px"
              />
            </div>
          </TableCell>
          <TableCell
            data-label="Employee"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>{fullName}</span>
            </div>
          </TableCell>
          <TableCell
            data-label="Team"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>{teamName}</span>
            </div>
          </TableCell>
          {allowanceInfo(activeTab)}
          <TableCell
            width="70"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell "
            data-label="Calendar"
            color="white"
          >
            <div className="text-right">
              <Icon
                onClick={e => {
                  e.stopPropagation();
                  this.props.openAllowanceCalendarModal(user);
                }}
                className="mr-2"
                src={CalendarViewIcon}
              />
            </div>
          </TableCell>
        </TableRow>
        <SeparatorRow />
      </Fragment>
    );
  }
}

export default AllowanceInfo;
