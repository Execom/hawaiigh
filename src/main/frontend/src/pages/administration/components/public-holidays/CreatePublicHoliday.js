import { Formik } from 'formik';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closeDrawer } from '@/store/actions/drawerActions';
import { createPublicHoliday } from '@/store/actions/publicHolidayActions';
import { getIsSubmitting } from '@/store/selectors';
import DrawerCalendarInput, {
  DrawerContent,
  DrawerFooter,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import { validationSchema } from '@/pages/administration/components/public-holidays/helpers';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';

class CreatePublicHoliday extends Component {
  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    return (
      <Formik
        validationSchema={validationSchema}
        initialValues={{
          name: '',
          date: null
        }}
        onSubmit={(values, { setSubmitting }) => {
          this.props.createPublicHoliday({
            ...values,
            date: moment(values.date).format('YYYY-MM-DD')
          });
          setSubmitting(false);
        }}
        enableReinitialize
      >
        {({
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                caption="Public holiday name"
                name="name"
                handleChange={handleChange}
                className={errors.name && touched.name ? 'border-danger' : ''}
              />
              <InputPair
                view="other"
                caption="Public holiday date"
                element={
                  <DatePicker
                    customInput={<DrawerCalendarInput />}
                    className={
                      errors.date && touched.date ? 'border-danger' : ''
                    }
                    showYearDropdown
                    dateFormat="dd MMM yyyy"
                    locale="en-GB"
                    selected={values.date}
                    onChange={e => {
                      setFieldValue('date', e);
                    }}
                  />
                }
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting}
              formClick={handleSubmit}
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      </Formik>
    );
  }
}

const mapStateToProps = state => ({
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { createPublicHoliday, closeDrawer, toastrError },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatePublicHoliday);
