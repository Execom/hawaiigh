import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withResetOnNavigate from '../../../../components/HOC/withResetOnNavigate';
import { openActionDrawer } from '../../../../store/actions/drawerActions';
import { requestPublicHolidayYears } from '../../../../store/actions/publicHolidayActions';
import { getPublicHolidayYears } from '../../../../store/selectors';
import AdministrationTable from '../common/AdministrationTable';
import HolidaysYear from './HolidaysYear';

class PublicHolidays extends Component {
  render() {
    const { years, last, page } = this.props.publicHolidays;
    const publicHolidaysYears = Object.keys(years).length
      ? Object.keys(years)
          .reverse()
          .map(year => {
            return <HolidaysYear key={year} year={year} />;
          })
      : null;

    return (
      <AdministrationTable
        width={['25%']}
        headerItems={['Year']}
        body={publicHolidaysYears}
        page={page}
        clickAction={() =>
          this.props.openActionDrawer({
            type: 'createPublicHoliday',
            heading: 'Create Public Holiday'
          })
        }
        requestAction={requestPublicHolidayYears}
        last={last}
        placeholderCellCount={1}
        initialLoad
      />
    );
  }
}

const mapStateToProps = state => ({
  publicHolidays: getPublicHolidayYears(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestPublicHolidayYears, openActionDrawer }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(PublicHolidays));
