import React, { Component, Fragment } from 'react';
import {
  TableCell,
  TableRow,
  SeparatorRow,
  TableDataYear
} from '../../../../components/common/TableStyledComponents';
import ArrowIcon from '../../../../../src/img/icons/arrow-down.svg';
import { Icon } from '../../../requests/components/styled';
import PublicHolidaysItem from './PublicHolidaysItem';
import { getPublicHolidayYears } from '../../../../store/selectors';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withResetOnNavigate from '../../../../components/HOC/withResetOnNavigate';
import {
  requestPublicHolidaysByYear,
  expandAccordion
} from '../../../../store/actions/publicHolidayActions';
import { Collapse } from 'reactstrap';

class HolidaysYear extends Component {
  state = {
    isOpen: false
  };

  fetchPublicHolidaysForYear = () => {
    const {
      publicHolidays,
      year,
      expandAccordion,
      requestPublicHolidaysByYear
    } = this.props;

    if (!publicHolidays.years[year].length) {
      requestPublicHolidaysByYear({
        year: year
      });
    } else {
      expandAccordion({
        year: year,
        expanded: !publicHolidays.accordionExpanded[year]
      });
    }
  };

  render() {
    const {
      publicHolidays: { years, accordionExpanded },
      year
    } = this.props;

    const publicHolidaysItems = years[year].length
      ? years[year].map(item => (
          <PublicHolidaysItem key={item.id} publicHoliday={item} year={year} />
        ))
      : null;

    return (
      <Fragment>
        <TableRow clickable onClick={this.fetchPublicHolidaysForYear}>
          <TableCell
            width="100%"
            color="white"
            className="bg-white d-flex justify-content-between pl-lg-4"
            hideCellTitle
          >
            <span>{year}</span>
            <Icon
              open={accordionExpanded[year]}
              size="15px"
              src={ArrowIcon}
              alt="Arrow for toggling accordion item"
            />
          </TableCell>
        </TableRow>
        <tr>
          <TableDataYear>
            <Collapse isOpen={accordionExpanded[year]}>
              {publicHolidaysItems}
            </Collapse>
          </TableDataYear>
        </tr>
        <SeparatorRow height="5px" />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  publicHolidays: getPublicHolidayYears(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { requestPublicHolidaysByYear, expandAccordion },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(HolidaysYear));
