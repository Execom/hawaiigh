import styled from 'styled-components';

export default styled.div`
  padding: 5px 0;
  background: white;
  margin: 2px 25px;
  border: 1px solid #d3d3d3;
  cursor: pointer;
`;
