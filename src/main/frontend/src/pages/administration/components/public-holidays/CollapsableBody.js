import styled from 'styled-components';

export const CollapsableBody = styled.div`
  max-height: ${props => (props.isOpen ? '500px' : '0px')};
  transition: max-height 0.1s ease;
  overflow: hidden;
`;
