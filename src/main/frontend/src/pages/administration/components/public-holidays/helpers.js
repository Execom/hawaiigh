import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
  name: Yup.string().required('Name is a required field.'),
  date: Yup.string()
    .nullable()
    .required('Date is a required field.')
});
