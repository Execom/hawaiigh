import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeleteIcon from '../../../../img/icons/delete.svg';
import EditIcon from '../../../../img/icons/edit.svg';
import { openActionDrawer } from '../../../../store/actions/drawerActions';
import { openConfirmationModal } from '../../../../store/actions/modalActions';
import { removePublicHoliday } from '../../../../store/actions/publicHolidayActions';
import { formatDate } from '../../../../store/helperFunctions';
import { Icon } from '../years/styled';
import HolidayItem from './HolidayItem';

class PublicHolidayItem extends Component {
  handleDelete = (id, year) => {
    this.props.openConfirmationModal({
      message:
        'This action will delete the selected public holiday. Are you sure you want to continue?',
      values: id,
      year,
      clickAction: removePublicHoliday
    });
  };

  render() {
    const {
      publicHoliday: { id, name, date },
      year
    } = this.props;

    return (
      <Fragment>
        <HolidayItem
          onClick={() =>
            this.props.openActionDrawer({
              type: 'editPublicHoliday',
              heading: 'Edit Public Holiday',
              id
            })
          }
          className="justify-content-between row align-items-center"
        >
          <div data-label="Name" color="white" className="col">
            <div className="">
              <span>{name}</span>
            </div>
          </div>
          <div className="col" data-label="Date" color="white">
            <div className="">
              <span>{formatDate(date)}</span>
            </div>
          </div>
          <div className="col" data-label="Actions" color="white">
            <div className="text-right">
              <Icon
                onClick={() =>
                  this.props.openActionDrawer({
                    type: 'editPublicHoliday',
                    heading: 'Edit Public Holiday',
                    id
                  })
                }
                className="mr-2"
                src={EditIcon}
                alt="Edit icon"
              />
              <Icon
                onClick={e => {
                  e.stopPropagation();
                  this.handleDelete({ id, year });
                }}
                src={DeleteIcon}
                alt="Delete icon"
              />
            </div>
          </div>
        </HolidayItem>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { removePublicHoliday, openActionDrawer, openConfirmationModal },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(PublicHolidayItem);
