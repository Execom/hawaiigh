import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { closeDrawer } from '@/store/actions/drawerActions';
import { openConfirmationModal } from '@/store/actions/modalActions';
import { createYear } from '@/store/actions/yearActions';
import { getIsSubmitting } from '@/store/selectors';
import {
  DrawerContent,
  DrawerFooter,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import { validationSchema } from '@/pages/administration/components/years/helpers';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';

class CreateYear extends Component {
  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  shouldModalOpen = (errors, values) => {
    const message =
      'Year is set to active, if you confirm it will be irreversible, are you sure that you want to continue?';

    if (errors.year) {
      this.showErrorToast(errors.year);
    } else {
      this.props.openConfirmationModal({
        message,
        values,
        clickAction: createYear
      });
    }
  };

  render() {
    return (
      <Formik
        validationSchema={validationSchema}
        initialValues={{
          year: new Date().getFullYear() + 1,
          active: false,
          allowances: []
        }}
        onSubmit={this.props.createYear}
        render={({
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue,
          validateForm,
          setFieldTouched,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                caption="Year (example: 2021)"
                defaultValue={new Date().getFullYear() + 1}
                name="year"
                handleChange={handleChange}
                type="number"
                className={errors.year && touched.year ? 'border-danger' : ''}
              />
              <InputPair
                view="switch"
                caption="Active"
                name="active"
                handleChange={() => setFieldValue('active', !values.active)}
                checked={values.active}
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting}
              formClick={() => {
                values.active
                  ? validateForm()
                      .then(setFieldTouched('year', true))
                      .then(this.shouldModalOpen(errors, values))
                  : handleSubmit();
              }}
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { createYear, openConfirmationModal, closeDrawer, toastrError },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateYear);
