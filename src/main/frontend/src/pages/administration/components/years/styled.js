import styled from 'styled-components';

export const Icon = styled.img`
  cursor: pointer;
  width: 20px;
  filter: grayscale(1);
  transition: 0.2s ease-in-out;

  &:hover {
    filter: grayscale(0);
  }
`;
