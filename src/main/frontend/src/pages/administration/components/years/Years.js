import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withResetOnNavigate from '../../../../components/HOC/withResetOnNavigate';
import { openActionDrawer } from '../../../../store/actions/drawerActions';
import { requestYears } from '../../../../store/actions/yearActions';
import { getYears } from '../../../../store/selectors';
import AdministrationTable from '../common/AdministrationTable';
import YearItem from './YearItem';

class Teams extends Component {
  componentDidMount() {
    this.props.requestYears({
      page: 0
    });
  }

  render() {
    const { last, results, page } = this.props.years;
    const sortedResults = [...results].sort((a, b) => b.year - a.year);
    const yearItems = sortedResults.map(item => {
      return <YearItem key={item.id} year={item} />;
    });

    if (!yearItems.length) return null;

    return (
      <AdministrationTable
        width={['25%']}
        headerItems={['Year', 'Status']}
        body={yearItems}
        requestAction={requestYears}
        last={last}
        page={page}
        placeholderCellCount={2}
        clickAction={() =>
          this.props.openActionDrawer({
            type: 'createYear',
            heading: 'Create Year'
          })
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  years: getYears(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestYears, openActionDrawer }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(Teams));
