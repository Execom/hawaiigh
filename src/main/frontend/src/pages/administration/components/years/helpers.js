import * as Yup from 'yup';
import moment from 'moment';

export const validationSchema = Yup.object().shape({
  year: Yup.number('Year must be a number')
    .min(2000, "Entered year can't be before year 2000.")
    .max(2099, "Entered year can't be after year 2099.")
    .required('Year is a required field.')
});

export const formatDate = date => moment(date).format('YYYY-MM-DD');

export const formatDateOrReturnNull = date =>
  date ? moment(date).format('YYYY-MM-DD') : null;

export const parseDate = date => moment(date).toDate();

export const parseDateOrReturnNull = date =>
  date ? moment(date).toDate() : null;
