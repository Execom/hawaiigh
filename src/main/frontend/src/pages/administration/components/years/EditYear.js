import { Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import { bindActionCreators } from 'redux';
import { closeDrawer } from '@/store/actions/drawerActions';
import { openConfirmationModal } from '@/store/actions/modalActions';
import {
  requestYear,
  resetYear,
  updateYear
} from '@/store/actions/yearActions';
import { getIsSubmitting, getYear } from '@/store/selectors';
import DrawerCalendarInput, {
  DrawerContent,
  DrawerFooter,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import {
  validationSchema,
  formatDate
} from '@/pages/administration/components/years/helpers';
import {
  getCarriedOverHoursExpirationDateMinDate,
  isDateBeforeDate
} from '@/pages/administration/dateUtils';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';
import { initializeExpirationDateField } from '@/store/helperFunctions';

class EditYear extends Component {
  componentWillMount() {
    this.props.resetYear();
  }

  componentDidMount() {
    this.props.requestYear(this.props.id);
  }

  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  shouldModalOpen = (errors, values) => {
    const message =
      'Year is set to active, if you confirm it will be irreversible, are you sure that you want to continue?';

    if (errors.year) {
      this.showErrorToast(errors.year);
    } else {
      this.props.openConfirmationModal({
        message,
        values,
        clickAction: updateYear
      });
    }
  };

  handleSubmitInit = (
    values,
    errors,
    isYearAlreadyActive,
    setFieldTouched,
    validateForm,
    handleSubmit
  ) => {
    if (values.active && !isYearAlreadyActive) {
      validateForm()
        .then(setFieldTouched('year', true))
        .then(
          this.shouldModalOpen(errors, {
            ...values,
            carriedOverHoursExpirationDate: formatDate(
              values.carriedOverHoursExpirationDate
            )
          })
        );
    } else {
      handleSubmit();
    }
  };

  handleSubmit = (values, { setSubmitting }) => {
    this.props.updateYear({
      ...values,
      carriedOverHoursExpirationDate: formatDate(
        values.carriedOverHoursExpirationDate
      )
    });
    setSubmitting(false);
  };

  shouldDisableExpiredDaysInputs = values => {
    const {
      carriedOverHoursExpirable,
      carriedOverHoursExpirationDate
    } = values;

    if (carriedOverHoursExpirable) {
      return isDateBeforeDate(
        new Date(carriedOverHoursExpirationDate),
        new Date()
      );
    }
  };

  handleShouldDaysExpireChange = (values, setFieldValue) => {
    if (
      isDateBeforeDate(
        new Date(),
        new Date(values.carriedOverHoursExpirationDate)
      )
    ) {
      setFieldValue(
        'carriedOverHoursExpirable',
        !values.carriedOverHoursExpirable
      );
    } else {
      !values.carriedOverHoursExpirable &&
        this.showErrorToast(
          'Expiration date for carried over days must be a future date'
        );
    }
  };

  render() {
    if (!this.props.year) return null;

    const {
      carriedOverHoursExpirationDate,
      year,
      active: isYearAlreadyActive
    } = this.props.year;

    const initialValues = {
      ...this.props.year,
      carriedOverHoursExpirationDate: initializeExpirationDateField(
        carriedOverHoursExpirationDate,
        year
      )
    };

    return (
      <Formik
        validationSchema={validationSchema}
        initialValues={initialValues}
        onSubmit={this.handleSubmit}
        render={({
          initialValues,
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue,
          setFieldTouched,
          validateForm,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                disabled={isYearAlreadyActive}
                caption="Year (example: 2021)"
                defaultValue={values.year}
                name="year"
                type="number"
                handleChange={handleChange}
                className={errors.year && touched.year ? 'border-danger' : ''}
              />
              <InputPair
                disabled={isYearAlreadyActive}
                view="switch"
                caption="Active"
                name="active"
                handleChange={() => setFieldValue('active', !values.active)}
                checked={values.active}
              />
              <InputPair
                disabled={this.shouldDisableExpiredDaysInputs(values)}
                view="switch"
                caption="Expired days"
                name="expired days"
                handleChange={() =>
                  this.handleShouldDaysExpireChange(values, setFieldValue)
                }
                checked={values.carriedOverHoursExpirable}
              />
              <InputPair
                disabled={this.shouldDisableExpiredDaysInputs(values)}
                view="other"
                caption="Expiration date for carried over days"
                element={
                  <DatePicker
                    disabled={this.shouldDisableExpiredDaysInputs(values)}
                    customInput={<DrawerCalendarInput />}
                    className={
                      errors.carriedOverHoursExpirationDate &&
                      touched.carriedOverHoursExpirationDate
                        ? 'border-danger'
                        : ''
                    }
                    showYearDropdown
                    dateFormat="dd MMM yyyy"
                    locale="en-GB"
                    selected={values.carriedOverHoursExpirationDate}
                    minDate={getCarriedOverHoursExpirationDateMinDate(
                      values.year
                    )}
                    onChange={e => {
                      setFieldValue('carriedOverHoursExpirationDate', e);
                    }}
                  />
                }
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting || values === initialValues}
              formClick={() =>
                this.handleSubmitInit(
                  values,
                  errors,
                  isYearAlreadyActive,
                  setFieldTouched,
                  validateForm,
                  handleSubmit
                )
              }
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  year: getYear(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateYear,
      requestYear,
      openConfirmationModal,
      closeDrawer,
      resetYear,
      toastrError
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditYear);
