import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import withResetOnNavigate from '../../../../components/HOC/withResetOnNavigate';
import { removeYear, requestYear } from '../../../../store/actions/yearActions';
import { getYear } from '../../../../store/selectors';

class Year extends Component {
  componentDidMount() {
    this.props.requestYear(this.props.match.params.id);
  }

  render() {
    if (!this.props.year) return null;

    const {
      year: { id, year, active }
    } = this.props;

    return (
      <div className="d-flex flex-grow-1 p-4 flex-column align-items-center">
        <h1>Year</h1>
        <h5 className="text-danger mb-3">{year}</h5>
        <h1>Active</h1>
        <h5 className="text-danger mb-3">{active.toString()}</h5>
        {!active && (
          <div>
            <button className="mr-3 btn">
              <NavLink to={`/years/${id}/edit`}>Edit</NavLink>
            </button>
            <button
              className="btn btn-danger"
              onClick={() =>
                this.props.removeYear({
                  id: id
                })
              }
            >
              Delete
            </button>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  year: getYear(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestYear, removeYear }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(Year));
