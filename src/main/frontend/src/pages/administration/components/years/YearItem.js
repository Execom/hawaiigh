import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  SeparatorRow,
  TableCell,
  TableRow
} from '../../../../components/common/TableStyledComponents';
import DeleteIcon from '../../../../img/icons/delete.svg';
import EditIcon from '../../../../img/icons/edit.svg';
import { openActionDrawer } from '../../../../store/actions/drawerActions';
import { openConfirmationModal } from '../../../../store/actions/modalActions';
import { removeYear } from '../../../../store/actions/yearActions';
import { Icon } from './styled';

class YearItem extends Component {
  handleYearDelete = e => {
    e.stopPropagation();
    this.props.openConfirmationModal({
      message:
        'This action will delete the selected year. Are you sure you want to continue?',
      values: { id: this.props.year.id },
      clickAction: removeYear
    });
  };

  handleOpenActionDrawer = () =>
    this.props.openActionDrawer({
      type: 'editYear',
      heading: 'Edit Year',
      id: this.props.year.id
    });

  render() {
    const {
      year: { year, active }
    } = this.props;

    return (
      <Fragment>
        <TableRow
          className="d-block d-lg-table-row"
          clickable
          onClick={this.handleOpenActionDrawer}
        >
          <TableCell
            width="33.33333333%"
            data-label="Year"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>{year}</span>
            </div>
          </TableCell>
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell pl-lg-4"
            data-label="Status"
            color="white"
          >
            <div className="text-right text-lg-left">
              <span>{active ? 'Active' : 'Inactive'}</span>
            </div>
          </TableCell>
          <TableCell
            className={`justify-content-between py-lg-0 py-2 ${
              active ? 'd-none d-lg-table-cell' : 'd-flex d-lg-table-cell'
            }`}
            data-label="Actions"
            color="white"
          >
            <div className="text-right">
              <Icon
                onClick={this.handleOpenActionDrawer}
                className="mr-2"
                src={EditIcon}
                alt="Edit icon"
              />
              {!active && (
                <Icon
                  onClick={this.handleYearDelete}
                  src={DeleteIcon}
                  alt="Delete icon"
                />
              )}
            </div>
          </TableCell>
        </TableRow>
        <SeparatorRow />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { removeYear, openConfirmationModal, openActionDrawer },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(YearItem);
