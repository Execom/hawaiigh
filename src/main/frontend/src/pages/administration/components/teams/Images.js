import _includes from 'lodash/includes';
import React, { Component } from 'react';
import { UserImage } from '../../../../components/common/userImage';
import Execom from '../../../../img/execom.jpg';
import { getImageUrl } from '../../../../store/getImageUrl';
import { NumberSpan } from './styled';

class Images extends Component {
  generateImageItems = items =>
    Array.from({ length: 5 }, (_, i) => {
      const image = getImageUrl(items[i].imageUrl, 100);

      return (
        <UserImage
          title={items[i].fullName}
          borderBlack
          className="position-relative"
          key={items[i].id}
          isSmall
          image={_includes(image, 'null') ? Execom : image}
          size="30px"
          position={i}
          isInTeamRow
        />
      );
    });

  render() {
    const { items } = this.props;

    return (
      <div className="position-relative d-inline-flex">
        {items.length > 5
          ? this.generateImageItems(items)
          : items.map((item, i) => {
              const image = getImageUrl(item.imageUrl, 100);

              return (
                <UserImage
                  title={item.fullName}
                  borderBlack
                  className="position-relative"
                  key={item.id}
                  isSmall
                  image={_includes(image, 'null') ? Execom : image}
                  size="30px"
                  position={i}
                  isInTeamRow
                />
              );
            })}
        {items.length > 5 && (
          <UserImage
            className="position-relative"
            borderBlack
            key="last"
            image={0}
            isSmall
            size="30px"
            position={-1}
            isInTeamRow
          >
            <NumberSpan>+{items.length - 5}</NumberSpan>
          </UserImage>
        )}
      </div>
    );
  }
}

export default Images;
