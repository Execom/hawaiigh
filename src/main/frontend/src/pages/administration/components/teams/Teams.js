import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withResetOnNavigate from '../../../../components/HOC/withResetOnNavigate';
import { openActionDrawer } from '../../../../store/actions/drawerActions';
import { requestTeams } from '../../../../store/actions/teamActions';
import { getTeams } from '../../../../store/selectors';
import AdministrationTable from '../common/AdministrationTable';
import TeamItem from './TeamItem';

class Teams extends Component {
  componentDidMount() {
    this.props.requestTeams({
      page: 0,
      size: 30
    });
  }

  render() {
    const { results, last, page } = this.props.teams;

    const teamItems = results.length
      ? results
          .filter(item => !item.deleted)
          .map(item => {
            return <TeamItem key={item.id} team={item} />;
          })
      : null;

    return (
      <AdministrationTable
        headerItems={['Name', 'Approvers', 'Members']}
        body={teamItems}
        requestAction={requestTeams}
        page={page}
        placeholderCellCount={3}
        clickAction={() =>
          this.props.openActionDrawer({
            type: 'createTeam',
            heading: 'Create Team'
          })
        }
        last={last}
      />
    );
  }
}

const mapStateToProps = state => ({
  teams: getTeams(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestTeams, openActionDrawer }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(Teams));
