import { FieldArray, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EmployeeTeamSearchResults from '@/components/search-dropdown/search-results/EmployeeTeamSearchResults';
import SearchDropdown from '@/components/search-dropdown/SearchDropdown';
import { closeDrawer } from '@/store/actions/drawerActions';
import { createTeam } from '@/store/actions/teamActions';
import {
  clearEmployees,
  requestEmployees,
  searchEmployees
} from '@/store/actions/userActions';
import { getIsSubmitting, getSearchEmployees } from '@/store/selectors';
import {
  DrawerContent,
  DrawerFooter,
  WhiteBackgroundDiv,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import { teamValidationSchema } from '@/pages/administration/components/teams/helpers';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { toastrError } from '@/store/sagas/helpers/toastrHelperSaga';

class CreateTeam extends Component {
  closeDrawer = () => {
    this.props.closeDrawer();
  };

  showErrorToast = error => this.props.toastrError(error);

  render() {
    return (
      <Formik
        validationSchema={teamValidationSchema}
        initialValues={{
          name: '',
          teamApprovers: [],
          users: [],
          sendEmailToTeammatesForBonusRequestEnabled: false,
          sendEmailToTeammatesForSicknessRequestEnabled: false,
          sendEmailToTeammatesForAnnualRequestEnabled: false
        }}
        onSubmit={this.props.createTeam}
        render={({
          handleSubmit,
          handleChange,
          values,
          errors,
          touched,
          setFieldValue,
          isSubmitting
        }) => (
          <Fragment>
            <DrawerContent className="p-4">
              <InputPair
                caption="Team name"
                name="name"
                handleChange={handleChange}
                className={errors.name && touched.name ? 'border-danger' : ''}
                separator
              />
              <InputPair
                view="chips"
                caption="Sickness request emails"
                name="sicknessRequestEmails"
                handleChange={val =>
                  setFieldValue('sicknessRequestEmails', val)
                }
                className={
                  errors.sicknessRequestEmails && touched.sicknessRequestEmails
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                view="chips"
                caption="Annual request emails"
                name="annualRequestEmails"
                handleChange={val => setFieldValue('annualRequestEmails', val)}
                className={
                  errors.annualRequestEmails && touched.annualRequestEmails
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                view="chips"
                caption="Bonus request emails"
                name="bonusRequestEmails"
                handleChange={val => setFieldValue('bonusRequestEmails', val)}
                separator
                className={
                  errors.bonusRequestEmails && touched.bonusRequestEmails
                    ? 'border-danger'
                    : ''
                }
              />
              <InputPair
                view="switch"
                caption="Send email to teammates for annual request"
                name="sendEmailToTeammatesForAnnualRequestEnabled"
                handleChange={() =>
                  setFieldValue(
                    'sendEmailToTeammatesForAnnualRequestEnabled',
                    !values.sendEmailToTeammatesForAnnualRequestEnabled
                  )
                }
                checked={values.sendEmailToTeammatesForAnnualRequestEnabled}
              />
              <InputPair
                view="switch"
                caption="Send email to teammates for bonus request"
                name="sendEmailToTeammatesForBonusRequestEnabled"
                handleChange={() =>
                  setFieldValue(
                    'sendEmailToTeammatesForBonusRequestEnabled',
                    !values.sendEmailToTeammatesForBonusRequestEnabled
                  )
                }
                checked={values.sendEmailToTeammatesForBonusRequestEnabled}
              />
              <InputPair
                view="switch"
                caption="Send email to teammates for sickness request"
                name="sendEmailToTeammatesForSicknessRequestEnabled"
                handleChange={() =>
                  setFieldValue(
                    'sendEmailToTeammatesForSicknessRequestEnabled',
                    !values.sendEmailToTeammatesForSicknessRequestEnabled
                  )
                }
                checked={values.sendEmailToTeammatesForSicknessRequestEnabled}
                separator
              />
              <InputPair
                view="other"
                caption="Search for employees"
                element={
                  <SearchDropdown
                    searchResults={this.props.employees}
                    searchAction={searchEmployees}
                    includeLoggedUser
                    resetAction={clearEmployees}
                  >
                    {(inputReference, resetInput) => (
                      <EmployeeTeamSearchResults
                        resetInput={resetInput}
                        inputReference={inputReference}
                        employees={this.props.employees}
                        selectedApprovers={values.teamApprovers}
                        selectedMembers={values.users}
                      />
                    )}
                  </SearchDropdown>
                }
              />
              <InputPair
                view="other"
                caption="Team members"
                element={
                  <WhiteBackgroundDiv className="p-2 border rounded">
                    {!values.users.length ? (
                      <div>
                        <span>No members selected</span>
                      </div>
                    ) : (
                      values.users.map((user, index) => {
                        return (
                          <h5
                            key={user.id}
                            className="d-flex justify-content-between mb-2"
                          >
                            {user.fullName}
                            <FieldArray
                              name="users"
                              render={arrayHelpers => (
                                <span
                                  className="text-danger ml-2"
                                  onClick={() => {
                                    arrayHelpers.remove(index);
                                  }}
                                >
                                  x
                                </span>
                              )}
                            />
                          </h5>
                        );
                      })
                    )}
                  </WhiteBackgroundDiv>
                }
              />
              <InputPair
                view="other"
                caption="Team approvers"
                element={
                  <WhiteBackgroundDiv className="p-2 border rounded">
                    {!values.teamApprovers.length ? (
                      <div>
                        <span>No approvers selected</span>
                      </div>
                    ) : (
                      values.teamApprovers.map((user, index) => {
                        return (
                          <h5
                            key={user.id}
                            className="d-flex justify-content-between mb-2"
                          >
                            {user.fullName}
                            <FieldArray
                              name="teamApprovers"
                              render={arrayHelpers => (
                                <span
                                  className="text-danger ml-2"
                                  onClick={() => {
                                    arrayHelpers.remove(index);
                                  }}
                                >
                                  x
                                </span>
                              )}
                            />
                          </h5>
                        );
                      })
                    )}
                  </WhiteBackgroundDiv>
                }
              />
            </DrawerContent>
            <DrawerFooter
              isSubmitting={this.props.isSubmitting}
              formClick={handleSubmit}
              closeClick={this.closeDrawer}
            />
            <FormikErrorDetector
              formikProps={{ isSubmitting, errors }}
              onError={this.showErrorToast}
            />
          </Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  employees: getSearchEmployees(state),
  isSubmitting: getIsSubmitting(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { createTeam, requestEmployees, closeDrawer, toastrError },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateTeam);
