import * as Yup from 'yup';
import { validateEmail } from '../../../../helpers';

const emailsStringTest = function(value) {
  if (!value) return true;

  const emails = value.split(',');

  for (let email of emails) {
    if (!validateEmail(email)) return false;
  }

  return true;
};

export const validationSchema = Yup.object().shape({
  name: Yup.string().required()
});

export const teamValidationSchema = Yup.object().shape({
  name: Yup.string().required('Name is a required field.'),
  sicknessRequestEmails: Yup.string()
    .nullable()
    .notRequired()
    .test('emails-string', "Emails aren't valid", emailsStringTest),
  annualRequestEmails: Yup.string()
    .nullable()
    .notRequired()
    .test('emails-string', "Emails aren't valid", emailsStringTest),
  bonusRequestEmails: Yup.string()
    .nullable()
    .notRequired()
    .test('emails-string', "Emails aren't valid", emailsStringTest)
});
