import React from 'react';
import Employees from '@/pages/administration/components/employees/Employees';
import LeaveProfiles from '@/pages/administration/components/leave-profiles/LeaveProfiles';
import LeaveTypes from '@/pages/administration/components/leave-types/LeaveTypes';
import PublicHolidays from '@/pages/administration/components/public-holidays/PublicHolidays';
import Teams from '@/pages/administration/components/teams/Teams';
import Years from '@/pages/administration/components/years/Years';
import Allowances from '@/pages/administration/components/allowances/Allowances';

export const tabList = [
  { label: 'Years', content: <Years /> },
  { label: 'Teams', content: <Teams /> },
  { label: 'Employees', content: <Employees /> },
  { label: 'Public Holidays', content: <PublicHolidays /> },
  { label: 'Leave types', content: <LeaveTypes /> },
  { label: 'Leave profiles', content: <LeaveProfiles /> },
  { label: 'Allowances', content: <Allowances /> }
];

export const calculatePage = (employeesTab, employeePage, page) => {
  if (!employeesTab) {
    return page;
  }

  return employeePage + 1;
};

export const showOnlyOneDecimal = value =>
  value - Math.round(value) !== 0 ? value.toFixed(1) : value;

export const parseTeamApproversStringIntoArray = value =>
  value
    .replace(/@execom.eu/g, '')
    .replace(/\[/g, '')
    .replace(/\]/g, '')
    .split(', ')
    .reverse();

export const wrapEverySecondElementToParentheses = values => {
  return values.map(value =>
    values.indexOf(value) % 2 === 0
      ? value
      : values.length > 2
      ? values.indexOf(value) === values.length - 1
        ? ` (${value}) `
        : ` (${value}), `
      : ` (${value}) `
  );
};

export const makeAllowanceInDays = allowance => {
  const typeOfAllowance = [
    'annual',
    'training',
    'approvedBonus',
    'bonusManualAdjust',
    'carriedOver',
    'manualAdjust',
    'trainingManualAdjust',
    'pendingTraining',
    'pendingAnnual',
    'takenAnnual',
    'takenTraining',
    'sickness',
    'remainingAnnual',
    'remainingTraining',
    'totalAnnual',
    'totalTraining'
  ];
  const allowanceInDays = { ...allowance };

  typeOfAllowance.forEach(item => (allowanceInDays[item] /= 8));

  return allowanceInDays;
};
