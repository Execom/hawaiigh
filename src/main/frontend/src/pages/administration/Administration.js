import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PageHeading from '../../components/common/PageHeading';
import { Tabs } from '../../components/tabs/Tabs';
import { setActiveTab } from '../../store/actions/tabActions';
import { getActiveTab } from '../../store/selectors';
import { tabList } from './helpers';

class Administration extends Component {
  handleTabClick = index => {
    this.props.setActiveTab(index);
  };

  render() {
    const { activeTabIndex } = this.props;
    const selectedTab = tabList[activeTabIndex];

    return (
      <div className="d-flex flex-grow-1 flex-column">
        <PageHeading title="Administration" mobileHidden />
        <Tabs
          handleTabClick={this.handleTabClick}
          data={tabList}
          activeTabIndex={activeTabIndex}
        />
        <div className="flex-1 flex-grow-1 d-flex">{selectedTab.content}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  activeTabIndex: getActiveTab(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setActiveTab }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Administration);
