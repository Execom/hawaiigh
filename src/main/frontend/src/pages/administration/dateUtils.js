export const getMostRecentDateFromDatesArr = datesArr =>
  new Date(Math.max(...datesArr));

export const getFirstDayOfSubsequentYearDate = year => new Date(year + 1, 0, 1);

export const getTomorrowDate = () =>
  new Date().setDate(new Date().getDate() + 1);

export const isDateBeforeDate = (date1, date2) => date1 < date2;

export const getCarriedOverHoursExpirationDateMinDate = year => {
  const firstDayOfSubsequentYearDate = getFirstDayOfSubsequentYearDate(year);
  const tomorrowDate = getTomorrowDate();
  const datesArr = [firstDayOfSubsequentYearDate, tomorrowDate];
  return getMostRecentDateFromDatesArr(datesArr);
};
