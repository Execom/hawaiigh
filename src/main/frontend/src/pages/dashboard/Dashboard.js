import { push } from 'connected-react-router';
import _get from 'lodash/get';
import _startsWith from 'lodash/startsWith';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import FloatingActionButton from '../../components/common/FloatingActionButton/FloatingActionButton';
import PageHeading from '../../components/common/PageHeading';
import withResetOnNavigate from '../../components/HOC/withResetOnNavigate';
import { requestLeaveTypes } from '../../store/actions/absenceActions';
import { requestYearsWithAllowance } from '../../store/actions/allowanceActions';
import { openRequestModal } from '../../store/actions/modalActions';
import { requestPublicHolidays } from '../../store/actions/publicHolidayActions';
import {
  removeSearchedEmployee,
  requestUserDays
} from '../../store/actions/userActions';
import {
  getLeaveTypes,
  getPublicHolidays,
  getRequest,
  getSearchedEmployee,
  getUserDays,
  getYearsWithAllowance
} from '../../store/selectors';
import DashboardContainer from './components/DashboardContainer';

const PageTitle = styled.h1`
  font-size: 20px;
  font-weight: 600;
  margin: 24px auto 0;
`;

class Dashboard extends Component {
  componentDidMount() {
    if (
      this.props.location.search &&
      !this.props.searchedEmployee &&
      !_startsWith(this.props.location.search, '?requestId=')
    ) {
      this.props.push(`/${this.props.match.params[0]}`);
    }
    this.props.requestPublicHolidays();
    this.props.requestLeaveTypes();
    this.props.requestYearsWithAllowance();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.search && !this.props.location.search) {
      this.props.removeSearchedEmployee();
    }

    if (this.props.searchedEmployee !== prevProps.searchedEmployee) {
      this.props.requestYearsWithAllowance({
        userId: _get(this.props.searchedEmployee, 'id')
      });
    }
  }

  componentWillUnmount() {
    this.props.removeSearchedEmployee();
  }

  openModal = () => {
    this.props.openRequestModal();
  };

  getHeading = () => {
    const {
      searchedEmployee,
      location: { pathname }
    } = this.props;

    // TODO: Refactor to switch?
    if (pathname.includes('/dashboard')) {
      return searchedEmployee
        ? `${searchedEmployee.fullName.split(' ')[0]}'s dashboard`
        : 'My dashboard';
    } else if (pathname.includes('/requests')) {
      return searchedEmployee
        ? `${searchedEmployee.fullName.split(' ')[0]}'s requests`
        : 'My requests';
    }

    return 'Team calendar';
  };

  render() {
    return (
      <div className="d-flex flex-grow-1 flex-column pt-lg-4 mx-xl-5 px-xl-5">
        {this.props.searchedEmployee && (
          <PageTitle className="d-flex d-lg-none">
            {this.getHeading()}
          </PageTitle>
        )}

        <PageHeading
          title={this.getHeading()}
          buttonTitle="+ New Request"
          buttonAction={this.openModal}
          location={this.props.location.pathname}
          mobileHidden={this.getHeading() === 'Team calendar'}
        />
        <FloatingActionButton />
        {this.props.yearsWithAllowance.length ? (
          <DashboardContainer
            publicHolidays={this.props.publicHolidays}
            yearsWithAllowance={this.props.yearsWithAllowance}
          />
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  publicHolidays: getPublicHolidays(state),
  userDays: getUserDays(state),
  request: getRequest(state),
  leaveTypes: getLeaveTypes(state),
  yearsWithAllowance: getYearsWithAllowance(state),
  searchedEmployee: getSearchedEmployee(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestPublicHolidays,
      requestUserDays,
      requestLeaveTypes,
      requestYearsWithAllowance,
      openRequestModal,
      removeSearchedEmployee,
      push
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(Dashboard));
