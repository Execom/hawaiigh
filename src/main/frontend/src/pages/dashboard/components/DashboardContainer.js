import { ConnectedRouter } from 'connected-react-router';
import _get from 'lodash/get';
import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { selectFromYearArray } from '../../../components/calendar/calendarUtils';
import { requestUserDays } from '../../../store/actions/userActions';
import { getSearchedEmployee, getUserDays } from '../../../store/selectors';
import { history } from '../../../store/store';
import TeamCalendarContainer from '../../team-calendar/TeamCalendarContainer';
import RequestsYearHandler from '../../requests/components/RequestsYearHandler';
import DashboardYearHandler from './DashboardYearHandler';

class DashboardContainer extends Component {
  componentDidMount() {
    this.props.requestUserDays({
      year: selectFromYearArray(
        moment().year(),
        this.props.yearsWithAllowance,
        'year'
      ),
      userId: _get(this.props.searchedEmployee, 'id')
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.yearsWithAllowance !== prevProps.yearsWithAllowance) {
      this.props.requestUserDays({
        year: selectFromYearArray(
          moment().year(),
          this.props.yearsWithAllowance,
          'year'
        ),
        userId: _get(this.props.searchedEmployee, 'id')
      });
    }
  }

  render() {
    if (
      this.props.publicHolidays === null ||
      this.props.userDays === null ||
      this.props.yearsWithAllowance === null
    )
      return null;

    return (
      <ConnectedRouter history={history}>
        <Switch>
          <Route
            path="/dashboard"
            render={() =>
              this.props.userDays && (
                <DashboardYearHandler
                  publicHolidays={this.props.publicHolidays}
                  userDays={this.props.userDays}
                  yearsWithAllowance={this.props.yearsWithAllowance}
                  userRequests={this.props.userRequestsForMonth}
                />
              )
            }
          />
          <Route
            path="/requests"
            render={() =>
              this.props.userDays && (
                <RequestsYearHandler
                  publicHolidays={this.props.publicHolidays}
                  userDays={this.props.userDays}
                  yearsWithAllowance={this.props.yearsWithAllowance}
                  userRequests={this.props.userRequestsForMonth}
                />
              )
            }
          />
          <Route
            path="/team-calendar"
            render={() => (
              <TeamCalendarContainer
                yearsWithAllowance={this.props.yearsWithAllowance}
              />
            )}
          />
        </Switch>
      </ConnectedRouter>
    );
  }
}

const mapStateToProps = state => ({
  userDays: getUserDays(state),
  searchedEmployee: getSearchedEmployee(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestUserDays
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardContainer);
