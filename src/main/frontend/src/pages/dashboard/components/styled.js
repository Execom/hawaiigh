import styled from 'styled-components';

export const DashboardContainerBlock = styled.div`
  border: none;
  box-shadow: 3px 3px 10px #d3d3d3;

  @media (min-width: 767px) {
    min-height: 390px;
  }
`;

export const DashboardChartContainer = styled.div`
  @media (max-width: 767px) {
    height: 330px;
  }
`;

export const Legend = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: ${props => props.sick || props.approved || props.pending};
`;

export const StyledImage = styled.img`
  width: 22px;
`;

export const ArrowSelect = styled.img`
  width: 10px;
  right: 8px;
  top: 8px;
  position: absolute;
  pointer-events: none;

  &.edit-employee-arrow {
    top: 36px;
    right: 14px;
  }

  &.input-no-caption-arrow {
    top: 12px;
    right: 14px;
  }
`;
