import _flatMap from 'lodash/flatMap';
import moment from 'moment';
import React from 'react';
import HolidayImg from '../../../img/holiday2.svg';
import { Legend, StyledImage } from './styled';

export const checkIfType = (value, type) => typeof value === type;

export const legendObject = [
  {
    title: 'Approved',
    element: <Legend approved="#A6D7A8" className="mr-3" />
  },
  { title: 'Sick', element: <Legend sick="#FDA5A7" className="mr-3" /> },
  { title: 'Pending', element: <Legend pending="#FEF2B0" className="mr-3" /> },
  {
    title: 'Public Holidays',
    element: (
      <StyledImage className="mr-3" src={HolidayImg} alt="Public holiday" />
    ),
    hoverAction: true
  }
];

export const allowanceTypes = [
  { id: 'LEAVE', label: 'Leave' },
  { id: 'TRAINING', label: 'Training' },
  { id: 'SICKNESS', label: 'Sickness' }
];

export const createMonthsList = yearsWithAllowance =>
  _flatMap(
    yearsWithAllowance.map(year =>
      moment.months().map(month => `${year}-${month}`)
    )
  );
