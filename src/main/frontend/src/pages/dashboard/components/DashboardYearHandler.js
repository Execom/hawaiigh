import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _get from 'lodash/get';
import moment from 'moment';
import {
  createMonthCalendar,
  createYearCalendar,
  selectFromYearArray
} from '@/components/calendar/calendarUtils';
import YearlyCalendar from '@/components/calendar/YearlyCalendar';
import {
  requestAllowance,
  requestAllowanceForUser
} from '@/store/actions/allowanceActions';
import { requestUserDays } from '@/store/actions/userActions';
import { setSelectedDate } from '@/store/actions/yearActions';
import {
  getAllowance,
  getRouter,
  getSearchedEmployee,
  getSelectedDate,
  getUserDays,
  getWindowWidth
} from '@/store/selectors';
import TeamCalendar from '@/pages/team-calendar/components/TeamCalendar';
import {
  allowanceTypes,
  createMonthsList,
  legendObject
} from '@/pages/dashboard/components/helpers';
import {
  DashboardContainerBlock,
  DashboardChartContainer
} from '@/pages/dashboard/components/styled';
import { DashboardCarousel } from '@/components/common/DashboardCarousel';
import DoughnutChart from '@/components/allowance-chart/DoughnutChart';
import ChartTab from '@/components/allowance-chart/ChartTab';

class DashboardYearHandler extends Component {
  currentYear = moment().year();
  yearsWithAllowance = this.props.yearsWithAllowance;
  selectedMonth = selectFromYearArray(
    this.currentYear,
    this.yearsWithAllowance,
    'month'
  );
  selectedYear = selectFromYearArray(
    this.currentYear,
    this.yearsWithAllowance,
    'year'
  );
  selectValue = (type, year) =>
    selectFromYearArray(
      year || this.currentYear,
      this.yearsWithAllowance,
      type
    );

  state = {
    monthsWithAllowance: createMonthsList(this.yearsWithAllowance),
    selectedYear: this.selectValue('year'),
    selectedMonth: this.selectValue('month'),
    calendar: createYearCalendar(
      this.selectValue('year'),
      this.props.publicHolidays,
      this.props.userDays
    ),
    monthCalendar: createMonthCalendar(
      this.selectValue('month'),
      this.props.publicHolidays,
      this.props.userDays,
      'days'
    ),
    publicHolidaysHover: false,
    activeTab: 'LEAVE'
  };

  componentDidMount() {
    this.props.requestAllowanceForUser({
      year: this.selectValue('year', this.currentYear),
      userId: _get(this.props.searchedEmployee, 'id')
    });

    this.props.setSelectedDate(this.state.selectedYear);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.userDays !== this.props.userDays) {
      this.renderYearCalendar(this.state.selectedYear);
      this.renderMonthCalendar(this.state.selectedMonth);
    }

    if (prevProps.searchedEmployee !== this.props.searchedEmployee) {
      this.props.requestAllowanceForUser({
        year: this.selectValue('year', this.currentYear),
        userId: _get(this.props.searchedEmployee, 'id')
      });
    }

    if (prevProps.yearsWithAllowance !== this.props.yearsWithAllowance) {
      this.setState({
        selectedYear: this.selectValue('year', this.currentYear)
      });
    }
  }

  renderMonthCalendar = selectedMonth => {
    this.setState(prevState => ({
      ...prevState,
      monthCalendar: createMonthCalendar(
        selectedMonth,
        this.props.publicHolidays,
        this.props.userDays,
        'days'
      )
    }));
  };

  renderYearCalendar = selectedYear => {
    this.setState(prevState => ({
      ...prevState,
      selectedYear: selectedYear,
      calendar: createYearCalendar(
        selectedYear,
        this.props.publicHolidays,
        this.props.userDays
      )
    }));
  };

  changeSelectedValue = ({ value }, selectedValue, type) => {
    if (value !== selectedValue) {
      type === 'year'
        ? this.handleYearChange(value)
        : this.handleMonthChange(value);
    }
  };

  mouseMovementHandler = hover => {
    this.setState({
      publicHolidaysHover: hover
    });
  };

  refreshCalendarData = (year, userId) => {
    this.props.requestUserDays({
      year,
      userId
    });
    this.props.requestAllowanceForUser({
      year,
      userId
    });
  };

  handleYearChange = selectedYear => {
    const monthYear = +moment(this.state.selectedMonth, 'YYYY-MMMM').format(
      'YYYY'
    );

    this.refreshCalendarData(
      selectedYear,
      _get(this.props.searchedEmployee, 'id')
    );
    this.props.setSelectedDate(selectedYear);
    this.renderYearCalendar(selectedYear);

    if (selectedYear !== monthYear) {
      this.setState({
        selectedMonth:
          moment().year() !== +selectedYear
            ? `${selectedYear}-January`
            : `${selectedYear}-${moment().format('MMMM')}`
      });
    }
  };

  handleMonthChange = selectedMonth => {
    const selectedYear = +moment(selectedMonth, 'YYYY-MMMM').format('YYYY');

    this.setState({
      selectedMonth
    });

    if (selectedYear !== this.props.selectedDate) {
      this.props.setSelectedDate(selectedYear);

      this.setState({
        selectedYear
      });
      this.refreshCalendarData(
        selectedYear,
        _get(this.props.searchedEmployee, 'id')
      );
      this.renderYearCalendar(selectedYear);
    }

    this.renderMonthCalendar(selectedMonth);
  };

  handleTabClick = tabId => {
    this.setState({ activeTab: tabId });
  };

  render() {
    const { pathname } = this.props.router.location;
    const {
      selectedYear,
      selectedMonth,
      monthsWithAllowance,
      calendar,
      publicHolidaysHover,
      monthCalendar
    } = this.state;
    const {
      yearsWithAllowance,
      selectDay,
      userDays,
      windowWidth,
      allowance
    } = this.props;
    return (
      <div className="px-4 d-flex flex-column position-relative mb-2">
        <div className="flex-column flex-lg-row d-flex position-relative flex-grow-1 mb-2">
          <DashboardContainerBlock className="p-3 rounded mb-3 mb-lg-0 mb-xl-0 mb-sm-3 bg-white col-lg-9 d-flex flex-column">
            <DashboardCarousel
              path={pathname}
              selectedYear={selectedYear}
              yearsWithAllowance={yearsWithAllowance}
              selectedMonth={selectedMonth}
              monthsWithAllowance={monthsWithAllowance}
              handleYearChange={this.handleYearChange}
              handleMonthChange={this.handleMonthChange}
            />
            {calendar && (
              <div className="d-none d-md-flex flex-grow-1">
                <YearlyCalendar
                  calendar={calendar}
                  selectDay={selectDay}
                  publicHolidaysHover={publicHolidaysHover}
                />
              </div>
            )}
            {userDays && (
              <div className="d-md-none">
                <TeamCalendar
                  isDashboard
                  calendar={monthCalendar}
                  userDays={userDays}
                  windowWidth={windowWidth}
                />
              </div>
            )}
            <div className="d-none d-md-flex justify-content-center mt-4 mb-2">
              {legendObject.map(item => {
                return (
                  <div
                    onMouseOver={() =>
                      item.hoverAction && this.mouseMovementHandler(true)
                    }
                    onMouseLeave={() =>
                      item.hoverAction && this.mouseMovementHandler(false)
                    }
                    key={item.title}
                    className="mx-3 d-flex align-items-center"
                  >
                    {item.element}
                    <span>{item.title}</span>
                  </div>
                );
              })}
            </div>
          </DashboardContainerBlock>
          {allowance && (
            <div className="pl-lg-4 pl-xl-4 pl-0 pr-0 mb-0 col-lg-3 flex-column d-flex flex-grow-1">
              <DashboardContainerBlock className="d-flex flex-column p-3 rounded bg-white flex-grow-1">
                <div className="d-flex flex-column flex-grow-1">
                  <div className="d-flex flex-row">
                    {allowanceTypes.map(({ id, label }) => (
                      <ChartTab
                        label={label}
                        isActive={this.state.activeTab === id}
                        handleTabClick={this.handleTabClick}
                        key={id}
                        tabId={id}
                      />
                    ))}
                  </div>
                  <DashboardChartContainer className="d-flex flex-row flex-grow-1">
                    <DoughnutChart
                      allowance={this.props.allowance}
                      activeTab={this.state.activeTab}
                    />
                  </DashboardChartContainer>
                </div>
              </DashboardContainerBlock>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  allowance: getAllowance(state),
  searchedEmployee: getSearchedEmployee(state),
  windowWidth: getWindowWidth(state),
  userDays: getUserDays(state),
  router: getRouter(state),
  selectedDate: getSelectedDate(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestAllowance,
      requestUserDays,
      requestAllowanceForUser,
      setSelectedDate
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardYearHandler);
