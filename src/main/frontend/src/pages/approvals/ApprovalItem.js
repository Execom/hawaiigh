import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TableRow } from '../../components/common/Table';
import {
  SeparatorRow,
  TableCell
} from '../../components/common/TableStyledComponents';
import { handleRequestModal } from '../../store/actions/modalActions';
import { formatDate } from '../../store/helperFunctions';

class ApprovalItem extends Component {
  openModal = request => {
    this.props.handleRequestModal({ request, isApprover: true });
  };

  render() {
    const {
      request,
      request: {
        user,
        absence: { name },
        reason,
        days
      }
    } = this.props;

    return (
      <Fragment>
        <TableRow
          className="d-block d-lg-table-row"
          onClick={() => this.openModal(request)}
        >
          <TableCell
            data-label="Employee"
            color="white"
            className="pl-lg-4 d-flex justify-content-between d-lg-table-cell"
          >
            <div className="text-right text-lg-left">
              <span>{user.fullName}</span>
            </div>
          </TableCell>
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell"
            data-label="Team"
            color="white"
          >
            <div className="text-right text-lg-left">
              <span>{user.teamName}</span>
            </div>
          </TableCell>
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell"
            data-label="Request"
            color="white"
          >
            <div className="text-right text-lg-left">
              <span>{formatDate(days[0].date)} - </span>
              <span>{formatDate(days[days.length - 1].date)}</span>
            </div>
          </TableCell>
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell"
            data-label="Leave type"
            color="white"
          >
            <span className="text-right text-lg-left font-weight-bold">
              {name}
            </span>
          </TableCell>
          <TableCell
            className="d-flex justify-content-between d-lg-table-cell"
            data-label="Reason"
            color="white"
          >
            <span className="text-right text-lg-left">{reason}</span>
          </TableCell>
        </TableRow>
        <SeparatorRow />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ handleRequestModal }, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(ApprovalItem);
