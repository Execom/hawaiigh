import _startsWith from 'lodash/startsWith';
import _trim from 'lodash/trim';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PageHeading from '../../components/common/PageHeading';
import {
  SeparatorRow,
  TableHeaderNative
} from '../../components/common/TableStyledComponents';
import withResetOnNavigate from '../../components/HOC/withResetOnNavigate';
import {
  handleRequestModal,
  openInfoModal
} from '../../store/actions/modalActions';
import { requestRequestsForApproval } from '../../store/actions/requestsActions';
import { getRequestsForApproval } from '../../store/selectors';
import ApprovalItem from './ApprovalItem';

class Approvals extends Component {
  componentDidMount() {
    this.props.requestRequestsForApproval();
  }

  componentDidUpdate(prevProps) {
    if (
      !prevProps.requestsForApproval &&
      prevProps.requestsForApproval !== this.props.requestsForApproval
    ) {
      if (_startsWith(this.props.location.search, '?requestId=')) {
        const requestId = _trim(this.props.location.search, '?requestId=');
        const request = this.props.requestsForApproval.find(
          request => request.id === +requestId
        );

        if (request) {
          this.props.handleRequestModal({ request, isApprover: true });
        } else {
          this.props.openInfoModal("This request doesn't exist!");
        }
      }
    }
  }

  render() {
    if (!this.props.requestsForApproval) return null;

    const items = this.props.requestsForApproval.map(request => (
      <ApprovalItem key={request.id} request={request} />
    ));

    return (
      <div className="d-flex flex-grow-1 flex-column">
        <PageHeading title="Approvals" mobileHidden />
        <div className="d-flex justify-content-center px-4 flex-grow-1">
          {this.props.requestsForApproval.length ? (
            <table className="table table-hover mt-3">
              <thead>
                <tr>
                  <TableHeaderNative
                    color="grey"
                    className="border-right-0 pl-4"
                  >
                    Employee
                  </TableHeaderNative>
                  <TableHeaderNative color="grey">Team</TableHeaderNative>
                  <TableHeaderNative color="grey">Request</TableHeaderNative>
                  <TableHeaderNative color="grey">Leave type</TableHeaderNative>
                  <TableHeaderNative color="grey">Reason</TableHeaderNative>
                </tr>
                <SeparatorRow height="10px" />
              </thead>
              <tbody>{items}</tbody>
            </table>
          ) : (
            <div className="d-flex flex-grow-1 align-items-center justify-content-center">
              <h5 className="text-danger mt-4 mt-lg-2">
                No requests at the moment
              </h5>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  requestsForApproval: getRequestsForApproval(state).requests
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestRequestsForApproval,
      handleRequestModal,
      openInfoModal
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withResetOnNavigate()(Approvals));
