import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { selectFromYearArray } from '@/components/calendar/calendarUtils';
import { requestTeamRequestsForMonth } from '@/store/actions/requestsActions';
import {
  requestUserDaysForTeam,
  resetUserDaysValues
} from '@/store/actions/teamActions';
import { getTeamRequests, getUser } from '@/store/selectors';
import MonthHandlerContainer from '@/pages/team-calendar/components/TeamCalendarMonthHandler';
import { formatSelectedMonth } from '@/store/helperFunctions';

class TeamCalendarContainer extends Component {
  componentDidMount() {
    const selectedYearAndMonth = selectFromYearArray(
      moment().year(),
      this.props.yearsWithAllowance,
      'month'
    );
    this.props.requestTeamRequestsForMonth({
      yearMonth: formatSelectedMonth(selectedYearAndMonth),
      teamId: this.props.teamId
    });
    this.props.requestUserDaysForTeam({
      month: formatSelectedMonth(selectedYearAndMonth),
      teamId: this.props.teamId
    });
  }

  componentWillUnmount() {
    this.props.resetUserDaysValues();
  }

  render() {
    if (!this.props.teamRequests) return null;

    return (
      <MonthHandlerContainer
        yearsWithAllowance={this.props.yearsWithAllowance}
        teamRequests={this.props.teamRequests}
      />
    );
  }
}

const mapStateToProps = state => ({
  teamRequests: getTeamRequests(state),
  teamId: getUser(state).teamId
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestTeamRequestsForMonth,
      requestUserDaysForTeam,
      resetUserDaysValues
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamCalendarContainer);
