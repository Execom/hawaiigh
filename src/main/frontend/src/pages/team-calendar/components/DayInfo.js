import React, { Component } from 'react';
import SpanAction from '../../../components/common/SpanAction';
import { IconImage, RequestStatus, Results, ResultsContainer } from './styled';

class DayInfo extends Component {
  state = {
    expandedDay: false
  };

  componentDidUpdate = () => {
    this.props.expandedMonth &&
      this.state.expandedDay &&
      this.setState({
        expandedDay: false
      });
  };

  toggleResults = () => {
    this.setState(prevState => ({
      expandedDay: !prevState.expandedDay
    }));
  };

  showMoreComponent = (requests, expandedMonth, expandedDay) => {
    if (requests.length > 5 && !expandedMonth && !expandedDay) {
      return (
        <SpanAction className="ml-1" onClick={this.toggleResults}>
          Show {requests.length - 5} more
        </SpanAction>
      );
    }

    return null;
  };

  render() {
    const { expandedMonth, requests } = this.props;
    const { expandedDay } = this.state;

    return (
      <ResultsContainer
        className="d-none d-md-block"
        expandedMonth={expandedMonth}
      >
        <Results
          expandedMonth={expandedMonth}
          expandedDay={expandedDay}
          className={expandedDay ? 'shadow-lg border pb-3' : ''}
        >
          {requests.map((request, index) => {
            return (
              <div
                title={request.absenceName}
                className={`${
                  // There needs to be bottom margin after first 5 results because "Show More" will be shown
                  index === 4 && (!expandedMonth && !expandedDay)
                    ? 'mb-5'
                    : 'mb-0'
                } d-flex align-items-center`}
                key={index}
              >
                <RequestStatus
                  requestStatus={request.requestStatus}
                  absenceType={request.absenceType}
                >
                  <IconImage
                    src={request.iconUrl}
                    width="15"
                    alt={request.absenceName}
                  />
                </RequestStatus>
                <h5 className="d-inline-block ml-2 flex-grow-1 ellipsis-text">
                  {request.user.fullName}
                </h5>
              </div>
            );
          })}
          {expandedDay && (
            <div className="mt-3">
              <SpanAction className="ml-1" onClick={this.toggleResults}>
                Show Less
              </SpanAction>
            </div>
          )}
        </Results>
        {this.showMoreComponent(requests, expandedMonth, expandedDay)}
      </ResultsContainer>
    );
  }
}

export default DayInfo;
