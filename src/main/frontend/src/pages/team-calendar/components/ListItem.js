import React, { Component } from 'react';
import { createMonth } from '../../../components/calendar/calendarUtils';
import Day from '../../../components/calendar/Day';
import { ListItemRow } from './styled';
import styled from 'styled-components';

const DayCellLarge = styled.div`
  width: 55px;
`;

class ListItem extends Component {
  render() {
    return this.props.users.map(user => (
      <ListItemRow key={user.id} className="d-flex align-items-stretch">
        <div className="p-2 d-flex flex-grow-1 align-items-center justify-content-start">
          {user.fullName}
        </div>
        {createMonth(
          this.props.selectedMonth,
          this.props.publicHolidays,
          user.days
        ).map((day, index) => (
          <Day
            key={`${user.fullName}-${index}`}
            {...day}
            simplified
            isListView
          />
        ))}
        <DayCellLarge className="p-2 d-flex align-items-center justify-content-center">
          {user.nonDeductedDays}
        </DayCellLarge>
        <DayCellLarge className="p-2 d-flex align-items-center justify-content-center">
          {user.leaveDays}
        </DayCellLarge>
        <DayCellLarge className="p-2 d-flex align-items-center justify-content-center">
          {user.sickDays}
        </DayCellLarge>
      </ListItemRow>
    ));
  }
}

export default ListItem;
