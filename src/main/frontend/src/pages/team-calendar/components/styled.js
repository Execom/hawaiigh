import React from 'react';
import ReactSVG from 'react-svg';
import styled from 'styled-components';
import { ifProp, prop } from 'styled-tools';
import switchProp from 'styled-tools/dist/cjs/switchProp';

export const ArrowImage = styled.img`
  width: 15px;
  transform: rotate(${props => (props.expanded ? '-180deg' : '0')});
  transition: 0.3s ease-in-out;
`;

export const Image = styled.img`
  max-width: 25px;
`;

export const ViewChange = styled.div`
  position: absolute;
  margin-left: 8px;
  justify-content: center;
  align-items: center;
  left: 0;
  span {
    transition: 0.2s ease-in-out;
    cursor: pointer;
    z-index: 2;
    background: transparent;

    &:first-of-type {
      pointer-events: ${props =>
        props.activeView === 'calendar' ? 'none' : 'auto'};
    }

    &:last-of-type {
      pointer-events: ${props =>
        props.activeView === 'list' ? 'none' : 'auto'};
    }
  }
  @media (min-width: 1267px) {
    display: flex;
  }
  @media (max-width: 1266px) {
    display: ${props => (props.teamCalendar ? 'flex' : 'none')};
  }

  ${props =>
    props.teamCalendar
      ? '@media (max-width: 767px) { flex-direction: column; display: grid; margin-left: 5px; padding-bottom: 5px }'
      : ''}
`;

export const ResultsToggle = styled.span`
  right: 0;
  cursor: pointer;
`;

export const TableHeading = styled.div`
  font-size: 14px;
  display: table-cell;

  &:first-of-type,
  &:nth-child(2) {
    min-width: 20px;

    @media (max-width: 767px) {
      display: none;
    }
  }

  @media (min-width: 768px) {
    min-width: 80px;
  }
`;

export const TableCell = styled.div`
  font-size: 14px;
  max-width: 100px;
  width: ${ifProp('weekNumber', '30px', 'auto')};
  background: ${ifProp('weekNumber', '#CCCCCC', 'white')};
  z-index: ${props => props.index};
  position: relative;
  border-left: ${props =>
    props.isToday ? '2px solid grey' : '1px solid #c0c0c0'};
  border-top: ${props =>
    props.isToday ? '2px solid grey' : '1px solid #c0c0c0'};
  border: ${ifProp('isToday', '2px solid grey')};

  &:last-child {
    border-right: 1px solid #c0c0c0;
  }
  }

  @media (min-width: 768px) {
    max-height: 100px;
  }

  @media (max-width: 767px) {
    height: 40px;
    vertical-align: middle;
    background: ${ifProp('isWeekend', '#B5B5B6', 'white')};
  }
`;

export const DayHeading = styled.h5`
  display: flex;

  @media (min-width: 768px) {
    background: ${ifProp('isWeekend', '#B5B5B6', '#F2F2F2')};
  }

  @media (max-width: 767px) {
    display: block;

    ${({ hasRequests }) =>
      hasRequests &&
      `
  &:after {
      content: '';
      width: 10px;
      height: 10px;
      border-radius: 50%;
      background: #FB4B4F;
      position: absolute;
      top: 5px;
    }
    `}
  }
`;

export const VerticalText = styled.h5`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) rotate(-90deg);
`;

export const DayNumber = styled.span`
  position: ${ifProp('isHoliday', 'absolute', 'static')};
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  @media (min-width: 767px) {
    position: static;
  }
`;

export const EmployeeContainer = styled.div`
  height: 70vh;
  overflow: auto;
`;

export const TableHeadingContainer = styled.div`
  min-width: 18px;
  width: 2%;
  font-size: 14px;
`;

export const TableHeadingText = styled.div`
  width: 55px;
  font-size: 12px;
`;

export const ListItemRow = styled.div`
  &:nth-child(even) {
    background-color: #f2f2f2;
  }
  > div {
    &:first-child {
      width: 150px;
    }
  }
`;

export const Results = styled.div`
  max-height: ${props =>
    props.expandedMonth || props.expandedDay ? 'auto' : '125px'};
  min-height: 125px;
  overflow: hidden;
  background: white;
  position: relative;
  z-index: ${props => (props.expandedDay ? '1' : 'auto')};
`;

export const IconImage = styled.img`
  min-width: 15px;
  width: 15px;
  max-width: 15px;
`;

export const ResultsContainer = styled.div`
  min-height: 155px;
  max-height: ${props => (props.expandedMonth ? 'auto' : '155px')};
`;

export const RequestStatus = styled.div`
  width: 21px;
  height: 21px;
  min-width: 21px;
  max-width: 21px;
  padding: 3px;
  background-color: ${props =>
    props.absenceType === 'SICKNESS'
      ? '#E45052'
      : switchProp(prop('requestStatus'), {
          APPROVED: '#69bf69',
          PENDING: '#ecb041'
        })};
`;

export const ViewIcon = styled(({ isSelected, ...rest }) => (
  <ReactSVG {...rest} />
))`
  cursor: pointer;
  svg {
    path.ViewType,
    rect.rectView,
    rect.rectGroup {
      transition: all 0.3s ease-in-out;
    }

    rect.rectSingle {
      transition: all 0.3s ease-in-out;
    }
  }

  &:hover {
    svg {
      path.ViewType,
      rect.rectView,
      rect.rectGroup {
        fill: #fb4b4f;
      }

      rect.rectSingle {
        fill: black;
      }
    }
  }

  svg {
    height: 30px;
    width: 30px;

    path.ViewType,
    rect.rectView,
    rect.rectGroup {
      fill: ${props => (props.isSelected ? '#FB4B4F' : 'black')};
    }

    rect.rectSingle {
      fill: ${props => (!props.isSelected ? '#FB4B4F' : 'black')};
    }
  }
`;

export const HolidayIconContainer = styled.div`
  margin-left: auto;

  @media (max-width: 767px) {
    position: relative;
    left: 17px;
    bottom: 30px;
  }
`;

export const DayNumberContainer = styled.div`
  display: inline-block;
  padding: 4px 8px;
  z-index: 1;
`;

export const CalendarViewImage = styled.img`
  width: 20px;
  margin-right: 1px;
`;
