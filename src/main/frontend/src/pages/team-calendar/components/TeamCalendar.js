import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Day from '../../../components/calendar/Day';
import HolidayImg from '../../../img/holiday2.svg';
import { openTeamRequestsModal } from '../../../store/actions/modalActions';
import { getWindowWidth } from '../../../store/selectors';
import DayInfo from './DayInfo';
import { daysOfWeek } from './helpers';
import {
  DayHeading,
  DayNumber,
  TableCell,
  TableHeading,
  VerticalText,
  HolidayIconContainer,
  DayNumberContainer,
  CalendarViewImage
} from './styled';
import styled from 'styled-components';

const TableRow = styled.div`
  div {
    &:last-child {
      ${TableCell} {
        border-bottom: 1px solid #c0c0c0;
      }
    }
  }
`;

class TeamCalendar extends Component {
  state = {
    expandedDay: false
  };

  toggleResults = () => {
    this.setState(prevState => ({
      expandedDay: !prevState.expandedDay
    }));
  };

  shouldOpenModal = requests => {
    if (requests.length && this.props.windowWidth < 768) {
      this.props.openTeamRequestsModal(requests);
    }
  };

  // Method for rendering month calendar
  renderDaysCalendar = () => {
    let table = [];
    // Number of for loops per week
    const forLoops = this.props.calendar.length / 7;

    for (let i = 0; i < forLoops; i++) {
      let children = [
        // First cell that holds week value
        <TableCell
          weekNumber={true}
          className="border align-middle d-none d-md-table-cell position-relative"
          key={`${i}-week`}
        >
          <VerticalText>
            <span className="mb-2">Week</span>
            {this.props.calendar[i * 7]
              ? this.props.calendar[i * 7].week
              : this.props.calendar[i * 7 + 6].week}
          </VerticalText>
        </TableCell>
      ];

      // This is rendering 7 days at a time
      for (let j = i * 7; j < (i + 1) * 7; j++) {
        const selectedDay = this.props.calendar[j];

        if (selectedDay) {
          children.push(
            <TableCell
              isWeekend={selectedDay.isWeekend}
              isToday={selectedDay.isToday}
              className="d-table-cell"
              index={forLoops - i}
              key={j}
            >
              {this.props.isDashboard ? (
                <Day
                  isWeekend={selectedDay.isWeekend}
                  isMobile
                  publicHoliday={selectedDay.publicHoliday}
                  day={selectedDay.day}
                  userDay={selectedDay.requests}
                />
              ) : (
                <DayHeading
                  onClick={() => this.shouldOpenModal(selectedDay.requests)}
                  isWeekend={selectedDay.isWeekend}
                  className="text-center text-md-left calendar-day-heading"
                  hasRequests={selectedDay.requests.length}
                >
                  <DayNumberContainer isHoliday={selectedDay.publicHoliday}>
                    <DayNumber>{selectedDay.day.format('D')}</DayNumber>
                  </DayNumberContainer>
                  {selectedDay.publicHoliday && (
                    <HolidayIconContainer>
                      <CalendarViewImage
                        src={HolidayImg}
                        alt="public holiday"
                      />
                    </HolidayIconContainer>
                  )}
                </DayHeading>
              )}
              {!this.props.isDashboard && (
                <DayInfo
                  expandedMonth={this.props.expandedMonth}
                  requests={selectedDay.requests}
                />
              )}
            </TableCell>
          );
        } else {
          children.push(<TableCell className="d-table-cell" key={j} />);
        }
      }
      table.push(
        <div className="d-table-row" key={i}>
          {children}
        </div>
      );
    }

    return table;
  };

  render() {
    return (
      <div className="w-100 d-table">
        <div className="bg-danger text-white py-1 d-table-row-group">
          <div className="d-table-row">
            {daysOfWeek.map((day, index) => {
              return (
                <Fragment key={index}>
                  <TableHeading className="py-2 text-center d-md-none">
                    {day && day.substring(0, 1)}
                  </TableHeading>
                  <TableHeading className="py-2 text-center d-none d-md-table-cell">
                    {day}
                  </TableHeading>
                </Fragment>
              );
            })}
          </div>
        </div>
        <TableRow className="d-table-row-group">
          {this.renderDaysCalendar()}
        </TableRow>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  windowWidth: getWindowWidth(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      openTeamRequestsModal
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamCalendar);
