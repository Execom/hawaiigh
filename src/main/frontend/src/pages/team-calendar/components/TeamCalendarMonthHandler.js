import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _flatMap from 'lodash/flatMap';
import moment from 'moment';
import {
  createMonthCalendar,
  selectFromYearArray
} from '@/components/calendar/calendarUtils';
import { CarouselContainer } from '@/components/common/TeamCalendarCarousel';
import { default as ArrowDown } from '@/img/icons/arrow-down.svg';
import CalendarViewIcon from '@/img/icons/calendar_view.svg';
import ListViewIconIcon from '@/img/icons/list_view.svg';
import { requestAllowanceForUser } from '@/store/actions/allowanceActions';
import { requestTeamRequestsForMonth } from '@/store/actions/requestsActions';
import {
  removeUserDaysForTeamOnYearChange,
  requestTeamsName,
  requestUserDaysForTeam
} from '@/store/actions/teamActions';
import { setSelectedDate } from '@/store/actions/yearActions';
import {
  getPublicHolidays,
  getTeamRequests,
  getTeams,
  getUser,
  getUserDaysForTeam,
  getWindowWidth
} from '@/store/selectors';
import { ArrowSelect } from '@/pages/dashboard/components/styled';
import ListView from '@/pages/team-calendar/components/ListView';
import {
  ResultsToggle,
  ViewChange,
  ViewIcon
} from '@/pages/team-calendar/components/styled';
import TeamCalendar from '@/pages/team-calendar/components/TeamCalendar';
import { formatSelectedMonth, formatMonth } from '@/store/helperFunctions';

class TeamCalendarMonthHandler extends Component {
  createMonthsList = yearsWithAllowance => {
    const monthsArray = _flatMap(
      yearsWithAllowance.map(year => {
        return moment
          .months()
          .reverse()
          .map(month => `${year}-${month}`);
      })
    );

    return monthsArray;
  };

  state = {
    activeView: 'calendar',
    expandedMonth: false,
    selectedTeam: this.props.teamId,
    selectedMonth: selectFromYearArray(
      moment().year(),
      this.props.yearsWithAllowance,
      'month'
    ),
    monthsWithAllowance: this.createMonthsList(this.props.yearsWithAllowance),
    calendar: createMonthCalendar(
      selectFromYearArray(
        moment().year(),
        this.props.yearsWithAllowance,
        'month'
      ),
      this.props.publicHolidays,
      this.props.teamRequests,
      'requests'
    )
  };

  componentDidMount() {
    createMonthCalendar(
      this.state.selectedMonth,
      this.props.publicHolidays,
      this.props.teamRequests,
      'requests'
    );
    this.props.setSelectedDate(this.state.selectedMonth);
    this.props.requestTeamsName();

    const year = selectFromYearArray(
      moment().year(),
      this.props.yearsWithAllowance,
      'year'
    );

    this.props.requestAllowanceForUser({
      year
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.teamRequests !== this.props.teamRequests) {
      this.renderMonthCalendar(this.state.selectedMonth);
    }

    if (
      prevProps.windowWidth > 1266 &&
      this.props.windowWidth < 1267 &&
      this.state.activeView === 'list'
    ) {
      this.setState({
        activeView: 'calendar'
      });
    }
  }

  handleCalendarChange = (selectedMonth, selectedTeam) => {
    this.props.removeUserDaysForTeamOnYearChange();
    this.props.requestTeamRequestsForMonth({
      yearMonth: formatSelectedMonth(selectedMonth),
      teamId: selectedTeam
    });
    this.props.requestUserDaysForTeam({
      month: formatSelectedMonth(selectedMonth),
      teamId: selectedTeam
    });
    this.setState({
      selectedMonth,
      selectedTeam
    });
    this.props.setSelectedDate(selectedMonth);
  };

  renderMonthCalendar = selectedMonth => {
    this.setState(prevState => {
      return {
        ...prevState,
        calendar: createMonthCalendar(
          selectedMonth,
          this.props.publicHolidays,
          this.props.teamRequests,
          'requests'
        )
      };
    });
  };

  changeView = view => {
    this.setState({
      activeView: view
    });
  };

  toggleResults = () => {
    this.setState(prevState => ({
      expandedMonth: !prevState.expandedMonth
    }));
  };

  render() {
    const { activeView } = this.state;

    return (
      <div className="px-4 d-flex flex-column mb-4 flex-grow-1">
        <div className="d-flex flex-column flex-grow-1">
          <CarouselContainer className="justify-content-lg-center position-relative justify-content-around rounded d-flex align-items-center p-2 my-3">
            <ViewChange teamCalendar activeView={this.state.activeView}>
              <span
                onClick={() => this.changeView('calendar')}
                className="mr-2"
              >
                <ViewIcon
                  isSelected={activeView === 'calendar'}
                  src={CalendarViewIcon}
                />
              </span>
              <span onClick={() => this.changeView('list')}>
                <ViewIcon
                  isSelected={activeView === 'list'}
                  src={ListViewIconIcon}
                />
              </span>
            </ViewChange>
            <div className="d-flex flex-column flex-md-row col-12 justify-content-center z-index-1">
              <div className="position-relative mr-2 col-sm-6 col-md-4 col-lg-3 col-xl-2 pr-0">
                <select
                  onChange={e =>
                    this.handleCalendarChange(
                      e.target.value,
                      this.state.selectedTeam
                    )
                  }
                  defaultValue={this.state.selectedMonth}
                  className="select-custom mb-2 mb-md-0 pl-2 pr-5 py-1 w-100 rounded"
                >
                  {this.state.monthsWithAllowance.map(month => (
                    <option key={month} value={month}>
                      {formatMonth(month)}
                    </option>
                  ))}
                </select>
                <ArrowSelect src={ArrowDown} alt="arrow down" />
              </div>
              <div className="position-relative mr-2 col-sm-6 col-md-4 col-lg-3 col-xl-2 pr-0">
                <select
                  onChange={e =>
                    this.handleCalendarChange(
                      this.state.selectedMonth,
                      e.target.value
                    )
                  }
                  value={this.state.selectedTeam}
                  className="select-custom mb-2 mb-md-0 pl-2 pr-5 py-1 w-100 rounded"
                >
                  {this.props.teams.map(team => (
                    <option key={team.id} value={team.id}>
                      {team.name}
                    </option>
                  ))}
                </select>
                <ArrowSelect src={ArrowDown} alt="arrow down" />
              </div>
            </div>
            {this.state.activeView === 'calendar' && (
              <ResultsToggle
                onClick={this.toggleResults}
                className="mr-2 d-none d-md-block position-absolute"
              />
            )}
          </CarouselContainer>
          <div
            className={
              this.state.activeView === 'calendar'
                ? 'd-flex flex-column'
                : 'd-none'
            }
          >
            <TeamCalendar
              expandedMonth={this.state.expandedMonth}
              calendar={this.state.calendar}
              teamRequests={this.props.teamRequests}
              windowWidth={this.props.windowWidth}
            />
          </div>
          <div
            className={
              this.state.activeView === 'calendar'
                ? 'd-none'
                : 'd-flex flex-column flex-grow-1'
            }
          >
            <ListView
              selectedMonth={this.state.selectedMonth}
              userDaysForTeam={this.props.userDaysForTeam}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  teamRequests: getTeamRequests(state),
  teamId: getUser(state).teamId,
  userDaysForTeam: getUserDaysForTeam(state),
  teams: [{ name: 'All employees', id: '' }, ...getTeams(state).name],
  windowWidth: getWindowWidth(state),
  publicHolidays: getPublicHolidays(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestTeamRequestsForMonth,
      setSelectedDate,
      requestUserDaysForTeam,
      removeUserDaysForTeamOnYearChange,
      requestTeamsName: requestTeamsName,
      requestAllowanceForUser
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamCalendarMonthHandler);
