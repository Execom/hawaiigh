import moment from 'moment';
import React, { Component, Fragment } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestUserDaysForTeam } from '../../../store/actions/teamActions';
import {
  getPublicHolidays,
  getUserDaysForTeam
} from '../../../store/selectors';
import ListItem from './ListItem';
import {
  EmployeeContainer,
  TableHeadingContainer,
  TableHeadingText
} from './styled';

class ListView extends Component {
  createWeeksForMonth = month => {
    const daysInMonth = [];

    for (let i = 1; i <= moment(month, 'YYYY-MMMM').daysInMonth(); i++) {
      daysInMonth.push(
        moment(`${month}-${i}`, 'YYYY-MMMM-DD')
          .format('dd')
          .slice(0, 1)
      );
    }

    return daysInMonth;
  };

  render() {
    if (!this.props.userDaysForTeam.results.length) return null;

    return (
      <Fragment>
        <div className="w-100">
          <div className="bg-danger text-white py-1">
            <div className="d-flex align-items-center">
              <div className="flex-grow-1" />
              {this.createWeeksForMonth(this.props.selectedMonth).map(
                (day, index) => (
                  <TableHeadingContainer
                    className="text-center py-2"
                    key={index}
                  >
                    {day}
                  </TableHeadingContainer>
                )
              )}
              <TableHeadingText className="text-center align-middle pr-1">
                Non Deducted Days
              </TableHeadingText>
              <TableHeadingText className="text-center align-middle pr-1">
                Leave Days
              </TableHeadingText>
              <TableHeadingText className="text-center align-middle pr-2">
                Sick Days
              </TableHeadingText>
            </div>
          </div>
        </div>

        <EmployeeContainer className="flex-grow-1">
          <InfiniteScroll
            loadMore={() => {
              this.props.requestUserDaysForTeam({
                month: moment(this.props.selectedMonth, 'YYYY-MMMM').format(
                  'YYYY-MM'
                ),
                page: this.props.userDaysForTeam.page + 1
              });
            }}
            hasMore={!this.props.userDaysForTeam.last}
            threshold={50}
            initialLoad={false}
            useWindow={false}
          >
            <div className="w-100 d-table">
              <div className="text-white py-1 d-table-row-group">
                <div className="d-table-row">
                  <div className="d-table-cell" />
                </div>
              </div>
              <div className="d-table-row-group">
                {this.props.userDaysForTeam.results && (
                  <ListItem
                    publicHolidays={this.props.publicHolidays}
                    selectedMonth={this.props.selectedMonth}
                    users={this.props.userDaysForTeam.results}
                  />
                )}
              </div>
            </div>
          </InfiniteScroll>
        </EmployeeContainer>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  userDaysForTeam: getUserDaysForTeam(state),
  publicHolidays: getPublicHolidays(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestUserDaysForTeam
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListView);
