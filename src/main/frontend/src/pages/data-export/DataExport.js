import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { requestTeamsName } from '@/store/actions/teamActions';
import { requestDataExport } from '@/store/actions/exportActions';
import { requestYearsWithAllowance } from '@/store/actions/allowanceActions';
import {
  getTeams,
  getIsSubmitting,
  getYearsWithAllowance
} from '@/store/selectors';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import DrawerCalendarInput, {
  DrawerContent,
  DrawerFooterForDataExport,
  InputPair
} from '@/pages/administration/components/common/DrawerElements';
import FormikErrorDetector from '@/components/common/FormikErrorDetector';
import { Formik } from 'formik';
import {
  constructQueryParameters,
  SORT_OPTIONS
} from '@/pages/data-export/helpers';
import {
  LEAVE_REQUESTS_REPORT,
  SICKNESS_REPORT,
  USERS_REPORT,
  ALLOWANCES_REPORT,
  LEAVE_PROFILES_REPORT,
  ASC,
  DESC
} from '@/helpers/enum';

class DataExport extends Component {
  state = {
    directions: [DESC, ASC, ASC]
  };

  componentDidMount() {
    this.props.requestTeamsName();
    this.props.requestYearsWithAllowance({
      includeAllUsers: true
    });
  }

  render() {
    const teamsOptions = this.props.teams.map(team => {
      return (
        <option key={team.id} value={team.id}>
          {team.name}
        </option>
      );
    });
    const yearsOptions = this.props.years.map((year, index) => {
      return (
        <option key={`${year}${index}`} value={year}>
          {year}
        </option>
      );
    });

    return (
      <div className="d-flex flex-grow-1 flex-column pt-lg-4 mx-xl-5 px-xl-5 data-export-container">
        <Formik
          initialValues={{
            reportType: LEAVE_REQUESTS_REPORT,
            teamId: '',
            fromDate: moment()
              .startOf('year')
              .toDate(),
            toDate: moment()
              .endOf('year')
              .toDate(),
            includeActiveEmployees: true,
            includeScheduledEmployees: false,
            includeArchivedEmployees: false,
            exportDateRange: false,
            year: this.props.years[0],
            exportFormat: 'csv',
            sortOptions: SORT_OPTIONS[LEAVE_REQUESTS_REPORT],
            firstSortOption: 'day.date',
            secondSortOption: 'Select sort property',
            thirdSortOption: 'Select sort property',
            properties: ['day.date'],
            directions: this.state.directions
          }}
          enableReinitialize
          onSubmit={(values, { setSubmitting }) => {
            const queryParams = constructQueryParameters(values);
            this.props.requestDataExport({
              queryParams,
              reportType: values.reportType
            });
            setSubmitting(false);
          }}
        >
          {({
            handleSubmit,
            handleChange,
            values,
            errors,
            setFieldValue,
            isSubmitting
          }) => (
            <Fragment>
              <DrawerContent className="p-4">
                <InputPair
                  view="select-without-label"
                  caption="Report Type"
                  name="reportType"
                  handleChange={e => {
                    handleChange(e);

                    if (
                      e.target.value !== 'USERS_REPORT' &&
                      values.includeScheduledEmployees &&
                      !values.includeActiveEmployees &&
                      !values.includeArchivedEmployees
                    ) {
                      setFieldValue('includeActiveEmployees', true);
                    }

                    const selectedReportType =
                      e.target.value === SICKNESS_REPORT
                        ? LEAVE_REQUESTS_REPORT
                        : e.target.value;

                    setFieldValue(
                      'sortOptions',
                      SORT_OPTIONS[selectedReportType]
                    );

                    const defaultValueLabel = Object.keys(
                      SORT_OPTIONS[selectedReportType]
                    )[0];
                    const defaultValue =
                      SORT_OPTIONS[selectedReportType][defaultValueLabel];

                    setFieldValue('firstSortOption', [defaultValue]);

                    setFieldValue('properties', [defaultValue]);

                    const directions = this.state.directions;
                    directions[0] =
                      selectedReportType === LEAVE_REQUESTS_REPORT ? DESC : ASC;

                    this.setState(directions);
                  }}
                  value={values.reportType}
                  disabledValue="Select report type"
                  options={
                    <Fragment>
                      <option value={LEAVE_REQUESTS_REPORT}>
                        Leave Requests Report
                      </option>
                      <option value={SICKNESS_REPORT}>Sickness Report</option>
                      <option value={USERS_REPORT}>Users Report</option>
                      <option value={ALLOWANCES_REPORT}>
                        Allowances Report
                      </option>
                      <option value={LEAVE_PROFILES_REPORT}>
                        Leave Profiles Report
                      </option>
                    </Fragment>
                  }
                />
                {values.reportType !== LEAVE_PROFILES_REPORT && (
                  <Fragment>
                    <InputPair
                      view="select-without-label"
                      caption="Team"
                      name="teamId"
                      handleChange={handleChange}
                      value={values.teamId}
                      options={teamsOptions}
                    />
                    {!values.teamId && (
                      <div className="d-flex flex-row">
                        <InputPair
                          view="switch"
                          caption="Active employees"
                          name="includeActiveEmployees"
                          disabled={
                            (values.reportType !== USERS_REPORT &&
                              !values.includeArchivedEmployees) ||
                            (values.reportType === USERS_REPORT &&
                              !values.includeArchivedEmployees &&
                              !values.includeScheduledEmployees)
                          }
                          handleChange={checked => {
                            setFieldValue('includeActiveEmployees', checked);
                          }}
                          flex
                          checked={values.includeActiveEmployees}
                        />
                        {values.reportType === USERS_REPORT && (
                          <InputPair
                            view="switch"
                            caption="Scheduled employees"
                            name="includeScheduledEmployees"
                            handleChange={checked => {
                              setFieldValue(
                                'includeScheduledEmployees',
                                checked
                              );

                              if (
                                !checked &&
                                !values.includeActiveEmployees &&
                                !values.includeArchivedEmployees
                              ) {
                                setFieldValue('includeActiveEmployees', true);
                              }
                            }}
                            flex
                            leftPadding
                            checked={values.includeScheduledEmployees}
                          />
                        )}
                        <InputPair
                          view="switch"
                          caption="Archived employees"
                          name="includeArchivedEmployees"
                          handleChange={checked => {
                            setFieldValue('includeArchivedEmployees', checked);

                            if (
                              (values.reportType !== USERS_REPORT &&
                                !checked &&
                                !values.includeActiveEmployees) ||
                              (values.reportType === USERS_REPORT &&
                                !checked &&
                                !values.includeActiveEmployees &&
                                !values.includeScheduledEmployees)
                            ) {
                              setFieldValue('includeActiveEmployees', true);
                            }
                          }}
                          flex
                          leftPadding
                          checked={values.includeArchivedEmployees}
                        />
                      </div>
                    )}
                    {(values.reportType === LEAVE_REQUESTS_REPORT ||
                      values.reportType === SICKNESS_REPORT) && (
                      <Fragment>
                        <InputPair
                          view="switch"
                          caption="Export Date Range?"
                          name="exportDateRange"
                          handleChange={checked => {
                            setFieldValue('exportDateRange', checked);
                          }}
                          checked={values.exportDateRange}
                        />
                        {values.exportDateRange && (
                          <Fragment>
                            <InputPair
                              view="other"
                              caption="From Date"
                              element={
                                <DatePicker
                                  customInput={<DrawerCalendarInput />}
                                  showYearDropdown
                                  dateFormat="dd MMM yyyy"
                                  locale="en-GB"
                                  selected={values.fromDate}
                                  onChange={e => {
                                    setFieldValue('fromDate', e);
                                    if (
                                      values.fromDate.getFullYear() !==
                                        e.getFullYear() ||
                                      e > values.toDate
                                    ) {
                                      setFieldValue(
                                        'toDate',
                                        moment(e)
                                          .endOf('year')
                                          .toDate()
                                      );
                                    }
                                  }}
                                />
                              }
                            />
                            <InputPair
                              view="other"
                              caption="To Date"
                              element={
                                <DatePicker
                                  customInput={<DrawerCalendarInput />}
                                  showYearDropdown
                                  dateFormat="dd MMM yyyy"
                                  locale="en-GB"
                                  selected={values.toDate}
                                  minDate={values.fromDate}
                                  onChange={e => {
                                    setFieldValue('toDate', e);
                                  }}
                                />
                              }
                            />
                          </Fragment>
                        )}
                      </Fragment>
                    )}
                  </Fragment>
                )}
                {values.reportType === ALLOWANCES_REPORT && (
                  <InputPair
                    view="select-without-label"
                    caption="Allowances for year"
                    name="year"
                    handleChange={handleChange}
                    value={values.year}
                    options={yearsOptions}
                  />
                )}
                {values.reportType !== LEAVE_PROFILES_REPORT && (
                  <Fragment>
                    <div className="d-flex flex-row">
                      <InputPair
                        sort
                        view="select-without-label"
                        caption="First sort option"
                        name="firstSortOption"
                        handleChange={e => {
                          setFieldValue('firstSortOption', e.target.value);
                          setFieldValue(
                            'secondSortOption',
                            'Select sort property'
                          );
                          values.properties = [e.target.value];
                        }}
                        value={values.firstSortOption}
                        options={
                          <Fragment>
                            {Object.keys(values.sortOptions).map(
                              (option, index) => (
                                <option
                                  key={`${option}${index}`}
                                  value={values.sortOptions[option]}
                                >
                                  {option}
                                </option>
                              )
                            )}
                          </Fragment>
                        }
                      />
                      <div
                        className="sort-direction"
                        onClick={() => {
                          const directions = this.state.directions;
                          directions[0] = directions[0] === ASC ? DESC : ASC;
                          this.setState(directions);
                        }}
                      >
                        {this.state.directions[0] === ASC ? '↑' : '↓'}
                      </div>
                    </div>
                    {values.properties.length > 0 && (
                      <div className="d-flex flex-row">
                        <InputPair
                          sort
                          view="select-without-label"
                          caption="Second sort option"
                          name="secondSortOption"
                          handleChange={e => {
                            setFieldValue('secondSortOption', e.target.value);
                            values.properties = [
                              values.firstSortOption,
                              e.target.value
                            ];
                            setFieldValue(
                              'thirdSortOption',
                              'Select sort property'
                            );
                          }}
                          value={values.secondSortOption}
                          options={
                            <Fragment>
                              {Object.keys(values.sortOptions)
                                .filter(
                                  option =>
                                    values.sortOptions[option] !=
                                    values.firstSortOption
                                )
                                .map((option, index) => (
                                  <option
                                    key={`${option}${index}`}
                                    value={values.sortOptions[option]}
                                  >
                                    {option}
                                  </option>
                                ))}
                              <option
                                key="selectSortProperty"
                                value="Select sort property"
                                hidden={true}
                              >
                                Select sort property
                              </option>
                            </Fragment>
                          }
                        />
                        <div
                          className="sort-direction"
                          onClick={() => {
                            const directions = this.state.directions;
                            directions[1] = directions[1] === ASC ? DESC : ASC;
                            this.setState(directions);
                          }}
                        >
                          {this.state.directions[1] === ASC ? '↑' : '↓'}
                        </div>
                      </div>
                    )}
                    {values.properties.length > 1 && (
                      <div className="d-flex flex-row">
                        <InputPair
                          sort
                          view="select-without-label"
                          caption="Third sort option"
                          name="thirdSortOption"
                          handleChange={e => {
                            setFieldValue('thirdSortOption', e.target.value);
                            values.properties[2] = e.target.value;
                          }}
                          value={values.thirdSortOption}
                          options={
                            <Fragment>
                              {Object.keys(values.sortOptions)
                                .filter(
                                  option =>
                                    ![
                                      values.firstSortOption,
                                      values.secondSortOption
                                    ].includes(values.sortOptions[option])
                                )
                                .map((option, index) => (
                                  <option
                                    key={`${option}${index}`}
                                    value={values.sortOptions[option]}
                                  >
                                    {option}
                                  </option>
                                ))}
                              <option
                                key="Select sort property"
                                value="Select sort property"
                                hidden={true}
                              >
                                Select sort property
                              </option>
                            </Fragment>
                          }
                        />
                        <div
                          className="sort-direction"
                          onClick={() => {
                            const directions = this.state.directions;
                            directions[2] = directions[2] === ASC ? DESC : ASC;
                            this.setState(directions);
                          }}
                        >
                          {this.state.directions[2] === ASC ? '↑' : '↓'}
                        </div>
                      </div>
                    )}
                  </Fragment>
                )}
              </DrawerContent>
              <DrawerFooterForDataExport
                isSubmitting={this.props.isSubmitting}
                formClick={handleSubmit}
                handleChange={handleChange}
                exportFormat={values.exportFormat}
              />
              <FormikErrorDetector
                formikProps={{ isSubmitting, errors }}
                onError={this.showErrorToast}
              />
            </Fragment>
          )}
        </Formik>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  teams: [{ id: '', name: 'All Employees' }, ...getTeams(state).name],
  isSubmitting: getIsSubmitting(state),
  years: getYearsWithAllowance(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestTeamsName,
      requestYearsWithAllowance,
      requestDataExport
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DataExport);
