import { formatDate } from '@/pages/administration/components/years/helpers';
import {
  LEAVE_REQUESTS_REPORT,
  SICKNESS_REPORT,
  ALLOWANCES_REPORT,
  USERS_REPORT,
  LEAVE_PROFILES_REPORT,
  USER_FULL_NAME,
  USER_TEAM_NAME,
  USER_STATUS,
  USER_STARTED_WORKING_AT_EXECOM_DATE,
  USER_ROLE,
  REQUEST_USER_FULL_NAME,
  REQUEST_USER_TEAM_NAME,
  REQUEST_STATUS,
  REQUEST_ABSENCE_TYPE,
  REQUEST_START_DATE,
  ALLOWANCE_USER_FULL_NAME,
  ALLOWANCE_TAKEN_ANNUAL,
  USER_FULL_NAME_LABEL,
  TEAM_NAME_LABEL,
  USER_CURRENT_STATUS_LABEL,
  USER_STARTED_WORKING_DATE_LABEL,
  ROLE_LABEL,
  REQUEST_CURRENT_STATUS_LABEL,
  ABSENCE_TYPE_LABEL,
  LEAVE_START_DATE_LABEL,
  TAKEN_ALLOWANCE_LABEL
} from '@/helpers/enum';

export const SORT_OPTIONS = {
  LEAVE_REQUESTS_REPORT: {
    [LEAVE_START_DATE_LABEL]: REQUEST_START_DATE,
    [REQUEST_CURRENT_STATUS_LABEL]: REQUEST_STATUS,
    [ABSENCE_TYPE_LABEL]: REQUEST_ABSENCE_TYPE,
    [USER_FULL_NAME_LABEL]: REQUEST_USER_FULL_NAME,
    [TEAM_NAME_LABEL]: REQUEST_USER_TEAM_NAME
  },
  USERS_REPORT: {
    [USER_FULL_NAME_LABEL]: USER_FULL_NAME,
    [TEAM_NAME_LABEL]: USER_TEAM_NAME,
    [USER_CURRENT_STATUS_LABEL]: USER_STATUS,
    [USER_STARTED_WORKING_DATE_LABEL]: USER_STARTED_WORKING_AT_EXECOM_DATE,
    [ROLE_LABEL]: USER_ROLE
  },
  ALLOWANCES_REPORT: {
    [USER_FULL_NAME_LABEL]: ALLOWANCE_USER_FULL_NAME,
    [TAKEN_ALLOWANCE_LABEL]: ALLOWANCE_TAKEN_ANNUAL,
    [TEAM_NAME_LABEL]: REQUEST_USER_TEAM_NAME
  }
};

export const constructQueryParameters = values => {
  const {
    reportType,
    teamId,
    includeActiveEmployees,
    includeScheduledEmployees,
    includeArchivedEmployees,
    exportDateRange,
    fromDate,
    toDate,
    year,
    exportFormat,
    properties,
    directions
  } = values;
  let queryParams = { exportFormat, properties, directions };

  const userStatusTypes = [
    ...(includeActiveEmployees ? ['ACTIVE'] : []),
    ...(includeScheduledEmployees && reportType === USERS_REPORT
      ? ['INACTIVE']
      : []),
    ...(includeArchivedEmployees ? ['DELETED'] : [])
  ];

  if (reportType !== LEAVE_PROFILES_REPORT) {
    queryParams = {
      ...queryParams,
      ...(teamId && { teamId }),
      ...(!teamId && { userStatusTypes })
    };
  }

  if (reportType === LEAVE_REQUESTS_REPORT || reportType === SICKNESS_REPORT) {
    queryParams = {
      ...queryParams,
      ...(exportDateRange && {
        fromDate: formatDate(fromDate),
        toDate: formatDate(toDate)
      }),
      ...(reportType === LEAVE_REQUESTS_REPORT && {
        absenceType: ['LEAVE', 'TRAINING', 'BONUS']
      }),
      ...(reportType === SICKNESS_REPORT && { absenceType: 'SICKNESS' })
    };
  }

  if (reportType === ALLOWANCES_REPORT) {
    queryParams.year = year;
  }

  return queryParams;
};
