import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';

export default class LineChart extends Component {
  chartReference = React.createRef();

  render() {
    return (
      <Line
        data={this.props.data}
        ref={this.chartReference}
        options={{
          tooltips: {
            mode: 'point',
            intersect: false
          },
          responsive: true,
          maintainAspectRatio: false,
          height: 2,
          title: {
            display: true,
            text: this.props.title,
            fontSize: 15,
            fontFamily: 'Montserrat',
            fontColor: '#212529',
            padding: 25
          },
          legend: {
            display: true,
            position: 'bottom',
            align: 'center',
            labels: {
              fontColor: '#212529',
              fontSize: 14,
              boxWidth: 15,
              fontFamily: 'Source Sans Pro'
            }
          },
          elements: {
            center: {
              innerLabel: '',
              innerValue: ''
            }
          }
        }}
      />
    );
  }
}
