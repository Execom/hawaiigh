import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import { ArrowSelect } from '@/pages/dashboard/components/styled';
import ArrowDown from '@/img/icons/arrow-down.svg';
import ScheduledUsers from '@/img/icons/scheduled_users.svg';
import { TimestampFilterGroup, DatetimeSelect, Refresh } from './styled';
import { requestHardwareMonitoringData } from '@/store/actions/monitoringActions';
import { TIME_RANGES, getTimeRange } from './helpers';

class FilterGroup extends Component {
  state = {
    monitoringTimeRange: {
      fromTimestamp: moment()
        .subtract(moment.duration('6:00:00'))
        .format('YYYY-MM-DDTHH:mm:ss'),
      toTimestamp: moment().format('YYYY-MM-DDTHH:mm:ss')
    },
    showTimepicker: false,
    relativeTimePickerDefault: '06:00:00'
  };

  handleTimeRangeChange = e => {
    const { monitoringTimeRange } = this.state;
    const timeRange = e.target.value;

    monitoringTimeRange.toTimestamp = moment().format('YYYY-MM-DDTHH:mm:ss');
    monitoringTimeRange.fromTimestamp = moment()
      .subtract(moment.duration(timeRange))
      .format('YYYY-MM-DDTHH:mm:ss');

    this.props.requestHardwareMonitoringData(monitoringTimeRange);

    this.setState({
      monitoringTimeRange,
      relativeTimePickerDefault: timeRange
    });
  };

  handleDatePicker = e => {
    const { value, name } = e.target;
    const { fromTimestamp, toTimestamp } = this.state.monitoringTimeRange;
    let newFromDate = moment(fromTimestamp);
    let newToDate = moment(toTimestamp);

    switch (name) {
      case 'fromDay':
        newFromDate = newFromDate.set('date', value);
        break;
      case 'fromMonth':
        newFromDate = newFromDate.set('month', value);
        break;
      case 'fromHours':
        newFromDate = newFromDate.set('hour', value);
        break;
      case 'fromMinutes':
        newFromDate = newFromDate.set('minute', value);
        break;
      case 'toDay':
        newToDate = newToDate.set('date', value);
        break;
      case 'toMonth':
        newToDate = newToDate.set('month', value);
        break;
      case 'toHours':
        newToDate = newToDate.set('hour', value);
        break;
      case 'toMinutes':
        newToDate = newToDate.set('minute', value);
        break;
      default:
        break;
    }

    const newFormattedToDate = newToDate.format('YYYY-MM-DDTHH:mm:ss');
    const newFormattedFromDate = newFromDate.format('YYYY-MM-DDTHH:mm:ss');

    let monitoringTimeRange = {
      fromTimestamp: newFormattedFromDate,
      toTimestamp: newFormattedToDate
    };

    this.setState({ monitoringTimeRange });
  };

  refresh = () => {
    this.props.requestHardwareMonitoringData(this.state.monitoringTimeRange);
    const relativeTimePickerDefault = 'Relative time range';
    this.setState({ relativeTimePickerDefault });
  };

  handleClick = () => {
    const showTimepicker = !this.state.showTimepicker;
    this.setState({ showTimepicker });
  };

  render() {
    const fromTime = moment(this.state.monitoringTimeRange.fromTimestamp);
    const toTime = moment(this.state.monitoringTimeRange.toTimestamp);
    const relativeTimePickerDefault = this.state.relativeTimePickerDefault;
    const time = getTimeRange(this.props.oldestTimestamp);

    return (
      <TimestampFilterGroup>
        <img className="time" src={ScheduledUsers} alt="Scheduled icon" />
        <div className="dropdown">
          <div className="position-relative relative-timerange-selector">
            <button className="dropbtn" onClick={this.handleClick}>
              Time range selector
            </button>
            <ArrowSelect src={ArrowDown} alt="arrow down" />
          </div>
          <div
            className={
              this.state.showTimepicker
                ? 'dropdown-content show-timepicker'
                : 'dropdown-content'
            }
          >
            <div>
              <div className="date-picker">
                <DatetimeSelect
                  key={100 + fromTime.date()}
                  name="fromDay"
                  onChange={this.handleDatePicker}
                  defaultValue={fromTime.date()}
                >
                  {time.days.map(day => (
                    <option key={`fromDay${day}`} value={day}>
                      {day}
                    </option>
                  ))}
                </DatetimeSelect>
                .
                <DatetimeSelect
                  key={200 + fromTime.month()}
                  name="fromMonth"
                  onChange={this.handleDatePicker}
                  defaultValue={fromTime.month()}
                >
                  {time.months.map(month => (
                    <option key={`fromMonth${month}`} value={month}>
                      {month}
                    </option>
                  ))}
                </DatetimeSelect>
                {`.${time.currentYear}.`}
                <DatetimeSelect
                  key={300 + fromTime.hour()}
                  className="hours"
                  name="fromHours"
                  onChange={this.handleDatePicker}
                  defaultValue={fromTime.hour()}
                >
                  {time.hours.map(hour => (
                    <option key={`fromHour${hour}`} value={hour}>
                      {hour}
                    </option>
                  ))}
                </DatetimeSelect>
                :
                <DatetimeSelect
                  key={400 + fromTime.minute()}
                  className="minutes"
                  name="fromMinutes"
                  onChange={this.handleDatePicker}
                  defaultValue={fromTime.minute()}
                >
                  {time.minutes.map(minute => (
                    <option key={`fromMinut${minute}`} value={minute}>
                      {minute}
                    </option>
                  ))}
                </DatetimeSelect>
                h
              </div>
              <div className="date-picker">
                <DatetimeSelect
                  key={500 + toTime.date()}
                  name="toDay"
                  onChange={this.handleDatePicker}
                  defaultValue={toTime.date()}
                >
                  {time.days.map(day => (
                    <option key={`toDay${day}`} value={day}>
                      {day}
                    </option>
                  ))}
                </DatetimeSelect>
                .
                <DatetimeSelect
                  key={600 + toTime.month()}
                  name="toMonth"
                  onChange={this.handleDatePicker}
                  defaultValue={toTime.month()}
                >
                  {time.months.map(month => (
                    <option key={`toMonth${month}`} value={month}>
                      {month}
                    </option>
                  ))}
                </DatetimeSelect>
                {`.${time.currentYear}.`}
                <DatetimeSelect
                  key={700 + toTime.hour()}
                  className="hours"
                  name="toHours"
                  onChange={this.handleDatePicker}
                  defaultValue={toTime.hour()}
                >
                  {time.hours.map(hour => (
                    <option key={`toHour${hour}`} value={hour}>
                      {hour}
                    </option>
                  ))}
                </DatetimeSelect>
                :
                <DatetimeSelect
                  key={800 + toTime.minute()}
                  className="minutes"
                  name="toMinutes"
                  onChange={this.handleDatePicker}
                  defaultValue={toTime.minute()}
                >
                  {time.minutes.map(minute => (
                    <option key={`toMinute${minute}`} value={minute}>
                      {minute}
                    </option>
                  ))}
                </DatetimeSelect>
                h
              </div>
            </div>
            <Refresh onClick={this.refresh}>Refresh</Refresh>
          </div>
        </div>
        <div className="position-relative timerange-selector">
          <select
            key={relativeTimePickerDefault}
            className="drop-down select-custom pl-2 py-1 d-md-flex no-border"
            onChange={this.handleTimeRangeChange}
            defaultValue={relativeTimePickerDefault}
          >
            {Object.keys(TIME_RANGES).map((range, index) => (
              <option key={`${range}${index}`} value={TIME_RANGES[range]}>
                {range}
              </option>
            ))}
            <option value="Relative time range" disabled hidden>
              Relative time range
            </option>
          </select>
          <ArrowSelect src={ArrowDown} alt="arrow down" />
        </div>
      </TimestampFilterGroup>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestHardwareMonitoringData
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(FilterGroup);
