import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  requestHardwareMonitoringData,
  requestHttpEndpointsMonitoringData,
  requestOldestTimestamp
} from '@/store/actions/monitoringActions';
import { requestAdmins, removeAdmin } from '@/store/actions/adminActions';
import { openAddAdminModal } from '@/store/actions/modalActions';
import {
  getHardwareMonitoringData,
  getHttpEndpointsMonitoringData,
  getAdmins,
  getUser,
  getOldestTimestamp
} from '@/store/selectors';
import { DashboardFlexContainer, LineGraphBlock, TableBlock } from './styled';
import LineChart from './LineChart';
import HttpRequestsTable from './Table';
import AdminTable from './AdminTable';
import AddAdminButton from './AddAdminButton';
import FilterGroup from './FilterGroup';

class AdminDashboard extends Component {
  state = {
    cpuUsageData: {
      labels: [],
      datasets: [
        {
          datalabels: {
            labels: {
              title: null
            }
          },
          label: 'Machine CPU Usage',
          data: [],
          fill: false, // Don't fill area under the line
          borderColor: 'red', // Line color
          borderWidth: 1,
          pointRadius: 0.5,
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#2200ff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2
        },
        {
          datalabels: {
            labels: {
              title: null
            }
          },
          label: 'JVM CPU Usage',
          data: [],
          fill: false, // Don't fill area under the line
          borderColor: 'green', // Line color
          borderWidth: 1,
          pointRadius: 0.5,
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#2200ff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2
        }
      ]
    },
    memoryUsageData: {
      labels: [],
      datasets: [
        {
          datalabels: {
            labels: {
              title: null
            }
          },
          label: 'JVM memory usage',
          data: [],
          fill: false, // Don't fill area under the line
          borderColor: 'blue', // Line color
          borderWidth: 1,
          pointRadius: 0.5,
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#2200ff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2
        }
      ]
    },
    httpRequests: null,
    admins: null,
    oldestTimestamp: null
  };

  componentDidMount() {
    this.props.requestHardwareMonitoringData(this.state.monitoringTimeRange);
    this.props.requestHttpEndpointsMonitoringData();
    this.props.loggedUser.userAdminPermission === 'SUPER' &&
      this.props.requestAdmins();
    this.props.requestOldestTimestamp();
  }

  componentWillReceiveProps() {
    const { cpuUsageData, memoryUsageData } = { ...this.state };
    const admins = this.props.admins;
    const oldestTimestamp = this.props.oldestTimestamp;

    const machineCpuUsage =
      this.props.hardwareMonitoringData == null
        ? []
        : this.props.hardwareMonitoringData.map(entry => entry.systemCpuUsage);

    const jvmCpuUsage =
      this.props.hardwareMonitoringData == null
        ? []
        : this.props.hardwareMonitoringData.map(entry => entry.jvmCpuUsage);

    const memoryUsage =
      this.props.hardwareMonitoringData == null
        ? []
        : this.props.hardwareMonitoringData.map(entry => entry.memoryUsage);

    const labels =
      this.props.hardwareMonitoringData == null
        ? []
        : this.props.hardwareMonitoringData.map(entry =>
            moment(entry.timestamp).format('DD.MM. HH:mm')
          );

    const httpRequests = this.props.httpEndpointMonitorignData
      ? [...this.props.httpEndpointMonitorignData]
      : null;

    cpuUsageData.datasets[0].data = [...machineCpuUsage];
    cpuUsageData.datasets[1].data = [...jvmCpuUsage];
    memoryUsageData.datasets[0].data = [...memoryUsage];
    cpuUsageData.labels = labels;
    memoryUsageData.labels = labels;

    this.setState({
      cpuUsageData,
      memoryUsageData,
      httpRequests,
      admins,
      oldestTimestamp
    });
  }

  handleAdminRemove = admin => {
    admin.userAdminPermission = 'NONE';
    this.props.removeAdmin(admin);
  };

  handleAddAdmin = () => {
    this.props.openAddAdminModal();
  };

  render() {
    return (
      <Fragment>
        <FilterGroup oldestTimestamp={this.state.oldestTimestamp} />
        <DashboardFlexContainer>
          <LineGraphBlock>
            <LineChart data={this.state.cpuUsageData} title="CPU usage" />
          </LineGraphBlock>
          <LineGraphBlock>
            <LineChart
              data={this.state.memoryUsageData}
              title="JVM memory usage in MB"
            />
          </LineGraphBlock>
          <TableBlock>
            <HttpRequestsTable httpRequests={this.state.httpRequests} />
          </TableBlock>
          {this.props.loggedUser.userAdminPermission === 'SUPER' && (
            <TableBlock>
              <div className="admin-table-header">
                <div className="table-header">Admins</div>
                <AddAdminButton handleAddAdmin={this.handleAddAdmin} />
              </div>
              <AdminTable
                handleAdminRemove={this.handleAdminRemove}
                admins={this.state.admins}
              />
            </TableBlock>
          )}
        </DashboardFlexContainer>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  hardwareMonitoringData: getHardwareMonitoringData(state),
  httpEndpointMonitorignData: getHttpEndpointsMonitoringData(state),
  admins: getAdmins(state),
  loggedUser: getUser(state),
  oldestTimestamp: getOldestTimestamp(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestHardwareMonitoringData,
      requestHttpEndpointsMonitoringData,
      requestAdmins,
      removeAdmin,
      openAddAdminModal,
      requestOldestTimestamp
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminDashboard);
