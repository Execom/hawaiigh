import React, { Fragment } from 'react';
import DeleteIcon from '@/img/icons/delete.svg';
import { Icon } from '@/pages/administration/components/years/styled';

const AdminTable = props => {
  return (
    <Fragment>
      <table className="table table-dark">
        <thead className="bg-danger">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col"> </th>
          </tr>
        </thead>
        {props.admins != null && (
          <tbody>
            {props.admins.map((admin, i) => (
              <tr key={admin.email}>
                <th scope="row">{i + 1}</th>
                <td>{admin.fullName}</td>
                <td>{admin.email}</td>
                <td>
                  {admin.userAdminPermission === 'BASIC'
                    ? 'Admin'
                    : 'Super Admin'}
                </td>
                <td className="delete-admin-cell">
                  {admin.userAdminPermission !== 'SUPER' && (
                    <Icon
                      className
                      onClick={e => {
                        e.stopPropagation();
                        props.handleAdminRemove(admin);
                      }}
                      src={DeleteIcon}
                      alt="Delete icon"
                    />
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        )}
      </table>
    </Fragment>
  );
};

export default AdminTable;
