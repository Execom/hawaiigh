import React from 'react';
import Add from '@/img/icons/add.svg';
import { Icon } from '@/pages/administration/components/years/styled';
import { IconContainer } from './styled';

const AddAdminButton = props => {
  return (
    <IconContainer className="bg-danger" onClick={props.handleAddAdmin}>
      <Icon src={Add} alt="Add Admin" />
    </IconContainer>
  );
};

export default AddAdminButton;
