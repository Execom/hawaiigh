import styled from 'styled-components';

export const LineGraphBlock = styled.div`
  width: 100%;
  height: 500px;
  border: solid 1px #d3d3d3;
  border-radius: 15px;
  box-shadow: 6px 6px 10px #d3d3d3;
  margin: 20px;
  padding: 10px;
`;

export const TableBlock = styled.div`
  width: 100%;
  border: solid 1px #d3d3d3;
  border-radius: 15px;
  box-shadow: 6px 6px 10px #d3d3d3;
  margin: 20px;
  padding: 10px;

  @media (max-width: 1095px) {
    font-size: 12px;
  }
`;

export const DashboardFlexContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-content: center;
  flex-wrap: wrap;
`;

export const TimestampFilterGroup = styled.div`
  display: flex;
  margin-top: 6px;
  margin-left: auto;
  margin-right: 20px;
  padding-right: 18px;
  padding-left: 2px;
  border: 1px solid darkgray;
  border-radius: 0.25rem;
`;

export const DatetimeSelect = styled.select`
  border: none;
  appearance: none;

  :hover {
    cursor: pointer;
    background-color: #eaeaea;
  }
`;

export const Refresh = styled.button`
  margin-left: 12px;
  padding: 7px;
  border: gray solid 1px;
  border-radius: 8px;

  :hover {
    border: black solid 1px;
    box-shadow: 0px 4px 6px 0px rgba(0, 0, 0, 0.2);
    cursor: pointer;
  }
`;

export const IconContainer = styled.div`
  width: fit-content;
  height: fit-content;
  padding: 10px;
  border-radius: 50%;
  margin-left: auto;
  box-shadow: 1px 1px 16px #936262;
  cursor: pointer;
`;
