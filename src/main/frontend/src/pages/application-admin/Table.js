import React, { Fragment } from 'react';

const HttpRequestsTable = props => {
  return (
    <Fragment>
      <div className="table-header">Http Endpoints Metric</div>
      <table className="table">
        <thead className="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Endpoint</th>
            <th scope="col">Exception</th>
            <th scope="col">Status</th>
            <th scope="col">Number of requests</th>
            <th scope="col">Max response time [sec]</th>
          </tr>
        </thead>
        {props.httpRequests != null && (
          <tbody>
            {props.httpRequests.map((httpEndpoint, i) => (
              <tr key={i + httpEndpoint.endpointUrl}>
                <th scope="row">{i + 1}</th>
                <td>{httpEndpoint.endpointUrl}</td>
                <td>{httpEndpoint.exception}</td>
                <td>{`${httpEndpoint.status}, ${httpEndpoint.outcome}`}</td>
                <td>{httpEndpoint.numberOfRequests}</td>
                <td>{httpEndpoint.maxResponseTime}</td>
              </tr>
            ))}
          </tbody>
        )}
      </table>
    </Fragment>
  );
};

export default HttpRequestsTable;
