import moment from 'moment';

export const TIME_RANGES = {
  'Last 5 minutes': '00:05:00',
  'Last 15 minutes': '00:15:00',
  'Last 30 minutes': '00:30:00',
  'Last 1 hour': '01:00:00',
  'Last 3 hours': '03:00:00',
  'Last 6 hours': '06:00:00',
  'Last 12 hours': '12:00:00',
  'Last 24 hours': '24:00:00',
  'Last 2 days': '48:00:00'
};

export const getTimeRange = timeStamp => {
  const oldestTimestamp = moment(timeStamp);
  const currentTimestamp = moment();
  const currentTimestampDay = currentTimestamp.date();
  const oldestTimestampDay =
    timeStamp === null ? currentTimestampDay : oldestTimestamp.date();
  const days =
    oldestTimestampDay === currentTimestampDay
      ? [currentTimestampDay]
      : [oldestTimestampDay, currentTimestampDay];
  const currentMonth = currentTimestamp.month() + 1;
  const currentYear = currentTimestamp.year();
  const months =
    days[0] > days[1] ? [currentMonth - 1, currentMonth] : [currentMonth];
  const hours = range(0, 23);
  const minutes = range(0, 59);
  return { days, months, currentYear, hours, minutes };
};

const range = (start, end) => {
  if (start === end) return [start];
  return [start, ...range(start + 1, end)];
};
