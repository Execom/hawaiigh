import React, { Component } from 'react';
import Requests from '../../components/requests/Requests';

export default class RequestsWrapper extends Component {
  render() {
    return (
      <div className="d-flex justify-content-center">
        <Requests />
      </div>
    );
  }
}
