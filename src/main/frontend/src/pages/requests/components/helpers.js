import ApprovedIcon from '../../../img/icons/approved.svg';
import BonusIcon from '../../../img/icons/bonus_requests.svg';
import CanceledIcon from '../../../img/icons/canceled.svg';
import CancellationPendingIcon from '../../../img/icons/cancellation_pending.svg';
import LeaveIcon from '../../../img/icons/palm_tree.svg';
import PendingIcon from '../../../img/icons/pending.svg';
import RejectedIcon from '../../../img/icons/rejected.svg';
import SicknessIcon from '../../../img/icons/sickness_requests.svg';
import TrainingIcon from '../../../img/icons/training_requests.svg';

export const filters = [
  'pending',
  'approved',
  'rejected',
  'canceled',
  'cancellation_pending'
];

export const icons = {
  leave: { url: LeaveIcon, alt: 'leave' },
  sickness: { url: SicknessIcon, alt: 'sickness' },
  training: { url: TrainingIcon, alt: 'training' },
  bonus: { url: BonusIcon, alt: 'bonus' }
};

export const requestStatusIcons = {
  APPROVED: ApprovedIcon,
  REJECTED: RejectedIcon,
  CANCELLATION_PENDING: CancellationPendingIcon,
  CANCELED: CanceledIcon,
  PENDING: PendingIcon
};
