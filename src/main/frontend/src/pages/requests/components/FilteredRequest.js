import React, { Component } from 'react';
import Collapse from 'reactstrap/lib/Collapse';
import { TableFooter } from '../../../components/common/Table';
import {
  SeparatorRow,
  TableHeaderNative
} from '../../../components/common/TableStyledComponents';
import ArrowIcon from '../../../img/icons/arrow-down.svg';
import {
  Bordered,
  CollapseBodyContainer,
  CollapseButton,
  Icon,
  NoResults
} from './styled';

export default class FilteredRequest extends Component {
  state = {
    open: false
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.collapseOpen !== this.props.collapseOpen &&
      this.props.shouldParentActivateToggle
    ) {
      this.setState({
        open: this.props.collapseOpen
      });
    }
  }

  toggle = () => {
    this.setState(
      prevState => ({ open: !prevState.open }),
      () => this.props.action(this.state.open)
    );
  };

  render() {
    return (
      <div>
        <CollapseButton
          onClick={this.toggle}
          className="d-flex justify-content-between px-3 align-items-center py-1 rounded mb-2 bg-white"
        >
          <div className="align-items-center d-flex">
            <Icon
              className="mr-2"
              src={this.props.icon.url}
              alt={this.props.icon.alt}
            />
            <span className="text-capitalize">{this.props.title}</span>
          </div>
          <Icon open={this.state.open} size="15px" src={ArrowIcon} alt="" />
        </CollapseButton>
        <Collapse isOpen={this.state.open}>
          <CollapseBodyContainer className="pb-2">
            {this.props.leaveRequests.length ? (
              <table className="table table-hover mb-0">
                <thead className="text-center">
                  <tr>
                    <TableHeaderNative as="" width="230px">
                      Status
                    </TableHeaderNative>
                    <TableHeaderNative width="30%">Reason</TableHeaderNative>
                    <TableHeaderNative>Type</TableHeaderNative>
                    <TableHeaderNative>Start</TableHeaderNative>
                    <TableHeaderNative>End</TableHeaderNative>
                    <TableHeaderNative>Days</TableHeaderNative>
                  </tr>
                  <SeparatorRow height="10px" />
                </thead>
                <tbody className="text-center">
                  {this.props.leaveRequests}
                </tbody>
                <TableFooter className="text-center">
                  <SeparatorRow height="10px" />
                  <tr>
                    <th className="border-0" colSpan={2} />
                    <Bordered className="text-right border-0" colSpan={3}>
                      Total
                    </Bordered>
                    <Bordered className="font-weight-bold border-0">
                      {this.props.total / 8}
                    </Bordered>
                  </tr>
                </TableFooter>
              </table>
            ) : (
              <NoResults className="py-4 text-center">No results</NoResults>
            )}
          </CollapseBodyContainer>
        </Collapse>
      </div>
    );
  }
}
