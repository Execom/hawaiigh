import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Collapse } from 'reactstrap';
import { bindActionCreators } from 'redux';
import _flatMap from 'lodash/flatMap';
import _get from 'lodash/get';
import _startsWith from 'lodash/startsWith';
import _trim from 'lodash/trim';
import { FilterItem } from '@/components/common/FilterItem';
import { FULL_DAY } from '@/helpers/enum';
import Arrow from '@/img/icons/arrow-down.svg';
import FilterIcon from '@/img/icons/filter.svg';
import {
  handleRequestModal,
  openInfoModal,
  openRequestModal
} from '@/store/actions/modalActions';
import {
  requestLeaveRequestsForYear,
  requestUserLeaveRequestsForYear
} from '@/store/actions/requestsActions';
import { reducerTotal } from '@/store/helperFunctions';
import {
  getLeaveRequests,
  getRouter,
  getSearchedEmployee
} from '@/store/selectors';
import FilteredRequest from '@/pages/requests/components/FilteredRequest';
import { filters, icons } from '@/pages/requests/components/helpers';
import RequestItem from '@/pages/requests/components/RequestItem';
import {
  FilterImage,
  FilterImageContainer,
  Filters,
  Image
} from '@/pages/requests/components/styled';

class Requests extends Component {
  state = {
    activeFilters: [],
    collapseOpen: false,
    numberOfOpenedCollapses: 0,
    filterCollapse: false,
    requests: true
  };

  componentDidMount() {
    this.props.requestUserLeaveRequestsForYear({
      year: this.props.selectedYear,
      id: _get(this.props.searchedEmployee, 'id')
    });
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.searchedEmployee !== this.props.searchedEmployee ||
      this.props.selectedYear !== prevProps.selectedYear
    ) {
      this.props.requestUserLeaveRequestsForYear({
        year: this.props.selectedYear,
        id: _get(this.props.searchedEmployee, 'id')
      });
    }

    if (
      !prevProps.leaveRequests &&
      prevProps.leaveRequests !== this.props.leaveRequests
    ) {
      if (_startsWith(this.props.router.location.search, '?requestId=')) {
        const requestId = _trim(
          this.props.router.location.search,
          '?requestId='
        );
        const request = _flatMap(this.props.leaveRequests).find(
          request => request.id === +requestId
        );

        if (request) {
          this.props.handleRequestModal({ request });
        } else {
          this.props.openInfoModal("This request doesn't exist!");
        }
      }
    }
  }

  openModal = () => {
    this.props.openRequestModal();
  };

  toggleCollapse = () =>
    this.setState(prevState => ({
      collapseOpen: !prevState.collapseOpen,
      numberOfOpenedCollapses: prevState.collapseOpen ? 0 : 4
    }));

  toggleFilterCollapse = () => {
    this.setState(prevState => ({ collapse: !prevState.collapse }));
  };

  numberOfOpenedCollapses = (active, numberOfOpenedCollapses) =>
    active ? numberOfOpenedCollapses + 1 : numberOfOpenedCollapses - 1;

  toggleSingleCollapse = singleCollapseActive =>
    this.setState(prevState => ({
      numberOfOpenedCollapses: this.numberOfOpenedCollapses(
        singleCollapseActive,
        prevState.numberOfOpenedCollapses
      ),
      collapseOpen:
        this.numberOfOpenedCollapses(
          singleCollapseActive,
          prevState.numberOfOpenedCollapses
        ) === 4
    }));

  countTotal = items => {
    if (items.length) {
      return items
        .map(item =>
          item.days
            .map(day => (day.duration === FULL_DAY ? 8 : 4))
            .reduce(reducerTotal, 0)
        )
        .reduce(reducerTotal, 0);
    }
  };

  filterRequestsByStatus = (status, checked) => {
    if (checked) {
      this.setState(prevState => ({
        activeFilters: [...prevState.activeFilters, status]
      }));
    } else {
      this.setState(prevState => ({
        activeFilters: prevState.activeFilters.filter(item => item !== status)
      }));
    }
  };

  render() {
    if (!this.props.leaveRequests) return null;

    const filterCheckboxes = filters.map((filter, index) => {
      return (
        <FilterItem
          key={index}
          action={e =>
            this.filterRequestsByStatus(e.target.value, e.target.checked)
          }
          filter={filter}
        />
      );
    });

    const leaveRequests = Object.keys(this.props.leaveRequests).map(
      (key, index) => {
        const items = this.state.activeFilters.length
          ? this.props.leaveRequests[key].filter(
              req => this.state.activeFilters.indexOf(req.requestStatus) > -1
            )
          : this.props.leaveRequests[key];

        return (
          <FilteredRequest
            icon={icons[key]}
            key={index}
            index={index}
            title={key}
            total={this.countTotal(items)}
            collapseOpen={this.state.collapseOpen}
            shouldParentActivateToggle={
              this.state.numberOfOpenedCollapses === 4 ||
              this.state.numberOfOpenedCollapses === 0
            }
            action={singleCollapse => this.toggleSingleCollapse(singleCollapse)}
            leaveRequests={items.map(item => (
              <RequestItem key={item.id} request={item} />
            ))}
          />
        );
      }
    );

    return (
      <Fragment>
        <Filters className="position-absolute">
          <FilterImageContainer
            filterActive={!!this.state.activeFilters.length}
          >
            <FilterImage
              filterCollapseOpen={this.state.filterCollapse}
              onClick={this.toggleFilterCollapse}
              className="p-1 rounded"
              src={FilterIcon}
            />
          </FilterImageContainer>
        </Filters>
        <Image
          onClick={this.toggleCollapse}
          expanded={this.state.collapseOpen}
          className="position-absolute"
          src={Arrow}
          alt="arrow"
        />
        <Collapse isOpen={this.state.collapse}>
          <div className="mb-3 px-3">{filterCheckboxes}</div>
        </Collapse>
        <div className="row">
          <div className="col-md-12">{leaveRequests}</div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  leaveRequests: getLeaveRequests(state),
  searchedEmployee: getSearchedEmployee(state),
  router: getRouter(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestLeaveRequestsForYear,
      openRequestModal,
      requestUserLeaveRequestsForYear,
      handleRequestModal,
      openInfoModal
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Requests);
