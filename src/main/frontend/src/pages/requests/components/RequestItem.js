import _replace from 'lodash/replace';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CapitalizedText } from '../../../components/common/capitalizedText';
import { TableRow } from '../../../components/common/Table';
import {
  SeparatorRow,
  TableCell
} from '../../../components/common/TableStyledComponents';
import { FULL_DAY } from '../../../helpers/enum';
import { handleRequestModal } from '../../../store/actions/modalActions';
import { formatDate, reducerTotal } from '../../../store/helperFunctions';
import { getSearchedEmployee } from '../../../store/selectors';
import { requestStatusIcons } from './helpers';
import { StatusIcon } from './styled';

class RequestItem extends Component {
  openModal = (request, searchedEmployee) => {
    if (searchedEmployee) {
      this.props.handleRequestModal({
        request,
        previewOnly: true
      });
    } else {
      this.props.handleRequestModal({ request });
    }
  };

  render() {
    const {
      request: { absence, requestStatus, reason }
    } = this.props;

    const days = this.props.request.days
      .map(day => (day.duration === FULL_DAY ? 8 : 4))
      .reduce(reducerTotal, 0);

    return (
      <Fragment>
        <TableRow
          className="d-block d-lg-table-row"
          onClick={() =>
            this.openModal(this.props.request, this.props.searchedEmployee)
          }
        >
          <TableCell
            data-label="Status"
            className="d-flex d-lg-table-cell justify-content-between align-items-center text-right text-lg-center"
          >
            <div className="d-flex justify-content-center align-items-center">
              <StatusIcon
                className="mr-2"
                src={requestStatusIcons[requestStatus]}
                requestStatus={requestStatus}
              />
              <CapitalizedText className="text-left">
                {_replace(requestStatus, '_', ' ')}
              </CapitalizedText>
            </div>
          </TableCell>
          <TableCell
            data-label="Reason"
            className="d-flex justify-content-between d-lg-table-cell text-right text-lg-center"
          >
            <span>{reason || '/'}</span>
          </TableCell>
          <TableCell
            data-label="Type"
            className="d-flex d-lg-table-cell text-right text-lg-center justify-content-between"
          >
            <CapitalizedText>{absence.name}</CapitalizedText>
          </TableCell>
          <TableCell
            data-label="Start"
            className="d-flex d-lg-table-cell text-right text-lg-center justify-content-between"
          >
            <span>{formatDate(this.props.request.days[0].date)}</span>
          </TableCell>
          <TableCell
            data-label="End"
            className="d-flex d-lg-table-cell text-right text-lg-center justify-content-between"
          >
            <span>
              {formatDate(
                this.props.request.days[this.props.request.days.length - 1].date
              )}
            </span>
          </TableCell>
          <TableCell
            data-label="Days"
            className="d-flex d-lg-table-cell text-right text-lg-center justify-content-between"
          >
            <span>{days / 8}</span>
          </TableCell>
        </TableRow>
        <SeparatorRow height="10px" />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  searchedEmployee: getSearchedEmployee(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ handleRequestModal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestItem);
