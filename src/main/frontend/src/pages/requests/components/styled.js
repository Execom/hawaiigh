import styled from 'styled-components';
import { ifProp } from 'styled-tools';

export const Filters = styled.div`
  top: 22px;
  left: 40px;
`;

export const FilterImageContainer = styled.div`
  &:after {
    visibility: ${ifProp('filterActive', 'visible', 'hidden')};
    content: '';
    top: -3px;
    width: 10px;
    height: 10px;
    background: #fb4b4f;
    position: absolute;
    border-radius: 50%;
    right: -3px;
  }
`;

export const FilterImage = styled.img`
  width: 30px;
  cursor: pointer;
  border: 1px solid #008b8b;
  transition: 0.1s ease-in-out;
  box-shadow: ${props =>
    props.filterCollapseOpen ? '0 .5rem 1rem rgba(0,0,0,.15)' : 'none'};

  &:hover {
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15);
  }
`;

export const Image = styled.img`
  width: 15px;
  transform: rotate(${props => (props.expanded ? '-180deg' : '0')});
  transition: 0.3s ease-in-out;
  position: absolute;
  right: 41px;
  top: 30px;
  cursor: pointer;
`;

export const StatusIcon = styled.img`
  flex: 0 0 20px;
  width: 20px;
`;

export const CollapseButton = styled.div`
  cursor: pointer;
`;

export const Bordered = styled.th`
  border-top: 2px solid black !important;
  color: #fb4b4f;
  font-size: 20px;
`;

export const Icon = styled.img`
  height: ${props => (props.size ? props.size : '35px')};
  transform: ${props => (props.open ? 'rotate(-180deg)' : 'rotate(0)')};
  transition: 0.3s ease-in-out;

  &.employee-audit-arrow {
    position: absolute;
    top: 20px;
    right: 30px;
  }
`;

export const NoResults = styled.h5`
  color: #fb4b4f;
  font-size: 18px;
`;

export const CollapseBodyContainer = styled.div`
  background: #ededed;
`;
