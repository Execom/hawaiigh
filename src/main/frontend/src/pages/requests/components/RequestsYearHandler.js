import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _get from 'lodash/get';
import moment from 'moment';
import {
  createYearCalendar,
  selectFromYearArray
} from '@/components/calendar/calendarUtils';
import {
  requestAllowance,
  requestAllowanceForUser
} from '@/store/actions/allowanceActions';
import { requestUserDays } from '@/store/actions/userActions';
import { setSelectedDate } from '@/store/actions/yearActions';
import {
  getAllowance,
  getRouter,
  getSearchedEmployee,
  getSelectedDate,
  getUserDays,
  getWindowWidth
} from '@/store/selectors';
import Requests from '@/pages/requests/components/Requests';
import { createMonthsList } from '@/pages/dashboard/components/helpers';
import { RequestCarousel } from '@/components/common/RequestCarousel';

class RequestsYearHandler extends Component {
  currentYear = moment().year();
  yearsWithAllowance = this.props.yearsWithAllowance;
  selectedYear = selectFromYearArray(
    this.currentYear,
    this.yearsWithAllowance,
    'year'
  );
  selectValue = (type, year) =>
    selectFromYearArray(
      year || this.currentYear,
      this.yearsWithAllowance,
      type
    );

  state = {
    monthsWithAllowance: createMonthsList(this.yearsWithAllowance),
    selectedYear: this.selectValue('year'),
    calendar: createYearCalendar(
      this.selectValue('year'),
      this.props.publicHolidays,
      this.props.userDays
    ),
    publicHolidaysHover: false
  };

  componentDidMount() {
    this.props.requestAllowanceForUser({
      year: this.selectValue('year', this.currentYear),
      userId: _get(this.props.searchedEmployee, 'id')
    });

    this.props.setSelectedDate(this.state.selectedYear);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.userDays !== this.props.userDays) {
      this.renderYearCalendar(this.state.selectedYear);
    }

    if (prevProps.searchedEmployee !== this.props.searchedEmployee) {
      this.props.requestAllowanceForUser({
        year: this.selectValue('year', this.currentYear),
        userId: _get(this.props.searchedEmployee, 'id')
      });
    }

    if (prevProps.yearsWithAllowance !== this.props.yearsWithAllowance) {
      this.setState({
        selectedYear: this.selectValue('year', this.currentYear)
      });
    }
  }

  renderYearCalendar = selectedYear => {
    this.setState(prevState => ({
      ...prevState,
      selectedYear: selectedYear,
      calendar: createYearCalendar(
        selectedYear,
        this.props.publicHolidays,
        this.props.userDays
      )
    }));
  };

  mouseMovementHandler = hover => {
    this.setState({
      publicHolidaysHover: hover
    });
  };

  refreshCalendarData = (year, userId) => {
    this.props.requestUserDays({
      year,
      userId
    });
    this.props.requestAllowanceForUser({
      year,
      userId
    });
  };

  handleYearChange = selectedYear => {
    this.refreshCalendarData(
      selectedYear,
      _get(this.props.searchedEmployee, 'id')
    );
    this.props.setSelectedDate(selectedYear);
    this.renderYearCalendar(selectedYear);
  };

  render() {
    const { pathname } = this.props.router.location;
    const { selectedYear, monthsWithAllowance } = this.state;
    const { yearsWithAllowance } = this.props;

    return (
      <div className="px-4 d-flex flex-column position-relative mb-2">
        <RequestCarousel
          path={pathname}
          selectedYear={selectedYear}
          yearsWithAllowance={yearsWithAllowance}
          monthsWithAllowance={monthsWithAllowance}
          handleYearChange={this.handleYearChange}
        />
        <div className="flex-column">
          <Requests selectedYear={selectedYear} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  allowance: getAllowance(state),
  searchedEmployee: getSearchedEmployee(state),
  windowWidth: getWindowWidth(state),
  userDays: getUserDays(state),
  router: getRouter(state),
  selectedDate: getSelectedDate(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestAllowance,
      requestUserDays,
      requestAllowanceForUser,
      setSelectedDate
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestsYearHandler);
