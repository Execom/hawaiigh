import { replace } from 'connected-react-router';
import dotenv from 'dotenv';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MainLogo } from '../../components/common/mainLogo';
import ExecomLogo from '../../img/execom_logo.png';
import { clearAuthorization } from '../../store/actions/authorizationActions';
import { clearStore } from '../../store/actions/clearStoreActions';
import { closeDrawer } from '../../store/actions/drawerActions';
import { requestToken } from '../../store/actions/getTokenActions';
import { removeRedirectAfterLoginRoute } from '../../store/actions/redirectAfterLoginActions';
import { clearUser } from '../../store/actions/userActions';
import { toastrError } from '../../store/sagas/helpers/toastrHelperSaga';
import {
  getAuthorization,
  getMenuDrawer,
  getRedirectAfterLoginRoute
} from '../../store/selectors';
import store from '../../store/store';
import { LoginButton, LoginContainer, LogoContainer } from './styled';
import { getBackendLoginUrl } from '@/helpers';
import queryString from 'query-string';

dotenv.config();

class Login extends Component {
  componentDidMount = () => {
    if (this.props.location.state && this.props.location.state.logout) {
      localStorage.removeItem('token');
      this.props.clearStore();
      this.props.clearAuthorization();
      this.props.clearUser();
      this.props.history.replace('/login', {
        logout: false
      });
    }

    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.getRegistrations().then(function(registrations) {
        for (let registration of registrations) {
          registration.unregister();
        }
      });
    }

    caches.keys().then(cacheKeys => {
      cacheKeys.forEach(cache => caches.delete(cache));
    });

    if (this.props.menuDrawerOpen) {
      this.props.closeDrawer();
    }

    if (this.props.location.search) {
      const values = queryString.parse(this.props.location.search);
      if (values.error) {
        this.props.toastrError(values.error);
      }
    }
  };

  componentDidUpdate() {
    if (this.props.authorization) {
      store.dispatch(
        replace(this.props.redirectAfterLoginRoute || '/dashboard')
      );

      if (this.props.redirectAfterLoginRoute) {
        store.dispatch(removeRedirectAfterLoginRoute());
      }
    }
  }

  render() {
    return (
      <LoginContainer className="pb-5 d-flex justify-content-center align-items-end">
        <LogoContainer className="text-center rounded py-3">
          <MainLogo>
            <p>Hawaii</p>
            <span>
              <img src={ExecomLogo} alt="execom" />
              HR tool
            </span>
          </MainLogo>
          <LoginButton href={getBackendLoginUrl()}>Log in</LoginButton>
        </LogoContainer>
      </LoginContainer>
    );
  }
}

const mapStateToProps = state => ({
  authorization: getAuthorization(state),
  redirectAfterLoginRoute: getRedirectAfterLoginRoute(state),
  menuDrawerOpen: getMenuDrawer(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestToken,
      clearAuthorization,
      clearUser,
      removeRedirectAfterLoginRoute,
      closeDrawer,
      toastrError,
      clearStore
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
