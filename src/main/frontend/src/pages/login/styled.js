import styled from 'styled-components';
import HawaiiWallpaper from '../../img/hawaii_wallpaper.jpg';

export const LoginButton = styled.a`
  color: white;
  padding: 10px 20px 10px 20px;
  background: #fa4c50;
  display: inline-block;
  font-weight: 600;
  border-radius: 10px;

  &:hover {
    color: #fff;
    text-decoration: none;
  }
`;

export const LoginContainer = styled.div`
  background: url(${HawaiiWallpaper});
  height: 100%;
  width: 100%;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: center;
`;

export const LogoContainer = styled.div`
  max-width: 300px;
  width: 300px;
  background-color: rgba(50, 50, 52, 0.7);
  border: 2px solid rgba(50, 50, 52);
`;
