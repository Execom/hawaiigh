import { injectGlobal } from 'styled-components';

injectGlobal`
@import url('https://fonts.googleapis.com/css?family=Montserrat');
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');

*:not(input):not(textarea){
  user-select: none;	
}
    
@supports (-webkit-overflow-scrolling: touch) {
  *{
    user-select: initial;	
  }

  body {
    &.modal-open {
      bottom: 0;
      left: 0;
      position: fixed;
      right: 0;
      top: 0;
    }
  }
}

html, body {
  margin: 0;
  padding: 0;
  display: flex;
  height: 100%;
  flex-grow: 1;
}

body {	
  font-family: 'Montserrat', sans-serif;
  -webkit-font-smoothing: antialiased;
}

#root {
  width: 100%;
  height: 100%;
}

.root-wrapper {
  display: flex;
  flex-direction: column;
  width: 100%;

  .nav{
    background-color: rgb(50, 50, 52);
    width: 100%;
    height: 100px;
  }
}

aside a {	
  text-decoration: none;	
  display: block;
  transition: 0.2s ease-in-out;

  span{
    padding-bottom: 5px;
    position: relative;

    &:after {
      content: '';
      width: 80%;
      border-bottom: 2px solid #dc3545;
      position: absolute;
      right: 0;
      bottom: 0;
      left: 0;
      margin: auto;
      transform: scaleX(0);
      transition: all 0.3s ease-in;

      @media (max-width: 992px) {
        display: none;
      }
    }
  }

  &:hover,	
  &:focus,	
  &:active {	
    text-decoration: none;	
  }	
  &.active {
    span {
      &:after {
        transform: scaleX(1);
      }
    }

    @media (max-width: 992px) {
      background: #E45052;
    }
  }
}	
	
button {	
  border: none;	
  
  &:hover,	
  &:focus,	
  &:active {	
    outline: none;	
  }	
}	
	
img {	
  max-width: 100%;	
}
	
textarea {	
  resize: none;	
  overflow-y: hidden;	
  
  &:focus,	
  &:active {	
    outline: none;	
  }	
}	

select {
  &:focus,	
  &:active {	
    outline: none;	
  }	
}

input {	
  &:focus,	
    &:active {	
        outline: none;	
    }	
}	

.react-datepicker-wrapper {
  width: 100%;
  
  div, input {
      width: 100%;
  }
}

InfiniteScroll {
  height: 70vh;
  overflow: auto;
}

// TODO: Move styling to the Radio Component

.radio {
  margin: 0.5rem;
  input[type="radio"] {
    position: absolute;
    opacity: 0;
    + .radio-label {
      &:before {
        content: '';
        background: #f4f4f4;
        border-radius: 100%;
        border: 1px solid darken(#f4f4f4, 25%);
        display: inline-block;
        width: 1.4em;
        height: 1.4em;
        position: relative;
        top: -0.2em;
        margin-right: 1em; 
        vertical-align: top;
        cursor: pointer;
        text-align: center;
        transition: all 250ms ease;
      }
    }
    &:checked {
      + .radio-label {
        &:before {
          background-color: #3197EE;
          box-shadow: inset 0 0 0 4px #f4f4f4;
        }
      }
    }
    &:focus {
      + .radio-label {
        &:before {
          outline: none;
          border-color: #3197EE;
        }
      }
    }
    &:disabled {
      + .radio-label {
        &:before {
          box-shadow: inset 0 0 0 4px #f4f4f4;
          border-color: darken(#f4f4f4, 25%);
          background: darken(#f4f4f4, 25%);
        }
      }
    }
    + .radio-label {
      &:empty {
        &:before {
          margin-right: 0;
        }
      }
    }
  }
}

.modal-xl-custom { 
  @media (min-width: 991px){
    max-width: 850px;
  }
}

div.allowance-group {
  display: flex;
  justify-content: space-between;
  margin-bottom: 4px;
}

div.filter-group {
  display: flex;
  margin-top: 6px;
  z-index: 0;
}

select.drop-down {
  margin-right: 4px;
}

@media (max-width: 768px){
  div.allowance-group {
    flex-direction: column;
    align-items: center;
  }
}

td {
  overflow: hidden;
}

button.radio-button-custom {
  padding: 0px 7px;
  cursor: pointer;
  color: black;
  background: transparent;
}

button.active-tab {
  border-bottom: 2px solid #dc3545;
  padding-bottom: 4px;
}

.holiday-img {
  width: 17px;
}

.select-custom {
  min-height: 20px;
  cursor: pointer;
  color: black;
  background: transparent;
  border-radius: 0;
  appearance:none;
  z-index: 1;
  position: relative;
  border: 1px solid darkgray;

  &.select-request {
    border: none;
    border-bottom: 1px solid;
    width: 100%;
  }

  &.select-with-categories {
    span[role="option"] {
      padding-left: 20px;

      &.react-dropdown-select-item-disabled {
        padding-left: 10px;
        
        ins {
          display: none;
        }
        
        &:hover {
          background: #f2f2f2;
          cursor: auto;
        }
      }
    }
  }

  &.select-year {
    width: auto;
  }

  &.select-lg {
    width: 100%;
  }

  &:focus, &:focus-within {
    box-shadow: none!important;
    cursor: pointer;
    outline: none;
  }
  
  input {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    margin-left: 0;

    &::placeholder {
      color: black;
    }
  }
}

.debounce-input-custom {
  font-size: 14px;
  border-bottom: 1px solid #a8a8a8;
  font-size: 14px;
  background: transparent;
  border-radius: 0;
}

.search-bar-custom {
  width: 100%;
  padding: 2px 5px;
  border: none;
  border-bottom: 1px solid #d3d3d3;
  background: transparent;
}

.table {
  border-collapse: separate;
}

.ellipsis-text {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

.modal-content {
  @media (max-width: 992px) {
    .max-height {
      max-height: inherit;
      
      .modal-body {
        overflow: auto;
      }
    }
  }
}

.d-table-row-group {
  display: table-row-group;
}

div.swipe-area {
  z-index: 2;

  @media (min-width: 992px) {
      display: none;
  }
}

.overflow {
  &-hidden {
    overflow: hidden;
  }

  &-auto {
    overflow: auto;
  }
}

.table-content {
  height: 0;
  overflow-x: auto;
}

div.MuiDrawer-root.MuiDrawer-modal {
  z-index: 1049 !important;
}

div.drawer {
  @media (max-width: 550px) {
    width: 90%;
  }
}

.cursor-pointer {
  cursor: pointer;
}

.table-enter {
  opacity: 0;

  &-active {
    opacity: 1;
    transition: opacity 300ms, transform 300ms;
  }
}

.table-exit {
  opacity: 1;
  
  &-active {
    opacity: 0;
    transition: opacity 300ms, transform 300ms;
  }
}

.data-export-container {
  @media (min-width: 550px) {
    margin-left: 5rem !important;
    margin-right: 5rem !important;
  }
  @media (min-width: 992px) {
    margin-left: 17rem !important;
    margin-right: 17rem !important;
  }
  @media (min-width: 1300px) {
    margin-left: 22rem !important;
    margin-right: 22rem !important;
  }
}

.fixed-select-width {
  width: 100px;
}

div.request-button {
  padding-right: 0px;

  @media (max-width: 991px) {
    padding-right: 8px;
  }
}

div.table-header {
  text-align: center;
  fontSize: 15px;
  fontColor: '#212529';
  padding: 25px;
  font-weight: 700;

  @media (max-width: 1095px) {
    font-size: 15px;
  }
}

div.timerange-selector {
  margin-left: auto;
}

img.time {
  max-width: 21px;
  margin-right: 3px;
}

select.no-border {
  border: none;
  margin-right: 24px;
}

div.date-picker {
  display: flex;
}

.dropbtn {
  font-size: 16px;
  padding: 4px 0;
  cursor: pointer;
  margin-right: 23px;
  width: 162px;
  z-index: 2;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  padding: 10px;
  border: solid 1px gray;
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  right: -1px;
}

.show-timepicker {
  display: flex;
}

select.hours {
  margin-left: 30px;
}

select.minutes {
  margin-left: 2px;
}

div.admin-table-header {
  display: flex;
}

td.delete-admin-cell{
  text-align: right;
}

.calendar-day-heading {
  height: 22px;
}

div.sort-direction {
  font-size: 30px;
  height: fit-content;
  padding-top: 21px;
  margin-left: 5px;
}
`;
