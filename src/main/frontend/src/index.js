import en from 'date-fns/locale/en-GB';
import React from 'react';
import { registerLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import 'react-datepicker/dist/react-datepicker.css';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ReduxToastr from 'react-redux-toastr';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';
import 'reset-css/reset.css';
import App from './App';
import ModalWrapper from './components/modal/ModalWrapper';
import registerServiceWorker from './registerServiceWorker';
import store from './store/store';
import './styles/globalStyles.js';
import styled from 'styled-components';
registerLocale('en-GB', en);

const AppWrapper = styled.div`
  max-width: 100vw;
`;

ReactDOM.render(
  <Provider store={store}>
    <AppWrapper className="d-flex flex-grow-1 flex-column h-100">
      <ReduxToastr
        timeOut={4000}
        newestOnTop={false}
        preventDuplicates
        position="bottom-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        progressBar
        duration={4000}
        closeOnToastrClick
      />
      <ModalWrapper />
      <App />
    </AppWrapper>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
