import { ConnectedRouter } from 'connected-react-router';
import _debounce from 'lodash/debounce';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import Panel from './layout/Panel';
import Login from './pages/login/Login';
import { requestTokenFromStorage } from './store/actions/getTokenFromSessionStorageActions';
import { closeModal } from './store/actions/modalActions';
import { setWindowWidth } from './store/actions/responsiveActions';
import { getModal, getRouter, getAuthorization } from './store/selectors';
import { history } from './store/store';
import applyTokenFromUrl from '@/helpers/applyTokenFromUrl';

class App extends Component {
  componentDidMount() {
    applyTokenFromUrl();
    this.props.requestTokenFromStorage();
    this.props.setWindowWidth(window.innerWidth);

    window.addEventListener(
      'resize',
      _debounce(() => this.props.setWindowWidth(window.innerWidth), 300)
    );
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.modalOpen &&
      !this.props.router.location.search &&
      prevProps.router.location.search
    ) {
      this.props.closeModal();
    }
  }

  render() {
    return (
      <ConnectedRouter history={history}>
        <Switch>
          <Route exact path="/login" component={Login} />
          {this.props.authorization ? (
            <Route path="/" component={Panel} />
          ) : (
            <Redirect to="/login" />
          )}
        </Switch>
      </ConnectedRouter>
    );
  }
}

const mapStateToProps = state => ({
  modalOpen: getModal(state).open,
  router: getRouter(state),
  authorization: getAuthorization(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestTokenFromStorage,
      setWindowWidth,
      closeModal
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
