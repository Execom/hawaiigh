package eu.execom.hawaii.batch;

import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.repository.AllowanceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class AllowanceDbWriter implements ItemWriter<Allowance> {

  private AllowanceRepository allowanceRepository;

  public AllowanceDbWriter(AllowanceRepository allowanceRepository) {
    this.allowanceRepository = allowanceRepository;
  }

  @Override
  public void write(List<? extends Allowance> importedAllowances) throws Exception {
    allowanceRepository.saveAll(importedAllowances);
  }

}
