package eu.execom.hawaii.batch;

import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.model.batch.AllowanceImportData;
import eu.execom.hawaii.repository.AllowanceRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.repository.YearRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Optional;

@Slf4j
@Component
public class AllowanceProcessor implements ItemProcessor<AllowanceImportData, Allowance> {

  private static final int HALF_DAY = 4;
  private static final double MODULUS_BOUNDARY = 2;
  private static final MonthDay DEFAULT_CARRIED_OVER_EXPIRATION = MonthDay.of(3, 31);

  private UserRepository userRepository;
  private YearRepository yearRepository;
  private AllowanceRepository allowanceRepository;

  public AllowanceProcessor(
      UserRepository userRepository,
      YearRepository yearRepository,
      AllowanceRepository allowanceRepository) {
    this.yearRepository = yearRepository;
    this.userRepository = userRepository;
    this.allowanceRepository = allowanceRepository;
  }

  @Override
  public Allowance process(AllowanceImportData allowanceImportData) {

    Optional<User> user = userRepository.findByEmail(allowanceImportData.getEmail());
    if (user.isEmpty()) {
      return null;
    }

    if (!isWholeYearAllowance(allowanceImportData)) {
      log.info("Import of allowance ignored for employee: '{}', with email: '{}' in time period: '{}'.",
          allowanceImportData.getEmployeeName(), allowanceImportData.getEmail(),
          allowanceImportData.getAllowanceName());
      return null;
    }

    Year year = getYearFromImport(allowanceImportData);
    Allowance allowance = createAllowanceFromImport(allowanceImportData, user.get(), year);
    log.debug("Converting '{}' into '{}'.", allowanceImportData, allowance);

    return allowance;
  }

  /*
   * Only allowances for the whole year will be imported.
   * (There is only one allowance assigned for less than a year in a whole
   * export file, which can only be qualified as human error. Nevertheless,
   * this employee has another allowance for that year assigned which
   * includes allowance for that whole year so there will be no lost data.)
   */
  private boolean isWholeYearAllowance(AllowanceImportData allowanceImportData) {
    String[] allowanceName = allowanceImportData.getAllowanceName().split(" ");
    String startMonth = allowanceName[0];
    String endMonth = allowanceName[3];

    return (is(startMonth, Month.JANUARY) && is(endMonth, Month.DECEMBER)) || (is(startMonth, Month.FEBRUARY) && is(
        endMonth, Month.JANUARY));
  }

  private boolean is(String monthName, Month month) {
    return monthName.equalsIgnoreCase(month.getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
  }

  private Year getYearFromImport(AllowanceImportData allowanceImportData) {
    int yearFromImport = Integer.parseInt(allowanceImportData.getAllowanceName().split(" ")[1]);
    Year year = yearRepository.findOneByYear(yearFromImport);

    return year == null ? createYear(yearFromImport) : year;
  }

  private Year createYear(int yearFromImport) {
    LocalDate carriedOverExpirationDate = LocalDate.of(yearFromImport, DEFAULT_CARRIED_OVER_EXPIRATION.getMonth(),
        DEFAULT_CARRIED_OVER_EXPIRATION.getDayOfMonth());

    return yearRepository.save(new Year(yearFromImport, true, carriedOverExpirationDate));
  }

  private Allowance createAllowanceFromImport(AllowanceImportData allowanceImportData, User user, Year year) {

    Allowance allowance = allowanceRepository.findByUserIdAndYearYear(user.getId(), year.getYear());
    if (allowance == null) {
      allowance = new Allowance();
    }
    allowance.setAnnual(roundToHalfDayPrecision(allowanceImportData.getLeaveAllocatedHours()));
    allowance.setPendingAnnual((roundToHalfDayPrecision(allowanceImportData.getLeavePendingHours())));
    allowance.setTakenAnnual(roundToHalfDayPrecision(allowanceImportData.getLeaveApprovedHours()));

    allowance.setApprovedBonus(roundToHalfDayPrecision(allowanceImportData.getLeaveToilComputedHours()));
    allowance.setBonusManualAdjust(roundToHalfDayPrecision(allowanceImportData.getLeaveToilAdjustHours()));

    allowance.setTraining(roundToHalfDayPrecision(allowanceImportData.getCustom1AllocatedHours()));
    allowance.setTakenTraining(roundToHalfDayPrecision(allowanceImportData.getCustom1TakenHours()));
    allowance.setTrainingManualAdjust(roundToHalfDayPrecision(allowanceImportData.getCustom1AdjustHours()));

    allowance.setSickness(roundToHalfDayPrecision(allowanceImportData.getSickApprovedHours()));
    allowance.setCarriedOver(roundToHalfDayPrecision(allowanceImportData.getLeaveCarryOverHours()));
    allowance.setManualAdjust(roundToHalfDayPrecision(allowanceImportData.getLeaveAdjustHours()));

    allowance.setUser(user);
    allowance.setYear(year);
    allowance.setBonus(user.getLeaveProfile().getMaxBonusDays());

    handleExpiredCarriedOverHours(allowanceImportData, allowance);
    manuallyAdjustIncorrectValues(allowanceImportData, allowance);

    return allowance;
  }

  /*
   *  As some users have expired carried over hours and we don't have information about
   *  their eventual amount, we have to check if any user have them and calculate exact
   *  amount of those hours so they could be subtracted from users carried over hours.
   *  Otherwise, those hours would be on users disposal to use them as they have never
   *  expired.
   */
  private void handleExpiredCarriedOverHours(AllowanceImportData allowanceImportData, Allowance allowance) {
    if (allowanceImportData.getLeaveCarryOverHours() < MODULUS_BOUNDARY) {
      return;
    }

    int totalRemainingAllowance = getTotalRemainingAllowance(allowance);
    int totalRemainingAllowanceFromImport = roundToHalfDayPrecision(allowanceImportData.getLeaveRemainingIncPending());

    if (totalRemainingAllowance > totalRemainingAllowanceFromImport) {
      int expiredCarriedOverHours = totalRemainingAllowance - totalRemainingAllowanceFromImport;
      allowance.setExpiredCarriedOver(expiredCarriedOverHours);
    }
  }

  /*
   * Removes any possibility of error in calculation and makes sure imported data
   * is correct.
   */
  private void manuallyAdjustIncorrectValues(AllowanceImportData allowanceImportData, Allowance allowance) {

    int calculatedRemainingAllowance = getTotalRemainingAllowance(allowance) - allowance.getExpiredCarriedOver();
    int remainingAllowanceFromImport = roundToHalfDayPrecision(allowanceImportData.getLeaveRemainingIncPending());

    if (calculatedRemainingAllowance != remainingAllowanceFromImport) {
      var difference = calculatedRemainingAllowance - remainingAllowanceFromImport;
      allowance.setManualAdjust(allowance.getManualAdjust() - difference);
    }
  }

  private int getTotalRemainingAllowance(Allowance allowance) {
    return
        allowance.getAnnual() + allowance.getCarriedOver() + allowance.getManualAdjust() + allowance.getApprovedBonus()
            + allowance.getBonusManualAdjust() - allowance.getPendingAnnual() - allowance.getTakenAnnual();
  }

  /**
   * Handles calculation of allowance values properly so it could be divisible by 4, which
   * represents half day expressed in hours. Half day is the smallest amount which can be
   * requested by final user, so there is no point in having excess amount of hours at users
   * disposal, when he can't use it. Users allowance will be slightly increased or decreased.
   */
  private int roundToHalfDayPrecision(double importedHours) {
    double modulus = importedHours % HALF_DAY;
    return modulus == 0 ? (int) importedHours : getCalculatedHours(importedHours);
  }

  private int getCalculatedHours(double importedHours) {
    return importedHours < 0 ? calculateNegativeValue(importedHours) : calculateHours(importedHours);
  }

  private int calculateNegativeValue(double importedHours) {
    return calculateHours(Math.abs(importedHours)) * -1;
  }

  private int calculateHours(double importedHours) {
    double modulus = importedHours % HALF_DAY;
    double result = modulus < MODULUS_BOUNDARY ?
        roundToLowerValue(importedHours, modulus) :
        roundToHigherValue(importedHours, modulus);

    return (int) Math.round(result);
  }

  private double roundToLowerValue(double importedHours, double modulus) {
    return importedHours - modulus;
  }

  private double roundToHigherValue(double importedHours, double modulus) {
    return importedHours + HALF_DAY - modulus;
  }
}
