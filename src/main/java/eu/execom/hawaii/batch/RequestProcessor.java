package eu.execom.hawaii.batch;

import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.batch.CalendarImportData;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.Duration;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import eu.execom.hawaii.repository.RequestRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.execom.hawaii.util.RequestImportConstants.AFTERNOON;
import static eu.execom.hawaii.util.RequestImportConstants.NOON;

@Slf4j
@Component
public class RequestProcessor implements ItemProcessor<CalendarImportData, Request> {

  private final List<String> skippedFullNames = new ArrayList<>();

  private UserRepository userRepository;
  private AppogeeLeaveProcessor appogeeLeaveProcessor;
  private RequestRepository requestRepository;
  private EmailService emailService;

  public RequestProcessor(UserRepository userRepository, AppogeeLeaveProcessor appogeeLeaveProcessor,
      RequestRepository requestRepository, EmailService emailService) {
    this.userRepository = userRepository;
    this.requestRepository = requestRepository;
    this.appogeeLeaveProcessor = appogeeLeaveProcessor;
    this.emailService = emailService;
  }

  @Override
  public Request process(CalendarImportData calendarImportData) {
    /**
     * If request includes half days we need to update days duration (MORNING or AFTERNOON) because this information
     * is not available in Appogee files. Request that includes only full days will not be updated.
     */
    if (!calendarImportData.isAllDayEvent()) {
      String fullName = getUsersFullName(calendarImportData);
      Optional<List<User>> users = userRepository.findAllByFullName(fullName);

      if (users.isEmpty() || skippedFullNames.contains(fullName)) {
        return null;
      } else if (users.get().size() > 1) {
        skippedFullNames.add(fullName);
        log.warn("Multiple users with name '{}' detected, request import will not occur.", fullName);
        emailService.sendCalendarImportOmittedDueToSameFullName(users.get().get(0));
        return null;
      }

      User user = users.get().get(0);
      List<Day> days = createDaysFromImport(calendarImportData, user.getEmail());
      LocalDate startDate = calendarImportData.getEventStart().toLocalDate();
      LocalDate endDate = calendarImportData.getEventEnd().toLocalDate();
      List<Request> requests = findRequest(calendarImportData, user, days);

      if (requests.isEmpty()) {
        log.warn("Calendar request for user '{}' for period from '{}' to '{}' not found in database as request "
            + "that includes half days.",
            user.getEmail(), startDate, endDate);
        emailService.sendCalendarImportOmittedDueToNotFoundInDatabase(user, calendarImportData);
        return null;
      }

      if (requests.size() > 1) {
        log.warn("Could not uniquely identify request for user '{}' for period from '{}' to '{}', "
            + "request will not be updated.", user.getEmail(), startDate, endDate);
        emailService.sendCalendarImportOmittedDueToNotUniquelyIdentifiable(user, calendarImportData);
        return null;
      }

      Request request = requests.get(0);
      int calendarHours = days.stream().mapToInt(day -> day.getDuration().getHours()).sum();
      int dbHours = request.getDays().stream().mapToInt(day -> day.getDuration().getHours()).sum();
      if (calendarHours == dbHours) {
        request.getDays().forEach(day -> day.setDuration(getDuration(calendarImportData, day.getDate())));
      } else {
        log.error("Calculated calendar request hours '{}' didn't match with existing hours in database '{}', "
            + "request update will not occur for user '{}' for period from '{}' to '{}'.", calendarHours, dbHours,
            user.getEmail(), startDate, endDate);
        emailService.sendCalendarImportOmittedDueToHoursMismatch(user, calendarImportData);
        return null;
      }

      return request;
    } else {
      return null;
    }
  }

  private List<Request> findRequest(CalendarImportData calendarImportData, User user, List<Day> days) {

    List<AbsenceType> absenceTypes = getAbsenceType(calendarImportData);
    List<LocalDate> dates = days.stream().map(Day::getDate).collect(Collectors.toList());

    /**
     * First try to find request by using all available data for filtering
     */
    List<Request> requests = requestRepository.findAllHavingHalfDays(List.of(RequestStatus.APPROVED), absenceTypes,
        user.getEmail(), dates, calendarImportData.getReason());

    /**
     * In some special cases, request reason is not the same as in Appogee, so will try to find it
     * without reason, this will however increase the chance of getting more then one result
     */
    if (requests.isEmpty()) {
      requests = requestRepository.findAllHavingHalfDays(List.of(RequestStatus.APPROVED), absenceTypes,
          user.getEmail(), dates, null);
    }

    /**
     * In some special cases, calendar can contain requests that are not strictly approved
     */
    if (requests.isEmpty()) {
      requests = requestRepository.findAllHavingHalfDays(RequestStatus.nonApprovedRequests(), absenceTypes,
          user.getEmail(), dates, null);
    }

    return requests;
  }

  private String getUsersFullName(CalendarImportData calendarImportData) {
    return calendarImportData.getEventTitle().split(":")[1].trim();
  }

  private List<AbsenceType> getAbsenceType(CalendarImportData calendarImportData) {
    String absenceTypeGroup = calendarImportData.getEventTitle().split(":")[0].trim();
    String absenceName = !calendarImportData.getEventDescription().isEmpty() ?
        calendarImportData.getEventDescription().split(":")[1].trim() : "";

    if (absenceTypeGroup.equals("Sick")) {
      return Collections.singletonList(AbsenceType.SICKNESS);
    }

    return AbsenceType.from(absenceName);
  }

  private List<Day> createDaysFromImport(CalendarImportData calendarImportData, String email) {
    return calendarImportData.getEventStart()
                             .toLocalDate()
                             .datesUntil(calendarImportData.getEventEnd().toLocalDate().plusDays(1))
                             .filter(date -> appogeeLeaveProcessor.isWorkDay(date, email))
                             .map(date -> getDay(calendarImportData, date))
                             .collect(Collectors.toList());
  }

  private Day getDay(CalendarImportData calendarImportData, LocalDate date) {
    Day day = new Day();
    day.setDate(date);
    day.setDuration(getDuration(calendarImportData, date));
    return day;
  }

  private Duration getDuration(CalendarImportData calendarImportData, LocalDate currentDate) {
    LocalDateTime startDateTime = calendarImportData.getEventStart();
    LocalDateTime endDateTime = calendarImportData.getEventEnd();
    LocalDate startDate = startDateTime.toLocalDate();
    LocalDate endDate = endDateTime.toLocalDate();

    if (calendarImportData.isAllDayEvent()) {
      return Duration.FULL_DAY;
    } else if (startDate.equals(endDate)) {
      boolean isRequestFullDay = java.time.Duration.between(startDateTime, endDateTime).toHours() == 8;
      if (isRequestFullDay) {
        return Duration.FULL_DAY;
      } else {
        return startDateTime.getHour() >= NOON ? Duration.AFTERNOON : Duration.MORNING;
      }
    } else if (startDate.equals(currentDate)) {
      return startDateTime.getHour() < NOON ? Duration.FULL_DAY : Duration.AFTERNOON;
    } else if (endDate.equals(currentDate)) {
      return endDateTime.getHour() <= AFTERNOON ? Duration.MORNING : Duration.FULL_DAY;
    } else {
      return Duration.FULL_DAY;
    }
  }

}
