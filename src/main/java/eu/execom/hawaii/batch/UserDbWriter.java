package eu.execom.hawaii.batch;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class UserDbWriter implements ItemWriter<User> {

  private UserRepository userRepository;

  public UserDbWriter(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public void write(List<? extends User> importedUsers) {
    userRepository.saveAll(importedUsers);
  }

}
