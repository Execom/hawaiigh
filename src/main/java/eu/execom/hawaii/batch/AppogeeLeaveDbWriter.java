package eu.execom.hawaii.batch;

import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.batch.RequestImportData;
import eu.execom.hawaii.repository.RequestRepository;
import eu.execom.hawaii.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AppogeeLeaveDbWriter implements ItemWriter<Request> {

  private RequestRepository requestRepository;
  private UserRepository userRepository;

  public AppogeeLeaveDbWriter(RequestRepository requestRepository, UserRepository userRepository) {
    this.requestRepository = requestRepository;
    this.userRepository = userRepository;
  }

  @Override
  public void write(List<? extends Request> importedRequests) {
    var usersWithRequestsInDb = userRepository.findByRequestsNotEmpty();
    var usersWithRequestsInCsv = importedRequests.stream()
                                                 .map(Request::getUser)
                                                 .distinct()
                                                 .collect(Collectors.toList());

    var currentRequests = requestRepository.findAll();
    var currentRequestsFormatted = mapToRequestImportData(currentRequests);
    var importedRequestsFormatted = mapToRequestImportData(importedRequests);

    /**
     * Request removal conditions:
     * 1. Request user must be in both request table in DB and in request CSV (we have to clean all previous requests
     * for these users in order to import new requests because there has been changes since previous import)
     * 2. Request in DB must not be totally the same as in CSV file
     */
    var requestsForRemoval = currentRequests.stream()
                                            .filter(isDeprecated(usersWithRequestsInDb, usersWithRequestsInCsv)
                                            .and(isNewRequest(importedRequestsFormatted)))
                                            .collect(Collectors.toList());
    /**
     * Request addition conditions:
     * 1. Request in CSV file must not be totally the same as in DB
     */
    var requestsForAddition = importedRequests.stream()
                                              .filter(isNewRequest(currentRequestsFormatted))
                                              .collect(Collectors.toList());

    requestRepository.deleteAll(requestsForRemoval);
    requestRepository.saveAll(requestsForAddition);
  }

  private List<RequestImportData> mapToRequestImportData(List<? extends Request> requests) {
    return requests.stream()
                   .map(RequestImportData::new)
                   .collect(Collectors.toList());
  }

  private Predicate<Request> isDeprecated(List<User> usersWithRequestsInDb, List<User> usersWithRequestsInCsv) {
    return request -> usersWithRequestsInDb.contains(request.getUser()) &&
                      usersWithRequestsInCsv.contains(request.getUser());
  }

  private Predicate<Request> isNewRequest(List<RequestImportData> requests) {
    return request -> !requests.contains(new RequestImportData(request));
  }
}
