package eu.execom.hawaii.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;

import java.io.File;

@Slf4j
public class FileDeletingTasklet implements Tasklet {

  private Resource resource;

  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

    if (resource.exists()) {
      File file = resource.getFile();
      boolean deleted = file.delete();

      if (!deleted) {
        log.debug("Could not delete file '{}' on path '{}'.", file.getName(), file.getPath());
        throw new UnexpectedJobExecutionException("Could not delete file " + file.getPath());
      }
    }

    return RepeatStatus.FINISHED;
  }

  public void setResource(Resource resource) {
    this.resource = resource;
  }
}