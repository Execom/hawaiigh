package eu.execom.hawaii.batch;

import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.batch.UserImportData;
import eu.execom.hawaii.model.enumerations.LeaveProfileType;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.LeaveProfileRepository;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
public class UserProcessor implements ItemProcessor<UserImportData, User> {

  private static final Map<String, LeaveProfileType> LEAVE_PROFILE_TYPES = new HashMap<>();
  private static final Map<String, UserStatusType> USER_TYPES = new HashMap<>();
  private static final Map<String, UserRole> USER_ROLE = new HashMap<>();

  private TeamRepository teamRepository;
  private LeaveProfileRepository leaveProfileRepository;
  private UserRepository userRepository;

  public UserProcessor(
      TeamRepository teamRepository,
      LeaveProfileRepository leaveProfileRepository,
      UserRepository userRepository) {
    this.teamRepository = teamRepository;
    this.leaveProfileRepository = leaveProfileRepository;
    this.userRepository = userRepository;
  }

  @PostConstruct
  public void init() {
    LEAVE_PROFILE_TYPES.put("Employees (3 days working)", LeaveProfileType.EMPLOYEES_3_DAYS_WORKING);
    LEAVE_PROFILE_TYPES.put("Employees (4 days working)", LeaveProfileType.EMPLOYEES_4_DAYS_WORKING);
    LEAVE_PROFILE_TYPES.put("0 - 5 Years of Service", LeaveProfileType.ZERO_TO_FIVE_YEARS);
    LEAVE_PROFILE_TYPES.put("5 - 10 Years of Service", LeaveProfileType.FIVE_TO_TEN_YEARS);
    LEAVE_PROFILE_TYPES.put("10 - 15 Years of Service", LeaveProfileType.TEN_TO_FIFTEEN_YEARS);
    LEAVE_PROFILE_TYPES.put("15 - 20 Years of Service", LeaveProfileType.FIFTEEN_TO_TWENTY_YEARS);
    LEAVE_PROFILE_TYPES.put("20 - 25 Years of Service", LeaveProfileType.TWENTY_TO_TWENTYFIVE_YEARS);
    LEAVE_PROFILE_TYPES.put("25 - 30 Years of Service", LeaveProfileType.TWENTYFIVE_TO_THIRTY_YEARS);
    LEAVE_PROFILE_TYPES.put("30 - 35 Years of Service", LeaveProfileType.THIRTY_TO_THIRTYFIVE_YEARS);
    LEAVE_PROFILE_TYPES.put("35 - 40 Years of Service", LeaveProfileType.THIRTYFIVE_TO_FORTY_YEARS);

    USER_TYPES.put("active", UserStatusType.ACTIVE);
    USER_TYPES.put("inactive", UserStatusType.INACTIVE);
    USER_TYPES.put("deleted", UserStatusType.DELETED);

    USER_ROLE.put("hr manager", UserRole.HR_MANAGER);
    USER_ROLE.put("user", UserRole.USER);
  }

  @Override
  public User process(UserImportData userImportData) {
    User user = createUserFromImport(userImportData);
    log.debug("Converting '{}' into '{}'.", userImportData, user);

    return user;
  }

  private User createUserFromImport(UserImportData userImportData) {
    LeaveProfileType leaveProfileType = LEAVE_PROFILE_TYPES.get(userImportData.getLeaveProfile());
    UserStatusType userStatusType = USER_TYPES.get(userImportData.getStatus());
    UserRole userRole = USER_ROLE.get(userImportData.getUserRole());

    Optional<User> userOptional = userRepository.findByEmail(userImportData.getEmail());
    User user = userOptional.orElseGet(User::new);
    user.setFullName(userImportData.getFirstName() + " " + userImportData.getLastName());
    user.setEmail(userImportData.getEmail());
    user.setUserRole(userRole);
    user.setJobTitle(userImportData.getJobTitle());
    user.setStartedWorkingDate(userImportData.getContinuousStartDate());
    user.setStartedProfessionalCareerDate(userImportData.getProfessionalStartDate());
    user.setStartedWorkingAtExecomDate(userImportData.getStartDate());
    user.setStoppedWorkingAtExecomDate(userImportData.getEndDate());
    user.setTeam(getTeamByName(userImportData.getTeam()));
    user.setLeaveProfile(getLeaveProfileByLeaveProfileType(leaveProfileType));
    user.setUserStatusType(userStatusType);
    user.setYearsOfService(getYearsOfService(user));

    return user;
  }

  private Team getTeamByName(String teamName) {
    boolean teamExists = teamRepository.existsByName(teamName);
    if (teamExists) {
      return teamRepository.findByName(teamName);
    } else {
      Team team = new Team();
      team.setName(teamName);
      return teamRepository.create(team);
    }
  }

  private LeaveProfile getLeaveProfileByLeaveProfileType(LeaveProfileType leaveProfileType) {
    boolean leaveProfileExists = leaveProfileRepository.existsByLeaveProfileType(leaveProfileType);
    if (leaveProfileExists) {
      return leaveProfileRepository.findOneByLeaveProfileType(leaveProfileType);
    } else {
      LeaveProfile leaveProfile = new LeaveProfile();
      leaveProfile.setLeaveProfileType(leaveProfileType);
      leaveProfile.setName(leaveProfileType.toString());
      return leaveProfileRepository.save(leaveProfile);
    }
  }

  private int getYearsOfService(User user) {
    return Period.between(user.getStartedWorkingDate(), LocalDate.now()).getYears();
  }

}
