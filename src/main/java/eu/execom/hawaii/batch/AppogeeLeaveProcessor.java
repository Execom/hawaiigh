package eu.execom.hawaii.batch;

import eu.execom.hawaii.model.Absence;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.PublicHoliday;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.batch.AppogeeLeaveRequestImportData;
import eu.execom.hawaii.model.enumerations.Duration;
import eu.execom.hawaii.model.enumerations.LeaveProfileType;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.AbsenceRepository;
import eu.execom.hawaii.repository.PublicHolidayRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.execom.hawaii.util.RequestImportConstants.DECEASE_FAMILY_MEMBER;
import static eu.execom.hawaii.util.RequestImportConstants.DESEASE_FAMILY_MEMBER;
import static eu.execom.hawaii.util.RequestImportConstants.FAMILY_MEMBER_WEDDING;
import static eu.execom.hawaii.util.RequestImportConstants.FUNERAL_FAMILY_MEMBER;
import static eu.execom.hawaii.util.RequestImportConstants.SERIOUS_ILLNESS_OF_CLOSE_FAMILY_MEMBER;
import static eu.execom.hawaii.util.RequestImportConstants.WEDDING_OF_FAMILY_MEMBER;

@Slf4j
@Component
public class AppogeeLeaveProcessor implements ItemProcessor<AppogeeLeaveRequestImportData, Request> {

  private static final Map<String, RequestStatus> REQUEST_STATUS = new HashMap<>();
  private static final Map<String, LeaveProfileType> EMPLOYEES_LEAVE_PROFILE_TYPES = new HashMap<>();
  private static List<LocalDate> PUBLIC_HOLIDAY_DATES = new ArrayList<>();
  private static List<Absence> ABSENCES = new ArrayList<>();

  private PublicHolidayRepository publicHolidayRepository;
  private AbsenceRepository absenceRepository;
  private UserRepository userRepository;
  private EmailService emailService;

  public AppogeeLeaveProcessor(PublicHolidayRepository publicHolidayRepository,
      UserRepository userRepository, AbsenceRepository absenceRepository, EmailService emailService) {
    this.publicHolidayRepository = publicHolidayRepository;
    this.userRepository = userRepository;
    this.absenceRepository = absenceRepository;
    this.emailService = emailService;
  }

  @PostConstruct
  public void init() {
    REQUEST_STATUS.put("rejected", RequestStatus.REJECTED);
    REQUEST_STATUS.put("cancelled", RequestStatus.CANCELED);
    REQUEST_STATUS.put("approved", RequestStatus.APPROVED);
    REQUEST_STATUS.put("pending", RequestStatus.PENDING);

    List<PublicHoliday> publicHolidays = publicHolidayRepository.findAll();
    PUBLIC_HOLIDAY_DATES = publicHolidays.stream().map(PublicHoliday::getDate).collect(Collectors.toList());

    ABSENCES = absenceRepository.findAll();
  }

  @Override
  public Request process(AppogeeLeaveRequestImportData leaveImport) {
    Request request = createRequest(leaveImport);
    log.debug("Creating request '{}' from Appogee Leave", leaveImport);

    if (request.getDays().isEmpty()) {
      log.warn("Request for user '{}' for period from '{}' to '{}' does not contain working days."
              + " This request will not be imported.",
          leaveImport.getEmail(),
          leaveImport.getStartDate(),
          leaveImport.getEndDate());
      emailService.sendRequestImportDueToNoWorkingDays(leaveImport);
      return null;
    }

    return request;
  }

  private Request createRequest(AppogeeLeaveRequestImportData leaveImport) {
    Optional<User> user = userRepository.findByEmail(leaveImport.getEmail());
    if (user.isEmpty()) {
      log.warn("User with email: '{}' not found in database, request will not be created.", leaveImport.getEmail());
      return null;
    }
    return createRequestFromImport(leaveImport);
  }

  /**
   * As we need to filter persisted requests that correspond with imported request from Appogee Leave this method
   * checks if imported request days fall between already persisted requests from Google Calendar.
   *
   * @param firstDay of Request.
   * @param lastDay of Request.
   * @param request against which firstDay and lastDay are being checked.
   * @return boolean indicating if firstDay and lastDay fall between first and last day of request.
   */
  public boolean isBetween(LocalDate firstDay, LocalDate lastDay, Request request) {
    var persistedRequestFirstDay = request.getDays().get(0).getDate();
    var persistedRequestLastDay = request.getDays().get(request.getDays().size() - 1).getDate();

    return !persistedRequestFirstDay.isBefore(firstDay) && !persistedRequestLastDay.isAfter(lastDay);
  }

  /**
   * Creation of request based on Appogee leave and sickness requests data
   *
   * @param leaveImport imported request from Appogee Leave.
   * @return created request made from data from leaveImport.
   */
  private Request createRequestFromImport(AppogeeLeaveRequestImportData leaveImport) {
    Optional<User> user = userRepository.findByEmail(leaveImport.getEmail());
    Optional<User> approvedBy = userRepository.findByEmail(leaveImport.getApprovedBy());
    var absence = getAbsence(leaveImport.getType());

    List<Day> requestedDays = createDaysFromImport(leaveImport);

    Request importedRequest = new Request();
    user.ifPresent(importedRequest::setUser);
    approvedBy.ifPresent(value -> importedRequest.setCurrentlyApprovedBy(Collections.singletonList(value)));
    absence.ifPresent(importedRequest::setAbsence);
    importedRequest.setRequestStatus(REQUEST_STATUS.get(leaveImport.getStatus()));
    importedRequest.setReason(leaveImport.getReason());
    importedRequest.setSubmissionTime(leaveImport.getDateSubmitted().atTime(LocalTime.NOON));
    importedRequest.setDays(requestedDays);
    importedRequest.setApproverComment(leaveImport.getApproverComments());
    importedRequest.setCancellationReason(leaveImport.getCancellationReason());
    importedRequest.setApproverCancellationComment(importedRequest.getApproverCancellationComment());
    requestedDays.forEach(day -> day.setRequest(importedRequest));

    return importedRequest;
  }

  Optional<Absence> getAbsence(String absenceName) {
    absenceName = DECEASE_FAMILY_MEMBER.equals(absenceName) ? FUNERAL_FAMILY_MEMBER : absenceName;
    absenceName = DESEASE_FAMILY_MEMBER.equals(absenceName) ? SERIOUS_ILLNESS_OF_CLOSE_FAMILY_MEMBER : absenceName;
    absenceName = WEDDING_OF_FAMILY_MEMBER.equals(absenceName) ? FAMILY_MEMBER_WEDDING : absenceName;
    String finalAbsenceName = absenceName;

    return ABSENCES.stream().filter(absence -> finalAbsenceName.contains(absence.getName())).findFirst();
  }

  private List<Day> createDaysFromImport(AppogeeLeaveRequestImportData leaveImport) {
    return leaveImport.getStartDate()
                      .datesUntil(leaveImport.getEndDate().plusDays(1))
                      .filter(date -> isWorkDay(date, leaveImport.getEmail()))
                      .map(date -> getDay(leaveImport, date))
                      .collect(Collectors.toList());
  }

  boolean isWorkDay(LocalDate date, String email) {
    boolean isWeekend = DayOfWeek.SATURDAY.equals(date.getDayOfWeek()) || DayOfWeek.SUNDAY.equals(date.getDayOfWeek());
    boolean isPublicHoliday = PUBLIC_HOLIDAY_DATES.contains(date);
    boolean isShorterWorkingWeek = isShorterWorkingWeek(date, email);

    return !isWeekend && !isPublicHoliday && !isShorterWorkingWeek;
  }

  private boolean isShorterWorkingWeek(LocalDate date, String email) {
    LeaveProfileType usersLeaveProfileType = getUsersLeaveProfileType(email);
    switch (usersLeaveProfileType) {
      case EMPLOYEES_3_DAYS_WORKING:
        return DayOfWeek.TUESDAY.equals(date.getDayOfWeek()) || DayOfWeek.THURSDAY.equals(date.getDayOfWeek());
      case EMPLOYEES_4_DAYS_WORKING:
        return DayOfWeek.FRIDAY.equals(date.getDayOfWeek());
      default:
        return false;
    }
  }

  private LeaveProfileType getUsersLeaveProfileType(String email) {
    if (EMPLOYEES_LEAVE_PROFILE_TYPES.isEmpty()) {
      //@formatter:off
      Map<String, LeaveProfileType> leaveProfileTypes = userRepository.findAllByUserStatusType(List.of(UserStatusType.ACTIVE))
                                      .stream()
                                      .collect(Collectors.toMap(User::getEmail,
                                          user -> user.getLeaveProfile().getLeaveProfileType()));
      //@formatter:on
      EMPLOYEES_LEAVE_PROFILE_TYPES.putAll(leaveProfileTypes);
    }
    return EMPLOYEES_LEAVE_PROFILE_TYPES.get(email);
  }

  private Day getDay(AppogeeLeaveRequestImportData leaveImport, LocalDate date) {
    Day day = new Day();
    day.setDate(date);
    day.setDuration(getDuration(leaveImport, date));

    return day;
  }

  /**
   * As we need duration of day that was requested for each day this function calculates that duration based on
   * available parameters. If requested day was not full but either MORNING or AFTERNOON we cannot be sure which one
   * was it. Code is written to default to MORNING. Later in calendar import this we have additional checking to
   * update duration based on hours in calendar.
   *
   * @param leaveImport imported request data.
   * @param date current date for which we are getting duration.
   * @return duration for date.
   */
  private Duration getDuration(AppogeeLeaveRequestImportData leaveImport, LocalDate date) {
    var endDate = leaveImport.getEndDate();
    long appogeeComputedHours = (long) leaveImport.getComputedHours();
    var includesHalfDay = appogeeComputedHours % 8 != 0;
    var isEndDate = date.equals(endDate);

    return includesHalfDay && isEndDate ? Duration.MORNING : Duration.FULL_DAY;
  }

}
