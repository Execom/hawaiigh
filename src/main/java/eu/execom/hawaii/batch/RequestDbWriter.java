package eu.execom.hawaii.batch;

import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.repository.RequestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class RequestDbWriter implements ItemWriter<Request> {

  private RequestRepository requestRepository;

  @Autowired
  public RequestDbWriter(RequestRepository requestRepository) {
    this.requestRepository = requestRepository;
  }

  @Override
  public void write(List<? extends Request> importedRequests) {
    importedRequests.forEach(request -> {
      log.debug("Request: '{}' updated for user with email '{}'", request, request.getUser().getEmail());
      requestRepository.save(request);
    });
  }
}