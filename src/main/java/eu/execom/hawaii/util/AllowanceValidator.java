package eu.execom.hawaii.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AllowanceValidator implements ConstraintValidator<ValidAllowance, Integer> {

  private static final int HALF_DAY = 4;

  @Override
  public void initialize(ValidAllowance validAllowance) {
  }

  @Override
  public boolean isValid(Integer allowanceValue, ConstraintValidatorContext context) {
    return allowanceValue % HALF_DAY == 0;
  }
}
