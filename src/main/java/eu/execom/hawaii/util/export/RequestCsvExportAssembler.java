package eu.execom.hawaii.util.export;

import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.repository.DayRepository;
import eu.execom.hawaii.service.AllowanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RequestCsvExportAssembler {

  private static final int FULL_DAY = 8;
  private AllowanceService allowanceService;
  private DayRepository dayRepository;

  @Autowired
  public RequestCsvExportAssembler(AllowanceService allowanceService, DayRepository dayRepository) {
    this.allowanceService = allowanceService;
    this.dayRepository = dayRepository;
  }

  public String getRequestExportCsv(List<Request> requests, AbsenceType absenceType) {
    StringBuilder result = new StringBuilder();
    var headers = getRequestExportHeaders(absenceType);
    result.append(headers).append("\n");

    for (Request request : requests) {
      var column = getRequestExportColumn(request, absenceType);
      result.append(column).append("\n");
    }

    return result.toString();
  }

  private String getRequestExportHeaders(AbsenceType absenceType) {
    String[] bonusRequestHeaderValues = TableHeaders.BONUS_REQUEST;
    String[] requestHeaderValues = TableHeaders.REQUEST;
    return isBonus(absenceType) ? assembleColumn(bonusRequestHeaderValues) : assembleColumn(requestHeaderValues);
  }

  private String getRequestExportColumn(Request request, AbsenceType queriedAbsence) {

    User user = request.getUser();
    var teamName = user.getTeam().getName();

    String fullName = user.getFullName();

    var email = user.getEmail();
    var requestAbsence = request.getAbsence().getAbsenceType().getDescription();
    var requestStatus = request.getRequestStatus().getDescription();

    List<Day> requestDays = request.getDays();
    var startDate = requestDays.get(0).getDate().toString();

    int daysCount = requestDays.size() - 1;
    var endDate = requestDays.get(daysCount).getDate().toString();
    var totalHours = calculateTotalRequestHours(request.getId());
    var hoursInRange = getHoursInRange(request);
    var days = getDays(totalHours);
    var daysInRange = getDaysInRange(request);
    var reason = request.getReason();

    String[] bonusRequestColumn = {teamName, fullName, email, requestAbsence, requestStatus, startDate, endDate,
        String.valueOf(totalHours), "\"" + reason + "\""};

    String[] requestColumn = {teamName, fullName, email, requestAbsence, requestStatus, startDate, endDate,
        String.valueOf(totalHours), String.valueOf(hoursInRange), String.valueOf(days), String.valueOf(daysInRange),
        "\"" + reason + "\""};

    return isBonus(queriedAbsence) ? assembleColumn(bonusRequestColumn) : assembleColumn(requestColumn);
  }

  private int getHoursInRange(Request request) {
    return allowanceService.calculateHours(request.getDays());
  }

  private float getDays(int totalHours) {
    return (float) totalHours / FULL_DAY;
  }

  private float getDaysInRange(Request request) {
    return (float) allowanceService.calculateHours(request.getDays()) / FULL_DAY;
  }

  private int calculateTotalRequestHours(Long requestId) {
    var requestDays = dayRepository.findByRequestId(requestId);

    return allowanceService.calculateHours(requestDays);
  }

  private String assembleColumn(String[] column) {
    StringBuilder stringBuilder = new StringBuilder();
    for (String field : column) {
      stringBuilder.append(field).append(",");
    }

    return stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString();
  }

  private boolean isBonus(AbsenceType absenceType) {
    return AbsenceType.BONUS.equals(absenceType);
  }
}
