package eu.execom.hawaii.util.export;

import com.itextpdf.text.pdf.PdfPTable;
import eu.execom.hawaii.model.LeaveProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.util.List;

@Component
public class LeaveProfilesPdfExportAssembler {

  private static final int NUMBER_OF_TABLE_COLUMNS = 9;
  private static final int[] COLUMNS_RELATIVE_WIDTH = {3, 5, 2, 2, 2, 2, 2, 2, 2};

  private PdfGenerator pdfGenerator;

  @Autowired
  public LeaveProfilesPdfExportAssembler(PdfGenerator pdfGenerator) {
    this.pdfGenerator = pdfGenerator;
  }

  public ByteArrayInputStream getLeaveProfileExportPdf(List<LeaveProfile> leaveProfiles) {
    PdfPTable table = pdfGenerator.createPdfTable(NUMBER_OF_TABLE_COLUMNS, TableHeaders.LEAVE, COLUMNS_RELATIVE_WIDTH);

    for (int i = 0; i < leaveProfiles.size(); i++) {
      LeaveProfile leaveProfile = leaveProfiles.get(i);

      table.addCell(pdfGenerator.createCell(leaveProfile.getName(), i));
      table.addCell(pdfGenerator.createCell(leaveProfile.getComment(), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(leaveProfile.getEntitlement()), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(leaveProfile.getTraining()), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(leaveProfile.getMaxBonusDays()), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(leaveProfile.getMaxRelevantExperienceBonus()), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(leaveProfile.getMaxCarriedOver()), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(leaveProfile.getMaxAllowanceFromNextYear()), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(leaveProfile.isUpgradeable()), i));
    }

    return pdfGenerator.createPdfFile("Leave Profiles", table, true);
  }
}
