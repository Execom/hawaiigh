package eu.execom.hawaii.util.export;

import eu.execom.hawaii.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class UserCsvExportAssembler {

  public String getUserExportCsv(List<User> users) {
    List<String[]> dataLines = new ArrayList<>(Collections.singletonList(TableHeaders.USER_CSV));
    for (User user : users) {
      dataLines.add(new String[] {user.getFullName(), user.getEmail(), String.valueOf(user.getStartedWorkingDate()),
          String.valueOf(user.getStartedWorkingAtExecomDate()), String.valueOf(user.getStoppedWorkingAtExecomDate()),
          String.valueOf(user.getUserStatusType()), user.getTeam().getName(), user.getLeaveProfile().getName()});
    }

    return CsvGenerator.createCsvFile(dataLines);
  }
}