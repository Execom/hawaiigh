package eu.execom.hawaii.util.export;

import eu.execom.hawaii.dto.export.AllowanceExportDto;
import eu.execom.hawaii.model.Allowance;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class AllowanceCsvExportAssembler {

  public String getAllowanceExportCsv(List<Allowance> allowances) {
    //TODO Refactor this if needed after discussion is resolved https://github.com/execom-eu/hawaii/pull/1309#discussion_r421327429
    List<String[]> dataLines = new ArrayList<>(Collections.singletonList(TableHeaders.ALLOWANCE));
    for (Allowance allowance : allowances) {
      AllowanceExportDto allowanceDto = new AllowanceExportDto(allowance);
      dataLines.add(
          new String[] {allowanceDto.getUserFullName(), allowanceDto.getUserTeamName(), allowanceDto.getCarriedOver(),
              allowanceDto.getTotalLeave(), allowanceDto.getTakenLeave(), allowanceDto.getPendingLeave(),
              allowanceDto.getRemainingLeave(), allowanceDto.getTotalTraining(), allowanceDto.getTakenTraining(),
              allowanceDto.getPendingTraining(), allowanceDto.getRemainingTraining(), allowanceDto.getTakenSickness()});
    }
    return CsvGenerator.createCsvFile(dataLines);
  }
}
