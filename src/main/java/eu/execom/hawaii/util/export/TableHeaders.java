package eu.execom.hawaii.util.export;

final class TableHeaders {

  private TableHeaders() {
  }

  static final String[] ALLOWANCE = {"Employee", "Team", "Carried Over", "Total Leave", "Taken Leave", "Pending Leave",
      "Remaining Leave", "Total Training", "Taken Training", "Pending Training", "Remaining Training",
      "Total Taken Sickness"};

  static final String[] LEAVE = {"Name", "Description", "Annual Leave", "Annual Training", "Max Bonus",
      "Max Relevant Experience Bonus", "Max Carry Over Leave", "Max Allowance From Next Year", "Allow Override"};

  static final String[] REQUEST = {"Team", "Name", "Email", "Absence Type", "Status", "Start Date", "End Date", "Hours",
      "Hours in Range", "Days", "Days in Range", "Reason"};

  static final String[] USER_PDF = {"Name", "Team", "E-mail", "Status", "Started working"};

  //TODO check if user csv and pdf headers need to be the same after discussion is resolved https://github.com/execom-eu/hawaii/pull/1309#discussion_r421360792
  static final String[] USER_CSV = {"Name", "Email", "StartedWorkingDate", "StartedWorkingAtExecomDate",
      "StoppedWorkingAtExecomDate", "Status", "Team", "LeaveProfile"};

  static final String[] BONUS_REQUEST = {"Team", "Name", "Email", "Absence Type", "Status", "Hours", "Reason"};
}
