package eu.execom.hawaii.util.export;

import com.itextpdf.text.pdf.PdfPTable;
import eu.execom.hawaii.dto.export.AllowanceExportDto;
import eu.execom.hawaii.model.Allowance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.util.List;

@Component
public class AllowancePdfExportAssembler {

  private static final int NUMBER_OF_TABLE_COLUMNS = 12;
  private static final int[] COLUMNS_RELATIVE_WIDTH = {3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};

  private PdfGenerator pdfGenerator;

  @Autowired
  public AllowancePdfExportAssembler(PdfGenerator pdfGenerator) {
    this.pdfGenerator = pdfGenerator;
  }

  public ByteArrayInputStream getAllowanceExportPdf(List<Allowance> allowances) {
    PdfPTable table = pdfGenerator.createPdfTable(NUMBER_OF_TABLE_COLUMNS, TableHeaders.ALLOWANCE,
        COLUMNS_RELATIVE_WIDTH);

    for (int i = 0; i < allowances.size(); i++) {
      AllowanceExportDto allowanceExportDto = new AllowanceExportDto(allowances.get(i));

      table.addCell(pdfGenerator.createCell(allowanceExportDto.getUserFullName(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getUserTeamName(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getCarriedOver(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getTotalLeave(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getTakenLeave(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getPendingLeave(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getRemainingLeave(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getTotalTraining(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getTakenTraining(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getPendingTraining(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getRemainingTraining(), i));
      table.addCell(pdfGenerator.createCell(allowanceExportDto.getTakenSickness(), i));
    }

    return pdfGenerator.createPdfFile("Allowances", table, true);
  }
}
