package eu.execom.hawaii.util.export;

import com.itextpdf.text.pdf.PdfPTable;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.service.AllowanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.util.List;

@Component
public class RequestPdfExportAssembler {

  private static final int NUMBER_OF_TABLE_COLUMNS = 12;
  private static final int[] COLUMNS_RELATIVE_WIDTH = {4, 3, 4, 3, 3, 3, 3, 2, 2, 2, 2, 5};

  private AllowanceService allowanceService;
  private PdfGenerator pdfGenerator;

  @Autowired
  public RequestPdfExportAssembler(PdfGenerator pdfGenerator, AllowanceService allowanceService) {
    this.pdfGenerator = pdfGenerator;
    this.allowanceService = allowanceService;
  }

  public ByteArrayInputStream getRequestExportPdf(List<Request> requests) {
    PdfPTable table = pdfGenerator.createPdfTable(NUMBER_OF_TABLE_COLUMNS, TableHeaders.REQUEST,
        COLUMNS_RELATIVE_WIDTH);

    for (int i = 0; i < requests.size(); i++) {
      Request request = requests.get(i);
      User user = request.getUser();
      List<Day> days = request.getDays();
      int hours = allowanceService.calculateHours(days);
      double numberOfDays = (float) hours / 8.0;

      table.addCell(pdfGenerator.createCell(user.getTeam().getName(), i));
      table.addCell(pdfGenerator.createCell(user.getFullName(), i));
      table.addCell(pdfGenerator.createCell(user.getEmail().split("@")[0], i));
      table.addCell(pdfGenerator.createCell(request.getAbsence().getAbsenceType().toString(), i));
      table.addCell(pdfGenerator.createCell(request.getRequestStatus().toString(), i));
      table.addCell(pdfGenerator.createDateCell(days.get(0).getDate(), i));
      table.addCell(pdfGenerator.createDateCell(days.get(days.size() - 1).getDate(), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(hours), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(hours), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(numberOfDays), i));
      table.addCell(pdfGenerator.createCell(String.valueOf(numberOfDays), i));
      table.addCell(pdfGenerator.createCell(request.getReason(), i));
    }

    return pdfGenerator.createPdfFile("Requests", table, true);
  }
}
