package eu.execom.hawaii.util.export;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CsvGenerator {

  public static String createCsvFile(List<String[]> dataLines) {
    return dataLines.stream().map(CsvGenerator::convertToCsv).collect(Collectors.joining("\n"));
  }

  private static String convertToCsv(String[] data) {
    return Arrays.stream(data).map(CsvGenerator::escapeSpecialCharacters).collect(Collectors.joining(","));
  }

  private static String escapeSpecialCharacters(String data) {
    String escapedData = data.replaceAll("\\R", " ");
    if (data.contains(",") || data.contains("\"") || data.contains("'")) {
      data = data.replace("\"", "\"\"");
      escapedData = "\"" + data + "\"";
    }

    return escapedData;
  }
}
