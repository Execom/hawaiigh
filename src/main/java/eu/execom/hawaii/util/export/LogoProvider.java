package eu.execom.hawaii.util.export;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Lazy
@Component
public class LogoProvider {

  Image getLogo() {
    Image execomLogo = null;
    try {
      execomLogo = Image.getInstance(ClassPathResource.class.getResource("/static/icons/execom_logo_red.png"));
    } catch (BadElementException | IOException e) {
      log.error("Error loading Execom logo {}", e.getMessage());
    }
    return execomLogo;
  }
}
