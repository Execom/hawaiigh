package eu.execom.hawaii.util.export;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.BaseFont;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Slf4j
@Lazy
@Component
class CustomFontFactory {

  private static final String REGULAR_FONT_PATH = "static/fonts/Montserrat-Regular.ttf";
  private static final String BOLD_FONT_PATH = "static/fonts/Montserrat-Black.ttf";

  Font getRegularFont(float fontSize) {
    return FontFactory.getFont(REGULAR_FONT_PATH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, fontSize, Font.NORMAL,
        BaseColor.BLACK);
  }

  Font getBoldFont(float fontSize) {
    return FontFactory.getFont(BOLD_FONT_PATH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, fontSize, Font.NORMAL,
        BaseColor.BLACK);
  }
}
