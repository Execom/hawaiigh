package eu.execom.hawaii.util.export;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
@Component
public class PdfGenerator {

  private static final int COLUMNS_WIDTH_PERCENTAGE = 100;
  private static final int A4_PAPER_LONG_SIDE = 842;
  private static final int A4_PAPER_SHORT_SIDE = 595;
  private static final int LANDSCAPE_HEADER_WIDTH = 780;
  private static final int PORTRAIT_HEADER_WIDTH = 527;
  private static final int[] HEADER_COLUMNS_WIDTH = {24, 6};
  private static final String[] HEADER_ITEMS = {"Execom", "Novi Sad", "Bulevar vojvode Stepe 50",
      "https://www.execom.eu/"};

  private CustomFontFactory customFontFactory;
  private LogoProvider logoProvider;

  @Lazy
  @Autowired
  public PdfGenerator(CustomFontFactory customFontFactory, LogoProvider logoProvider) {
    this.customFontFactory = customFontFactory;
    this.logoProvider = logoProvider;
  }

  PdfPTable createPdfTable(int numberOfColumns, String[] headerValues, int[] columnsWidths) {
    PdfPTable table = new PdfPTable(numberOfColumns);
    table.setWidthPercentage(COLUMNS_WIDTH_PERCENTAGE);
    try {
      table.setWidths(columnsWidths);
    } catch (DocumentException e) {
      log.error("Error occurred {}", e.getMessage());
    }
    setHeaderValues(headerValues, table);
    return table;
  }

  private void setHeaderValues(String[] headerValues, PdfPTable table) {
    Font font = customFontFactory.getBoldFont(8f);
    for (String headerValue : headerValues) {
      PdfPCell hCell = new PdfPCell(new Phrase(headerValue, font));
      applyCellFormatting(hCell);
      hCell.setBackgroundColor(BaseColor.GRAY);
      table.addCell(hCell);
    }
  }

  PdfPCell createDateCell(LocalDate date, int index) {
    String formatedDate = formatDateForPdf(date);
    return createCell(formatedDate, index);
  }

  private String formatDateForPdf(LocalDate localDate) {
    return localDate.format(DateTimeFormatter.ofPattern("dd MMM yyyy"));
  }

  PdfPCell createCell(String cellValue, int index) {
    Font cellFont = customFontFactory.getRegularFont(8f);
    PdfPCell cell = new PdfPCell(new Phrase(cellValue, cellFont));
    setCellStyle(cell, index);
    return cell;
  }

  private void setCellStyle(PdfPCell cell, int index) {
    applyCellFormatting(cell);
    if (index % 2 == 1) {
      cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
    }
  }

  private void applyCellFormatting(PdfPCell cell) {
    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    cell.setPadding(5);
    cell.setBorderWidthLeft(0);
    cell.setBorderWidthRight(0);
    cell.setBorderWidthTop(0);
  }

  ByteArrayInputStream createPdfFile(String titleValue, PdfPTable table, boolean isLandscape) {
    ByteArrayOutputStream pdfFileOutputStream = new ByteArrayOutputStream();
    int paperWidth = isLandscape ? A4_PAPER_LONG_SIDE : A4_PAPER_SHORT_SIDE;
    int paperHeight = isLandscape ? A4_PAPER_SHORT_SIDE : A4_PAPER_LONG_SIDE;

    Font titleFont = customFontFactory.getBoldFont(10f);
    Phrase titleText = new Phrase(titleValue, titleFont);
    Paragraph title = new Paragraph();
    title.add(titleText);
    title.setAlignment(Element.ALIGN_CENTER);

    Document document = new Document(new Rectangle(paperWidth, paperHeight));
    try {
      PdfWriter.getInstance(document, pdfFileOutputStream);
      document.open();
      document.addCreationDate();
      document.add(createHeader(isLandscape));
      document.add(title);
      document.add(Chunk.NEWLINE);
      document.add(table);
    } catch (DocumentException e) {
      log.error("Error occurred {}", e.getMessage());
    } finally {
      document.close();
    }
    return new ByteArrayInputStream(pdfFileOutputStream.toByteArray());
  }

  private PdfPTable createHeader(boolean isLandscape) throws DocumentException {
    int headerWidth = isLandscape ? LANDSCAPE_HEADER_WIDTH : PORTRAIT_HEADER_WIDTH;
    PdfPTable header = new PdfPTable(2);
    header.setWidths(HEADER_COLUMNS_WIDTH);
    header.setTotalWidth(headerWidth);
    header.setLockedWidth(true);
    header.getDefaultCell().setFixedHeight(40);
    header.getDefaultCell().setBorder(Rectangle.BOTTOM);
    header.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);

    PdfPCell text = createTextCell();
    Font font = customFontFactory.getRegularFont(6f);
    for (String headerItem : HEADER_ITEMS) {
      text.addElement(new Phrase(headerItem, font));
    }
    text.setHorizontalAlignment(Element.ALIGN_LEFT);
    header.addCell(text);

    header.addCell(logoProvider.getLogo());
    return header;
  }

  private PdfPCell createTextCell() {
    PdfPCell textCell = new PdfPCell();
    textCell.setPaddingBottom(10);
    textCell.setPaddingLeft(10);
    textCell.setBorder(Rectangle.BOTTOM);
    textCell.setBorderColor(BaseColor.LIGHT_GRAY);

    return textCell;
  }
}
