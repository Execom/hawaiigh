package eu.execom.hawaii.util.export;

import eu.execom.hawaii.model.LeaveProfile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class LeaveProfilesCsvAssembler {

  public String getLeaveProfileExportCsv(List<LeaveProfile> leaveProfiles) {
    List<String[]> dataLines = new ArrayList<>(Collections.singletonList(TableHeaders.LEAVE));
    for (LeaveProfile leaveProfile : leaveProfiles) {
      dataLines.add(new String[] {leaveProfile.getName(), leaveProfile.getComment(),
          String.valueOf(leaveProfile.getEntitlement()), String.valueOf(leaveProfile.getTraining()),
          String.valueOf(leaveProfile.getMaxBonusDays()), String.valueOf(leaveProfile.getMaxRelevantExperienceBonus()),
          String.valueOf(leaveProfile.getMaxCarriedOver()), String.valueOf(leaveProfile.getMaxAllowanceFromNextYear()),
          String.valueOf(leaveProfile.isUpgradeable())});
    }
    return CsvGenerator.createCsvFile(dataLines);
  }
}
