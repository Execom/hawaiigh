package eu.execom.hawaii.util.export;

import lombok.Value;

@Value
public class ExportFile<T> {
  private T bodyData;
  private String fileName;
  private String fileFormat;
}
