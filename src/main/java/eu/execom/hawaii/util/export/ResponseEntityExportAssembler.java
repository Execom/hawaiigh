package eu.execom.hawaii.util.export;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ResponseEntityExportAssembler {

  public ResponseEntity getResponseEntity(ExportFile exportFile) {
    var headers = new HttpHeaders();
    headers.add("Content-Disposition", "inline; filename=\"" + exportFile.getFileName() + "\"");
    return ResponseEntity.ok()
                         .headers(headers)
                         .contentType(getContentType(exportFile.getFileFormat()))
                         .body(exportFile.getBodyData());
  }

  private MediaType getContentType(String exportType) {
    return exportType.equals("csv") ? MediaType.valueOf("text/csv; charset=UTF-8") : MediaType.APPLICATION_PDF;
  }
}
