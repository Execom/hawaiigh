package eu.execom.hawaii.util.export;

import com.itextpdf.text.pdf.PdfPTable;
import eu.execom.hawaii.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.util.List;

@Component
public class UserPdfExportAssembler {

  private static final int NUMBER_OF_TABLE_COLUMNS = 5;
  private static final int[] COLUMNS_RELATIVE_WIDTH = {3, 2, 3, 2, 2};

  private PdfGenerator pdfGenerator;

  @Autowired
  public UserPdfExportAssembler(PdfGenerator pdfGenerator) {
    this.pdfGenerator = pdfGenerator;
  }

  public ByteArrayInputStream getUserExportPdf(List<User> users) {
    PdfPTable table = pdfGenerator.createPdfTable(NUMBER_OF_TABLE_COLUMNS, TableHeaders.USER_PDF,
        COLUMNS_RELATIVE_WIDTH);

    for (int i = 0; i < users.size(); i++) {
      User user = users.get(i);

      table.addCell(pdfGenerator.createCell(user.getFullName(), i));
      table.addCell(pdfGenerator.createCell(user.getTeam().getName(), i));
      table.addCell(pdfGenerator.createCell(user.getEmail(), i));
      table.addCell(pdfGenerator.createCell(user.getUserStatusType().toString(), i));
      table.addCell(pdfGenerator.createDateCell(user.getStartedWorkingAtExecomDate(), i));
    }

    return pdfGenerator.createPdfFile("Employees", table, false);
  }
}
