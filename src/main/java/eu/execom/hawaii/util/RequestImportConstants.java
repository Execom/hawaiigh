package eu.execom.hawaii.util;

public final class RequestImportConstants {

  public static final int NOON = 12;

  public static final int AFTERNOON = 14;

  public static final String PENDING = "Pending Leave";

  // Old absence name from Appogee which is switched with the current one declared bellow this one
  public static final String DECEASE_FAMILY_MEMBER = "Decease family member";
  public static final String FUNERAL_FAMILY_MEMBER = "Funeral family member";

  // Old absence name from Appogee which is switched with the current one declared bellow this one
  // Intentional spelling error to match the absence name from Appogee
  public static final String DESEASE_FAMILY_MEMBER = "Desease family member";
  public static final String SERIOUS_ILLNESS_OF_CLOSE_FAMILY_MEMBER = "Serious illness of close family member";

  // Old absence name from Appogee which is switched with the current one declared bellow this one
  public static final String WEDDING_OF_FAMILY_MEMBER = "Wedding of family member";
  public static final String FAMILY_MEMBER_WEDDING = "Family Member Wedding";
}