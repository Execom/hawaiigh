package eu.execom.hawaii.util;

import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class EmailDomainValidator implements ConstraintValidator<EmailExecom, String> {

  private static final String REGEXP = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@execom.eu";

  @Override
  public void initialize(EmailExecom constraintAnnotation) {
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (StringUtils.isEmpty(value)) {
      return true;
    } else if (value.contains(",")) {
      return Arrays.stream(value.split("\\s*,\\s*")).allMatch(s -> s.matches(REGEXP));
    } else {
      return value.matches(REGEXP);
    }
  }
}