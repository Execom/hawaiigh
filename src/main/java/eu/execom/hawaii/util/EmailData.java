package eu.execom.hawaii.util;

import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum EmailData {

    SICKNESS("Sickness Notification", "recipientsSicknessEmail.ftl"),
    BONUS("Bonus Leave Notification", "recipientsBonusEmail.ftl"),
    ANNUAL("Annual Leave Notification", "recipientsAnnualEmail.ftl"),
    REVIEW_BONUS_ALLOWANCE_UPDATE("Review Bonus Allowance Update for Relevant Work Experience", "bonusAllowanceUpdateForHrManagersEmail.ftl"),
    REVIEW_REQUEST_NO_WORKING_DAYS("Review Imported Request", "requestImportOmittedDueNoWorkingDays.ftl"),
    REVIEW_CALENDAR_REQUEST_SAME_FULL_NAME("Review Imported Request", "calendarImportOmittedDueToSameName.ftl"),
    REVIEW_CALENDAR_REQUEST_NOT_FOUND_IN_DATABASE("Review Imported Request", "calendarImportOmittedDueToNotFoundInDatabase.ftl"),
    REVIEW_CALENDAR_REQUEST_NOT_UNIQUELY_IDENTIFIABLE("Review Imported Request", "calendarImportOmittedDueToNotUniquelyIdentifiable.ftl"),
    REVIEW_CALENDAR_REQUEST_HOURS_MISMATCH("Review Imported Request", "calendarImportOmittedDueHoursMismatch.ftl"),
    LEAVE_PROFILE_UPDATE("Leave Profile Update Notification", "leaveProfileUpdateEmail.ftl"),
    BONUS_ALLOWANCE_UPDATE("Bonus Day for Relevant Work Experience", "bonusAllowanceUpdateForUserEmail.ftl"),
    SICKNESS_REQUEST_CANCELLATION("Sickness Cancellation Request from %s requires your Approval", "createRequestEmail.ftl"),
    BONUS_REQUEST_CREATED("Bonus Request from %s requires your Approval", "createRequestEmail.ftl"),
    BONUS_REQUEST_CANCELLATION("Sickness Cancellation Request from %s requires your Approval", "createRequestEmail.ftl"),
    LEAVE_REQUEST_CREATED("Leave Request from %s requires your Approval", "createRequestEmail.ftl"),
    LEAVE_REQUEST_CANCELLATION("Bonus Request Cancellation from %s requires your Approval", "createRequestEmail.ftl"),
    LEAVE_REQUEST("Leave Request %s", "createRequestEmail.ftl"),
    SICKNESS_REQUEST("Sickness Request %s", "createRequestEmail.ftl"),
    BONUS_REQUEST("Bonus Request %s", "createRequestEmail.ftl"),
    HIGH_RESOURCES_USAGE_ALERT("WARNING - High resources usage", "createResourcesAlertEmail.ftl"),
    HTTP_ENDPOINT_FAILED_ALERT("WARNING - HTTP endpoint failed", "createHttpEndpointAlertEmail.ftl");

    private final String subject;
    private final String template;

    public String getSubject(String name) {
        return String.format(subject, name);
    }

    /**
     * Gets email data on created request for newly created or already approved request which is cancelled and requires
     * approvers attention again.
     *
     * @param request for which email should be send.
     * @return EmailProvider enum which has email subject and template
     */

    public static EmailData getRequestCreatedEmailData(Request request) {

        AbsenceType absenceType = request.getAbsence().getAbsenceType();
        switch (absenceType) {
            case SICKNESS:
                return SICKNESS_REQUEST_CANCELLATION;
            case LEAVE:
                return request.isCancellationPending() ? LEAVE_REQUEST_CANCELLATION : LEAVE_REQUEST_CREATED;
            case TRAINING:
                return request.isCancellationPending() ? LEAVE_REQUEST_CANCELLATION : LEAVE_REQUEST_CREATED;
            case BONUS:
                return request.isCancellationPending() ? BONUS_REQUEST_CANCELLATION : BONUS_REQUEST_CREATED;
            default:
                throw new IllegalArgumentException("Unrecognized absence type: " + absenceType);
        }
    }
}
