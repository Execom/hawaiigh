package eu.execom.hawaii.service;

import eu.execom.hawaii.model.PublicHoliday;
import eu.execom.hawaii.repository.PublicHolidayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * Public holiday management service.
 */
@Service
public class PublicHolidayService {

  private static final DateTimeFormatter DD_MMM_YYYY = DateTimeFormatter.ofPattern("dd MMM yyyy");
  private PublicHolidayRepository publicHolidayRepository;

  @Autowired
  public PublicHolidayService(PublicHolidayRepository publicHolidayRepository) {
    this.publicHolidayRepository = publicHolidayRepository;
  }

  /**
   * Retrieves a publicHoliday with a specific id.
   *
   * @param id the PublicHoliday id.
   * @return the PublicHoliday.
   */
  public PublicHoliday getById(Long id) {
    return publicHolidayRepository.getOne(id);
  }

  /**
   * Retrieves all publicHolidays.
   *
   * @param pageable page, size, and sort information for pagination.
   * @return paged list of public holidays.
   */
  public Page<PublicHoliday> findAll(Pageable pageable) {
    return publicHolidayRepository.findAll(pageable);
  }

  /**
   * Retrieves all publicHolidays sorted by public holiday name.
   *
   * @param deleted is it deleted.
   * @return a list of publicHolidays.
   */
  public List<PublicHoliday> findAllByDeleted(boolean deleted) {
    return publicHolidayRepository.findAllByDeletedOrderByNameAsc(deleted);
  }

  /**
   * Retrieves all public holidays in given year sorted by public holiday name.
   *
   * @param year year of public holiday.
   * @return a list of publicHolidays.
   */
  public List<PublicHoliday> findAllByYear(Integer year) {
    LocalDate startDate = LocalDate.of(year, 1, 1);
    LocalDate endDate = LocalDate.of(year, 12, 31);

    return publicHolidayRepository.findAllByDateIsBetweenOrderByDate(startDate, endDate);
  }

  /**
   * Retrieves all public holidays between two dates
   *
   * @param fromDate
   * @param toDate
   * @return a list of public holidays.
   */
  public List<PublicHoliday> findAllBetweenDates(LocalDate fromDate, LocalDate toDate) {
    return publicHolidayRepository.findAllByDateIsBetween(fromDate, toDate);
  }

  /**
   * Saves the provided PublicHoliday if public holiday with given date doesn't exist in database.
   *
   * @param publicHoliday the PublicHoliday to be persisted.
   * @return the PublicHoliday.
   */
  public PublicHoliday save(PublicHoliday publicHoliday) {
    if (publicHolidayRepository.existsByDate(publicHoliday.getDate())) {
      throw new EntityExistsException(
          "Public holiday with date : " + publicHoliday.getDate().format(DD_MMM_YYYY) + " already exists.");
    }

    return publicHolidayRepository.save(publicHoliday);
  }

  public PublicHoliday update(PublicHoliday publicHoliday) {
    validateDateIsUnique(publicHoliday);

    return publicHolidayRepository.save(publicHoliday);
  }

  private void validateDateIsUnique(PublicHoliday publicHoliday) {
    Optional<PublicHoliday> oldPublicHolidayOptional = publicHolidayRepository.findById(publicHoliday.getId());

    oldPublicHolidayOptional.ifPresentOrElse(oldPublicHoliday -> validateDateIsUnique(oldPublicHoliday, publicHoliday),
        () -> {
          throw new EntityExistsException("Selected holiday does not exist in database");
        });
  }

  private void validateDateIsUnique(PublicHoliday oldPublicHoliday, PublicHoliday publicHoliday) {
    boolean dateUpdated = !oldPublicHoliday.getDate().equals(publicHoliday.getDate());
    boolean existsInDatabase = publicHolidayRepository.existsByDate(publicHoliday.getDate());

    if (dateUpdated && existsInDatabase) {
      throw new EntityExistsException(
          "Public holiday with date : " + publicHoliday.getDate().format(DD_MMM_YYYY) + " already exists.");
    }
  }

  /**
   * Logically deletes PublicHoliday.
   *
   * @param id the PublicHoliday id.
   */
  public void delete(Long id) {
    publicHolidayRepository.deleteById(id);
  }

}
