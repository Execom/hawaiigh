package eu.execom.hawaii.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import eu.execom.hawaii.exceptions.AuditDataParseException;
import eu.execom.hawaii.model.AuditInformation;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.audit.AllowanceAudit;
import eu.execom.hawaii.model.audit.Audit;
import eu.execom.hawaii.model.audit.AuditedChanges;
import eu.execom.hawaii.model.audit.RequestAudit;
import eu.execom.hawaii.model.audit.TeamAudit;
import eu.execom.hawaii.model.audit.UserAudit;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import eu.execom.hawaii.model.enumerations.OperationPerformed;
import eu.execom.hawaii.repository.AuditInformationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AuditInformationService {

  private static final ObjectMapper OBJECT_MAPPER;

  private AuditInformationRepository auditInformationRepository;

  static {
    OBJECT_MAPPER = new ObjectMapper();
    OBJECT_MAPPER.registerModule(new JavaTimeModule());
    OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  @Autowired
  public AuditInformationService(AuditInformationRepository auditInformationRepository) {
    this.auditInformationRepository = auditInformationRepository;
  }

  @PostConstruct
  public void init() {
    OBJECT_MAPPER.registerModule(new JavaTimeModule());
    OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
  }

  public AuditInformation getById(Long id) {
    return auditInformationRepository.getOne(id);
  }

  public List<AuditInformation> getAll() {
    return auditInformationRepository.findAll();
  }

  /**
   * Save audit information.
   *
   * @param operationPerformed what operation was performed (CREATE, REMOVE, UPDATE, DELETE, ACTIVATE).
   * @param modifiedByUser user that made change on that entity.
   * @param modifiedEntityId entity on which changes have been performed.
   * @param previousState state of the object before the change.
   * @param currentState state of the object after the change.
   */
  void saveAudit(OperationPerformed operationPerformed, User modifiedByUser, Long modifiedEntityId, Audit previousState,
      Audit currentState) {

    AuditInformation auditInformation = new AuditInformation();

    auditInformation.setOperationPerformed(operationPerformed);
    auditInformation.setAuditedEntity(currentState.getAuditedEntity());
    auditInformation.setModifiedDateTime(LocalDateTime.now());
    auditInformation.setModifiedByUser(modifiedByUser);
    auditInformation.setModifiedEntityId(modifiedEntityId);
    auditInformation.setPreviousValue(serialize(previousState));
    auditInformation.setCurrentValue(serialize(currentState));

    auditInformationRepository.save(auditInformation);
  }

  /**
   * Convert object to string formatted as JSON.
   *
   * @param objectForSerialization previous or current state of the object.
   */
  private String serialize(Audit objectForSerialization) {
    String currentValue = null;

    try {
      currentValue = OBJECT_MAPPER.writeValueAsString(objectForSerialization);
    } catch (JsonProcessingException e) {
      log.error("Failed to map object of class '{}' as string", objectForSerialization.getClass());
    }

    return currentValue;
  }

  @SuppressWarnings("unchecked")
  public Map<AuditInformation, List<AuditedChanges>> getAudit(Long id, AuditedEntity auditedEntity) {
    Class clazz = getAuditedClass(auditedEntity);

    Map<AuditInformation, List<AuditedChanges>> auditInformationMap = new TreeMap<>();
    List<AuditInformation> teamAudit = auditInformationRepository.getAllByAuditedEntityAndModifiedEntityIdOrderByModifiedDateTimeDesc(
        auditedEntity, id);

    for (AuditInformation auditInformation : teamAudit) {
      Audit oldValue = deserialize(auditInformation.getPreviousValue(), clazz);
      Audit newValue = deserialize(auditInformation.getCurrentValue(), clazz);
      List<AuditedChanges> auditedChanges = oldValue.compareFields(newValue);

      addChangesToMap(auditInformationMap, auditInformation, auditedChanges);
    }

    return auditInformationMap;
  }

  private Class getAuditedClass(AuditedEntity auditedEntity) {
    if (AuditedEntity.ALLOWANCE.equals(auditedEntity)) {
      return AllowanceAudit.class;
    } else {
      return AuditedEntity.TEAM.equals(auditedEntity) ? TeamAudit.class : UserAudit.class;
    }
  }

  private void addChangesToMap(Map<AuditInformation, List<AuditedChanges>> auditInformationMap,
      AuditInformation auditInformation, List<AuditedChanges> auditedChanges) {
    if (!auditedChanges.isEmpty() || isUserArchivedOrRestored(auditInformation)) {
      auditInformationMap.merge(auditInformation, auditedChanges, (savedChanges, newChanges) -> newChanges);
    }
  }

  private boolean isUserArchivedOrRestored(AuditInformation auditInformation) {
    return OperationPerformed.DELETE.equals(auditInformation.getOperationPerformed())
        || OperationPerformed.ACTIVATE.equals(auditInformation.getOperationPerformed());
  }

  public List<RequestAudit> getApprovalFlowForRequestAudit(Long id) {
    //    @formatter:off
    return auditInformationRepository
        .getAllByAuditedEntityAndModifiedEntityIdOrderByModifiedDateTimeAsc(AuditedEntity.REQUEST, id)
        .stream()
        .map(toRequestAudit())
        .collect(Collectors.toList());
    //@formatter:on
  }

  private Function<AuditInformation, RequestAudit> toRequestAudit() {
    return audit -> deserialize(audit.getCurrentValue(), RequestAudit.class);
  }

  private <T extends Audit> T deserialize(String currentValue, Class<T> clazz) {
    try {
      return OBJECT_MAPPER.readValue(currentValue, clazz);
    } catch (JsonProcessingException e) {
      log.error("Failed to map JSON representation to an object: {}", currentValue);
      throw new AuditDataParseException("Audit logs are not loaded correctly, please contact IT support.");
    }
  }
}
