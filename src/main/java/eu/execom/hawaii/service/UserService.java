package eu.execom.hawaii.service;

import eu.execom.hawaii.dto.CreateTokenDto;
import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.exceptions.DateOutOfScopeException;
import eu.execom.hawaii.exceptions.UnauthorizedAccessException;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.UserPushToken;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.model.audit.UserAudit;
import eu.execom.hawaii.model.enumerations.OperationPerformed;
import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.LeaveProfileRepository;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserPushTokensRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.repository.YearRepository;
import liquibase.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.el.PropertyNotFoundException;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.MonthDay;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * User management service.
 */
@Slf4j
@Service
public class UserService {

  private static final int HALF_DAY = 4;
  private static final MonthDay HALF_YEAR_DATE = MonthDay.of(6, 30);
  private static final int MINIMUM_QUERY_LENGTH = 3;
  private static final String ARCHIVE_TEAM_NAME = "Archived Users";
  private static final List<UserAdminPermission> ADMIN_PERMISSIONS = Arrays.asList(UserAdminPermission.BASIC,
      UserAdminPermission.SUPER);
  private static final String EMAIL_REGEX = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@execom.eu";
  private static final String INACTIVE_SUPER_ADMIN_MESSAGE = "User '%s' is '%s'. Only active user can be super admin.";
  private static final String INACTIVE_ADMIN_MESSAGE = "User '%s' is '%s'. Only active users can be admins.";

  @Value("${superAdmin.email}")
  private String superAdminMail;

  private UserRepository userRepository;
  private UserPushTokensRepository userPushTokensRepository;
  private YearRepository yearRepository;
  private AuditInformationService auditInformationService;
  private TeamRepository teamRepository;
  private AllowanceService allowanceService;
  private LeaveProfileRepository leaveProfileRepository;
  private Environment environment;

  @Autowired
  public UserService(UserRepository userRepository, UserPushTokensRepository userPushTokensRepository,
      YearRepository yearRepository, AuditInformationService auditInformationService, TeamRepository teamRepository,
      AllowanceService allowanceService, LeaveProfileRepository leaveProfileRepository, Environment environment) {
    this.userRepository = userRepository;
    this.userPushTokensRepository = userPushTokensRepository;
    this.yearRepository = yearRepository;
    this.auditInformationService = auditInformationService;
    this.teamRepository = teamRepository;
    this.allowanceService = allowanceService;
    this.leaveProfileRepository = leaveProfileRepository;
    this.environment = environment;
  }

  /**
   * Initializes super admin identified by an email in application.properties.
   */
  @EventListener(ApplicationReadyEvent.class)
  public void init() {
    String[] activeProfiles = environment.getActiveProfiles();
    if (Arrays.asList(activeProfiles).contains("test")) {
      return;
    }
    validateSuperAdminEmail();
    User newSuperAdmin = findByEmail(superAdminMail);
    throwExceptionIfUserIsActive(INACTIVE_SUPER_ADMIN_MESSAGE, newSuperAdmin);
    removeOldSuperAdminsIfExist(newSuperAdmin);
    newSuperAdmin.setUserAdminPermission(UserAdminPermission.SUPER);
    userRepository.save(newSuperAdmin);
    log.info("New super admin is '{}'.", newSuperAdmin.getFullName());
  }

  /**
   * Checks if super admin email is set in the application.properties, and is it valid.
   */
  private void validateSuperAdminEmail() {
    if (StringUtils.isEmpty(superAdminMail)) {
      String message = "Super admin mail is not set.";
      log.error(message);
      throw new PropertyNotFoundException(message);
    }
    if (superAdminMail.contains(",")) {
      String message = "Multiple super admin mails are not allowed.";
      log.error(message);
      throw new IllegalArgumentException(message);
    }
    if (!superAdminMail.matches(EMAIL_REGEX)) {
      String message = "Super admin mail is not valid.";
      log.error(message);
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Checks if there are old super admins and sets their admin permissions to basic.
   *
   * @param newSuperAdmin new super admin.
   */
  private void removeOldSuperAdminsIfExist(User newSuperAdmin) {
    List<User> oldSuperAdmins = userRepository.findAllByUserAdminPermission(UserAdminPermission.SUPER);
    boolean oldSuperAdminsDontExist = oldSuperAdmins.isEmpty();
    boolean newSuperAdminIsAlreadySet = oldSuperAdmins.size() == 1 && oldSuperAdmins.contains(newSuperAdmin);
    if (oldSuperAdminsDontExist || newSuperAdminIsAlreadySet) {
      return;
    }
    oldSuperAdmins.forEach(user -> user.setUserAdminPermission(UserAdminPermission.BASIC));
    userRepository.saveAll(oldSuperAdmins);
    log.warn("Old super admins permissions are revoked.");
  }

  /**
   * Retrieves a list of all active users from repository with a matching team.
   *
   * @param team Team a user belongs to
   * @return a list of all active users belonging to a given team
   */
  public List<User> findAllActiveUsersByTeam(Team team) {
    List<User> users = userRepository.findAllByUserStatusTypeAndTeam(UserStatusType.ACTIVE, team);
    users.addAll(team.getTeamApprovers());
    return users;
  }

  /**
   * Retrieves a list of all users from repository.
   *
   * @param userStatusType what is user status (ACTIVE, INACTIVE or DELETED)
   * @return a list of all users.
   */
  public List<User> findAllByUserStatusType(List<UserStatusType> userStatusType) {
    return userRepository.findAllByUserStatusType(userStatusType)
                         .stream()
                         .filter(User::hasAnyRoleOtherThanHrAssistant)
                         .collect(Collectors.toList());
  }

  /**
   * Retrieves paged list of all users from repository.
   *
   * @param userStatusType users status (ACTIVE, INACTIVE or DELETED)
   * @return paged users.
   */
  public Page<User> findAllByUserStatusType(List<UserStatusType> userStatusType, Pageable pageable) {
    var users = findAllByUserStatusType(userStatusType);

    return createPageableUsersList(users, pageable);
  }

  /**
   * Retrieves a list of all users searched by given query depending on users role.
   * If includeLoggedUser is true, logged user is included in result set.
   *
   * @param authUser logged user.
   * @param userStatusType what is user status (ACTIVE, INACTIVE or DELETED)
   * @param searchQuery search by given query.
   * @param includeLoggedUser is logged user included in result set(default: false).
   * @param pageable page, size, and sort information.
   * @return a list of queried users by given search.
   */
  public Page<User> searchUsers(User authUser, List<UserStatusType> userStatusType, String searchQuery,
      boolean includeLoggedUser, Pageable pageable) {
    if (searchQuery.length() < MINIMUM_QUERY_LENGTH) {
      return Page.empty(pageable);
    }
    return userRepository.searchByStatusTypeEmailAndFullName(userStatusType, searchQuery, searchQuery)
                         .stream()
                         .filter(loggedInUser(authUser, includeLoggedUser)
                         .and(usersBeingApprovedByGivenUser(authUser))
                         .and(User::hasAnyRoleOtherThanHrAssistant))
                         .collect(Collectors.collectingAndThen(Collectors.toList(),
                                 users -> createPageableUsersList(users, pageable)));
  }

  private Predicate<User> usersBeingApprovedByGivenUser(User authUser) {
    return user -> authUser.hasRoleHrManager() || user.getApprovers().contains(authUser) || user.equals(authUser);
  }

  private Predicate<User> loggedInUser(User authUser, boolean includeLoggedUser) {
    return user -> !user.getId().equals(authUser.getId()) || includeLoggedUser;
  }

  /**
   * Retrieves all admin users.
   *
   * @return a list of admin users.
   */
  public List<User> findAllAdmins() {
    return userRepository.findAllByUserAdminPermissionInOrderByUserAdminPermissionDesc(ADMIN_PERMISSIONS);
  }

  /**
   * Retrieves searched non admin users.
   *
   * @param searchQuery substring of name or email.
   * @return a list of non-admin users.
   */
  public List<User> searchAllNonAdminActiveUsersByEmailAndFullName(String searchQuery) {
    return userRepository.searchByUserStatusTypeUserAdminPermissionEmailAndFullName(UserStatusType.ACTIVE,
        UserAdminPermission.NONE, searchQuery);
  }

  /***
   * Get user with the given userId. If userId is null return authUser
   *
   * @param userId Users id
   * @param authUser whose role should be checked to see if required permissions to access the user are met.
   * @return user if id is specified and authUser has permission. If id is not specified return authUser.
   * @throws AccessDeniedException if authUser is not HR manager or approver for the requested users team.
   */
  @Transactional(readOnly = true)
  public User findUserById(Long userId, User authUser) {
    return Optional.ofNullable(userId).isEmpty() ? authUser : getUser(userId, authUser);
  }

  User getUser(Long id, User authUser) {
    User user = findById(id);
    boolean isApprover = user.getApprovers().stream().anyMatch(approver -> approver.equals(authUser));
    if (!isApprover && !authUser.hasRoleHrManager()) {
      throw new AccessDeniedException("Only users team approvers and hr managers can access this information.");
    }

    return user;
  }

  /**
   * Retrieves a User with a specific id
   *
   * @param id Users id
   * @return User with specified id if exists
   */
  @Transactional(readOnly = true)
  public User findById(Long id) {
    return userRepository.findById(id)
                         .orElseThrow(
                             () -> new EntityNotFoundException("User with id '" + id + "' doesn't exist in database."));
  }

  /**
   * Retrieves a User with a specific email.
   *
   * @param email User email
   * @return User with specified email if exists
   * @throws EntityNotFoundException if a user with given email is not found
   */
  @Transactional(readOnly = true)
  public User findByEmail(String email) {
    return userRepository.findByEmail(email)
                         .orElseThrow(() -> new EntityNotFoundException("User with given email doesn't exist."));
  }

  /**
   * Saves the provided User to repository.
   *
   * @param user the User entity to be persisted.
   * @return saved User.
   */
  @Transactional
  public User save(User user) {
    return userRepository.save(user);
  }

  /**
   * Change user admin permission level.
   *
   * @param updatedUser updated user with new admin permission level.
   * @param modifiedByUser super admin.
   * @return persisted updated user.
   */
  public User updateAdmin(User updatedUser, User modifiedByUser) {
    User oldUser = findById(updatedUser.getId());
    validateAdminIsEditedBySuperAdmin(oldUser, updatedUser, modifiedByUser);
    validateSuperAdminRoleChange(oldUser, updatedUser);
    oldUser.setUserAdminPermission(updatedUser.getUserAdminPermission());
    return userRepository.save(oldUser);
  }

  /**
   * Check if user admin permission level is edited by super admin.
   *
   * @param oldUser old user object.
   * @param updatedUser updated user object.
   * @param modifiedByUser user that modifies existing user.
   */
  private void validateAdminIsEditedBySuperAdmin(User oldUser, User updatedUser, User modifiedByUser) {
    List<User> superAdmins = userRepository.findAllByUserAdminPermission(UserAdminPermission.SUPER);
    boolean isLoggedUserSuperAdmin = superAdmins.contains(modifiedByUser);
    validateAdminPermissionIsAddedBySuperAdmin(oldUser, updatedUser, isLoggedUserSuperAdmin);
    validateAdminPermissionIsRevokedBySuperAdmin(oldUser, updatedUser, isLoggedUserSuperAdmin);
  }

  /**
   * Checks if admin role is assigned by super admin.
   *
   * @param oldUser old user object.
   * @param updatedUser edited user.
   * @param isLoggedUserSuperAdmin is user that is assigning admin rights super admin.
   */
  private void validateAdminPermissionIsAddedBySuperAdmin(User oldUser, User updatedUser,
      boolean isLoggedUserSuperAdmin) {
    throwExceptionIfUserIsActive(INACTIVE_ADMIN_MESSAGE, oldUser);
    boolean isUpdatedUserAdmin = UserAdminPermission.BASIC.equals(updatedUser.getUserAdminPermission());
    if (isUpdatedUserAdmin && !isLoggedUserSuperAdmin) {
      throw new ActionNotAllowedException("Admin can be added by super admin only.");
    }
  }

  /**
   * Throw ActionNotAllowedException if user is not active.
   *
   * @param messageTemplate for exception.
   * @param user for which status is checked.
   */
  private void throwExceptionIfUserIsActive(String messageTemplate, User user) {
    UserStatusType userStatusType = user.getUserStatusType();
    if (!UserStatusType.ACTIVE.equals(userStatusType)) {
      String message = String.format(messageTemplate, user.getFullName(), userStatusType.getDescription());
      log.error(message);
      throw new ActionNotAllowedException(message);
    }
  }

  /**
   * Checks if admin role is revoked by super admin.
   *
   * @param oldUser old user.
   * @param updatedUser new updated user.
   * @param isLoggedUserSuperAdmin is user that is revoking admin rights super admin.
   */
  private void validateAdminPermissionIsRevokedBySuperAdmin(User oldUser, User updatedUser,
      boolean isLoggedUserSuperAdmin) {
    boolean isOldUserAdmin = UserAdminPermission.BASIC.equals(oldUser.getUserAdminPermission());
    boolean isAdminPermissionRemoved = UserAdminPermission.NONE.equals(updatedUser.getUserAdminPermission());
    if (isOldUserAdmin && isAdminPermissionRemoved && !isLoggedUserSuperAdmin) {
      throw new ActionNotAllowedException("Admin can be removed by super admin only.");
    }
  }

  private void validateSuperAdminRoleChange(User oldUser, User updatedUser) {
    boolean isOldPermissionSuperAdmin = UserAdminPermission.SUPER.equals(oldUser.getUserAdminPermission());
    boolean isNewPermissionSuperAdmin = UserAdminPermission.SUPER.equals(updatedUser.getUserAdminPermission());
    if (isOldPermissionSuperAdmin != isNewPermissionSuperAdmin) {
      throw new ActionNotAllowedException("Super admin role cannot be assigned or revoked.");
    }
  }

  /**
   * Saves the provided User to repository.
   * Makes audit of that save.
   *
   * @param updatedUser the User entity to be persisted.
   * @param modifiedByUser user that made the change to User entity.
   * @return saved User.
   */
  @Transactional
  public User update(User updatedUser, User modifiedByUser) {
    User oldUser = findById(updatedUser.getId());

    validateEmailIsUnique(oldUser.getEmail(), updatedUser.getEmail());
    validateUserIsNotMemberAndApproverOfTeam(updatedUser);

    forbidLoggedUserToChangeHisRole(updatedUser, modifiedByUser);
    saveAuditInformation(OperationPerformed.UPDATE, modifiedByUser, updatedUser);
    updatedUser.setUserAdminPermission(oldUser.getUserAdminPermission());

    return userRepository.save(updatedUser);
  }

  private void forbidLoggedUserToChangeHisRole(User updatedUser, User modifiedByUser) {
    if (updatedUser.equals(modifiedByUser) && !updatedUser.getUserRole().equals(modifiedByUser.getUserRole())) {
      throw new IllegalArgumentException("You are not allowed to change your own role.");
    }
  }

  /**
   * Saves the provided User to repository.
   * Makes audit of that save.
   *
   * @param user the User entity to be persisted.
   * @param modifiedByUser user that made the change to User entity.
   * @return saved User.
   */
  @Transactional
  public User create(User user, User modifiedByUser) {
    checkIfNewUserIsAdmin(user);
    validateEmailIsUnique("", user.getEmail());
    validateUserIsNotMemberAndApproverOfTeam(user);

    validateDates(user.getStartedWorkingAtExecomDate(), user.getStartedWorkingDate(),
        "Started working at Execom date cannot be before started working date");
    validateDates(user.getStartedWorkingAtExecomDate(), user.getStartedProfessionalCareerDate(),
        "Started professional career date cannot be after started working at Execom date");

    activateUserBasedOnStartedWorkingAtExecomDate(user);
    userRepository.save(user);
    saveAuditInformation(OperationPerformed.CREATE, modifiedByUser, user);

    return user;
  }

  /**
   * Check if created user is admin. Admin role can only be assign by super admin after user is created.
   *
   * @param user new user.
   */
  private void checkIfNewUserIsAdmin(User user) {
    switch (user.getUserAdminPermission()) {
      case SUPER:
        throw new ActionNotAllowedException("Super admin can't be created.");
      case BASIC:
        throw new ActionNotAllowedException("Admin can't be created.");
      case NONE:
        break;
      default:
        throw new IllegalArgumentException("Unrecognized permission: " + user.getUserAdminPermission());
    }
  }

  private void validateEmailIsUnique(String oldEmail, String newEmail) {
    if (!oldEmail.equals(newEmail) && userRepository.existsByEmail(newEmail)) {
      throw new EntityExistsException("User with email '" + newEmail + "' already exists in database.");
    }
  }

  private void validateUserIsNotMemberAndApproverOfTeam(User user) {
    var approverTeams =
        user.getId() == null ? user.getApproverTeams() : userRepository.getOne(user.getId()).getApproverTeams();
    if (approverTeams.stream().anyMatch(team -> team.getId().equals(user.getTeam().getId()))) {
      throw new ActionNotAllowedException(
          "User is not allowed to be a member and approver of one team at the same time.");
    }
  }

  private void validateDates(LocalDate firstDate, LocalDate secondDate, String errorMessage) {
    if (!firstDate.equals(secondDate) && firstDate.isBefore(secondDate)) {
      throw new DateOutOfScopeException(errorMessage);
    }
  }

  /**
   * User that may need activation can be INACTIVE or DELETED.
   * <p>
   * When we create user we have option to make that user active or inactive by setting startedWorkingAtExecomDate
   * to future date or current date. Activating can be done by scheduled job or by directly calling this method and
   * passing value for this date to today.
   * <p>
   * Also if user is archived (users status is DELETED) restoring can be done automatically on date specified at that
   * field or directly by calling this method and setting specified date at today.
   *
   * @param user that is being activated.
   * @param modifiedByUser user to whom changes will be prescribed to in audit information.
   */
  public void activate(User user, User modifiedByUser) {
    LocalDate today = LocalDate.now();
    UserStatusType userStatusType = user.getUserStatusType();
    switch (userStatusType) {
      case INACTIVE:
        user.setStartedWorkingAtExecomDate(today);
        if (allowanceService.existsByUserId(user.getId())) {
          reactivateArchivedUser(user, modifiedByUser, today);
        } else {
          activateUserBasedOnStartedWorkingAtExecomDate(user);
        }
        break;
      case DELETED:
        if (user.getStartedWorkingAtExecomDate().isAfter(today)) {
          validateUserHasTeamAssigned(user);
          user.setUserStatusType(UserStatusType.INACTIVE);
          saveAuditInformation(OperationPerformed.UPDATE, modifiedByUser, user);
          userRepository.save(user);
          return;
        } else {
          reactivateArchivedUser(user, modifiedByUser, today);
        }
        break;
      case ACTIVE:
        log.warn("User already active");
        return;
      default:
        throw new IllegalArgumentException("Unsupported user status type: " + userStatusType);
    }
    saveAuditInformation(OperationPerformed.ACTIVATE, modifiedByUser, user);
    userRepository.save(user);
  }

  private void activateUserBasedOnStartedWorkingAtExecomDate(User user) {
    if (user.getStartedWorkingAtExecomDate().isAfter(LocalDate.now())) {
      user.setUserStatusType(UserStatusType.INACTIVE);
    } else {
      user.setUserStatusType(UserStatusType.ACTIVE);
      user.setStoppedWorkingAtExecomDate(null);
      user.setAllowances(createAllowanceForUserOnCreateUser(user));
      user.setYearsOfService(calculateYearsOfService(user));
    }
  }

  private void validateUserHasTeamAssigned(User updatedUser) {
    Team archivedTeam = teamRepository.findByName(ARCHIVE_TEAM_NAME);
    if (updatedUser.isDeleted() && updatedUser.getTeam().getId().equals(archivedTeam.getId())) {
      throw new IllegalArgumentException("To restore a user, you need to assign users team first.");
    }
  }

  private void reactivateArchivedUser(User user, User modifiedByUser, LocalDate today) {
    user.setUserStatusType(UserStatusType.ACTIVE);
    user.setStartedWorkingAtExecomDate(today);
    user.setStoppedWorkingAtExecomDate(null);
    updateUserAllowancesForActiveYears(user, modifiedByUser);
    user.setYearsOfService(calculateYearsOfService(user));
  }

  /**
   * Logically deletes User.
   */
  @Transactional
  public void delete(Long userId, User modifiedByUser, LocalDate stoppedWorkingDate) {
    var user = userRepository.getOne(userId);
    checkIfDeletedUserIsSuperAdmin(user);
    isUserEditable(UserRole.HR_ASSISTANT.equals(user.getUserRole()), "You are not allowed to delete this user.");
    var today = LocalDate.now();
    stoppedWorkingDate = stoppedWorkingDate == null ? today : stoppedWorkingDate;
    user.setStoppedWorkingAtExecomDate(stoppedWorkingDate);
    if (stoppedWorkingDate.isAfter(today)) {
      saveAuditInformation(OperationPerformed.UPDATE, modifiedByUser, user);
    } else {
      user.setUserStatusType(UserStatusType.DELETED);
      user.setTeam(teamRepository.findByName(ARCHIVE_TEAM_NAME));
      removeAllApproverTeams(user);
      saveAuditInformation(OperationPerformed.DELETE, modifiedByUser, user);
    }
    user.setUserAdminPermission(UserAdminPermission.NONE);
    userRepository.save(user);
  }

  private void checkIfDeletedUserIsSuperAdmin(User user) {
    List<User> superAdmins = userRepository.findAllByUserAdminPermission(UserAdminPermission.SUPER);
    if (superAdmins.contains(user)) {
      throw new ActionNotAllowedException("Super admin can't be deleted.");
    }
  }

  public void cancelDelete(Long id, User modifiedByUser) {
    User user = findUserById(id, modifiedByUser);
    UserAudit previousUserState = UserAudit.fromUser(user);
    user.setStoppedWorkingAtExecomDate(null);
    UserAudit currentUserState = UserAudit.fromUser(user);
    auditInformationService.saveAudit(
        OperationPerformed.UPDATE,
        modifiedByUser,
        user.getId(),
        previousUserState,
        currentUserState);
    save(user);
  }

  private void isUserEditable(boolean statementTrue, String message) {
    if (statementTrue) {
      throw new ActionNotAllowedException(message);
    }
  }

  private void removeAllApproverTeams(User user) {
    List<Team> approverTeams = user.getApproverTeams();
    approverTeams.forEach(team -> team.getTeamApprovers().remove(user));
  }

  private void updateUserAllowancesForActiveYears(User user, User modifiedByUser) {
    var openedActiveYears = yearRepository.findAllByYearGreaterThanEqualAndActiveTrue(LocalDate.now().getYear());
    List<Allowance> userAllowances = allowanceService.getAllByUserId(user.getId());
    Map<Integer, Allowance> allowanceMap = userAllowances.stream().collect(usersAllowancesToMap());

    for (Year year : openedActiveYears) {
      Optional<Allowance> oldAllowance = Optional.ofNullable(allowanceMap.get(year.getYear()));
      oldAllowance.ifPresent(allowance -> removeCollidingAllowances(modifiedByUser, userAllowances, year, allowance));

      Allowance newAllowance = createAllowance(user, year);
      userAllowances.add(newAllowance);
    }

    user.setAllowances(userAllowances);
  }

  private Collector<Allowance, ?, Map<Integer, Allowance>> usersAllowancesToMap() {
    return Collectors.toMap(allowance -> allowance.getYear().getYear(), allowance -> allowance, (a1, a2) -> a2);
  }

  private void removeCollidingAllowances(User modifiedByUser, List<Allowance> userAllowances, Year year,
      Allowance allowance) {
    userAllowances.remove(allowance);
    allowance.setComment("Deleting existing allowance for active year " + year.getYear());
    allowanceService.delete(allowance, modifiedByUser);
  }

  /**
   * Gets users leave profile and currently active years, and creates allowances
   * according with values from leave profile
   */
  private List<Allowance> createAllowanceForUserOnCreateUser(User user) {
    var openedActiveYears = yearRepository.findAllByYearGreaterThanEqualAndActiveTrue(LocalDate.now().getYear());
    List<Allowance> userAllowances = new ArrayList<>();
    for (Year year : openedActiveYears) {
      Allowance allowance = createAllowance(user, year);
      userAllowances.add(allowance);
    }

    return userAllowances;
  }

  private int calculateYearsOfService(User user) {
    return Period.between(user.getStartedWorkingDate(), user.getStartedWorkingAtExecomDate()).getYears();
  }

  /**
   * If year when user started working at execom is not the same as current year that means that
   * next year is open, and allowances are created solely based on users leave profile. If year is
   * same as current than this is the year when user started working at execom and allowances are
   * created based on remaining months in this year in reference to users leave profile. Allowance is
   * rounded to 4h witch is half a day, because that's the smallest period of time that can be taken
   * as annual leave.
   */
  private Allowance createAllowance(User user, Year year) {
    LeaveProfile leaveProfile = leaveProfileRepository.getOne(user.getLeaveProfile().getId());
    Allowance allowance = new Allowance();
    allowance.setUser(user);
    allowance.setYear(year);

    LocalDate startedWorkingAtExecomDate = user.getStartedWorkingAtExecomDate();
    if (startedWorkingAtExecomDate.getYear() != year.getYear()) {
      allowance.setAnnual(leaveProfile.getEntitlement());
      allowance.setTraining(leaveProfile.getTraining());
      allowance.setBonus(leaveProfile.getMaxBonusDays());
    } else {
      float entitlement = leaveProfile.getEntitlement();
      float entitlementPerMonth = entitlement / 12;
      float trainingEntitlement = leaveProfile.getTraining();
      float trainingEntitlementPerMonth = trainingEntitlement / 12;
      float totalMonthsWithCorrection = calculateTotalMonths(startedWorkingAtExecomDate);
      float remainingMonthsInYear = totalMonthsWithCorrection - startedWorkingAtExecomDate.getMonthValue();

      allowance.setAnnual(Math.round(entitlementPerMonth * remainingMonthsInYear / HALF_DAY) * HALF_DAY);
      allowance.setTraining(Math.round(trainingEntitlementPerMonth * remainingMonthsInYear / HALF_DAY) * HALF_DAY);
      allowance.setBonus(leaveProfile.getMaxBonusDays());
    }

    return allowance;
  }

  /**
   * Remaining months are calculated based on the day user started working in Execom. If starting
   * day is the first working day of the month (the first and second day may be a weekend) then
   * whole starting month is taken into consideration.
   */
  private float calculateTotalMonths(LocalDate date) {
    int dayOfMonth = date.getDayOfMonth();
    if (dayOfMonth == 1) {
      return 13;
    }
    boolean isStartDayMonday = date.getDayOfWeek().equals(DayOfWeek.MONDAY);
    if ((dayOfMonth == 2 || dayOfMonth == 3) && isStartDayMonday) {
      return 13;
    }
    return 12;
  }

  /**
   * Updates values for allowances for active years. Since Leave Profile was just updated, values
   * for already created allowances for currently active years need to be updated as well.
   * If date of Leave Profile update was before half year mark, only half of allowance bonus should
   * be added, and full amount if it was afterwards. Following years allowances receive full amount.
   *
   * @param user the User entity for which allowances should be updated.
   * @param previousLeaveProfile the LeaveProfile entity necessary for determining difference in allowance.
   * @param todaysDate year, month and date for today.
   */
  public void updateAllowanceForUserOnLeaveProfileUpdate(User user, LeaveProfile previousLeaveProfile,
      LocalDate todaysDate) {
    var hrAssistant = userRepository.findFirstByUserRole(UserRole.HR_ASSISTANT);
    var openedActiveYears = yearRepository.findAllByYearGreaterThanEqualAndActiveTrue(todaysDate.getYear());
    var userAllowances = allowanceService.getAllByUserId(user.getId());
    var nextYearBonus = user.getLeaveProfile().getEntitlement() - previousLeaveProfile.getEntitlement();
    int thisYearBonus = (MonthDay.from(todaysDate).isBefore(HALF_YEAR_DATE)) ? nextYearBonus : (nextYearBonus / 2);

    for (Year year : openedActiveYears) {
      for (Allowance allowance : userAllowances) {
        if (allowance.getYear().getYear() == year.getYear()) {
          if (todaysDate.getYear() == year.getYear()) {
            allowance.setManualAdjust(allowance.getManualAdjust() + thisYearBonus);
          } else {
            allowance.setManualAdjust(allowance.getManualAdjust() + nextYearBonus);
          }
          allowance.setComment(
              "Upon Leave Profile update, allowance for year '" + year.getYear() + "' was also updated");
          allowanceService.update(allowance, hrAssistant);
        }
      }
    }
  }

  /**
   * First, checks if the exact same push token for authenticated user already exists
   * If it doesn't exist, create new UserPushToken and return the new UserPushToken
   * If already exists, just pass him through and return the old UserPushToken
   */
  @Transactional
  public UserPushToken getUsersPushToken(User authUser, CreateTokenDto createTokenDto) {
    Optional<UserPushToken> authUserPushToken = userPushTokensRepository.findOneByUserAndPushToken(authUser,
        createTokenDto.getPushToken());

    return authUserPushToken.orElseGet(() -> creteNewUserPushToken(authUser, createTokenDto));
  }

  private UserPushToken creteNewUserPushToken(User authUser, CreateTokenDto createTokenDto) {
    UserPushToken userPushToken = new UserPushToken();
    userPushToken.setUser(authUser);
    userPushToken.setPushToken(createTokenDto.getPushToken());
    userPushToken.setPlatform(createTokenDto.getPlatform());
    userPushToken.setName(createTokenDto.getName());
    userPushToken.setCreateDateTime(LocalDateTime.now());
    userPushTokensRepository.save(userPushToken);

    return userPushToken;
  }

  @Transactional
  public void deleteUserPushToken(User authUser, Long id) {
    List<UserPushToken> userPushTokens = authUser.getUserPushTokens();
    for (UserPushToken userPushToken : userPushTokens) {
      if (userPushToken.getId().equals(id)) {
        userPushTokensRepository.delete(userPushToken);
      }
    }
  }

  void saveAuditInformation(OperationPerformed operationPerformed, User modifiedByUser, User modifiedUser) {
    var currentUserState = UserAudit.fromUser(modifiedUser);

    if (OperationPerformed.CREATE.equals(operationPerformed)) {
      auditInformationService.saveAudit(operationPerformed, modifiedByUser, null, null, currentUserState);
    } else {
      User userFromDb = userRepository.findByIdRequireNew(modifiedUser.getId());
      var previousUserState = UserAudit.fromUser(userFromDb);
      auditInformationService.saveAudit(operationPerformed, modifiedByUser, modifiedUser.getId(), previousUserState,
          currentUserState);
    }
  }

  public void validateUserIsTeamMemberOrApprover(User authUser, Team team) {
    var loggedUser = findById(authUser.getId());
    if (!loggedUser.getApproverTeams().contains(team) && !loggedUser.getTeam().equals(team)) {
      throw new UnauthorizedAccessException(
          "You must be either a member or approver in " + team.getName() + " to access this resources.");
    }
  }

  public Page<User> createPageableUsersList(List<User> users, Pageable pageable) {
    long start = pageable.getOffset();
    long end = start + pageable.getPageSize() > users.size() ? users.size() : start + pageable.getPageSize();

    return new PageImpl<>(users.subList((int) start, (int) end), pageable, users.size());
  }

  public Page<User> findAllByUserStatusTypeSorted(List<UserStatusType> userStatusTypes,
      Pageable pageable, String sort, Sort.Direction direction) {
    UserStatusType currentUserStatusType = userStatusTypes.get(0);
    List<User> users = userRepository.findAll(new Sort(direction, sort))
                                     .stream()
                                     .filter(byUserStatus(currentUserStatusType)
                                     .and(User::hasAnyRoleOtherThanHrAssistant))
                                     .collect(Collectors.toList());

    return createPageableUsersList(users, pageable);
  }

  private Predicate<User> byUserStatus(UserStatusType userStatusType) {
    return user -> user.getUserStatusType().equals(userStatusType);
  }
}
