package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.MailCreationException;
import eu.execom.hawaii.exceptions.MailSendingException;
import eu.execom.hawaii.exceptions.MailTemplateException;
import eu.execom.hawaii.model.Email;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.batch.AppogeeLeaveRequestImportData;
import eu.execom.hawaii.model.batch.CalendarImportData;
import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.model.metric.HardwareMetrics;
import eu.execom.hawaii.model.metric.HttpEndpointMetrics;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.util.EmailData;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmailService {

  private static final String EMPTY_STRING = "";
  private static final String USER_NAME = "userName";
  private static final String USER_EMAIL = "userEmail";
  private static final String ABSENCE_NAME = "absenceName";
  private static final String START_DATE = "startDate";
  private static final String END_DATE = "endDate";
  private static final String NUMBER_OF_REQUESTED_DAYS = "numberOfRequestedDays";
  private static final String REASON = "reason";
  private static final String TEAM_NAME = "teamName";
  private static final String STATUS = "status";
  private static final String APPROVER = "approver";
  private static final String REQUEST_ID = "requestId";
  private static final String SERVER_NAME = "serverName";
  private static final String SYSTEM_CPU_USAGE = "systemCpuUsage";
  private static final String JVM_CPU_USAGE = "jvmCpuUsage";
  private static final String MEMORY_USAGE = "memoryUsage";
  private static final String ENDPOINT_URL = "endpointUrl";
  private static final String NUMBER_OF_FAILED_REQUESTS = "numberOfFailedRequests";
  private static final String EXCEPTION_NAME = "exceptionName";
  private static final UserAdminPermission[] ADMIN_PERMISSIONS = {UserAdminPermission.SUPER, UserAdminPermission.BASIC};

  @Value("${server.name}")
  private String serverName;

  private JavaMailSender emailSender;

  private Configuration freemarkerConfig;

  private UserRepository userRepository;

  @Autowired
  public EmailService(JavaMailSender emailSender, Configuration freemarkerConfig, UserRepository userRepository) {
    this.emailSender = emailSender;
    this.freemarkerConfig = freemarkerConfig;
    this.userRepository = userRepository;
  }

  /**
   * Create email on request creation and sends for approval to team approvers.
   *
   * @param request the Request.
   */
  @Async
  public void sendEmailForApproval(Request request) {
    List<String> approversEmail = getUsersEmailAddresses(request.getUser().getApprovers());
    var emailData = EmailData.getRequestCreatedEmailData(request);
    String userName = request.getUser().getFullName();
    var requestId = request.getId().toString();
    String absenceName = request.getAbsence().getName();
    int numberOfRequestedDays = request.getDays().size();
    LocalDate startDate = request.getDays().get(0).getDate();
    LocalDate endDate = request.getDays().get(numberOfRequestedDays - 1).getDate();
    var reason = request.getReason();
    Map<String, Object> templateData = Map.of(USER_NAME, userName, SERVER_NAME, serverName, REQUEST_ID, requestId,
            ABSENCE_NAME, absenceName, START_DATE, startDate, END_DATE, endDate, NUMBER_OF_REQUESTED_DAYS,
            numberOfRequestedDays, REASON, reason);

    sendEmail(new Email(approversEmail, emailData, templateData));
  }

  /**
   * Create email to send notification that users leave profile was updated
   *
   * @param user the User.
   */
  @Async
  public void sendLeaveProfileUpdateEmail(User user) {
    sendAutomaticUpdateForReviewEmail(user, EmailData.LEAVE_PROFILE_UPDATE);
  }

  /**
   * Create email to send notification to hr managers that requests for users with the same name
   * can not be updated with Google Calendar data.
   *
   * @param user the User.
   */
  @Async
  public void sendCalendarImportOmittedDueToSameFullName(User user) {
    sendAutomaticUpdateForReviewEmail(user, EmailData.REVIEW_CALENDAR_REQUEST_SAME_FULL_NAME);
  }

  /**
   * Create email to send notification to hr managers that request from Google Calendar
   * can not be found in database.
   *
   * @param user the User.
   * @param calendarImport used to find out event start and end date.
   */
  @Async
  public void sendCalendarImportOmittedDueToNotFoundInDatabase(User user, CalendarImportData calendarImport) {
    sendAutomaticUpdateForReviewEmail(user, calendarImport, EmailData.REVIEW_CALENDAR_REQUEST_NOT_FOUND_IN_DATABASE);
  }

  /**
   * Create email to send notification to hr managers that request from Google Calendar
   * have total hours different than request in database based on Appogee data.
   *
   * @param user the User.
   * @param calendarImport used to find out event start and end date.
   */
  @Async
  public void sendCalendarImportOmittedDueToHoursMismatch(User user, CalendarImportData calendarImport) {
    sendAutomaticUpdateForReviewEmail(user, calendarImport, EmailData.REVIEW_CALENDAR_REQUEST_HOURS_MISMATCH);
  }

  /**
   * Create email to send notification to hr managers that request from Google Calendar
   * can not be uniquely identifiable with existing requests in database.
   *
   * @param user the User.
   * @param calendarImport used to find out event start and end date.
   */
  @Async
  public void sendCalendarImportOmittedDueToNotUniquelyIdentifiable(User user, CalendarImportData calendarImport) {
    sendAutomaticUpdateForReviewEmail(user, calendarImport, EmailData.REVIEW_CALENDAR_REQUEST_NOT_UNIQUELY_IDENTIFIABLE);
  }

  /**
   * Create email to send notification to hr managers that request can not be imported because it has no working days.
   *
   * @param leaveImport used to find out request start and end date.
   */
  @Async
  public void sendRequestImportDueToNoWorkingDays(AppogeeLeaveRequestImportData leaveImport) {
    sendAutomaticUpdateForReviewEmail(leaveImport);
  }

  /**
   * Send notification that user received bonus allowance for relevant work experience.
   *
   * @param user the User.
   */
  @Async
  public void sendBonusAllowanceUpdateEmail(User user) {
    sendAutomaticUpdateForReviewEmail(user, EmailData.REVIEW_BONUS_ALLOWANCE_UPDATE);
  }

  private void sendAutomaticUpdateForReviewEmail(User user, EmailData emailData) {
    List<User> hrManagers = userRepository.findAllByUserStatusTypeAndUserRole(UserStatusType.ACTIVE, UserRole.HR_MANAGER);
    List<String> hrManagersEmails = getUsersEmailAddresses(hrManagers);
    String fullName = user.getFullName();
    Map<String, Object> templateData = Map.of(USER_NAME, fullName, SERVER_NAME, serverName);

    sendEmail(new Email(hrManagersEmails, emailData, templateData));
  }

  private void sendAutomaticUpdateForReviewEmail(AppogeeLeaveRequestImportData leaveImport) {
    List<User> hrManagers = userRepository.findAllByUserStatusTypeAndUserRole(UserStatusType.ACTIVE, UserRole.HR_MANAGER);
    List<String> hrManagersEmails = getUsersEmailAddresses(hrManagers);
    String userEmail = leaveImport.getEmail();
    LocalDate startDate = leaveImport.getStartDate();
    LocalDate endDate = leaveImport.getEndDate();

    Map<String, Object> templateData = Map.of(USER_EMAIL, userEmail, START_DATE, startDate,
        END_DATE, endDate, SERVER_NAME, serverName);

    sendEmail(new Email(hrManagersEmails, EmailData.REVIEW_REQUEST_NO_WORKING_DAYS, templateData));
  }

  private void sendAutomaticUpdateForReviewEmail(User user, CalendarImportData calendarImport, EmailData emailData) {
    List<User> hrManagers = userRepository.findAllByUserStatusTypeAndUserRole(UserStatusType.ACTIVE, UserRole.HR_MANAGER);
    List<String> hrManagersEmails = getUsersEmailAddresses(hrManagers);
    String userEmail = user.getEmail();
    LocalDate startDate = calendarImport.getEventStart().toLocalDate();
    LocalDate endDate = calendarImport.getEventEnd().toLocalDate();

    Map<String, Object> templateData = Map.of(USER_EMAIL, userEmail, START_DATE, startDate,
        END_DATE, endDate, SERVER_NAME, serverName);

    sendEmail(new Email(hrManagersEmails, emailData, templateData));
  }

  @Async
  public void sendAllowanceUpdateEmail(User user) {
    List<String> userEmail = Collections.singletonList(user.getEmail());
    Map<String, Object> templateData = Map.of(SERVER_NAME, serverName);

    sendEmail(new Email(userEmail, EmailData.BONUS_ALLOWANCE_UPDATE, templateData));
  }

  /**
   * Create email after team approver action on given request and send to request user.
   *
   * @param request the Request.
   */
  @Async
  public void sendRequestStatusUpdateEmail(Request request) {
    List<String> userEmail = Collections.singletonList(request.getUser().getEmail());
    EmailData emailData = EmailData.getRequestCreatedEmailData(request);
    String userName = request.getUser().getFullName();
    String status = request.getRequestStatus().toString().toLowerCase();
    String approver = getApprover(request);
    String absenceName = request.getAbsence().getName();
    int numberOfRequestedDays = request.getDays().size();
    LocalDate startDate = request.getDays().get(0).getDate();
    LocalDate endDate = request.getDays().get(numberOfRequestedDays - 1).getDate();
    String reason = request.getReason();
    var requestId = request.getId().toString();

    Map<String, Object> templateData = Map.of(USER_NAME, userName, STATUS, status, APPROVER, approver, ABSENCE_NAME,
            absenceName, START_DATE, startDate, END_DATE, endDate, NUMBER_OF_REQUESTED_DAYS, numberOfRequestedDays,
            REASON, reason, SERVER_NAME, serverName, REQUEST_ID, requestId);

    sendEmail(new Email(userEmail, emailData, templateData));
  }

  private String getApprover(Request request) {
    var currentlyApprovedBy = request.getCurrentlyApprovedBy();
    return currentlyApprovedBy.size() > 1 ? "all approvers" : currentlyApprovedBy.get(0).getFullName();
  }

  /**
   * Sends warning email when resources usage is high.
   *
   * @param hardwareMetrics recent average HardwareMetrics resources usage.
   */
  public void sendHighResourcesUsageWarningEmail(HardwareMetrics hardwareMetrics) {
    List<String> adminEmails = getAdminEmails();
    EmailData emailData = EmailData.HIGH_RESOURCES_USAGE_ALERT;
    double systemCpuUsagePercent = hardwareMetrics.getSystemCpuUsage() * 100.0;
    double jvmCpuUsagePercent = hardwareMetrics.getJvmCpuUsage() * 100.0;
    double memoryUsage = hardwareMetrics.getMemoryUsage();

    Map<String, Object> templateData = Map.of(SERVER_NAME, serverName, SYSTEM_CPU_USAGE, systemCpuUsagePercent,
        JVM_CPU_USAGE, jvmCpuUsagePercent, MEMORY_USAGE, memoryUsage);

    sendEmail(new Email(adminEmails, emailData, templateData));
  }

  /**
   * Sends warning email when HTTP endpoint fails multiple times.
   *
   * @param httpEndpointMetrics HttpEndpointMetrics object.
   */
  public void sendHttpEndpointFailedWarningEmail(HttpEndpointMetrics httpEndpointMetrics) {
    List<String> adminsEmail = getAdminEmails();
    EmailData emailData = EmailData.HTTP_ENDPOINT_FAILED_ALERT;
    String endpointUrl = httpEndpointMetrics.getEndpointUrl();
    int numberOfFailedRequests = httpEndpointMetrics.getNumberOfRequests();
    String exceptionName = httpEndpointMetrics.getException();

    Map<String, Object> templateData = Map.of(SERVER_NAME, serverName, ENDPOINT_URL, endpointUrl,
        NUMBER_OF_FAILED_REQUESTS, numberOfFailedRequests, EXCEPTION_NAME, exceptionName);

    sendEmail(new Email(adminsEmail, emailData, templateData));
  }

  /**
   * Returns admins email addresses.
   */
  private List<String> getAdminEmails() {
    List<User> admins = userRepository.findAllByUserAdminPermissionInOrderByUserAdminPermissionDesc(
        Arrays.asList(ADMIN_PERMISSIONS));
    return getUsersEmailAddresses(admins);
  }

  /**
   * Send email on user submitting sickness request to recipients for given request.
   *
   * @param request the Request.
   */
  @Async
  public void sendSicknessRequestCreatedEmail(Request request) {
    List<String> recipients = getUsersEmailAddresses(request.getUser().getApprovers());
    recipients.addAll(getNotifiersEmailAddresses(request.getUser().getTeam().getSicknessRequestEmails()));
    if (request.getUser().getTeam().isSendEmailToTeammatesForSicknessRequestEnabled()) {
      recipients.addAll(getTeammatesEmailAddresses(request));
    }

    sendEmail(EmailData.SICKNESS, recipients, request);
  }

  private List<String> getUsersEmailAddresses(List<User> users) {
    return users.stream()
                .map(User::getEmail)
                .collect(Collectors.toList());
  }

  /**
   * Send email on user approved annual leave request to recipients for given request.
   *
   * @param request the Request.
   */
  @Async
  public void sendAnnualRequestCreatedEmail(Request request) {
    List<String> recipients = getNotifiersEmailAddresses(request.getUser().getTeam().getAnnualRequestEmails());
    if (request.getUser().getTeam().isSendEmailToTeammatesForAnnualRequestEnabled()) {
      recipients.addAll(getTeammatesEmailAddresses(request));
    }

    sendEmail(EmailData.ANNUAL, recipients, request);
  }

  /**
   * Send email on user approved bonus leave request to recipients for given request.
   *
   * @param request the Request.
   */
  @Async
  public void sendBonusRequestCreatedEmail(Request request) {
    List<String> recipients = getNotifiersEmailAddresses(request.getUser().getTeam().getBonusRequestEmails());
    if (request.getUser().getTeam().isSendEmailToTeammatesForBonusRequestEnabled()) {
      recipients.addAll(getTeammatesEmailAddresses(request));
    }

    sendEmail(EmailData.BONUS, recipients, request);
  }

  private List<String> getNotifiersEmailAddresses(String annualRequestEmails) {
    return Optional.ofNullable(annualRequestEmails)
                   .map(this::getRecipientsEmails)
                   .stream()
                   .flatMap(Collection::stream)
                   .collect(Collectors.toList());
  }

  private List<String> getRecipientsEmails(String emails) {
    return Arrays.asList(emails.split("\\s*,\\s*"));
  }

  private List<String> getTeammatesEmailAddresses(Request request) {
    var userEmail = request.getUser().getEmail();
    return request.getUser()
                  .getTeam()
                  .getUsers()
                  .stream()
                  .map(User::getEmail)
                  .filter(isTeammateEmail(userEmail))
                  .collect(Collectors.toList());
  }

  private Predicate<String> isTeammateEmail(String requestUserEmail) {
    return email -> !requestUserEmail.equals(email);
  }

  private void sendEmail(EmailData emailData, List<String> recipients, Request request) {
    if (!recipients.isEmpty()) {
      Map<String, Object> templateData = collectTemplateData(request);
      Email email = new Email(recipients, emailData, templateData);

      sendEmail(email);
    }
  }

  private Map<String, Object> collectTemplateData(Request request) {
    String userName = request.getUser().getFullName();
    String teamName = request.getUser().getTeam().getName();
    int numberOfRequestedDays = request.getDays().size();
    LocalDate startDate = request.getDays().get(0).getDate();
    LocalDate endDate = request.getDays().get(numberOfRequestedDays - 1).getDate();
    String reason = request.getAbsence() == null ? EMPTY_STRING : request.getAbsence().getName();

    return Map.of(USER_NAME, userName, TEAM_NAME, teamName, NUMBER_OF_REQUESTED_DAYS, numberOfRequestedDays, START_DATE,
        startDate, END_DATE, endDate, REASON, reason, SERVER_NAME, serverName);
  }

  private void sendEmail(Email email) {
    MimeMessage message = emailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message);
    Template template = getTemplate(email.getEmailData().getTemplate());

    var emailText = processTemplateIntoString(template, email.getTemplateData());
    createEmail(email, helper, emailText);

    try {
      emailSender.send(message);
    } catch (MailException exception) {
      log.error("Error on sending email to: " + email.getTo(), exception);
      throw new MailSendingException("Error on sending email to: " + email.getTo());
    }
  }

  private Template getTemplate(String templateName) {
    Template template = null;
    try {
      template = freemarkerConfig.getTemplate(templateName);
    } catch (IOException exception) {
      log.error("Unable to find template with name: {}", templateName);
      throw new MailTemplateException("Unable to find template with name: " + templateName);
    }
    return template;
  }

  private void createEmail(Email email, MimeMessageHelper helper, String emailText) {
    var listSize = email.getTo().size();
    var userFullName = email.getTemplateData().get("userName");
    try {
      helper.setTo(email.getTo().toArray(new String[listSize]));
      helper.setSubject(email.getEmailData().getSubject((String) userFullName));
      helper.getMimeMessage().setContent(emailText, "text/html;charset=utf-8");
    } catch (MessagingException exception) {
      log.error("Error creating email with details: {}", exception.getMessage());
      throw new MailCreationException("Error creating email with details: " + exception.getMessage());
    }
  }

  private String processTemplateIntoString(Template template, Map<String, Object> templateData) {
    String emailText = EMPTY_STRING;
    try {
      emailText = FreeMarkerTemplateUtils.processTemplateIntoString(Objects.requireNonNull(template), templateData);
    } catch (IOException | TemplateException exception) {
      log.error("Error processing template to string with details: {}", exception.getMessage());
      throw new MailTemplateException("Error processing template to string with details: " + exception.getMessage());
    }
    return emailText;
  }
}
