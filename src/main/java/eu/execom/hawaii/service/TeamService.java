package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.audit.TeamAudit;
import eu.execom.hawaii.model.enumerations.OperationPerformed;
import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import eu.execom.hawaii.repository.TeamRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Team management service.
 */
@Slf4j
@Service
public class TeamService {

  private static final int MINIMUM_QUERY_LENGTH = 3;

  private TeamRepository teamRepository;
  private AuditInformationService auditInformationService;
  private UserService userService;

  @Autowired
  public TeamService(TeamRepository teamRepository, AuditInformationService auditInformationService,
      UserService userService) {
    this.teamRepository = teamRepository;
    this.auditInformationService = auditInformationService;
    this.userService = userService;
  }

  /**
   * Retrieves a list of all teams from repository sorted by team name.
   *
   * @return a list of all teams.
   */
  public Page<Team> findAll(Pageable pageable) {
    return teamRepository.findAllByEditableTrueOrderByName(pageable);
  }

  /**
   * Retrieves a team with a specific id.
   *
   * @param id Team id.
   * @return Team with provided id if exists.
   */
  public Team getById(Long id) {
    return teamRepository.getOne(id);
  }

  /**
   * Retrieves a list of teams from repository by given team name query.
   *
   * @param searchQuery team name query.
   * @param pageable the Pageable information about size per page and number of page.
   * @return a list of teams searched by name.
   */
  public Page<Team> searchTeamsByName(String searchQuery, Pageable pageable) {
    return searchQuery.length() < MINIMUM_QUERY_LENGTH ?
        Page.empty() :
        teamRepository.findByNameContainingAndEditableTrue(searchQuery, pageable);
  }

  /**
   * Retrieves a list of all user teams based on users role, membership and approval rights for team
   * calendar list view.
   *
   * @param authUser logged user.
   * @return a list of all user teams
   */
  public List<Team> getTeamsBasedOnUserRole(User authUser) {
    List<Team> teams = new ArrayList<>();
    switch (authUser.getUserRole()) {
      case USER:
        if (!authUser.isApprover()) {
          break;
        }
        var approverTeams = userService.findById(authUser.getId()).getApproverTeams();
        teams.addAll(approverTeams);
        teams.add(authUser.getTeam());
        teams.sort(byTeamName());
        break;
      case OFFICE_MANAGER:
      case HR_MANAGER:
        teams = findAll(Pageable.unpaged()).stream().collect(Collectors.toList());
        break;
      default:
        throw new IllegalArgumentException("Unsupported user role: " + authUser.getUserRole());
    }

    return teams;
  }

  /**
   * Saves the provided Team to repository. Validates team with given name doesn't exist in database.
   * Makes audit of that save.
   *
   * @param team the Team entity to be persisted.
   * @param modifiedByUser user that made changes to that Team entity.
   * @return saved Team.
   */
  @Transactional
  public Team create(Team team, User modifiedByUser) {
    validateTeamNameIsUnique(team.getName(), "");
    teamRepository.create(team);

    var users = team.getUsers();
    users.forEach(user -> user.setTeam(team));
    saveAuditInformation(OperationPerformed.CREATE, modifiedByUser, team);
    saveAuditForUpdatedUsers(modifiedByUser, users);

    return team;
  }

  /**
   * Saves the provided Team to repository.
   *
   * @param updatedTeam the Team entity to be persisted.
   * @param modifiedByUser user that made change to Team entity.
   * @return saved Team.
   */
  @Transactional
  public Team update(Team updatedTeam, User modifiedByUser) {
    Team databaseTeam = teamRepository.getOne(updatedTeam.getId());

    validateTeamIsEditable(databaseTeam, "You are not allowed to update this team.");
    validateTeamNameIsUnique(updatedTeam.getName(), databaseTeam.getName());
    validateUsersAreNotRemoved(updatedTeam, databaseTeam);

    List<User> teamMembers = updatedTeam.getUsers();
    teamMembers.forEach(user -> user.setTeam(updatedTeam));

    List<User> teamApprovers = updatedTeam.getTeamApprovers();
    List<User> teamMembersAndApprovers = new ArrayList<>();
    teamMembersAndApprovers.addAll(teamMembers);
    teamMembersAndApprovers.addAll(teamApprovers);
    for (User user : teamMembersAndApprovers) {
      UserAdminPermission userAdminPermission = userService.findById(user.getId()).getUserAdminPermission();
      user.setUserAdminPermission(userAdminPermission);
    }
    saveAuditInformation(OperationPerformed.UPDATE, modifiedByUser, updatedTeam);
    saveAuditForNewMembers(updatedTeam, modifiedByUser, databaseTeam);

    return teamRepository.save(updatedTeam);
  }

  private void validateTeamNameIsUnique(String teamName, String oldTeamName) {
    if (!oldTeamName.equals(teamName) && teamRepository.existsByName(teamName)) {
      throw new EntityExistsException("Team with name " + teamName + " already exists in database.");
    }
  }

  private void validateUsersAreNotRemoved(Team updatedTeam, Team databaseTeam) {
    if (!updatedTeam.getUsers().containsAll(databaseTeam.getUsers())) {
      throw new ActionNotAllowedException("You are not allowed to remove users from team without assigning a new one");
    }
  }

  /**
   * Deletes Team from database if its member list is empty.
   *
   * @param modifiedByUser user that made change to Team entity.
   * @param id - the team id.
   */
  @Transactional
  public void delete(Long id, User modifiedByUser) {
    var team = getById(id);
    validateTeamIsEditable(team, "You are not allowed to delete this team.");
    if (team.getUsers().isEmpty()) {
      teamRepository.deleteById(id);
      saveAuditInformation(OperationPerformed.DELETE, modifiedByUser, team);
    } else {
      logAndThrowActionNotAllowedException(team);
    }
  }

  private void validateTeamIsEditable(Team team, String message) {
    if (!team.isEditable()) {
      throw new ActionNotAllowedException(message);
    }
  }

  private void logAndThrowActionNotAllowedException(Team team) {
    log.error("Team: '{}', still contains '{}' members, team needs to be empty before it can be deleted.",
        team.getName(), team.getUsers().size());
    throw new ActionNotAllowedException(
        "Team still contains members, team needs to be empty before it can be deleted.");
  }

  private void saveAuditInformation(OperationPerformed operationPerformed, User modifiedByUser, Team team) {
    TeamAudit previousTeamState = getPreviousTeamState(operationPerformed, team);
    TeamAudit currentTeamState = TeamAudit.fromTeam(team);

    auditInformationService.saveAudit(operationPerformed, modifiedByUser, team.getId(), previousTeamState,
        currentTeamState);
  }

  private TeamAudit getPreviousTeamState(OperationPerformed operationPerformed, Team team) {
    if (OperationPerformed.CREATE.equals(operationPerformed)) {
      return new TeamAudit();
    } else if (OperationPerformed.DELETE.equals(operationPerformed)) {
      return TeamAudit.fromTeam(team);
    }
    Team oldTeam = teamRepository.findByIdJoinFetchTeamApprovers(team.getId());

    return TeamAudit.fromTeam(oldTeam);
  }

  private void saveAuditForNewMembers(Team updatedTeam, User modifiedByUser, Team team) {
    var newMembers = getNewMembers(updatedTeam, team);
    saveAuditForUpdatedUsers(modifiedByUser, newMembers);
  }

  private List<User> getNewMembers(Team updatedTeam, Team oldTeam) {
    return updatedTeam.getUsers()
                      .stream()
                      .filter(member -> !oldTeam.getUsers().contains(member))
                      .collect(Collectors.toList());
  }

  private void saveAuditForUpdatedUsers(User modifiedByUser, List<User> users) {
    users.forEach(user -> userService.saveAuditInformation(OperationPerformed.UPDATE, modifiedByUser, user));
  }

  private Comparator<Team> byTeamName() {
    return Comparator.comparing(team -> team.getName().toLowerCase());
  }

}
