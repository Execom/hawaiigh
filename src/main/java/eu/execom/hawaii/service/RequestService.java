package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.NotAuthorizedApprovalException;
import eu.execom.hawaii.exceptions.RequestAlreadyHandledException;
import eu.execom.hawaii.exceptions.UnauthorizedAccessException;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.PublicHoliday;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.audit.RequestAudit;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.Duration;
import eu.execom.hawaii.model.enumerations.OperationPerformed;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import eu.execom.hawaii.repository.DayRepository;
import eu.execom.hawaii.repository.RequestRepository;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RequestService {

  private static final String REQUESTS_CACHE = "requestsCache";
  private static final String APPROVE = "approve";
  private static final String CANCEL = "cancel";
  private static final String REJECT = "reject";

  private RequestRepository requestRepository;
  private UserRepository userRepository;
  private DayRepository dayRepository;
  private TeamRepository teamRepository;
  private AllowanceService allowanceService;
  private GoogleCalendarService googleCalendarService;
  private EmailService emailService;
  private SendNotificationsService sendNotificationsService;
  private AuditInformationService auditInformationService;
  private UserService userService;
  private TeamService teamService;
  private PublicHolidayService publicHolidayService;

  @Autowired
  public RequestService(RequestRepository requestRepository, UserRepository userRepository, DayRepository dayRepository,
      TeamRepository teamRepository, AllowanceService allowanceService, GoogleCalendarService googleCalendarService,
      EmailService emailService, SendNotificationsService sendNotificationsService,
      AuditInformationService auditInformationService, UserService userService, TeamService teamService,
      PublicHolidayService publicHolidayService) {
    this.requestRepository = requestRepository;
    this.userRepository = userRepository;
    this.dayRepository = dayRepository;
    this.teamRepository = teamRepository;
    this.allowanceService = allowanceService;
    this.googleCalendarService = googleCalendarService;
    this.emailService = emailService;
    this.sendNotificationsService = sendNotificationsService;
    this.auditInformationService = auditInformationService;
    this.userService = userService;
    this.teamService = teamService;
    this.publicHolidayService = publicHolidayService;
  }

  public List<Request> findAllByDateRange(LocalDate date) {
    var startDate = date.withDayOfMonth(1);
    var endDate = date.withDayOfMonth(date.lengthOfMonth());
    List<Request> requests = requestRepository.findAll();

    return requests.stream()
                   .filter(isBetween(startDate, endDate))
                   .sorted(byRequestStartingDate())
                   .collect(Collectors.toList());
  }

  public List<Request> getPendingRequestsFromTeams(User loggedUser) {
    return loggedUser.getApproverTeams()
                     .stream()
                     .map(Team::getUsers)
                     .flatMap(List::stream)
                     .map(User::getRequests)
                     .flatMap(List::stream)
                     .filter(approvedRequests(loggedUser)
                     .and(Request::isPendingOrCancellationPending))
                     .sorted(Comparator.comparing((Request req) -> req.getDays().get(0).getDate()))
                     .collect(Collectors.toList());
  }

  private Predicate<Request> approvedRequests(User loggedUser) {
    return request -> !request.getCurrentlyApprovedBy().contains(loggedUser);
  }

  /**
   * Retrieves a list of requests by userId from repository.
   *
   * @param userId the User id.
   * @return a list of all requests for given user.
   */
  public List<Request> findAllByUser(Long userId) {
    return requestRepository.findAllByUserId(userId)
                            .stream()
                            .sorted(byRequestStartingDate())
                            .collect(Collectors.toList());
  }

  /**
   * Retrieves a list of requests by userId for given year from repository.
   *
   * @param userId the userId
   * @param year requested year
   * @param authUser logged user
   * @return a list of requests for user in given year.
   */
  public List<Request> findAllByUserForYear(Long userId, Integer year, User authUser) {
    year = year == null ? LocalDate.now().getYear() : year;
    userId = userId == null ? authUser.getId() : userService.getUser(userId, authUser).getId();
    var startDate = LocalDate.of(year, 1, 1);
    var endDate = LocalDate.of(year, 12, 31);

    return findAllByUserWithinDates(startDate, endDate, userId);
  }

  /**
   * Retrieves a list of request by given dates, ordered by request start date descending.
   *
   * @param startDate from date.
   * @param endDate to date.
   * @return a list of requests.
   */
  public List<Request> findAllByUserWithinDates(LocalDate startDate, LocalDate endDate, Long userId) {
    var user = userService.findById(userId);
    List<Request> requests = requestRepository.findAllRequestsByUserIdOrderByStartingDateDesc(userId);
    requests.forEach(request -> validateUserIsAuthorizedForGivenAction(user, request));

    return requests.stream().filter(isBetween(startDate, endDate)).collect(Collectors.toList());
  }

  public List<Request> findAllByTeamApproversWithinDates(Long teamId, YearMonth requestedYearMonth){
    var teamApprovers = teamService.getById(teamId).getTeamApprovers();
    requestedYearMonth = requestedYearMonth == null ? YearMonth.now() : requestedYearMonth;
    var startDate = requestedYearMonth.atDay(1);
    var endDate = requestedYearMonth.atEndOfMonth();

    return collectRequestsForUsersInGivenDateRange(teamApprovers, startDate, endDate);
  }

  private Predicate<Request> isBetween(LocalDate startDate, LocalDate endDate) {
    return request -> checkAnyDayWithinDates(request.getDays(), startDate, endDate);
  }

  private boolean checkAnyDayWithinDates(List<Day> days, LocalDate startDate, LocalDate endDate) {
    return days.stream()
               .anyMatch(day -> (day.getDate().isAfter(startDate) || day.getDate().isEqual(startDate)) && (
                   day.getDate().isBefore(endDate) || day.getDate().isEqual(endDate)));
  }

  /**
   * Retrieves a list of requests for all users from requested team for requested month.
   * If team id is not given, return a list of requests for all users. If month is not given,
   * return list of requests for current month.
   *
   * @param teamId the Team id.
   * @param requestedYearMonth the YearMonth.
   * @return a list of all requests for given team.
   */
  public List<Request> findAllByTeamByMonthOfYear(Long teamId, YearMonth requestedYearMonth) {
    requestedYearMonth = requestedYearMonth == null ? YearMonth.now() : requestedYearMonth;
    var startDate = requestedYearMonth.atDay(1);
    var endDate = requestedYearMonth.atEndOfMonth();
    List<User> users = teamId == null ? userRepository.findAll() : teamRepository.getOne(teamId).getUsers();

    return collectRequestsForUsersInGivenDateRange(users, startDate, endDate);
  }

  private List<Request> collectRequestsForUsersInGivenDateRange(List<User> users, LocalDate startDate,
      LocalDate endDate) {
    return users.stream()
                .map(user -> findAllByUserWithinDates(startDate, endDate, user.getId()))
                .flatMap(List::stream)
                .filter(withoutCanceledRejectedBonus())
                .map(this::hideSicknessName)
                .sorted(byUserFullName())
                .collect(Collectors.toList());
  }

  Predicate<Request> withoutCanceledRejectedBonus() {
    return request -> !(request.isCanceled() || request.isRejected() || request.getAbsence().isBonusDays());
  }

  private Request hideSicknessName(Request request) {
    if (request.getAbsence().isSickness()) {
      request.getAbsence().setName(AbsenceType.SICKNESS.getDescription());
    }

    return request;
  }

  private Comparator<Request> byUserFullName() {
    return Comparator.comparing(request -> request.getUser().getFullName());
  }

  /**
   * Retrieves a list of all requests by status of request from repository.
   *
   * @param requestStatus status of request.
   * @return a list of all requests for given request status.
   */
  public List<Request> findAllByRequestStatus(RequestStatus requestStatus) {
    return requestRepository.findAllByRequestStatus(requestStatus)
                            .stream()
                            .sorted(byRequestStartingDate())
                            .collect(Collectors.toList());
  }

  /**
   * Retrieves a list of all requests by absence type from repository.
   *
   * @param absenceType a type of Absence.
   * @return a list of all requests for given absence type.
   */
  public List<Request> findAllByAbsenceType(AbsenceType absenceType) {
    return requestRepository.findAllByAbsenceAbsenceType(absenceType)
                            .stream()
                            .sorted(byRequestStartingDate())
                            .collect(Collectors.toList());
  }

  private Comparator<Request> byRequestStartingDate() {
    return Comparator.comparing(request -> request.getDays().get(0).getDate());
  }

  /**
   * Retrieves a Request with a specific id if user is allowed to access particular request,
   * which depends on his role.
   *
   * @param id Request id.
   * @return Request for given id if exist.
   */
  public Request getById(Long id, User authUser) {
    Request request = requestRepository.getOne(id);
    validateUserIsAuthorizedForGivenAction(authUser, request);

    return request;
  }

  public void validateUserIsAuthorizedToAccessRequest(Long requestId, User authUser) {
    var request = requestRepository.getOne(requestId);
    validateUserIsAuthorizedForGivenAction(authUser, request);
  }

  /**
   * Checks if user has a security clearance to perform given action.
   *
   * @param user for which security clearance is verified.
   * @param request that user is trying to get.
   */
  private void validateUserIsAuthorizedForGivenAction(User user, Request request) {
    boolean isLoggedUserRequestOwner = user.equals(request.getUser());

    switch (user.getUserRole()) {
      case OFFICE_MANAGER:
      case USER:
        if (!user.isApprover()) {
          break;
        }
        var usersTeam = request.getUser().getTeam();
        var approverTeams = userRepository.getOne(user.getId()).getApproverTeams();
        if (!approverTeams.contains(usersTeam) && !isLoggedUserRequestOwner) {
          throw new UnauthorizedAccessException("You are not authorized to access this request.");
        }
        break;
      case HR_MANAGER:
        break;
      default:
        throw new IllegalArgumentException("Unsupported user role: " + user.getUserRole());
    }
  }

  /**
   * Retrieves a first and last requests year.
   *
   * @return a Map of first and last year.
   */
  public Map<String, Integer> getFirstAndLastRequestsYear() {
    var firstDayRequest = dayRepository.findFirstByOrderByDateAsc();
    var lastDayRequest = dayRepository.findFirstByOrderByDateDesc();
    var firstYear = firstDayRequest.getDate().getYear();
    var lastYear = lastDayRequest.getDate().getYear();

    Map<String, Integer> firstAndLastDate = new LinkedHashMap<>();
    firstAndLastDate.put("first", firstYear);
    firstAndLastDate.put("last", lastYear);

    return firstAndLastDate;
  }

  /**
   * Save the provided request to repository, with setting initial status
   * of request depending of absence type SICKNESS or any other.
   * Also applies leave days from request to pending field on user's allowance.
   * Makes audit of that save.
   *
   * @param newRequest the Request entity to be persisted.
   * @return a saved request with id.
   */
  @CacheEvict(value = REQUESTS_CACHE, key = "#newRequest.user.id")
  @Transactional(isolation = Isolation.READ_COMMITTED)
  public Request create(Request newRequest, User authUser) {
    assignValuesToNewRequest(newRequest);
    checkIfRequestDaysAlreadyExist(newRequest);
    createRequest(newRequest);

    googleCalendarService.handleCreatedRequest(newRequest);
    saveAuditInformation(OperationPerformed.CREATE, authUser, newRequest, new RequestAudit());

    return newRequest;
  }

  private void assignValuesToNewRequest(Request newRequest) {
    filterRequestDays(newRequest);
    User requestOwner = userRepository.getOne(newRequest.getUser().getId());
    newRequest.setUser(requestOwner);
    newRequest.setSubmissionTime(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES));
    newRequest.setCurrentlyApprovedBy(new ArrayList<>());
  }

  private void filterRequestDays(Request request) {
    List<Day> requestDays = request.getDays();
    List<LocalDate> publicHolidayDates = getPublicHolidayDatesInRange(requestDays);
    List<Day> filteredRequestDays =
        requestDays.stream()
            .filter(isWorkingDay().and(day -> !publicHolidayDates.contains(day.getDate())))
            .collect(Collectors.toList());
    request.setDays(filteredRequestDays);
    filteredRequestDays.forEach(day -> day.setRequest(request));
  }

  private List<LocalDate> getPublicHolidayDatesInRange(List<Day> days) {
    LocalDate fromDate = days.get(0).getDate();
    LocalDate toDate = days.get(days.size() - 1).getDate();
    return publicHolidayService.findAllBetweenDates(fromDate, toDate).stream()
        .map(PublicHoliday::getDate)
        .collect(Collectors.toList());
  }

  private Predicate<Day> isWorkingDay() {
    return day -> {
      DayOfWeek dayOfWeek = day.getDate().getDayOfWeek();
      return !(DayOfWeek.SATURDAY.equals(dayOfWeek) || DayOfWeek.SUNDAY.equals(dayOfWeek));
    };
  }

  private void checkIfRequestDaysAlreadyExist(Request request) {
    List<Day> matchingDays = getUserMatchingDays(request);
    if (!matchingDays.isEmpty()) {
      logAndThrowEntityExistsException(matchingDays);
    }
  }

  private void createRequest(Request newRequest) {

    if (newRequest.getAbsence().isSickness()) {
      newRequest.setRequestStatus(RequestStatus.APPROVED);
      allowanceService.applyRequest(newRequest, false);
      emailService.sendSicknessRequestCreatedEmail(newRequest);
      save(newRequest);
    } else {
      newRequest.setRequestStatus(RequestStatus.PENDING);
      allowanceService.applyPendingRequest(newRequest, false);
      var persistedRequest = save(newRequest);
      emailService.sendEmailForApproval(persistedRequest);
      sendNotificationsService.sendNotificationToApproversAboutSubmittedRequest(newRequest);
    }
  }

  private List<Day> getUserMatchingDays(Request request) {
    var requestOwnerId = request.getUser().getId();
    List<Request> userRequests = requestRepository.findAllByUserId(requestOwnerId);

    return collectMatchingDays(request, userRequests);
  }

  private List<Day> collectMatchingDays(Request request, List<Request> userRequests) {
    return userRequests.stream()
                       .map(Request::getDays)
                       .flatMap(Collection::stream)
                       .filter(isRequestDaysMatch(request)
                       .and(isDayRequestApprovedOrPending()))
                       .collect(Collectors.toList());
  }

  private Predicate<Day> isRequestDaysMatch(Request request) {
    return day -> request.getDays()
                         .stream()
                         .anyMatch(newRequestDay -> newRequestDay.getDate().equals(day.getDate()) && (
                             newRequestDay.getDuration().equals(day.getDuration()) || Duration.FULL_DAY.equals(
                                 newRequestDay.getDuration()) || Duration.FULL_DAY.equals(day.getDuration())));
  }

  private Predicate<Day> isDayRequestApprovedOrPending() {
    return day -> day.getRequest().isApproved() || day.getRequest().isPending() || day.getRequest()
                                                                                      .isCancellationPending();
  }

  private void logAndThrowEntityExistsException(List<Day> matchingDays) {
    log.error("Request for days: '{}' overlaps with existing requests.",
        matchingDays.stream().map(day -> day.getDate().toString()).collect(Collectors.joining(", ")));
    throw new EntityExistsException("New request overlaps with existing one.");
  }

  /**
   * Saves the provided Request to repository.
   *
   * @param request the Request entity to be persisted.
   * @return saved Request.
   */
  @Transactional
  public Request save(Request request) {
    return requestRepository.save(request);
  }

  /**
   * Saves changed request status. If status is changed to APPROVED/CANCELED/REJECTED,
   * applies leave days from the request to the user's allowance,
   * creates an event in the user's Google calendar
   * and sends notification to user who made request, WHEN the request is handled by approver.
   * Makes audit of that update.
   *
   * @param request to be persisted.
   * @return saved request.
   */
  @CacheEvict(value = REQUESTS_CACHE, key = "#request.user.id")
  @Transactional(isolation = Isolation.SERIALIZABLE)
  public Request handleRequestStatusUpdate(Request request, User authUser) {
    Request existingRequest = findById(request.getId());
    request.setUser(existingRequest.getUser());
    boolean requestHasPendingCancellation = existingRequest.isCancellationPending();

    if (!requestHasPendingCancellation) {
      handleRequest(request, authUser, existingRequest);
    } else {
      handleApprovedRequestCancellation(request, authUser);
    }
    saveAuditInformation(OperationPerformed.UPDATE, authUser, request,
        RequestAudit.fromRequest(existingRequest, authUser));

    return update(request);
  }

  private Request findById(Long id) {
    return requestRepository.findById(id)
                            .orElseThrow(() -> new EntityNotFoundException(
                                "Request with id '" + id + "' doesn't exist in database."));
  }

  private void handleRequest(Request request, User authUser, Request existingRequest) {
    validateRequestIsNotAlreadyHandled(request, existingRequest);
    switch (request.getRequestStatus()) {
      case APPROVED:
        handleRequestApproval(request, authUser);
        break;
      case CANCELED:
        handleRequestCancellation(request, existingRequest, authUser);
        break;
      case REJECTED:
        rejectRequest(request, authUser);
        break;
      default:
        throw new IllegalArgumentException("Unsupported request status: " + request.getRequestStatus());
    }
  }

  private void validateRequestIsNotAlreadyHandled(Request request, Request existingRequest) {
    if (request.isCanceled()) {
      validateRequestNotAlreadyCanceled(existingRequest);
    } else {
      validateRequestNotAlreadyApproverOrRejected(existingRequest);
    }
  }

  private void validateRequestNotAlreadyCanceled(Request existingRequest) {
    var requestOwner = existingRequest.getUser();
    if (existingRequest.isCanceled()) {
      logAndThrowRequestAlreadyHandledException(existingRequest, requestOwner);
    }
  }

  private void validateRequestNotAlreadyApproverOrRejected(Request existingRequest) {
    var requestOwner = existingRequest.getUser();
    if (existingRequest.isApproved() || existingRequest.isRejected()) {
      logAndThrowRequestAlreadyHandledException(existingRequest, requestOwner);
    }
  }

  private void handleRequestApproval(Request request, User authUser) {
    var requestOwner = request.getUser();
    validateUserIsAuthorizedToHandleRequest(requestOwner, authUser, APPROVE);

    if (request.getAbsence().isBonusDays()) {
      handleBonusRequestApproval(request, authUser);
    } else {
      setApprover(request, authUser);
    }

    if (request.isPending()) {
      return;
    }

    applyRequest(request);
  }

  private void handleBonusRequestApproval(Request request, User approver) {
    checkCurrentlyApprovedBy(approver, request);
    int neededApprovals = request.getUser().getApprovers().size();

    if (request.getCurrentlyApprovedBy().size() != neededApprovals) {
      request.setRequestStatus(RequestStatus.PENDING);
    }
  }

  private void checkCurrentlyApprovedBy(User approver, Request request) {
    if (!request.getCurrentlyApprovedBy().contains(approver)) {
      request.getCurrentlyApprovedBy().add(approver);
    }
  }

  private void applyRequest(Request request) {
    var requestOwner = request.getUser();
    allowanceService.applyPendingRequest(request, true);
    allowanceService.applyRequest(request, false);
    sendNotificationsService.sendNotificationForRequestedLeave(request.getRequestStatus(), requestOwner);
    emailService.sendRequestStatusUpdateEmail(request);
    sendEmailToTeammatesAndNotifiers(request);
    googleCalendarService.handleRequestUpdate(request, false);
  }

  private void sendEmailToTeammatesAndNotifiers(Request request) {
    if (request.getAbsence().isBonusDays()) {
      emailService.sendBonusRequestCreatedEmail(request);
    } else {
      emailService.sendAnnualRequestCreatedEmail(request);
    }
  }

  private void handleRequestCancellation(Request request, Request existingRequest, User authUser) {
    var requestOwner = request.getUser();
    var userIsLoggedUser = requestOwner.equals(authUser);

    if (userIsLoggedUser && existingRequest.isApproved()) {
      handleApprovedRequestCancellation(request);
    } else if (userIsLoggedUser && existingRequest.isPending()) {
      cancelRequest(request);
    } else {
      logAndThrowNotAuthorizedApprovalException(requestOwner, CANCEL);
    }
  }

  private void handleApprovedRequestCancellation(Request request) {
    request.setRequestStatus(RequestStatus.CANCELLATION_PENDING);
    request.setCurrentlyApprovedBy(new ArrayList<>());
    emailService.sendEmailForApproval(request);
    sendNotificationsService.sendNotificationToApproversAboutSubmittedRequest(request);
  }

  private void cancelRequest(Request request) {
    allowanceService.applyPendingRequest(request, true);
    googleCalendarService.handleRequestUpdate(request, true);
  }

  private void rejectRequest(Request request, User authUser) {
    var requestOwner = request.getUser();
    validateUserIsAuthorizedToHandleRequest(requestOwner, authUser, REJECT);
    setApprover(request, authUser);

    allowanceService.applyPendingRequest(request, true);
    emailService.sendRequestStatusUpdateEmail(request);
    sendNotificationsService.sendNotificationForRequestedLeave(request.getRequestStatus(), requestOwner);
    googleCalendarService.handleRequestUpdate(request, true);
  }

  private void handleApprovedRequestCancellation(Request request, User authUser) {
    if (request.isApproved()) {
      approveRequestCancellation(request, authUser);
    } else {
      rejectRequestCancellation(request, authUser);
    }
  }

  private void approveRequestCancellation(Request request, User authUser) {
    var requestOwner = request.getUser();
    validateUserIsAuthorizedToHandleRequest(requestOwner, authUser, APPROVE);
    setApprover(request, authUser);
    request.setRequestStatus(RequestStatus.CANCELED);
    allowanceService.applyRequest(request, true);
    emailService.sendRequestStatusUpdateEmail(request);
    sendNotificationsService.sendNotificationForRequestedLeave(request.getRequestStatus(), requestOwner);
    googleCalendarService.handleRequestUpdate(request, true);
  }

  private void rejectRequestCancellation(Request request, User authUser) {
    var requestOwner = request.getUser();
    validateUserIsAuthorizedToHandleRequest(requestOwner, authUser, REJECT);
    setApprover(request, authUser);
    emailService.sendRequestStatusUpdateEmail(request);
    sendNotificationsService.sendNotificationForRequestedLeave(RequestStatus.REJECTED, requestOwner);
    request.setRequestStatus(RequestStatus.APPROVED);
  }

  private void validateUserIsAuthorizedToHandleRequest(User requestOwner, User authUser, String status) {
    var userIsRequestApprover = isUserRequestApprover(authUser, requestOwner);
    var userIsLoggedUser = requestOwner.getId().equals(authUser.getId());
    if (!userIsRequestApprover || userIsLoggedUser) {
      logAndThrowNotAuthorizedApprovalException(requestOwner, status);
    }
  }

  private boolean isUserRequestApprover(User approver, User requestOwner) {
    return requestOwner.getApprovers().stream().anyMatch(teamApprover -> teamApprover.equals(approver));
  }

  private void setApprover(Request request, User approver) {
    request.setCurrentlyApprovedBy(List.of(approver));
  }

  private void logAndThrowRequestAlreadyHandledException(Request request, User requestOwner) {
    var requestStatus = request.getRequestStatus().getDescription();
    log.error("Request by user: '{}', is already '{}'.", requestOwner.getEmail(), requestStatus);
    throw new RequestAlreadyHandledException(requestStatus);
  }

  private void logAndThrowNotAuthorizedApprovalException(User requestOwner, String handleAction) {
    log.error("Approver not authorized to '{}' this request for user with email: '{}'", handleAction,
        requestOwner.getEmail());
    throw new NotAuthorizedApprovalException(handleAction);
  }

  /**
   * Saves the provided Request to repository.
   *
   * @param request the Request entity to be persisted.
   * @return saved Request.
   */
  @Transactional
  public Request update(Request request) {
    return requestRepository.save(request);
  }

  private void saveAuditInformation(OperationPerformed operationPerformed, User modifiedByUser, Request request,
      RequestAudit previousRequestState) {
    RequestAudit currentRequestState = getCurrentRequestState(modifiedByUser, request, previousRequestState);

    auditInformationService.saveAudit(operationPerformed, modifiedByUser, request.getId(), previousRequestState,
        currentRequestState);
  }

  private RequestAudit getCurrentRequestState(User modifiedByUser, Request request, RequestAudit previousRequestState) {
    RequestAudit currentRequestState = RequestAudit.fromRequest(request, modifiedByUser);
    getComment(previousRequestState, currentRequestState).ifPresent(currentRequestState::setComment);

    return currentRequestState;
  }

  /**
   * With every update of request only one comment (reason, approver comment, cancellation reason, approver cancellation
   * comment) can change, find that difference and put it in field named comment.
   *
   * @param previousRequestState persisted version of Request.
   * @param currentRequestState current version of Request with introduced changes.
   * @return difference in comments between old and new versions of Requests.
   */
  private Optional<String> getComment(RequestAudit previousRequestState, RequestAudit currentRequestState) {
    var currentReason = Optional.ofNullable(currentRequestState.getReason());
    var previousReason = Optional.ofNullable(previousRequestState.getReason());
    var currentApproverComment = Optional.ofNullable(currentRequestState.getApproverComment());
    var previousApproverComment = Optional.ofNullable(previousRequestState.getApproverComment());
    var currentCancellationReason = Optional.ofNullable(currentRequestState.getCancellationReason());
    var previousCancellationReason = Optional.ofNullable(previousRequestState.getCancellationReason());

    if (!currentReason.equals(previousReason)) {
      return currentReason;
    } else if (!currentApproverComment.equals(previousApproverComment)) {
      return currentApproverComment;
    } else if (!currentCancellationReason.equals(previousCancellationReason)) {
      return currentCancellationReason;
    } else {
      return Optional.ofNullable(currentRequestState.getApproverCancellationComment());
    }
  }

}