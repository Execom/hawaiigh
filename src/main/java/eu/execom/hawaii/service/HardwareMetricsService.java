package eu.execom.hawaii.service;

import eu.execom.hawaii.model.metric.HardwareMetrics;
import eu.execom.hawaii.repository.HardwareMetricsRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class HardwareMetricsService {

  private static LocalDateTime lastAlarmTimestamp;

  @Value("${alert.cpu.systemUsageThreshold}")
  private double systemCpuUsageThreshold;
  @Value("${alert.cpu.jvmUsageThreshold}")
  private double jvmCpuUsageThreshold;
  @Value("${alert.memory.jvmUsageThresholdMb}")
  private double memoryUsageThreshold;
  @Value("${alert.monitoredTimespanMinutes}")
  private int monitoredTimespan;
  @Value("${alert.notificationSnoozeTimeMinutes}")
  private int snoozeTime;

  private HardwareMetricsRepository hardwareMetricsRepository;
  private EmailService emailService;

  public HardwareMetricsService(HardwareMetricsRepository hardwareMetricsRepository, EmailService emailService) {
    this.hardwareMetricsRepository = hardwareMetricsRepository;
    this.emailService = emailService;
  }

  /**
   * Saves HardwareMetrics object.
   *
   * @param hardwareMetrics object to be persisted.
   * @return persisted object.
   */
  public HardwareMetrics save(HardwareMetrics hardwareMetrics) {
    checkAlarms(hardwareMetrics);
    return hardwareMetricsRepository.save(hardwareMetrics);
  }

  /**
   * Checks if resources high usage alarm needs to be triggered.
   *
   * @param hardwareMetrics current HardwareMetrics.
   */
  private void checkAlarms(HardwareMetrics hardwareMetrics) {
    int numberOfLatestHardwareMonitoringEntries = getNumberOfHardwareMonitoringEntriesForLastMinutes(monitoredTimespan);
    if (numberOfLatestHardwareMonitoringEntries < monitoredTimespan) {
      return;
    }
    if (lastAlarmTimestamp != null) {
      LocalDateTime currentTimestamp = LocalDateTime.now();
      long minutesSinceLastAlarm = ChronoUnit.MINUTES.between(lastAlarmTimestamp, currentTimestamp);
      if (minutesSinceLastAlarm < snoozeTime) {
        return;
      }
    }
    boolean isCurrentResourcesUsageHigh = isResourcesUsageHigh(hardwareMetrics);
    if (isCurrentResourcesUsageHigh) {
      HardwareMetrics averageHardwareMetrics = getAverageResourcesUsageForLastMinutes(monitoredTimespan);
      boolean isAverageResourcesUsageHigh = isResourcesUsageHigh(averageHardwareMetrics);
      if (isAverageResourcesUsageHigh) {
        emailService.sendHighResourcesUsageWarningEmail(averageHardwareMetrics);
        lastAlarmTimestamp = LocalDateTime.now();
      }
    }
  }

  /**
   * Checks if number of HardwareMonitoring entries is greater than number of minutes defining timespan for high
   * resources usage alarm trigger. This prevents triggering false alarms at the application startup when imports
   * are performed and resources usage is generally high.
   *
   * @param minutes defining monitored timespan.
   * @return number of HardwareMonitoring entries.
   */
  private int getNumberOfHardwareMonitoringEntriesForLastMinutes(int minutes) {
    LocalDateTime timestamp = LocalDateTime.now().minusMinutes(minutes);
    return hardwareMetricsRepository.countByTimestampGreaterThan(timestamp);
  }

  /**
   * Checks if resources usage is high.
   *
   * @param hardwareMetrics HardwareMetrics representing resources usage.
   * @return true if resources usage is high, false otherwise.
   */
  private boolean isResourcesUsageHigh(HardwareMetrics hardwareMetrics) {
    boolean isSystemCpuUsageHigh = hardwareMetrics.getSystemCpuUsage() >= systemCpuUsageThreshold;
    boolean isJvmCpuUsageHigh = hardwareMetrics.getJvmCpuUsage() >= jvmCpuUsageThreshold;
    boolean isMemoryUsageHigh = hardwareMetrics.getMemoryUsage() >= memoryUsageThreshold;
    return isMemoryUsageHigh || isJvmCpuUsageHigh || isSystemCpuUsageHigh;
  }

  /**
   * Retrieves HardwareMetrics object with average resources usage for the last minutes defined by input parameter.
   *
   * @param minutes whole number of minutes.
   * @return HardwareMetrics object with average resources usage.
   */
  public HardwareMetrics getAverageResourcesUsageForLastMinutes(int minutes) {
    LocalDateTime timestamp = LocalDateTime.now().minusMinutes(minutes);
    return hardwareMetricsRepository.getAverageHardwareMetricsFromTimestampUntilNow(timestamp);
  }

  /**
   * Retrieves all HardwareMetrics for given time span, if time span is not set returns HardwareMetrics from the
   * last 6 hours.
   *
   * @param fromTimestamp from timestamp.
   * @param toTimestamp to timespan.
   * @return List of HardwareMetrics.
   */
  public List<HardwareMetrics> findAllByTimestampBetween(LocalDateTime fromTimestamp, LocalDateTime toTimestamp) {
    if (fromTimestamp == null) {
      if (toTimestamp == null) {
        toTimestamp = LocalDateTime.now();
      }
      fromTimestamp = toTimestamp.minusHours(6);
    }
    return hardwareMetricsRepository.findAllByTimestampBetween(fromTimestamp, toTimestamp);
  }

  /**
   * Retrieves timestamp from the oldest HardwareMetrics entry.
   *
   * @return timestamp.
   */
  public LocalDateTime getOldestTimestamp() {
    return hardwareMetricsRepository.getOldestTimestamp();
  }

  /**
   * Deletes HardwareMetrics older than 2 days.
   */
  @Transactional
  public void deleteMonitoringData(int daysOld) {
    LocalDateTime timestampDeletePoint = LocalDate.now().atTime(0, 0).minusDays(daysOld);
    hardwareMetricsRepository.deleteAllByTimestampLessThanEqual(timestampDeletePoint);
  }
}
