package eu.execom.hawaii.service;

import eu.execom.hawaii.model.metric.HttpEndpointMetrics;
import eu.execom.hawaii.repository.HttpEndpointMetricsRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class HttpEndpointMetricsService {

  @Value("${alert.httpEndpoint.numberOfFailedRequestsThreshold}")
  private int numberOfFailedRequestsThreshold;
  @Value("${alert.notificationSnoozeTimeMinutes}")
  private int snoozeTime;

  private HttpEndpointMetricsRepository httpEndpointMetricsRepository;
  private EmailService emailService;

  public HttpEndpointMetricsService(HttpEndpointMetricsRepository httpEndpointMetricsRepository,
      EmailService emailService) {
    this.httpEndpointMetricsRepository = httpEndpointMetricsRepository;
    this.emailService = emailService;
  }

  /**
   * Deletes old HttpEndpointMetrics from previous application instance.
   */
  @PostConstruct
  private void deleteOldHttpEndpointMetrics() {
    deleteAll();
  }

  /**
   * Saves HttpEndpointMetrics entry.
   *
   * @param httpEndpointMetrics object to be persisted.
   * @return persisted object.
   */
  public HttpEndpointMetrics save(HttpEndpointMetrics httpEndpointMetrics) {
    checkAlarms(httpEndpointMetrics);
    return httpEndpointMetricsRepository.save(httpEndpointMetrics);
  }

  /**
   * Checks if http endpoint fail alarm needs to be triggered.
   *
   * @param httpEndpointMetrics HttpEndpointMetrics object.
   */
  private void checkAlarms(HttpEndpointMetrics httpEndpointMetrics) {
    LocalDateTime lastFailAlarmTimestamp = httpEndpointMetrics.getLastFailAlarmTimestamp();
    if (lastFailAlarmTimestamp != null) {
      LocalDateTime currentTimestamp = LocalDateTime.now();
      long minutesSinceLastAlarm = ChronoUnit.MINUTES.between(lastFailAlarmTimestamp, currentTimestamp);
      if (minutesSinceLastAlarm < snoozeTime) {
        return;
      }
    }
    boolean isExceptionRaised = httpEndpointMetrics.isExceptionRaised();
    boolean didEndpointFailedTooMuchTimes = httpEndpointMetrics.getNumberOfRequests() > numberOfFailedRequestsThreshold;
    if (isExceptionRaised && didEndpointFailedTooMuchTimes) {
      emailService.sendHttpEndpointFailedWarningEmail(httpEndpointMetrics);
      httpEndpointMetrics.setLastFailAlarmTimestamp(LocalDateTime.now());
    }
  }

  /**
   * Retrieves all HttpEndpointMetrics.
   *
   * @return List of HttpEndpointMetrics.
   */
  public List<HttpEndpointMetrics> findAll() {
    return httpEndpointMetricsRepository.findAll();
  }

  /**
   * Checks if HttpEndpointMetrics exists and retrieves it.
   *
   * @param httpEndpointMetrics object for which check is conducted.
   * @return HttpEndpointMetrics if exists.
   */
  public HttpEndpointMetrics getHttpEndpointMetricIfExists(HttpEndpointMetrics httpEndpointMetrics) {
    String endpointUrl = httpEndpointMetrics.getEndpointUrl();
    String method = httpEndpointMetrics.getMethod();
    String outcome = httpEndpointMetrics.getOutcome();
    String exception = httpEndpointMetrics.getException();
    return httpEndpointMetricsRepository.findByEndpointUrlAndMethodAndOutcomeAndException(endpointUrl, method, outcome,
        exception);
  }

  /**
   * Deletes all HttpEndpointMetrics entries.
   */
  public void deleteAll() {
    httpEndpointMetricsRepository.deleteAll();
  }
}
