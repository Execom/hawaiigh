package eu.execom.hawaii.service;

import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TeamApproverService {

  private UserRepository userRepository;
  private TeamRepository teamRepository;

  @Autowired
  public TeamApproverService(UserRepository userRepository, TeamRepository teamRepository) {
    this.teamRepository = teamRepository;
    this.userRepository = userRepository;
  }

  public void handleApproverTeamsChange(User updatedUser) {
    var databaseUser = userRepository.findById(updatedUser.getId())
                                     .orElseThrow(() -> new EntityNotFoundException(
                                         "User with id: '" + updatedUser.getId() + "' doesn't exist in database."));
    removeApproverTeam(updatedUser, databaseUser);
    addApproverTeam(updatedUser, databaseUser);
  }

  public void addUserToTeamApprovers(User user) {
    var approverTeams = user.getApproverTeams();
    addApproverTeam(approverTeams, user);
  }

  /**
   * Remove approver team from user approverTeams list if needed.
   *
   * @param updatedUser the User entity to be persisted.
   */
  private void removeApproverTeam(User updatedUser, User databaseUser) {
    var formerApproverTeams = databaseUser.getApproverTeams();
    var currentApproverTeams = updatedUser.getApproverTeams();
    List<Team> teamsForRemoval = filterTeams(formerApproverTeams, currentApproverTeams);

    if (!teamsForRemoval.isEmpty()) {
      removeUserFromTeamApproversList(teamsForRemoval, updatedUser);
    }
  }

  private void removeUserFromTeamApproversList(List<Team> approverTeams, User user) {
    approverTeams.forEach(team -> {
      team.getTeamApprovers().remove(user);
      teamRepository.save(team);
    });
  }

  /**
   * Add approver team to user approverTeams list if needed.
   *
   * @param updatedUser the User entity to be persisted.
   */
  private void addApproverTeam(User updatedUser, User databaseUser) {
    var formerApproverTeams = databaseUser.getApproverTeams();
    var currentApproverTeams = updatedUser.getApproverTeams();
    List<Team> newApproverTeams = filterTeams(currentApproverTeams, formerApproverTeams);

    if (!newApproverTeams.isEmpty()) {
      addApproverTeam(newApproverTeams, updatedUser);
    }
  }

  private void addApproverTeam(List<Team> approverTeams, User user) {
    approverTeams.forEach(team -> {
      Team approverTeam = teamRepository.getOne(team.getId());
      addUserToApproversList(user, approverTeam);
    });
  }

  private void addUserToApproversList(User user, Team approverTeam) {
    if (!approverTeam.getTeamApprovers().contains(user)) {
      approverTeam.getTeamApprovers().add(user);
      teamRepository.save(approverTeam);
    }
  }

  private List<Team> filterTeams(List<Team> approverTeams, List<Team> teams) {
    return approverTeams.stream().filter(team -> !teams.contains(team)).collect(Collectors.toList());
  }
}
