package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.model.Absence;
import eu.execom.hawaii.repository.AbsenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;

/**
 * Absence type management service.
 */
@Service
public class AbsenceService {

  private AbsenceRepository absenceRepository;

  @Autowired
  public AbsenceService(AbsenceRepository absenceRepository) {
    this.absenceRepository = absenceRepository;
  }

  /**
   * Retrieves a list of all Absence types from repository sorted by name.
   *
   * @return a list of all Absence types.
   */
  public Page<Absence> findAll(Pageable pageable) {
    return absenceRepository.findAll(pageable);
  }

  /**
   * Retrieves an Absence with a specific id.
   *
   * @param id Absence id.
   * @return Absence with provided id if exists.
   * @throws EntityNotFoundException if an Absence with given id is not found.
   */
  public Absence getById(Long id) {
    return absenceRepository.getOne(id);
  }

  /**
   * Saves the provided Absence to repository. It is not allowed to create new absences of training
   * and bonus types. Also, to create new absence type, absence name needs to be unique.
   *
   * @param absence the Absence entity to be persisted.
   */
  public Absence save(Absence absence) {
    requireAbsenceIsNotBonusNorTraining(absence);

    try {
      return absenceRepository.save(absence);
    } catch (DataIntegrityViolationException e) {
      throw new DataIntegrityViolationException("Absence name '" + absence.getName() + "' already exists.");
    }
  }

  private void requireAbsenceIsNotBonusNorTraining(Absence absence) {
    if (absence.isBonusDays() || absence.isTraining()) {
      throw new ActionNotAllowedException("It is not allowed to create new bonus or training absences.");
    }
  }

  /**
   * Update the provided Absence to repository. It is not allowed to update absences of training
   * and undeductible types. Also, to update absence type's name, absence name needs to be unique.
   *
   * @param absence the Absence entity to be updated.
   */
  public Absence update(Absence absence) {
    requireTrainingIsDeductible(absence);

    try {
      return absenceRepository.save(absence);
    } catch (DataIntegrityViolationException e) {
      throw new DataIntegrityViolationException("Absence name '" + absence.getName() + "' already exists.");
    }
  }

  private void requireTrainingIsDeductible(Absence absence) {
    if (absence.isTraining() && !absence.isDeducted()) {
      throw new ActionNotAllowedException("Training and education leave type must be deducted!");
    }
  }

  /**
   * Deletes absence from database. It is now allowed to delete absences of type training and bonus.
   *
   * @param id - the Absence id.
   * @throws EntityNotFoundException if an Absence with given id is not found.
   */
  public void delete(Long id) {
    var absence = absenceRepository.getById(id)
                                   .orElseThrow(() -> new EntityNotFoundException("Absence already deleted."));

    if (absence.isTraining() || absence.isBonusDays()) {
      throw new ActionNotAllowedException("It is not allowed to delete training or bonus absences.");
    }

    try {
      absenceRepository.deleteById(id);
    } catch (DataIntegrityViolationException e) {
      throw new DataIntegrityViolationException(
          "It is not allowed to delete this leave type because it is used in our application.");
    }
  }

}
