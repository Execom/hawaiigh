package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.exceptions.SortParameterTooLongException;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.AllowanceRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.repository.YearRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
public class YearService {

  private YearRepository yearRepository;
  private UserRepository userRepository;
  private AllowanceRepository allowanceRepository;

  private static final String YEAR_REGEX = "^(20)\\d{2}$";
  private static final int MAXIMUM_SORT_PARAMETER_LENGTH = 30;

  @Autowired
  public YearService(YearRepository yearRepository, UserRepository userRepository,
      AllowanceRepository allowanceRepository) {
    this.yearRepository = yearRepository;
    this.userRepository = userRepository;
    this.allowanceRepository = allowanceRepository;
  }

  public Page<Year> getAll(Pageable pageable) {
    if (pageable.getSort().toString().length() > MAXIMUM_SORT_PARAMETER_LENGTH) {
      throw new SortParameterTooLongException();
    }
    return yearRepository.findAll(pageable);
  }

  public Year findById(Long id) {
    return yearRepository.findById(id)
                         .orElseThrow(
                             () -> new EntityExistsException("Year with id '" + id + "' doesn't exist in database."));
  }

  public Year save(Year year) {
    validateYear(year);
    validateCarriedOverExpirationActivation(year);
    if (year.getId() != null) {
      validateYearChange(year);
    }

    try {
      yearRepository.save(year);
    } catch (DataIntegrityViolationException e) {
      throw new EntityExistsException("Year " + year.getYear() + " already exists.");
    }
    createAllowanceWhenActivatingYear(year);

    return year;
  }

  public Year update(Year year) {
    validateYear(year);
    Year databaseYear = findById(year.getId());
    validateCarriedOverExpirationActivation(year);
    validateYearChange(year);

    if (databaseYear.getAllowances().isEmpty()) {
      createAllowanceWhenActivatingYear(year);
    }

    try {
      yearRepository.save(year);
    } catch (DataIntegrityViolationException e) {
      throw new EntityExistsException("Year " + year.getYear() + " already exists.");
    }

    return year;
  }

  private void validateYear(Year year) {
    if (!String.valueOf(year.getYear()).matches(YEAR_REGEX)) {
      throw new IllegalArgumentException("Year '" + year.getYear() + "' is not a valid year.");
    }
  }

  private void validateCarriedOverExpirationActivation(Year year) {
    LocalDate carriedOverExpirationDate = year.getCarriedOverHoursExpirationDate();

    if (year.isCarriedOverHoursExpirable() && carriedOverExpirationDate == null) {
      throw new ActionNotAllowedException(
          "In order to activate carried over hours expiration mechanism, expiration date needs to be set.");
    }
  }

  private void validateYearChange(Year year) {
    Year databaseYear = findById(year.getId());
    if (databaseYear.isActive() && databaseYear.getYear() != year.getYear()) {
      throw new ActionNotAllowedException("It is not allowed to change the name of active year.");
    }
    if (databaseYear.isActive() && !year.isActive()) {
      throw new ActionNotAllowedException("It is not allowed to change the status of active year.");
    }
  }

  void createAllowanceWhenActivatingYear(Year year) {
    if (!year.isActive()) {
      return;
    }
    List<User> activeUsers = userRepository.findAllByUserStatusType(List.of(UserStatusType.ACTIVE));
    List<Allowance> allowanceForUsersForCreatedYear = new LinkedList<>();

    for (User user : activeUsers) {
      Allowance allowance = createAllowance(year, user);
      allowanceForUsersForCreatedYear.add(allowance);
    }

    allowanceRepository.saveAll(allowanceForUsersForCreatedYear);
  }

  Allowance createAllowance(Year createdYear, User user) {
    LeaveProfile leaveProfile = user.getLeaveProfile();

    return new Allowance(user, createdYear, leaveProfile.getEntitlement(), leaveProfile.getTraining(),
        leaveProfile.getMaxBonusDays());
  }

  public void deleteById(Long id) {
    var year = findById(id);
    validateYearIsRemovable(year);

    yearRepository.deleteById(id);
  }

  private void validateYearIsRemovable(Year year) {
    if (year.isActive() && !year.getAllowances().isEmpty()) {
      throw new ActionNotAllowedException("Only inactive and years with no allowances assigned can be deleted.");
    }
  }

}
