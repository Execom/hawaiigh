package eu.execom.hawaii.service;

import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.function.Predicate;

@Slf4j
@Service
public class UserRoleService {

  private TeamRepository teamRepository;
  private UserRepository userRepository;

  @Autowired
  public UserRoleService(TeamRepository teamRepository, UserRepository userRepository) {
    this.teamRepository = teamRepository;
    this.userRepository = userRepository;
  }

  public void changeUserRole(Team updatedTeam) {
    Team oldTeam = teamRepository.findById(updatedTeam.getId())
                                 .orElseThrow(() -> new EntityNotFoundException(
                                     "Team with id '" + updatedTeam.getId() + "' does not exist."));

    var oldTeamApprovers = oldTeam.getTeamApprovers();
    var newTeamApprovers = updatedTeam.getTeamApprovers();

    oldTeamApprovers.stream()
                    .filter(Predicate.not(User::hasRoleHrManager)
                    .and(user -> !newTeamApprovers.contains(user))
                    .and(user -> checkIfUserHasOtherApproverTeams(oldTeam, user)))
                    .forEach(user -> userRepository.save(user));
  }

  private boolean checkIfUserHasOtherApproverTeams(Team oldTeam, User approver) {
    return approver.getApproverTeams().stream().allMatch(team -> team.equals(oldTeam));
  }
}
