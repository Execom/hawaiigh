package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.enumerations.LeaveProfileType;
import eu.execom.hawaii.repository.LeaveProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Leave profile management service.
 */
@Service
public class LeaveProfileService {

  private LeaveProfileRepository leaveProfileRepository;

  @Autowired
  public LeaveProfileService(LeaveProfileRepository leaveProfileRepository) {
    this.leaveProfileRepository = leaveProfileRepository;
  }

  /**
   * Retrieves a list of all leave profiles from repository.
   *
   * @return a list of all leave profiles.
   */
  public Page<LeaveProfile> findAll(Pageable pageable) {
    return leaveProfileRepository.findAll(pageable);
  }

  public List<LeaveProfile> findAll() {
    return leaveProfileRepository.findAll();
  }

  /**
   * Retrieves a leave profile with a specific id.
   *
   * @param id Leave profile id.
   * @return Leave profile with provided id if exists.
   */
  public LeaveProfile getById(Long id) {
    return leaveProfileRepository.getOne(id);
  }

  /**
   * Creates custom leave profile to repository.
   *
   * @param leaveProfile the LeaveProfile entity to be persisted.
   */
  public LeaveProfile create(LeaveProfile leaveProfile) {
    leaveProfile.setLeaveProfileType(LeaveProfileType.CUSTOM);
    leaveProfile.setUpgradeable(false);
    leaveProfile.setUsers(new ArrayList<>());

    return leaveProfileRepository.save(leaveProfile);
  }

  /**
   * Updates the provided leave profile and saves it to repository.
   *
   * @param leaveProfile the LeaveProfile entity to be persisted.
   */
  public LeaveProfile update(LeaveProfile leaveProfile) {

    return leaveProfileRepository.save(leaveProfile);
  }

  /**
   * Deletes leave profile.
   *
   * @param id - the leave profile id.
   * @throws EntityNotFoundException if a leave profile with given id is not found.
   */
  public void delete(Long id) {
    LeaveProfile leaveProfile = getLeaveProfile(id);
    validateLeaveProfileIsCustom(leaveProfile);
    try {
      leaveProfileRepository.delete(leaveProfile);
    } catch (DataIntegrityViolationException e) {
      throw new DataIntegrityViolationException("To delete a leave profile, its member list needs to be empty.");
    }
  }

  private LeaveProfile getLeaveProfile(Long id) {
    return leaveProfileRepository.getById(id)
                                 .orElseThrow(() -> new EntityNotFoundException(
                                     "Leave Profile doesn't exist or is already deleted."));
  }

  private void validateLeaveProfileIsCustom(LeaveProfile leaveProfile) {
    if (!leaveProfile.isCustom()) {
      throw new ActionNotAllowedException("Only Custom leave profiles can be deleted.");
    }
  }
}
