package eu.execom.hawaii.service;

import eu.execom.hawaii.model.Icon;
import eu.execom.hawaii.repository.IconRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IconService {

  private IconRepository iconRepository;

  @Autowired
  public IconService(IconRepository iconRepository) {
    this.iconRepository = iconRepository;
  }

  public List<Icon> getAllIcons() {
    return iconRepository.findAll();
  }
}