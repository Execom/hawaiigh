package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.AllowanceNotFoundException;
import eu.execom.hawaii.exceptions.InsufficientHoursException;
import eu.execom.hawaii.exceptions.NegativeAllowanceException;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.PublicHoliday;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.model.audit.AllowanceAudit;
import eu.execom.hawaii.model.enumerations.OperationPerformed;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.AllowanceRepository;
import eu.execom.hawaii.repository.PublicHolidayRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AllowanceService {

  private static final String ANNUAL = "annual";
  private static final String TRAINING = "training";
  private static final String BONUS = "bonus";
  private static final int MINIMUM_QUERY_LENGTH = 3;

  private AllowanceRepository allowanceRepository;
  private PublicHolidayRepository publicHolidayRepository;
  private AuditInformationService auditInformationService;

  @Autowired
  public AllowanceService(AllowanceRepository allowanceRepository, PublicHolidayRepository publicHolidayRepository,
      AuditInformationService auditInformationService) {
    this.allowanceRepository = allowanceRepository;
    this.publicHolidayRepository = publicHolidayRepository;
    this.auditInformationService = auditInformationService;
  }

  /**
   * Saves the provided Allowance to repository.
   *
   * @param allowance the Allowance entity to be persisted.
   * @return saved Allowance.
   */
  @Transactional
  public Allowance save(Allowance allowance) {
    return allowanceRepository.save(allowance);
  }

  /**
   * Apply pending request on request user allowance.
   *
   * @param request the Request.
   */
  @Transactional
  public void applyPendingRequest(Request request, boolean requestCanceled) {
    var absence = request.getAbsence();
    if (!absence.isDeducted()) {
      return;
    }

    var yearOfRequest = request.getDays().get(0).getDate().getYear();
    var currentYearAllowance = getByUserAndYear(request.getUser().getId(), yearOfRequest);
    var workingDays = getWorkingDaysOnly(request.getDays());
    var hours = calculateHours(workingDays);
    if (requestCanceled) {
      hours = -hours;
    }

    if (absence.isLeave()) {
      checkRemainingAnnualHours(currentYearAllowance, hours);
      if (requestCanceled) {
        cancelPendingAnnual(currentYearAllowance, hours);
      } else {
        applyPendingAnnual(currentYearAllowance, hours);
      }
    } else if (absence.isTraining()) {
      checkRemainingTrainingHours(currentYearAllowance, hours);
      applyPendingTraining(currentYearAllowance, hours);
    } else if (absence.isBonusDays()) {
      checkRemainingBonusHours(currentYearAllowance, hours);
      applyPendingBonus(currentYearAllowance, hours);
    }
  }

  /**
   * Retrieves a Allowance by given user.
   *
   * @param userId Allowance user.
   * @return Allowance.
   */
  public Allowance getByUserAndYear(Long userId, Integer year) {
    int allowanceForYear = year == null ? LocalDate.now().getYear() : year;
    Allowance allowance = allowanceRepository.findByUserIdAndYearYear(userId, allowanceForYear);
    if (allowance == null) {
      throw new AllowanceNotFoundException("Allowance for user with id '" + userId + "' not found.");
    }
    return allowance;
  }

  List<Allowance> getAllByUserId(Long userId) {
    return allowanceRepository.findAllByUserId(userId);
  }

  boolean existsByUserId(Long userId) {
    return allowanceRepository.existsByUserId(userId);
  }

  /**
   * Gets pending annual hours for current year and for next year if next year is active. Checks if pending annual hours for next year minus
   * canceled hours is less than zero.
   * <p>
   * If it is, it means that some or all of those canceled hours belong to current year and rest to next year.
   * If it isn't, it means that all of those canceled hours belong to next year.
   * Also, if next year isn't active, all canceled hours belong to current year.
   */
  private void cancelPendingAnnual(Allowance currentYearAllowance, int requestedHours) {
    var currentYearPendingAnnual = currentYearAllowance.getPendingAnnual();
    var nextYearPendingInPreviousYear = 0;
    var nextYearRequestedHours = requestedHours;

    if (nextYearAllowanceExists(currentYearAllowance)) {
      var nextYearAllowance = getNextYearAllowance(currentYearAllowance);
      nextYearPendingInPreviousYear = nextYearAllowance.getPendingInPreviousYear();
      nextYearRequestedHours = nextYearPendingInPreviousYear + requestedHours;
      if (nextYearRequestedHours < 0) {
        currentYearAllowance.setPendingAnnual(currentYearPendingAnnual + nextYearRequestedHours);
        nextYearAllowance.setPendingInPreviousYear(0);
        allowanceRepository.save(currentYearAllowance);
      } else {
        nextYearAllowance.setPendingInPreviousYear(nextYearRequestedHours);
      }
      allowanceRepository.save(nextYearAllowance);
    } else {
      currentYearAllowance.setPendingAnnual(currentYearPendingAnnual + nextYearRequestedHours);
      allowanceRepository.save(currentYearAllowance);
    }
  }

  /**
   * Gets pending annual hours for current year and for next year if year is active. Checks if there is less hours in current
   * year then is requested.
   * <p>
   * If it is, it means that some or all of requested hours will be taken from next year allowance if next year is active.
   * If it isn't, it means that all of requested hours will be taken from current year if there is enough remaining hours.
   * if there is not enough annual hours in current year and next year isn't active, request won't be created.
   */
  private void applyPendingAnnual(Allowance currentYearAllowance, int requestedHours) {
    var currentYearPendingAnnual = currentYearAllowance.getPendingAnnual();
    var remainingHoursCurrentYear = calculateRemainingAnnualHours(currentYearAllowance);
    var nextYearRequestedHours = requestedHours - remainingHoursCurrentYear;

    if (nextYearAllowanceExists(currentYearAllowance)) {
      var nextYearAllowance = getNextYearAllowance(currentYearAllowance);
      var nextYearPendingInPreviousYear = nextYearAllowance.getPendingInPreviousYear();
      nextYearRequestedHours += nextYearPendingInPreviousYear;
      if (nextYearRequestedHours > 0) {
        currentYearAllowance.setPendingAnnual(currentYearPendingAnnual + remainingHoursCurrentYear);
        nextYearAllowance.setPendingInPreviousYear(nextYearRequestedHours);
        allowanceRepository.save(nextYearAllowance);
      } else {
        currentYearAllowance.setPendingAnnual(currentYearPendingAnnual + requestedHours);
      }

    } else {
      currentYearAllowance.setPendingAnnual(currentYearPendingAnnual + requestedHours);
    }
    allowanceRepository.save(currentYearAllowance);
  }

  private void applyPendingTraining(Allowance allowance, int requestedHours) {
    var allowancePendingTraining = allowance.getPendingTraining();
    var calculatedPendingTraining = allowancePendingTraining + requestedHours;
    allowance.setPendingTraining(calculatedPendingTraining);
    allowanceRepository.save(allowance);
  }

  private void applyPendingBonus(Allowance allowance, int requestedHours) {
    var allowancePendingBonus = allowance.getPendingBonus();
    var calculatedPendingBonus = allowancePendingBonus + requestedHours;
    allowance.setPendingBonus(calculatedPendingBonus);
    allowanceRepository.save(allowance);
  }

  /**
   * Apply request after approval or cancellation on the request user allowance.
   *
   * @param request the Request.
   */
  @Transactional
  public void applyRequest(Request request, boolean requestCanceled) {
    var absence = request.getAbsence();
    if (!absence.isDeducted() && !absence.isSickness()) {
      return;
    }

    var yearOfRequest = request.getDays().get(0).getDate().getYear();
    var requestForPreviousYear = isRequestForPreviousYear(yearOfRequest, request);
    var currentYearAllowance = getByUserAndYear(request.getUser().getId(), yearOfRequest);

    var workingDays = getWorkingDaysOnly(request.getDays());
    var hours = calculateHours(workingDays);
    if (requestCanceled) {
      hours = -hours;
    }

    var absenceType = absence.getAbsenceType();
    switch (absenceType) {
      case LEAVE:
        checkRemainingAnnualHours(currentYearAllowance, hours);
        if (requestCanceled) {
          cancelAnnual(currentYearAllowance, hours);
        } else {
          applyAnnual(currentYearAllowance, hours);
        }
        break;
      case TRAINING:
        checkRemainingTrainingHours(currentYearAllowance, hours);
        applyTraining(currentYearAllowance, hours);
        break;
      case SICKNESS:
        applySickness(currentYearAllowance, hours);
        break;
      case BONUS:
        checkRemainingBonusHours(currentYearAllowance, hours);
        applyBonus(currentYearAllowance, hours);
        if (requestForPreviousYear) {
          var nextYearAllowance = getByUserAndYear(request.getUser().getId(), yearOfRequest + 1);
          addHoursToCarriedOver(nextYearAllowance, request.getUser().getLeaveProfile(), hours);
        }
        break;
      default:
        throw new IllegalArgumentException("Unsupported absence type: " + absenceType);
    }
  }

  /**
   * If next year is active, gets taken annual hours for current and next year. Checks if taken annual hours for next year minus
   * canceled hours is less than zero.
   * <p>
   * If it is, it means that some or all of those canceled hours belong to current year and rest to next year.
   * If it isn't, it means that all of those canceled hours belong to next year.
   * Also, if next year isn't active, all canceled hours belong to current year.
   */
  private void cancelAnnual(Allowance currentYearAllowance, int requestedHours) {
    var currentYearTakenAnnual = currentYearAllowance.getTakenAnnual();

    if (nextYearAllowanceExists(currentYearAllowance)) {
      var nextYearAllowance = getNextYearAllowance(currentYearAllowance);
      var nextYearTakenInPreviousYear = nextYearAllowance.getTakenInPreviousYear();
      var nextYearRequestedHours = nextYearTakenInPreviousYear + requestedHours;
      if (nextYearRequestedHours < 0) {
        currentYearAllowance.setTakenAnnual(currentYearTakenAnnual + nextYearRequestedHours);
        nextYearAllowance.setTakenInPreviousYear(0);

        allowanceRepository.save(currentYearAllowance);
      } else {
        nextYearAllowance.setTakenInPreviousYear(nextYearRequestedHours);
      }

      allowanceRepository.save(nextYearAllowance);
    } else {
      currentYearAllowance.setTakenAnnual(currentYearTakenAnnual + requestedHours);
      allowanceRepository.save(currentYearAllowance);
    }
  }

  /**
   * Gets taken annual hours for current year and for next year if year is active. Checks if there is less available hours in current
   * year then is requested hours.
   * <p>
   * If it is, it means that some or all of requested hours will be taken from next year allowance if next year is active.
   * If it isn't, it means that all of requested hours will be taken from current year if there is enough remaining hours.
   * if there is not enough annual hours in current year and next year isn't active, request won't be created.
   */
  private void applyAnnual(Allowance currentYearAllowance, int requestedHours) {
    var currentYearAnnual = currentYearAllowance.getTakenAnnual();
    var remainingAnnualHoursCurrentYear = calculateRemainingAnnualHoursWithoutPending(currentYearAllowance);
    var nextYearRequestedHours = requestedHours - remainingAnnualHoursCurrentYear;

    if (nextYearAllowanceExists(currentYearAllowance)) {
      var nextYearAllowance = getNextYearAllowance(currentYearAllowance);
      var nextYearTakenInPreviousYear = nextYearAllowance.getTakenInPreviousYear();
      nextYearRequestedHours += nextYearTakenInPreviousYear;

      if (nextYearRequestedHours > 0) {
        currentYearAllowance.setTakenAnnual(currentYearAnnual + remainingAnnualHoursCurrentYear);
        nextYearAllowance.setTakenInPreviousYear(nextYearRequestedHours);
        allowanceRepository.save(nextYearAllowance);
      } else {
        currentYearAllowance.setTakenAnnual(currentYearAnnual + requestedHours);
      }

    } else {
      currentYearAllowance.setTakenAnnual(currentYearAnnual + requestedHours);
    }
    allowanceRepository.save(currentYearAllowance);
  }

  private void applyTraining(Allowance allowance, int requestedHours) {
    var calculatedTraining = allowance.getTakenTraining() + requestedHours;
    allowance.setTakenTraining(calculatedTraining);
    allowanceRepository.save(allowance);
  }

  private void applySickness(Allowance allowance, int hours) {
    var calculatedSickness = allowance.getSickness() + hours;
    allowance.setSickness(calculatedSickness);
    allowanceRepository.save(allowance);
  }

  private void applyBonus(Allowance allowance, int hours) {
    int calculatedBonus = allowance.getApprovedBonus() + hours;
    allowance.setApprovedBonus(calculatedBonus);
    allowanceRepository.save(allowance);
  }

  private void addHoursToCarriedOver(Allowance nextYearAllowance, LeaveProfile leaveProfile, int hours) {
    var alreadyCarriedOver = nextYearAllowance.getCarriedOver();
    var availableHours = getAvailableHoursForCarryOver(leaveProfile, nextYearAllowance, hours);
    nextYearAllowance.setCarriedOver(alreadyCarriedOver + availableHours);

    allowanceRepository.save(nextYearAllowance);
  }

  private int getAvailableHoursForCarryOver(LeaveProfile leaveProfile, Allowance nextYearAllowance, int hours) {
    var remainingHoursForCarryOver = leaveProfile.getMaxCarriedOver() - nextYearAllowance.getCarriedOver();

    return Math.min(hours, remainingHoursForCarryOver);
  }

  private boolean isRequestForPreviousYear(int yearOfRequest, Request request) {
    return yearOfRequest < request.getSubmissionTime().getYear();
  }

  private List<Day> getWorkingDaysOnly(List<Day> days) {
    var workingDaysWithoutWeekend = getWorkingDaysWithoutWeekend(days);

    Set<Integer> requestYears = workingDaysWithoutWeekend.stream()
                                                         .map(Day::getDate)
                                                         .map(LocalDate::getYear)
                                                         .collect(Collectors.toCollection(LinkedHashSet::new));

    var yearFrom = requestYears.stream().findFirst().orElse(0);
    var yearTo = requestYears.stream().reduce((a, b) -> b).orElse(0);
    var startYearFrom = LocalDate.of(yearFrom, 1, 1);
    var endYearTo = LocalDate.of(yearTo, 12, 31);
    var publicHolidays = publicHolidayRepository.findAllByDateIsBetween(startYearFrom, endYearTo);

    return getWorkingDaysWithoutPublicHolidays(workingDaysWithoutWeekend, publicHolidays);
  }

  private List<Day> getWorkingDaysWithoutPublicHolidays(List<Day> workingDaysWithoutWeekend,
      List<PublicHoliday> publicHolidays) {
    return workingDaysWithoutWeekend.stream().filter(isWorkday(publicHolidays)).collect(Collectors.toList());
  }

  private Predicate<Day> isWorkday(List<PublicHoliday> publicHolidays) {
    return day -> publicHolidays.stream().noneMatch(publicHoliday -> publicHoliday.getDate().equals(day.getDate()));
  }

  private List<Day> getWorkingDaysWithoutWeekend(List<Day> days) {
    return days.stream().filter(isWorkday()).collect(Collectors.toList());
  }

  private Predicate<Day> isWorkday() {
    return day -> !(DayOfWeek.SATURDAY.equals(day.getDate().getDayOfWeek()) || DayOfWeek.SUNDAY.equals(
        day.getDate().getDayOfWeek()));
  }

  public int calculateHours(List<Day> days) {
    return days.stream().mapToInt(day -> day.getDuration().getHours()).sum();
  }

  /**
   * Checks remaining annual hours that user has available for current and next year.
   * If user requested more hours than it is available exception is thrown.
   */
  private void checkRemainingAnnualHours(Allowance currentYearAllowance, int requestedHours) {

    var userEmail = currentYearAllowance.getUser().getEmail();
    var remainingHoursCurrentYear = calculateRemainingAnnualHours(currentYearAllowance);
    int remainingHoursNextYear = getRemainingHoursNextYear(currentYearAllowance);

    if (requestedHours > remainingHoursCurrentYear + remainingHoursNextYear) {
      logAndThrowInsufficientHoursException(remainingHoursCurrentYear, requestedHours, userEmail, ANNUAL);
    }
  }

  /**
   * Calculates available hours for use in current year for given user.
   * That is all available hours that user has
   * minus hours that he already spent, minus hours that are pending for approval and hours used in previous year.
   */
  public int calculateRemainingAnnualHours(Allowance allowance) {
    var totalHours = calculateTotalAnnualHours(allowance);
    var takenAnnual = allowance.getTakenAnnual();
    var pendingAnnual = allowance.getPendingAnnual();
    var usedInPreviousYear = allowance.getTakenInPreviousYear() + allowance.getPendingInPreviousYear();

    return totalHours - takenAnnual - pendingAnnual - usedInPreviousYear;
  }

  private int calculateRemainingAnnualHoursWithoutPending(Allowance allowance) {
    var totalHours = calculateTotalAnnualHours(allowance);
    var takenAnnual = allowance.getTakenAnnual();
    var usedInPreviousYear = allowance.getTakenInPreviousYear();

    return totalHours - takenAnnual - usedInPreviousYear;
  }

  public int calculateTotalAnnualHours(Allowance allowance) {
    return
        allowance.getAnnual() + allowance.getApprovedBonus() + allowance.getCarriedOver() + allowance.getManualAdjust()
            + allowance.getBonusManualAdjust() - allowance.getExpiredCarriedOver();
  }

  public int getRemainingHoursNextYear(Allowance currentYearAllowance) {
    var remainingHoursNextYear = 0;
    if (nextYearAllowanceExists(currentYearAllowance)) {
      var nextYearAllowance = getNextYearAllowance(currentYearAllowance);
      remainingHoursNextYear = calculateNextYearRemainingAnnualHours(nextYearAllowance);
    }

    return remainingHoursNextYear;
  }

  /**
   * Calculates available hours for use in next year for given user.
   * Those hours are calculated based on spent annual hours plus hours that are pending for approval
   * and maximum allowed hours from next year that user can use in current year with is set based on users LeaveProfile.
   */
  public int calculateNextYearRemainingAnnualHours(Allowance allowance) {
    var maxAllowedAllowanceFromNextYear = allowance.getUser().getLeaveProfile().getMaxAllowanceFromNextYear();
    var allowanceFromNextYear = allowance.getPendingInPreviousYear() + allowance.getTakenInPreviousYear();

    return maxAllowedAllowanceFromNextYear - allowanceFromNextYear;
  }

  private void checkRemainingTrainingHours(Allowance allowance, int requestedHours) {
    var userEmail = allowance.getUser().getEmail();
    var remainingHours = calculateRemainingTrainingHours(allowance);
    if (requestedHours > remainingHours) {
      logAndThrowInsufficientHoursException(remainingHours, requestedHours, userEmail, TRAINING);
    }
  }

  public int calculateRemainingTrainingHours(Allowance allowance) {
    var totalTraining = allowance.getTraining();
    var trainingManualAdjust = allowance.getTrainingManualAdjust();
    var takenTraining = allowance.getTakenTraining();
    var pendingTraining = allowance.getPendingTraining();

    return totalTraining + trainingManualAdjust - takenTraining - pendingTraining;
  }

  private void checkRemainingBonusHours(Allowance allowance, int requestedHours) {
    var userEmail = allowance.getUser().getEmail();
    var remainingBonusHours = calculateRemainingBonusHours(allowance);
    if (requestedHours > remainingBonusHours) {
      logAndThrowInsufficientHoursException(remainingBonusHours, requestedHours, userEmail, BONUS);
    }
  }

  public int calculateRemainingBonusHours(Allowance allowance) {
    var totalBonus = allowance.getBonus();
    var approvedBonus = allowance.getApprovedBonus();
    var pendingBonus = allowance.getPendingBonus();

    return totalBonus - approvedBonus - pendingBonus;
  }

  private void logAndThrowInsufficientHoursException(int remainingHours, int requestedHours, String userEmail,
      String leaveType) {
    log.error("Insufficient hours: available '{}', requested '{}', for user with email '{}'", remainingHours,
        requestedHours, userEmail);
    throw new InsufficientHoursException(leaveType);
  }

  public boolean nextYearAllowanceExists(Allowance currentYearAllowance) {
    var year = currentYearAllowance.getYear().getYear() + 1;
    var userId = currentYearAllowance.getUser().getId();
    return allowanceRepository.existsByUserIdAndYearYear(userId, year);
  }

  private Allowance getNextYearAllowance(Allowance currentYearAllowance) {
    var year = currentYearAllowance.getYear().getYear() + 1;
    var userId = currentYearAllowance.getUser().getId();

    return allowanceRepository.findByUserIdAndYearYear(userId, year);
  }

  public Map<String, Integer> getFirstAndLastAllowancesYear(User authUser) {
    List<Allowance> allowances = getAllByUserId(authUser.getId());
    Map<String, Integer> firstAndLastYear = new HashMap<>();

    IntSummaryStatistics firstAndLastAllowanceYear = allowances.stream()
                                                               .map((allowance -> allowance.getYear().getYear()))
                                                               .collect(Collectors.summarizingInt(Integer::intValue));

    firstAndLastYear.put("first", firstAndLastAllowanceYear.getMin());
    firstAndLastYear.put("last", firstAndLastAllowanceYear.getMax());

    return firstAndLastYear;
  }

  public List<Integer> getAllYearsWithAllowanceForUser(User user) {
    return allowanceRepository.findAllYearsWithAllowanceForUser(user.getId());
  }

  public List<Integer> getAllYearsWithAllowance() {
    return allowanceRepository.findAllYearsWithAllowance();
  }

  @Transactional
  public Allowance update(Allowance updatedAllowance, User modifiedByUser) {
    saveAuditInformation(OperationPerformed.UPDATE, modifiedByUser, updatedAllowance);

    return allowanceRepository.save(updateAllowanceValues(updatedAllowance));
  }

  @Transactional
  public void delete(Allowance allowance, User modifiedByUser) {
    saveAuditInformation(OperationPerformed.DELETE, modifiedByUser, allowance);
    allowanceRepository.deleteById(allowance.getId());
  }

  private void saveAuditInformation(OperationPerformed action, User modifiedByUser, Allowance updatedAllowance) {
    AllowanceAudit previousAllowanceState = getPreviousAllowanceState(updatedAllowance);
    AllowanceAudit currentAllowanceState = AllowanceAudit.fromAllowance(updatedAllowance);

    auditInformationService.saveAudit(action, modifiedByUser, updatedAllowance.getId(), previousAllowanceState,
        currentAllowanceState);
  }

  private AllowanceAudit getPreviousAllowanceState(Allowance updatedAllowance) {
    Optional<Allowance> oldAllowance = allowanceRepository.findByIdRequireNew(updatedAllowance.getId());

    return AllowanceAudit.fromAllowance(oldAllowance.orElse(new Allowance()));
  }

  private Allowance updateAllowanceValues(Allowance updatedAllowance) {
    var allowance = allowanceRepository.getOne(updatedAllowance.getId());
    var remainingAllowance = allowance.getAnnual() - allowance.getPendingAnnual() - allowance.getTakenAnnual();
    if (remainingAllowance + updatedAllowance.getManualAdjust() < 0) {
      throw new NegativeAllowanceException(allowance.getUser(), remainingAllowance);
    }
    allowance.setBonusManualAdjust(updatedAllowance.getBonusManualAdjust());
    allowance.setCarriedOver(updatedAllowance.getCarriedOver());
    allowance.setTrainingManualAdjust(updatedAllowance.getTrainingManualAdjust());
    allowance.setManualAdjust(updatedAllowance.getManualAdjust());

    return allowance;
  }

  public List<Allowance> getAllowanceByYearAndCarriedOverGreaterThan(Year year) {
    return allowanceRepository.findAllByYearAndCarriedOverGreaterThan(year, 0);
  }

  public Page<Allowance> getAllowancesFiltered(
      int year, Long teamId, List<UserStatusType> statusType, Pageable pageable) {
    var allowances = teamId == null
            ? allowanceRepository.findAllByYearYearAndUserUserStatusTypeInOrderByUserFullName(year, statusType)
            : allowanceRepository.findAllByYearYearAndUserUserStatusTypeInAndUserTeamIdOrderByUserFullName(
                year, statusType, teamId);

    return createPageableAllowancesList(allowances, pageable);
  }

  public Page<Allowance> getAllowancesFilteredSearch(
      int year, Long teamId, List<UserStatusType> statusType, String query, Pageable pageable) {
    if (query.length() < MINIMUM_QUERY_LENGTH) {
      return Page.empty(pageable);
    }
    var allowances = teamId == null
            ? allowanceRepository.searchByYearAndUser(year, statusType, query, query)
            : allowanceRepository.searchByYearAndUserAndTeam(year, statusType, query, query, teamId);

    return createPageableAllowancesList(allowances, pageable);
  }

  private Page<Allowance> createPageableAllowancesList(List<Allowance> allowances, Pageable pageable) {
    long start = pageable.getOffset();
    long end = start + pageable.getPageSize() > allowances.size()
            ? allowances.size()
            : start + pageable.getPageSize();

    return new PageImpl<>(allowances.subList((int) start, (int) end), pageable, allowances.size());
  }
}
