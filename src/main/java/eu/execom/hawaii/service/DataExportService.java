package eu.execom.hawaii.service;

import eu.execom.hawaii.dto.export.SortDto;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.AllowanceRepository;
import eu.execom.hawaii.repository.LeaveProfileRepository;
import eu.execom.hawaii.repository.RequestRepository;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.util.export.AllowanceCsvExportAssembler;
import eu.execom.hawaii.util.export.AllowancePdfExportAssembler;
import eu.execom.hawaii.util.export.ExportFile;
import eu.execom.hawaii.util.export.LeaveProfilesCsvAssembler;
import eu.execom.hawaii.util.export.LeaveProfilesPdfExportAssembler;
import eu.execom.hawaii.util.export.RequestCsvExportAssembler;
import eu.execom.hawaii.util.export.RequestPdfExportAssembler;
import eu.execom.hawaii.util.export.UserCsvExportAssembler;
import eu.execom.hawaii.util.export.UserPdfExportAssembler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DataExportService {

  private static final Map<String, String> DEFAULT_SORT_PROPERTIES = Map.of("requests", "day.date", "users", "fullName",
      "allowances", "user.fullName");
  private static final Map<String, Sort.Direction> DEFAULT_SORT_DIRECTIONS = Map.of("requests", Sort.Direction.DESC,
      "users", Sort.Direction.ASC, "allowances", Sort.Direction.ASC);
  private static final int MAX_SORT_PROPERTIES = 3;
  private static final String EXCEEDED_NUMBER_OF_PROPERTIES_MASSAGE = "Sort can be done for maximum '%S' properties.";

  private RequestRepository requestRepository;
  private TeamRepository teamRepository;
  private UserRepository userRepository;
  private AllowanceRepository allowanceRepository;
  private LeaveProfileRepository leaveProfileRepository;
  private UserCsvExportAssembler userCsvExportAssembler;
  private UserPdfExportAssembler userPdfExportAssembler;
  private AllowanceCsvExportAssembler allowanceCsvExportAssembler;
  private AllowancePdfExportAssembler allowancePdfExportAssembler;
  private LeaveProfilesCsvAssembler leaveProfilesCsvAssembler;
  private LeaveProfilesPdfExportAssembler leaveProfilesPdfExportAssembler;
  private RequestCsvExportAssembler requestCsvExportAssembler;
  private RequestPdfExportAssembler requestPdfExportAssembler;

  @Autowired
  public DataExportService(RequestRepository requestRepository, TeamRepository teamRepository,
      UserRepository userRepository, AllowanceRepository allowanceRepository,
      UserCsvExportAssembler userCsvExportAssembler, UserPdfExportAssembler userPdfExportAssembler,
      AllowanceCsvExportAssembler allowanceCsvExportAssembler, AllowancePdfExportAssembler allowancePdfExportAssembler,
      LeaveProfileRepository leaveProfileRepository, LeaveProfilesCsvAssembler leaveProfilesCsvAssembler,
      LeaveProfilesPdfExportAssembler leaveProfilesPdfExportAssembler,
      RequestCsvExportAssembler requestCsvExportAssembler, RequestPdfExportAssembler requestPdfExportAssembler) {
    this.requestRepository = requestRepository;
    this.teamRepository = teamRepository;
    this.userRepository = userRepository;
    this.allowanceRepository = allowanceRepository;
    this.userCsvExportAssembler = userCsvExportAssembler;
    this.userPdfExportAssembler = userPdfExportAssembler;
    this.allowanceCsvExportAssembler = allowanceCsvExportAssembler;
    this.allowancePdfExportAssembler = allowancePdfExportAssembler;
    this.leaveProfileRepository = leaveProfileRepository;
    this.leaveProfilesCsvAssembler = leaveProfilesCsvAssembler;
    this.leaveProfilesPdfExportAssembler = leaveProfilesPdfExportAssembler;
    this.requestCsvExportAssembler = requestCsvExportAssembler;
    this.requestPdfExportAssembler = requestPdfExportAssembler;
  }

  public ExportFile exportRequests(String exportFormat, Long teamId, List<UserStatusType> userStatusTypes,
      AbsenceType absenceType, LocalDate fromDate, LocalDate toDate, SortDto sortDto) {
    String fileFormat = determineFileFormat(exportFormat);
    String fileName = createRequestsExportFileName(fileFormat, teamId, userStatusTypes, absenceType, fromDate, toDate);
    Sort sort = createSort(sortDto, "requests");
    var bodyData = getRequestExportFile(fileFormat, teamId, userStatusTypes, absenceType, fromDate, toDate, sort);
    return new ExportFile(bodyData, fileName, fileFormat);
  }

  private String createRequestsExportFileName(String fileFormat, Long teamId, List<UserStatusType> userStatusTypes,
      AbsenceType absenceType, LocalDate startDate, LocalDate endDate) {
    String absenceName = assignAbsenceName(absenceType);
    String dateRange = assignDateRange(startDate, endDate);
    String teamName = assignTeamName(teamId);
    String userStatusTypeName = assignUserStatusTypeName(userStatusTypes);

    return teamName + absenceName + dateRange + userStatusTypeName + assignFileSuffix(fileFormat);
  }

  private String assignAbsenceName(AbsenceType absenceType) {
    var teamName = absenceType == null ? "all-requests" : getAbsenceName(absenceType).toLowerCase();
    return "-" + teamName;
  }

  private String getAbsenceName(AbsenceType absenceType) {
    String fileName;
    switch (absenceType) {
      case LEAVE:
        fileName = "Leave";
        break;
      case SICKNESS:
        fileName = "Sickness";
        break;
      case TRAINING:
        fileName = "Training";
        break;
      case BONUS:
        fileName = "Bonus";
        break;
      default:
        throw new IllegalArgumentException(
            "Exporting data of type: " + absenceType + " is not supported or data type does not exist");
    }

    return fileName;
  }

  private String assignDateRange(LocalDate startDate, LocalDate endDate) {
    return startDate != null ? "-" + startDate + "-to-" + endDate : "";
  }

  /**
   * Creates Sort object for sorting entities requested from db. If properties from sortDto is not set default
   * Sort object based on export type will be created.
   *
   * @param sortDto defining multiple sort criteria.
   * @param exportType defines default Sort object.
   * @return Sort object.
   */
  public Sort createSort(SortDto sortDto, String exportType) {
    List<String> properties = sortDto.getProperties();
    boolean areSortPropertiesAssigned = !(properties == null || properties.isEmpty());
    if (!areSortPropertiesAssigned) {
      return createDefaultSort(exportType);
    }

    int propertiesLength = properties.size();
    validateNumberOfSortProperties(propertiesLength);

    List<Sort.Direction> directions = sortDto.getDirections() == null ? new ArrayList<>() : sortDto.getDirections();
    checkIfSortDirectionIsSetForEveryProperty(propertiesLength, directions);

    List<Sort.Order> orders = new ArrayList<>();
    for (int i = 0; i < properties.size(); i++) {
      Sort.Order order = new Sort.Order(directions.get(i), properties.get(i));
      orders.add(order);
    }

    return Sort.by(orders);
  }

  /**
   * Returns default Sort object for different export types.
   *
   * @param exportType type of export.
   * @return Sort object.
   */
  private Sort createDefaultSort(String exportType) {
    Sort.Direction defaultDirection = DEFAULT_SORT_DIRECTIONS.get(exportType);
    String defaultProperty = DEFAULT_SORT_PROPERTIES.get(exportType);
    return Sort.by(new Sort.Order(defaultDirection, defaultProperty));
  }

  /**
   * Throws exception if number of sort properties exceeds maximum allowed.
   *
   * @param propertiesLength number of properties for multiple sorting.
   */
  private void validateNumberOfSortProperties(int propertiesLength) {
    if (propertiesLength > MAX_SORT_PROPERTIES) {
      String message = String.format(EXCEEDED_NUMBER_OF_PROPERTIES_MASSAGE, MAX_SORT_PROPERTIES);
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Checks if sort direction is set for each property. If not default direction ASC will be set.
   *
   * @param propertiesLength number of sort properties.
   * @param directions list of set directions.
   */
  private void checkIfSortDirectionIsSetForEveryProperty(int propertiesLength, List<Sort.Direction> directions) {
    while (propertiesLength > directions.size()) {
      directions.add(Sort.Direction.ASC);
    }
  }

  private <T> T getRequestExportFile(String fileFormat, Long teamId, List<UserStatusType> userStatusTypes,
      AbsenceType absenceType, LocalDate fromDate, LocalDate toDate, Sort sort) {
    var requests = getRequestForExport(teamId, userStatusTypes, absenceType, fromDate, toDate, sort);
    if (fileFormat.equals("csv")) {
      return (T) requestCsvExportAssembler.getRequestExportCsv(requests, absenceType);
    }
    return (T) new InputStreamResource(requestPdfExportAssembler.getRequestExportPdf(requests));
  }

  private List<Request> getRequestForExport(Long teamId, List<UserStatusType> userStatusTypes, AbsenceType absenceType,
      LocalDate startDate, LocalDate endDate, Sort sort) {
    boolean absenceTypesEmpty = absenceType == null;
    List<AbsenceType> absenceTypes = getAbsenceTypesForExport(absenceType);

    return requestRepository.findAllRequestsForExport(userStatusTypes, absenceTypes, teamId, startDate, endDate,
        absenceTypesEmpty, sort);
  }

  private List<AbsenceType> getAbsenceTypesForExport(AbsenceType absenceType) {
    if (absenceType == null) {
      return null;
    }
    List<AbsenceType> absenceTypes = new ArrayList<>(List.of(absenceType));
    if (absenceType.equals(AbsenceType.LEAVE)) {
      absenceTypes.add(AbsenceType.TRAINING);
    }
    return absenceTypes;
  }

  public ExportFile exportUsers(String exportFormat, Long teamId, List<UserStatusType> userStatusTypes, SortDto sortDto) {
    String fileFormat = determineFileFormat(exportFormat);
    String fileName = createUserExportFileName(exportFormat, teamId, userStatusTypes);
    Sort sort = createSort(sortDto, "users");
    var bodyData = getUserExportFile(fileFormat, teamId, userStatusTypes, sort);
    return new ExportFile(bodyData, fileName, fileFormat);
  }

  private String createUserExportFileName(String exportFormat, Long teamId, List<UserStatusType> userStatusTypes) {
    String teamName = assignTeamName(teamId);
    String userStatusTypeName = assignUserStatusTypeName(userStatusTypes);

    return "export-for-" + teamName + userStatusTypeName + assignFileSuffix(exportFormat);
  }

  private <T> T getUserExportFile(String fileFormat, Long teamId, List<UserStatusType> userStatusTypes, Sort sort) {
    var users = getUsersForExport(teamId, userStatusTypes, sort);
    if (fileFormat.equals("csv")) {
      return (T) userCsvExportAssembler.getUserExportCsv(users);
    }
    return (T) new InputStreamResource(userPdfExportAssembler.getUserExportPdf(users));
  }

  private List<User> getUsersForExport(Long teamId, List<UserStatusType> userStatusTypes, Sort sort) {
    List<User> users = userRepository.getUsersForExport(teamId, userStatusTypes, sort);

    return users.stream().filter(User::hasAnyRoleOtherThanHrAssistant).collect(Collectors.toList());
  }

  private String assignFileSuffix(String exportFormat) {
    return "." + exportFormat;
  }

  public ExportFile exportAllowances(String exportFormat, Long teamId, List<UserStatusType> userStatusTypes, int year,
      SortDto sortDto) {
    String fileFormat = determineFileFormat(exportFormat);
    String fileName = createAllowanceExportFilename(exportFormat, teamId, userStatusTypes, year);
    Sort sort = createSort(sortDto, "allowances");
    var bodyData = getAllowanceExportFile(fileFormat, teamId, userStatusTypes, year, sort);
    return new ExportFile(bodyData, fileName, fileFormat);
  }

  private String createAllowanceExportFilename(String exportFormat, Long teamId, List<UserStatusType> userStatusTypes,
      int year) {
    String teamName = assignTeamName(teamId);
    String userStatusTypeName = assignUserStatusTypeName(userStatusTypes);
    return year + "-allowance-" + teamName + userStatusTypeName + assignFileSuffix(exportFormat);
  }

  private String assignUserStatusTypeName(List<UserStatusType> userStatusTypes) {
    String absenceTypesToString = userStatusTypes.stream()
                                                 .map(u -> u.toString().toLowerCase())
                                                 .collect(Collectors.joining("-"));

    return "-" + absenceTypesToString;
  }

  private String assignTeamName(Long teamId) {
    Optional<Team> team = teamId == null ? Optional.empty() : teamRepository.findById(teamId);
    return team.map(value -> "team-" + value.getName().toLowerCase()).orElse("all-employees");
  }

  private <T> T getAllowanceExportFile(String fileFormat, Long teamId, List<UserStatusType> userStatusTypes, int year,
      Sort sort) {
    var allowances = getAllowancesForExport(teamId, userStatusTypes, year, sort);
    if (fileFormat.equals("csv")) {
      return (T) allowanceCsvExportAssembler.getAllowanceExportCsv(allowances);
    }
    return (T) new InputStreamResource(allowancePdfExportAssembler.getAllowanceExportPdf(allowances));
  }

  private List<Allowance> getAllowancesForExport(Long teamId, List<UserStatusType> userStatusTypes, int year,
      Sort sort) {
    return allowanceRepository.findAllByYearTeamIdAndUserStatusType(year, teamId, userStatusTypes, sort);
  }

  public ExportFile exportLeaveProfiles(String exportFormat) {
    String fileFormat = determineFileFormat(exportFormat);
    String fileName = fileFormat.equals("csv") ? "leave-profiles.csv" : "leave-profiles.pdf";
    var bodyData = getLeaveProfileExportFile(fileFormat);
    return new ExportFile(bodyData, fileName, fileFormat);
  }

  private String determineFileFormat(String exportFormat) {
    boolean isCsvFormat = exportFormat.equalsIgnoreCase("csv");
    boolean isPdfFormat = exportFormat.equalsIgnoreCase("pdf");
    return (isCsvFormat || isPdfFormat) ? exportFormat.toLowerCase() : "csv";
  }

  private <T> T getLeaveProfileExportFile(String fileFormat) {
    var leaveProfiles = leaveProfileRepository.findAll();
    if (fileFormat.equals("csv")) {
      return (T) leaveProfilesCsvAssembler.getLeaveProfileExportCsv(leaveProfiles);
    }
    return (T) new InputStreamResource(leaveProfilesPdfExportAssembler.getLeaveProfileExportPdf(leaveProfiles));
  }
}