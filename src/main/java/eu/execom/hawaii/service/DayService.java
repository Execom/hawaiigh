package eu.execom.hawaii.service;

import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.repository.DayRepository;
import eu.execom.hawaii.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Absence type management service.
 */
@Service
public class DayService {

  private DayRepository dayRepository;
  private RequestRepository requestRepository;
  private RequestService requestService;

  @Autowired
  public DayService(DayRepository dayRepository, RequestRepository requestRepository, RequestService requestService) {
    this.dayRepository = dayRepository;
    this.requestRepository = requestRepository;
    this.requestService = requestService;
  }

  /**
   * Get Days of absence of user between two dates
   *
   * @param user - selected user.
   * @param startDate - Starting with date
   * @param endDate - Ending with date
   * @return found days
   */
  public List<Day> getUserAbsencesDays(User user, LocalDate startDate, LocalDate endDate) {
    List<Request> allRequests = requestRepository.findAllByUserId(user.getId());
    var filteredRequests = filterOutBonusCanceledRejected(allRequests);

    return dayRepository.findAllByRequestInAndDateIsBetween(filteredRequests, startDate, endDate);
  }

  private List<Request> filterOutBonusCanceledRejected(List<Request> requests) {
    return requests.stream().filter(requestService.withoutCanceledRejectedBonus()).collect(Collectors.toList());
  }

}
