package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.UserAdminDto;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.service.UserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@RestController
@RequestMapping("/api/admins")
public class AdminController {

  private static final ModelMapper MAPPER = new ModelMapper();

  private UserService userService;

  @Autowired
  public AdminController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping
  public ResponseEntity<List<UserAdminDto>> getAdmins() {
    List<User> admins = userService.findAllAdmins();
    Type adminDtosType = new TypeToken<List<UserAdminDto>>() {}.getType();
    List<UserAdminDto> adminDtos = MAPPER.map(admins, adminDtosType);
    return new ResponseEntity<>(adminDtos, HttpStatus.OK);
  }

  @GetMapping("/users")
  public ResponseEntity<List<UserAdminDto>> getUsers(@RequestParam String searchQuery) {
    List<User> users = userService.searchAllNonAdminActiveUsersByEmailAndFullName(searchQuery);
    Type adminDtosType = new TypeToken<List<UserAdminDto>>() {}.getType();
    List<UserAdminDto> adminDtos = MAPPER.map(users, adminDtosType);
    return new ResponseEntity<>(adminDtos, HttpStatus.OK);
  }

  @PutMapping("/{id}")
  public ResponseEntity<UserAdminDto> updateAdminPermission(@Valid @ApiIgnore @AuthenticationPrincipal User authUser,
      @PathVariable Long id, @RequestBody @Valid UserAdminDto userAdminDto) {
    userAdminDto.setId(id);
    User updatedUser = MAPPER.map(userAdminDto, User.class);
    User persistedUser = userService.updateAdmin(updatedUser, authUser);
    return new ResponseEntity<>(MAPPER.map(persistedUser, UserAdminDto.class), HttpStatus.OK);
  }
}
