package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.HardwareMetricsDto;
import eu.execom.hawaii.model.metric.HardwareMetrics;
import eu.execom.hawaii.model.metric.HttpEndpointMetrics;
import eu.execom.hawaii.service.HardwareMetricsService;
import eu.execom.hawaii.service.HttpEndpointMetricsService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/metrics")
public class MetricController {

  private static final ModelMapper MAPPER = new ModelMapper();

  private HardwareMetricsService hardwareMetricsService;
  private HttpEndpointMetricsService httpEndpointMetricsService;

  @Autowired
  public MetricController(HardwareMetricsService hardwareMetricsService,
      HttpEndpointMetricsService httpEndpointMetricsService) {
    this.hardwareMetricsService = hardwareMetricsService;
    this.httpEndpointMetricsService = httpEndpointMetricsService;
  }

  @GetMapping("/hardware")
  public ResponseEntity<List<HardwareMetricsDto>> getHardwareMetrics(
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime fromTimestamp,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime toTimestamp) {
    List<HardwareMetrics> hardwareMetrics = hardwareMetricsService.findAllByTimestampBetween(fromTimestamp,
        toTimestamp);
    Type hardwareMetricDtosType = new TypeToken<List<HardwareMetricsDto>>() {}.getType();
    List<HardwareMetricsDto> hardwareMetricDtos = MAPPER.map(hardwareMetrics, hardwareMetricDtosType);
    return new ResponseEntity<>(hardwareMetricDtos, HttpStatus.OK);
  }

  @GetMapping("/oldestTimestamp")
  public ResponseEntity<LocalDateTime> getOldestTimestamp() {
    LocalDateTime oldestTimestamp = hardwareMetricsService.getOldestTimestamp();
    return new ResponseEntity<>(oldestTimestamp, HttpStatus.OK);
  }

  @GetMapping("/http")
  public ResponseEntity<List<HttpEndpointMetrics>> getHttpEndpointMetrics() {
    List<HttpEndpointMetrics> httpEndpointMetrics = httpEndpointMetricsService.findAll();
    return new ResponseEntity<>(httpEndpointMetrics, HttpStatus.OK);
  }
}
