package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.PublicHolidayDto;
import eu.execom.hawaii.model.PublicHoliday;
import eu.execom.hawaii.service.PublicHolidayService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/publicholidays")
public class PublicHolidayController {

  private static final ModelMapper MAPPER = new ModelMapper();

  private PublicHolidayService publicHolidayService;

  @Autowired
  public PublicHolidayController(PublicHolidayService publicHolidayService) {
    this.publicHolidayService = publicHolidayService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<PublicHolidayDto> getPublicHoliday(@PathVariable Long id) {
    var publicHoliday = publicHolidayService.getById(id);
    var publicHolidayDto = new PublicHolidayDto(publicHoliday);

    return new ResponseEntity<>(publicHolidayDto, HttpStatus.OK);
  }

  @GetMapping
  public ResponseEntity<List<PublicHolidayDto>> getPublicHolidays(
      @RequestParam(defaultValue = "false") boolean deleted) {
    var publicHolidays = publicHolidayService.findAllByDeleted(deleted);
    var publicHolidayDtos = publicHolidays.stream().map(PublicHolidayDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(publicHolidayDtos, HttpStatus.OK);
  }

  @GetMapping("/administration")
  public ResponseEntity<Page<Integer>> getAllYearsFromPublicHoliday(
      @PageableDefault(size = 150, sort = "date") Pageable pageable) {
    Page<Integer> years = publicHolidayService.findAll(pageable)
                                              .map(PublicHoliday::getDate)
                                              .map(LocalDate::getYear)
                                              .stream()
                                              .distinct()
                                              .collect(toPagedYears(pageable));

    return new ResponseEntity<>(years, HttpStatus.OK);
  }

  private Collector<Integer, Object, PageImpl<Integer>> toPagedYears(Pageable pageable) {
    return Collectors.collectingAndThen(Collectors.toList(), years -> new PageImpl<>(years, pageable, years.size()));
  }

  @GetMapping("/year")
  public ResponseEntity<List<PublicHolidayDto>> getPublicHolidaysByYear(@RequestParam(required = false) Integer year) {
    year = year == null ? LocalDate.now().getYear() : year;
    List<PublicHolidayDto> publicHolidayDtos = publicHolidayService.findAllByYear(year)
                                                                   .stream()
                                                                   .map(PublicHolidayDto::new)
                                                                   .collect(Collectors.toList());

    return new ResponseEntity<>(publicHolidayDtos, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<PublicHolidayDto> createPublicHoliday(@RequestBody @Valid PublicHolidayDto publicHolidayDto) {
    var publicHoliday = MAPPER.map(publicHolidayDto, PublicHoliday.class);
    publicHoliday = publicHolidayService.save(publicHoliday);
    var publicHolidayDtoResponse = new PublicHolidayDto(publicHoliday);

    return new ResponseEntity<>(publicHolidayDtoResponse, HttpStatus.OK);
  }

  @PutMapping("/{id}")
  public ResponseEntity<PublicHolidayDto> updatePublicHoliday(
      @PathVariable Long id, @RequestBody @Valid PublicHolidayDto publicHolidayDto) {
    publicHolidayDto.setId(id);
    var publicHoliday = MAPPER.map(publicHolidayDto, PublicHoliday.class);
    publicHoliday = publicHolidayService.update(publicHoliday);
    var publicHolidayDtoResponse = new PublicHolidayDto(publicHoliday);

    return new ResponseEntity<>(publicHolidayDtoResponse, HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deletePublicHoliday(@PathVariable Long id) {
    publicHolidayService.delete(id);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }
}
