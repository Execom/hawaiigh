package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.RequestDto;
import eu.execom.hawaii.dto.RequestViewDto;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import eu.execom.hawaii.service.RequestService;
import eu.execom.hawaii.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/requests")
public class RequestController {

  private static final ModelMapper MAPPER = new ModelMapper();

  private RequestService requestService;
  private UserService userService;

  @Autowired
  public RequestController(RequestService requestService, UserService userService) {
    this.requestService = requestService;
    this.userService = userService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<RequestDto> getById(@PathVariable Long id, @ApiIgnore @AuthenticationPrincipal User authUser) {
    var request = requestService.getById(id, authUser);

    return new ResponseEntity<>(new RequestDto(request), HttpStatus.OK);
  }

  @GetMapping("/month")
  public ResponseEntity<List<RequestDto>> getAllRequestsByMonthOfYear(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
    List<Request> requests = requestService.findAllByDateRange(date);
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  @GetMapping("/approval")
  public ResponseEntity<List<RequestDto>> getAllRequestsForApproval(@ApiIgnore @AuthenticationPrincipal User authUser) {
    var loggedUser = userService.findByEmail(authUser.getEmail());
    List<Request> requests = requestService.getPendingRequestsFromTeams(loggedUser);
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  @GetMapping("/years/range")
  public ResponseEntity<Map<String, Integer>> getFirstAndLastRequestsYear() {
    var firstAndLastRequestDates = requestService.getFirstAndLastRequestsYear();
    return new ResponseEntity<>(firstAndLastRequestDates, HttpStatus.OK);
  }

  @GetMapping("/user/{id}")
  public ResponseEntity<List<RequestDto>> getRequestsByUserId(@PathVariable Long id) {
    var requests = requestService.findAllByUser(id);
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  @GetMapping("/user")
  public ResponseEntity<List<RequestDto>> getUserRequests(@ApiIgnore @AuthenticationPrincipal User authUser) {
    var requests = requestService.findAllByUser(authUser.getId());
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  //@formatter:off
  @GetMapping("/user/month")
  public ResponseEntity<List<RequestDto>> getUserRequestsByMonth(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestParam(required = false) Long userId,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) YearMonth yearMonth) {
    //@formatter:on
    userId = userId == null ? authUser.getId() : userId;
    yearMonth = yearMonth == null ? YearMonth.now() : yearMonth;
    var startDate = yearMonth.atDay(1);
    var endDate = yearMonth.atEndOfMonth();

    var requests = requestService.findAllByUserWithinDates(startDate, endDate, userId);
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  //@formatter:off
  @GetMapping("/user/{id}/dates")
  public ResponseEntity<List<RequestDto>> getRequestsByUserByDates(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
      @PathVariable Long id) {
    //@formatter:on
    var requests = requestService.findAllByUserWithinDates(startDate, endDate, id);
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  //@formatter:off
  @GetMapping("/user/year")
  public ResponseEntity<List<RequestDto>> getUserRequestsByYear(
      @RequestParam(required = false) Long id,
      @RequestParam(required = false) Integer year,
      @ApiIgnore @AuthenticationPrincipal User authUser) {
    //@formatter:on
    List<Request> requests = requestService.findAllByUserForYear(id, year, authUser);
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  //@formatter:off
  @GetMapping("/team/month")
  public ResponseEntity<List<RequestViewDto>> getTeamRequestsByMonth(
      @RequestParam(required = false) Long teamId,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) YearMonth yearMonth) {
    //@formatter:on
    var requests = requestService.findAllByTeamByMonthOfYear(teamId, yearMonth);
    if (teamId != null) {
      var teamApproversRequests =
          requestService.findAllByTeamApproversWithinDates(teamId, yearMonth);
      requests.addAll(teamApproversRequests);
    }
    var requestDtos = requests.stream().map(RequestViewDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  @GetMapping("/requeststatus/{status}")
  public ResponseEntity<List<RequestDto>> getRequestsByRequestStatus(@PathVariable RequestStatus status) {
    var requests = requestService.findAllByRequestStatus(status);
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  @GetMapping("/absencetype/{type}")
  public ResponseEntity<List<RequestDto>> getRequestsByAbsenceType(@PathVariable AbsenceType type) {
    var requests = requestService.findAllByAbsenceType(type);
    var requestDtos = requests.stream().map(RequestDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(requestDtos, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<RequestDto> createRequest(@ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestBody @Valid RequestDto requestDto) {
    var request = MAPPER.map(requestDto, Request.class);
    request = requestService.create(request, authUser);

    return new ResponseEntity<>(new RequestDto(request), HttpStatus.OK);
  }

  @PutMapping("/{id}")
  public ResponseEntity<RequestDto> handleRequestStatus(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @PathVariable Long id,
      @RequestBody @Valid RequestDto requestDto) {
    requestDto.setId(id);
    var request = MAPPER.map(requestDto, Request.class);
    request = requestService.handleRequestStatusUpdate(request, authUser);

    return new ResponseEntity<>(new RequestDto(request), HttpStatus.OK);
  }
}