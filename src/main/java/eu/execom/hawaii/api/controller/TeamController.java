package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.TeamDto;
import eu.execom.hawaii.dto.TeamViewDto;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.service.TeamService;
import eu.execom.hawaii.service.UserRoleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/teams")
public class TeamController {

  private static final ModelMapper MAPPER = new ModelMapper();
  private static final int DEFAULT_PAGE_SIZE = 30;

  private TeamService teamService;
  private UserRoleService userRoleService;

  @Autowired
  public TeamController(TeamService teamService, UserRoleService userRoleService) {
    this.teamService = teamService;
    this.userRoleService = userRoleService;
  }

  @GetMapping
  public ResponseEntity<Page<TeamDto>> getTeams(@RequestParam(required = false, defaultValue = "0") Integer page,
      @Nullable @RequestParam(required = false) Integer size) {
    Pageable pageable = size == null ? Pageable.unpaged() : PageRequest.of(page, size);
    Page<Team> teams = teamService.findAll(pageable);

    return new ResponseEntity<>(teams.map(TeamDto::new), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<TeamDto> getTeam(@PathVariable Long id) {
    var team = teamService.getById(id);
    var teamDto = new TeamDto(team);
    return new ResponseEntity<>(teamDto, HttpStatus.OK);
  }

  @GetMapping("/search")
  public ResponseEntity<Page<TeamViewDto>> searchTeamsByName(@RequestParam String searchQuery,
      @PageableDefault(size = DEFAULT_PAGE_SIZE, sort = "name") Pageable pageable) {
    Page<Team> teams = teamService.searchTeamsByName(searchQuery, pageable);

    return new ResponseEntity<>(teams.map(TeamViewDto::new), HttpStatus.OK);
  }

  @GetMapping("/name")
  public ResponseEntity<List<TeamViewDto>> getAllTeamsName(
      @ApiIgnore @AuthenticationPrincipal User authUser) {
    var teams = teamService.getTeamsBasedOnUserRole(authUser);
    var teamDtos = teams.stream().map(TeamViewDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(teamDtos, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<TeamDto> createTeam(@ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestBody @Valid TeamDto teamDto) {
    var team = MAPPER.map(teamDto, Team.class);
    team = teamService.create(team, authUser);

    return new ResponseEntity<>(new TeamDto(team), HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<TeamDto> updateTeam(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @PathVariable Long id,
      @RequestBody @Valid TeamDto teamDto) {
    teamDto.setId(id);
    var team = MAPPER.map(teamDto, Team.class);
    userRoleService.changeUserRole(team);
    team = teamService.update(team, authUser);

    return new ResponseEntity<>(new TeamDto(team), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deleteTeam(@ApiIgnore @AuthenticationPrincipal User authUser, @PathVariable Long id) {
    teamService.delete(id, authUser);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }
}
