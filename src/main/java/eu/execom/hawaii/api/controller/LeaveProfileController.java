package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.LeaveProfileDto;
import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.service.LeaveProfileService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/leaveprofiles")
public class LeaveProfileController {

  private static final ModelMapper MAPPER = new ModelMapper();

  private LeaveProfileService leaveProfileService;

  @Autowired
  public LeaveProfileController(LeaveProfileService leaveProfileService) {
    this.leaveProfileService = leaveProfileService;
  }

  @GetMapping
  public ResponseEntity<Page<LeaveProfileDto>> getLeaveProfiles(@PageableDefault(size = 30) Pageable pageable) {
    var leaveProfiles = leaveProfileService.findAll(pageable);

    return new ResponseEntity<>(leaveProfiles.map(LeaveProfileDto::new), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<LeaveProfileDto> getLeaveProfile(@PathVariable Long id) {
    var leaveProfile = leaveProfileService.getById(id);
    var leaveProfileDto = new LeaveProfileDto(leaveProfile);

    return new ResponseEntity<>(leaveProfileDto, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<LeaveProfileDto> createLeaveProfile(@RequestBody @Valid LeaveProfileDto leaveProfileDto) {
    var leaveProfile = leaveProfileService.create(MAPPER.map(leaveProfileDto, LeaveProfile.class));

    return new ResponseEntity<>(new LeaveProfileDto(leaveProfile), HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<LeaveProfileDto> updateLeaveProfile(
      @PathVariable Long id, @RequestBody @Valid LeaveProfileDto leaveProfileDto) {
    leaveProfileDto.setId(id);
    var leaveProfile = leaveProfileService.update(MAPPER.map(leaveProfileDto, LeaveProfile.class));

    return new ResponseEntity<>(new LeaveProfileDto(leaveProfile), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deleteLeaveProfile(@PathVariable Long id) {
    leaveProfileService.delete(id);

    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }
}
