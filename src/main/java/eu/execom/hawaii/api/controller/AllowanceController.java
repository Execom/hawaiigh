package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.AllowanceEditDto;
import eu.execom.hawaii.dto.AllowanceForUserDto;
import eu.execom.hawaii.dto.AllowanceWithoutYearDto;
import eu.execom.hawaii.dto.UsersAllowancePerYearDto;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.restfactory.AllowanceDtoFactory;
import eu.execom.hawaii.service.AllowanceService;
import eu.execom.hawaii.service.UserService;
import liquibase.util.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/allowances")
public class AllowanceController {

  private static final ModelMapper MAPPER = new ModelMapper();
  private static final int DEFAULT_PAGE_SIZE = 30;

  private AllowanceService allowanceService;
  private UserService userService;
  private AllowanceDtoFactory allowanceDtoFactory;

  @Autowired
  public AllowanceController(AllowanceService allowanceService, UserService userService,
      AllowanceDtoFactory allowanceDtoFactory) {
    this.allowanceService = allowanceService;
    this.userService = userService;
    this.allowanceDtoFactory = allowanceDtoFactory;
  }

  @GetMapping("/years/range")
  public ResponseEntity<Map<String, Integer>> getFirstAndLastAllowancesYear(
      @ApiIgnore @AuthenticationPrincipal User authUser) {
    var firstAndLastAllowanceYear = allowanceService.getFirstAndLastAllowancesYear(authUser);
    return new ResponseEntity<>(firstAndLastAllowanceYear, HttpStatus.OK);
  }

  @GetMapping("/user/{id}")
  public ResponseEntity<AllowanceForUserDto> getAllowancesForUser(@PathVariable Long id) {
    var user = userService.findById(id);
    AllowanceForUserDto allowanceForUserDto = allowanceDtoFactory.getAllowancesForUser(user);

    return new ResponseEntity<>(allowanceForUserDto, HttpStatus.OK);
  }

  @GetMapping("/years")
  public ResponseEntity<List<Integer>> getAllYearsWithAllowance(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestParam(required = false) Long userId,
      @RequestParam(required = false) boolean includeAllUsers) {
    List<Integer> allYears;
    if (!includeAllUsers) {
      var user = userId == null ? authUser : userService.findById(userId);
      allYears = allowanceService.getAllYearsWithAllowanceForUser(user);
    } else {
      allYears = allowanceService.getAllYearsWithAllowance();
    }

    return new ResponseEntity<>(allYears, HttpStatus.OK);
  }

  @GetMapping("/user/year")
  public ResponseEntity<AllowanceWithoutYearDto> getAllowanceForUserInYear(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestParam(required = false) Long userId,
      @RequestParam(required = false) Integer year) {
    Long requestedUserId = userId == null ? authUser.getId() : userId;
    Allowance allowance = allowanceService.getByUserAndYear(requestedUserId, year);
    AllowanceWithoutYearDto allowanceWithoutYearDto =
        allowanceDtoFactory.createAllowanceWithoutYearDto(allowance);

    return new ResponseEntity<>(allowanceWithoutYearDto, HttpStatus.OK);
  }

  @GetMapping("users/{year}")
  public ResponseEntity<Page<UsersAllowancePerYearDto>> getUsersAllowancePerYear(
      @PathVariable int year,
      @RequestParam(required = false) Long teamId,
      @RequestParam(required = false, defaultValue = "ACTIVE") List<UserStatusType> userStatusType,
      @RequestParam(required = false) String searchQuery,
      @PageableDefault(size = DEFAULT_PAGE_SIZE) Pageable pageable) {
    Page<Allowance> allowances = StringUtils.isEmpty(searchQuery)
            ? allowanceService.getAllowancesFiltered(year, teamId, userStatusType, pageable)
            : allowanceService.getAllowancesFilteredSearch(year, teamId, userStatusType, searchQuery, pageable);

    return new ResponseEntity<>(
        allowances.map(allowance -> new UsersAllowancePerYearDto(allowance, allowanceDtoFactory)),
        HttpStatus.OK);
  }

  @GetMapping("/me")
  public ResponseEntity<AllowanceWithoutYearDto> getAllowancesForAuthUser(
      @ApiIgnore @AuthenticationPrincipal User authUser, @RequestParam int year) {
    Allowance allowance = allowanceService.getByUserAndYear(authUser.getId(), year);
    AllowanceWithoutYearDto allowanceWithoutYearDto = allowanceDtoFactory.createAllowanceWithoutYearDto(allowance);

    return new ResponseEntity<>(allowanceWithoutYearDto, HttpStatus.OK);
  }

  @PutMapping("/{id}")
  public ResponseEntity<AllowanceWithoutYearDto> update(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @PathVariable Long id,
      @RequestBody @Valid AllowanceEditDto allowanceEditDto) {
    allowanceEditDto.setId(id);
    Allowance manualAdjust = MAPPER.map(allowanceEditDto, Allowance.class);
    Allowance allowance = allowanceService.update(manualAdjust, authUser);

    return new ResponseEntity<>(allowanceDtoFactory.createAllowanceWithoutYearDto(allowance), HttpStatus.OK);
  }

}
