package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.YearDto;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.service.YearService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/years")
public class YearController {

  private static final ModelMapper MAPPER = new ModelMapper();
  private static final int DEFAULT_PAGE_SIZE = 30;

  private YearService yearService;

  @Autowired
  public YearController(YearService yearService) {
    this.yearService = yearService;
  }

  @GetMapping
  public ResponseEntity<Page<YearDto>> getYears(
      @PageableDefault(size = DEFAULT_PAGE_SIZE) Pageable pageable) {
    Page<Year> years = yearService.getAll(pageable);

    return new ResponseEntity<>(years.map(YearDto::new), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<YearDto> findYearById(@PathVariable Long id) {
    var yearDto = new YearDto(yearService.findById(id));

    return new ResponseEntity<>(yearDto, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<YearDto> createYear(@RequestBody YearDto yearDto) {
    yearService.save(MAPPER.map(yearDto, Year.class));

    return new ResponseEntity<>(yearDto, HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<YearDto> updateYear(@PathVariable Long id, @RequestBody YearDto yearDto) {
    yearDto.setId(id);
    var year = yearService.update(MAPPER.map(yearDto, Year.class));

    return new ResponseEntity<>(new YearDto(year), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deleteYear(@PathVariable Long id) {
    yearService.deleteById(id);

    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }
}
