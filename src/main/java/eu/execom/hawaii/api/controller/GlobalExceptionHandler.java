package eu.execom.hawaii.api.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.exceptions.AllowanceNotFoundException;
import eu.execom.hawaii.exceptions.DateOutOfScopeException;
import eu.execom.hawaii.exceptions.InsufficientHoursException;
import eu.execom.hawaii.exceptions.NotAuthorizedApprovalException;
import eu.execom.hawaii.exceptions.RequestAlreadyHandledException;
import eu.execom.hawaii.exceptions.SortParameterTooLongException;
import eu.execom.hawaii.exceptions.UnauthorizedAccessException;
import eu.execom.hawaii.exceptions.UnauthorizedEmailDomainException;
import eu.execom.hawaii.exceptions.NegativeAllowanceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

  private static final String ERROR = "error";
  private static final String INVALID_FORMAT_MESSAGE = "Value '%s' for type '%s' is invalid.";

  @ExceptionHandler(NotAuthorizedApprovalException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, NotAuthorizedApprovalException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(UnauthorizedAccessException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, UnauthorizedAccessException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(UnauthorizedEmailDomainException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, UnauthorizedEmailDomainException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, AccessDeniedException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, EntityNotFoundException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(AllowanceNotFoundException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, AllowanceNotFoundException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(NegativeAllowanceException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, NegativeAllowanceException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(EntityExistsException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, EntityExistsException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, DataIntegrityViolationException ex) {
    logException(request, ex);

    return errorResponse(ex.getMostSpecificCause().getMessage(), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(RequestAlreadyHandledException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, RequestAlreadyHandledException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(ActionNotAllowedException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, ActionNotAllowedException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(DateOutOfScopeException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, DateOutOfScopeException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Map<String, List<String>>> handle(HttpServletRequest request,
      MethodArgumentNotValidException ex) {
    logException(request, ex);

    List<String> messages = ex.getBindingResult()
                              .getFieldErrors()
                              .stream()
                              .map(DefaultMessageSourceResolvable::getDefaultMessage)
                              .collect(Collectors.toList());

    return errorResponses(messages);
  }

  @ExceptionHandler(InsufficientHoursException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, InsufficientHoursException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
  }

  @ExceptionHandler(SortParameterTooLongException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, SortParameterTooLongException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, HttpMessageNotReadableException ex) {
    logException(request, ex);
    String message = ex.getCause() instanceof InvalidFormatException ?
        createInvalidFormatExceptionMessage(ex) :
        ex.getMostSpecificCause().getMessage();

    return errorResponse(message, HttpStatus.BAD_REQUEST);
  }

  private String createInvalidFormatExceptionMessage(HttpMessageNotReadableException ex) {
    String fullFieldName = ex.getMessage().split("`")[1];
    String[] fullFieldNameArray = fullFieldName.split("\\.");
    String field = fullFieldNameArray[fullFieldNameArray.length - 1];
    String value = ex.getMessage().split("\"")[1];
    return String.format(INVALID_FORMAT_MESSAGE, value, field);
  }

  @ExceptionHandler(MissingServletRequestParameterException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, MissingServletRequestParameterException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<Map<String, String>> handle(
      HttpServletRequest request, MethodArgumentTypeMismatchException ex) {
    logException(request, ex);

    return errorResponse(ex.getMostSpecificCause().getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request,
      HttpRequestMethodNotSupportedException ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Map<String, String>> handle(HttpServletRequest request, Exception ex) {
    logException(request, ex);

    return errorResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private void logException(HttpServletRequest request, Exception ex) {
    log.error("Requesting resource: '{}' raised exception: '{}'", request.getRequestURI(), ex);
  }

  private ResponseEntity<Map<String, String>> errorResponse(String message, HttpStatus statusCode) {
    return new ResponseEntity<>(Collections.singletonMap(ERROR, message), statusCode);
  }

  private ResponseEntity<Map<String, List<String>>> errorResponses(List<String> messages) {
    return new ResponseEntity<>(Collections.singletonMap(ERROR, messages), HttpStatus.BAD_REQUEST);
  }

}
