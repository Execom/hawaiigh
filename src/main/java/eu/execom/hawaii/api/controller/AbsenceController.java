package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.AbsenceDto;
import eu.execom.hawaii.dto.IconDto;
import eu.execom.hawaii.model.Absence;
import eu.execom.hawaii.service.AbsenceService;
import eu.execom.hawaii.service.IconService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/leavetypes")
public class AbsenceController {

  private static final ModelMapper MAPPER = new ModelMapper();

  private AbsenceService absenceService;
  private IconService iconService;

  @Autowired
  public AbsenceController(AbsenceService absenceService, IconService iconService) {
    this.absenceService = absenceService;
    this.iconService = iconService;
  }

  @GetMapping
  public ResponseEntity<Page<AbsenceDto>> getAbsences(@RequestParam(required = false, defaultValue = "0") Integer page,
      @RequestParam(required = false) Integer size) {
    Pageable pageable = size == null ? Pageable.unpaged() : PageRequest.of(page, size);
    Page<Absence> absences = absenceService.findAll(pageable);

    return new ResponseEntity<>(absences.map(AbsenceDto::new), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<AbsenceDto> getAbsenceById(@PathVariable Long id) {
    var absence = absenceService.getById(id);

    return new ResponseEntity<>(new AbsenceDto(absence), HttpStatus.OK);
  }

  @GetMapping("/icons")
  public ResponseEntity<List<IconDto>> getAllIcons() {
    var icons = iconService.getAllIcons();
    var iconDtos = icons.stream().map(IconDto::new).collect(Collectors.toList());

    return new ResponseEntity<>(iconDtos, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<AbsenceDto> createAbsence(@RequestBody @Valid AbsenceDto absenceDto) {
    var absence = absenceService.save(MAPPER.map(absenceDto, Absence.class));

    return new ResponseEntity<>(new AbsenceDto(absence), HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<AbsenceDto> updateAbsence(
      @PathVariable Long id, @RequestBody @Valid AbsenceDto absenceDto) {
    absenceDto.setId(id);
    var absence = absenceService.update(MAPPER.map(absenceDto, Absence.class));

    return new ResponseEntity<>(new AbsenceDto(absence), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deleteAbsence(@PathVariable Long id) {
    absenceService.delete(id);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }
}
