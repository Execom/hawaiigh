package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.DayViewDto;
import eu.execom.hawaii.dto.LoggedUserDto;
import eu.execom.hawaii.dto.UserDto;
import eu.execom.hawaii.dto.UserWithApproverTeamsDto;
import eu.execom.hawaii.dto.UserWithDaysDto;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.restfactory.UserWithDaysDtoFactory;
import eu.execom.hawaii.service.DayService;
import eu.execom.hawaii.service.TeamApproverService;
import eu.execom.hawaii.service.TeamService;
import eu.execom.hawaii.service.UserRoleService;
import eu.execom.hawaii.service.UserService;
import liquibase.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
@Slf4j
public class UserController {

  private static final ModelMapper MAPPER = new ModelMapper();
  private static final List<UserStatusType> ACTIVE = Collections.singletonList(UserStatusType.ACTIVE);
  private static final int DEFAULT_PAGE_SIZE = 30;

  private UserService userService;
  private DayService dayService;
  private TeamService teamService;
  private UserRoleService userRoleService;
  private TeamApproverService teamApproverService;
  private UserWithDaysDtoFactory userWithDaysDtoFactory;

  @Autowired
  public UserController(UserService userService, DayService dayService, TeamService teamService,
      UserRoleService userRoleService, TeamApproverService teamApproverService,
      UserWithDaysDtoFactory userWithDaysDtoFactory) {
    this.userService = userService;
    this.dayService = dayService;
    this.teamService = teamService;
    this.userRoleService = userRoleService;
    this.teamApproverService = teamApproverService;
    this.userWithDaysDtoFactory = userWithDaysDtoFactory;
  }

  @GetMapping
  public ResponseEntity<Page<UserDto>> getUsers(
      @RequestParam(required = false, defaultValue = "ACTIVE") List<UserStatusType> userStatusType,
      @PageableDefault(size = DEFAULT_PAGE_SIZE) Pageable pageable) {
    Page<User> users = userService.findAllByUserStatusType(userStatusType, pageable);

    return new ResponseEntity<>(users.map(UserDto::new), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<UserWithApproverTeamsDto> getUserById(@PathVariable Long id) {
    var user = userService.findById(id);

    return new ResponseEntity<>(new UserWithApproverTeamsDto(user), HttpStatus.OK);
  }

  @GetMapping("/allUsers")
  public ResponseEntity<List<UserWithDaysDto>> getAllUsersRestrictedList(
      @RequestParam(required = false) LocalDate startDate, @RequestParam(required = false) LocalDate endDate,
      @RequestParam(required = false) Integer year) {
    startDate = assignDefaultStartDate(startDate, endDate, year);
    endDate = assignDefaultEndDate(startDate, endDate, year);

    List<User> users = userService.findAllByUserStatusType(ACTIVE);
    List<UserWithDaysDto> userDtos = createUsersWithDaysDtos(users, startDate, endDate);

    return new ResponseEntity<>(userDtos, HttpStatus.OK);
  }

  //@formatter:off
  @GetMapping("/teamCalendarList")
  public ResponseEntity<Page<UserWithDaysDto>> getAllTeamUsersDays(
      @RequestParam(required = false) Long teamId,
      @RequestParam(required = false) YearMonth month,
      @PageableDefault(size = DEFAULT_PAGE_SIZE, sort = "fullName") Pageable pageable) {
  //@formatter:on
    month = month == null ? YearMonth.now() : month;
    var startDate = month.atDay(1);
    var endDate = month.atEndOfMonth();
    Page<User> users = userService.createPageableUsersList(getUsers(teamId), pageable);
    Page<UserWithDaysDto> pageableUserDtos = users.map(
        user -> userWithDaysDtoFactory.createUserWithDaysDto(user, startDate, endDate));

    return new ResponseEntity<>(pageableUserDtos, HttpStatus.OK);
  }

  private List<User> getUsers(@RequestParam(required = false) Long teamId) {
    return teamId == null ?
        userService.findAllByUserStatusType(ACTIVE) :
        userService.findAllActiveUsersByTeam(teamService.getById(teamId));
  }

  //@formatter:off
  @GetMapping("/teamUsers")
  public ResponseEntity<List<UserWithDaysDto>> getTeamUsersRestrictedList(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestParam(required = false) LocalDate startDate,
      @RequestParam(required = false) LocalDate endDate,
      @RequestParam(required = false) Integer year,
      @RequestParam(required = false) Long teamId) {
   //@formatter:on
    startDate = assignDefaultStartDate(startDate, endDate, year);
    endDate = assignDefaultEndDate(startDate, endDate, year);
    Team team = teamId != null ? getTeam(teamId, authUser) : authUser.getTeam();

    List<User> users = userService.findAllActiveUsersByTeam(team);
    List<UserWithDaysDto> usersDtos = createUsersWithDaysDtos(users, startDate, endDate);

    return new ResponseEntity<>(usersDtos, HttpStatus.OK);
  }

  private Team getTeam(Long teamId, User authUser) {
    var team = teamService.getById(teamId);
    userService.validateUserIsTeamMemberOrApprover(authUser, team);

    return team;
  }

  private List<UserWithDaysDto> createUsersWithDaysDtos(List<User> users, LocalDate startDate, LocalDate endDate) {
    return users.stream()
                .map(user -> userWithDaysDtoFactory.createUserWithDaysDto(user, startDate, endDate))
                .collect(Collectors.toList());
  }

  @GetMapping("/monthDays")
  public ResponseEntity<UserWithDaysDto> getUserDaysInGivenMonth(@ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestParam(required = false) Long userId, @RequestParam(required = false) YearMonth yearMonth) {

    yearMonth = yearMonth == null ? YearMonth.now() : yearMonth;
    var startDate = yearMonth.atDay(1);
    var endDate = yearMonth.atEndOfMonth();

    User user = userId == null ? authUser : userService.findById(userId);
    UserWithDaysDto userWithDaysDto = userWithDaysDtoFactory.createUserWithDaysDto(user, startDate, endDate);

    return new ResponseEntity<>(userWithDaysDto, HttpStatus.OK);
  }

  //@formatter:off
  @GetMapping("/days")
  public ResponseEntity<List<DayViewDto>> getUserDays(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestParam(required = false) Long userId,
      @RequestParam(required = false) LocalDate startDate,
      @RequestParam(required = false) LocalDate endDate,
      @RequestParam(required = false) Integer year) {
  //@formatter:on
    startDate = assignDefaultStartDate(startDate, endDate, year);
    endDate = assignDefaultEndDate(startDate, endDate, year);

    var user = userService.findUserById(userId, authUser);
    List<Day> days = dayService.getUserAbsencesDays(user, startDate, endDate);
    List<DayViewDto> daysDto = days.stream().map(DayViewDto::new).collect(Collectors.toList());
    userWithDaysDtoFactory.formatSicknessName(daysDto);

    return new ResponseEntity<>(daysDto, HttpStatus.OK);
  }

  //@formatter:off
  @GetMapping("/search")
  public ResponseEntity<Page<UserDto>> searchUsersByNameAndEmail(
      @ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestParam(required = false, defaultValue = "ACTIVE") List<UserStatusType> userStatusType,
      @RequestParam(required = false) String searchQuery,
      @RequestParam(required = false) boolean includeLoggedUser,
      @RequestParam(required = false) String sort,
      @RequestParam(required = false) Sort.Direction sortDirection,
      @PageableDefault(size = DEFAULT_PAGE_SIZE) Pageable pageable) {
  //@formatter:on
    Page<User> users;
    if (StringUtils.isEmpty(sort)) {
      users = searchQuery == null
              ? userService.findAllByUserStatusType(userStatusType, pageable)
              : userService.searchUsers(authUser, userStatusType, searchQuery, includeLoggedUser, pageable);
    } else {
      users = userService.findAllByUserStatusTypeSorted(userStatusType, pageable, sort, sortDirection);
    }

    return new ResponseEntity<>(users.map(UserDto::new), HttpStatus.OK);
  }

  @GetMapping("/email/{email}")
  public ResponseEntity<UserDto> getUserByEmail(@PathVariable String email) {
    User user = userService.findByEmail(email);

    return new ResponseEntity<>(new UserDto(user), HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<UserWithApproverTeamsDto> createUser(@Valid @ApiIgnore @AuthenticationPrincipal User authUser,
      @RequestBody @Valid UserWithApproverTeamsDto userDto) {
    User user = MAPPER.map(userDto, User.class);
    User persistedUser = userService.create(user, authUser);
    teamApproverService.addUserToTeamApprovers(persistedUser);

    return new ResponseEntity<>(new UserWithApproverTeamsDto(persistedUser), HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<UserWithApproverTeamsDto> updateUser(
      @Valid @ApiIgnore @AuthenticationPrincipal User authUser,
      @PathVariable Long id,
      @RequestBody @Valid UserWithApproverTeamsDto userDto) {
    userDto.setId(id);
    var user = MAPPER.map(userDto, User.class);
    teamApproverService.handleApproverTeamsChange(user);
    user = userService.update(user, authUser);

    return new ResponseEntity<>(new UserWithApproverTeamsDto(user), HttpStatus.OK);
  }

  @PutMapping("/{id}/activate")
  public ResponseEntity activateUser(
      @Valid @ApiIgnore @AuthenticationPrincipal User authUser,
      @PathVariable Long id,
      @RequestBody UserWithApproverTeamsDto userDto) {
    userDto.setId(id);
    var user = MAPPER.map(userDto, User.class);
    userService.activate(user, authUser);

    return new ResponseEntity(HttpStatus.OK);
  }

  @PutMapping("/canceldelete/{id}")
  public ResponseEntity cancelDelete(@Valid @ApiIgnore @AuthenticationPrincipal User authUser, @PathVariable Long id) {
    userService.cancelDelete(id, authUser);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity archiveUser(@ApiIgnore @AuthenticationPrincipal User authUser, @PathVariable Long id,
     @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate stoppedWorkingDate) {
    userService.delete(id, authUser, stoppedWorkingDate);

    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }

  @GetMapping("/me")
  public ResponseEntity<UserDto> me(@ApiIgnore @AuthenticationPrincipal User authUser) {
    return new ResponseEntity<>(new LoggedUserDto(authUser), HttpStatus.OK);
  }

  private LocalDate assignDefaultStartDate(LocalDate startDate, LocalDate endDate, Integer year) {
    year = year == null ? LocalDate.now().getYear() : year;
    if (startDate == null && endDate == null) {
      startDate = LocalDate.of(year, 1, 1);
    } else if (startDate == null) {
      startDate = endDate.minusYears(1);
    }

    return startDate;
  }

  private LocalDate assignDefaultEndDate(LocalDate startDate, LocalDate endDate, Integer year) {
    year = year == null ? LocalDate.now().getYear() : year;
    if (startDate == null && endDate == null) {
      endDate = LocalDate.of(year, 12, 31);
    } else if (endDate == null) {
      endDate = startDate.plusYears(1);
    }

    return endDate;
  }

}
