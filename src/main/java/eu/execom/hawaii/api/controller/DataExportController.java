package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.export.SortDto;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.service.DataExportService;
import eu.execom.hawaii.util.export.ExportFile;
import eu.execom.hawaii.util.export.ResponseEntityExportAssembler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/export")
public class DataExportController {

  private DataExportService dataExportService;
  private ResponseEntityExportAssembler entityExportAssembler;

  @Autowired
  public DataExportController(DataExportService dataExportService,
      ResponseEntityExportAssembler entityExportAssembler) {
    this.dataExportService = dataExportService;
    this.entityExportAssembler = entityExportAssembler;
  }

  @GetMapping("/requests")
  public ResponseEntity exportRequests(
      @RequestParam(required = false) AbsenceType absenceType,
      @RequestParam(required = false) Long teamId,
      @RequestParam(defaultValue = "ACTIVE") List<UserStatusType> userStatusTypes,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate,
      @RequestParam(required = false, defaultValue = "csv") String exportFormat,
      SortDto sortDto) {
    ExportFile exportFile = dataExportService.exportRequests(exportFormat, teamId, userStatusTypes,
        absenceType, fromDate, toDate, sortDto);

    return entityExportAssembler.getResponseEntity(exportFile);
  }

  @GetMapping("/users")
  public ResponseEntity exportUsers(
      @RequestParam(required = false) Long teamId,
      @RequestParam(defaultValue = "ACTIVE") List<UserStatusType> userStatusTypes,
      @RequestParam(required = false, defaultValue = "csv") String exportFormat,
      SortDto sortDto) {
    ExportFile exportFile = dataExportService.exportUsers(exportFormat, teamId, userStatusTypes, sortDto);

    return entityExportAssembler.getResponseEntity(exportFile);
  }

  @GetMapping("/allowances")
  public ResponseEntity exportAllowances(
      @RequestParam int year,
      @RequestParam(required = false) Long teamId,
      @RequestParam(defaultValue = "ACTIVE") List<UserStatusType> userStatusTypes,
      @RequestParam(required = false, defaultValue = "csv") String exportFormat,
      SortDto sortDto) {
    ExportFile exportFile = dataExportService.exportAllowances(exportFormat, teamId, userStatusTypes, year, sortDto);

    return entityExportAssembler.getResponseEntity(exportFile);
  }

  @GetMapping("/leaveprofiles")
  public ResponseEntity exportLeaveProfiles(
      @RequestParam(required = false, defaultValue = "csv") String exportFormat) {
    ExportFile exportFile = dataExportService.exportLeaveProfiles(exportFormat);

    return entityExportAssembler.getResponseEntity(exportFile);
  }
}
