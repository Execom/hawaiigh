package eu.execom.hawaii.api.controller;

import eu.execom.hawaii.dto.AuditInformationDto;
import eu.execom.hawaii.dto.audit.AllowanceAuditDto;
import eu.execom.hawaii.dto.audit.RequestAuditDto;
import eu.execom.hawaii.dto.audit.TeamAuditDto;
import eu.execom.hawaii.dto.audit.UserAuditDto;
import eu.execom.hawaii.model.AuditInformation;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.audit.AuditedChanges;
import eu.execom.hawaii.model.audit.RequestAudit;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import eu.execom.hawaii.service.AuditInformationService;
import eu.execom.hawaii.service.RequestService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/audit")
public class AuditInformationController {

  private AuditInformationService auditInformationService;
  private RequestService requestService;

  @Autowired
  public AuditInformationController(AuditInformationService auditInformationService, RequestService requestService) {
    this.auditInformationService = auditInformationService;
    this.requestService = requestService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<AuditInformationDto> getAuditInformation(@PathVariable Long id) {
    var auditInformation = auditInformationService.getById(id);
    var auditInformationDto = new AuditInformationDto(auditInformation);

    return new ResponseEntity<>(auditInformationDto, HttpStatus.OK);
  }

  @GetMapping
  public ResponseEntity<List<AuditInformationDto>> getAllAuditInformation() {
    var allAuditInformation = auditInformationService.getAll();
    var allAuditInformationDto = allAuditInformation.stream()
                                                    .map(AuditInformationDto::new)
                                                    .collect(Collectors.toList());

    return new ResponseEntity<>(allAuditInformationDto, HttpStatus.OK);
  }

  @GetMapping("/approvalFlow/{id}")
  public ResponseEntity<List<RequestAuditDto>> getApprovalFlow(@PathVariable Long id,
      @ApiIgnore @AuthenticationPrincipal User authUser) {
    requestService.validateUserIsAuthorizedToAccessRequest(id, authUser);
    List<RequestAudit> requestAudits = auditInformationService.getApprovalFlowForRequestAudit(id);
    List<RequestAuditDto> requestAuditDtos = requestAudits.stream()
                                                          .map(RequestAuditDto::new)
                                                          .collect(Collectors.toList());

    return new ResponseEntity<>(requestAuditDtos, HttpStatus.OK);
  }

  @GetMapping("/allowance/{id}")
  public ResponseEntity<List<AllowanceAuditDto>> getAllowanceAudit(@PathVariable Long id) {
    Map<AuditInformation, List<AuditedChanges>> allowanceAuditMap = auditInformationService.getAudit(id,
        AuditedEntity.ALLOWANCE);
    List<AllowanceAuditDto> allowanceAuditDtos = getAuditDto(allowanceAuditMap, AllowanceAuditDto.class);

    return new ResponseEntity<>(allowanceAuditDtos, HttpStatus.OK);
  }

  @GetMapping("/team/{id}")
  public ResponseEntity<List<TeamAuditDto>> getTeamAudit(@PathVariable Long id) {
    Map<AuditInformation, List<AuditedChanges>> teamAuditMap = auditInformationService.getAudit(id, AuditedEntity.TEAM);
    List<TeamAuditDto> teamAuditDtos = getAuditDto(teamAuditMap, TeamAuditDto.class);

    return new ResponseEntity<>(teamAuditDtos, HttpStatus.OK);
  }

  @GetMapping("/user/{id}")
  public ResponseEntity<List<UserAuditDto>> getUserUpdateInfo(@PathVariable Long id) {
    Map<AuditInformation, List<AuditedChanges>> userAuditMap = auditInformationService.getAudit(id, AuditedEntity.USER);
    List<UserAuditDto> userAuditDtos = getAuditDto(userAuditMap, UserAuditDto.class);

    return new ResponseEntity<>(userAuditDtos, HttpStatus.OK);
  }

  private <T> List<T> getAuditDto(Map<AuditInformation, List<AuditedChanges>> auditMap, Class<T> clazz) {
    return auditMap.entrySet().stream().map(audit -> getAuditDto(audit, clazz)).collect(Collectors.toList());
  }

  @SneakyThrows
  private <T> T getAuditDto(Map.Entry<AuditInformation, List<AuditedChanges>> audit, Class<T> clazz) {
    return clazz.getDeclaredConstructor(AuditInformation.class, List.class)
                  .newInstance(audit.getKey(), audit.getValue());
  }

}
