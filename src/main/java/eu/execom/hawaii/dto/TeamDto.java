package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.util.EmailExecom;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class TeamDto {

  private Long id;

  @NotNull
  @Size(max = 50, message = "Name is too long.")
  private String name;

  @EmailExecom
  @Size(max = 512, message = "There are too many emails in sickness request emails column.")
  private String sicknessRequestEmails;

  @EmailExecom
  @Size(max = 512, message = "There are too many emails in annual request emails column.")
  private String annualRequestEmails;

  @EmailExecom
  @Size(max = 512, message = "There are too many emails in bonus request emails column.")
  private String bonusRequestEmails;

  @NotNull
  private boolean sendEmailToTeammatesForSicknessRequestEnabled;

  @NotNull
  private boolean sendEmailToTeammatesForAnnualRequestEnabled;

  @NotNull
  private boolean sendEmailToTeammatesForBonusRequestEnabled;

  private List<UserDto> users = new ArrayList<>();

  private List<UserDto> teamApprovers = new ArrayList<>();

  public TeamDto(Team team) {
    this.id = team.getId();
    this.name = team.getName();
    this.sicknessRequestEmails = team.getSicknessRequestEmails();
    this.annualRequestEmails = team.getAnnualRequestEmails();
    this.bonusRequestEmails = team.getBonusRequestEmails();
    this.sendEmailToTeammatesForSicknessRequestEnabled = team.isSendEmailToTeammatesForSicknessRequestEnabled();
    this.sendEmailToTeammatesForAnnualRequestEnabled = team.isSendEmailToTeammatesForAnnualRequestEnabled();
    this.sendEmailToTeammatesForBonusRequestEnabled = team.isSendEmailToTeammatesForBonusRequestEnabled();
    this.users = team.getUsers().stream().map(UserDto::new).collect(Collectors.toList());
    this.teamApprovers = team.getTeamApprovers().stream().map(UserDto::new).collect(Collectors.toList());
  }
}
