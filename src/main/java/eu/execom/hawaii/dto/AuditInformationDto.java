package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.AuditInformation;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import eu.execom.hawaii.model.enumerations.OperationPerformed;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class AuditInformationDto {

  private Long auditInformationId;
  private AuditedEntity auditedEntity;
  private OperationPerformed operationPerformed;
  private UserDto modifier;
  private Long modifiedEntityId;
  private String previousValue;
  private String currentValue;

  private LocalDateTime modifiedDateTime;

  public AuditInformationDto(AuditInformation auditInformation) {
    this.auditInformationId = auditInformation.getId();
    this.auditedEntity = auditInformation.getAuditedEntity();
    this.operationPerformed = auditInformation.getOperationPerformed();
    this.modifier = new UserDto(auditInformation.getModifiedByUser());
    this.modifiedEntityId = auditInformation.getModifiedEntityId();
    this.previousValue = auditInformation.getPreviousValue();
    this.currentValue = auditInformation.getCurrentValue();
    this.modifiedDateTime = auditInformation.getModifiedDateTime();
  }

}
