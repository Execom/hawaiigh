package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class UserAdminDto {

  private Long id;

  private String fullName;

  private String email;

  @NotNull
  private UserAdminPermission userAdminPermission;

  private String imageUrl;

  private List<UserPushTokenDto> userPushTokens = new ArrayList<>();

  public UserAdminDto(User user) {
    this.id = user.getId();
    this.fullName = user.getFullName();
    this.email = user.getEmail();
    this.userAdminPermission = user.getUserAdminPermission();
    this.imageUrl = user.getImageUrl();
    this.userPushTokens = user.getUserPushTokens().stream().map(UserPushTokenDto::new).collect(Collectors.toList());
  }
}
