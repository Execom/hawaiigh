package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.util.ValidAllowance;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Range;

@Data
@NoArgsConstructor
public class AllowanceEditDto {

  @NotNull
  private Long id;

  @NotNull
  @Size(max = 500, message = "Comment is too long.")
  private String comment;

  @ValidAllowance(message = "'Manual adjust' imputed value must be divisible by 4.")
  @Range(min = -240, max = 240, message = "Manual adjustment can be done for a maximum of 30 days. ")
  private int manualAdjust;

  @ValidAllowance(message = "'Bonus manual adjust' imputed value must be divisible by 4.")
  @Range(min = 0, max = 40, message = "Employee can be granted for a maximum of 5 bonus days. ")
  private int bonusManualAdjust;

  @ValidAllowance(message = "'Carried over' imputed value must be divisible by 4.")
  @Range(min = 0, max = 40, message = "Employee can carry over a maximum of 5 days. ")
  private int carriedOver;

  @ValidAllowance(message = "'Training manual adjust' imputed value must be divisible by 4.")
  @Range(min = -16, max = 16, message = "Manual adjustment of training leave can be done for a maximum of 2 days. ")
  private int trainingManualAdjust;

  public AllowanceEditDto(Allowance allowance) {
    this.id = allowance.getId();
    this.manualAdjust = allowance.getManualAdjust();
    this.bonusManualAdjust = allowance.getBonusManualAdjust();
    this.carriedOver = allowance.getCarriedOver();
    this.trainingManualAdjust = allowance.getTrainingManualAdjust();
    this.comment = allowance.getComment();
  }

}
