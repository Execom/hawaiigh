package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.restfactory.AllowanceDtoFactory;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

@Data
@NoArgsConstructor
public class UsersAllowancePerYearDto {

    private static final ModelMapper MAPPER = new ModelMapper();

    private UserDto user;
    private AllowanceWithoutYearDto allowance;

    public UsersAllowancePerYearDto (Allowance allowance, AllowanceDtoFactory allowanceDtoFactory){
        this.user = MAPPER.map(allowance.getUser(), UserDto.class);
        this.allowance = allowanceDtoFactory.createAllowanceWithoutYearDto(allowance);
    }

}
