package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class RequestWithCommentsDto {
  private Long id;

  @NotNull
  private UserDto user;

  @NotNull
  private AbsenceDto absence;

  private RequestStatus requestStatus;

  private LocalDateTime submissionTime;

  private CommentsDto comments;

  @NotNull
  private List<DayDto> days;

  private List<UserDto> currentlyApprovedBy;

  public RequestWithCommentsDto(Request request) {
    this.id = request.getId();
    this.user = new UserDto(request.getUser());
    this.absence = new AbsenceDto(request.getAbsence());
    this.requestStatus = request.getRequestStatus();
    this.submissionTime = request.getSubmissionTime();
    this.comments = new CommentsDto(request);
    this.days = request.getDays().stream().map(DayDto::new).collect(Collectors.toList());
    this.currentlyApprovedBy = request.getCurrentlyApprovedBy().stream().map(UserDto::new).collect(Collectors.toList());
  }
}