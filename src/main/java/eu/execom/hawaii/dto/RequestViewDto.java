package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class RequestViewDto {

  private Long id;

  private UserDto user;

  private String absenceName;

  private AbsenceType absenceType;

  private String iconUrl;

  private RequestStatus requestStatus;

  private List<DayDto> days;

  private List<UserDto> currentlyApprovedBy;

  public RequestViewDto(Request request) {
    this.id = request.getId();
    this.user = new UserDto(request.getUser());
    this.absenceName = request.getAbsence().getName();
    this.absenceType = request.getAbsence().getAbsenceType();
    this.iconUrl = request.getAbsence().getIcon().getIconUrl();
    this.requestStatus = request.getRequestStatus();
    this.days = request.getDays().stream().map(DayDto::new).collect(Collectors.toList());
    this.currentlyApprovedBy = request.getCurrentlyApprovedBy().stream().map(UserDto::new).collect(Collectors.toList());

  }

}
