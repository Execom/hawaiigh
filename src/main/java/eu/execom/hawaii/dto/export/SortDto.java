package eu.execom.hawaii.dto.export;

import lombok.Data;
import org.springframework.data.domain.Sort;

import java.util.List;

@Data
public class SortDto {
  private List<String> properties;
  private List<Sort.Direction> directions;
}