package eu.execom.hawaii.dto.export;

import eu.execom.hawaii.model.Request;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class RequestExportDto {

  private String teamName;

  private String userFirstName;

  private String userLastName;

  private String userEmail;

  private String absenceType;

  private String requestStatus;

  private LocalDate startDate;

  private LocalDate endDate;

  private String reason;

  private int hours;

  private int hoursInRange;

  private float days;

  private float daysInRange;

  public RequestExportDto(Request request) {
    this.teamName = request.getUser().getTeam().getName();
    this.userEmail = request.getUser().getEmail();
    this.absenceType = request.getAbsence().getAbsenceType().getDescription();
    this.requestStatus = request.getRequestStatus().getDescription();
    this.reason = request.getReason();
  }

}
