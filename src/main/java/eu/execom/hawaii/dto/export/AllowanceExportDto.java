package eu.execom.hawaii.dto.export;

import eu.execom.hawaii.model.Allowance;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AllowanceExportDto {

  private String userFullName;
  private String userTeamName;
  private String totalLeave;
  private String carriedOver;
  private String takenLeave;
  private String pendingLeave;
  private String remainingLeave;
  private String totalTraining;
  private String takenTraining;
  private String pendingTraining;
  private String remainingTraining;
  private String takenSickness;

  public AllowanceExportDto(Allowance allowance) {
    this.userFullName = allowance.getUser().getFullName();
    this.userTeamName = allowance.getUser().getTeam().getName();
    this.totalLeave = getTotalLeaveInDays(allowance);
    this.carriedOver = convertHoursToDays(allowance.getCarriedOver());
    this.takenLeave = convertHoursToDays(allowance.getTakenAnnual());
    this.pendingLeave = convertHoursToDays(allowance.getPendingAnnual());
    this.remainingLeave = getReaminingLeaveInDays(allowance);
    this.totalTraining = convertHoursToDays(allowance.getTraining() + allowance.getTrainingManualAdjust());
    this.takenTraining = convertHoursToDays(allowance.getTakenTraining());
    this.pendingTraining = convertHoursToDays(allowance.getPendingTraining());
    this.remainingTraining = getReaminingTrainingDays(allowance);
    this.takenSickness = convertHoursToDays(allowance.getSickness());
  }

  private String convertHoursToDays(int hours) {
    return String.valueOf(hours / 8.0);
  }

  private String getTotalLeaveInDays(Allowance allowance) {
    double leave = allowance.getAnnual()
            + allowance.getApprovedBonus()
            + allowance.getCarriedOver()
            + allowance.getManualAdjust()
            + allowance.getBonusManualAdjust();

    return String.valueOf(leave / 8.0);
  }

  private String getReaminingLeaveInDays(Allowance allowance) {
    double totalLeave = Double.parseDouble(getTotalLeaveInDays(allowance));
    double totalTakenLeave = Double.parseDouble(
            convertHoursToDays(allowance.getTakenAnnual() + allowance.getPendingAnnual()));
    return String.valueOf(totalLeave - totalTakenLeave);
  }

  private String getReaminingTrainingDays(Allowance allowance) {
    return convertHoursToDays(
        allowance.getTraining()
            + allowance.getTrainingManualAdjust()
            - allowance.getTakenTraining()
            - allowance.getPendingTraining());
  }
}
