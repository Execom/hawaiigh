package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.enumerations.LeaveProfileType;
import eu.execom.hawaii.util.ValidAllowance;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class LeaveProfileDto {

  private Long id;

  @NotNull
  @Size(max = 50, message = "Name is too long.")
  private String name;

  @NotNull
  @ValidAllowance(message = "'Entitlement' imputed value must be divisible by 4.")
  @Max(value = 2080, message = "Max entitlement' cannot be more than there are days in a year")
  private int entitlement;

  @NotNull
  @ValidAllowance(message = "'Max carried over' imputed value must be divisible by 4.")
  @Max(value = 2080, message = "Max carried over' cannot be more than there are days in a year")
  private int maxCarriedOver;

  @NotNull
  @ValidAllowance(message = "'Max bonus days' imputed value must be divisible by 4.")
  @Max(value = 2080, message = "Max bonus days' cannot be more than there are days in a year")
  private int maxBonusDays;

  @NotNull
  @ValidAllowance(message = "'Max relevant experience bonus' imputed value must be divisible by 4.")
  @Max(value = 2080, message = "Max relevant experience bonus' cannot be more than there are days in a year")
  private int maxRelevantExperienceBonus;

  @NotNull
  @ValidAllowance(message = "'Training' imputed value must be divisible by 4.")
  @Max(value = 2080, message = "Max training' cannot be more than there are days in a year")
  private int training;

  @NotNull
  @ValidAllowance(message = "'Max allowance from next year' imputed value must be divisible by 4.")
  @Max(value = 2080, message = "Max allowance from next year' cannot be more than there are days in a year")
  private int maxAllowanceFromNextYear;

  @NotNull
  private boolean upgradeable;

  @NotNull
  private LeaveProfileType leaveProfileType;

  @Size(max = 255, message = "Comment is too long.")
  private String comment;

  private List<UserDto> users;

  public LeaveProfileDto(LeaveProfile leaveProfile) {
    this.id = leaveProfile.getId();
    this.name = leaveProfile.getName();
    this.entitlement = leaveProfile.getEntitlement();
    this.maxCarriedOver = leaveProfile.getMaxCarriedOver();
    this.maxBonusDays = leaveProfile.getMaxBonusDays();
    this.maxRelevantExperienceBonus = leaveProfile.getMaxRelevantExperienceBonus();
    this.training = leaveProfile.getTraining();
    this.maxAllowanceFromNextYear = leaveProfile.getMaxAllowanceFromNextYear();
    this.upgradeable = leaveProfile.isUpgradeable();
    this.leaveProfileType = leaveProfile.getLeaveProfileType();
    this.comment = leaveProfile.getComment();
    this.users = leaveProfile.getUsers().stream().map(UserDto::new).collect(Collectors.toList());
  }

}
