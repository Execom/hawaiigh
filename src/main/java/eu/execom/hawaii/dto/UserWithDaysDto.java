package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class UserWithDaysDto {

  private Long id;
  private Long teamId;
  private String teamName;
  private String fullName;
  private String email;
  private String jobTitle;
  private List<DayViewDto> days;
  private Long leaveDays;
  private Long nonDeductedDays;
  private Long sickDays;

  public UserWithDaysDto(User user, List<Day> days) {
    this.id = user.getId();
    this.teamId = user.getTeam().getId();
    this.teamName = user.getTeam().getName();
    this.fullName = user.getFullName();
    this.email = user.getEmail();
    this.jobTitle = user.getJobTitle();
    this.days = days.stream().map(DayViewDto::new).collect(Collectors.toList());

  }

}
