package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Allowance;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AllowanceWithoutYearDto {

  private Long id;
  private Long userId;
  private int totalAnnual;
  private int pendingAnnual;
  private int takenAnnual;
  private int remainingAnnual;
  private int annual;
  private int manualAdjust;
  private int sickness;
  private int totalTraining;
  private int pendingTraining;
  private int takenTraining;
  private int remainingTraining;
  private int training;
  private int totalBonus;
  private int pendingBonus;
  private int approvedBonus;
  private int remainingBonus;
  private int bonus;
  private int bonusManualAdjust;
  private int pendingInPreviousYear;
  private int takenInPreviousYear;
  private int nextYearAllowance;
  private int trainingManualAdjust;
  private int carriedOver;
  private int expiredCarriedOver;

  public AllowanceWithoutYearDto(Allowance allowance) {
    this.id = allowance.getId();
    this.userId = allowance.getUser().getId();
    this.annual = allowance.getAnnual();
    this.pendingAnnual = allowance.getPendingAnnual();
    this.takenAnnual = allowance.getTakenAnnual();
    this.sickness = allowance.getSickness();
    this.training = allowance.getTraining();
    this.pendingTraining = allowance.getPendingTraining();
    this.takenTraining = allowance.getTakenTraining();
    this.bonus = allowance.getBonus();
    this.pendingBonus = allowance.getPendingBonus();
    this.approvedBonus = allowance.getApprovedBonus();
    this.bonusManualAdjust = allowance.getBonusManualAdjust();
    this.pendingInPreviousYear = allowance.getPendingInPreviousYear();
    this.takenInPreviousYear = allowance.getTakenInPreviousYear();
    this.manualAdjust = allowance.getManualAdjust();
    this.trainingManualAdjust = allowance.getTrainingManualAdjust();
    this.carriedOver = allowance.getCarriedOver();
    this.expiredCarriedOver = allowance.getExpiredCarriedOver();
  }
}
