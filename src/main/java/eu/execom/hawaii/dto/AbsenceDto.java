package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Absence;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class AbsenceDto {

  private Long id;

  @NotNull
  private AbsenceType absenceType;

  @NotNull
  @Size(max = 100, message = "Name is too long.")
  private String name;

  @Size(max = 255, message = "Comment is too long.")
  private String comment;

  @NotNull
  private boolean deducted;

  @NotNull
  private Long iconId;

  private String iconUrl;

  public AbsenceDto(Absence absence) {
    this.id = absence.getId();
    this.name = absence.getName();
    this.comment = absence.getComment();
    this.deducted = absence.isDeducted();
    this.iconId = absence.getIcon().getId();
    this.iconUrl = absence.getIcon().getIconUrl();
    this.absenceType = absence.getAbsenceType();
  }

}
