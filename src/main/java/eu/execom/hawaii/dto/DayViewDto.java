package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.Duration;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class DayViewDto {

  private Long id;

  @NotNull
  private Long requestId;

  @NotNull
  private LocalDate date;

  @NotNull
  private Duration duration;

  private RequestStatus requestStatus;

  private String iconUrl;

  private AbsenceType absenceType;

  private String absenceName;

  private boolean isDeducted;

  public DayViewDto(Day day) {
    this.id = day.getId();
    this.requestId = day.getRequest().getId();
    this.date = day.getDate();
    this.duration = day.getDuration();
    this.requestStatus = day.getRequest().getRequestStatus();
    this.absenceType = day.getRequest().getAbsence().getAbsenceType();
    this.absenceName = day.getRequest().getAbsence().getName();
    this.isDeducted = day.getRequest().getAbsence().isDeducted();
    this.iconUrl = day.getRequest().getAbsence().getIcon().getIconUrl();
  }
}
