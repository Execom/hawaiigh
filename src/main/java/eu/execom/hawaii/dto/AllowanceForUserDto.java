package eu.execom.hawaii.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AllowanceForUserDto {

  private int remainingAnnualHours;
  private int nextYearRemainingAnnualHours;
  private int remainingTrainingHours;

  private AllowanceForUserDto(int remainingAnnualHours, int nextYearRemainingAnnualHours, int remainingTrainingHours) {
    this.remainingAnnualHours = remainingAnnualHours;
    this.nextYearRemainingAnnualHours = nextYearRemainingAnnualHours;
    this.remainingTrainingHours = remainingTrainingHours;
  }

  public static AllowanceForUserDto createNew(int remainingAnnualHours, int nextYearRemainingAnnualHours,
      int remainingTrainingHours) {
    return new AllowanceForUserDto(remainingAnnualHours, nextYearRemainingAnnualHours, remainingTrainingHours);
  }
}
