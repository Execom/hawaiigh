package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.metric.HardwareMetrics;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class HardwareMetricsDto {

  private Long id;
  private double systemCpuUsage;
  private double jvmCpuUsage;
  private double memoryUsage;
  private LocalDateTime timestamp;

  public HardwareMetricsDto(HardwareMetrics hardwareMetrics) {
    this.id = hardwareMetrics.getId();
    this.systemCpuUsage = hardwareMetrics.getSystemCpuUsage();
    this.jvmCpuUsage = hardwareMetrics.getJvmCpuUsage();
    this.memoryUsage = hardwareMetrics.getMemoryUsage();
    this.timestamp = hardwareMetrics.getTimestamp();
  }
}
