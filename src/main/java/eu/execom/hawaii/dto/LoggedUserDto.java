package eu.execom.hawaii.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class LoggedUserDto extends UserDto {

  @JsonProperty("isApprover")
  private boolean isApprover;
  private UserAdminPermission userAdminPermission;

  public LoggedUserDto(User user) {
    super(user);
    this.userAdminPermission = user.getUserAdminPermission();
    this.isApprover = user.isApprover();
  }
}
