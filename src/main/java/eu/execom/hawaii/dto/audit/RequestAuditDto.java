package eu.execom.hawaii.dto.audit;

import eu.execom.hawaii.model.audit.RequestAudit;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RequestAuditDto {

  private Long id;
  private RequestStatus requestStatus;
  private LocalDateTime modifiedDateTime;
  private String requestHandlerEmail;
  private String requestHandlerFullName;
  private String comment;
  private String reason;
  private String approverComment;
  private String cancellationReason;
  private String approverCancellationComment;

  public RequestAuditDto(RequestAudit requestAudit) {
    this.id = requestAudit.getId();
    this.requestStatus = requestAudit.getRequestStatus();
    this.modifiedDateTime = requestAudit.getModifiedDateTime();
    this.requestHandlerEmail = requestAudit.getRequestHandlerEmail();
    this.requestHandlerFullName = requestAudit.getRequestHandlerFullName();
    this.comment = requestAudit.getComment();
    this.reason = requestAudit.getReason();
    this.approverComment = requestAudit.getApproverComment();
    this.cancellationReason = requestAudit.getCancellationReason();
    this.approverCancellationComment = requestAudit.getApproverCancellationComment();
  }
}
