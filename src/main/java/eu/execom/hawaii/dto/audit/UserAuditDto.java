package eu.execom.hawaii.dto.audit;

import eu.execom.hawaii.model.AuditInformation;
import eu.execom.hawaii.model.audit.AuditedChanges;
import eu.execom.hawaii.model.enumerations.OperationPerformed;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class UserAuditDto {

  private Long id;
  private OperationPerformed operationPerformed;
  private String modifierFullName;
  private String modifierEmail;
  private LocalDateTime modifiedDateTime;
  private List<AuditedChanges> changesPerformed;

  public UserAuditDto(AuditInformation auditInformation, List<AuditedChanges> auditedChanges) {
    this.id = auditInformation.getId();
    this.operationPerformed = auditInformation.getOperationPerformed();
    this.modifierFullName = auditInformation.getModifiedByUser().getFullName();
    this.modifierEmail = auditInformation.getModifiedByUser().getEmail();
    this.modifiedDateTime = auditInformation.getModifiedDateTime();
    this.changesPerformed = auditedChanges;
  }

}
