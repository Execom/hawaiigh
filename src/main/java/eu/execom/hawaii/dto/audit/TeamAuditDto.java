package eu.execom.hawaii.dto.audit;

import eu.execom.hawaii.model.AuditInformation;
import eu.execom.hawaii.model.audit.AuditedChanges;
import eu.execom.hawaii.model.enumerations.OperationPerformed;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class TeamAuditDto {

  private Long id;
  private OperationPerformed operationPerformed;
  private LocalDateTime modifiedDateTime;
  private String modifierEmail;
  private String modifierFullName;
  private List<AuditedChanges> changesPerformed;

  public TeamAuditDto(AuditInformation auditInformation, List<AuditedChanges> auditedChanges) {
    this.id = auditInformation.getId();
    this.operationPerformed = auditInformation.getOperationPerformed();
    this.modifiedDateTime = auditInformation.getModifiedDateTime();
    this.modifierEmail = auditInformation.getModifiedByUser().getEmail();
    this.modifierFullName = auditInformation.getModifiedByUser().getFullName();
    this.changesPerformed = auditedChanges;
  }
}