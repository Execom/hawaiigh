package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class RequestDto {

  private Long id;

  @NotNull
  private UserDto user;

  @NotNull
  private AbsenceDto absence;

  private RequestStatus requestStatus;

  @NotNull
  @Size(max = 255, message = "Reason is too long.")
  private String reason;

  @Size(max = 255, message = "Comment is too long.")
  private String approverComment;

  @Size(max = 255, message = "Cancellation reason is too long.")
  private String cancellationReason;

  @Size(max = 255, message = "Cancellation comment is too long.")
  private String approverCancellationComment;

  private LocalDateTime submissionTime;

  @NotNull
  private List<DayDto> days;

  private List<UserDto> currentlyApprovedBy;

  public RequestDto(Request request) {
    this.id = request.getId();
    this.user = new UserDto(request.getUser());
    this.absence = new AbsenceDto(request.getAbsence());
    this.requestStatus = request.getRequestStatus();
    this.reason = request.getReason();
    this.approverComment = request.getApproverComment();
    this.cancellationReason = request.getCancellationReason();
    this.approverCancellationComment = request.getApproverCancellationComment();
    this.submissionTime = request.getSubmissionTime();
    this.days = request.getDays().stream().map(DayDto::new).collect(Collectors.toList());
    this.currentlyApprovedBy = request.getCurrentlyApprovedBy().stream().map(UserDto::new).collect(Collectors.toList());
  }

}