package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Team;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TeamViewDto {

  private Long id;

  private String name;

  public TeamViewDto(Team team) {
    this.id = team.getId();
    this.name = team.getName();
  }
}
