package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.metric.HardwareMetrics;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class HardwareMetricDto {

  private Long id;
  private double systemCpuUsage;
  private double jvmCpuUsage;
  private double memoryUsage;
  private LocalDateTime timestamp;

  public HardwareMetricDto(HardwareMetrics hardwareMetrics) {
    this.id = hardwareMetrics.getId();
    this.systemCpuUsage = hardwareMetrics.getSystemCpuUsage();
    this.jvmCpuUsage = hardwareMetrics.getJvmCpuUsage();
    this.memoryUsage = hardwareMetrics.getMemoryUsage();
    this.timestamp = hardwareMetrics.getTimestamp();
  }
}
