package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Request;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class CommentsDto {

  @NotNull
  private String reason;

  private String approverComment;

  private String cancellationReason;

  private String approverCancellationComment;

  public CommentsDto(Request request) {
    this.reason = request.getReason();
    this.approverComment = request.getApproverComment();
    this.cancellationReason = request.getCancellationReason();
    this.approverCancellationComment = request.getApproverCancellationComment();
  }

}
