package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Icon;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class IconDto {

  private Long id;

  @NotNull
  private String iconUrl;

  public IconDto(Icon icon) {
    this.id = icon.getId();
    this.iconUrl = icon.getIconUrl();
  }
}
