package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.util.EmailExecom;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class UserWithApproverTeamsDto {

  private Long id;

  @NotNull
  @Positive(message="Team ID must be a positive number.")
  private Long teamId;

  private String teamName;

  @NotNull
  @Positive(message="Leave profile ID must be a positive number.")
  private Long leaveProfileId;

  @NotNull
  @Size(max = 50, message = "Name is too long.")
  private String fullName;

  @NotNull
  @EmailExecom
  @Size(max = 50, message = "Email is too long.")
  private String email;

  @NotNull
  private UserRole userRole;

  @Size(max = 50, message = "Job title is too long.")
  private String jobTitle;

  @NotNull
  private UserStatusType userStatusType;

  @NotNull
  private LocalDate startedWorkingDate;

  private LocalDate startedProfessionalCareerDate;

  @NotNull
  private LocalDate startedWorkingAtExecomDate;

  private LocalDate stoppedWorkingAtExecomDate;

  private int yearsOfService;

  @Size(max = 250, message = "Image URL is too long.")
  private String imageUrl;

  private List<UserPushTokenDto> userPushTokens = new ArrayList<>();

  private List<TeamViewDto> approverTeams = new ArrayList<>();

  public UserWithApproverTeamsDto(User user) {
    this.id = user.getId();
    this.teamId = (user.getTeam().getId());
    this.teamName = (user.getTeam().getName());
    this.leaveProfileId = (user.getLeaveProfile().getId());
    this.fullName = user.getFullName();
    this.email = user.getEmail();
    this.userRole = user.getUserRole();
    this.jobTitle = user.getJobTitle();
    this.userStatusType = user.getUserStatusType();
    this.startedWorkingDate = user.getStartedWorkingDate();
    this.startedProfessionalCareerDate = user.getStartedProfessionalCareerDate();
    this.startedWorkingAtExecomDate = user.getStartedWorkingAtExecomDate();
    this.stoppedWorkingAtExecomDate = user.getStoppedWorkingAtExecomDate();
    this.yearsOfService = user.getYearsOfService();
    this.imageUrl = user.getImageUrl();
    this.approverTeams = user.getApproverTeams().stream().map(TeamViewDto::new).collect(Collectors.toList());
    this.userPushTokens = user.getUserPushTokens().stream().map(UserPushTokenDto::new).collect(Collectors.toList());
  }
}
