package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.UserPushToken;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EditPushTokenDto {
  private Long oldPushTokenId;
  private String newPushToken;

  public EditPushTokenDto(UserPushToken userPushToken) {
    this.oldPushTokenId = userPushToken.getId();
    this.newPushToken = userPushToken.getPushToken();
  }
}
