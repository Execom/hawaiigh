package eu.execom.hawaii.dto;

import eu.execom.hawaii.model.Year;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class YearDto {

  private Long id;

  @NotNull
  private int year;

  @NotNull
  private boolean active;

  private boolean carriedOverHoursExpirable;

  private LocalDate carriedOverHoursExpirationDate;

  public YearDto(Year year) {
    this.id = year.getId();
    this.year = year.getYear();
    this.active = year.isActive();
    this.carriedOverHoursExpirable = year.isCarriedOverHoursExpirable();
    this.carriedOverHoursExpirationDate = year.getCarriedOverHoursExpirationDate();
  }
}
