package eu.execom.hawaii.configuration.batch;

import eu.execom.hawaii.batch.FileDeletingTasklet;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.batch.AppogeeLeaveRequestImportData;
import eu.execom.hawaii.model.batch.CalendarImportData;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class RequestBatchConfiguration {

  private static final String[] CALENDAR_FIELD_NAMES = new String[] {"Event Title", "Event Description", "Reason",
      "Event Start", "Event End", "Date Created", "All Day Event"};

  private static final String[] LEAVE_REQUESTS_FILED_NAMES = new String[] {"Name", "Email", "Parent Team", "Team",
      "EmployeeStatus", "PayrollNo", "DateSubmitted", "Type", "Status", "StartDate", "EndDate", "Hours",
      "ComputedHours", "Days", "HoursInRange", "ComputedHoursInRange", "DaysInRange", "Reason", "LeaveYear",
      "CreatedViaBackfill", "ApprovedBy", "DateApproved", "ApproverComments", "CancellationReason",
      "CancellationApproverComments"};

  @Value("${calendarResourcePath}")
  private Resource calendarResource;

  @Value("${leaveRequestsResourcePath}")
  private Resource leaveResource;

  @Value("${sicknessRequestsResourcePath}")
  private Resource sicknessResource;

  private StepBuilderFactory stepBuilderFactory;

  @Autowired
  public RequestBatchConfiguration(StepBuilderFactory stepBuilderFactory) {
    this.stepBuilderFactory = stepBuilderFactory;
  }

  @Bean
  public Step importCalendarRequestsFile(ItemReader<CalendarImportData> itemReader,
      ItemProcessor<CalendarImportData, Request> itemProcessor, ItemWriter<Request> requestDbWriter) {
    //@formatter:off
    return stepBuilderFactory.get("Import-calendar-requests-file")
                             .<CalendarImportData, Request>chunk(100)
                             .reader(itemReader)
                             .processor(itemProcessor)
                             .writer(requestDbWriter)
                             .build();
    //@formatter:on
  }

  @Bean
  public Step importLeaveRequestsAppogeeFiles(ItemReader<AppogeeLeaveRequestImportData> itemReader,
      ItemProcessor<AppogeeLeaveRequestImportData, Request> itemProcessor, ItemWriter<Request> appogeeLeaveDbWriter) {
    //@formatter:off
    return stepBuilderFactory.get("Import-leave-requests-appogee-files")
                             .<AppogeeLeaveRequestImportData, Request>chunk(Integer.MAX_VALUE)
                             .reader(itemReader)
                             .processor(itemProcessor)
                             .writer(appogeeLeaveDbWriter)
                             .build();
    //@formatter:on
  }

  @Bean
  public Step removeCalendarRequestsFile() {
    FileDeletingTasklet task = new FileDeletingTasklet();
    task.setResource(calendarResource);

    return stepBuilderFactory.get("Remove-calendar-requests-file").tasklet(task).build();
  }

  @Bean
  public Step removeLeaveRequestsFile() {
    FileDeletingTasklet task = new FileDeletingTasklet();
    task.setResource(leaveResource);

    return stepBuilderFactory.get("Remove-leave-requests-file").tasklet(task).build();
  }

  @Bean
  public Step removeSicknessRequestsFile() {
    FileDeletingTasklet task = new FileDeletingTasklet();
    task.setResource(sicknessResource);

    return stepBuilderFactory.get("Remove-sickness-requests-file").tasklet(task).build();
  }

  @Bean
  public FlatFileItemReader<CalendarImportData> calendarFileItemReader() {
    FlatFileItemReader<CalendarImportData> flatFileItemReader = new FlatFileItemReader<>();
    flatFileItemReader.setResource(calendarResource);
    flatFileItemReader.setName("Calendar CSV Reader");
    flatFileItemReader.setLinesToSkip(1);
    flatFileItemReader.setStrict(false);
    flatFileItemReader.setEncoding("UTF-8");
    flatFileItemReader.setLineMapper(calendarLineMapper());

    return flatFileItemReader;
  }

  @Bean
  public MultiResourceItemReader<AppogeeLeaveRequestImportData> multiResourceItemReader() {
    Resource[] appogeeResources = {leaveResource, sicknessResource};

    MultiResourceItemReader<AppogeeLeaveRequestImportData> resourceItemReader = new MultiResourceItemReader<>();
    resourceItemReader.setResources(appogeeResources);
    resourceItemReader.setDelegate(appogeeLeaveFileItemReader());

    return resourceItemReader;
  }

  private FlatFileItemReader<AppogeeLeaveRequestImportData> appogeeLeaveFileItemReader() {
    FlatFileItemReader<AppogeeLeaveRequestImportData> flatFileItemReader = new FlatFileItemReader<>();
    flatFileItemReader.setName("Appogee Leave CSV Reader");
    flatFileItemReader.setLinesToSkip(1);
    flatFileItemReader.setStrict(false);
    flatFileItemReader.setEncoding("UTF-8");
    flatFileItemReader.setLineMapper(appogeeLeaveLineMapper());

    return flatFileItemReader;
  }

  private LineMapper<CalendarImportData> calendarLineMapper() {
    DefaultLineMapper<CalendarImportData> defaultLineMapper = new DefaultLineMapper<>();

    DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
    lineTokenizer.setDelimiter(",");
    lineTokenizer.setStrict(false);
    lineTokenizer.setNames(CALENDAR_FIELD_NAMES);

    LocalDateTimeBeanWrapperFieldSetMapper<CalendarImportData> fieldSetMapper = new LocalDateTimeBeanWrapperFieldSetMapper<>();
    fieldSetMapper.setTargetType(CalendarImportData.class);

    defaultLineMapper.setLineTokenizer(lineTokenizer);
    defaultLineMapper.setFieldSetMapper(fieldSetMapper);

    return defaultLineMapper;
  }

  private LineMapper<AppogeeLeaveRequestImportData> appogeeLeaveLineMapper() {
    DefaultLineMapper<AppogeeLeaveRequestImportData> defaultLineMapper = new DefaultLineMapper<>();

    DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
    lineTokenizer.setDelimiter(",");
    lineTokenizer.setStrict(false);
    lineTokenizer.setNames(LEAVE_REQUESTS_FILED_NAMES);

    LocalDateBeanWrapperFieldSetMapper<AppogeeLeaveRequestImportData> fieldSetMapper = new LocalDateBeanWrapperFieldSetMapper<>();
    fieldSetMapper.setTargetType(AppogeeLeaveRequestImportData.class);

    defaultLineMapper.setLineTokenizer(lineTokenizer);
    defaultLineMapper.setFieldSetMapper(fieldSetMapper);

    return defaultLineMapper;
  }

}