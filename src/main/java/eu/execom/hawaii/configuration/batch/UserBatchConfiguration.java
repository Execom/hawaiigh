package eu.execom.hawaii.configuration.batch;

import eu.execom.hawaii.batch.FileDeletingTasklet;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.batch.UserImportData;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class UserBatchConfiguration {

  private static final String[] USER_IMPORT_FIELD_NAMES = {"FirstName", "LastName", "Email", "PayrollNo",
      "ProfessionalStartDate", "ContinuousStartDate", "StartDate", "EndDate", "Status", "Team", "VirtualTeam",
      "LeaveProfile", "JobTitle", "UserRole"};

  @Value("${userResourcePath}")
  private Resource resource;

  private StepBuilderFactory stepBuilderFactory;

  @Autowired
  public UserBatchConfiguration(StepBuilderFactory stepBuilderFactory) {
    this.stepBuilderFactory = stepBuilderFactory;
  }

  @Bean
  public Step importUserFile(ItemReader<UserImportData> itemReader, ItemProcessor<UserImportData, User> itemProcessor,
      ItemWriter<User> itemWriter) {
    //@formatter:off
    return stepBuilderFactory.get("Import-user-file")
                             .<UserImportData, User>chunk(100)
                             .reader(itemReader)
                             .processor(itemProcessor)
                             .writer(itemWriter)
                             .build();
    //@formatter:on
  }

  @Bean
  public Step removeUserFile() {
    FileDeletingTasklet task = new FileDeletingTasklet();
    task.setResource(resource);

    return stepBuilderFactory.get("Remove-user-file").tasklet(task).build();
  }

  @Bean
  public FlatFileItemReader<UserImportData> userFileItemReader() {
    FlatFileItemReader<UserImportData> flatFileItemReader = new FlatFileItemReader<>();
    flatFileItemReader.setResource(resource);
    flatFileItemReader.setName("User CSV Reader");
    flatFileItemReader.setLinesToSkip(1);
    flatFileItemReader.setStrict(false);
    flatFileItemReader.setEncoding("UTF-8");
    flatFileItemReader.setLineMapper(userLineMapper());

    return flatFileItemReader;
  }

  private LineMapper<UserImportData> userLineMapper() {
    DefaultLineMapper<UserImportData> defaultLineMapper = new DefaultLineMapper<>();

    DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
    lineTokenizer.setDelimiter(",");
    lineTokenizer.setStrict(false);
    lineTokenizer.setNames(USER_IMPORT_FIELD_NAMES);

    LocalDateBeanWrapperFieldSetMapper<UserImportData> fieldSetMapper = new LocalDateBeanWrapperFieldSetMapper<>();
    fieldSetMapper.setTargetType(UserImportData.class);

    defaultLineMapper.setLineTokenizer(lineTokenizer);
    defaultLineMapper.setFieldSetMapper(fieldSetMapper);

    return defaultLineMapper;
  }

}