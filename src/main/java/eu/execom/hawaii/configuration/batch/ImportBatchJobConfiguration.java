package eu.execom.hawaii.configuration.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class ImportBatchJobConfiguration {

  private JobBuilderFactory jobBuilderFactory;

  @Autowired
  public ImportBatchJobConfiguration(JobBuilderFactory jobBuilderFactory) {
    this.jobBuilderFactory = jobBuilderFactory;
  }

  @Bean
  public Job csvImportJob(
      Step importUserFile,
      Step removeUserFile,
      Step importAllowanceFile,
      Step removeAllowanceFile,
      Step importLeaveRequestsAppogeeFiles,
      Step removeLeaveRequestsFile,
      Step removeSicknessRequestsFile,
      Step importCalendarRequestsFile,
      Step removeCalendarRequestsFile) {

    return jobBuilderFactory
        .get("Load-CSV-Data")
        .incrementer(new RunIdIncrementer())
        .start(importUserFile)
        .next(removeUserFile)
        .next(importAllowanceFile)
        .next(removeAllowanceFile)
        .next(importLeaveRequestsAppogeeFiles)
        .next(removeLeaveRequestsFile)
        .next(removeSicknessRequestsFile)
        .next(importCalendarRequestsFile)
        .next(removeCalendarRequestsFile)
        .build();
  }
}
