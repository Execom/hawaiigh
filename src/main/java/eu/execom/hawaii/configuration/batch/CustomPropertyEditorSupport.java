package eu.execom.hawaii.configuration.batch;

import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CustomPropertyEditorSupport extends PropertyEditorSupport {

  private static final String LOCAL_DATE_FORMAT = "yyyy-MM-dd";

  private String dateFormat;
  private DateTimeFormatter formatter;

  CustomPropertyEditorSupport(String dateFormat) {
    super();
    this.dateFormat = dateFormat;
    this.formatter = DateTimeFormatter.ofPattern(dateFormat);
  }

  @Override
  public String getAsText() throws IllegalArgumentException {
    Object date = getValue();
    if (date == null) {
      return "";
    }

    if (dateFormat.equals(LOCAL_DATE_FORMAT)) {
      return formatter.format((LocalDate) date);
    } else {
      return formatter.format(((LocalDateTime) date));
    }
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    if (!StringUtils.isEmpty(text)) {

      if (dateFormat.equals(LOCAL_DATE_FORMAT)) {
        setValue(LocalDate.parse(text, formatter));
      } else {
        setValue(LocalDateTime.parse(text, formatter));
      }
    } else {
      setValue(null);
    }
  }

}
