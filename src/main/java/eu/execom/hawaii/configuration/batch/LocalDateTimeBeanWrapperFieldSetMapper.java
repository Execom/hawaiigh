package eu.execom.hawaii.configuration.batch;

import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.validation.DataBinder;

import java.time.LocalDateTime;

public class LocalDateTimeBeanWrapperFieldSetMapper<T> extends BeanWrapperFieldSetMapper<T> {

  private static final String LOCAL_DATE_TIME_FORMAT = "yyyy-MM-dd H:m:s";

  @Override
  protected void initBinder(DataBinder binder) {
    binder.registerCustomEditor(LocalDateTime.class, new CustomPropertyEditorSupport(LOCAL_DATE_TIME_FORMAT));
  }

}