package eu.execom.hawaii.configuration.batch;

import eu.execom.hawaii.batch.FileDeletingTasklet;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.batch.AllowanceImportData;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class AllowanceBatchConfiguration {

  @Value("${allowanceResourcePath}")
  private Resource resource;

  private StepBuilderFactory stepBuilderFactory;

  @Autowired
  public AllowanceBatchConfiguration(StepBuilderFactory stepBuilderFactory) {
    this.stepBuilderFactory = stepBuilderFactory;
  }

  @Bean
  public Step importAllowanceFile(ItemReader<AllowanceImportData> itemReader,
      ItemProcessor<AllowanceImportData, Allowance> itemProcessor, ItemWriter<Allowance> itemWriter) {
    //@formatter:off
    return  stepBuilderFactory.get("Import-allowance-file")
                              .<AllowanceImportData, Allowance>chunk(100)
                              .reader(itemReader)
                              .processor(itemProcessor)
                              .writer(itemWriter)
                              .build();
    //@formatter:on
  }

  @Bean
  public Step removeAllowanceFile() {
    FileDeletingTasklet task = new FileDeletingTasklet();
    task.setResource(resource);

    return stepBuilderFactory.get("Remove-allowance-file").tasklet(task).build();
  }

  @Bean
  public FlatFileItemReader<AllowanceImportData> allowanceFileItemReader() {
    FlatFileItemReader<AllowanceImportData> flatFileItemReader = new FlatFileItemReader<>();
    flatFileItemReader.setResource(resource);
    flatFileItemReader.setName("Allowance CSV Reader");
    flatFileItemReader.setLinesToSkip(1);
    flatFileItemReader.setStrict(false);
    flatFileItemReader.setEncoding("UTF-8");
    flatFileItemReader.setLineMapper(allowanceLineMapper());

    return flatFileItemReader;
  }

  private LineMapper<AllowanceImportData> allowanceLineMapper() {
    DefaultLineMapper<AllowanceImportData> defaultLineMapper = new DefaultLineMapper<>();

    DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
    lineTokenizer.setDelimiter(",");
    lineTokenizer.setStrict(false);
    lineTokenizer.setNames("EmployeeName", "Email", "ParentTeam", "Team", "PayrollNo", "AllowanceName",
        "LeaveAllocatedHours", "LeaveAccruedHours", "LeaveCarryOverHours", "LeaveAdjustHours", "LeaveToilComputedHours",
        "LeaveToilAdjustHours", "LeaveToilTotalHours", "LeavePendingHours", "LeaveApprovedHours", "LeaveRemaining",
        "LeaveRemaining(IncPending)", "SickAllocatedHours", "SickAccruedHours", "SickCarryOverHours", "SickAdjustHours",
        "SickPendingHours", "SickApprovedHours", "SickRemaining", "SickRemaining(IncPending)", "Custom1AllocatedHours",
        "Custom1AccruedHours", "Custom1CarryOverHours", "Custom1AdjustHours", "Custom1PendingHours",
        "Custom1TakenHours");

    BeanWrapperFieldSetMapper fieldSetMapper = new BeanWrapperFieldSetMapper();
    fieldSetMapper.setTargetType(AllowanceImportData.class);

    defaultLineMapper.setLineTokenizer(lineTokenizer);
    defaultLineMapper.setFieldSetMapper(fieldSetMapper);

    return defaultLineMapper;
  }

}
