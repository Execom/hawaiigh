package eu.execom.hawaii.configuration.batch;

import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.validation.DataBinder;

import java.time.LocalDate;

public class LocalDateBeanWrapperFieldSetMapper<T> extends BeanWrapperFieldSetMapper<T> {

  private static final String LOCAL_DATE_FORMAT = "yyyy-MM-dd";

  @Override
  protected void initBinder(DataBinder binder) {
    binder.registerCustomEditor(LocalDate.class, new CustomPropertyEditorSupport(LOCAL_DATE_FORMAT));
  }

}
