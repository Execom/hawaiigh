package eu.execom.hawaii.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "app")
public class AppProperties {
  private final Auth auth = new Auth();
  private final OAuth2 oauth2 = new OAuth2();

  public static class Auth {
    private String tokenSecret;
    private long tokenExpirationMilliseconds;

    public String getTokenSecret() {
      return tokenSecret;
    }

    @SuppressWarnings("unused")
    public void setTokenSecret(String tokenSecret) {
      this.tokenSecret = tokenSecret;
    }

    public long getTokenExpirationMilliseconds() {
      return tokenExpirationMilliseconds;
    }

    @SuppressWarnings("unused")
    public void setTokenExpirationMilliseconds(long tokenExpirationMsec) {
      this.tokenExpirationMilliseconds = tokenExpirationMsec;
    }
  }

  public static final class OAuth2 {
    private List<String> authorizedRedirectUris = new ArrayList<>();

    public List<String> getAuthorizedRedirectUris() {
      return authorizedRedirectUris;
    }
  }

  public Auth getAuth() {
    return auth;
  }

  public OAuth2 getOauth2() {
    return oauth2;
  }
}
