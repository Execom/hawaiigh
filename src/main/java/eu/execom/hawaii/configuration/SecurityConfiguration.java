package eu.execom.hawaii.configuration;

import eu.execom.hawaii.security.CustomUserDetailsService;
import eu.execom.hawaii.security.RestAuthenticationEntryPoint;
import eu.execom.hawaii.security.TokenAuthenticationFilter;
import eu.execom.hawaii.security.TokenProvider;
import eu.execom.hawaii.security.oauth2.CustomOAuth2UserService;
import eu.execom.hawaii.security.oauth2.HttpCookieOAuth2AuthorizationRequestRepository;
import eu.execom.hawaii.security.oauth2.OAuth2AuthenticationFailureHandler;
import eu.execom.hawaii.security.oauth2.OAuth2AuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableWebSecurity
@EnableSwagger2
@Import(springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration.class)
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final String[] userEndpoints = {"/api/users/teamUsers", "/api/users/days", "/api/users/teamCalendarList",
      "/api/users/me", "/api/users/monthDays", "/api/teams/name", "/api/requests/{id}", "/api/requests/team/month",
      "/api/requests/user", "/api/requests/user/month", "/api/requests/years/range", "/api/requests/user/year",
      "/api/allowances/me", "/api/allowances/years", "/api/allowances/user/year", "/api/publicholidays/**",
      "/api/leavetypes/**", "/api/years/**", "/api/audit/approvalFlow/{id}", "/api/security/user"};

  private final String[] administrationEndpoints = {"/api/export/**", "/api/security/administration"};

  private final String[] approverEndpoints = {"/api/users/{id}", "/api/users/search", "/api/requests/approval",
      "/api/allowances/**"};

  private final String[] hrManagerEndpoints = {"/api/users/**", "/api/teams/**", "/api/requests/**",
      "/api/leaveprofiles/**", "/api/import/**", "/api/audit/**", "/api/security/hr_manager"};

  private final String[] adminEndpoints = {"/api/metrics/**", "/api/security/admin"};

  private final String[] superAdminEndpoints = {"/api/admins/**", "/api/security/super_admin"};

  private final String[] allUserRoles = {"HR_MANAGER", "USER", "APPROVER", "OFFICE_MANAGER"};

  private TokenProvider tokenProvider;

  private CustomUserDetailsService customUserDetailsService;

  private CustomOAuth2UserService customOAuth2UserService;

  private OAuth2AuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler;

  private OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;

  //@formatter:off
  @Autowired
  public SecurityConfiguration(
      CustomUserDetailsService customUserDetailsService,
      CustomOAuth2UserService customOAuth2UserService,
      OAuth2AuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler,
      OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler,
      TokenProvider tokenProvider) {
    this.customUserDetailsService = customUserDetailsService;
    this.customOAuth2UserService = customOAuth2UserService;
    this.oAuth2AuthenticationSuccessHandler = oAuth2AuthenticationSuccessHandler;
    this.oAuth2AuthenticationFailureHandler = oAuth2AuthenticationFailureHandler;
    this.tokenProvider = tokenProvider;
  }
  //@formatter:on

  /*
    By default, Spring OAuth2 uses HttpSessionOAuth2AuthorizationRequestRepository to save
    the authorization request. But, since our service is stateless, we can't save it in
    the session. We'll save the request in a Base64 encoded cookie instead.
  */

  @Bean
  public HttpCookieOAuth2AuthorizationRequestRepository cookieAuthorizationRequestRepository() {
    return new HttpCookieOAuth2AuthorizationRequestRepository();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean(BeanIds.AUTHENTICATION_MANAGER)
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public TokenAuthenticationFilter tokenAuthenticationFilter() {
    return new TokenAuthenticationFilter(tokenProvider, customUserDetailsService);
  }

  @Bean
  public FilterRegistrationBean filterRegistrationBean() {
    FilterRegistrationBean<TokenAuthenticationFilter> filterFilterRegistrationBean = new FilterRegistrationBean<>(
        tokenAuthenticationFilter());
    filterFilterRegistrationBean.setEnabled(false);

    return filterFilterRegistrationBean;
  }

  @Override
  public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
    authenticationManagerBuilder.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    //@formatter:off
    http
        .cors()
           .and()
             .sessionManagement()
             .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
           .and()
         .csrf()
           .disable()
         .formLogin()
           .disable()
         .httpBasic()
           .disable()
        .exceptionHandling()
           .authenticationEntryPoint(new RestAuthenticationEntryPoint())
          .and()
           .authorizeRequests()
             .antMatchers(
                 "/error",
                 "/favicon.ico",
                 "/**/*.gif",
                 "/**/*.jpg",
                 "/**/*.html",
                 "/**/*.css",
                 "/**/*.js")
               .permitAll()
             .antMatchers("/auth/**", "/oauth2/**")
               .permitAll()
             .antMatchers(userEndpoints).hasAnyAuthority(allUserRoles)
             .antMatchers(HttpMethod.POST, "/api/requests").hasAnyAuthority(allUserRoles)
             .antMatchers(HttpMethod.PUT, "/api/requests").hasAnyAuthority(allUserRoles)
             .antMatchers(HttpMethod.PUT, "/api/users/loginUpdate").hasAnyAuthority(allUserRoles)
             .antMatchers(administrationEndpoints).hasAnyAuthority("OFFICE_MANAGER", "HR_MANAGER")
             .antMatchers(approverEndpoints).hasAnyAuthority("HR_MANAGER", "APPROVER")
             .antMatchers(hrManagerEndpoints).hasAuthority("HR_MANAGER")
             .antMatchers(superAdminEndpoints).hasAuthority("SUPER_ADMIN")
             .antMatchers(adminEndpoints).hasAnyAuthority("SUPER_ADMIN", "ADMIN")
             .antMatchers("/api/**")
               .permitAll()
          .and()
        .oauth2Login()
          .authorizationEndpoint()
            .baseUri("/oauth2/authorize")
            .authorizationRequestRepository(cookieAuthorizationRequestRepository())
          .and()
            .redirectionEndpoint()
            .baseUri("/oauth2/callback/*")
          .and()
            .userInfoEndpoint()
            .userService(customOAuth2UserService)
          .and()
            .successHandler(oAuth2AuthenticationSuccessHandler)
            .failureHandler(oAuth2AuthenticationFailureHandler);
    //@formatter:on

    http.addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring().antMatchers("/swagger-resources/**", "/webjars/**", "swagger-ui.html");
  }
}
