package eu.execom.hawaii.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

  private static final int MAX_AGE_SECS = 3600;
  @SuppressWarnings("unused")
  @Value("${hawaii.localhost}")
  private String local;
  @SuppressWarnings("unused")
  @Value("${hawaii.localhost.dev}")
  private String localDev;
  @SuppressWarnings("unused")
  @Value("${hawaii.staging}")
  private String staging;
  @SuppressWarnings("unused")
  @Value("${hawaii.production}")
  private String production;

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
            .allowedOrigins(local, localDev, staging, production)
            .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS")
            .allowedHeaders("*")
            .allowCredentials(true)
            .maxAge(MAX_AGE_SECS);
  }

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/{spring:\\w+(?:-\\w+)*}").setViewName("forward:/");
    registry.addViewController("/{icons:(?:[^icons]*)}/**").setViewName("forward:/");
  }
}