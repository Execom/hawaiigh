package eu.execom.hawaii.security;

import eu.execom.hawaii.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class UserPrincipal implements OAuth2User, UserDetails {

  private static final long serialVersionUID = -4265776601124373754L;

  private Long id;
  private String email;
  private Collection<? extends GrantedAuthority> authorities;
  private Map<String, Object> attributes;

  private UserPrincipal(Long id, String email, Collection<? extends GrantedAuthority> authorities) {
    this.id = id;
    this.email = email;
    this.authorities = authorities;
  }

  public static UserPrincipal create(User user) {
    List<GrantedAuthority> authorities = new ArrayList<>();
    String userRole = user.getUserRole().toString();
    authorities.add(new SimpleGrantedAuthority(userRole));

    switch (user.getUserAdminPermission()) {
      case NONE:
        break;
      case BASIC:
        authorities.add(new SimpleGrantedAuthority("ADMIN"));
        break;
      case SUPER:
        authorities.add(new SimpleGrantedAuthority("SUPER_ADMIN"));
        break;
      default:
        throw new IllegalArgumentException("Unrecognized permission: " + user.getUserAdminPermission());
    }

    if (user.isApprover()) {
      authorities.add(new SimpleGrantedAuthority("APPROVER"));
    }

    return new UserPrincipal(user.getId(), user.getEmail(), authorities);
  }

  Long getId() {
    return id;
  }

  String getEmail() {
    return email;
  }

  @Override
  public String getPassword() {
    return null;
  }

  @Override
  public String getUsername() {
    return email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return authorities;
  }

  @Override
  public Map<String, Object> getAttributes() {
    return attributes;
  }

  @Override
  public String getName() {
    return String.valueOf(this.getId());
  }
}
