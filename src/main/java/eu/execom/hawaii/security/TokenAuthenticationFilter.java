package eu.execom.hawaii.security;

import eu.execom.hawaii.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class TokenAuthenticationFilter extends OncePerRequestFilter {

  private static final String TOKEN_HEADER = "Authorization";
  private static final String TOKEN_PREFIX = "Bearer ";
  private static final int TOKEN_PREFIX_LENGTH = 7;

  @Value("${tokenIssuer}")
  private String tokenIssuer;

  private TokenProvider tokenProvider;

  private CustomUserDetailsService customUserDetailsService;

  @Autowired
  public TokenAuthenticationFilter(TokenProvider tokenProvider, CustomUserDetailsService customUserDetailsService) {
    this.tokenProvider = tokenProvider;
    this.customUserDetailsService = customUserDetailsService;
  }

  @SuppressWarnings("NullableProblems")
  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String token = getTokenFromRequest(request);
    if (token != null) {
      boolean isValidToken = tokenProvider.validateToken(token);

      if (isValidToken) {
        String issuer = tokenProvider.getIssuerFromToken(token);
        String email = tokenProvider.getUserEmailFromToken(token);
        User user = customUserDetailsService.loadUserByEmail(email);

        if (!issuer.equals(tokenIssuer)) {
          response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
          log.error("Issuer '{}' is not valid token issuer.", issuer);
          return;
        }

        if (!user.isActive()) {
          response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
          log.error("User '{}' found in database, but is not active.", user.getFullName());
          return;
        }

        UserPrincipal userPrincipal = UserPrincipal.create(user);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null,
            userPrincipal.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authentication);
      }
    }

    filterChain.doFilter(request, response);
  }

  private String getTokenFromRequest(HttpServletRequest request) {
    String bearerToken = request.getHeader(TOKEN_HEADER);

    return StringUtils.hasText(bearerToken) && bearerToken.startsWith(TOKEN_PREFIX) ?
        bearerToken.substring(TOKEN_PREFIX_LENGTH) :
        null;
  }
}