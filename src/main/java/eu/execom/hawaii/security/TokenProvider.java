package eu.execom.hawaii.security;

import eu.execom.hawaii.configuration.AppProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class TokenProvider {

  private AppProperties appProperties;

  @Value("${tokenIssuer}")
  private String tokenIssuer;

  @Autowired
  public TokenProvider(AppProperties appProperties) {
    this.appProperties = appProperties;
  }

  public String createToken(Authentication authentication) {

    UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
    Date currentTime = new Date();
    Date expirationTime = new Date(currentTime.getTime() + appProperties.getAuth().getTokenExpirationMilliseconds());
    String email = userPrincipal.getEmail();

    return Jwts.builder()
               .setSubject(email)
               .setIssuer(tokenIssuer)
               .setIssuedAt(currentTime)
               .setExpiration(expirationTime)
               .signWith(SignatureAlgorithm.HS512, appProperties.getAuth().getTokenSecret())
               .compact();
  }

  private Claims getClaims(String token) {
    return Jwts.parser()
               .setSigningKey(appProperties.getAuth().getTokenSecret())
               .parseClaimsJws(token)
               .getBody();
  }

  String getUserEmailFromToken(String token) {
    return getClaims(token).getSubject();
  }

  String getIssuerFromToken(String token) {
    return getClaims(token).getIssuer();
  }

  boolean validateToken(String authToken) {
    try {
      Jwts.parser().setSigningKey(appProperties.getAuth().getTokenSecret()).parseClaimsJws(authToken);
      return true;
    } catch (SignatureException ex) {
      log.error("Token signature is invalid.");
    } catch (MalformedJwtException ex) {
      log.error("Invalid token.");
    } catch (ExpiredJwtException ex) {
      log.error("Token has expired.");
    } catch (UnsupportedJwtException ex) {
      log.error("Unsupported token.");
    } catch (IllegalArgumentException ex) {
      log.error("Token claims string is empty.");
    }
    return false;
  }
}
