package eu.execom.hawaii.security.oauth2;

import java.util.Map;

public class GoogleOAuth2UserInfo {

  private Map<String, Object> attributes;

  private GoogleOAuth2UserInfo(Map<String, Object> attributes) {
    this.attributes = attributes;
  }

  static GoogleOAuth2UserInfo getOAuth2UserInfo(Map<String, Object> attributes) {
    return new GoogleOAuth2UserInfo(attributes);
  }

  public String getId() {
    return (String) attributes.get("sub");
  }

  public String getName() {
    return (String) attributes.get("name");
  }

  public String getEmail() {
    return (String) attributes.get("email");
  }

  public String getImageUrl() {
    return (String) attributes.get("picture");
  }
}
