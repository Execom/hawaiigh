package eu.execom.hawaii.security.oauth2;

import com.google.common.base.Strings;
import eu.execom.hawaii.exceptions.OAuth2AuthenticationProcessingException;
import eu.execom.hawaii.exceptions.UnauthorizedEmailDomainException;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

  private static final String EXECOM_DOMAIN = "execom.eu";

  private UserRepository userRepository;

  @Autowired
  public CustomOAuth2UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
    OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

    try {
      return processOAuth2User(oAuth2User);
    } catch (AuthenticationException authenticationException) {
      throw authenticationException;
    } catch (Exception exception) {
      throw new InternalAuthenticationServiceException(exception.getMessage(), exception.getCause());
    }
  }

  OAuth2User processOAuth2User(OAuth2User oAuth2User) {

    GoogleOAuth2UserInfo oAuth2UserInfo = GoogleOAuth2UserInfo.getOAuth2UserInfo(oAuth2User.getAttributes());
    String userEmail = oAuth2UserInfo.getEmail();

    validateEmailIsPresent(userEmail);
    validateEmailDomain(userEmail);
    User user = getUser(oAuth2UserInfo);

    return UserPrincipal.create(user);
  }

  private void validateEmailIsPresent(String userEmail) {
    if (Strings.isNullOrEmpty(userEmail)) {
      throw new OAuth2AuthenticationProcessingException("Failed to get email from OAuth2 provider.");
    }
  }

  private void validateEmailDomain(String userEmail) {
    String userEmailDomain = userEmail.substring(userEmail.indexOf("@") + 1);
    if (!EXECOM_DOMAIN.equalsIgnoreCase(userEmailDomain)) {
      throw new UnauthorizedEmailDomainException(
          "Email domain '" + userEmailDomain + "' is not supported.");
    }
  }

  private User getUser(GoogleOAuth2UserInfo oAuth2UserInfo) {
    return userRepository.findByEmail(oAuth2UserInfo.getEmail())
                         .map(u -> updateExistingUser(u, oAuth2UserInfo))
                         .orElseThrow(
                             () -> new EntityNotFoundException("Provided email does not belong to any user in the database."));
  }

  private User updateExistingUser(User user, GoogleOAuth2UserInfo oAuth2UserInfo) {
    Optional<String> imageUrl = Optional.ofNullable(user.getImageUrl());
    if (imageUrl.isPresent() && imageUrl.get().equals(oAuth2UserInfo.getImageUrl())) {
      return user;
    } else {
      user.setImageUrl(oAuth2UserInfo.getImageUrl());
      return userRepository.save(user);
    }
  }

}

