package eu.execom.hawaii.exceptions;

public class FieldNotAccessibleException extends RuntimeException {
  private static final long serialVersionUID = 361452070437737005L;

  public FieldNotAccessibleException(String message) {
    super(message);
  }
}