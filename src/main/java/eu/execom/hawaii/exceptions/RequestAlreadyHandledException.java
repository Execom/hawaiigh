package eu.execom.hawaii.exceptions;

public class RequestAlreadyHandledException extends RuntimeException {

  private static final long serialVersionUID = 502519430069830423L;

  public RequestAlreadyHandledException(String requestStatus) {
    super("This request has already been " + requestStatus + ".");
  }
}
