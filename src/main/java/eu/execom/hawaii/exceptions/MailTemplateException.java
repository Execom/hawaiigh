package eu.execom.hawaii.exceptions;

public class MailTemplateException extends RuntimeException {

    private static final long serialVersionUID = 2387269116675591721L;

    public MailTemplateException(String message) {
        super(message);
    }

}
