package eu.execom.hawaii.exceptions;

import eu.execom.hawaii.model.User;

public class NegativeAllowanceException extends RuntimeException {

  private static final String MESSAGE = "Allowance for user '%s' can be manually adjusted for a minimum of '-%.1f' days.";

  public NegativeAllowanceException(User user, int remainingAllowance) {
    super(String.format(MESSAGE, user.getFullName(), Math.round(remainingAllowance / 4.0) / 2.0));
  }
}
