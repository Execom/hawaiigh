package eu.execom.hawaii.exceptions;

public class PushNotificationSendingException extends RuntimeException {

    private static final long serialVersionUID = -2988407226283945911L;

    public PushNotificationSendingException(String message) {
        super(message);
    }
}
