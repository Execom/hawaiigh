package eu.execom.hawaii.exceptions;

import org.springframework.security.core.AuthenticationException;

public class OAuth2AuthenticationProcessingException extends AuthenticationException {

  private static final long serialVersionUID = -5618221133246098666L;

  public OAuth2AuthenticationProcessingException(String message) {
    super(message);
  }
}
