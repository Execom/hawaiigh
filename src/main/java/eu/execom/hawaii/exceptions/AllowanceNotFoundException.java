package eu.execom.hawaii.exceptions;

public class AllowanceNotFoundException extends RuntimeException {

  public AllowanceNotFoundException(String message) {
    super(message);
  }
}
