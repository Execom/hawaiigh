package eu.execom.hawaii.exceptions;

import org.springframework.security.authentication.InternalAuthenticationServiceException;

public class UnauthorizedEmailDomainException extends InternalAuthenticationServiceException {

  private static final long serialVersionUID = -6203074223117980576L;

  public UnauthorizedEmailDomainException(String message) {
    super(message);
  }
}
