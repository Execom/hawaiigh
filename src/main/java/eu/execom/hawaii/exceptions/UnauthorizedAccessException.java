package eu.execom.hawaii.exceptions;

public class UnauthorizedAccessException extends RuntimeException {

  private static final long serialVersionUID = 5117865424495555910L;

  public UnauthorizedAccessException(String message) {
    super(message);
  }
}
