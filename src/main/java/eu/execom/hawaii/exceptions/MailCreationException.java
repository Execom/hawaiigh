package eu.execom.hawaii.exceptions;

public class MailCreationException extends RuntimeException {

    private static final long serialVersionUID = -5915866626754875508L;

    public MailCreationException(String message) {
        super(message);
    }

}
