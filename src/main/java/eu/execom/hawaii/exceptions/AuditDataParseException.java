package eu.execom.hawaii.exceptions;

public class AuditDataParseException extends RuntimeException {

    private static final long serialVersionUID = 4926892170920222717L;

    public AuditDataParseException(String message) {
        super(message);
    }
}
