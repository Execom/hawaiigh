package eu.execom.hawaii.exceptions;

public class DateOutOfScopeException extends RuntimeException {

  public DateOutOfScopeException(String message) {
    super(message);
  }

}
