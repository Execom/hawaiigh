package eu.execom.hawaii.exceptions;

public class SortParameterTooLongException extends RuntimeException {

  private static final long serialVersionUID = 3279745025779020740L;

  public SortParameterTooLongException() {
    super("Sort parameter is too long.");
  }
}
