package eu.execom.hawaii.exceptions;

public class MailSendingException extends RuntimeException {

    private static final long serialVersionUID = 5419330796718546196L;

    public MailSendingException(String message) {
        super(message);
    }

}
