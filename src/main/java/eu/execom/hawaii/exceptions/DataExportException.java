package eu.execom.hawaii.exceptions;

public class DataExportException extends RuntimeException {

    private static final long serialVersionUID = -3047188940001847171L;

    public DataExportException(String message) {
        super(message);
    }

}
