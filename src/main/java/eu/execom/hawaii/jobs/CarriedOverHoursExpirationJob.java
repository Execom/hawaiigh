package eu.execom.hawaii.jobs;

import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.repository.YearRepository;
import eu.execom.hawaii.service.AllowanceService;
import eu.execom.hawaii.service.DayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CarriedOverHoursExpirationJob {

  private YearRepository yearRepository;
  private DayService dayService;
  private AllowanceService allowanceService;

  @Autowired
  public CarriedOverHoursExpirationJob(YearRepository yearRepository, DayService dayService,
      AllowanceService allowanceService) {
    this.yearRepository = yearRepository;
    this.dayService = dayService;
    this.allowanceService = allowanceService;
  }

  /*
   * Scheduled task whose responsibility is to check every evening at 23:00 if carried over hours expiration
   * date falls on that day. If it falls on given day, it runs mechanism which reduces users allowance by
   * amount of hours he has carried over from previous year and have not spent until expiration date.
   * This amount is persisted in allowance database table.
   */
  @Scheduled(cron = "0 0 23 * * *", zone = "Europe/Belgrade")
  void handleExpiredCarriedOverHours() {

    LocalDate currentDate = LocalDate.now();
    Year year = yearRepository.findOneByYear(currentDate.getYear());
    LocalDate carriedOverExpirationDate = year.getCarriedOverHoursExpirationDate();

    if (!year.isCarriedOverHoursExpirable()) {
      return;
    }

    if (currentDate.equals(carriedOverExpirationDate)) {
      reduceUsersAllowanceByExpiredCarriedOverHours(year);
    }
  }

  private void reduceUsersAllowanceByExpiredCarriedOverHours(Year year) {
    allowanceService.getAllowanceByYearAndCarriedOverGreaterThan(year)
                    .forEach(allowance -> setExpiredCarriedOverHours(year, allowance));
  }

  private void setExpiredCarriedOverHours(Year year, Allowance allowance) {

    LocalDate startDate = LocalDate.of(year.getYear(), Month.JANUARY, 1);
    LocalDate expirationDate = year.getCarriedOverHoursExpirationDate();
    List<Day> userAbsencesDays = getUsersSpentDays(allowance, startDate, expirationDate);

    int spentHoursUntilExpirationDate = allowanceService.calculateHours(userAbsencesDays);
    int usersCarriedOverHours = allowance.getCarriedOver();

    if (spentHoursUntilExpirationDate < usersCarriedOverHours) {
      int expiredCarriedOverHours = usersCarriedOverHours - spentHoursUntilExpirationDate;
      allowance.setExpiredCarriedOver(expiredCarriedOverHours);
      allowanceService.save(allowance);
    }
  }

  private List<Day> getUsersSpentDays(Allowance allowance, LocalDate startDate, LocalDate expirationDate) {
    return dayService.getUserAbsencesDays(allowance.getUser(), startDate, expirationDate)
                     .stream()
                     .filter(day -> !day.getRequest().isPending())
                     .collect(Collectors.toList());
  }

}
