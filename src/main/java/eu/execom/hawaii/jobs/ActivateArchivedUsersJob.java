package eu.execom.hawaii.jobs;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Component
public class ActivateArchivedUsersJob {

  private UserRepository userRepository;
  private UserService userService;

  @Autowired
  public ActivateArchivedUsersJob(UserRepository userRepository, UserService userService) {
    this.userRepository = userRepository;
    this.userService = userService;
  }

  /**
   * Check every day at 5:00 am to see if there is user scheduled for activation from archived users.
   */
  @Scheduled(cron = "0 0 5 * * *", zone = "Europe/Belgrade")
  @Transactional
  public void activateArchivedOrInactiveUsers() {
    LocalDate today = LocalDate.now();
    User hrAssistant = userRepository.findFirstByUserRole(UserRole.HR_ASSISTANT);

    List<User> usersToActivate = userRepository.findAllByUserStatusTypeAndStartedWorkingAtExecomDateLessThanEqual(
        UserStatusType.INACTIVE, today);

    usersToActivate.forEach(user -> userService.activate(user, hrAssistant));
  }
}
