package eu.execom.hawaii.jobs;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;

@Component
public class ArchivingUserJob {

  private UserRepository userRepository;
  private UserService userService;

  @Autowired
  public ArchivingUserJob(UserRepository userRepository, UserService userService) {
    this.userRepository = userRepository;
    this.userService = userService;
  }

  /**
   * Check every day at 6 am to see if someones last day at execom has passed and archive said user.
   */
  @Scheduled(cron = "0 0 6 * * *", zone = "Europe/Belgrade")
  void logicallyDeleteUser() {
    List<User> leavingUsers = userRepository.findAllByUserStatusTypeAndStoppedWorkingAtExecomDateIsNotNull(
        UserStatusType.ACTIVE);

    leavingUsers.stream()
                .filter(usersThatHaveLeftTheCompany()
                .and(User::hasAnyRoleOtherThanHrAssistant))
                .forEach(this::archiveAndDelete);
  }

  private Predicate<User> usersThatHaveLeftTheCompany() {
    var todaysDate = LocalDate.now();
    return user -> user.getStoppedWorkingAtExecomDate().isBefore(todaysDate);
  }

  private void archiveAndDelete(User user) {
    User hrAssistant = userRepository.findFirstByUserRole(UserRole.HR_ASSISTANT);
    user.setUserStatusType(UserStatusType.DELETED);
    userService.delete(user.getId(), hrAssistant, user.getStoppedWorkingAtExecomDate());
  }

}