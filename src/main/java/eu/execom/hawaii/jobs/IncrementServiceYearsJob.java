package eu.execom.hawaii.jobs;

import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.LeaveProfileRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.service.EmailService;
import eu.execom.hawaii.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

@Component
public class IncrementServiceYearsJob {

  private static final int UPDATE_THRESHOLD = 5;
  private static final Map<Integer, LeaveProfile> LEAVE_PROFILES = new HashMap<>();

  private LeaveProfileRepository leaveProfileRepository;
  private EmailService emailService;
  private UserRepository userRepository;
  private UserService userService;

  @Autowired
  public IncrementServiceYearsJob(LeaveProfileRepository leaveProfileRepository, EmailService emailService,
      UserRepository userRepository, UserService userService) {
    this.leaveProfileRepository = leaveProfileRepository;
    this.emailService = emailService;
    this.userRepository = userRepository;
    this.userService = userService;
  }

  @PostConstruct
  public void init() {
    List<LeaveProfile> leaveProfiles = leaveProfileRepository.findAllByUpgradeableTrueOrderByEntitlementAsc();

    for (LeaveProfile leaveProfile : leaveProfiles) {
      int mapKey = leaveProfiles.indexOf(leaveProfile) * UPDATE_THRESHOLD;

      for (int key = mapKey; key < mapKey + UPDATE_THRESHOLD; key++) {
        LEAVE_PROFILES.put(key, leaveProfile);
      }
    }
  }

  /**
   * Check every day at 6:00 am to see if years of service for given user needs to be incremented.
   * If user reaches 5, 10, 15 years and uses leave profile is upgradeable job will update leave profile, and notify
   * hr managers of this change.
   */
  @Scheduled(cron = "0 0 6 * * *", zone = "Europe/Belgrade")
  void addServiceYearsToUser() {
    List<User> users = userService.findAllByUserStatusType(Collections.singletonList(UserStatusType.ACTIVE));

    LocalDate todaysDate = LocalDate.now();

    users.stream()
         .filter(yearsOfServiceUpToDate(todaysDate)
         .and(User::hasAnyRoleOtherThanHrAssistant))
         .forEach(user -> incrementYearsOfServiceAndUpdateLeaveProfile(user, todaysDate));
  }

  private Predicate<User> yearsOfServiceUpToDate(LocalDate todaysDate) {
    return user -> Period.between(user.getStartedWorkingDate(), todaysDate).getYears() != user.getYearsOfService();
  }

  private void incrementYearsOfServiceAndUpdateLeaveProfile(User user, LocalDate todaysDate) {
    User hrAssistant = userRepository.findFirstByUserRole(UserRole.HR_ASSISTANT);
    user.setYearsOfService(Period.between(user.getStartedWorkingDate(), todaysDate).getYears());
    updateLeaveProfile(user, todaysDate);
    userService.update(user, hrAssistant);
  }

  private void updateLeaveProfile(User user, LocalDate todaysDate) {
    if (user.getLeaveProfile().isUpgradeable()) {
      var previousLeaveProfile = user.getLeaveProfile();
      user.setLeaveProfile(LEAVE_PROFILES.get(user.getYearsOfService()));
      userService.updateAllowanceForUserOnLeaveProfileUpdate(user, previousLeaveProfile, todaysDate);
      emailService.sendLeaveProfileUpdateEmail(user);
    }
  }
}
