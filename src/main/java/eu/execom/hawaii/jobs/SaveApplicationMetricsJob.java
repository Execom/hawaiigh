package eu.execom.hawaii.jobs;

import eu.execom.hawaii.model.metric.HardwareMetrics;
import eu.execom.hawaii.model.metric.HttpEndpointMetrics;
import eu.execom.hawaii.service.HardwareMetricsService;
import eu.execom.hawaii.service.HttpEndpointMetricsService;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.prometheus.client.Collector;
import io.prometheus.client.CollectorRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

@Component
public class SaveApplicationMetricsJob {

  private static final List<String> MONITORED_VALUES = Arrays.asList("system_cpu_usage", "process_cpu_usage",
      "jvm_memory_used_bytes", "http_server_requests_seconds", "http_server_requests_seconds_max");

  private PrometheusMeterRegistry prometheusMeterRegistry;
  private HardwareMetricsService hardwareMetricsService;
  private HttpEndpointMetricsService httpEndpointMetricsService;

  public SaveApplicationMetricsJob(PrometheusMeterRegistry prometheusMeterRegistry,
      HardwareMetricsService hardwareMetricsService, HttpEndpointMetricsService httpEndpointMetricsService) {
    this.prometheusMeterRegistry = prometheusMeterRegistry;
    this.hardwareMetricsService = hardwareMetricsService;
    this.httpEndpointMetricsService = httpEndpointMetricsService;
  }

  /**
   * Reads application metrics and saves it every minute.
   */
  @Scheduled(fixedRate = 60000)
  private void saveApplicationMetrics() {
    CollectorRegistry prometheusRegistry = prometheusMeterRegistry.getPrometheusRegistry();
    Enumeration<Collector.MetricFamilySamples> metricFamilySamplesEnumeration = prometheusRegistry.metricFamilySamples();

    HardwareMetrics hardwareMetrics = new HardwareMetrics();

    while (metricFamilySamplesEnumeration.hasMoreElements()) {
      Collector.MetricFamilySamples metricFamilySamples = metricFamilySamplesEnumeration.nextElement();
      String metricFamilyName = metricFamilySamples.name;

      if (!MONITORED_VALUES.contains(metricFamilyName)) {
        continue;
      }

      for (Collector.MetricFamilySamples.Sample sample : metricFamilySamples.samples) {
        String metricName = sample.name;
        double metricValue = sample.value;

        if (metricValue < 0) {
          return;
        }

        switch (metricName) {
          case "system_cpu_usage":
            hardwareMetrics.setSystemCpuUsage(metricValue);
            break;
          case "process_cpu_usage":
            hardwareMetrics.setJvmCpuUsage(metricValue);
            break;
          case "jvm_memory_used_bytes":
            hardwareMetrics.addToMemory(metricValue);
            break;
          case "http_server_requests_seconds_count":
            createHttpEndpointMetricOrUpdateNumberOfRequests(sample);
            break;
          case "http_server_requests_seconds_max":
            createHttpEndpointMetricOrUpdateMaxResponseTime(sample);
            break;
          default:
            break;
        }
      }
      hardwareMetricsService.save(hardwareMetrics);
    }
  }

  /**
   * Creates new HttpEndpointMetrics with number of requests or updates existing HttpEndpointMetric with new number
   * of requests.
   *
   * @param sample metrics entry.
   */
  private void createHttpEndpointMetricOrUpdateNumberOfRequests(Collector.MetricFamilySamples.Sample sample) {
    HttpEndpointMetrics httpEndpointMetrics = new HttpEndpointMetrics();
    int metricValue = (int) sample.value;
    httpEndpointMetrics.setNumberOfRequests(metricValue);
    setHttpAttributes(httpEndpointMetrics, sample);
    HttpEndpointMetrics existingHttpEndpointMetrics = httpEndpointMetricsService.getHttpEndpointMetricIfExists(
        httpEndpointMetrics);
    if (existingHttpEndpointMetrics != null) {
      int oldNumberOfRequests = existingHttpEndpointMetrics.getNumberOfRequests();
      int newNumberOfRequests = httpEndpointMetrics.getNumberOfRequests();
      if (oldNumberOfRequests < newNumberOfRequests) {
        existingHttpEndpointMetrics.setNumberOfRequests(newNumberOfRequests);
        httpEndpointMetricsService.save(existingHttpEndpointMetrics);
      }
    } else {
      httpEndpointMetricsService.save(httpEndpointMetrics);
    }
  }

  /**
   * Creates new HttpEndpointMetrics with max response time or updates existing HttpEndpointMetric with new max
   * response time in seconds.
   *
   * @param sample metrics entry.
   */
  private void createHttpEndpointMetricOrUpdateMaxResponseTime(Collector.MetricFamilySamples.Sample sample) {
    HttpEndpointMetrics httpEndpointMetrics = new HttpEndpointMetrics();
    double metricValue = sample.value;
    httpEndpointMetrics.setMaxResponseTime(metricValue);
    setHttpAttributes(httpEndpointMetrics, sample);
    HttpEndpointMetrics existingHttpEndpointMetrics = httpEndpointMetricsService.getHttpEndpointMetricIfExists(
        httpEndpointMetrics);
    if (existingHttpEndpointMetrics != null) {
      double oldMaxResponseTime = existingHttpEndpointMetrics.getMaxResponseTime();
      double newMaxResponseTime = httpEndpointMetrics.getMaxResponseTime();
      if (oldMaxResponseTime < newMaxResponseTime) {
        existingHttpEndpointMetrics.setMaxResponseTime(newMaxResponseTime);
        httpEndpointMetricsService.save(existingHttpEndpointMetrics);
      }
    } else {
      httpEndpointMetricsService.save(httpEndpointMetrics);
    }
  }

  /**
   * Sets HttpEndpointMetrics attributes (exception, method, outcome, status and uri) from metrics entry label names.
   *
   * @param httpEndpointMetrics HttpEndpointMetrics object.
   * @param sample metrics entry.
   */
  private void setHttpAttributes(HttpEndpointMetrics httpEndpointMetrics, Collector.MetricFamilySamples.Sample sample) {
    for (int i = 0; i < sample.labelNames.size(); ++i) {
      String attributeName = sample.labelNames.get(i);
      String attributeValue = sample.labelValues.get(i);
      switch (attributeName) {
        case "exception":
          httpEndpointMetrics.setException(attributeValue);
          break;
        case "method":
          httpEndpointMetrics.setMethod(attributeValue);
          break;
        case "outcome":
          httpEndpointMetrics.setOutcome(attributeValue);
          break;
        case "status":
          httpEndpointMetrics.setStatus(attributeValue);
          break;
        case "uri":
          httpEndpointMetrics.setEndpointUrl(attributeValue);
          break;
        default:
          break;
      }
    }
  }
}



