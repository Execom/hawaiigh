package eu.execom.hawaii.jobs;

import eu.execom.hawaii.service.HardwareMetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DeleteOldMetricsJob {

  @Value("${eu.execom.hawaii.metrics.retentionDays}")
  private int retentionDays;

  private HardwareMetricsService hardwareMetricsService;

  @Autowired
  public DeleteOldMetricsJob(HardwareMetricsService hardwareMetricsService) {
    this.hardwareMetricsService = hardwareMetricsService;
  }

  /**
   * Every day at 1:00 am deletes monitoring data older than 2 days.
   */
  @Scheduled(cron = "0 0 1 * * *", zone = "Europe/Belgrade")
  private void deleteOldMetrics() {
    hardwareMetricsService.deleteMonitoringData(retentionDays);
  }

}
