package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.UserPushToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserPushTokensRepository extends JpaRepository<UserPushToken, Long> {
  Optional<UserPushToken> findOneByUserAndPushToken(User user, String pushToken);
}
