package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.Absence;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AbsenceRepository extends JpaRepository<Absence, Long> {

  boolean existsByName(String name);

  Optional<Absence> getById(Long id);
}
