package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.metric.HttpEndpointMetrics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HttpEndpointMetricsRepository extends JpaRepository<HttpEndpointMetrics, Long> {
  HttpEndpointMetrics findByEndpointUrlAndMethodAndOutcomeAndException(String endpointUrl, String method,
      String outcome, String exception);
}
