package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.metric.HardwareMetrics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface HardwareMetricsRepository extends JpaRepository<HardwareMetrics, Long> {
  List<HardwareMetrics> findAllByTimestampBetween(LocalDateTime fromTimestamp, LocalDateTime toTimestamp);

  @Query("SELECT MIN(hardwareMetrics.timestamp) FROM HardwareMetrics hardwareMetrics")
  LocalDateTime getOldestTimestamp();

  @Query("SELECT new eu.execom.hawaii.model.metric.HardwareMetrics("
      + "AVG(hardwareMetrics.systemCpuUsage) AS systemCpuUsage, "
      + "AVG(hardwareMetrics.jvmCpuUsage) AS jvmCpuUsage, "
      + "AVG(hardwareMetrics.memoryUsage) AS memoryUsage) "
      + "FROM HardwareMetrics hardwareMetrics "
      + "WHERE hardwareMetrics.timestamp > :timestamp")
  HardwareMetrics getAverageHardwareMetricsFromTimestampUntilNow(@Param("timestamp") LocalDateTime timestamp);

  void deleteAllByTimestampLessThanEqual(LocalDateTime timestampDeletePoint);

  int countByTimestampGreaterThan(LocalDateTime timestamp);
}
