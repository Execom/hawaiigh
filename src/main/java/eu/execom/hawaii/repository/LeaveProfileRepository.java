package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.enumerations.LeaveProfileType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LeaveProfileRepository extends JpaRepository<LeaveProfile, Long> {

  Optional<LeaveProfile> getById(Long id);

  List<LeaveProfile> findAllByUpgradeableTrueOrderByEntitlementAsc();

  LeaveProfile findOneByLeaveProfileType(LeaveProfileType leaveProfileType);

  boolean existsByLeaveProfileType(LeaveProfileType leaveProfileType);
}
