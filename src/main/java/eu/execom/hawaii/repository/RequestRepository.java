package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface RequestRepository extends JpaRepository<Request, Long> {

  List<Request> findAllByUserId(Long id);

  List<Request> findAllByUserIdAndAbsenceAbsenceType(Long id, AbsenceType absenceType);

  List<Request> findAllByRequestStatus(RequestStatus requestStatus);

  List<Request> findAllByAbsenceAbsenceType(AbsenceType absenceType);

  @Query("SELECT DISTINCT req FROM Request req LEFT JOIN req.days d"
      + " WHERE req.user.id = :userId"
      + " GROUP BY req.id"
      + " ORDER BY MIN(d.date) DESC")
  List<Request> findAllRequestsByUserIdOrderByStartingDateDesc(
      @Param("userId") Long userId);

  //@formatter:off
  @Query("SELECT DISTINCT req FROM Request req LEFT JOIN FETCH req.days day WHERE (:teamId IS null OR req.user.team.id = :teamId)"
      + " AND (:absenceTypesEmpty IS true OR req.absence.absenceType IN :absenceTypes)"
      + " AND (:startDate IS null OR :endDate IS null OR day.date BETWEEN :startDate AND :endDate)"
      + " AND (req.user.userStatusType in :userStatusTypes)")
  List<Request> findAllRequestsForExport(
      @Param("userStatusTypes") List<UserStatusType> userStatusTypes,
      @Param("absenceTypes") List<AbsenceType> absenceTypes,
      @Param("teamId") Long teamId,
      @Param("startDate") LocalDate startDate,
      @Param("endDate") LocalDate endDate,
      @Param("absenceTypesEmpty") boolean absenceTypesEmpty,
      Sort sort);
  //@formatter:on

  @Query("SELECT req FROM Request req JOIN FETCH req.days WHERE req.requestStatus = 'APPROVED'")
  List<Request> findAllApprovedWithDaysEager();

  @Query("SELECT DISTINCT req FROM Request req "
      + " JOIN req.days d"
      + " JOIN req.user u"
      + " JOIN req.absence a"
      + " WHERE req.requestStatus IN :requestStatuses"
      + " AND a.absenceType IN :absenceTypes"
      + " AND u.email = :email"
      + " AND d.date IN :dates"
      + " AND (:reason IS null OR req.reason = :reason)"
      + " GROUP BY req.id HAVING MOD(SUM(CASE"
      + " WHEN d.duration = 'MORNING' OR d.duration = 'AFTERNOON' THEN 4"
      + " ELSE 8"
      + " END), 8) <> 0")
  List<Request> findAllHavingHalfDays(
      @Param("requestStatuses") List<RequestStatus> requestStatuses,
      @Param("absenceTypes") List<AbsenceType> absenceTypes,
      @Param("email") String email,
      @Param("dates") List<LocalDate> dates,
      @Param("reason") String reason
  );
}
