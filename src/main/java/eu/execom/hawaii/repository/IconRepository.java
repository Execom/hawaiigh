package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.Icon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IconRepository extends JpaRepository<Icon, Long> {
}