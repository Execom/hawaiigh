package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findByEmail(String email);

  Optional<List<User>> findAllByFullName(String fullName);

  @Query("SELECT u FROM User u WHERE u.id = :id")
  @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
  User findByIdRequireNew(@Param("id") Long id);

  @Query("SELECT user FROM User user WHERE user.userStatusType IN (:statusTypes) ORDER BY user.fullName ASC")
  List<User> findAllByUserStatusType(@Param("statusTypes") List<UserStatusType> statusTypes);

  List<User> findAllByUserStatusTypeAndUserRole(UserStatusType userStatusType, UserRole userRole);

  User findFirstByUserRole(UserRole userRole);

  List<User> findAllByUserStatusTypeAndStoppedWorkingAtExecomDateIsNotNull(UserStatusType userStatusType);

  List<User> findAllByUserStatusTypeAndStartedWorkingAtExecomDateLessThanEqual(UserStatusType userStatusType,
      LocalDate today);

  List<User> findAllByUserStatusTypeAndStartedProfessionalCareerDateLessThanEqual(UserStatusType userStatusType,
      LocalDate fiveYearsInPast);

  List<User> findAllByUserAdminPermissionInOrderByUserAdminPermissionDesc(List<UserAdminPermission> userAdminPermissions);

  List<User> findAllByUserAdminPermission(UserAdminPermission userAdminPermission);

  @Query("SELECT user FROM User user WHERE user.userStatusType IN (:statusTypes) AND "
      + "(user.email LIKE %:email% OR user.fullName LIKE %:fullName%) ORDER BY user.fullName ASC")
  List<User> searchByStatusTypeEmailAndFullName(@Param("statusTypes") List<UserStatusType> statusTypes,
      @Param("email") String email, @Param("fullName") String fullName);

  @Query("SELECT user FROM User user WHERE user.userAdminPermission = :userAdminPermission AND "
      + "user.userStatusType = :userStatusType AND "
      + "(user.email LIKE %:searchQuery% OR user.fullName LIKE %:searchQuery%) ORDER BY user.fullName ASC")
  List<User> searchByUserStatusTypeUserAdminPermissionEmailAndFullName(
      @Param("userStatusType") UserStatusType userStatusType,
      @Param("userAdminPermission") UserAdminPermission userAdminPermission,
      @Param("searchQuery") String searchQuery);

  List<User> findAllByUserStatusTypeAndTeam(UserStatusType userStatusType, Team team);

  boolean existsByEmail(String email);

  @Query("SELECT user FROM User user WHERE  user.userStatusType IN (:statusTypes)"
      + "AND (:teamId IS null OR user.team.id = :teamId)")
  List<User> getUsersForExport(
      @Param("teamId") Long teamId,
      @Param("statusTypes") List<UserStatusType> statusTypes,
      Sort sort);

  List<User> findByRequestsNotEmpty();
}
