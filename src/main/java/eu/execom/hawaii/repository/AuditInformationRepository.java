package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.AuditInformation;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuditInformationRepository extends JpaRepository<AuditInformation, Long> {

  List<AuditInformation> getAllByAuditedEntityAndModifiedEntityIdOrderByModifiedDateTimeDesc(
      AuditedEntity auditedEntity,
      Long id);

  List<AuditInformation> getAllByAuditedEntityAndModifiedEntityIdOrderByModifiedDateTimeAsc(AuditedEntity auditedEntity,
      Long id);
}
