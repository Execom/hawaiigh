package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.Team;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TeamRepository extends CustomizedTeamRepository<Team, Long> {

  boolean existsByName(String name);

  Team findByName(String name);

  Page<Team> findByNameContainingAndEditableTrue(String searchQuery, Pageable pageable);

  Page<Team> findAllByEditableTrueOrderByName(Pageable pageable);
}
