package eu.execom.hawaii.repository;

import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface AllowanceRepository extends JpaRepository<Allowance, Long> {

  Allowance findByUserIdAndYearYear(Long userId, int year);

  List<Allowance> findAll();

  List<Allowance> findAllByUserId(Long userId);

  boolean existsByUserIdAndYearYear(Long userId, int year);

  boolean existsByUserId(Long userId);

  @Query("SELECT a FROM Allowance a WHERE a.id = :id")
  @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
  Optional<Allowance> findByIdRequireNew(@Param("id") Long id);

  List<Allowance> findAllByYearAndCarriedOverGreaterThan(Year year, int greaterThan);

  List<Allowance> findAllByYearYearAndUserUserStatusTypeInOrderByUserFullName(
      int year, List<UserStatusType> statusType);

  List<Allowance> findAllByYearYearAndUserUserStatusTypeInAndUserTeamIdOrderByUserFullName(
      int year, List<UserStatusType> statusType, Long teamId);

  @Query("SELECT allowance FROM Allowance allowance "
          + "JOIN allowance.user user "
          + "JOIN allowance.year year "
          + "WHERE year.year = :year "
          + "AND user.userStatusType IN (:statusTypes) "
          + "AND (user.email LIKE %:email% OR user.fullName LIKE %:fullName%) "
          + "ORDER BY user.fullName ASC")
  List<Allowance> searchByYearAndUser(
      @Param("year") int year,
      @Param("statusTypes") List<UserStatusType> statusTypes,
      @Param("email") String email,
      @Param("fullName") String fullName);

  @Query("SELECT allowance FROM Allowance allowance "
          + "JOIN allowance.user user "
          + "JOIN allowance.year year "
          + "WHERE year.year = :year "
          + "AND user.userStatusType IN (:statusTypes) "
          + "AND (user.email LIKE %:email% OR user.fullName LIKE %:fullName%) "
          + "AND user.team.id = :teamId "
          + "ORDER BY user.fullName ASC")
  List<Allowance> searchByYearAndUserAndTeam(
      @Param("year") int year,
      @Param("statusTypes") List<UserStatusType> statusTypes,
      @Param("email") String email,
      @Param("fullName") String fullName,
      @Param("teamId") Long teamId);

  @Query("SELECT DISTINCT allowance.year.year FROM Allowance allowance "
          + "ORDER BY allowance.year.year DESC")
  List<Integer> findAllYearsWithAllowance();

  @Query("SELECT allowance FROM Allowance allowance "
      + " WHERE (:teamId IS null OR allowance.user.team.id = :teamId)"
      + " AND (allowance.year.year = :year)"
      + " AND (allowance.user.userStatusType in :userStatusTypes)")
  List<Allowance> findAllByYearTeamIdAndUserStatusType(
      @Param("year") int year,
      @Param("teamId") Long teamId,
      @Param("userStatusTypes") List<UserStatusType> userStatusTypes,
      Sort sort);

  @Query("SELECT allowance.year.year FROM Allowance allowance "
          + "WHERE allowance.user.id = :userId "
          + "ORDER BY allowance.year.year DESC")
  List<Integer> findAllYearsWithAllowanceForUser(@Param("userId") Long userId);
}
