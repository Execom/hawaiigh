package eu.execom.hawaii.restfactory;

import eu.execom.hawaii.dto.DayViewDto;
import eu.execom.hawaii.dto.UserWithDaysDto;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.service.DayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class UserWithDaysDtoFactory {

  private DayService dayService;

  @Autowired
  public UserWithDaysDtoFactory(DayService dayService) {
    this.dayService = dayService;
  }

  public UserWithDaysDto createUserWithDaysDto(User user, LocalDate startDate, LocalDate endDate) {
    var userDays = dayService.getUserAbsencesDays(user, startDate, endDate);
    UserWithDaysDto userWithDaysDto = new UserWithDaysDto(user, userDays);
    userWithDaysDto.setLeaveDays(getDeductedDays(userWithDaysDto));
    userWithDaysDto.setNonDeductedDays(getNonDeductedDays(userWithDaysDto));
    userWithDaysDto.setSickDays(getSicknessDays(userWithDaysDto));
    formatSicknessName(userWithDaysDto.getDays());

    return userWithDaysDto;
  }

  private Long getSicknessDays(UserWithDaysDto userWithDaysDto) {
    return userWithDaysDto.getDays()
                          .stream()
                          .filter(dayDto -> dayDto.getAbsenceType().equals(AbsenceType.SICKNESS))
                          .count();
  }

  private Long getDeductedDays(UserWithDaysDto userWithDaysDto) {
    return userWithDaysDto.getDays().stream().filter(DayViewDto::isDeducted).count();
  }

  private Long getNonDeductedDays(UserWithDaysDto userWithDaysDto) {
    return userWithDaysDto.getDays()
                          .stream()
                          .filter(dayDto -> dayDto.getAbsenceType().equals(AbsenceType.LEAVE) && !dayDto.isDeducted())
                          .count();
  }

  public void formatSicknessName(List<DayViewDto> days) {
    days.stream()
        .filter(dayViewDto -> dayViewDto.getAbsenceType().equals(AbsenceType.SICKNESS))
        .forEach(dayViewDto -> dayViewDto.setAbsenceName(AbsenceType.SICKNESS.getDescription()));
  }
}
