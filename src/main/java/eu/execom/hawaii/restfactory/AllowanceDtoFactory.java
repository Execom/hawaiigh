package eu.execom.hawaii.restfactory;

import eu.execom.hawaii.dto.AllowanceForUserDto;
import eu.execom.hawaii.dto.AllowanceWithoutYearDto;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.service.AllowanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class AllowanceDtoFactory {

  private AllowanceService allowanceService;

  @Autowired
  public AllowanceDtoFactory(AllowanceService allowanceService) {
    this.allowanceService = allowanceService;
  }

  public AllowanceWithoutYearDto createAllowanceWithoutYearDto(Allowance allowance) {
    AllowanceWithoutYearDto allowanceWithoutYearDto = new AllowanceWithoutYearDto(allowance);
    allowanceWithoutYearDto.setRemainingAnnual(allowanceService.calculateRemainingAnnualHours(allowance));
    allowanceWithoutYearDto.setRemainingTraining(allowanceService.calculateRemainingTrainingHours(allowance));
    allowanceWithoutYearDto.setTotalAnnual(allowanceService.calculateTotalAnnualHours(allowance));
    allowanceWithoutYearDto.setNextYearAllowance(allowanceService.getRemainingHoursNextYear(allowance));
    allowanceWithoutYearDto.setRemainingBonus(allowanceService.calculateRemainingBonusHours(allowance));
    allowanceWithoutYearDto.setTotalBonus(calculateTotalBonus(allowance));
    allowanceWithoutYearDto.setTotalTraining(calculateTotalTraining(allowance));

    return allowanceWithoutYearDto;
  }

  private int calculateTotalBonus(Allowance allowance) {
    return allowance.getApprovedBonus() + allowance.getBonusManualAdjust();
  }

  private int calculateTotalTraining(Allowance allowance) {
    return allowance.getTraining() + allowance.getTrainingManualAdjust();
  }

  public AllowanceForUserDto getAllowancesForUser(User user) {
    var yearOfRequest = LocalDate.now().getYear();

    var currentYearAllowance = allowanceService.getByUserAndYear(user.getId(), yearOfRequest);
    var remainingAnnualHours = allowanceService.calculateRemainingAnnualHours(currentYearAllowance);
    var remainingTrainingHours = allowanceService.calculateRemainingTrainingHours(currentYearAllowance);
    var nextYearRemainingAnnualHours = allowanceService.nextYearAllowanceExists(currentYearAllowance) ?
        allowanceService.calculateNextYearRemainingAnnualHours(currentYearAllowance) :
        0;

    return AllowanceForUserDto.createNew(remainingAnnualHours, nextYearRemainingAnnualHours, remainingTrainingHours);
  }

}
