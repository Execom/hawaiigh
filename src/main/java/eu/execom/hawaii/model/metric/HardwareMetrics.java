package eu.execom.hawaii.model.metric;

import eu.execom.hawaii.model.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class HardwareMetrics extends BaseEntity {

  private static final int MEGA_BYTES = 1_048_576;

  public HardwareMetrics(double systemCpuUsage, double jvmCpuUsage, double memoryUsage) {
    this.systemCpuUsage = systemCpuUsage;
    this.jvmCpuUsage = jvmCpuUsage;
    this.memoryUsage = memoryUsage;
  }

  @NotNull
  private double memoryUsage = 0;

  @NotNull
  private double systemCpuUsage;

  @NotNull
  private double jvmCpuUsage;

  @CreationTimestamp
  private LocalDateTime timestamp;

  public void addToMemory(double memory) {
    double memoryInMb = memory / MEGA_BYTES;
    this.setMemoryUsage(Math.round((this.memoryUsage + memoryInMb) * 100) / 100.0);
  }
}
