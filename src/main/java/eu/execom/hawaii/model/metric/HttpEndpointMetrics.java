package eu.execom.hawaii.model.metric;

import eu.execom.hawaii.model.BaseEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
public class HttpEndpointMetrics extends BaseEntity {

  private static final String NO_EXCEPTION = "None";

  @NotNull
  private String endpointUrl;

  private String status;

  private String method;

  private String outcome;

  private String exception;

  @NotNull
  private int numberOfRequests;

  @NotNull
  private double maxResponseTime;

  private LocalDateTime lastFailAlarmTimestamp;

  public boolean isExceptionRaised() {
    return !NO_EXCEPTION.equals(this.exception);
  }
}
