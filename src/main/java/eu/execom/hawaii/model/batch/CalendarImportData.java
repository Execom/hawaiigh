package eu.execom.hawaii.model.batch;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CalendarImportData {

  private String eventTitle;
  private String eventDescription;
  private String reason;
  private LocalDateTime eventStart;
  private LocalDateTime eventEnd;
  private LocalDateTime dateCreated;
  private boolean allDayEvent;
}