package eu.execom.hawaii.model.batch;

import lombok.Data;

import java.time.LocalDate;

@Data
public class AppogeeLeaveRequestImportData {
  String name;
  String email;
  String parentTeam;
  String team;
  String employeeStatus;
  String payrollNo;
  LocalDate dateSubmitted;
  String type;
  String status;
  LocalDate startDate;
  LocalDate endDate;
  float hours;
  float computedHours;
  float days;
  float hoursInRange;
  float computedHoursInRange;
  float daysInRange;
  String reason;
  String leaveYear;
  String createdViaBackfill;
  String approvedBy;
  LocalDate dateApproved;
  String approverComments;
  String cancellationReason;
  String cancellationApproverComments;
}
