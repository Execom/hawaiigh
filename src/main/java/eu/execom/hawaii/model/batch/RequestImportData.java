package eu.execom.hawaii.model.batch;

import eu.execom.hawaii.model.Absence;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
public class RequestImportData {

  private User user;
  private Absence absence;
  private RequestStatus requestStatus;
  private String reason;
  private String approverComment;
  private String cancellationReason;
  private String approverCancellationComment;
  private LocalDateTime submissionTime;
  private int hours;
  private LocalDate startDate;
  private LocalDate endDate;

  public RequestImportData(Request request) {
    this.user = request.getUser();
    this.absence = request.getAbsence();
    this.requestStatus = request.getRequestStatus();
    this.reason = request.getReason();
    this.approverComment = request.getApproverComment();
    this.cancellationReason = request.getCancellationReason();
    this.approverCancellationComment = request.getApproverCancellationComment();
    this.submissionTime = request.getSubmissionTime();
    this.hours = request.getDays()
                        .stream()
                        .mapToInt(day -> day.getDuration().getHours())
                        .sum();
    this.startDate = request.getDays().get(0).getDate();
    this.endDate = request.getDays().get(request.getDays().size() - 1).getDate();
  }
}
