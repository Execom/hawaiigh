package eu.execom.hawaii.model.batch;

import lombok.Data;

@Data
public class AllowanceImportData {

  private String employeeName;
  private String email;
  private String parentTeam;
  private String team;
  private String payrollNo;
  private String allowanceName;
  private double leaveAllocatedHours;
  private double leaveAccruedHours;
  private double leaveCarryOverHours;
  private double leaveAdjustHours;
  private double leaveToilComputedHours;
  private double leaveToilAdjustHours;
  private double leaveToilTotalHours;
  private double leavePendingHours;
  private double leaveApprovedHours;
  private double leaveRemaining;
  private double leaveRemainingIncPending;
  private double sickAllocatedHours;
  private double sickAccruedHours;
  private double sickCarryOverHours;
  private double sickAdjustHours;
  private double sickPendingHours;
  private double sickApprovedHours;
  private double sickRemaining;
  private double sickRemainingIncPending;
  private double custom1AllocatedHours;
  private double custom1AccruedHours;
  private double custom1CarryOverHours;
  private double custom1AdjustHours;
  private double custom1PendingHours;
  private double custom1TakenHours;

}
