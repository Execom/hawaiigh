package eu.execom.hawaii.model.audit;

import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class TeamAudit extends Audit {

  private String teamName;
  private List<List<String>> approversInfo;

  private TeamAudit(Team team) {
    this.teamName = team.getName();
    this.approversInfo = team.getTeamApprovers().stream().map(getUsersNameAndEmail()).collect(Collectors.toList());
    this.setAuditedEntity(AuditedEntity.TEAM);
  }

  private Function<User, List<String>> getUsersNameAndEmail() {
    return approver -> List.of(approver.getEmail(), approver.getFullName());
  }

  public static TeamAudit fromTeam(Team team) {
    return new TeamAudit(team);
  }
}
