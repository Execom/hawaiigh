package eu.execom.hawaii.model.audit;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class UserAudit extends Audit {

  private Long teamId;
  private Long leaveProfileId;
  private String fullName;
  private String email;
  private UserRole userRole;
  private String jobTitle;
  private int yearsOfService;
  private UserStatusType userStatusType;
  private LocalDate startedWorkingDate;
  private LocalDate startedProfessionalCareerDate;
  private LocalDate startedWorkingAtExecomDate;
  private LocalDate stoppedWorkingAtExecomDate;
  private String imageUrl;

  private UserAudit(User user) {
    this.teamId = user.getTeam().getId();
    this.leaveProfileId = user.getLeaveProfile().getId();
    this.fullName = user.getFullName();
    this.email = user.getEmail();
    this.userRole = user.getUserRole();
    this.jobTitle = user.getJobTitle();
    this.yearsOfService = user.getYearsOfService();
    this.userStatusType = user.getUserStatusType();
    this.startedWorkingDate = user.getStartedWorkingDate();
    this.startedProfessionalCareerDate = user.getStartedProfessionalCareerDate();
    this.startedWorkingAtExecomDate = user.getStartedWorkingAtExecomDate();
    this.stoppedWorkingAtExecomDate = user.getStoppedWorkingAtExecomDate();
    this.imageUrl = user.getImageUrl();
    this.setAuditedEntity(AuditedEntity.USER);
  }

  public static UserAudit fromUser(User user) {
    return new UserAudit(user);
  }
}
