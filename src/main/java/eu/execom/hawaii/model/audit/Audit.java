package eu.execom.hawaii.model.audit;

import eu.execom.hawaii.exceptions.FieldNotAccessibleException;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Data
@MappedSuperclass
public abstract class Audit {

  private AuditedEntity auditedEntity;

  public List<AuditedChanges> compareFields(Audit audit) {
    List<AuditedChanges> changeList = new ArrayList<>();
    Field[] fields = this.getClass().getDeclaredFields();

    for (Field field : fields) {
      field.setAccessible(true);

      try {
        boolean isEmployeeDeletionCanceled = "stoppedWorkingAtExecomDate".equals(field.getName())
                && field.get(this) != null
                && field.get(audit) == null;
        boolean isFieldUpdated = field.get(audit) != null && !field.get(audit).equals(field.get(this));
        if (isFieldUpdated || isEmployeeDeletionCanceled) {
          changeList.add(getAuditedChanges(audit, field));
        }
      } catch (IllegalAccessException e) {
        throw new FieldNotAccessibleException(
            "Field accessor does not have access for " + this.getClass());
      }
    }

    return changeList;
  }

  private AuditedChanges getAuditedChanges(Audit audit, Field field) throws IllegalAccessException {
    AuditedChanges change = new AuditedChanges();
    change.setFieldName(field.getName());
    change.setOldObjectValue(field.get(this) != null ? field.get(this).toString() : "");
    String newValue = field.get(audit) == null ? "" : field.get(audit).toString();
    change.setNewObjectValue(newValue);

    return change;
  }
}
