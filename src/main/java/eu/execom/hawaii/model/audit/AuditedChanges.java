package eu.execom.hawaii.model.audit;

import lombok.Data;

@Data
public class AuditedChanges {

  private String fieldName;
  private String oldObjectValue;
  private String newObjectValue;

}