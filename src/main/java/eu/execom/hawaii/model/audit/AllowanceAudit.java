package eu.execom.hawaii.model.audit;

import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class AllowanceAudit extends Audit {

  private String comment;
  private int manualAdjust;
  private int trainingManualAdjust;
  private int bonusManualAdjust;
  private int carriedOver;

  private AllowanceAudit(Allowance allowance) {
    this.manualAdjust = allowance.getManualAdjust();
    this.trainingManualAdjust = allowance.getTrainingManualAdjust();
    this.bonusManualAdjust = allowance.getBonusManualAdjust();
    this.carriedOver = allowance.getCarriedOver();
    this.comment = allowance.getComment();
    this.setAuditedEntity(AuditedEntity.ALLOWANCE);
  }

  public static AllowanceAudit fromAllowance(Allowance allowance) {
    return new AllowanceAudit(allowance);
  }
}
