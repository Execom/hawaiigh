package eu.execom.hawaii.model.audit;

import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.AuditedEntity;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class RequestAudit extends Audit {

  private Long id;
  private RequestStatus requestStatus;
  private LocalDateTime modifiedDateTime;
  private String requestHandlerEmail;
  private String requestHandlerFullName;
  private String comment;
  private String reason;
  private String approverComment;
  private String cancellationReason;
  private String approverCancellationComment;

  private RequestAudit(Request request, User modifiedByUser) {
    this.id = request.getId();
    this.requestStatus = request.getRequestStatus();
    this.modifiedDateTime = LocalDateTime.now();
    this.requestHandlerEmail = modifiedByUser.getEmail();
    this.requestHandlerFullName = modifiedByUser.getFullName();
    this.reason = request.getReason();
    this.approverComment = request.getApproverComment();
    this.cancellationReason = request.getCancellationReason();
    this.approverCancellationComment = request.getApproverCancellationComment();
    this.setAuditedEntity(AuditedEntity.REQUEST);
  }

  public static RequestAudit fromRequest(Request request, User modifiedByUser) {
    return new RequestAudit(request, modifiedByUser);
  }
}
