package eu.execom.hawaii.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Icon extends BaseEntity implements Serializable {

  private static final long serialVersionUID = -6626180538017782539L;

  @NotNull
  @Column(unique = true)
  String iconUrl;

  @OneToMany(mappedBy = "icon", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  private List<Absence> absence;
}
