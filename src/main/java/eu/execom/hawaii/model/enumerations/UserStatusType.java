package eu.execom.hawaii.model.enumerations;

import lombok.Getter;

public enum UserStatusType {
  //@formatter:off
  ACTIVE("active"),
  INACTIVE("inactive"),
  DELETED("deleted");
  //@formatter:on

  @Getter
  private String description;

  UserStatusType(String description) {
    this.description = description;
  }

}
