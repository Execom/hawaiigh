package eu.execom.hawaii.model.enumerations;

public enum UserRole {
  //@formatter:off
  HR_ASSISTANT,
  HR_MANAGER,
  OFFICE_MANAGER,
  USER
  //@formatter:on

}
