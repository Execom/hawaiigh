package eu.execom.hawaii.model.enumerations;

import lombok.Getter;

import java.util.List;

public enum RequestStatus {
  //@formatter:off
  PENDING("pending"),
  APPROVED("approved"),
  REJECTED("rejected"),
  CANCELED("canceled"),
  CANCELLATION_PENDING("cancellation pending");
  //@formatter:on

  @Getter
  private String description;

  RequestStatus(String description) {
    this.description = description;
  }

  public static List<RequestStatus> nonApprovedRequests() {
    return List.of(PENDING, REJECTED, CANCELED, CANCELLATION_PENDING);
  }
}
