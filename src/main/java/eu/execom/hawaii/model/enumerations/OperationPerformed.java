package eu.execom.hawaii.model.enumerations;

public enum OperationPerformed {
  //@formatter:off
  CREATE,
  REMOVE,
  UPDATE,
  DELETE,
  ACTIVATE
  //@formatter:on
}
