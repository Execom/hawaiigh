package eu.execom.hawaii.model.enumerations;

public enum AuditedEntity {
  //@formatter:off
  REQUEST,
  USER,
  TEAM,
  ALLOWANCE
  //@formatter:on
}
