package eu.execom.hawaii.model.enumerations;

import lombok.Getter;

public enum Duration {
  //@formatter:off
  FULL_DAY(8),
  MORNING(4),
  AFTERNOON(4);
  //@formatter:on

  @Getter
  private final int hours;

  Duration(int hours) {
    this.hours = hours;
  }
}
