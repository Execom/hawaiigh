package eu.execom.hawaii.model.enumerations;

import lombok.Getter;

import java.util.Collections;
import java.util.List;

public enum AbsenceType {

  //@formatter:off
  LEAVE("Leave"),
  TRAINING("Training"),
  BONUS("Bonus"),
  SICKNESS("Sickness");
  //@formatter:on

  @Getter
  private String description;

  AbsenceType(String description) {
    this.description = description;
  }

  public static List<AbsenceType> from(String absenceName) {
    switch (absenceName) {
      case "Training & Education":
        return Collections.singletonList(TRAINING);
      case "Bonus Days":
        return Collections.singletonList(BONUS);
      case "":
        return List.of(values());
      default:
        return Collections.singletonList(LEAVE);
    }
  }
}
