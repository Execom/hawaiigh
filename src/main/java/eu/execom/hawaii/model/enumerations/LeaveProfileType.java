package eu.execom.hawaii.model.enumerations;

import lombok.Getter;

public enum LeaveProfileType {
  //@formatter:off
  EMPLOYEES_3_DAYS_WORKING("Employees 3 days working"),
  EMPLOYEES_4_DAYS_WORKING("Employees 4 days working"),
  ZERO_TO_FIVE_YEARS("0 - 5 years"),
  FIVE_TO_TEN_YEARS("5 - 10 years"),
  TEN_TO_FIFTEEN_YEARS("10 - 15 years"),
  FIFTEEN_TO_TWENTY_YEARS("15 - 20 years"),
  TWENTY_TO_TWENTYFIVE_YEARS("20 - 25 years"),
  TWENTYFIVE_TO_THIRTY_YEARS("25 - 30 years"),
  THIRTY_TO_THIRTYFIVE_YEARS("30 - 35 years"),
  THIRTYFIVE_TO_FORTY_YEARS("35 - 40 years"),
  CUSTOM("Custom");
  //@formatter:on

  @Getter
  private String description;

  LeaveProfileType(String description) {
    this.description = description;
  }

}

