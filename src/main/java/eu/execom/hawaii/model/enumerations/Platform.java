package eu.execom.hawaii.model.enumerations;

public enum Platform {
  //@formatter:off
  IOS,
  ANDROID
  //@formatter:on
}
