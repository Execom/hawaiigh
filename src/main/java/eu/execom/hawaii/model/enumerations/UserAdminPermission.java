package eu.execom.hawaii.model.enumerations;

public enum UserAdminPermission {
  NONE,
  BASIC,
  SUPER
}
