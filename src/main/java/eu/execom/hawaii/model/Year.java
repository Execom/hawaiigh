package eu.execom.hawaii.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@ToString(exclude = "allowances")
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class Year extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 7347843784040392307L;

  @NotNull
  private boolean active;

  @NotNull
  @Column(unique = true)
  private int year;

  private boolean carriedOverHoursExpirable;

  private LocalDate carriedOverHoursExpirationDate;

  @OneToMany(mappedBy = "year", fetch = FetchType.EAGER)
  private List<Allowance> allowances;

  public Year(int year, boolean active, LocalDate carriedOverHoursExpirationDate) {
    this.year = year;
    this.active = active;
    this.carriedOverHoursExpirationDate = carriedOverHoursExpirationDate;
    this.carriedOverHoursExpirable = false;
  }

}
