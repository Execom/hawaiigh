package eu.execom.hawaii.model;

import eu.execom.hawaii.model.enumerations.AbsenceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@EqualsAndHashCode(callSuper = false, exclude = {"icon", "leaveRequests"})
@ToString(exclude = {"icon", "leaveRequests"})
@Entity
@Data
public class Absence extends BaseEntity implements Serializable {

  private static final long serialVersionUID = -7375197378676775460L;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(updatable = false)
  private AbsenceType absenceType;

  @NotNull
  @Column(unique = true)
  private String name;

  private String comment;

  @NotNull
  private boolean deducted;

  @NotNull
  @ManyToOne
  private Icon icon;

  @OneToMany(mappedBy = "absence")
  private List<Request> leaveRequests;

  public boolean isBonusDays() {
    return AbsenceType.BONUS.equals(absenceType);
  }

  public boolean isLeave() {
    return AbsenceType.LEAVE.equals(absenceType);
  }

  public boolean isTraining() {
    return AbsenceType.TRAINING.equals(absenceType);
  }

  public boolean isSickness() {
    return AbsenceType.SICKNESS.equals(absenceType);
  }

}