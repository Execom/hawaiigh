package eu.execom.hawaii.model;

import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@ToString(exclude = {"approverTeams", "currentlyApprovedBy", "requests", "allowances", "userPushTokens",
    "modifierAuditInformation"})
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class User extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 950754391551134726L;

  @ManyToOne
  private Team team;

  @ManyToOne
  private LeaveProfile leaveProfile;

  @NotNull
  private String fullName;

  @NotNull
  @Email
  @Column(unique = true)
  @EqualsAndHashCode.Include
  private String email;

  @NotNull
  @Enumerated(EnumType.STRING)
  private UserRole userRole;

  private UserAdminPermission userAdminPermission = UserAdminPermission.NONE;

  private String jobTitle;

  @NotNull
  @Enumerated(EnumType.STRING)
  private UserStatusType userStatusType;

  @NotNull
  private LocalDate startedWorkingDate;

  private LocalDate startedProfessionalCareerDate;

  @NotNull
  private LocalDate startedWorkingAtExecomDate;

  private LocalDate stoppedWorkingAtExecomDate;

  private int yearsOfService;

  private String imageUrl;

  @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
  private List<UserPushToken> userPushTokens;

  @LazyCollection(LazyCollectionOption.FALSE)
  @ManyToMany(mappedBy = "teamApprovers")
  private List<Team> approverTeams;

  @OneToMany(mappedBy = "user")
  private List<Request> requests;

  @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  private List<Allowance> allowances;

  @OneToMany(mappedBy = "modifiedByUser")
  private List<AuditInformation> modifierAuditInformation;

  @ManyToMany(mappedBy = "currentlyApprovedBy", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  private List<Request> currentlyApprovedBy;

  public boolean isApprover() {
    switch (userRole) {
      case HR_MANAGER:
        return true;
      case USER:
        return !approverTeams.isEmpty();
      case OFFICE_MANAGER:
        return !approverTeams.isEmpty();
      default:
        return false;
    }
  }

  public boolean hasRoleHrManager() {
    return UserRole.HR_MANAGER.equals(userRole);
  }

  public boolean hasAnyRoleOtherThanHrAssistant() {
    return !UserRole.HR_ASSISTANT.equals(userRole);
  }

  public boolean isDeleted() {
    return UserStatusType.DELETED.equals(userStatusType);
  }

  public List<User> getApprovers() {
    return this.team.getTeamApprovers();
  }

  public boolean isActive() {
    return UserStatusType.ACTIVE.equals(userStatusType);
  }

}