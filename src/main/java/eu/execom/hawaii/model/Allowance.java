package eu.execom.hawaii.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * All integer field are represented in hour, beside year field.
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class Allowance extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 4288980454836980764L;

  @ManyToOne
  @EqualsAndHashCode.Include
  private User user;

  @ManyToOne
  @EqualsAndHashCode.Include
  private Year year;

  private int annual;

  private int pendingAnnual;

  private int takenAnnual;

  private int sickness;

  private int training;

  private int pendingTraining;

  private int takenTraining;

  private int bonus;

  private int pendingBonus;

  private int approvedBonus;

  private int bonusManualAdjust;

  private int pendingInPreviousYear;

  private int takenInPreviousYear;

  private int relevantWorkExperienceBonus;

  private int manualAdjust;

  private int trainingManualAdjust;

  private int carriedOver;

  private int expiredCarriedOver;

  private String comment;

  public Allowance(User user, Year year, int annual, int training, int bonus) {
    this.user = user;
    this.year = year;
    this.annual = annual;
    this.training = training;
    this.bonus = bonus;
  }
}
