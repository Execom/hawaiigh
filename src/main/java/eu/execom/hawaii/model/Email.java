package eu.execom.hawaii.model;

import java.util.List;
import java.util.Map;

import eu.execom.hawaii.util.EmailData;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Email {

  private List<String> to;
  private EmailData emailData;
  private Map<String, Object> templateData;

}
