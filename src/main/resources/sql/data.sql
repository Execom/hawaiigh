LOCK TABLES `icon` WRITE;
/*!40000 ALTER TABLE `icon` DISABLE KEYS */;
INSERT INTO `icon` VALUES
('1', 'icons/business-travel.svg'),
('2', 'icons/maternity-leave.svg'),
('3', 'icons/national-sport.svg'),
('4', 'icons/own-wedding.svg'),
('5', 'icons/sick-leave.svg'),
('6', 'icons/training-education.svg'),
('7', 'icons/vacation.svg'),
('8', 'icons/work-from-home.svg'),
('9', 'icons/court-invitation.svg'),
('10', 'icons/family-member-wedding.svg'),
('11', 'icons/family-saint-day.svg'),
('12', 'icons/funeral-member.svg'),
('13', 'icons/military-invitation.svg'),
('14', 'icons/serious-illness.svg'),
('15', 'icons/delivery.svg'),
('16', 'icons/moving.svg');
/*!40000 ALTER TABLE `icon` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `absence` WRITE;
/*!40000 ALTER TABLE `absence` DISABLE KEYS */;
INSERT INTO `absence` VALUES
(1,'Annual leave - Vacation','LEAVE','string',7,b'1'),
(2,'Training & Education','TRAINING','string',6,b'1'),
(3,'Bonus Days','BONUS','string',7,b'1'),
(4,'Asthma related','SICKNESS','string',5,b'0'),
(5,'Chest related','SICKNESS','string',5,b'0'),
(6,'Cold / Influenza','SICKNESS','string',5,b'0'),
(7,'Contagious disease - please give further information','SICKNESS','string',5,b'0'),
(8,'Dental Problems','SICKNESS','string',5,b'0'),
(9,'Depression or Stress related','SICKNESS','string',5,b'0'),
(10,'Diabetes related','SICKNESS','string',5,b'0'),
(11,'Eye Problems','SICKNESS','string',5,b'0'),
(12,'Fainting / Dizziness','SICKNESS','string',5,b'0'),
(13,'Fever','SICKNESS','string',5,b'0'),
(14,'Food Poisoning','SICKNESS','string',5,b'0'),
(15,'Gynaecology Related','SICKNESS','string',5,b'0'),
(16,'Heart Related','SICKNESS','string',5,b'0'),
(17,'Kidney Related','SICKNESS','string',5,b'0'),
(18,'Maternity related','SICKNESS','string',5,b'0'),
(19,'Medical Appointment','SICKNESS','string',5,b'0'),
(20,'Migraine / Headache','SICKNESS','string',5,b'0'),
(21,'Muscular pain or limb injury','SICKNESS','string',5,b'0'),
(22,'Neurological Related','SICKNESS','string',5,b'0'),
(23,'Other - Please give further information','SICKNESS','string',5,b'0'),
(24,'Pregnancy Related','SICKNESS','string',5,b'0'),
(25,'Road Traffic Accident','SICKNESS','string',5,b'0'),
(26,'Sickness / Nausea','SICKNESS','string',5,b'0'),
(27,'Stomach / digestion related','SICKNESS','string',5,b'0'),
(28,'Business travel','LEAVE','string',1,b'0'),
(29,'Own wedding','LEAVE','string',4,b'0'),
(30,'Maternity Leave','LEAVE','string',2,b'0'),
(31,'Work from home','LEAVE','string',8,b'0'),
(32,'National sport competition','LEAVE','string',3,b'0'),
(33,'Court invitation','LEAVE','string',9,b'0'),
(34,'Delivery or adoption','LEAVE','string',16,b'0'),
(35,'Family Member Wedding','LEAVE','string',10,b'0'),
(36,'Family Saint day','LEAVE','string',11,b'0'),
(37,'Funeral family member','LEAVE','string',12,b'0'),
(38,'Military invitation','LEAVE','string',13,b'0'),
(39,'Moving','LEAVE','string',16,b'0'),
(40,'Serious illness of close family member','LEAVE','string',14,b'0');

/*!40000 ALTER TABLE `absence` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `leave_profile` WRITE;
/*!40000 ALTER TABLE `leave_profile` DISABLE KEYS */;
INSERT INTO `leave_profile`  VALUES
(1,'Custom Profile','CUSTOM','non-upgradable',160,16,40,40,40,40,b'0'),
(2,'0 - 5 Years of Service','ZERO_TO_FIVE_YEARS','Upgradable',160,16,40,40,40,40,b'1'),
(3,'5 - 10 Years of Service','FIVE_TO_TEN_YEARS','Increase yearly allowance by 1 day',168,16,40,40,40,40,b'1'),
(4,'10 - 15 Years of Service','TEN_TO_FIFTEEN_YEARS','Increase yearly allowance by 2 days',176,16,40,40,40,40,b'1'),
(5,'15 - 20 Years of Service','FIFTEEN_TO_TWENTY_YEARS','Increase yearly allowance by 3 days',184,16,40,40,40,40,b'1'),
(6,'20 - 25 Years of Service','TWENTY_TO_TWENTYFIVE_YEARS','Increase yearly allowance by 4 days',192,16,40,40,40,40,b'1'),
(7,'25 - 30 Years of Service','TWENTYFIVE_TO_THIRTY_YEARS','Increase yearly allowance by 5 days',200,16,40,40,40,40,b'1'),
(8,'30 - 35 Years of Service','THIRTY_TO_THIRTYFIVE_YEARS','Increase yearly allowance by 6 days',208,16,40,40,40,40,b'1'),
(9,'35 - 40 Years of Service','THIRTYFIVE_TO_FORTY_YEARS','Increase yearly allowance by 7 days',216,16,40,40,40,40,b'1'),
(10,'3 days working','EMPLOYEES_3_DAYS_WORKING','Non-Upgradable',96,16,40,40,40,40,b'0'),
(11,'4 days working','EMPLOYEES_4_DAYS_WORKING','Non-Upgradable',128,16,40,40,40,40,b'0');
/*!40000 ALTER TABLE `leave_profile` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES
(1,'Archived Users',b'0',b'0',b'0','notifier@execom.eu','notifier@execom.eu','notifier@execom.eu',b'0'),
(2,'HR Assistent',b'0',b'0',b'0','notifier@execom.eu','notifier@execom.eu','notifier@execom.eu',b'0');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `user` WRITE;
INSERT INTO `user`
(id, full_name,email,user_status_type, job_title, user_role, years_of_service, started_working_date, started_working_at_execom_date, leave_profile_id, team_id) VALUES
(1,'HR Assistent AI','hrassistent@execom.eu','ACTIVE','HR assistent','HR_ASSISTANT',0,'1997-01-22','1997-01-22',1,1);
UNLOCK TABLES;

LOCK TABLES `public_holiday` WRITE;
/*!40000 ALTER TABLE `public_holiday` DISABLE KEYS */;
INSERT INTO `public_holiday`
(name, date, deleted) VALUES
('New Year','2015-01-01',b'0'),
('New Year','2015-01-02',b'0'),
('Ortodox Christmas','2015-01-07',b'0'),
('Serbian Statehood Day 1','2015-02-16',b'0'),
('Serbian Statehood Day 2','2015-02-17',b'0'),
('Ortodox Good Friday','2015-04-10',b'0'),
('Ortodox Ester Monday','2015-04-13',b'0'),
('Laybor Day 1','2015-05-01',b'0'),
('Armistice Day in WWI','2015-11-11',b'0'),
('New Year','2016-01-01',b'0'),
('Ortodox Christmas','2016-01-07',b'0'),
('Serbian Statehood Day 1','2016-02-15',b'0'),
('Serbian Statehood Day 2','2016-02-16',b'0'),
('Ortodox Good Friday','2016-04-29',b'0'),
('Ortodox Ester Monday','2016-05-02',b'0'),
('Laybor Day','2016-05-03',b'0'),
('Armistice Day in WWI','2016-11-11',b'0'),
('New Year','2017-01-02',b'0'),
('New Year','2017-01-03',b'0'),
('Serbian Statehood Day 1','2017-02-15',b'0'),
('Serbian Statehood Day 2','2017-02-16',b'0'),
('Ortodox Good Friday','2017-04-14',b'0'),
('Ortodox Ester Monday','2017-04-17',b'0'),
('Laybor Day 1','2017-05-01',b'0'),
('Laybor Day 2','2017-05-02',b'0'),
('Armistice Day in WWI','2017-11-11',b'0'),
('New Year','2018-01-01',b'0'),
('New Year','2018-01-02',b'0'),
('Serbian Statehood Day 1','2018-02-15',b'0'),
('Serbian Statehood Day 2','2018-02-16',b'0'),
('Ortodox Good Friday','2018-04-06',b'0'),
('Ortodox Ester Monday','2018-04-09',b'0'),
('May 1','2018-05-01',b'0'),
('May 2','2018-05-02',b'0'),
('Armistice Day in WWI','2018-11-12',b'0'),
('New Year','2019-01-01',b'0'),
('New Year','2019-01-02',b'0'),
('Ortodox Christmas','2019-01-07',b'0'),
('Serbian Statehood Day 1','2019-02-15',b'0'),
('Ortodox Good Friday','2019-04-26',b'0'),
('Ortodox Ester Monday','2019-04-29',b'0'),
('May 1','2019-05-01',b'0'),
('May 2','2019-05-02',b'0'),
('Armistice Day in WWI','2019-11-11',b'0'),
('New Year','2020-01-01',b'0'),
('New Year','2020-01-02',b'0'),
('Ortodox Christmas','2020-01-07',b'0');
/*!40000 ALTER TABLE `public_holiday` ENABLE KEYS */;
UNLOCK TABLES;
