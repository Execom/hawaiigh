<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
      <#include "emailStyle.css">
    </style>
</head>
<body>
<p>Request from calendar for user '${userEmail}' for period from '${startDate}' to '${endDate}' not found in database
as a request that includes half days based on Appogee data.
</p>
<p>Please <a href="${serverName}">login to the Hawaii system</a> to review/change imported requests.</p>
<hr>
<p class="footer">This is automatic notification email from the Hawaii application. Please do not reply to this email.</p>
</body>
</html>