<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
      <#include "emailStyle.css">
    </style>
</head>
<body>
<p>Request from calendar for user '${userEmail}' for period from '${startDate}' to '${endDate}' not uniquely identifiable.
There are multiple requests in the database based on Appogee data, so it is not clear which one to update in this case.
</p>
<p>Please <a href="${serverName}">login to the Hawaii system</a> to review/change imported requests.</p>
<hr>
<p class="footer">This is automatic notification email from the Hawaii application. Please do not reply to this email.</p>
</body>
</html>