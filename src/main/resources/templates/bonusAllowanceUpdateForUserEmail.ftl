<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
      <#include "emailStyle.css">
    </style>
</head>
<body>
<p>You have been awarded a bonus day based on relevant work experience.</p>
<p><a href="${serverName}">Login to the Hawaii system</a> to see this update.</p>
<hr>
<p class="footer">This is automatic notification email from the Hawaii application. Please do not reply to this email.</p>
</body>
</html>