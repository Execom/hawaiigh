<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        <#include "emailStyle.css">
    </style>
</head>
<body>
<p class="alert">High resource usage.</p>
<p>Please <a href="${serverName}/admin-dashboard">login to the Hawaii system</a> to review resource usage.</p>
<table>
    <caption>Hawaii application resource usage</caption>
    <tbody>
    <tr>
        <td class="left">System CPU usage</td>
        <td class="right">${systemCpuUsage}%</td>
    </tr>
    <tr>
        <td class="left">JVM CPU usage</td>
        <td class="right">${jvmCpuUsage}%</td>
    </tr>
    <tr>
        <td class="left">Memory usage</td>
        <td class="right">${memoryUsage} MB</td>
    </tr>
    </tbody>
</table>
<br>
<br>
<hr>
<p class="footer">This is a notification email from the Hawaii HR system. Please do not reply directly to this
    email.</p>
</body>
</html>