<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
      <#include "emailStyle.css">
    </style>
</head>
<body>
<p>User ${userName} was upgraded to higher Leave Profile.</p>
<p>Please <a href="${serverName}">login to the Hawaii system</a> to review/change this update.</p>
<hr>
<p class="footer">This is automatic notification email from the Hawaii application. Please do net reply to this email.</p>
</body>
</html>