<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        <#include "emailStyle.css">
    </style>
</head>
<body>
<p class="alert">Http Endpoint '${endpointUrl}' failed.</p>
<p>Please <a href="${serverName}/admin-dashboard">login to the Hawaii system</a> to review warning.</p>
<br>
<p>HTTP endpoint <b>'${endpointUrl}'</b> failed <b>${numberOfFailedRequests}</b> times with <b>${exceptionName}</b>
    raised.</p>
<br>
<br>
<hr>
<p class="footer">This is a notification email from the Hawaii HR system. Please do not reply directly to this
    email.</p>
</body>
</html>