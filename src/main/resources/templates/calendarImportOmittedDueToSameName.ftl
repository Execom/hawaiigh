<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
      <#include "emailStyle.css">
    </style>
</head>
<body>
<p>There is more than one user with name '${userName}' in the database. Requests for these users could not be
updated with calendar information regarding half day requests.</p>
<p>Please <a href="${serverName}">login to the Hawaii system</a> to review/change imported requests.</p>
<hr>
<p class="footer">This is automatic notification email from the Hawaii application. Please do not reply to this email.</p>
</body>
</html>