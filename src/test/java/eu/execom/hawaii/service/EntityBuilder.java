package eu.execom.hawaii.service;

import eu.execom.hawaii.dto.CreateTokenDto;
import eu.execom.hawaii.model.Absence;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Icon;
import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.PublicHoliday;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.UserPushToken;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.Duration;
import eu.execom.hawaii.model.enumerations.LeaveProfileType;
import eu.execom.hawaii.model.enumerations.Platform;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntityBuilder {

  private static LocalDate TODAYS_DATE = LocalDate.now();

  public static Team team() {
    var team = new Team();
    team.setName("My team 1");
    team.setId(1L);
    team.setAnnualRequestEmails("example@example.com");
    team.setSicknessRequestEmails("example@example.com");
    team.setBonusRequestEmails("example@example.com");
    team.setEditable(true);
    team.setUsers(new ArrayList<>());
    team.setTeamApprovers(new ArrayList<>());

    return team;
  }

  static Team team2() {
    var team = new Team();
    team.setName("My team 2");
    team.setId(2L);
    team.setAnnualRequestEmails("example@example.com");
    team.setSicknessRequestEmails("example@example.com");
    team.setBonusRequestEmails("example@example.com");
    team.setEditable(true);
    team.setUsers(new ArrayList<>());
    team.setTeamApprovers(new ArrayList<>());

    return team;
  }

  static Team archivedTeam() {
    var team = new Team();
    team.setName("Archived Users");
    team.setId(3L);
    team.setAnnualRequestEmails("example@example.com");
    team.setSicknessRequestEmails("example@example.com");
    team.setBonusRequestEmails("example@example.com");
    team.setEditable(false);
    team.setUsers(new ArrayList<>());
    team.setTeamApprovers(new ArrayList<>());

    return team;
  }

  public static User user(Team team) {
    var user = new User();
    user.setId(1L);
    user.setTeam(team);
    user.setLeaveProfile(leaveProfile());
    user.setFullName("Aria Stark");
    user.setEmail("aria.stark@gmail.com");
    user.setUserRole(UserRole.HR_MANAGER);
    user.setJobTitle("Developer");
    user.setStartedWorkingAtExecomDate(TODAYS_DATE);
    user.setStartedWorkingDate(TODAYS_DATE.minusYears(5));
    user.setStartedProfessionalCareerDate(TODAYS_DATE.minusYears(2));
    user.setYearsOfService(4);
    user.setUserStatusType(UserStatusType.ACTIVE);
    user.setApproverTeams(new ArrayList<>());
    user.setRequests(new ArrayList<>());
    user.setAllowances(new ArrayList<>());

    return user;
  }

  static User user2() {
    var user = new User();
    user.setId(2L);
    user.setTeam(team2());
    user.setLeaveProfile(leaveProfile());
    user.setFullName("Jessica Jones");
    user.setEmail("jjones@gmail.com");
    user.setUserRole(UserRole.USER);
    user.setJobTitle("Developer");
    user.setStartedWorkingAtExecomDate(LocalDate.of(2019, 2, 28));
    user.setStartedWorkingDate(TODAYS_DATE.minusYears(5));
    user.setYearsOfService(4);
    user.setUserStatusType(UserStatusType.ACTIVE);
    user.setApproverTeams(new ArrayList<>());
    user.setRequests(new ArrayList<>());
    user.setAllowances(new ArrayList<>());

    return user;
  }

  public static User approver() {
    var approver = new User();
    approver.setId(3L);
    approver.setTeam(team());
    approver.setLeaveProfile(leaveProfileII());
    approver.setFullName("Bruce Wayne");
    approver.setEmail("bruce.wayne@execom.eu");
    approver.setUserRole(UserRole.HR_MANAGER);
    approver.setJobTitle("Developer");
    approver.setStartedWorkingAtExecomDate(LocalDate.of(2016, 3, 3));
    approver.setStartedWorkingDate(TODAYS_DATE.minusYears(10));
    approver.setStartedProfessionalCareerDate(TODAYS_DATE.minusYears(5).minusDays(2));
    approver.setYearsOfService(9);
    approver.setUserStatusType(UserStatusType.ACTIVE);
    approver.setApproverTeams(new ArrayList<>());
    approver.setRequests(new ArrayList<>());
    approver.setAllowances(new ArrayList<>());
    approver.setImageUrl("picture");

    return approver;
  }

  public static User officeManager() {
    var officeManager = new User();
    officeManager.setId(8L);
    officeManager.setTeam(team());
    officeManager.setLeaveProfile(leaveProfile());
    officeManager.setFullName("Wade Wilson");
    officeManager.setEmail("wwilson@execom.eu");
    officeManager.setUserRole(UserRole.OFFICE_MANAGER);
    officeManager.setJobTitle("User");
    officeManager.setStartedWorkingAtExecomDate(LocalDate.of(2019, 3, 3));
    officeManager.setStartedWorkingDate(TODAYS_DATE.minusYears(10));
    officeManager.setStartedProfessionalCareerDate(TODAYS_DATE.minusYears(5).minusDays(2));
    officeManager.setYearsOfService(9);
    officeManager.setUserStatusType(UserStatusType.ACTIVE);
    officeManager.setApproverTeams(new ArrayList<>());
    officeManager.setRequests(new ArrayList<>());
    officeManager.setAllowances(new ArrayList<>());
    officeManager.setImageUrl("picture");

    return officeManager;
  }

  static User approver2() {
    var approver = new User();
    approver.setId(5L);
    approver.setTeam(team());
    approver.setLeaveProfile(leaveProfileII());
    approver.setFullName("Zoro Roronoa");
    approver.setEmail("zroronoa@execom.eu");
    approver.setUserRole(UserRole.USER);
    approver.setJobTitle("Developer");
    approver.setStartedWorkingAtExecomDate(LocalDate.of(2016, 3, 3));
    approver.setStartedWorkingDate(TODAYS_DATE.minusYears(10));
    approver.setStartedProfessionalCareerDate(TODAYS_DATE.minusYears(5).minusDays(2));
    approver.setYearsOfService(9);
    approver.setUserStatusType(UserStatusType.ACTIVE);
    approver.setApproverTeams(new ArrayList<>());
    approver.setRequests(new ArrayList<>());
    approver.setAllowances(new ArrayList<>());

    return approver;
  }


  public static User superAdmin() {
    var superAdmin = new User();
    superAdmin.setId(6L);
    superAdmin.setTeam(team());
    superAdmin.setLeaveProfile(leaveProfileII());
    superAdmin.setFullName("Clark Kent");
    superAdmin.setEmail("ckent@execom.eu");
    superAdmin.setUserRole(UserRole.USER);
    superAdmin.setJobTitle("Developer");
    superAdmin.setStartedWorkingAtExecomDate(LocalDate.of(2017, 3, 3));
    superAdmin.setStartedWorkingDate(TODAYS_DATE.minusYears(10));
    superAdmin.setStartedProfessionalCareerDate(TODAYS_DATE.minusYears(5).minusDays(2));
    superAdmin.setYearsOfService(9);
    superAdmin.setUserStatusType(UserStatusType.ACTIVE);
    superAdmin.setApproverTeams(new ArrayList<>());
    superAdmin.setRequests(new ArrayList<>());
    superAdmin.setAllowances(new ArrayList<>());
    superAdmin.setUserAdminPermission(UserAdminPermission.SUPER);

    return superAdmin;
  }

  public static User admin() {
    var admin = new User();
    admin.setId(6L);
    admin.setTeam(team());
    admin.setLeaveProfile(leaveProfileII());
    admin.setFullName("Lex Luthor");
    admin.setEmail("lluthor@execom.eu");
    admin.setUserRole(UserRole.USER);
    admin.setJobTitle("Developer");
    admin.setStartedWorkingAtExecomDate(LocalDate.of(2019, 3, 3));
    admin.setStartedWorkingDate(TODAYS_DATE.minusYears(10));
    admin.setStartedProfessionalCareerDate(TODAYS_DATE.minusYears(5).minusDays(2));
    admin.setYearsOfService(9);
    admin.setUserStatusType(UserStatusType.ACTIVE);
    admin.setApproverTeams(new ArrayList<>());
    admin.setRequests(new ArrayList<>());
    admin.setAllowances(new ArrayList<>());
    admin.setUserAdminPermission(UserAdminPermission.BASIC);

    return admin;
  }

  static User revokedAdmin() {
    var admin = new User();
    admin.setId(6L);
    admin.setTeam(team());
    admin.setLeaveProfile(leaveProfileII());
    admin.setFullName("Lex Luthor");
    admin.setEmail("lluthor@execom.eu");
    admin.setUserRole(UserRole.USER);
    admin.setJobTitle("Developer");
    admin.setStartedWorkingAtExecomDate(LocalDate.of(2019, 3, 3));
    admin.setStartedWorkingDate(TODAYS_DATE.minusYears(10));
    admin.setStartedProfessionalCareerDate(TODAYS_DATE.minusYears(5).minusDays(2));
    admin.setYearsOfService(9);
    admin.setUserStatusType(UserStatusType.ACTIVE);
    admin.setApproverTeams(new ArrayList<>());
    admin.setRequests(new ArrayList<>());
    admin.setAllowances(new ArrayList<>());
    admin.setUserAdminPermission(UserAdminPermission.NONE);

    return admin;
  }

  public static User leavingUser() {
    var leavingUser = new User();
    leavingUser.setId(4L);
    leavingUser.setTeam(team());
    leavingUser.setLeaveProfile(leaveProfileII());
    leavingUser.setFullName("Zoro Roronoa");
    leavingUser.setEmail("zroronoa@execom.eu");
    leavingUser.setUserRole(UserRole.USER);
    leavingUser.setJobTitle("Developer");
    leavingUser.setStartedWorkingAtExecomDate(LocalDate.of(2016, 3, 3));
    leavingUser.setStoppedWorkingAtExecomDate(TODAYS_DATE.minusDays(1));
    leavingUser.setStartedWorkingDate(TODAYS_DATE.minusYears(10));
    leavingUser.setYearsOfService(9);
    leavingUser.setUserStatusType(UserStatusType.ACTIVE);
    leavingUser.setApproverTeams(new ArrayList<>());
    leavingUser.setRequests(new ArrayList<>());
    leavingUser.setAllowances(new ArrayList<>());

    return leavingUser;
  }

  public static User createUser() {
    var user = new User();
    user.setId(1L);
    user.setTeam(team());
    user.setLeaveProfile(leaveProfile());
    user.setFullName("Test");
    user.setEmail("test@execom.eu");
    user.setUserRole(UserRole.USER);
    user.setJobTitle("Developer");
    user.setStartedWorkingAtExecomDate(LocalDate.of(2019, 2, 28));
    user.setStartedWorkingDate(TODAYS_DATE.minusYears(5));
    user.setYearsOfService(4);
    user.setUserStatusType(UserStatusType.ACTIVE);
    user.setApproverTeams(new ArrayList<>());
    user.setRequests(new ArrayList<>());
    user.setAllowances(new ArrayList<>());
    user.setUserAdminPermission(UserAdminPermission.NONE);

    return user;
  }

  static LeaveProfile leaveProfile() {
    var leaveProfile = new LeaveProfile();
    leaveProfile.setId(1L);
    leaveProfile.setName("Default");
    leaveProfile.setEntitlement(160);
    leaveProfile.setMaxRelevantExperienceBonus(40);
    leaveProfile.setMaxCarriedOver(40);
    leaveProfile.setMaxBonusDays(40);
    leaveProfile.setTraining(16);
    leaveProfile.setMaxAllowanceFromNextYear(40);
    leaveProfile.setUpgradeable(false);
    leaveProfile.setLeaveProfileType(LeaveProfileType.ZERO_TO_FIVE_YEARS);
    leaveProfile.setComment("No comment");
    leaveProfile.setUsers(new ArrayList<>());

    return leaveProfile;
  }

  static LeaveProfile leaveProfileII() {
    var leaveProfile = new LeaveProfile();
    leaveProfile.setId(2L);
    leaveProfile.setName("Five-Ten");
    leaveProfile.setEntitlement(168);
    leaveProfile.setMaxRelevantExperienceBonus(40);
    leaveProfile.setMaxCarriedOver(40);
    leaveProfile.setMaxBonusDays(40);
    leaveProfile.setTraining(24);
    leaveProfile.setMaxAllowanceFromNextYear(40);
    leaveProfile.setUpgradeable(true);
    leaveProfile.setLeaveProfileType(LeaveProfileType.FIVE_TO_TEN_YEARS);
    leaveProfile.setComment("No comment");
    leaveProfile.setUsers(new ArrayList<>());

    return leaveProfile;
  }

  static LeaveProfile leaveProfileIII() {
    var leaveProfile = new LeaveProfile();
    leaveProfile.setId(3L);
    leaveProfile.setName("Ten-Fifteen");
    leaveProfile.setEntitlement(176);
    leaveProfile.setMaxRelevantExperienceBonus(40);
    leaveProfile.setMaxCarriedOver(40);
    leaveProfile.setTraining(24);
    leaveProfile.setMaxAllowanceFromNextYear(40);
    leaveProfile.setUpgradeable(true);
    leaveProfile.setLeaveProfileType(LeaveProfileType.TEN_TO_FIFTEEN_YEARS);
    leaveProfile.setComment("No comment");
    leaveProfile.setUsers(new ArrayList<>());

    return leaveProfile;
  }

  public static Request request(Absence absence, List<Day> days) {
    var request = new Request();
    request.setId(1L);
    request.setUser(user(team()));
    request.getUser().setId(1L);
    request.setAbsence(absence);
    request.setRequestStatus(RequestStatus.PENDING);
    request.setSubmissionTime(LocalDateTime.of(TODAYS_DATE, LocalTime.NOON));
    request.setReason("My request reason");
    request.setDays(days);
    request.setCurrentlyApprovedBy(new ArrayList<>());

    return request;
  }

  static Request requestII(Absence absence, List<Day> days) {
    var request = new Request();
    request.setId(2L);
    request.setUser(user(team()));
    request.getUser().setId(1L);
    request.setAbsence(absence);
    request.setRequestStatus(RequestStatus.PENDING);
    request.setSubmissionTime(LocalDateTime.of(TODAYS_DATE, LocalTime.NOON));
    request.setReason("My request reason");
    request.setDays(days);

    return request;
  }

  static Request bonusRequest(List<Day> days) {
    var request = new Request();
    request.setId(1L);
    request.setUser(user(team()));
    request.getUser().setId(1L);
    request.setAbsence(absenceBonus());
    request.setRequestStatus(RequestStatus.PENDING);
    request.setSubmissionTime(LocalDateTime.of(TODAYS_DATE, LocalTime.NOON));
    request.setReason("My request reason");
    request.setDays(days);
    request.setCurrentlyApprovedBy(new ArrayList<>());

    return request;
  }

  public static Allowance allowance(User user) {
    var allowance = new Allowance();
    allowance.setId(1L);
    allowance.setUser(user);
    allowance.getUser().setId(1L);
    allowance.setYear(thisYear());
    allowance.setAnnual(160);
    allowance.setTakenAnnual(0);
    allowance.setSickness(0);
    allowance.setBonus(40);
    allowance.setApprovedBonus(0);
    allowance.setPendingBonus(0);
    allowance.setBonusManualAdjust(0);
    allowance.setCarriedOver(40);
    allowance.setManualAdjust(0);
    allowance.setTraining(16);
    allowance.setTakenTraining(0);
    allowance.setTakenInPreviousYear(0);
    allowance.setPendingInPreviousYear(0);

    return allowance;
  }

  public static Allowance nextYearAllowance(User user) {
    var allowance = new Allowance();
    allowance.setId(3L);
    allowance.setUser(user);
    allowance.getUser().setId(1L);
    allowance.setYear(nextYear());
    allowance.setAnnual(160);
    allowance.setTakenAnnual(0);
    allowance.setSickness(0);
    allowance.setBonus(40);
    allowance.setApprovedBonus(0);
    allowance.setPendingBonus(0);
    allowance.setBonusManualAdjust(0);
    allowance.setCarriedOver(40);
    allowance.setManualAdjust(0);
    allowance.setTraining(16);
    allowance.setTakenTraining(0);
    allowance.setTakenInPreviousYear(0);
    allowance.setPendingInPreviousYear(0);

    return allowance;
  }

  public static Allowance lastYearAllowance(User user) {
    var allowance = new Allowance();
    allowance.setId(4L);
    allowance.setUser(user);
    allowance.getUser().setId(1L);
    allowance.setYear(lastYear());
    allowance.setAnnual(160);
    allowance.setTakenAnnual(0);
    allowance.setSickness(0);
    allowance.setBonus(40);
    allowance.setApprovedBonus(0);
    allowance.setPendingBonus(0);
    allowance.setBonusManualAdjust(0);
    allowance.setCarriedOver(40);
    allowance.setManualAdjust(0);
    allowance.setTraining(16);
    allowance.setTakenTraining(0);
    allowance.setTakenInPreviousYear(0);
    allowance.setPendingInPreviousYear(0);

    return allowance;
  }

  public static Year thisYear() {
    var year = new Year();
    year.setId(1L);
    year.setActive(true);
    year.setYear(TODAYS_DATE.getYear());
    year.setAllowances(new ArrayList<>());

    return year;
  }

  static Year nextYear() {
    var year = new Year();
    year.setId(2L);
    year.setActive(true);
    year.setYear(TODAYS_DATE.plusYears(1).getYear());
    year.setAllowances(new ArrayList<>());

    return year;
  }

  static Year lastYear() {
    var year = new Year();
    year.setId(3L);
    year.setActive(true);
    year.setYear(TODAYS_DATE.minusYears(1).getYear());
    year.setAllowances(new ArrayList<>());

    return year;
  }

  static PublicHoliday publicholiday() {
    var publicHoliday = new PublicHoliday();
    publicHoliday.setId(1L);
    publicHoliday.setDeleted(false);
    publicHoliday.setName("New year");
    publicHoliday.setDate(LocalDate.of(2018, 1, 5));

    return publicHoliday;
  }

  public static Absence absenceAnnual() {
    var absence = new Absence();
    absence.setId(1L);
    absence.setAbsenceType(AbsenceType.LEAVE);
    absence.setName("Annual leave");
    absence.setComment("Description");
    absence.setDeducted(true);
    absence.setIcon(icon());
    absence.setLeaveRequests(new ArrayList<>());

    return absence;
  }

  static Absence absenceTraining() {
    var absence = new Absence();
    absence.setId(2L);
    absence.setAbsenceType(AbsenceType.TRAINING);
    absence.setName("Training");
    absence.setComment("Description");
    absence.setDeducted(true);
    absence.setIcon(icon());
    absence.setLeaveRequests(new ArrayList<>());

    return absence;
  }

  private static Icon icon() {
    var icon = new Icon();
    icon.setId(1L);
    icon.setIconUrl("icons/training.png");
    return icon;
  }

  public static Absence absenceSickness() {
    var absence = new Absence();
    absence.setId(3L);
    absence.setAbsenceType(AbsenceType.SICKNESS);
    absence.setName("Sickness");
    absence.setComment("Description");
    absence.setDeducted(false);
    absence.setIcon(icon());
    absence.setLeaveRequests(new ArrayList<>());

    return absence;
  }

  static Absence absenceBonus() {
    var absence = new Absence();
    absence.setId(4L);
    absence.setAbsenceType(AbsenceType.BONUS);
    absence.setName("Bonus");
    absence.setComment("Description");
    absence.setDeducted(true);
    absence.setIcon(icon());
    absence.setLeaveRequests(new ArrayList<>());

    return absence;
  }

  public static Day day(LocalDate date) {
    var day = new Day();
    day.setId(1L);
    day.setDate(date);
    day.setDuration(Duration.FULL_DAY);

    return day;
  }

  static UserPushToken userAndroidPushToken() {
    UserPushToken userPushToken = new UserPushToken();
    userPushToken.setId(1L);
    userPushToken.setPushToken("oldPushToken");
    userPushToken.setName("oldPushToken");
    userPushToken.setPlatform(Platform.ANDROID);
    userPushToken.setCreateDateTime(TODAYS_DATE.atTime(LocalTime.NOON));

    return userPushToken;
  }

  static UserPushToken userIOSPushToken() {
    UserPushToken userPushToken = new UserPushToken();
    userPushToken.setId(2L);
    userPushToken.setPushToken("oldPushToken");
    userPushToken.setName("oldPushToken");
    userPushToken.setPlatform(Platform.IOS);
    userPushToken.setCreateDateTime(TODAYS_DATE.atTime(LocalTime.NOON));

    return userPushToken;
  }

  static CreateTokenDto createTokenDto() {
    CreateTokenDto createTokenDto = new CreateTokenDto();
    createTokenDto.setPushToken("anyPushToken");
    createTokenDto.setPlatform(Platform.ANDROID);
    createTokenDto.setName("Token");

    return createTokenDto;
  }

  public static OAuth2User createOAuth2User() {
    List<SimpleGrantedAuthority> mockAuthorities;
    Map<String, Object> mockUserAttributes = new HashMap<>();

    mockUserAttributes.put("sub", 1L);
    mockUserAttributes.put("email", "test@execom.eu");
    mockUserAttributes.put("fullName", "Test");
    mockUserAttributes.put("picture", "Test");
    mockAuthorities = Collections.singletonList(new SimpleGrantedAuthority(UserRole.USER.toString()));

    return new DefaultOAuth2User(mockAuthorities, mockUserAttributes, "sub");
  }

  public static OAuth2User createOAuth2User2() {
    List<SimpleGrantedAuthority> mockAuthorities;
    Map<String, Object> mockUserAttributes = new HashMap<>();

    mockUserAttributes.put("sub", 2L);
    mockUserAttributes.put("email", "");
    mockUserAttributes.put("fullName", "Test");
    mockUserAttributes.put("picture", "Test");
    mockAuthorities = Collections.singletonList(new SimpleGrantedAuthority(UserRole.USER.toString()));

    return new DefaultOAuth2User(mockAuthorities, mockUserAttributes, "sub");
  }

  public static OAuth2User createOAuth2User3() {
    List<SimpleGrantedAuthority> mockAuthorities;
    Map<String, Object> mockUserAttributes = new HashMap<>();

    mockUserAttributes.put("sub", 3L);
    mockUserAttributes.put("email", "test@gmail.eu");
    mockUserAttributes.put("fullName", "Test");
    mockUserAttributes.put("picture", "Test");
    mockAuthorities = Collections.singletonList(new SimpleGrantedAuthority(UserRole.USER.toString()));

    return new DefaultOAuth2User(mockAuthorities, mockUserAttributes, "sub");
  }

}
