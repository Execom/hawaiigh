package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.repository.AllowanceRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.repository.YearRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityExistsException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class YearServiceTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private YearRepository yearRepository;

  @Mock
  private AllowanceRepository allowanceRepository;

  @InjectMocks
  private YearService yearService;

  private User mockUser;
  private List<User> mockUsers;
  private Year mockYear;
  private Year nextYear;
  private List<Year> mockAllYears;
  private Object[] allMocks;

  @Before
  public void setUp() {
    mockUser = EntityBuilder.approver();
    mockUsers = List.of(mockUser, EntityBuilder.user(EntityBuilder.team()));
    mockYear = EntityBuilder.thisYear();
    nextYear = EntityBuilder.nextYear();
    mockAllYears = Arrays.asList(mockYear, nextYear);
    allMocks = new Object[] {allowanceRepository, yearRepository};
  }

  @Test
  public void shouldGetYearsById() {
    // given
    var yearId = 1L;
    given(yearRepository.findById(yearId)).willReturn(Optional.of(mockYear));

    // when
    Year year = yearService.findById(yearId);

    // then
    assertThat("Expected year is current year", year.getYear(), is(EntityBuilder.thisYear().getYear()));
    verify(yearRepository).findById(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldGetAllYears() {
    // given
    Page<Year> pagedYears = new PageImpl<>(mockAllYears);
    given(yearRepository.findAll(Pageable.unpaged())).willReturn(pagedYears);

    // when
    Page<Year> allYears = yearService.getAll(Pageable.unpaged());

    // then
    assertThat("Expect that there are '2' years", allYears.getTotalElements(), is(2L));
    verify(yearRepository).findAll(Pageable.unpaged());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldSaveYear() {
    // given
    given(yearRepository.findById(mockYear.getId())).willReturn(Optional.of(mockYear));
    given(yearRepository.save(mockYear)).willReturn(mockYear);

    // when
    Year year = yearService.save(mockYear);

    // then
    assertThat("Expected year is current year", year.getYear(), is(EntityBuilder.thisYear().getYear()));
    verify(yearRepository).findById(anyLong());
    verify(yearRepository).save(any());
    verify(allowanceRepository).saveAll(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityExistsException.class)
  public void shouldFailToSaveYear() {
    // given
    given(yearRepository.findById(mockYear.getId())).willReturn(Optional.of(mockYear));
    given(yearRepository.save(mockYear)).willThrow(EntityExistsException.class);

    // when
    yearService.save(mockYear);

    // then
    verify(yearRepository).findById(anyLong());
    verify(yearRepository).save(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldFailToSaveYearBecauseYearIsInvalid() {
    // given
    var invalidYear = 1999;
    mockYear.setYear(invalidYear);

    // when
    yearService.save(mockYear);

    // then
  }

  @Test
  public void shouldDeleteYear() {
    // given
    mockYear.setActive(false);
    given(yearRepository.findById(mockYear.getId())).willReturn(Optional.of(mockYear));

    // when
    yearService.deleteById(mockYear.getId());

    // then
    verify(yearRepository).findById(anyLong());
    verify(yearRepository).deleteById(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToDeleteAYearWhichHasAllowanceAssigned() {
    // given
    mockYear.setAllowances(List.of(EntityBuilder.allowance(EntityBuilder.approver())));
    given(yearRepository.findById(mockYear.getId())).willReturn(Optional.of(mockYear));

    // when
    yearService.deleteById(mockYear.getId());

    // then
    verify(yearRepository).findById(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldCreateAllowanceOnCreateYear() {
    // given
    given(userRepository.findAllByUserStatusType(anyList())).willReturn(mockUsers);

    // when
    yearService.createAllowanceWhenActivatingYear(nextYear);

    // then
    verify(allowanceRepository).saveAll(anyList());
    verify(userRepository).findAllByUserStatusType(anyList());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldCreateAllowance() {
    // given

    // when
    var allowance = yearService.createAllowance(nextYear, mockUser);

    // then
    assertThat("Expect to have one allowance created for user", allowance, is(notNullValue()));
    assertThat("Expect allowance to be for next year", allowance.getYear().getYear(),
        is(EntityBuilder.nextYear().getYear()));
    verifyNoMoreInteractions(allMocks);
  }
}

