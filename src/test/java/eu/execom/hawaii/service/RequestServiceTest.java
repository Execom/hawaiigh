package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.NotAuthorizedApprovalException;
import eu.execom.hawaii.exceptions.RequestAlreadyHandledException;
import eu.execom.hawaii.exceptions.UnauthorizedAccessException;
import eu.execom.hawaii.model.Absence;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.model.enumerations.RequestStatus;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.repository.AbsenceRepository;
import eu.execom.hawaii.repository.DayRepository;
import eu.execom.hawaii.repository.RequestRepository;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.time.temporal.TemporalAdjusters.firstInMonth;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class RequestServiceTest {

  @Mock
  private AllowanceService allowanceService;
  @Mock
  private GoogleCalendarService googleCalendarService;
  @Mock
  private EmailService emailService;
  @Mock
  private RequestRepository requestRepository;
  @Mock
  private UserService userService;
  @Mock
  private UserRepository userRepository;
  @Mock
  private AbsenceRepository absenceRepository;
  @Mock
  private TeamRepository teamRepository;
  @Mock
  private DayRepository dayRepository;
  @Mock
  private SendNotificationsService sendNotificationsService;
  @Mock
  private AuditInformationService auditInformationService;
  @Mock
  private PublicHolidayService publicHolidayService;
  @InjectMocks
  private RequestService requestService;

  private int thisYear;
  private User mockUser;
  private User mockApprover;
  private Day dayOne;
  private Day dayTwo;
  private Day dayFromDifferentYear;
  private Absence absenceAnnual;
  private Absence absenceTraining;
  private Request requestOne;
  private Request requestTwo;
  private Request bonusRequest;
  private List<Request> mockRequests;
  private Object[] allMocks;

  @Before
  public void setUp() {
    thisYear = EntityBuilder.thisYear().getYear();
    mockUser = EntityBuilder.user(EntityBuilder.team());
    mockApprover = EntityBuilder.approver();

    absenceAnnual = EntityBuilder.absenceAnnual();
    absenceTraining = EntityBuilder.absenceTraining();

    LocalDate firstMonday = LocalDate.of(thisYear, 11, 1).with(firstInMonth(DayOfWeek.MONDAY));
    dayOne = EntityBuilder.day(firstMonday);
    dayTwo = EntityBuilder.day(firstMonday.plusDays(1));
    Day dayThree = EntityBuilder.day(firstMonday.plusDays(2));

    dayFromDifferentYear = EntityBuilder.day(LocalDate.of(2021, 1, 15));

    requestOne = EntityBuilder.request(absenceAnnual, List.of(dayOne));
    requestTwo = EntityBuilder.request(absenceTraining, Arrays.asList(dayTwo, dayThree));
    bonusRequest = EntityBuilder.bonusRequest(List.of(dayOne));
    mockRequests = Arrays.asList(requestOne, requestTwo);

    allMocks = new Object[] {allowanceService, googleCalendarService, emailService, requestRepository, userRepository,
        absenceRepository, teamRepository, auditInformationService, publicHolidayService};
  }

  @Test
  public void shouldGetPendingRequestsFromTeamsForApproval() {
    // given
    mockApprover.setApproverTeams(List.of(requestOne.getUser().getTeam()));
    var team = EntityBuilder.team();
    team.setUsers(List.of(mockUser));
    mockApprover.setApproverTeams(List.of(team));
    requestTwo.setRequestStatus(RequestStatus.CANCELLATION_PENDING);
    mockUser.setRequests(mockRequests);

    // when
    var requests = requestService.getPendingRequestsFromTeams(mockApprover);

    // then
    assertNotNull(requests);
    assertEquals("Expect that list of request contains exactly '2' elements.", 2, requests.size());
    assertEquals("Expect that first request in list has status 'pending'.", RequestStatus.PENDING,
        requests.get(0).getRequestStatus());
    assertEquals("Expect that second request in list has status 'cancellation pending'.",
        RequestStatus.CANCELLATION_PENDING, requests.get(1).getRequestStatus());
    assertThat("Expect that every request in list has status 'pending' or 'cancellation pending'.",
        requests.stream().anyMatch(request -> !request.isPendingOrCancellationPending()), is(false));
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllByDateRange() {
    // given
    given(requestRepository.findAll()).willReturn(mockRequests);
    var date = LocalDate.of(thisYear, 11, 1);

    // when
    List<Request> requests = requestService.findAllByDateRange(date);

    // then
    assertThat("Expect that list contains exactly '2' elements.", requests.size(), is(2));
    verify(requestRepository).findAll();
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFailToFindAnyByDateRange() {
    // given
    given(requestRepository.findAll()).willReturn(mockRequests);
    var date = LocalDate.of(thisYear, 11, 1);

    // when
    List<Request> requests = requestService.findAllByDateRange(date);

    // then
    assertThat("Expect that list contains exactly '2' elements.", requests.size(), is(2));
    verify(requestRepository).findAll();
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllByUserWithinDates() {
    // given
    var startDate = LocalDate.of(thisYear, 11, 18);
    var endDate = LocalDate.of(thisYear, 11, 22);

    // when
    List<Request> requests = requestService.findAllByUserWithinDates(startDate, endDate, 1L);

    // then
    assertTrue("Expect that list is empty.", requests.isEmpty());
    verify(userService).findById(any());
    verify(requestRepository).findAllRequestsByUserIdOrderByStartingDateDesc(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFailToFindAnyRequestByUserWithinDates() {
    // given
    given(userService.findById(any())).willReturn(mockUser);
    given(requestRepository.findAllRequestsByUserIdOrderByStartingDateDesc(mockUser.getId())).willReturn(mockRequests);
    var startDate = LocalDate.of(thisYear, 11, 18);
    var endDate = LocalDate.of(thisYear, 11, 18);

    // when
    List<Request> requests = requestService.findAllByUserWithinDates(startDate, endDate, 1L);

    // then
    assertTrue("Expect that list is empty.", requests.isEmpty());
    verify(userService).findById(any());
    verify(requestRepository).findAllRequestsByUserIdOrderByStartingDateDesc(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllByUser() {
    // given
    given(requestRepository.findAllByUserId(mockUser.getId())).willReturn(mockRequests);

    // when
    List<Request> requests = requestService.findAllByUser(1L);

    // then
    assertThat("Expect that list contains exactly '2' elements.", requests.size(), is(2));
    verify(requestRepository).findAllByUserId(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityNotFoundException.class)
  public void shouldFailToFindAllByUser() {
    // given
    given(requestRepository.findAllByUserId(2L)).willThrow(new EntityNotFoundException());

    // when
    requestService.findAllByUser(2L);

    // then
    verify(requestRepository).findAllByUserId(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllByUserForYear() {
    // given
    given(userService.getUser(anyLong(), any())).willReturn(mockUser);
    given(userService.findById(anyLong())).willReturn(mockUser);
    given(requestRepository.findAllRequestsByUserIdOrderByStartingDateDesc(anyLong())).willReturn(mockRequests);

    // when
    List<Request> requests = requestService.findAllByUserForYear(mockUser.getId(), thisYear, mockApprover);

    // then
    assertThat("Expect that list of request contains exactly 'two' elements.", requests.size(), is(2));
    assertThat("Expect that first day of first request in list date is thisYear-11-2",
        requests.get(0).getDays().get(0).getDate(), is(LocalDate.of(thisYear, 11, 2)));
    verify(userService).getUser(anyLong(), any());
    verify(userService).findById(anyLong());
    verify(requestRepository).findAllRequestsByUserIdOrderByStartingDateDesc(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllByRequestStatus() {
    // given
    given(requestRepository.findAllByRequestStatus(RequestStatus.PENDING)).willReturn(mockRequests);

    // when
    List<Request> requests = requestService.findAllByRequestStatus(RequestStatus.PENDING);

    // then
    assertThat("Expect that list of request contains exactly 'two' elements.", requests.size(), is(2));
    assertThat("Expect that request status is pending", requests.get(0).getRequestStatus(), is(RequestStatus.PENDING));
    verify(requestRepository).findAllByRequestStatus(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityNotFoundException.class)
  public void shouldFailToFindAnyRequestByRequestStatus() {
    // given
    given(requestRepository.findAllByRequestStatus(RequestStatus.PENDING)).willThrow(new EntityNotFoundException());

    // when
    requestService.findAllByRequestStatus(RequestStatus.PENDING);

    // then
    verify(requestRepository).findAllByRequestStatus(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllByAbsenceType() {
    // given
    given(requestRepository.findAllByAbsenceAbsenceType(AbsenceType.LEAVE)).willReturn(mockRequests);

    // when
    List<Request> requests = requestService.findAllByAbsenceType(AbsenceType.LEAVE);

    // then
    assertThat("Expect that list of requests contains exactly 'two' elements.", requests.size(), is(2));
    assertThat("Expect that request absence type is 'LEAVE'", requests.get(0).getAbsence().getAbsenceType(),
        is(AbsenceType.LEAVE));
    assertTrue("Expect that first request in list leave type 'is deducted'.",
        requests.get(0).getAbsence().isDeducted());
    verify(requestRepository).findAllByAbsenceAbsenceType(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityNotFoundException.class)
  public void shouldFailToFindAnyRequestByAbsenceType() {
    // given
    given(requestRepository.findAllByAbsenceAbsenceType(AbsenceType.LEAVE)).willThrow(new EntityNotFoundException());

    // when
    requestService.findAllByAbsenceType(AbsenceType.LEAVE);

    // then
    verify(requestRepository).findAllByAbsenceAbsenceType(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldGetRequestById() {
    // given
    given(requestRepository.getOne(1L)).willReturn(requestOne);

    // when
    Request request = requestService.getById(1L, mockUser);

    // then
    assertThat("Expect that request is just for one day", request.getDays().size(), is(1));
    assertThat("Expect that request date is thisYear-11-2", request.getDays().get(0).getDate(),
        is(LocalDate.of(thisYear, 11, 2)));
    verify(requestRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityNotFoundException.class)
  public void shouldFailToFindRequestById() {
    // given
    given(requestRepository.getOne(anyLong())).willThrow(new EntityNotFoundException());

    // when
    requestService.getById(2L, mockUser);

    // then
    verify(requestRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldGetRequestByIdIfLoggedUserIsApprover() {
    // given
    mockApprover.setUserRole(UserRole.USER);
    mockApprover.setApproverTeams(List.of(EntityBuilder.team()));

    given(requestRepository.getOne(1L)).willReturn(requestOne);
    given(userRepository.getOne(anyLong())).willReturn(mockApprover);

    // when
    Request request = requestService.getById(1L, mockApprover);

    // then
    assertThat("Expect that request is just for one day", request.getDays().size(), is(1));
    assertThat("Expect request owner full name to be : 'Aria Stark'", request.getUser().getFullName(),
        is("Aria Stark"));
    verify(requestRepository).getOne(anyLong());
    verify(userRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = UnauthorizedAccessException.class)
  public void shouldFailToGetRequestByIdIfLoggedUserIsNotUsersApprover() {
    // given
    mockApprover.setUserRole(UserRole.USER);
    mockApprover.setApproverTeams(List.of(EntityBuilder.team2()));
    given(requestRepository.getOne(1L)).willReturn(requestOne);
    given(userRepository.getOne(anyLong())).willReturn(mockApprover);

    // when
    requestService.getById(1L, mockApprover);

    // then
    verify(userRepository).getOne(anyLong());
    verify(requestRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldSucceedToGetRequestByIdWhenLoggedUserHasRoleOfficeManager() {
    // given
    mockApprover.setUserRole(UserRole.OFFICE_MANAGER);
    given(requestRepository.getOne(1L)).willReturn(requestOne);

    // when
    requestService.getById(1L, mockApprover);

    // then
    verify(requestRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldAccessRequest() {
    // given
    requestOne.setUser(mockUser);
    given(requestRepository.getOne(anyLong())).willReturn(requestOne);

    // when
    requestService.validateUserIsAuthorizedToAccessRequest(1L, mockUser);

    // then
    verify(requestRepository).getOne(anyLong());
    verifyNoMoreInteractions(requestRepository);
  }

  @Test
  public void shouldCreateRequestForApproval() {
    // given
    given(userRepository.getOne(anyLong())).willReturn(mockUser);
    given(requestRepository.save(any())).willReturn(requestOne);
    given(requestRepository.findAllByUserId(mockUser.getId())).willReturn(Collections.emptyList());

    // when
    Request savedRequest = requestService.create(requestOne, mockUser);

    // then
    assertThat("Expect that request owner is 'Aria Stark'", savedRequest.getUser().getFullName(), is("Aria Stark"));
    verify(userRepository).getOne(anyLong());
    verify(requestRepository).save(any());
    verify(googleCalendarService).handleCreatedRequest(any());
    verify(emailService).sendEmailForApproval(any());
    verify(allowanceService).applyPendingRequest(any(), anyBoolean());
    verify(requestRepository).findAllByUserId(anyLong());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(publicHolidayService).findAllBetweenDates(any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldCreateApprovedSicknessRequest() {
    // given
    requestOne.setAbsence(EntityBuilder.absenceSickness());
    requestOne.setRequestStatus(RequestStatus.APPROVED);

    given(userRepository.getOne(1L)).willReturn(mockUser);
    given(requestRepository.findAllByUserId(mockUser.getId())).willReturn(Collections.emptyList());
    given(requestRepository.save(requestOne)).willReturn(requestOne);

    // when
    Request savedRequest = requestService.create(requestOne, mockUser);

    // then
    assertEquals("Expect that request has status 'APPROVED'", savedRequest.getRequestStatus(), RequestStatus.APPROVED);
    assertTrue("Expect that sickness request doesn't need to be approved by approver to have status 'APPROVED'.",
        savedRequest.getCurrentlyApprovedBy().isEmpty());
    verify(userRepository).getOne(anyLong());
    verify(requestRepository).save(any());
    verify(googleCalendarService).handleCreatedRequest(any());
    verify(emailService).sendSicknessRequestCreatedEmail(any());
    verify(allowanceService).applyRequest(any(), anyBoolean());
    verify(requestRepository).findAllByUserId(any());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(publicHolidayService).findAllBetweenDates(any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityNotFoundException.class)
  public void shouldFailToFindUserForCreateRequest() {
    // given
    given(userRepository.getOne(1L)).willThrow(new EntityNotFoundException());

    // when
    requestService.create(requestOne, mockUser);

    // then
    verify(userRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityExistsException.class)
  public void shouldFailToCreateNewRequestDueOverlappingDays() {
    // given
    requestOne.setDays(requestTwo.getDays());

    given(userRepository.getOne(anyLong())).willReturn(mockUser);
    given(requestRepository.findAllByUserId(anyLong())).willReturn(List.of(requestTwo));

    // when
    requestService.create(requestOne, mockUser);

    // then
    verify(userRepository).getOne(anyLong());
    verify(requestRepository).findAllByUserId(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldHandleRequestStatusUpdateRejected() {
    // given
    requestOne.setRequestStatus(RequestStatus.REJECTED);
    requestTwo.getUser().getApprovers().add(mockApprover);

    given(requestRepository.findById(1L)).willReturn(Optional.of(requestTwo));
    given(requestRepository.save(any())).willReturn(requestOne);

    // when
    Request savedRequest = requestService.handleRequestStatusUpdate(requestOne, mockApprover);

    // then
    assertThat("Expect to saved request have status", savedRequest.getRequestStatus(), is(RequestStatus.REJECTED));
    assertEquals("Expect that leave is requested for 'one day'.", 1, savedRequest.getDays().size());
    verify(requestRepository).findById(anyLong());
    verify(allowanceService).applyPendingRequest(any(), anyBoolean());
    verify(requestRepository).save(any());
    verify(googleCalendarService).handleRequestUpdate(any(), anyBoolean());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(emailService).sendRequestStatusUpdateEmail(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldHandleRequestStatusUpdateCancellationPending() {
    // given
    requestOne.setRequestStatus(RequestStatus.CANCELED);
    requestTwo.setRequestStatus(RequestStatus.APPROVED);
    var changedRequest = EntityBuilder.request(absenceAnnual, List.of(dayOne));
    changedRequest.setRequestStatus(RequestStatus.CANCELLATION_PENDING);

    given(requestRepository.findById(1L)).willReturn(Optional.of(requestTwo));
    given(requestRepository.save(any())).willReturn(changedRequest);

    // when
    requestService.handleRequestStatusUpdate(requestOne, mockUser);

    // then
    assertEquals("Expect that handled request has status 'CANCELLATION PENDING", requestOne.getRequestStatus(),
        RequestStatus.CANCELLATION_PENDING);
    verify(requestRepository).findById(anyLong());
    verify(emailService).sendEmailForApproval(any());
    verify(requestRepository).save(any());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldCancelPendingRequest() {
    // given
    requestOne.setRequestStatus(RequestStatus.CANCELED);
    requestOne.setUser(mockUser);

    given(requestRepository.findById(anyLong())).willReturn(Optional.of(requestTwo));
    given(requestRepository.save(any())).willReturn(requestOne);

    // when
    Request savedRequest = requestService.handleRequestStatusUpdate(requestOne, mockUser);

    // then
    assertThat("Expect that saved request has status 'CANCELED'.", savedRequest.getRequestStatus(),
        is(RequestStatus.CANCELED));
    verify(requestRepository).findById(anyLong());
    verify(allowanceService).applyPendingRequest(any(), anyBoolean());
    verify(requestRepository).save(any());
    verify(googleCalendarService).handleRequestUpdate(any(), anyBoolean());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldHandleRequestStatusUpdateApproved() {
    // given=
    var databaseRequest = EntityBuilder.request(absenceAnnual, Collections.singletonList(dayOne));
    databaseRequest.getUser().getApprovers().add(mockApprover);
    requestOne.setRequestStatus(RequestStatus.APPROVED);

    given(requestRepository.findById(anyLong())).willReturn(Optional.of(databaseRequest));
    given(requestRepository.save(any())).willReturn(requestOne);

    // when
    Request savedRequest = requestService.handleRequestStatusUpdate(requestOne, mockApprover);

    // then
    assertThat("Expect that saved request has status 'APPROVED'.", savedRequest.getRequestStatus(),
        is(RequestStatus.APPROVED));
    assertTrue("Expect that request has been approved by 'Bruce Wayne'",
        savedRequest.getCurrentlyApprovedBy().contains(mockApprover));
    verify(requestRepository).findById(anyLong());
    verify(allowanceService).applyPendingRequest(any(), anyBoolean());
    verify(allowanceService).applyRequest(any(), anyBoolean());
    verify(emailService).sendRequestStatusUpdateEmail(any());
    verify(emailService).sendAnnualRequestCreatedEmail(any());
    verify(googleCalendarService).handleRequestUpdate(any(), anyBoolean());
    verify(requestRepository).save(any());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = RequestAlreadyHandledException.class)
  public void shouldFailToHandleRequestStatusUpdateIfRequestIsAlreadyApproved() {
    // given
    requestOne.setRequestStatus(RequestStatus.APPROVED);
    given(requestRepository.findById(1L)).willReturn(Optional.of(requestOne));

    // when
    requestService.handleRequestStatusUpdate(requestOne, mockApprover);

    // then
    verify(requestRepository).findById(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = RequestAlreadyHandledException.class)
  public void shouldFailToHandleRequestStatusUpdateIfRequestIsAlreadyRejected() {
    // given
    requestOne.setRequestStatus(RequestStatus.REJECTED);
    given(requestRepository.findById(1L)).willReturn(Optional.of(requestOne));

    // when
    requestService.handleRequestStatusUpdate(requestOne, mockApprover);

    // then
    verify(requestRepository).findById(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityNotFoundException.class)
  public void shouldFailToHandleRequestStatusUpdate() {
    // given
    given(requestRepository.findById(1L)).willThrow(new EntityNotFoundException());

    // when
    requestService.handleRequestStatusUpdate(requestOne, mockUser);

    // then
    verify(requestRepository).findById(anyLong());
  }

  @Test(expected = NotAuthorizedApprovalException.class)
  public void shouldFailToHandleRequestStatusCancelledBecauseApproverNotAuthorizedToApprove() {
    // given
    requestOne.setRequestStatus(RequestStatus.CANCELED);
    given(requestRepository.findById(1L)).willReturn(Optional.of(requestTwo));

    // when
    requestService.handleRequestStatusUpdate(requestOne, mockApprover);

    // then
    verify(requestRepository).findById(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = RequestAlreadyHandledException.class)
  public void shouldFailToHandleRequestStatusCancelledBecauseRequestAlreadyCancelled() {
    // given
    requestOne.setRequestStatus(RequestStatus.CANCELED);
    requestTwo.setRequestStatus(RequestStatus.CANCELED);
    given(requestRepository.findById(1L)).willReturn(Optional.of(requestTwo));

    // when
    requestService.handleRequestStatusUpdate(requestOne, mockApprover);

    // then
    verify(requestRepository).findById(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = NotAuthorizedApprovalException.class)
  public void shouldFailToHandleRequestStatusApprovedBecauseApproverNotAuthorizedToApprove() {
    // given
    requestOne.setRequestStatus(RequestStatus.APPROVED);
    given(requestRepository.findById(1L)).willReturn(Optional.of(requestTwo));

    // when
    requestService.handleRequestStatusUpdate(requestOne, mockApprover);

    // then
    verify(requestRepository).findById(anyLong());
    verifyNoMoreInteractions(requestRepository);
  }

  @Test(expected = NotAuthorizedApprovalException.class)
  public void shouldFailToHandleRequestStatusRejectedBecauseApproverNotAuthorizedToApprove() {
    // given
    requestOne.setRequestStatus(RequestStatus.REJECTED);
    given(requestRepository.findById(1L)).willReturn(Optional.of(requestTwo));

    // when
    requestService.handleRequestStatusUpdate(requestOne, mockApprover);

    // then
    verify(requestRepository).findById(anyLong());
    verifyNoMoreInteractions(requestRepository);
  }

  @Test
  public void shouldHandleBonusRequestStatusUpdate() {
    // given
    var databaseRequest = EntityBuilder.bonusRequest(Collections.singletonList(dayOne));
    databaseRequest.getUser().getApprovers().addAll(List.of(mockApprover, EntityBuilder.approver()));
    bonusRequest.setRequestStatus(RequestStatus.APPROVED);

    given(requestRepository.findById(1L)).willReturn(Optional.of(databaseRequest));
    given(requestRepository.save(any())).willReturn(bonusRequest);

    // when
    Request savedRequest = requestService.handleRequestStatusUpdate(bonusRequest, mockApprover);

    // then
    assertThat("Expect to saved request have status 'pending'.", savedRequest.getRequestStatus(),
        is(RequestStatus.PENDING));
    assertEquals("Expect that request has exactly 'one approval'.", 1, savedRequest.getCurrentlyApprovedBy().size());
    assertEquals("Expect that exactly 'one more approval' is needed in order to request becomes approved.", 1,
        bonusRequest.getUser().getApprovers().size() - savedRequest.getCurrentlyApprovedBy().size());
    verify(requestRepository).findById(anyLong());
    verify(requestRepository).save(any());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldApproveBonusRequest() {
    // given
    var databaseRequest = EntityBuilder.bonusRequest(Collections.singletonList(dayOne));
    databaseRequest.getUser().getApprovers().addAll(List.of(mockApprover));
    bonusRequest.setRequestStatus(RequestStatus.APPROVED);

    given(requestRepository.findById(1L)).willReturn(Optional.of(databaseRequest));
    given(requestRepository.save(any())).willReturn(bonusRequest);

    // when
    Request savedRequest = requestService.handleRequestStatusUpdate(bonusRequest, mockApprover);

    // then
    assertThat("Expect to saved request have status 'approved'", savedRequest.getRequestStatus(),
        is(RequestStatus.APPROVED));
    verify(requestRepository).findById(anyLong());
    verify(requestRepository).save(any());
    verify(allowanceService).applyPendingRequest(any(), anyBoolean());
    verify(allowanceService).applyRequest(any(), anyBoolean());
    verify(emailService).sendRequestStatusUpdateEmail(any());
    verify(emailService).sendBonusRequestCreatedEmail(any());
    verify(googleCalendarService).handleRequestUpdate(any(), anyBoolean());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldApproveCancellationWhenRequestHasPendingCancellation() {
    // given
    requestOne.setRequestStatus(RequestStatus.CANCELLATION_PENDING);
    requestOne.getUser().getApprovers().add(mockApprover);
    var request = EntityBuilder.request(EntityBuilder.absenceAnnual(), List.of(dayOne));
    request.setRequestStatus(RequestStatus.APPROVED);

    given(requestRepository.findById(1L)).willReturn(Optional.of(requestOne));
    given(requestRepository.save(any())).willReturn(requestOne);

    // when
    requestService.handleRequestStatusUpdate(request, mockApprover);

    // then
    assertThat("Expect that request has been cancelled", request.getRequestStatus(), is(RequestStatus.CANCELED));
    verify(requestRepository).findById(anyLong());
    verify(requestRepository).save(any());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(allowanceService).applyRequest(any(), anyBoolean());
    verify(emailService).sendRequestStatusUpdateEmail(any());
    verify(googleCalendarService).handleRequestUpdate(any(), anyBoolean());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldRejectCancellationWhenRequestHasPendingCancellation() {
    // given
    requestOne.setRequestStatus(RequestStatus.CANCELLATION_PENDING);
    var request = EntityBuilder.request(EntityBuilder.absenceAnnual(), List.of(dayOne));
    request.setRequestStatus(RequestStatus.REJECTED);
    requestOne.getUser().getApprovers().add(mockApprover);

    given(requestRepository.findById(1L)).willReturn(Optional.of(requestOne));
    given(requestRepository.save(any())).willReturn(requestOne);

    // when
    requestService.handleRequestStatusUpdate(request, mockApprover);

    // then
    assertThat("Expect that request has been cancelled", request.getRequestStatus(), is(RequestStatus.APPROVED));
    verify(requestRepository).findById(anyLong());
    verify(requestRepository).save(any());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(emailService).sendRequestStatusUpdateEmail(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllByTeamByMonthOfYear() {
    // given
    var team = EntityBuilder.team();
    List<Request> userOneRequests = List.of(requestOne, requestTwo);
    team.setUsers(Collections.singletonList(mockUser));

    given(teamRepository.getOne(1L)).willReturn(team);
    given(userService.findById(any())).willReturn(mockUser);
    given(requestRepository.findAllRequestsByUserIdOrderByStartingDateDesc(anyLong())).willReturn(userOneRequests);

    // when
    List<Request> requests = requestService.findAllByTeamByMonthOfYear(1L, YearMonth.of(thisYear, 11));

    // then
    assertEquals("Expect that list of request contains exactly 2 elements", 2, requests.size());
    verify(teamRepository).getOne(anyLong());
    verify(requestRepository).findAllRequestsByUserIdOrderByStartingDateDesc(any());
    verify(userService).findById(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldHideSicknessRequestsAbsenceNameWhenFindingAllTeamRequestsByMonthOfYear() {
    // given
    var team = EntityBuilder.team();
    requestOne.setAbsence(EntityBuilder.absenceSickness());
    requestTwo.setAbsence(EntityBuilder.absenceSickness());
    mockUser.setRequests(List.of(requestOne, requestTwo));
    team.setUsers(Collections.singletonList(mockUser));

    given(teamRepository.getOne(1L)).willReturn(team);
    given(userService.findById(any())).willReturn(mockUser);
    given(requestRepository.findAllRequestsByUserIdOrderByStartingDateDesc(anyLong())).willReturn(mockUser.getRequests());

    // when
    List<Request> requests = requestService.findAllByTeamByMonthOfYear(1L, YearMonth.of(thisYear, 11));

    // then
    assertNotNull(requests);
    assertEquals("Expect that list of request contain exactly '2' elements.", 2, requests.size());
    assertThat("Expect that first request leave type is 'Sickness'.", requests.get(0).getAbsence().getName(),
        is("Sickness"));
    assertThat("Expect that second request leave type is 'Sickness'.", requests.get(1).getAbsence().getName(),
        is("Sickness"));
    verify(teamRepository).getOne(anyLong());
    verify(requestRepository).findAllRequestsByUserIdOrderByStartingDateDesc(any());
    verify(userService).findById(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFailToFindAnyRequestByTeamByMonthOfYear() {
    // given
    var team = EntityBuilder.team();
    team.setId(1L);
    var userOne = EntityBuilder.user(team);
    userOne.setRequests(List.of(requestOne));
    List<Request> userOneRequests = new ArrayList<>(userOne.getRequests());
    var userTwo = EntityBuilder.user(team);
    userTwo.setId(2L);
    userTwo.setRequests(List.of(requestTwo));
    List<Request> userTwoRequests = new ArrayList<>(userTwo.getRequests());
    team.setUsers(List.of(userOne, userTwo));

    given(teamRepository.getOne(1L)).willReturn(team);
    given(userService.findById(any())).willReturn(userOne);
    given(requestRepository.findAllRequestsByUserIdOrderByStartingDateDesc(userOne.getId())).willReturn(userOneRequests);
    given(requestRepository.findAllRequestsByUserIdOrderByStartingDateDesc(userTwo.getId())).willReturn(userTwoRequests);

    // when
    List<Request> requests = requestService.findAllByTeamByMonthOfYear(1L, YearMonth.of(thisYear, 1));

    // then
    assertTrue("Expect that list is empty", requests.isEmpty());
    verify(teamRepository).getOne(anyLong());
    verify(requestRepository, times(2)).findAllRequestsByUserIdOrderByStartingDateDesc(any());
    verify(requestRepository, times(2)).findAllRequestsByUserIdOrderByStartingDateDesc(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindFirstAndLastRequestsYear() {
    // given
    given(dayRepository.findFirstByOrderByDateAsc()).willReturn(dayOne);
    given(dayRepository.findFirstByOrderByDateDesc()).willReturn(dayFromDifferentYear);

    // when
    Map<String, Integer> firstAndLastDate = requestService.getFirstAndLastRequestsYear();

    // then
    assertEquals("Expect that map has exactly '2' elements", 2, firstAndLastDate.size());
    verify(dayRepository).findFirstByOrderByDateAsc();
    verify(dayRepository).findFirstByOrderByDateDesc();
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindFirstAndLastRequestsYearBeingTheSame() {
    // given
    given(dayRepository.findFirstByOrderByDateAsc()).willReturn(dayOne);
    given(dayRepository.findFirstByOrderByDateDesc()).willReturn(dayTwo);

    // when
    Map<String, Integer> firstAndLastDate = requestService.getFirstAndLastRequestsYear();

    // then
    assertEquals("Expect that map has exactly '2' elements", 2, firstAndLastDate.size());
    verify(dayRepository).findFirstByOrderByDateAsc();
    verify(dayRepository).findFirstByOrderByDateDesc();
    verifyNoMoreInteractions(allMocks);
  }

}