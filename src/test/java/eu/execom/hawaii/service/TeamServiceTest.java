package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityExistsException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceTest {

  @Mock
  private TeamRepository teamRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private UserService userService;

  @Mock
  private AuditInformationService auditInformationService;

  @InjectMocks
  private TeamService teamService;

  private Team mockTeam;
  private Team mockTeam2;
  private List<Team> mockTeams;
  private User mockUser;
  private User mockApprover;
  private List<User> mockMembers;
  private Object[] allMocks;

  @Before
  public void setUp() {
    mockTeam = EntityBuilder.team();
    mockTeam2 = EntityBuilder.team2();
    mockUser = EntityBuilder.user(mockTeam);
    mockApprover = EntityBuilder.approver();
    mockMembers = new ArrayList<>(List.of(mockApprover, mockUser));
    mockTeams = new ArrayList<>(List.of(mockTeam, mockTeam2));
    allMocks = new Object[] {teamRepository, auditInformationService, userRepository};
  }

  @Test
  public void shouldGetAllTeams() {
    // given
    Page<Team> pagedTeam = new PageImpl<>(mockTeams);
    given(teamRepository.findAllByEditableTrueOrderByName(Pageable.unpaged())).willReturn(pagedTeam);

    // when
    var teams = teamService.findAll(Pageable.unpaged());

    // then
    assertThat("Expect name of first element in list to be My team 1", teams.getContent().get(0).getName(),
        is("My team 1"));
    assertThat("Expect list of teams size to be '2'", teams.getTotalElements(), is(2L));
    verify(teamRepository).findAllByEditableTrueOrderByName(Pageable.unpaged());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldGetTeamById() {
    // given
    given(teamRepository.getOne(anyLong())).willReturn(mockTeam);

    // when
    var team = teamService.getById(mockTeam.getId());

    // then
    assertThat("Expect team name to be 'My team 1'", team.getName(), is("My team 1"));
    verify(teamRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindTeamByTeamName() {
    // given
    Page<Team> pagedTeams = new PageImpl<>(mockTeams);
    given(teamRepository.findByNameContainingAndEditableTrue(anyString(), any())).willReturn(pagedTeams);

    // when
    Page<Team> teams = teamService.searchTeamsByName("test", Pageable.unpaged());

    // then
    assertThat("Expect number of elements in list to be '2'", teams.getTotalElements(), is(2L));
    verify(teamRepository).findByNameContainingAndEditableTrue(anyString(), any());
    verifyNoMoreInteractions(teamRepository);
  }

  @Test
  public void shouldReturnEmptyPageIfSearchQueryIsShorterThanMinimal() {
    // given
    var searchQuery = "ab";
    Pageable pageable = PageRequest.of(0, 2);

    // when
    var result = teamService.searchTeamsByName(searchQuery, pageable);

    // then
    assertThat("Expect that result has one empty page.", result.getTotalPages(), is(1));
    assertThat("Expect that there is '0' total elements.", result.getTotalElements(), is(0L));
    assertThat("Expect result of search to be empty page", result, is(Page.empty()));
  }

  @Test
  public void shouldFindAllUsersTeamsForUser() {
    // given
    mockUser.setUserRole(UserRole.USER);

    // when
    List<Team> userTeams = teamService.getTeamsBasedOnUserRole(mockUser);

    // then
    assertThat("Expect that user doesn't have a team", userTeams.size(), is(0));
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllUsersTeamsForApprover() {
    // given
    mockApprover.setApproverTeams(List.of(mockTeam));
    mockApprover.setUserRole(UserRole.USER);
    given(userService.findById(anyLong())).willReturn(mockApprover);

    // when
    List<Team> userTeams = teamService.getTeamsBasedOnUserRole(mockApprover);

    // then
    assertThat(
        "Expect that user teams list contains all teams where user is approver with one additional team where user is member",
        userTeams.size(), is(mockApprover.getApproverTeams().size() + 1));
    verify(userService).findById(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllUsersTeamsForHrManager() {
    // given
    Page<Team> pagedTeam = new PageImpl<>(mockTeams);
    given(teamRepository.findAllByEditableTrueOrderByName(Pageable.unpaged())).willReturn(pagedTeam);

    // when
    List<Team> userTeams = teamService.getTeamsBasedOnUserRole(mockUser);

    // then
    assertThat("Expect that user has access to 2 teams", userTeams.size(), is(2));
    verify(teamRepository).findAllByEditableTrueOrderByName(Pageable.unpaged());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldCreateTeam() {
    // given
    var user = EntityBuilder.approver();
    given(teamRepository.existsByName(anyString())).willReturn(false);
    given(teamRepository.create(mockTeam)).willReturn(mockTeam);

    // when
    var team = teamService.create(mockTeam, user);

    // then
    assertNotNull(team);
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(teamRepository).existsByName(anyString());
    verify(teamRepository).create(mockTeam);
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityExistsException.class)
  public void shouldFailToCreateTeamIfTeamWithGivenNameAlreadyExist() {
    // given
    given(teamRepository.existsByName(anyString())).willReturn(true);

    // when
    teamService.create(mockTeam, mockUser);

    // then
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldFailToCreateTeamIfInputDataIsTooLong() {
    // given
    given(teamRepository.existsByName(anyString())).willReturn(false);
    doThrow(new DataIntegrityViolationException("test")).when(teamRepository).create(any());

    // when
    teamService.create(mockTeam, mockUser);

    // then
    verify(teamRepository).existsByName(anyString());
  }

  @Test
  public void shouldDeleteTeam() {
    // given
    given(teamRepository.getOne(anyLong())).willReturn(mockTeam);

    // when
    teamService.delete(mockTeam.getId(), any());

    // then
    verify(teamRepository).getOne(anyLong());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(teamRepository).deleteById(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToDeleteTeamIfTeamIsNotEditable() {
    // given
    mockTeam.setEditable(false);
    given(teamRepository.getOne(anyLong())).willReturn(mockTeam);

    // when
    teamService.delete(1L, mockUser);

    // then
    verify(teamRepository).getOne(anyLong());
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToDeleteTeamIfTeamMemberListIsNotEmpty() {
    // given
    mockTeam.setUsers(List.of(mockUser));
    given(teamRepository.getOne(anyLong())).willReturn(mockTeam);

    // when
    teamService.delete(mockTeam.getId(), mockUser);

    // then
    verify(teamRepository).getOne(anyLong());
  }

  @Test
  public void shouldUpdateTeam() {
    // given
    mockTeam.setUsers(List.of(EntityBuilder.approver()));
    mockTeam2.setUsers(mockMembers);

    given(teamRepository.getOne(any())).willReturn(mockTeam);
    given(teamRepository.existsByName(anyString())).willReturn(false);
    given(teamRepository.save(mockTeam2)).willReturn(mockTeam);
    given(teamRepository.findByIdJoinFetchTeamApprovers(anyLong())).willReturn(mockTeam);

    // when
    teamService.update(mockTeam2, mockUser);

    // then
    assertEquals("There is '1' employee in mockTeam", 1, mockTeam.getUsers().size());
    assertEquals("There are '2' employees in mockTeam2", 2, mockTeam2.getUsers().size());
    verify(teamRepository).save(any());
    verify(teamRepository).existsByName(anyString());
    verify(teamRepository).getOne(any());
    verify(teamRepository).findByIdJoinFetchTeamApprovers(anyLong());
    verifyNoMoreInteractions(teamRepository);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToUpdateTeamIfTeamNotEditable() {
    // given
    mockTeam.setEditable(false);
    given(teamRepository.getOne(anyLong())).willReturn(mockTeam);

    // when
    teamService.update(mockTeam, mockUser);

    // then
  }

  @Test(expected = EntityExistsException.class)
  public void shouldFailToUpdateTeamIfTeamWithGivenNameAlreadyExist() {
    // given
    given(teamRepository.getOne(anyLong())).willReturn(mockTeam);
    given(teamRepository.existsByName(anyString())).willReturn(true);

    // when
    teamService.update(mockTeam2, mockUser);

    // then
  }

}