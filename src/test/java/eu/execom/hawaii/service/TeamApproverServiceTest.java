package eu.execom.hawaii.service;

import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class TeamApproverServiceTest {

  @Mock
  private TeamRepository teamRepository;

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private TeamApproverService teamApproverService;

  private User mockUser;

  private User updatedMockUser;

  private Team mockTeam;

  private Object[] allMocks;

  @Before
  public void setUp() {
    mockTeam = EntityBuilder.team2();
    mockUser = EntityBuilder.approver();
    updatedMockUser = EntityBuilder.approver();
    allMocks = new Object[] {teamRepository, userRepository};
  }

  @Test
  public void shouldRemoveApproverTeam() {
    // given
    mockTeam.getTeamApprovers().add(mockUser);
    mockUser.getApproverTeams().add(mockTeam);

    given(userRepository.findById(updatedMockUser.getId())).willReturn(Optional.of(mockUser));
    given(teamRepository.save(any())).willReturn(mockTeam);

    // when
    teamApproverService.handleApproverTeamsChange(updatedMockUser);

    // then
    assertThat("Expect team approver list to be empty", mockTeam.getTeamApprovers().size(), is(0));
    verify(userRepository).findById(any());
    verify(teamRepository).save(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldAddApproverTeam() {
    // given
    updatedMockUser.getApproverTeams().add(mockTeam);

    given(userRepository.findById(updatedMockUser.getId())).willReturn(Optional.of(mockUser));
    given(teamRepository.getOne(anyLong())).willReturn(mockTeam);
    given(teamRepository.save(any())).willReturn(mockTeam);

    // when
    teamApproverService.handleApproverTeamsChange(updatedMockUser);

    // then
    assertThat("Expect team approver list size to be '1'", mockTeam.getTeamApprovers().size(), is(1));
    verify(userRepository).findById(any());
    verify(teamRepository).getOne(anyLong());
    verify(teamRepository).save(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldAddUserToTeamApprovers() {
    // given
    mockUser.setApproverTeams(List.of(mockTeam));
    given(teamRepository.getOne(anyLong())).willReturn(mockTeam);
    given(teamRepository.save(any())).willReturn(mockTeam);

    // when
    teamApproverService.addUserToTeamApprovers(mockUser);
    System.out.println(mockTeam.getTeamApprovers());

    // then
    assertThat("Expect that user is approver in team", mockTeam.getTeamApprovers().contains(mockUser), is(true));
    verify(teamRepository).getOne(anyLong());
    verify(teamRepository).save(any());
    verifyNoMoreInteractions(teamRepository);
  }
}
