package eu.execom.hawaii.service;

import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.Request;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.repository.DayRepository;
import eu.execom.hawaii.repository.RequestRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class DayServiceTest {

  @Mock
  private DayRepository dayRepository;
  @Mock
  private RequestRepository requestRepository;
  @Mock
  private RequestService requestService;

  @InjectMocks
  private DayService dayService;

  private User mockUser;
  private LocalDate date;
  private LocalDate date2;
  private List<Request> mockRequests = new ArrayList<>();
  private List<Day> mockDays;
  private Object[] allMocks;

  @Before
  public void setUp() {
    Day day1 = EntityBuilder.day(LocalDate.of(2019, 2, 1));
    Day day2 = EntityBuilder.day(LocalDate.of(2019, 2, 5));
    mockDays = List.of(day1, day2);
    var mockRequest = EntityBuilder.request(EntityBuilder.absenceAnnual(), List.of(day1, day2));
    mockRequests = List.of(mockRequest);
    mockUser = mockRequest.getUser();
    date = LocalDate.of(2019, 01, 01);
    date2 = LocalDate.of(2019, 10, 01);

    allMocks = new Object[] {requestRepository, dayRepository, requestService};
  }

  @Test
  public void shouldGetUserAbsencesDays() {
    //given
    given(requestService.withoutCanceledRejectedBonus()).willReturn((Request::isApproved));
    given(requestRepository.findAllByUserId(any())).willReturn(mockRequests);
    given(dayRepository.findAllByRequestInAndDateIsBetween(any(), any(), any())).willReturn(mockDays);

    //when
    List<Day> days = dayService.getUserAbsencesDays(mockUser, date, date2);

    //then
    assertNotNull(mockDays);
    assertThat("Expect list size to be two", days.size(), is(2));
    verify(requestRepository).findAllByUserId(any());
    verify(requestService).withoutCanceledRejectedBonus();
    verify(dayRepository).findAllByRequestInAndDateIsBetween(any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

}