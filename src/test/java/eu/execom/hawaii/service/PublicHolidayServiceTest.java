package eu.execom.hawaii.service;

import eu.execom.hawaii.model.PublicHoliday;
import eu.execom.hawaii.repository.PublicHolidayRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityExistsException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class PublicHolidayServiceTest {

  @Mock
  private PublicHolidayRepository publicHolidayRepository;

  @InjectMocks
  private PublicHolidayService publicHolidayService;

  private PublicHoliday mockPublicHoliday;
  private List<PublicHoliday> mockPublicHolidays;

  @Before
  public void setUp() {
    mockPublicHoliday = EntityBuilder.publicholiday();
    var mockPublicHoliday2 = EntityBuilder.publicholiday();

    mockPublicHolidays = new ArrayList<>(Arrays.asList(mockPublicHoliday, mockPublicHoliday2));
  }

  @Test
  public void shouldGetPublicHolidayById() {
    // given
    var userId = 1L;
    given(publicHolidayRepository.getOne(userId)).willReturn(mockPublicHoliday);

    // when
    PublicHoliday publicHoliday = publicHolidayService.getById(userId);

    // then
    assertThat("Expect name to be New year", publicHoliday.getName(), is("New year"));
    verify(publicHolidayRepository).getOne(anyLong());
    verifyNoMoreInteractions(publicHolidayRepository);
  }

  @Test
  public void shouldGetAllPublicHolidays() {
    // given
    mockPublicHolidays.get(0).setDeleted(true);
    Page<PublicHoliday> pagedHolidays = new PageImpl<>(mockPublicHolidays);
    given(publicHolidayRepository.findAll(Pageable.unpaged())).willReturn(pagedHolidays);

    // when
    Page<PublicHoliday> publicHolidays = publicHolidayService.findAll(Pageable.unpaged());

    // then
    assertThat("Expect that there are '2' public holidays", publicHolidays.getTotalElements(), is(2L));
    assertTrue("Expect that first one is deleted", publicHolidays.getContent().get(0).isDeleted());
    assertFalse("Expect that second one is deleted", publicHolidays.getContent().get(1).isDeleted());
    verify(publicHolidayRepository).findAll(Pageable.unpaged());
    verifyNoMoreInteractions(publicHolidayRepository);
  }

  @Test
  public void shouldGetAllActivePublicHolidays() {
    // given
    given(publicHolidayRepository.findAllByDeletedOrderByNameAsc(false)).willReturn(mockPublicHolidays);

    // when
    List<PublicHoliday> publicHolidays = publicHolidayService.findAllByDeleted(false);

    // then
    assertThat("Expect that there are '2' public holidays", publicHolidays.size(), is(2));
    assertFalse("Expect that first one is deleted", publicHolidays.get(0).isDeleted());
    assertFalse("Expect that second one is deleted", publicHolidays.get(1).isDeleted());
    verify(publicHolidayRepository).findAllByDeletedOrderByNameAsc(anyBoolean());
    verifyNoMoreInteractions(publicHolidayRepository);
  }

  @Test
  public void shouldGetAllDeletedPublicHolidays() {
    // given
    mockPublicHolidays.get(0).setDeleted(true);
    mockPublicHolidays.get(1).setDeleted(true);
    given(publicHolidayRepository.findAllByDeletedOrderByNameAsc(true)).willReturn(mockPublicHolidays);

    // when
    List<PublicHoliday> publicHolidays = publicHolidayService.findAllByDeleted(true);

    // then
    assertThat("Expect that there are '2' public holidays", publicHolidays.size(), is(2));
    assertTrue("Expect that first one is deleted", publicHolidays.get(0).isDeleted());
    assertTrue("Expect that second one is deleted", publicHolidays.get(1).isDeleted());
    verify(publicHolidayRepository).findAllByDeletedOrderByNameAsc(anyBoolean());
    verifyNoMoreInteractions(publicHolidayRepository);
  }

  @Test
  public void shouldFindAllByYear() {
    // given
    var currentYear = LocalDate.now().getYear();
    var startDate = LocalDate.of(currentYear, 1, 1);
    var endDate = LocalDate.of(currentYear, 12, 31);
    given(publicHolidayRepository.findAllByDateIsBetweenOrderByDate(startDate, endDate)).willReturn(mockPublicHolidays);

    // when
    var publicHolidays = publicHolidayService.findAllByYear(currentYear);

    // then
    assertNotNull(publicHolidays);
    assertThat("Expect there is exactly 2 public holidays in public holidays list", publicHolidays.size(), is(2));
    assertThat("Expect that list of public holidays contain public holiday with name 'New year",
        publicHolidays.stream().anyMatch(publicHoliday -> publicHoliday.getName().equals("New year")), is(true));
    verify(publicHolidayRepository).findAllByDateIsBetweenOrderByDate(startDate, endDate);
    verifyNoMoreInteractions(publicHolidayRepository);
  }

  @Test
  public void shouldSavePublicHoliday() {
    // given
    given(publicHolidayRepository.existsByDate(mockPublicHoliday.getDate())).willReturn(false);
    given(publicHolidayRepository.save(mockPublicHoliday)).willReturn(mockPublicHoliday);

    // when
    PublicHoliday publicHoliday = publicHolidayService.save(mockPublicHoliday);

    // then
    assertThat("Expect name to be New year", publicHoliday.getName(), is(mockPublicHoliday.getName()));
    verify(publicHolidayRepository).existsByDate(mockPublicHoliday.getDate());
    verify(publicHolidayRepository).save(ArgumentMatchers.any());
    verifyNoMoreInteractions(publicHolidayRepository);
  }

  @Test(expected = EntityExistsException.class)
  public void shouldFailToSavePublicHolidayIfPublicHolidayWithGivenDateAlreadyExist() {
    // given
    given(publicHolidayRepository.existsByDate(mockPublicHoliday.getDate())).willReturn(true);

    // when
    publicHolidayService.save(mockPublicHoliday);

    // then
    verify(publicHolidayRepository).existsByDate(any());
    verifyNoMoreInteractions(publicHolidayRepository);
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldFailToSavePublicHolidayIfPublicHolidayNameIsTooLong() {
    // given
    given(publicHolidayRepository.existsByDate(mockPublicHoliday.getDate())).willReturn(false);
    doThrow(new DataIntegrityViolationException("test")).when(publicHolidayRepository).save(mockPublicHoliday);

    // when
    publicHolidayService.save(mockPublicHoliday);

    // then
    verify(publicHolidayRepository).existsByDate(any());
    verifyNoMoreInteractions(publicHolidayRepository);
  }

  @Test(expected = EntityExistsException.class)
  public void shouldFailToUpdateIfPublicHolidayWithGivenDateAlreadyExists() {
    // given
    var updatedPublicHoliday = EntityBuilder.publicholiday();
    updatedPublicHoliday.setDate(LocalDate.of(2018, 1, 6));
    given(publicHolidayRepository.findById(anyLong())).willReturn(Optional.of(mockPublicHoliday));
    given(publicHolidayRepository.existsByDate(updatedPublicHoliday.getDate())).willReturn(true);

    // when
    publicHolidayService.update(updatedPublicHoliday);

    // then
    verify(publicHolidayRepository).getOne(anyLong());
    verify(publicHolidayRepository).existsByDate(any());
    verifyNoMoreInteractions(publicHolidayRepository);
  }

}