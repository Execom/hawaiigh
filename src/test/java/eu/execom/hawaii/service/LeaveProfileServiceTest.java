package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.model.LeaveProfile;
import eu.execom.hawaii.model.enumerations.LeaveProfileType;
import eu.execom.hawaii.repository.LeaveProfileRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class LeaveProfileServiceTest {

  @Mock
  private LeaveProfileRepository leaveProfileRepository;

  @Mock
  private AllowanceService allowanceService;

  @InjectMocks
  private LeaveProfileService leaveProfileService;

  private LeaveProfile mockLeaveProfile;
  private List<LeaveProfile> leaveProfiles;

  @Before
  public void setUp() {
    mockLeaveProfile = EntityBuilder.leaveProfile();
    LeaveProfile mockLeaveProfile2 = EntityBuilder.leaveProfileII();

    leaveProfiles = List.of(mockLeaveProfile, mockLeaveProfile2);
  }

  @Test
  public void shouldGetAllLeaveProfiles() {
    // given
    Page<LeaveProfile> pagedLeaveProfiles = new PageImpl<>(leaveProfiles);
    given(leaveProfileRepository.findAll(Pageable.unpaged())).willReturn(pagedLeaveProfiles);

    // when
    Page<LeaveProfile> leaveProfiles = leaveProfileService.findAll(Pageable.unpaged());

    // then
    assertNotNull(leaveProfiles);
    assertThat("Expect that there are '2' leave profiles", leaveProfiles.getTotalElements(), is(2L));
    assertThat("Expect first one tobe named 'Default'", leaveProfiles.getContent().get(0).getName(), is("Default"));
    verify(leaveProfileRepository).findAll(Pageable.unpaged());
    verifyNoMoreInteractions(leaveProfileRepository);
  }

  @Test
  public void shouldGetLeaveProfileById() {
    // given
    var leaveProfileId = 1L;
    given(leaveProfileRepository.getOne(leaveProfileId)).willReturn(mockLeaveProfile);

    // when
    var leaveProfile = leaveProfileService.getById(leaveProfileId);

    // then
    assertThat("Expect name to be 'Default'", leaveProfile.getName(), is("Default"));
    verify(leaveProfileRepository).getOne(anyLong());
    verifyNoMoreInteractions(leaveProfileRepository);
  }

  @Test
  public void shouldCreateCustomLeaveProfile() {
    // given
    given(leaveProfileRepository.save(any())).willReturn(mockLeaveProfile);

    // when
    var leaveProfile = leaveProfileService.create(mockLeaveProfile);

    // then
    assertThat("Expect leave profile type to be 'CUSTOM'", leaveProfile.getLeaveProfileType(),
        is(LeaveProfileType.CUSTOM));
    assertThat("Expect leave profile to not be upgradeable", leaveProfile.isUpgradeable(), is(false));
    verify(leaveProfileRepository).save(any());
    verifyNoMoreInteractions(leaveProfileRepository);
  }

  @Test
  public void shouldUpdateLeaveProfile() {
    // given
    var updatedLeaveProfile = mockLeaveProfile;
    updatedLeaveProfile.setComment("test");
    given(leaveProfileRepository.save(any())).willReturn(updatedLeaveProfile);

    // when
    leaveProfileService.update(mockLeaveProfile);

    // then
    assertThat("Expect leave profile comment to be 'test'", mockLeaveProfile.getComment(), is("test"));
    verify(leaveProfileRepository).save(any());
    verifyNoMoreInteractions(leaveProfileRepository);
  }

  @Test
  public void shouldDeleteLeaveProfile() {
    // given
    mockLeaveProfile.setLeaveProfileType(LeaveProfileType.CUSTOM);
    given(leaveProfileRepository.getById(anyLong())).willReturn(Optional.of(mockLeaveProfile));

    // when
    leaveProfileService.delete(1L);

    // then
    verify(leaveProfileRepository).getById(anyLong());
    verify(leaveProfileRepository).delete(any());
    verifyNoMoreInteractions(leaveProfileRepository);
  }

  @Test(expected = EntityNotFoundException.class)
  public void shouldFailToDeleteLeaveProfileWhenEntityHaveNotBeenFound() {
    // given
    Mockito.doThrow(new EntityNotFoundException()).when(leaveProfileRepository).getById(anyLong());

    // when
    leaveProfileService.delete(1L);

    // then
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToDeleteLeaveProfileWhichIsNotCustom() {
    // given
    given(leaveProfileRepository.getById(anyLong())).willReturn(Optional.of(mockLeaveProfile));

    // when
    leaveProfileService.delete(1L);

    // then
  }

}