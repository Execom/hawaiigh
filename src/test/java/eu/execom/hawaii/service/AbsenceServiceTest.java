package eu.execom.hawaii.service;

import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.model.Absence;
import eu.execom.hawaii.model.enumerations.AbsenceType;
import eu.execom.hawaii.repository.AbsenceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class AbsenceServiceTest {

  @Mock
  private AbsenceRepository absenceRepository;

  @InjectMocks
  private AbsenceService absenceService;

  private Absence mockAbsence;
  private Absence mockTrainingAbsence;
  private List<Absence> absences;

  @Before
  public void setUp() {
    mockAbsence = EntityBuilder.absenceAnnual();
    mockTrainingAbsence = EntityBuilder.absenceTraining();
    var mockAbsence2 = EntityBuilder.absenceAnnual();
    var mockAbsence3 = EntityBuilder.absenceBonus();
    absences = Arrays.asList(mockAbsence, mockAbsence2, mockAbsence3);
  }

  @Test
  public void shouldGetAllAbsences() {
    // given
    Page<Absence> pagedAbsences = new PageImpl<>(absences);
    given(absenceRepository.findAll(Pageable.unpaged())).willReturn(pagedAbsences);

    // when
    Page<Absence> absences = absenceService.findAll(Pageable.unpaged());

    // then
    assertNotNull(absences);
    assertThat("Expect that there are '3' absences", absences.getTotalElements(), is(3L));
    assertThat("Expect fist in list to be named 'Annual leave'", absences.getContent().get(0).getName(),
        is("Annual leave"));
    verify(absenceRepository).findAll(Pageable.unpaged());
    verifyNoMoreInteractions(absenceRepository);
  }

  @Test
  public void shouldGetAbsenceById() {
    // given
    var absenceId = 1L;
    given(absenceRepository.getOne(absenceId)).willReturn(mockAbsence);

    // when
    var absence = absenceService.getById(absenceId);

    // then
    assertThat("Expect name to be 'Annual leave'", absence.getName(), is("Annual leave"));
    verify(absenceRepository).getOne(anyLong());
    verifyNoMoreInteractions(absenceRepository);
  }

  @Test
  public void shouldSaveAbsence() {
    mockAbsence = EntityBuilder.absenceAnnual();
    // given
    given(absenceRepository.save(mockAbsence)).willReturn(mockAbsence);

    // when
    Absence absence = absenceService.save(mockAbsence);

    // then
    assertNotNull(absence);
    verify(absenceRepository).save(mockAbsence);
    verifyNoMoreInteractions(absenceRepository);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToSaveAbsenceIfAbsenceTypeBonus() {
    // given
    mockAbsence.setAbsenceType(AbsenceType.BONUS);

    // when
    absenceService.save(mockAbsence);

    // then
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToSaveAbsenceIfAbsenceTypeTraining() {
    // given

    // when
    absenceService.save(mockTrainingAbsence);

    // then
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldFailToSaveAbsenceIfAbsenceWithGivenNameExists() {
    // given
    mockAbsence.setName("Annual Leave");
    given(absenceRepository.save(any())).willThrow(new DataIntegrityViolationException("Absence name '" + mockAbsence.getName() + "' already exists."));

    // when
    absenceService.save(mockAbsence);

    // then
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldFailToSaveAbsenceIfAbsenceNameTooLong() {
    // given
    mockAbsence.setName(" ".repeat(101));
    given(absenceRepository.save(any())).willThrow(new DataIntegrityViolationException("Name is too long"));

    // when
    absenceService.save(mockAbsence);

    // then
  }

  @Test
  public void shouldUpdateAbsence() {
    // given
    var updatedAbsence = mockAbsence;
    updatedAbsence.setName("test");
    updatedAbsence.setAbsenceType(AbsenceType.LEAVE);

    given(absenceRepository.save(mockAbsence)).willReturn(updatedAbsence);

    // when
    absenceService.update(mockAbsence);

    // then
    assertThat("Expect absence name to be 'test'", mockAbsence.getName(), is("test"));
    verify(absenceRepository).save(any());
    verifyNoMoreInteractions(absenceRepository);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToUpdateAbsence() {
    // given
    mockTrainingAbsence.setDeducted(false);

    // when
    absenceService.update(mockTrainingAbsence);

    // then
  }

  @Test
  public void shouldDeleteAbsence() {
    mockAbsence = EntityBuilder.absenceAnnual();
    // given
    var absenceId = mockAbsence.getId();
    given(absenceRepository.getById(anyLong())).willReturn(Optional.of(mockAbsence));

    // when
    absenceService.delete(absenceId);

    // then
    verify(absenceRepository).getById(anyLong());
    verify(absenceRepository).deleteById(anyLong());
    verifyNoMoreInteractions(absenceRepository);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToDeleteAbsenceIfAbsenceTypeBonus() {
    // given
    mockAbsence.setAbsenceType(AbsenceType.BONUS);
    given(absenceRepository.getById(anyLong())).willReturn(Optional.of(mockAbsence));

    // when
    absenceService.delete(mockAbsence.getId());

    // then
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToDeleteAbsenceIfAbsenceTypeTraining() {
    // given
    given(absenceRepository.getById(anyLong())).willReturn(Optional.of(mockTrainingAbsence));

    // when
    absenceService.delete(mockTrainingAbsence.getId());

    // then
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldFailToDeleteAbsenceBecauseItIsUsedInApplication() {
    // given
    var absenceId = mockAbsence.getId();
    given(absenceRepository.getById(anyLong())).willReturn(Optional.of(mockAbsence));
    doThrow(new DataIntegrityViolationException("test")).when(absenceRepository).deleteById(absenceId);

    // when
    absenceService.delete(absenceId);

    // then
  }
}