package eu.execom.hawaii.service;

import eu.execom.hawaii.dto.CreateTokenDto;
import eu.execom.hawaii.exceptions.ActionNotAllowedException;
import eu.execom.hawaii.exceptions.UnauthorizedAccessException;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.Team;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.UserPushToken;
import eu.execom.hawaii.model.Year;
import eu.execom.hawaii.model.enumerations.UserAdminPermission;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.repository.LeaveProfileRepository;
import eu.execom.hawaii.repository.TeamRepository;
import eu.execom.hawaii.repository.UserPushTokensRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.repository.YearRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;

import javax.persistence.EntityExistsException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private LeaveProfileRepository leaveProfileRepository;

  @Mock
  private YearRepository yearRepository;

  @Mock
  private AuditInformationService auditInformationService;

  @Mock
  private TeamRepository teamRepository;

  @Mock
  private AllowanceService allowanceService;

  @Mock
  private UserPushTokensRepository userPushTokensRepository;

  @InjectMocks
  private UserService userService;

  private User mockUser;
  private User mockUser2;
  private User mockUser3;
  private User mockApprover;
  private User mockSuperAdmin;
  private User mockAdmin;
  private User mockRevokedAdmin;
  private Team mockTeam;
  private Year thisYear;
  private Year nextYear;
  private List<Allowance> userAllowances;
  private List<User> initialUsers;
  private Object[] allMocks;

  @Before
  public void setUp() {
    mockTeam = EntityBuilder.team2();
    mockUser = EntityBuilder.user(mockTeam);
    mockUser2 = EntityBuilder.approver();
    mockUser3 = EntityBuilder.user2();
    mockUser3.setTeam(mockTeam);
    mockApprover = EntityBuilder.approver2();
    mockSuperAdmin = EntityBuilder.superAdmin();
    mockAdmin = EntityBuilder.admin();
    mockRevokedAdmin = EntityBuilder.revokedAdmin();
    mockTeam.setTeamApprovers(Collections.singletonList(mockApprover));
    thisYear = EntityBuilder.thisYear();
    nextYear = EntityBuilder.nextYear();

    userAllowances = new ArrayList<>(
        List.of(EntityBuilder.allowance(mockUser), EntityBuilder.nextYearAllowance(mockUser)));
    initialUsers = new ArrayList<>(Arrays.asList(mockUser, mockUser2, mockApprover, mockUser3));
    allMocks = new Object[] {userRepository, leaveProfileRepository, yearRepository, auditInformationService,
        teamRepository, allowanceService, userPushTokensRepository};
  }

  @Test
  public void shouldFindAllActiveUsersByTeam() {
    // given
    given(userRepository.findAllByUserStatusTypeAndTeam(any(), any())).willReturn(initialUsers);

    // when
    List<User> activeUsers = userService.findAllActiveUsersByTeam(mockTeam);

    // then
    assertThat("Expect that team has '5' active users", activeUsers.size(), is(5));
    assertThat("Expect that first user is 'ACTIVE'", activeUsers.get(0).getUserStatusType(), is(UserStatusType.ACTIVE));
    assertThat("Expect that second user is 'ACTIVE'", activeUsers.get(1).getUserStatusType(),
        is(UserStatusType.ACTIVE));

    verify(userRepository).findAllByUserStatusTypeAndTeam(any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFindAllByUserStatusType() {
    // given
    given(userRepository.findAllByUserStatusType(any())).willReturn(initialUsers);

    // when
    List<User> users = userService.findAllByUserStatusType(any());

    // then
    assertThat("Expect that there are '4' users with that user status", users.size(), is(4));
    assertThat("Expect first user to be named 'Aria Stark'", users.get(0).getFullName(), is("Aria Stark"));
    assertThat("Expect second user to be named 'Bruce Wayne'", users.get(1).getFullName(), is("Bruce Wayne"));

    verify(userRepository).findAllByUserStatusType(any());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldReturnEmptyListForSearchedUsersBecauseOfShortSearchQuery() {
    // given
    var activeUsers = Collections.singletonList(UserStatusType.ACTIVE);
    String searchQuery = "as";

    // when
    Page<User> users = userService.searchUsers(mockUser, activeUsers, searchQuery, true, Pageable.unpaged());

    // then
    assertTrue("Expect that empty list is returned", users.isEmpty());

    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldGetSearchedUsersWithLoggedUserForHrManagerRole() {
    // given
    var activeUsers = Collections.singletonList(UserStatusType.ACTIVE);
    PageRequest pageRequest = PageRequest.of(0, 5);
    given(userRepository.searchByStatusTypeEmailAndFullName(any(), any(), any())).willReturn(initialUsers);

    // when
    Page<User> users = userService.searchUsers(mockUser, activeUsers, "execom", true, pageRequest);

    // then
    assertThat("Expect that '4' users passed search criteria", initialUsers.size(), is(4));
    assertThat("Expect that '4' users were returned", users.getContent().size(), is(4));
    assertTrue("Expect that logged user is included", users.getContent().contains(mockUser));

    verify(userRepository).searchByStatusTypeEmailAndFullName(any(), anyString(), anyString());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldGetSearchedUsersWithoutLoggedUserForHrManagerRole() {
    // given
    var activeUsers = Collections.singletonList(UserStatusType.ACTIVE);
    PageRequest pageRequest = PageRequest.of(0, 5);
    given(userRepository.searchByStatusTypeEmailAndFullName(any(), any(), any())).willReturn(initialUsers);

    // when
    Page<User> users = userService.searchUsers(mockUser, activeUsers, "execom", false, pageRequest);

    // then
    assertThat("Expect that '4' users passed search criteria", initialUsers.size(), is(4));
    assertThat("Expect that '3' users were returned", users.getContent().size(), is(3));
    assertFalse("Expect that logged user is not included", users.getContent().contains(mockUser));

    verify(userRepository).searchByStatusTypeEmailAndFullName(any(), anyString(), anyString());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldGetSearchedUsersWithoutLoggedUserForApproverRole() {
    // given
    var activeUsers = Collections.singletonList(UserStatusType.ACTIVE);
    PageRequest pageRequest = PageRequest.of(0, 5);
    given(userRepository.searchByStatusTypeEmailAndFullName(any(), any(), any())).willReturn(initialUsers);

    // when
    Page<User> users = userService.searchUsers(mockApprover, activeUsers, "execom", false, pageRequest);

    // then
    assertThat("Expect that '4' users passed search criteria", initialUsers.size(), is(4));
    assertThat("Expect that '2' users passed search criteria and are in team where logged user is approver",
        users.getContent().size(), is(2));
    assertFalse("Expect that logged user is not included", users.getContent().contains(mockApprover));

    verify(userRepository).searchByStatusTypeEmailAndFullName(any(), anyString(), anyString());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldGetSearchedUsersWithLoggedUserForApproverRole() {
    // given
    var activeUsers = Collections.singletonList(UserStatusType.ACTIVE);
    PageRequest pageRequest = PageRequest.of(0, 5);
    given(userRepository.searchByStatusTypeEmailAndFullName(any(), any(), any())).willReturn(initialUsers);

    // when
    Page<User> users = userService.searchUsers(mockApprover, activeUsers, "execom", true, pageRequest);

    // then
    assertThat("Expect that '4' users passed search criteria", initialUsers.size(), is(4));
    assertThat("Expect that '3' users passed search criteria and is in team where logged user is approver",
        users.getContent().size(), is(3));
    assertTrue("Expect that logged user is included", users.getContent().contains(mockApprover));

    verify(userRepository).searchByStatusTypeEmailAndFullName(any(), anyString(), anyString());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldGetUserById() {
    // given
    given(userRepository.findById(mockUser.getId())).willReturn(Optional.of(mockUser));

    // when
    User user = userService.findById(mockUser.getId());

    // then
    assertThat("Expect name to be Aria Stark", user.getFullName(), is(mockUser.getFullName()));

    verify(userRepository).findById(anyLong());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldGetAuthUserByIdWhenUserIdIsNull() {
    // given

    // when
    User user = userService.findUserById(null, mockApprover);

    // then
    assertThat("Expect name to be 'Bruce Wayne'", user.getFullName(), is(mockApprover.getFullName()));

    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldGetUserByIdWhenAuthUserIsApprover() {
    // given
    mockUser.getTeam().setTeamApprovers(Collections.singletonList(mockApprover));
    given(userRepository.findById(mockUser.getId())).willReturn(Optional.of(mockUser));

    // when
    User user = userService.findUserById(mockUser.getId(), mockApprover);

    // then
    assertThat("Expect name to be 'Aria Stark'", user.getFullName(), is(mockUser.getFullName()));

    verify(userRepository).findById(anyLong());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldGetUserByIdWhenAuthUserIsHrManager() {
    // given
    mockUser.getTeam().setTeamApprovers(Collections.singletonList(mockApprover));
    given(userRepository.findById(mockUser.getId())).willReturn(Optional.of(mockUser));

    // when
    User user = userService.findUserById(mockUser.getId(), mockUser2);

    // then
    assertThat("Expect name to be 'Aria Stark'", user.getFullName(), is(mockUser.getFullName()));

    verify(userRepository).findById(anyLong());
    verifyNoMoreInteractions(userRepository);
  }

  @Test(expected = AccessDeniedException.class)
  public void shouldFailToGetUserByIdWhenAuthUserIsNotApproverOrHrManager() {
    // given
    mockUser2.setUserRole(UserRole.USER);
    mockUser.getTeam().setTeamApprovers(Collections.singletonList(mockApprover));
    given(userRepository.findById(mockUser.getId())).willReturn(Optional.of(mockUser));

    // when
    userService.findUserById(mockUser.getId(), mockUser2);

    // then
  }

  @Test(expected = AccessDeniedException.class)
  public void shouldFailToGetUserByIdWhenAuthUserIsNotApproverForUser() {
    // given
    mockUser2.setUserRole(UserRole.USER);
    mockUser.getTeam().setTeamApprovers(Collections.singletonList(mockApprover));
    given(userRepository.findById(mockUser.getId())).willReturn(Optional.of(mockUser));

    // when
    userService.findUserById(mockUser.getId(), mockUser2);

    // then
  }

  @Test
  public void shouldGetUserByEmail() {
    // given
    given(userRepository.findByEmail(mockUser.getEmail())).willReturn(Optional.of(mockUser));

    // when
    User user = userService.findByEmail(mockUser.getEmail());

    // then
    assertThat("Expect email to be 'aria.stark@gmail.com'", user.getEmail(), is(mockUser.getEmail()));

    verify(userRepository).findByEmail(anyString());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldSaveUser() {
    // given
    given(userRepository.save(mockUser)).willReturn(mockUser);

    // when
    User savedUser = userService.save(mockUser);

    // then
    assertThat("Expect name match to Aria Stark", savedUser.getFullName(), is(mockUser.getFullName()));

    verify(userRepository).save(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldUpdateUser() {
    // given
    mockUser.setFullName("Test");
    given(userRepository.findById(mockUser.getId())).willReturn(Optional.of(mockUser));
    given(userRepository.save(any())).willReturn(mockUser);
    given(userRepository.findByIdRequireNew(anyLong())).willReturn(mockUser);
    given(userRepository.getOne(mockUser.getId())).willReturn(mockUser);

    // when
    User updatedUser = userService.update(mockUser, mockUser2);

    // then
    assertThat("Expect that user is updated", updatedUser.getFullName(), is(mockUser.getFullName()));

    verify(userRepository).findById(anyLong());
    verify(userRepository).save(any());
    verify(userRepository).findByIdRequireNew(anyLong());
    verify(userRepository).getOne(anyLong());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldFailForUserToUpdateHisOwnRole() {
    // given
    var updatedMockUser = EntityBuilder.user(mockTeam);
    updatedMockUser.setUserRole(UserRole.USER);
    given(userRepository.findById(mockUser.getId())).willReturn(Optional.of(mockUser));
    given(userRepository.getOne(mockUser.getId())).willReturn(mockUser);

    // when
    userService.update(updatedMockUser, mockUser);

    // then
    verify(userRepository).findById(anyLong());
    verify(userRepository).getOne(anyLong());
  }

  @Test
  public void shouldCreateUserWithoutAllowance() {
    // given
    mockUser.setUserStatusType(UserStatusType.INACTIVE);
    mockUser.setStartedWorkingAtExecomDate(LocalDate.now().plusDays(1));
    mockUser.setYearsOfService(0);
    given(userRepository.existsByEmail(anyString())).willReturn(false);
    given(userRepository.getOne(mockUser.getId())).willReturn(mockUser);

    // when
    User user = userService.create(mockUser, mockUser2);

    // then
    assertThat("Expect users status to be 'INACTIVE'", user.getUserStatusType(), is(UserStatusType.INACTIVE));
    assertThat("Expect date at which user starts working at Execom to be in future",
        user.getStartedWorkingAtExecomDate(), greaterThan(LocalDate.now()));
    assertThat("Expect that user has no allowance", user.getAllowances().size(), is(0));
    assertThat("Expect that user has '0' years of service", user.getYearsOfService(), is(0));

    verify(userRepository).existsByEmail(anyString());
    verify(userRepository).save(any());
    verify(userRepository).getOne(anyLong());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldCreateUserWithAllowance() {
    // given
    mockUser.setUserStatusType(UserStatusType.INACTIVE);
    mockUser.setStartedWorkingAtExecomDate(LocalDate.now());
    mockUser.setYearsOfService(0);
    mockUser.setAllowances(null);
    given(userRepository.existsByEmail(anyString())).willReturn(false);
    given(yearRepository.findAllByYearGreaterThanEqualAndActiveTrue(anyInt())).willReturn(List.of(thisYear, nextYear));
    given(leaveProfileRepository.getOne(anyLong())).willReturn(EntityBuilder.leaveProfile());
    given(userRepository.getOne(mockUser.getId())).willReturn(mockUser);

    // when
    User user = userService.create(mockUser, mockUser2);

    // then
    assertThat("Expect users status to be 'ACTIVE'", user.getUserStatusType(), is(UserStatusType.ACTIVE));
    assertThat("Expect date at which user started working at Execom to be today",
        mockUser.getStartedWorkingAtExecomDate(), is(LocalDate.now()));
    assertThat("Expect that user has allowance for current and next year", user.getAllowances().size(), is(2));
    assertThat("Expect current year annual allowance to be less than next years",
        mockUser.getAllowances().get(0).getAnnual(), lessThan(mockUser.getAllowances().get(1).getAnnual()));
    assertThat("Expect that user has '5' years of service", user.getYearsOfService(), is(5));

    verify(userRepository).existsByEmail(anyString());
    verify(yearRepository).findAllByYearGreaterThanEqualAndActiveTrue(anyInt());
    verify(leaveProfileRepository, times(2)).getOne(anyLong());
    verify(userRepository).save(any());
    verify(userRepository).getOne(anyLong());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = EntityExistsException.class)
  public void shouldFailToCreateUserBecauseEmailExists() {
    // given
    given(userRepository.existsByEmail(anyString())).willReturn(true);

    // when
    userService.create(mockUser, mockUser2);

    // then
    verify(userRepository).existsByEmail(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldFailToCreateUserBecauseOfInputData() {
    // given
    byte[] array = new byte[100];
    new Random().nextBytes(array);
    String generatedString = new String(array, StandardCharsets.UTF_8);
    mockUser.setFullName(generatedString);
    given(userRepository.existsByEmail(anyString())).willReturn(false);
    given(userRepository.save(any())).willThrow(DataIntegrityViolationException.class);
    given(userRepository.getOne(mockUser.getId())).willReturn(mockUser);

    // when
    userService.create(mockUser, mockUser2);

    // then
    verify(userRepository).existsByEmail(any());
    verify(userRepository).save(any());
    verify(userRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldActivateNewlyCratedUser() {
    // given
    mockUser.setUserStatusType(UserStatusType.INACTIVE);
    mockUser.setStartedWorkingAtExecomDate(LocalDate.now().plusDays(10));
    mockUser.setAllowances(null);
    mockUser.setYearsOfService(100);
    given(allowanceService.existsByUserId(anyLong())).willReturn(false);
    given(yearRepository.findAllByYearGreaterThanEqualAndActiveTrue(anyInt())).willReturn(List.of(thisYear, nextYear));
    given(leaveProfileRepository.getOne(anyLong())).willReturn(EntityBuilder.leaveProfile());
    given(userRepository.findByIdRequireNew(mockUser.getId())).willReturn(mockUser);

    // when
    userService.activate(mockUser, mockUser2);

    // then
    assertThat("Expect users status to be 'ACTIVE'", mockUser.getUserStatusType(), is(UserStatusType.ACTIVE));
    assertThat("Expect date at which user started working at Execom to be today",
        mockUser.getStartedWorkingAtExecomDate(), is(LocalDate.now()));
    assertThat("Expect that user has allowances for two years", mockUser.getAllowances().size(), is(2));
    assertThat("Expect that user has '5' years of service", mockUser.getYearsOfService(), is(5));

    verify(allowanceService).existsByUserId(anyLong());
    verify(yearRepository).findAllByYearGreaterThanEqualAndActiveTrue(anyInt());
    verify(leaveProfileRepository, times(2)).getOne(anyLong());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(userRepository).findByIdRequireNew(any());
    verify(userRepository).save(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldActivateArchivedUser() {
    // given
    mockUser.setUserStatusType(UserStatusType.DELETED);
    mockUser.setStartedWorkingAtExecomDate(LocalDate.now());
    mockUser.setAllowances(userAllowances);
    mockUser.setYearsOfService(100);
    given(yearRepository.findAllByYearGreaterThanEqualAndActiveTrue(anyInt())).willReturn(List.of(thisYear, nextYear));
    given(leaveProfileRepository.getOne(anyLong())).willReturn(EntityBuilder.leaveProfile());
    given(userRepository.findByIdRequireNew(mockUser.getId())).willReturn(mockUser);
    given(allowanceService.getAllByUserId(anyLong())).willReturn(userAllowances);

    // when
    userService.activate(mockUser, mockUser2);

    // then
    assertThat("Expect users status to be 'ACTIVE'", mockUser.getUserStatusType(), is(UserStatusType.ACTIVE));
    assertThat("Expect date at which user started working at Execom to be today",
        mockUser.getStartedWorkingAtExecomDate(), is(LocalDate.now()));
    assertThat("Expect that user has allowances for two years", mockUser.getAllowances().size(), is(2));
    assertThat("Expect current year annual allowance to be less than next years",
        mockUser.getAllowances().get(0).getAnnual(), lessThan(mockUser.getAllowances().get(1).getAnnual()));
    assertThat("Expect that user has '5' years of service", mockUser.getYearsOfService(), is(5));

    verify(yearRepository).findAllByYearGreaterThanEqualAndActiveTrue(anyInt());
    verify(allowanceService).getAllByUserId(anyLong());
    verify(allowanceService, times(2)).delete(any(), any());
    verify(leaveProfileRepository, times(2)).getOne(anyLong());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(userRepository).findByIdRequireNew(any());
    verify(userRepository).save(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldScheduleForActivationArchivedUser() {
    // given
    mockUser.setUserStatusType(UserStatusType.DELETED);
    mockUser.setStartedWorkingAtExecomDate(LocalDate.now().plusDays(5));
    mockUser.setAllowances(userAllowances);
    mockUser.setYearsOfService(100);
    given(userRepository.findByIdRequireNew(mockUser.getId())).willReturn(mockUser);
    given(teamRepository.findByName(anyString())).willReturn(EntityBuilder.archivedTeam());

    // when
    userService.activate(mockUser, mockUser2);

    // then
    assertThat("Expect users status to be 'INACTIVE'", mockUser.getUserStatusType(), is(UserStatusType.INACTIVE));
    assertThat("Expect date at which user starts working at Execom to be in future",
        mockUser.getStartedWorkingAtExecomDate(), greaterThan(LocalDate.now()));
    assertThat("Expect that user has allowances for two years", mockUser.getAllowances().size(), is(2));
    assertThat("Expect that user has '100' years of service", mockUser.getYearsOfService(), is(100));

    verify(teamRepository).findByName(any());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(userRepository).findByIdRequireNew(any());
    verify(userRepository).save(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldFailToScheduleUserForActivation() {
    // given
    mockUser.setUserStatusType(UserStatusType.DELETED);
    mockUser.setStartedWorkingAtExecomDate(LocalDate.now().plusDays(5));
    mockUser.setTeam(EntityBuilder.archivedTeam());
    given(teamRepository.findByName(anyString())).willReturn(EntityBuilder.archivedTeam());

    // when
    userService.activate(mockUser, mockUser2);

    // then
    verify(teamRepository).findByName(anyString());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldFailToActivateActiveUser() {
    // given
    mockUser.setUserStatusType(UserStatusType.ACTIVE);

    // when
    userService.activate(mockUser, mockUser2);

    // then
    assertEquals("Expect that users status is 'ACTIVE'", UserStatusType.ACTIVE, mockUser.getUserStatusType());

    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldDeleteUser() {
    // given
    Team archivedTeam = EntityBuilder.archivedTeam();
    mockUser.setUserStatusType(UserStatusType.ACTIVE);
    mockUser.setApproverTeams(List.of(mockTeam));
    mockUser.getApproverTeams().get(0).setTeamApprovers(new ArrayList<>(Collections.singletonList(mockUser)));
    given(userRepository.getOne(anyLong())).willReturn(mockUser);
    given(teamRepository.findByName(anyString())).willReturn(archivedTeam);
    given(userRepository.findByIdRequireNew(anyLong())).willReturn(mockUser);
    given(userRepository.findAllByUserAdminPermission(any())).willReturn(Collections.singletonList(mockSuperAdmin));

    // when
    userService.delete(mockUser.getId(), mockUser2, LocalDate.now());

    // then
    assertEquals("Users status to be 'DELETED'", UserStatusType.DELETED, mockUser.getUserStatusType());
    assertEquals("Users team to be named 'Archived Users' ", mockUser.getTeam().getName(), archivedTeam.getName());
    assertFalse("Expect that user has no approver teams",
        mockUser.getApproverTeams().get(0).getTeamApprovers().contains(mockUser));

    verify(userRepository).getOne(anyLong());
    verify(userRepository).findAllByUserAdminPermission(any());
    verify(auditInformationService).saveAudit(any(), any(), any(), any(), any());
    verify(teamRepository).findByName(anyString());
    verify(userRepository).save(any());
    verify(userRepository).findByIdRequireNew(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToDeleteUserBecauseUsersRoleIsHrAssistant() {
    // given
    mockUser.setUserRole(UserRole.HR_ASSISTANT);
    given(userRepository.getOne(anyLong())).willReturn(mockUser);

    // when
    userService.delete(mockUser.getId(), mockUser2, LocalDate.now());

    // then
    verify(userRepository).getOne(anyLong());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldUpdateHalfAllowanceForCurrentYearAndFullForNextOnLeaveProfileUpdate() {
    // given
    mockUser.setLeaveProfile(EntityBuilder.leaveProfileIII());
    mockUser.setAllowances(List.of(EntityBuilder.allowance(mockUser), EntityBuilder.nextYearAllowance(mockUser)));
    var afterHalfYearDate = LocalDate.of(thisYear.getYear(), 12, 1);
    given(yearRepository.findAllByYearGreaterThanEqualAndActiveTrue(anyInt())).willReturn(List.of(thisYear, nextYear));
    given(allowanceService.getAllByUserId(anyLong())).willReturn(mockUser.getAllowances());

    // when
    userService.updateAllowanceForUserOnLeaveProfileUpdate(mockUser, EntityBuilder.leaveProfileII(), afterHalfYearDate);

    //then
    assertThat("Expect manual adjust to be '4'", mockUser.getAllowances().get(0).getManualAdjust(), is(4));
    assertThat("Expect that comment is left after allowance adjusting for current year",
        mockUser.getAllowances().get(0).getComment(), notNullValue());

    assertThat("Expect manual adjust for next year to be '8'", mockUser.getAllowances().get(1).getManualAdjust(),
        is(8));
    assertThat("Expect that comment is left after allowance adjusting for next year",
        mockUser.getAllowances().get(1).getComment(), notNullValue());

    verify(yearRepository).findAllByYearGreaterThanEqualAndActiveTrue(anyInt());
    verify(userRepository).findFirstByUserRole(any());
    verify(allowanceService).getAllByUserId(anyLong());
    verify(allowanceService, times(2)).update(any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldUpdateFullAllowanceForCurrentYearAndFullForNextOnLeaveProfileUpdate() {
    // given
    mockUser.setLeaveProfile(EntityBuilder.leaveProfileIII());
    mockUser.setAllowances(List.of(EntityBuilder.allowance(mockUser), EntityBuilder.nextYearAllowance(mockUser)));
    var beforeHalfYearDate = LocalDate.of(thisYear.getYear(), 1, 1);
    given(yearRepository.findAllByYearGreaterThanEqualAndActiveTrue(anyInt())).willReturn(List.of(thisYear, nextYear));
    given(allowanceService.getAllByUserId(anyLong())).willReturn(mockUser.getAllowances());

    // when
    userService.updateAllowanceForUserOnLeaveProfileUpdate(mockUser, EntityBuilder.leaveProfileII(),
        beforeHalfYearDate);

    //then
    assertThat("Expect manual adjust to be '8'", mockUser.getAllowances().get(0).getManualAdjust(), is(8));
    assertThat("Expect that comment is left after allowance adjusting for current year",
        mockUser.getAllowances().get(0).getComment(), notNullValue());

    assertThat("Expect manual adjust for next year to be '8'", mockUser.getAllowances().get(1).getManualAdjust(),
        is(8));
    assertThat("Expect that comment is left after allowance adjusting for next year",
        mockUser.getAllowances().get(1).getComment(), notNullValue());

    verify(yearRepository).findAllByYearGreaterThanEqualAndActiveTrue(anyInt());
    verify(userRepository).findFirstByUserRole(any());
    verify(allowanceService).getAllByUserId(anyLong());
    verify(allowanceService, times(2)).update(any(), any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldCreateNewUserPushToken() {
    // given
    CreateTokenDto createTokenDto = EntityBuilder.createTokenDto();
    mockUser.setUserPushTokens(null);
    given(userPushTokensRepository.findOneByUserAndPushToken(any(), anyString())).willReturn(Optional.empty());

    // when
    UserPushToken userPushToken = userService.getUsersPushToken(mockUser, createTokenDto);

    // then
    assertEquals("Expect that users push token was created", userPushToken.getUser(), mockUser);
    assertEquals("Expect that users push token value is 'anyPushToken'", userPushToken.getPushToken(),
        createTokenDto.getPushToken());

    verify(userPushTokensRepository).findOneByUserAndPushToken(any(), anyString());
    verify(userPushTokensRepository).save(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldReturnExistingUserPushToken() {
    // given
    CreateTokenDto createTokenDto = EntityBuilder.createTokenDto();
    UserPushToken oldPushToken = EntityBuilder.userAndroidPushToken();
    oldPushToken.setUser(mockUser);
    given(userPushTokensRepository.findOneByUserAndPushToken(any(), anyString())).willReturn(Optional.of(oldPushToken));

    // when
    UserPushToken userPushToken = userService.getUsersPushToken(mockUser, createTokenDto);

    // then
    assertEquals("Expect that old push token was used", userPushToken.getUser(), mockUser);
    assertEquals("Expect that users push token value is 'anyPushToken'", userPushToken.getPushToken(),
        oldPushToken.getPushToken());

    verify(userPushTokensRepository).findOneByUserAndPushToken(any(), anyString());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldDeleteUserPushToken() {
    // given
    UserPushToken androidPushToken = EntityBuilder.userAndroidPushToken();
    UserPushToken iosPushToken = EntityBuilder.userIOSPushToken();
    List<UserPushToken> userPushTokens = List.of(androidPushToken, iosPushToken);
    mockUser.setUserPushTokens(userPushTokens);

    // when
    userService.deleteUserPushToken(mockUser, androidPushToken.getId());

    // then
    verify(userPushTokensRepository).delete(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldCreatePageableUsersList() {
    // given
    PageRequest pageRequest = PageRequest.of(0, 5);
    Page<User> pagedUsers = new PageImpl<>(initialUsers, pageRequest, initialUsers.size());

    // when
    Page<User> result = userService.createPageableUsersList(initialUsers, pageRequest);

    // then
    assertEquals("Expect pagedUsers and result to be the same", pagedUsers, result);
  }

  @Test
  public void shouldConfirmThatUserIsMemberOrApproverInTeam() {
    // given
    given(userRepository.findById(anyLong())).willReturn(Optional.of(mockUser));

    // when
    userService.validateUserIsTeamMemberOrApprover(mockUser, mockTeam);

    // then
    assertEquals("Expect that user is member of given team.", mockTeam, mockUser.getTeam());
    verify(userRepository).findById(anyLong());
    verifyNoMoreInteractions(userRepository);
  }

  @Test(expected = UnauthorizedAccessException.class)
  public void shouldThrowUnauthorizedAccessExceptionIfUserIsNeitherMemberOrApproverOfGivenTeam() {
    // given
    given(userRepository.findById(anyLong())).willReturn(Optional.of(mockUser));

    // when
    userService.validateUserIsTeamMemberOrApprover(mockUser, EntityBuilder.team());

    // then
    verify(userRepository).findById(anyLong());
    verifyNoMoreInteractions(userRepository);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToCreateAdminOrSuperAdminUser() {
    // when
    userService.create(mockSuperAdmin, mockUser);
    userService.create(mockAdmin, mockUser);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToDeleteSuperAdmin() {
    // given
    given(userRepository.getOne(anyLong())).willReturn(mockSuperAdmin);
    given(userRepository.findAllByUserAdminPermission(any())).willReturn(Collections.singletonList(mockSuperAdmin));

    // when
    userService.delete(mockSuperAdmin.getId(), mockUser, LocalDate.now());

    // then
    verify(userRepository).getOne(anyLong());
    verify(userRepository).findAllByUserAdminPermission(any());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldRevokeAdmin() {
    // given
    given(userRepository.findById(mockAdmin.getId())).willReturn(Optional.of(mockAdmin));
    given(userRepository.save(any())).willReturn(mockRevokedAdmin);
    given(userRepository.findAllByUserAdminPermission(any())).willReturn(Collections.singletonList(mockSuperAdmin));

    // when
    User updatedUser = userService.updateAdmin(mockRevokedAdmin, mockSuperAdmin);

    // then
    assertThat("Expect that admin permission is NONE", updatedUser.getUserAdminPermission(), is(UserAdminPermission.NONE));

    verify(userRepository).findById(anyLong());
    verify(userRepository).save(any());
    verify(userRepository).findAllByUserAdminPermission(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test
  public void shouldAssignNewAdmin() {
    // given
    given(userRepository.findById(mockRevokedAdmin.getId())).willReturn(Optional.of(mockRevokedAdmin));
    given(userRepository.save(any())).willReturn(mockAdmin);
    given(userRepository.findAllByUserAdminPermission(any())).willReturn(Collections.singletonList(mockSuperAdmin));

    // when
    User updatedUser = userService.updateAdmin(mockAdmin, mockSuperAdmin);

    // then
    assertThat("Expect that admin permission is BASIC", updatedUser.getUserAdminPermission(), is(UserAdminPermission.BASIC));

    verify(userRepository).findById(anyLong());
    verify(userRepository).save(any());
    verify(userRepository).findAllByUserAdminPermission(any());
    verifyNoMoreInteractions(allMocks);
  }

  @Test(expected = ActionNotAllowedException.class)
  public void shouldFailToChangeUserAdminPermissionIfModifiedByNonSuperAdmin() {
    // given
    given(userRepository.findById(anyLong())).willReturn(Optional.of(mockUser2));
    given(userRepository.findAllByUserAdminPermission(any())).willReturn(Collections.singletonList(mockSuperAdmin));

    //when
    userService.updateAdmin(mockAdmin, mockUser);

    // then
    verify(userRepository).findById(anyLong());
    verify(userRepository).findAllByUserAdminPermission(any());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldConfirmUserIsApproverWhenHasApproverTeamsForUserRole() {
    //when
    mockUser3.getApproverTeams().add(mockTeam);

    //then
    assertTrue("Expect that user is approver.", mockUser3.isApprover());
  }

  @Test
  public void shouldConfirmUserIsNotApproverWhenDoesntHaveApproverTeamsForUserRole() {
    //when
    mockUser3.setApproverTeams(new ArrayList<>());

    //then
    assertFalse("Expect that user is not approver.", mockUser3.isApprover());
  }

  @Test
  public void shouldConfirmUserIsNotApproverWhenDoesntHaveApproverTeamsForOfficeManagerRole() {
    //when
    mockUser3.setApproverTeams(new ArrayList<>());
    mockUser3.setUserRole(UserRole.OFFICE_MANAGER);

    //then
    assertFalse("Expect that office manager is not approver.", mockUser3.isApprover());
  }

  @Test
  public void shouldConfirmUserIsApproverForUserOfficeManager() {
    //when
    mockUser3.getApproverTeams().add(mockTeam);
    mockUser3.setUserRole(UserRole.OFFICE_MANAGER);

    //then
    assertTrue("Expect that Office Manager is approver.", mockUser3.isApprover());
  }

  @Test
  public void shouldConfirmUserIsApproverForHrManagerRole() {
    //when
    mockUser3.setApproverTeams(new ArrayList<>());
    mockUser.setUserRole(UserRole.HR_MANAGER);

    //then
    assertFalse("Expect that Hr Manager is approver.", mockUser3.isApprover());
  }
}
