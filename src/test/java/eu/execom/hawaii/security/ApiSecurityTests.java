package eu.execom.hawaii.security;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserStatusType;
import eu.execom.hawaii.service.EntityBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ApiSecurityTests {

  private static final String SAMPLE_PROTECTED_USER_URL_PATH = "/api/security/user";
  private static final String SAMPLE_PROTECTED_OFFICE_MANAGER_URL_PATH = "/api/security/administration";
  private static final String SAMPLE_PROTECTED_HR_MANAGER_URL_PATH = "/api/security/hr_manager";
  private static final String SAMPLE_PROTECTED_ADMIN_URL_PATH = "/api/security/admin";
  private static final String SAMPLE_PROTECTED_SUPER_ADMIN_URL_PATH = "/api/security/super_admin";
  private static final String BEARER_TOKEN_HEADER = "Authorization";
  private static final String TEST_TOKEN = "JWT";
  private static final String BEARER_TOKEN = "Bearer " + TEST_TOKEN;

  @Autowired
  private TestRestTemplate restTemplate;

  @MockBean
  private CustomUserDetailsService customUserDetailsService;

  @MockBean
  private TokenProvider tokenProvider;

  private User mockUser;
  private User mockHrManager;
  private User mockOfficeManager;
  private User mockAdmin;
  private User mockSuperAdmin;

  @Before
  public void setUp() {
    mockUser = EntityBuilder.createUser();
    mockHrManager = EntityBuilder.approver();
    mockOfficeManager = EntityBuilder.officeManager();
    mockAdmin = EntityBuilder.admin();
    mockSuperAdmin = EntityBuilder.superAdmin();
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhenTokenHeaderIsNotSet() {
    // given

    // when
    ResponseEntity<String> response = restTemplate.getForEntity(SAMPLE_PROTECTED_USER_URL_PATH, String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhenTokenHeaderIsSetToEmptyValue() {
    // given

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_USER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader("   ")), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhenTokenIsInvalid() {
    // given

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_USER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader("test")), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhenTokenIsValidButUserIsInactive() {
    // given
    mockUser.setUserStatusType(UserStatusType.INACTIVE);

    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockUser);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_USER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnOkResponseForProtectedUrlRequestsWhenTokenIsValidAndUserIsActive() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockUser);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_USER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.OK));
    assertThat(response.getBody(), is("Security test action reached."));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhichRequireHrManagerAuthorityWhenTokenHeaderIsNotSet() {
    // given

    // when
    ResponseEntity<String> response = restTemplate.getForEntity(SAMPLE_PROTECTED_HR_MANAGER_URL_PATH, String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhichRequireHrManagerAuthorityWhenTokenHeaderIsSetToEmptyValue() {
    // given

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_HR_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader("   ")), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhichRequireHrManagerAuthorityWhenTokenIsInvalid() {
    // given

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_HR_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhichRequireHrManagerAuthorityWhenTokenIsValidButUserIsNotFound() {
    // given

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_HR_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnForbiddenStatusCodeForProtectedUrlRequestsWhichRequireOfficeOrHrManagerAuthorityWhenActiveUserHasUserAuthority() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockUser);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_OFFICE_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN));
  }

  @Test
  public void shouldReturnOkStatusCodeForProtectedUrlRequestsWhichRequireOfficeOrHrManagerAuthorityWhenActiveUserHasHrManagerAuthority() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockHrManager);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_OFFICE_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.OK));
    assertThat(response.getBody(), is("Security test for administration reached."));
  }

  @Test
  public void shouldReturnOkStatusCodeForProtectedUrlRequestsWhichRequireOfficeOrHrManagerAuthorityWhenActiveUserHasOfficeManagerAuthority() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockOfficeManager);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_OFFICE_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.OK));
    assertThat(response.getBody(), is("Security test for administration reached."));
  }

  @Test
  public void shouldReturnForbiddenStatusCodeForProtectedUrlRequestsWhichRequireBasicOrSuperAdminPermissonWhenActiveUserHasNoneAdminPermission() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockUser);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_ADMIN_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN));
  }

  @Test
  public void shouldReturnForbiddenStatusCodeForProtectedUrlRequestsWhichRequireSuperAdminPermissionWhenActiveUserHasBasicAdminPermission() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockAdmin);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_SUPER_ADMIN_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN));
  }

  @Test
  public void shouldReturnOkStatusCodeForProtectedUrlRequestsWhichRequireBasicAdminPermissionWhenActiveUserHasBasicSuperAdminPermission() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockSuperAdmin);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_ADMIN_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.OK));
    assertThat(response.getBody(), is("Security test for admin reached."));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForProtectedUrlRequestsWhichRequireHrManagerAuthorityWhenTokenIsValidButUserIsInactive() {
    // given
    mockHrManager.setUserStatusType(UserStatusType.INACTIVE);

    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockHrManager);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_HR_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  @Test
  public void shouldReturnForbiddenStatusCodeForProtectedUrlRequestsWhichRequireHrManagerAuthorityWhenActiveUserIsNotHrManager() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockUser);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_HR_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN));
  }

  @Test
  public void shouldReturnOkResponseForProtectedUrlRequestsWhichRequireHrManagerAuthorityWhenActiveUserIsHrManager() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("hawaii");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockHrManager);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_HR_MANAGER_URL_PATH, HttpMethod.GET,
        new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.OK));
    assertThat(response.getBody(), is("Security test for hr managers reached."));
  }

  @Test
  public void shouldReturnUnauthorizedStatusCodeForInvalidTokenIssuer() {
    // given
    given(tokenProvider.validateToken(TEST_TOKEN)).willReturn(true);
    given(tokenProvider.getUserEmailFromToken(TEST_TOKEN)).willReturn("test@execom.eu");
    given(tokenProvider.getIssuerFromToken(TEST_TOKEN)).willReturn("wrongIssuer");
    given(customUserDetailsService.loadUserByEmail("test@execom.eu")).willReturn(mockUser);

    // when
    ResponseEntity<String> response = restTemplate.exchange(SAMPLE_PROTECTED_USER_URL_PATH, HttpMethod.GET,
            new HttpEntity<>(createTokenHeader(BEARER_TOKEN)), String.class);

    // then
    assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
  }

  private HttpHeaders createTokenHeader(String value) {
    final HttpHeaders headers = new HttpHeaders();
    headers.add(BEARER_TOKEN_HEADER, value);

    return headers;
  }
}
