package eu.execom.hawaii.security;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/security")
class SecurityTestController {

  @GetMapping("/user")
  ResponseEntity<String> testAction() {
    return ResponseEntity.ok("Security test action reached.");
  }

  @GetMapping("/hr_manager")
  ResponseEntity<String> testActionHr() {
    return ResponseEntity.ok("Security test for hr managers reached.");
  }

  @GetMapping("/administration")
  ResponseEntity<String> testActionAdministration() {
    return ResponseEntity.ok("Security test for administration reached.");
  }

  @GetMapping("/admin")
  ResponseEntity<String> testActionAdmin() {
    return ResponseEntity.ok("Security test for admin reached.");
  }

  @GetMapping("/super_admin")
  ResponseEntity<String> testActionSuperAdmin() {
    return ResponseEntity.ok("Security test for super admin reached.");
  }
}
