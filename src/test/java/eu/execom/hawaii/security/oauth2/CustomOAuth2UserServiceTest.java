package eu.execom.hawaii.security.oauth2;

import eu.execom.hawaii.exceptions.OAuth2AuthenticationProcessingException;
import eu.execom.hawaii.exceptions.UnauthorizedEmailDomainException;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.service.EntityBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class CustomOAuth2UserServiceTest {

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private CustomOAuth2UserService customOAuth2UserService;

  private User mockUser;
  private OAuth2User mockOAuth2User;
  private OAuth2User mockOAuth2User2;
  private OAuth2User mockOAuth2User3;

  @Before
  public void setUp() {
    mockUser = EntityBuilder.approver();
    mockOAuth2User = EntityBuilder.createOAuth2User();
    mockOAuth2User2 = EntityBuilder.createOAuth2User2();
    mockOAuth2User3 = EntityBuilder.createOAuth2User3();
  }

  @Test
  public void shouldProcessOAuth2User() {
    // given
    var hrManagerAuthority = new SimpleGrantedAuthority(UserRole.HR_MANAGER.toString());
    given(userRepository.findByEmail(anyString())).willReturn(Optional.of(mockUser));
    given(userRepository.save(any())).willReturn(mockUser);

    // when
    OAuth2User oAuth2User = customOAuth2UserService.processOAuth2User(mockOAuth2User);

    // then

    assertThat("Expect that user has 'HR MANAGER' authority", oAuth2User.getAuthorities().contains(hrManagerAuthority),
        is(true));
    verify(userRepository).save(any());
    verify(userRepository).findByEmail(anyString());
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void shouldUpdateUsersImageWhenProcessingOAuth2User() {
    // given
    var oldImageUrl = mockUser.getImageUrl();
    given(userRepository.findByEmail(anyString())).willReturn(Optional.of(mockUser));
    given(userRepository.save(any())).willReturn(mockUser);

    // when
    customOAuth2UserService.processOAuth2User(mockOAuth2User);

    // then
    assertThat("Expect that users image has been updated", mockUser.getImageUrl().equals(oldImageUrl), is(false));
    verify(userRepository).save(any());
    verify(userRepository).findByEmail(anyString());
    verifyNoMoreInteractions(userRepository);
  }

  @Test(expected = OAuth2AuthenticationProcessingException.class)
  public void shouldFailToProcessUserIfEmailIsNotPresent() {
    // given

    // when
    customOAuth2UserService.processOAuth2User(mockOAuth2User2);

    // then
  }

  @Test(expected = UnauthorizedEmailDomainException.class)
  public void shouldFailToProcessUserIfHisEmailIsNotHostedByExecom() {
    // given

    // when
    customOAuth2UserService.processOAuth2User(mockOAuth2User3);

    // then
  }

}
