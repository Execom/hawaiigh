package eu.execom.hawaii.util;

import eu.execom.hawaii.util.export.CsvGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CsvGeneratorTest {

  @Test
  public void shouldEscapeSpecialCharacters()
          throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method escapeSpecialCharactersMethod = CsvGenerator.class.getDeclaredMethod("escapeSpecialCharacters", String.class);
    escapeSpecialCharactersMethod.setAccessible(true);

    // given
    String newRowCharacter = "Replacing new row character \n with whitespace.";
    String commas = "Fields containing commas, will be escaped with double quotes";
    String quotes = "Fields containing 'quotes' will be escaped with double quotes";
    String doubleQuotes = "Fields containing \"doubleQuotes\", will be escaped with double quotes";

    // when
    String escapedNewRowCharacter = (String) escapeSpecialCharactersMethod.invoke(null, newRowCharacter);
    String escapedComma = (String) escapeSpecialCharactersMethod.invoke(null, commas);
    String escapedQuotes = (String) escapeSpecialCharactersMethod.invoke(null, quotes);
    String escapedDoubleQuotes = (String) escapeSpecialCharactersMethod.invoke(null, doubleQuotes);

    //then
    assertFalse("Does not contain new row character", escapedNewRowCharacter.contains("\n"));
    assertTrue("Field containing comma is escaped with double quotes", escapedComma.contains("\""));
    assertTrue("Field containing quotes is escaped with double quotes", escapedQuotes.contains("\""));
    assertTrue("Field containing double quotes is escaped with double quotes", escapedDoubleQuotes.contains("\"\""));
  }

  @Test
  public void shouldConvertToCsv() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method convertToCsvMethod = CsvGenerator.class.getDeclaredMethod("convertToCsv", String[].class);
    convertToCsvMethod.setAccessible(true);

    // given
    String[] user = {"Aria", "Stark", "astark@execom.eu", "ACTIVE", "firstTeam", "firstLeaveProfile"};

    // when
    String csvFile = (String) convertToCsvMethod.invoke(null, (Object) user);

    // then
    assertTrue("Csv file contains commas", csvFile.contains(","));
  }

}