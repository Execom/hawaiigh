package eu.execom.hawaii.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class EmailValidatorTest {

  @InjectMocks
  private EmailDomainValidator emailDomainValidator;

  @Test
  public void shouldExcludeUnauthorizedEmails() {
    String emptyEmail = "";
    String multipleValidEmails = "first@execom.eu, second@execom.eu, third@execom.eu, last@execom.eu";
    String oneValidEmail = "only@execom.eu";
    String validAndInvalidEmails = "first@execom.eu, first@first.eu, second@execom.eu, second@second.eu";
    String oneInvalidEmail = "only@only.eu";

    boolean isNullEmailValid = emailDomainValidator.isValid(null, null);
    boolean isEmptyEmailValid = emailDomainValidator.isValid(emptyEmail, null);
    boolean areMultipleValidEmailsValid = emailDomainValidator.isValid(multipleValidEmails, null);
    boolean isOneValidEmailValid = emailDomainValidator.isValid(oneValidEmail, null);
    boolean areValidAndInvalidEmailsValid = emailDomainValidator.isValid(validAndInvalidEmails, null);
    boolean isOneInvalidEmailValid = emailDomainValidator.isValid(oneInvalidEmail, null);

    assertTrue("Email is valid", isNullEmailValid);
    assertTrue("Email is valid", isEmptyEmailValid);
    assertTrue("Emails are valid", areMultipleValidEmailsValid);
    assertTrue("Email is valid", isOneValidEmailValid);
    assertFalse("Emails are invalid", areValidAndInvalidEmailsValid);
    assertFalse("Email is invalid", isOneInvalidEmailValid);
  }
}