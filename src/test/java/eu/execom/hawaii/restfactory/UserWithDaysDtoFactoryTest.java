package eu.execom.hawaii.restfactory;

import eu.execom.hawaii.dto.UserWithDaysDto;
import eu.execom.hawaii.model.Day;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.service.DayService;
import eu.execom.hawaii.service.EntityBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.time.LocalDate;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserWithDaysDtoFactoryTest {

  @Mock
  private DayService dayService;

  @InjectMocks
  private UserWithDaysDtoFactory userWithDaysDtoFactory;

  private LocalDate startDate;
  private LocalDate endDate;

  private User mockUser;
  private List<Day> mockUserDays;

  @Before
  public void setUp() {
    var thisYear = LocalDate.now().getYear();
    mockUser = EntityBuilder.user(EntityBuilder.team());
    startDate = LocalDate.of(thisYear, 1, 1);
    endDate = LocalDate.of(thisYear, 1, 10);
    Day mockDay = EntityBuilder.day(startDate.plusDays(1));
    mockDay.setRequest(EntityBuilder.request(EntityBuilder.absenceSickness(), List.of(mockDay)));
    Day mockDay2 = EntityBuilder.day(startDate.plusDays(2));
    mockDay2.setRequest(EntityBuilder.request(EntityBuilder.absenceAnnual(), List.of(mockDay2)));
    Day mockDay3 = EntityBuilder.day(startDate.plusDays(3));
    mockDay3.setRequest(EntityBuilder.request(EntityBuilder.absenceAnnual(), List.of(mockDay3)));
    mockDay3.getRequest().getAbsence().setDeducted(false);
    mockUserDays = List.of(mockDay, mockDay2, mockDay3);
  }

  @Test
  public void shouldCreateUserWithDaysDto() {
    // given
    given(dayService.getUserAbsencesDays(mockUser, startDate, endDate)).willReturn(mockUserDays);

    // when
    UserWithDaysDto userWithDaysDto = userWithDaysDtoFactory.createUserWithDaysDto(mockUser, startDate, endDate);

    // then
    assertEquals("Expect that user has exactly '3' leave days in given time period.", 3,
        userWithDaysDto.getDays().size());
    assertEquals("Expect that user has exactly '1' sickness day in given time period.", 1,
        (long) userWithDaysDto.getSickDays());
    assertEquals("Expect that user has exactly '1' leave day in given time period.", 1,
        (long) userWithDaysDto.getSickDays());
    assertEquals("Expect that user has exactly '1' non deducted leave day in given time period.", 1,
        (long) userWithDaysDto.getNonDeductedDays());
    verify(dayService).getUserAbsencesDays(any(), any(), any());
    verifyNoMoreInteractions(dayService);
  }

}
