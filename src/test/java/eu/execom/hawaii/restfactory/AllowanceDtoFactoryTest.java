package eu.execom.hawaii.restfactory;

import eu.execom.hawaii.dto.AllowanceForUserDto;
import eu.execom.hawaii.dto.AllowanceWithoutYearDto;
import eu.execom.hawaii.model.Allowance;
import eu.execom.hawaii.model.User;
import eu.execom.hawaii.repository.AllowanceRepository;
import eu.execom.hawaii.service.AllowanceService;
import eu.execom.hawaii.service.EntityBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class AllowanceDtoFactoryTest {

  @Mock
  private AllowanceService allowanceService;

  @Mock
  private AllowanceRepository allowanceRepository;

  @InjectMocks
  private AllowanceDtoFactory allowanceDtoFactory;

  private int thisYear;
  private User mockUser;
  private Allowance currentYearAllowance;

  @Before
  public void setUp() {
    thisYear = EntityBuilder.thisYear().getYear();
    mockUser = EntityBuilder.user(EntityBuilder.team());
    currentYearAllowance = EntityBuilder.allowance(mockUser);
  }

  @Test
  public void shouldCreateAllowanceWithoutYearDto() {
    // given
    given(allowanceService.calculateRemainingAnnualHours(currentYearAllowance)).willReturn(10);
    given(allowanceService.calculateRemainingTrainingHours(currentYearAllowance)).willReturn(10);
    given(allowanceService.calculateTotalAnnualHours(currentYearAllowance)).willReturn(10);
    given(allowanceService.getRemainingHoursNextYear(currentYearAllowance)).willReturn(0);
    given(allowanceService.calculateRemainingBonusHours(currentYearAllowance)).willReturn(10);

    // when
    AllowanceWithoutYearDto allowanceWithoutYearDto = allowanceDtoFactory.createAllowanceWithoutYearDto(
        currentYearAllowance);

    // then
    assertEquals("Expect that there is '10' annual hours remaining in current year allowance.", 10,
        allowanceWithoutYearDto.getRemainingAnnual());
    assertEquals("Expect that there is '0' total bonus hours in current year allowance.", 0,
        allowanceWithoutYearDto.getTotalBonus());
    assertEquals("Expect that there is '16' total training hours in current year allowance.", 16,
        allowanceWithoutYearDto.getTotalTraining());

    verify(allowanceService).calculateRemainingAnnualHours(any());
    verify(allowanceService).calculateRemainingTrainingHours(any());
    verify(allowanceService).calculateTotalAnnualHours(any());
    verify(allowanceService).getRemainingHoursNextYear(any());
    verify(allowanceService).calculateRemainingBonusHours(any());
    verifyNoMoreInteractions(allowanceService);
  }

  @Test
  public void shouldGetAllowancesForUser() {
    // given
    currentYearAllowance.setTakenAnnual(100);
    given(allowanceService.getByUserAndYear(mockUser.getId(), thisYear)).willReturn(currentYearAllowance);
    given(allowanceService.nextYearAllowanceExists(currentYearAllowance)).willReturn(true);
    given(allowanceService.calculateRemainingAnnualHours(any())).willReturn(100);
    given(allowanceService.calculateRemainingTrainingHours(any())).willReturn(16);
    given(allowanceService.calculateNextYearRemainingAnnualHours(any())).willReturn(40);

    // when
    AllowanceForUserDto allowanceForUserDto = allowanceDtoFactory.getAllowancesForUser(mockUser);

    //then
    assertThat("Expect remaining annual hours to be 100", allowanceForUserDto.getRemainingAnnualHours(), is(100));
    assertThat("Expect next year remaining annual hours to be 40",
        allowanceForUserDto.getNextYearRemainingAnnualHours(), is(40));
    assertThat("Expect remaining training hours to be 40", allowanceForUserDto.getRemainingTrainingHours(), is(16));

    verify(allowanceService).getByUserAndYear(anyLong(), anyInt());
    verify(allowanceService).nextYearAllowanceExists(any());
    verifyNoMoreInteractions(allowanceRepository);
  }

  @Test
  public void shouldSetNextYearRemainingAnnualHoursToZeroIfNextYearIsNotActive() {
    // given
    currentYearAllowance.setTakenAnnual(100);
    given(allowanceService.getByUserAndYear(mockUser.getId(), thisYear)).willReturn(currentYearAllowance);
    given(allowanceService.nextYearAllowanceExists(currentYearAllowance)).willReturn(false);
    given(allowanceService.calculateRemainingAnnualHours(any())).willReturn(100);
    given(allowanceService.calculateRemainingTrainingHours(any())).willReturn(16);

    // when
    AllowanceForUserDto allowanceForUserDto = allowanceDtoFactory.getAllowancesForUser(mockUser);

    // then
    assertEquals("Expect next year remaining annual hours to be '0'", 0,
        allowanceForUserDto.getNextYearRemainingAnnualHours());

    verify(allowanceService).getByUserAndYear(anyLong(), anyInt());
    verify(allowanceService).nextYearAllowanceExists(any());
    verify(allowanceService).calculateRemainingAnnualHours(any());
    verify(allowanceService).calculateRemainingTrainingHours(any());
    verifyNoMoreInteractions(allowanceService);
  }
}
