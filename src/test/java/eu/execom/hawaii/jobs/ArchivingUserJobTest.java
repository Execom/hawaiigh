package eu.execom.hawaii.jobs;

import eu.execom.hawaii.model.User;
import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.service.EntityBuilder;
import eu.execom.hawaii.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class ArchivingUserJobTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private UserService userService;

  @InjectMocks
  private ArchivingUserJob archivingUserJob;

  private User leavingUser;

  @Before
  public void setUp() {
    leavingUser = EntityBuilder.leavingUser();
  }

  @Test
  public void logicallyDeleteUser() {
    // given
    given(userRepository.findAllByUserStatusTypeAndStoppedWorkingAtExecomDateIsNotNull(any())).willReturn(
        Collections.singletonList(leavingUser));

    // when
    archivingUserJob.logicallyDeleteUser();

    // then
    assertThat("Expect user status to be 'DELETED'", leavingUser.isDeleted(), is(true));
    verify(userRepository).findAllByUserStatusTypeAndStoppedWorkingAtExecomDateIsNotNull(any());
    verify(userRepository).findFirstByUserRole(UserRole.HR_ASSISTANT);
    verify(userService).delete(any(), any(), any());
    verifyNoMoreInteractions(userService, userRepository);
  }
}