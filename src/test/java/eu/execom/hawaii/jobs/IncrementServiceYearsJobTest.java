package eu.execom.hawaii.jobs;

import eu.execom.hawaii.model.enumerations.UserRole;
import eu.execom.hawaii.repository.LeaveProfileRepository;
import eu.execom.hawaii.repository.UserRepository;
import eu.execom.hawaii.service.EmailService;
import eu.execom.hawaii.service.EntityBuilder;
import eu.execom.hawaii.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class IncrementServiceYearsJobTest {

  @Mock
  private LeaveProfileRepository leaveProfileRepository;

  @Mock
  private EmailService emailService;

  @Mock
  private UserService userService;

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private IncrementServiceYearsJob incrementServiceYearsJob;

  private Object[] allMocks;

  @Before
  public void setUp() {
    allMocks = new Object[] {leaveProfileRepository, emailService, userService, userRepository};
  }

  @Test
  public void shouldAddServiceYearsToUser() {
    // given
    var user1 = EntityBuilder.user(EntityBuilder.team());
    var user2 = EntityBuilder.approver();
    var activeUsers = List.of(user1, user2);
    given(userService.findAllByUserStatusType(any())).willReturn(activeUsers);

    // when
    incrementServiceYearsJob.addServiceYearsToUser();

    //than
    assertThat("Expect years of service to be incremented to 5", user1.getYearsOfService(), is(5));
    assertThat("Expect leave profile id to be 1", user1.getLeaveProfile().getId(), is(1L));
    assertThat("Expect years of service to be incremented to 10", user2.getYearsOfService(), is(10));
    verify(userService).findAllByUserStatusType(any());
    verify(emailService).sendLeaveProfileUpdateEmail(any());
    verify(userService, times(2)).update(any(), any());
    verify(userService).updateAllowanceForUserOnLeaveProfileUpdate(any(), any(), any());
    verify(userRepository, times(2)).findFirstByUserRole(UserRole.HR_ASSISTANT);
    verifyNoMoreInteractions(allMocks);
  }
}